<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['my_content'] = 'home';	
		$this->load->view('v_home', $data);
	}	

	public function detail($menu_detail = 'home')
	{		
		$menuArr = array("ist", "sls", "ost", "stk", "sth", "sts", "slc","hpp","srp", "sld", "irp", "orp", "usr");
		// if( in_array($menu_detail, $menuArr) ){
		// if($menu_detail == 'ist' || $menu_detail == 'sls' || $menu_detail == 'ost'){
			// $this->load->model('masters/M_store');
			// $data['stores'] = $this->M_store->loadAll();			
		// }

		/* User Menu Admin */
		if($menu_detail == 'acs'){
			$this->load->model('admin/M_mstusermenu');
			$data['user_list'] = $this->M_mstusermenu->getUserRow();				
		}

		$data['my_content'] = $this->security->xss_clean($menu_detail);	
		$this->load->view('v_home', $data);
	}
}

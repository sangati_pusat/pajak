<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("admin/M_mstusermenu");		
	}

	public function index()
	{
		$data['loginMsg'] = '';
		$this->load->view('login', $data);
	}

	public function out()
	{
		$this->session->unset_userdata('hris_user_id');
		$this->session->unset_userdata('hris_user_password');
		$this->session->unset_userdata('my_menu');
		$this->session->sess_destroy();
		$data['loginMsg'] = '';
		$this->load->view('login', $data);
	}

	public function validate()
	{
		$user = '';
		$pass = '';
		$userGroup = '';
		$storeCode = '';
		if(isset($_POST['inputUser']) && isset($_POST['inputPassword']))
		{
			$user = $this->security->xss_clean($_POST['inputUser']);
			$pass = $this->security->xss_clean($_POST['inputPassword']);

			/* CHECK USER */
			$isValid = $this->M_mstusermenu->getUserByLogin($user, $pass);
			if($isValid >= 1){
				/* GET MENUS */
				$menuRows = $this->M_mstusermenu->loadMenuByLogin($user, $pass);

				/*echo "<pre>";
				print_r($menuRows);
				echo "</pre>";
				exit(0);*/

				$menuArray = array();
				foreach ($menuRows as $row) {
					$menuArray[] = $row['menu_code'];
					$userGroup = $row['user_group']; 
					// $storeCode = $row['store_code']; 
				}
				
				$this->session->set_userdata('my_menu', $menuArray);
				$this->session->set_userdata('hris_user_id', $user);
				$this->session->set_userdata('hris_user_password', $pass);
				$this->session->set_userdata('hris_user_group', $userGroup);
				// $this->session->set_userdata('pos_store_code', $storeCode);

				/* CREATE USER SESSION */
				// $data['my_menu'] = $menuArray;
				$data['my_content'] = 'home';	
				$this->load->view('v_home', $data);			
				// echo "Hello";exit(0);
			}
			else
			{
				// $this->load->library('encrypt');
				// $msg = 'Maurice Secret'; //Plain text 
				// $key = 'kar129056lll21!!!';
				// // Default MCRYPT_RIJNDAEL_256
				// $tEnc = $this->encrypt->encode($msg, $key);
				// $tDec = $this->encrypt->decode($tEnc, $key);
				// $tHasil = 'Encrypt : '.$tEnc.'-> Decrypt : '.$tDec;
				// $data['loginMsg'] = $tHasil;
				$data['loginMsg'] = 'User atau Password Anda Salah';
				$this->load->view('login', $data);				
			}			
		}
		else
		{
			$data['loginMsg'] = 'User atau password Anda salah';
			$this->load->view('login', $data);		
		}
	}

	public function isPasswordValid()
	{
		if(isset($_POST['userId'])){
			$user = $this->security->xss_clean($_POST['userId']);
		}
		if(isset($_POST['userPass'])){
			$pass = $this->security->xss_clean($_POST['userPass']);
		}
		$rs = $this->M_mstusermenu->isPasswordValid($user, $pass);
		echo $rs;
	}

	public function updatePassword()
	{
		if(isset($_POST['userId'])){
			$user = $this->security->xss_clean($_POST['userId']);
		}
		if(isset($_POST['newPass'])){
			$pass = $this->security->xss_clean($_POST['newPass']);
		}
		
		$this->M_mstusermenu->updatePasswordById($user, $pass);
	}
	
}

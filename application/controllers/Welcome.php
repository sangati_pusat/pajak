<?php
defined('BASEPATH') OR exit('No direct script access allowed');


// Load library phpspreadsheet
    require('./vendor/autoload.php');
    use PhpOffice\PhpSpreadsheet\Helper\Sample;
    use PhpOffice\PhpSpreadsheet\IOFactory;
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    // End load library phpspreadsheet

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}

	/* JUST FOR TEST */ 
     // public function getAutoComplete()
     //  {
     //     $name = '';
     //     if(isset($_POST['term'])){
     //       $name = $_POST['term'];
     //     }
     //     $this->db->select('bio_rec_id, full_name, id_card_address, cell_no, position');
     //     $this->db->from($this->db->database.'.mst_bio_rec');
     //     $this->db->like('full_name', $name);
     //     $this->db->order_by('full_name', 'ASC');
     //     $this->db->limit(20);
     //     $query = $this->db->get()->result_array();
     //     // echo $this->db->last_query(); exit(0);
     //     // return $query;
     //     /*return json_encode($query);*/
     //     $myData = array();
     //     foreach ($query as $key => $row) {
     //        $myData[] = array(
     //            'bioid' => $row['bio_rec_id'],         
     //            'fname' => $row['full_name'],         
     //            'address' => $row['id_card_address'],         
     //            'cellno' => $row['cell_no'],         
     //            'fposition' => $row['position']
     //        );            
     //     }  
     //     echo json_encode($myData);      
     //  }


      public function getItemMultiAutoByCode()
      { 
        $name = '';
        if(isset($_POST['name'])){
          $name = $_POST['name'];          
        }      

        $request = 0;
        if(isset($_POST['request'])){
          $request = $_POST['request'];          
        }
        if($request == 1){
            $strSql  = 'SELECT bio_rec_id, full_name, id_card_address, cell_no, position ';
            $strSql .= 'FROM '.$this->db->database.'.mst_bio_rec ';
            $strSql .= 'WHERE full_name LIKE "%'.$name.'%" ';
            $strSql .= 'ORDER BY full_name LIMIT 25';
            $rs = $this->db->query($strSql)->result_array();
            $myData = array();
            foreach ($rs as $row) {
                $myData[] = array(
                  'bio_id' => $row['bio_rec_id'],
                  'bio_name' => $row['full_name'],
                  'bio_address'  => $row['id_card_address'],
                  'bio_position'  => $row['position']
                ); 
            }
            echo json_encode($myData);
            exit(0);
        }

        // Get details
        if($request == 2)
        {
            $bioId = $_POST['bioId'];
            $strSql  = 'SELECT bio_rec_id, full_name, id_card_address, cell_no, position ';
            $strSql .= 'FROM '.$this->db->database.'.mst_bio_rec ';
            $strSql .= 'WHERE bio_rec_id = "'.$bioId.'" ';
            $strSql .= 'ORDER BY full_name LIMIT 25';
            $rs = $this->db->query($strSql)->result_array();
            // $rs = $this->db->query($strSql)->row_array();
            $myData = array();
            foreach ($rs as $row) {
                $myData[] = array(
                  'bio_id' => $row['bio_rec_id'],
                  'bio_name' => $row['full_name'],
                  'bio_address'  => $row['id_card_address'],
                  'bio_position'  => $row['position']
                ); 
            }
            echo json_encode($myData);
            // echo $strSql;
            exit(0);               
        }

      }


      // Export ke excel
        public function export()
        {
            // $provinsi = $this->provinsi_model->listing();
            // Create new Spreadsheet object
            $spreadsheet = new Spreadsheet();

            if (file_exists('assets/images/report_logo.jpg')) {
                // $gdImage = imagecreatefromjpeg('assets/images/report_logo.jpg');
                // Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
                // $objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
                // $objDrawing->setName('Sample image');
                // $objDrawing->setDescription('Sample image');
                // $objDrawing->setImageResource($gdImage);
                // $objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG);
                // $objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
                // $objDrawing->setHeight(70);
                // $objDrawing->setCoordinates('A1');
                // $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

                // echo "Hello"; exit(0);

                $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                $drawing->setName('Logo');
                $drawing->setDescription('Logo');
                $drawing->setPath('./assets/images/report_logo.jpg');
                $drawing->setCoordinates('C1');
                $drawing->setHeight(36);
                $drawing->setWorksheet($spreadsheet->getActiveSheet());
            }

            // Set document properties
            $spreadsheet->getProperties()->setCreator('Andoyo - Java Web Media')
                ->setLastModifiedBy('Andoyo - Java Web Medi')
                ->setTitle('Office 2007 XLSX Test Document')
                ->setSubject('Office 2007 XLSX Test Document')
                ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
                ->setKeywords('office 2007 openxml php')
                ->setCategory('Test result file');

            $totalStyle = array(
                'font'  => array(
                    'bold'  => true,
                    'color' => array('rgb' => '0000FF'),
                    // 'size'  => 15,
                    // 'name'  => 'Verdana'
                )
            );
            
            $allBorderStyle = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                    )
                )
            );

            $outlineBorderStyle = array(
              'borders' => array(
                'outline' => array(
                  'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                )
              )
            );

            $topBorderStyle = array(
              'borders' => array(
                'top' => array(
                  'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                )
              )
            );

            $bottomBorderStyle = array(
              'borders' => array(
                'bottom' => array(
                  'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                )
              )
            );

            $center = array();
            $center['alignment'] = array();
            $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
            $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

            $right = array();
            $right['alignment'] = array();
            $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
            $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

            $left = array();
            $left['alignment'] = array();
            $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
            $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

            /*$spreadsheet->getActiveSheet()->getStyle('B2')
                ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);
            $spreadsheet->getActiveSheet()->getStyle('B2')
                ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
            $spreadsheet->getActiveSheet()->getStyle('B2')
                ->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
            $spreadsheet->getActiveSheet()->getStyle('B2')
                ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
            $spreadsheet->getActiveSheet()->getStyle('B2')
                ->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
            $spreadsheet->getActiveSheet()->getStyle('B2')
                ->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
            $spreadsheet->getActiveSheet()->getStyle('B2')
                ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
            $spreadsheet->getActiveSheet()->getStyle('B2')
                ->getFill()->getStartColor()->setARGB('FFFF0000');*/

            /* AUTO WIDTH */     
            // foreach(range('A','B') as $columnID)
            // {
            //     $spreadsheet->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
            // }
 

            /* COLOURING FOOTER */
            $spreadsheet->getActiveSheet()->getStyle('A1:B1')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('F2BE6B');
            $spreadsheet->getActiveSheet()->getStyle('A1:B1')
                ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLUE);
       

            // Add some data
            $spreadsheet->getActiveSheet()
                ->setCellValue('A1', 'KODE PROVINSI')
                ->setCellValue('B1', 'NAMA PROVINSI');
            $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);

            $spreadsheet->getActiveSheet()->getStyle("A1:B1")->applyFromArray($center);
            $spreadsheet->getActiveSheet()->getStyle("A1:B1")->applyFromArray($allBorderStyle);

            $x = 0;
            for ($i=2; $i <= 11; $i++) { 
                # code...
                $x++;
                $spreadsheet->getActiveSheet()
                    ->setCellValue('A'.$i, 'A'.$x);
                    // ->setCellValue('B'.$i, $i);

                $spreadsheet->getActiveSheet()
                    ->setCellValueExplicit(
                        'B'.$i,
                        $x,
                        \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING
                    );

                   /* SET ROW COLOR */
                    if($i % 2 == 1)
                    {
                        $spreadsheet->getActiveSheet()->getStyle('A'.$i.':B'.$i)
                        ->getFill()
                        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('EAEBAF');             
                    } 
            }

            // Miscellaneous glyphs, UTF-8
            /*$i=2; foreach($provinsi as $provinsi) {

                $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue('A'.$i, $provinsi->id_provinsi)
                    ->setCellValue('B'.$i, $provinsi->nama_provinsi);
                    $i++;
            }*/


            // Rename worksheet
            $spreadsheet->getActiveSheet()->setTitle('Report Excel '.date('d-m-Y H'));

            // Set active sheet index to the first sheet, so Excel opens this as the first sheet
            $spreadsheet->setActiveSheetIndex(0);

            // Redirect output to a client’s web browser (Xlsx)
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0

            $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
            $writer->save('php://output');
            exit;
        }

        public function cetakPrinter(){            
            /* Menginstall Extensi php_printer di OS Windows
            Ekstensi PHP printer yang telah diunduh selanjutnya disalin kedalam folder dimana PHP di-install. 
            Selanjuntya ekstensi didaftarkan  agar dikenali oleh PHP dengan cara menambahkan kode berikut di file php.ini.
            extension=php_printer.dll
            Setelah itu, silahkan restart service apache, 
            dan kemudian tulis contoh kode berikut kedalam sebuah file PHP. */

            /* https://www.w3schools.com/php/func_string_wordwrap.asp 
            $str = "An example of a long word is: Supercalifragulistic";
            echo wordwrap($str,15,"<br>\n"); */
            
            $text  = "Hasaniva Service\n";
            $text .= "Jl. Surabaya Jawa Timur\n";
            $text .= "-------------------------\n";
            $text .= "No Antrian Anda : \n";
            $text .= "                         \n";
            $text .= "-------------------------\n";
            $text .= "Budayakan Tertib Antri\n";
            $text .= "Terima Kasih atas kunjungannya\n";
            $text .= date("d/m/Y h:i:s")."\n";
            // $printer = printer_open("\\\\SONNY\EPSON L555 Series"); //open printer
            $printer = printer_open("XP-58"); //open printer
            printer_write($printer, $text);   
            printer_close($printer);
        }
}

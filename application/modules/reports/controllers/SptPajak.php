<?php
defined('BASEPATH') OR exit('No direct script access allowed');
    
    use application\third_party\fpdf182\FPDF;
    // Load library phpspreadsheet
    require('./vendor/autoload.php');
    use PhpOffice\PhpSpreadsheet\Helper\Sample;
    use PhpOffice\PhpSpreadsheet\IOFactory;
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    // End load library phpspreadsheet


class SptPajak extends CI_Controller {

    public function __construct(){
        parent::__construct();
        
        include_once APPPATH . '/third_party/fpdf182/fpdf.php';
        $this->load->model('transactions/M_salary_slip'); 
        $this->load->model('masters/M_payroll_config'); 
        $this->load->model('masters/M_mst_pajak'); 

    }

    public function getViewData($pt, $year)
    {
        $this->db->select('*');
        $this->db->from($this->db->database.'.mst_spt_tax');
        $this->db->where('client_name', $pt);
        // if($group != 'A'){
        //     $this->db->where('payroll_group', $group);
        // }

        $this->db->where('tahun_pajak', $this->security->xss_clean($year));
        // $this->db->where('month_period', $this->security->xss_clean($month));
        $this->db->order_by('full_name',"asc");
        $query = $this->db->get()->result_array();
        // echo $this->db->last_query(); exit(0);
        /*return json_encode($query);*/
        $myData = array();
        foreach ($query as $key => $row) {
            $myData[] = array(
                $row['spt_id'],         
                $pt,         
                $row['full_name'],               
                $row['position']         
            
            );            
        }  
        echo json_encode($myData);   
    }

    public function toExport($clientName, $fullName)
    {             

        if($clientName == "Pontil_Sumbawa")
        {
            $this->printPdfPontilSmb_new($clientName, $fullName);
        } 
        elseif ($clientName == "Pontil_Timika") {
       
           $this->printPdfPontilBwg($clientName, $fullName);
        }               
    }

    // START EXPORT PDF //
    // function printPdfPontilSmb($clientName, $fullName){

    //     // $mPdf    = new M_mst_pajak ();
    //     $fullName      = $this->security->xss_clean($fullName);
    //     $pdf     = new FPDF();
    //     $pdf->AddPage();

    //     $queryTest = " mst_spt_tax 
    //                    WHERE client_name = '".$clientName."' 
    //                    AND full_name = '".$fullName."' ";

    //     $mPdf = $this->db->get($queryTest)->result_array();
    //     foreach ($mPdf as $row){
    //         $masa_awal = $row['masa_awal'];
    //         $masa_pajak = $row['masa_pajak'];
    //         $npwp_pemotong = $row['npwp_pemotong'];
    //         $npwp_no   = $row['npwp_no'];
    //         $masaAkhir = $row['masa_akhir'];
    //         $nik    = $row['nik'];
    //         $full_name = $row['full_name'];
    //         $address = $row['address'];
    //         $marital_status = $row['status_ptkp'];
    //         $position  = $row['position'];
    //         $bsProrateTotal = $row['total_1'];
    //         $tunjanganPPh = $row['total_2'];
    //         $otBonus   = $row['total_3'];
    //         $honorarium = $row['total_4'];
    //         $premiAsuransi = $row['total_5'];
    //         $natura = $row['total_6'];
    //         $thrBonus = $row['total_7'];
    //         $jmlBruto = $bsProrateTotal + $tunjanganPPh + $otBonus + $honorarium + $natura + $thrBonus;
    //         $tunjanganJabatan = $row['total_9'];
    //         $iuranPensiun = $row['total_10'];
    //         $jmlPengurangan = $tunjanganJabatan + $iuranPensiun;
    //         $jmlNeto = $jmlBruto - $jmlPengurangan;
    //         $netoSebelumnya = $row['total_13'];
    //         $netoSetahun = $jmlNeto;
    //         $ptkp = $row['total_15'];
    //         $pkpTotal = $row['total_16'];
    //         $pkpSetahun = $row['total_17'];
    //         $pphSebelumnya = $row['total_18'];
    //         $pphTerutang = $row['total_19'];
    //         $pphFinal = $row['total_20'];
    //         $npwp_pemotong = $row['npwp_pemotong'];
    //         $nama_pemotong = $row['nama_pemotong'];
    //         $tax_code = $row['tax_code'];
    //         $wpLuarNegri = $row['wp_luar_negri'];
    //         $nomorSpt = $row['no_bukti_potong'];
    //         $jmlTanggungan = $row['jumlah_tanggungan'];
    //         $masa_akhir = $row['masa_akhir'];

    //     }   
    //     /* Start Logo Kemenkeu */
    //     $pdf->Image('logo_kemenkeu.png',20,11,25,25);
    //     /* End Logo Kemenkeu */

    //     /* Start Tittle Center */ 
    //     $pdf->SetFont('Arial','B',10);
    //     $pdf->Line(60, 12, 60, 50); //garis horizontal 
    //     $pdf->Cell(190,10,'BUKTI PEMOTONGAN PAJAK PENGHASILAN', '', '1', 'C');
    //     $pdf->Cell(192,1,'PASAL 21 BAGI PEGAWAI TETAP ATAU', '', '', 'C');
    //     $pdf->Cell(-2,1,'FORMULIR 1721 - A1', '', '1', 'R');
    //     $pdf->Cell(189,10,'PENERIMAAN PENSIUN ATAU TUNJANGAN HARI', '', '', 'C');
    //     /* End Tittle Center */ 

    //     /* Start Tittle Kanan */ 
    //     $pdf->SetFont('Arial','',6); //pengaturan huruf//
    //     $pdf->Line(165,40, 165, 50); // garis horizontal
    //     $pdf->Cell(1,10,'Lembar ke-1 : untuk Penerima Penghasilan', '', '1','R');
    //     /* End Tittle Kanan */ 

    //     /* Start Tittle Bawah */
    //     $pdf->SetFont('Arial','B',10);
    //     $pdf->Cell(189,1,'TUA/JAMINAN HARI TUA BERKALA', '', '', 'C');
    //     $pdf->Line(165, 40, 60, 40); // garis vertical 
    //     /* Start Tittle Bawah */

    //     /* Start Tittle Kanan */ 
    //     $pdf->SetFont('Arial','',6); //pengaturan huruf//
    //     $pdf->Line(150, 12, 150, 40); //garis horizontal 
    //     $pdf->Cell(-10.5,0,'Lembar ke-2 : untuk Pemotong', '', '1','R');
    //     /* End Tittle Kanan */ 

    //     /* Start Masa Perolehan, Penghasilan */ 
    //     $pdf->SetFont('Arial','B',6);
    //     $pdf->Cell(180,10,'MASA PEROLEHAN', '', '1', 'R');
    
    //     $pdf->SetFont('Arial','B',6);
    //     $pdf->Cell(183,0,'PENGHASILAN [mm-mm]', '', '1', 'R');
    //     /* End Masa Perolehan, Penghasilan  */ 

    //     /* Start Tittle KEMENTERIAN KEUANGAN RI */ 
    //     $pdf->SetFont('Arial','B',8);
    //     $pdf->Cell(45,0,'KEMENTERIAN KEUANGAN RI ', '', '1', 'C');

    //     $pdf->SetFont('Arial','B',8);
    //     $pdf->Cell(45,8,'DIREKTORAT JENDERAL PAJAK ', '', '', 'C');
    //     /* End Tittle KEMENTERIAN KEUANGAN RI */ 

    //     /* Start Tittle Nomor Bukti Potong */ 
    //     $pdf->Cell(10,4,'',0,1);
    //     $pdf->Cell(125,1,'NOMOR : ', '', '','C');
    //     $pdf->Line(201, 50, 6, 50); // garis vertical 

    //     $pdf->SetFont('Arial','',4.5);
    //     $pdf->Cell(10,1,'',0,1);
    //     $pdf->Cell(143,1,'H.01', '', '','C');

    //     $pdf->SetFont('Arial','',8);
    //     $pdf->Cell(-100,-1,$nomorSpt,'', '','C');
    //     /* End Tittle Nomor Bukti Potong */

    //     $pdf->SetFont('Arial','',4.5);
    //     $pdf->Cell(10,0,'',0,1);
    //     $pdf->Cell(318,1,'H.02', '', '','C');

    //     /* Start Masa Perolehan Awal & Akhir */
    //     $pdf->SetFont('Arial','',7);
    //     $pdf->Cell(10,0,'',0,1);
    //     $pdf->Cell(330,1,$masa_awal,'', '','C');
    //     $pdf->Cell(10,0,'',0,1);
    //     $pdf->Cell(340,1,'-', '', '','C');
    //     $pdf->Cell(10,0,'',0,1);
    //     $pdf->Cell(350,1,$masa_akhir, '', '1','C');
    //     /* End Masa Perolehan Awal & Akhir */

    //     /* Start NPWP & Nama Pemotong */
    //     $pdf->SetFont('Arial','B',7);
    //     $pdf->Line(6, 70, 6, 50); 
    //     $pdf->Cell(1,10,'NPWP','','1','L');
    //     $pdf->SetFont('Arial','B',7);
    //     $pdf->Cell(10,1,'PEMOTONG :','','','L');
    //     $pdf->SetFont('Arial','',4.5);
    //     $pdf->Cell(19,1,'H.03','','','C');

    //     if($clientName = 'Pontil_Sumbawa'){
    //         $namaPtlSmb = 'PT. SANGATI SOERYA SEJAHTERA ';
    //         $npwpPtlSmb = '024322661913000';
    //     }

    //     $pdf->SetFont('Arial','',8);
    //     $pdf->Cell(15,1,$npwpPtlSmb,'','1','C');

    //     $pdf->SetFont('Arial','B',7);
    //     $pdf->Cell(1,7,'NAMA','','1','L');

    //     $pdf->SetFont('Arial','B',7);
    //     $pdf->Line(201, 70, 6, 70);
    //     $pdf->Cell(10,1,'PEMOTONG :','','','L');

    //     $pdf->SetFont('Arial','',4.5);
    //     $pdf->Cell(19,2,'H.04','','','C');

    //     $pdf->SetFont('Arial','',8);
    //     $pdf->Line(201, 70, 201, 50); 
    //     $pdf->Cell(38,1,$namaPtlSmb,'','1','C');
    //     /* Start NPWP & Nama Pemotong */

    //     $pdf->SetFont('Arial','B',7);
    //     $pdf->Cell(60,15,'A. IDENTITAS PENERIMA PENGHASILAN YANG DIPOTONG','','1','C');
    //     $pdf->Line(201,78,6, 78); 

    //     $pdf->SetFont('Arial','B',7);
    //     $pdf->Cell(8,1,'1. NPWP :','','','L');
    //     $pdf->SetFont('Arial','',4.5);
    //     $pdf->Cell(14,2,'A.01 ','','','C');

    //     $string = $npwp_no;
    //     $pdf->SetFont('Arial','',8);
    //     $pdf->Line(201,110, 201, 78); 
    //     $pdf->Cell(27,1,preg_replace('/\D/', '', $string),'','','L'); //npwp hanya angka

    //     $pdf->SetFont('Arial','B',7);
    //     $pdf->Cell(195,1,'6. STATUS / JUMLAH TANGGUNGAN KELUARGA UNTUK PTKP ','','1','C');
       
    //     $pdf->SetFont('Arial','B',7);
    //     $pdf->Cell(8,9,'2. NIK/NO.','','1','L');


    //     $pdf->SetFont('Arial','B',7);
    //     $pdf->Cell(14,1,'PASPOR :','','','L');

    //     $pdf->SetFont('Arial','',4.5);
    //     $pdf->Cell(3,2,'A.02 ','','','C');

    //     $pdf->SetFont('Arial','',8);
    //     $pdf->Cell(33,1,$nik,'','','C');

    //     /* Start jumlah tanggungan keluarga PTKP */
    //     if($marital_status=='K'){
    //         $tanggungan_k          = $jmlTanggungan;
    //         $tanggungan_tk         = '';
    //         $tanggungan_hb         = '';
    //     }else if($marital_status=='TK'){
    //         $tanggungan_tk         = $jmlTanggungan;
    //         $tanggungan_k          = '';
    //         $tanggungan_hb         = '';
    //     }else if($marital_status=='HB'){
    //         $tanggungan_hb         = $jmlTanggungan;
    //         $tanggungan_k          ='';
    //         $tanggungan_tk         ='';
    //     }
    //     /* End jumlah tanggungan keluarga PTKP */

    //     $pdf->SetFont('Arial','B',7);
    //     $pdf->Cell(67,1,'K / ','','','R');
    //     $pdf->Cell(2,1,$tanggungan_k ,'','','R');

    //     $pdf->SetFont('Arial','B',7);
    //     $pdf->Cell(12,1,'TK / ','','','R');
    //     $pdf->Cell(2,1,$tanggungan_tk ,'','','R');

    //     $pdf->SetFont('Arial','B',7);
    //     $pdf->Cell(15,1,'HB / ','','','R');
    //     $pdf->Cell(2,1,$tanggungan_hb ,'','1','R');

    //     $pdf->SetFont('Arial','',4.5);
    //     $pdf->Cell(138,9,'A.10 ','','','R'); //POSITION
    //     $pdf->SetFont('Arial','',8);
    //     $pdf->Cell(35,8,$position,'','','C'); //POSITION

    //     $pdf->SetFont('Arial','B',7);
    //     $pdf->Cell(25,1,'','','1','C');

    //     $pdf->SetFont('Arial','B',7);
    //     $pdf->Cell(8.5,8,'3. NAMA : ','','','L');

    //     $pdf->SetFont('Arial','',4.5);
    //     $pdf->Cell(12,9,'A.03','','','C');

    //     $pdf->SetFont('Arial','',8);
    //     $pdf->Cell(30,8,$full_name,'','','L'); 

    //     $pdf->SetFont('Arial','B',7);
    //     $pdf->Cell(83,6,'7. NAMA JABATAN :','','1','R');
    //     // $pdf->Cell(190,5,$position,'','1','C'); 
       

    //     $pdf->SetFont('Arial','B',7);
    //     $pdf->Cell(11,5,'4. ALAMAT :','','','L');
    //     $pdf->SetFont('Arial','',4.5);
    //     $pdf->Cell(13,6,'A.04','','','C');
    //     $pdf->SetFont('Arial','B',6);
    //     // if($address = 'NONE'){
    //     //     $address = '-';
    //     // }
    //     $pdf->Cell(8,5,'','','','C'); // address

    //     $pdf->SetFont('Arial','B',7);
    //     $pdf->Cell(104.5,4,'8. KARYAWAN ASING :','','1','R'); 
    //     $pdf->Cell(183,-4,'','','1','C'); 
    //     $pdf->Cell(140,5,'','','','');
    //     $pdf->Cell(6,5,'','1','','R');
    //     $pdf->Cell(7,6,$wpLuarNegri,'','','R'); 

    //     $pdf->Cell(183,1,'','','1','C'); 
    //     $pdf->Cell(298,2,'','','3','C'); 

    //     $pdf->SetFont('Arial','',4.5);
    //     $pdf->Cell(275,0,'A.11','','1','C'); 

    //     $pdf->SetFont('Arial','B',7);
    //     $pdf->Cell(19,10,'5. JENIS KELAMIN :','','','L');
    //     $pdf->SetFont('Arial','',4.5);
    //     $pdf->Cell(15,11,'A.05','','','C');

    //     $gender = $row['gender'];
    //     // // test($gender,1);
    //     if($gender = 'L'){
    //        $gender = 'X';
    //     }

    //     $pdf->SetFont('Arial','',13);
    //     $pdf->Cell(6,5,$gender,'1','','C');
    //     $pdf->SetFont('Arial','B',7);
    //     $pdf->Cell(16,10,'LAKI-LAKI','','','C');

    //     $pdf->SetFont('Arial','',4.5);
    //     $pdf->Cell(8,11,'A.06','','','C');
    //     $pdf->Cell(6,5,'','1','','C');

    //     $pdf->SetFont('Arial','B',7);
    //     $pdf->Cell(19,10,'PEREMPUAN','','','C');

    //     $pdf->SetFont('Arial','B',7);
    //     $pdf->Cell(54.5,10,'9. KODE NEGARA DOMISILI :','','','R'); 
    //     $pdf->Line(201,110,6,110); 

    //     $pdf->SetFont('Arial','',4.5);
    //     $pdf->Cell(4,11,'A.12','','','C');
    //     // $pdf->Line(201, 70, 201, 50);   
    //     $pdf->Line(6,110,6, 78); 

    //     $pdf->SetFont('Arial','B',7);
    //     $pdf->Cell(-230,23,'B. RINCIAN PENGHASILAN DAN PENGHITUNGAN PPh PASAL 21','','1','C'); 
    //     $pdf->Line(201,116,6,117); 

    //     $pdf->Cell(125,-10,'URAIAN ','','1','C'); 
    //     $pdf->SetFont('Arial','B',7);
    //     $pdf->Cell(20,19,'KODE OBJEK PAJAK :','','','C'); 
     
    //     $pdf->Cell(20,19,$tax_code,'','','C'); 
    //     $pdf->Line(201,127,6,127);


    //     $pdf->Line(201,122,6,122); 
    //     $pdf->Cell(140,10,'JUMLAH (Rp) ','','1','R');
    //     $pdf->Line(6,117,6,219);  //garis kiri uraian
    //     $pdf->Line(15,219,15,183); // garis kecil bawah penghitungan pph pasal 21
    //     $pdf->Line(15,131.5,15,163); // garis kecil bawah penghasilan bruto
    //     $pdf->Line(15,167,15,179); // garis kecil bawah pengurangan
    //     $pdf->Line(201,219,201,116); // garis paling kanan jumlah (Rp)

    //     $pdf->Line(160,219,160,116.5); // garis tengah jumlah (Rp)
    //     $pdf->SetFont('Arial','B',7);
    //     $pdf->Cell(22,9,'PENGHASILAN BRUTO :','','','C');
     
    //     $pdf->Line(6,131.5,201,131.5);

    //     $pdf->SetFont('Arial','',7);
    //     $pdf->Cell(-46,17,'1.','','','C');
    //     $pdf->Cell(95,17,'GAJI/PENSIUN ATAU THT/JHT','','1','C');

    //     // $pdf->Cell(16,2,number_format($hasilPajak,2,".",""),'', '1', 'R'); //nilai

    //     $pdf->SetFont('Arial','',7);
    //     $pdf->Cell(191,-17,number_format($bsProrateTotal,0,",","."),'','','R'); 
    //     $pdf->Line(6,135,201,135);

    //     $pdf->SetFont('Arial','',7);
    //     $pdf->Cell(-384,-9,'2.','','','C');
    //     $pdf->Cell(418,-9,'TUNJANGAN PPh','','1','C'); 

    //     $pdf->SetFont('Arial','',7);
    //     $pdf->Cell(191,9,number_format($tunjanganPPh,0,",","."),'','','R'); 
    //     $pdf->Line(201,139,6,139); 

    //     $pdf->SetFont('Arial','',7);
    //     $pdf->Cell(-384,17,'3.','','','C');
    //     $pdf->Cell(466,17,'TUNJANGAN LAINNYA, UANG LEMBUR DAN SEBAGAINYA','','1','C'); 

    //     $pdf->SetFont('Arial','',7);
    //     $pdf->Cell(191,-17,number_format($otBonus,0,",","."),'','','R');
    //     $pdf->Line(201,143,6,143); 

    //     $pdf->SetFont('Arial','',7);
    //     $pdf->Cell(-384,-10,'4.','','','C');
    //     $pdf->Cell(455,-10,'HONORARIUM DAN IMBALAN LAIN SEJENISNYA','','1','C'); 

    //     $pdf->SetFont('Arial','',7);
    //     $pdf->Cell(191,10,number_format($honorarium,0,",","."),'','','R');
    //     $pdf->Line(201,147,6,147); 

    //     $pdf->SetFont('Arial','',7);
    //     $pdf->Cell(-384,18,'5.','','','C');
    //     $pdf->Cell(459,18,'PREMI ASURANSI YANG DIBAYAR PEMBERI KERJA','','1','C'); 

    //     $pdf->SetFont('Arial','',7);
    //     $pdf->Cell(191,-18,number_format($premiAsuransi,0,",","."),'','','R');
    //     $pdf->Line(6,151,201,151);

    //     $pdf->SetFont('Arial','',7);
    //     $pdf->Cell(-384,-10,'6.','','','C');
    //     $pdf->Cell(537,-10,'PENERIMAAN DALAM BENTUK NATURA DAN KENIKMATAN LAINNYA YANG DIKENAKAN PEMOTONGAN PPh PASAL 21','','1','C');

    //     $pdf->SetFont('Arial','',7);
    //     $pdf->Cell(191,10,number_format($natura,0,",","."),'','','R');
    //     $pdf->Line(201,155,6,155);
        
    //     $pdf->SetFont('Arial','',7);
    //     $pdf->Cell(-384,18,'7.','','','C');
    //     $pdf->Cell(468,18,'TANTIEM, BONUS, GRATIFIKASI, JASA PRODUKSI DAN THR','','1','C');

    //     $pdf->SetFont('Arial','',7);
    //     $pdf->Cell(191,-18,number_format($thrBonus,0,",","."),'','','R'); 
    //     $pdf->Line(6,159,201,159);

    //     $pdf->SetFont('Arial','',7);
    //     $pdf->Cell(-384,-10,'8.','','','C');
    //     $pdf->Cell(447,-10,'JUMLAH PENGHASILAN BRUTO ( 1 S.D.7 )','','1','C'); 

    //     $pdf->SetFont('Arial','',7);
    //     $pdf->Cell(191,10,number_format($jmlBruto,0,",","."),'','','R');
    //     $pdf->Line(201,163,6,163);

    //     // $pdf->ln(2);
    //     $pdf->SetFont('Arial','B',7);
    //     // $pdf->Cell(-368,18,'PENGURANGAN :','','','C');
    //     $pdf->Cell(-368,18,'PENGURANGAN :','','1','C');
    //     $pdf->Line(6,167,201,167);
    //     // $pdf->Line(6,167,201,167);

    //     $pdf->SetFont('Arial','',7);
    //     $pdf->Cell(-2,-10,'9.','','','C');
    //     $pdf->Cell(55,-10,'BIAYA JABATAN/BIAYA PENSIUN','','1','C');

    //     $pdf->SetFont('Arial','',7);
    //     $pdf->Cell(191,10,number_format($tunjanganJabatan,0,",","."),'','','R');
    //     $pdf->Line(6,171,201,171);

    //     $pdf->SetFont('Arial','',7);
    //     $pdf->Cell(-384,18,'10.','','','C');
    //     $pdf->Cell(444,18,'IURAN PENSIUN ATAU IURAN THT/JHT','','1','C'); 

    //     $pdf->SetFont('Arial','',7);
    //     $pdf->Cell(191,-18,number_format($iuranPensiun,0,",","."),'','','R');
    //     $pdf->Line(201,175,6,175);

    //     $pdf->SetFont('Arial','',7);
    //     $pdf->Cell(-384,-10,'11.','','','C');
    //     $pdf->Cell(440,-10,'JUMLAH PENGURANGAN (9 S.D. 10)','','1','C'); 

    //     $pdf->SetFont('Arial','',7);
    //     $pdf->Cell(191,10,number_format($jmlPengurangan,0,",","."),'','','R');
    //     $pdf->Line(201,179,6,179);

    //     $pdf->SetFont('Arial','B',7);
    //     $pdf->Cell(-350,18,'PENGHITUNGAN PPh PASAL 21 :','','1','C');

    //     $pdf->SetFont('Arial','',7);
    //     $pdf->Cell(191,-10,number_format($jmlNeto,0,",","."),'','','R');
    //     $pdf->Line(6,183,201,183);

    //     $pdf->SetFont('Arial','',7);
    //     $pdf->Cell(-384,-10,'12.','','','C');
    //     $pdf->Cell(443,-10,'JUMLAH PENGHASILAN NETO ( 8- 11 )','','1','C'); 

    //     $pdf->SetFont('Arial','',7);
    //     $pdf->Cell(191,18,number_format($netoSebelumnya,0,",","."),'','','R');
    //     $pdf->Line(201,187,6,187);
        
    //     $pdf->SetFont('Arial','',7);
    //     $pdf->Cell(-384,18,'13.','','','C');
    //     $pdf->Cell(448,18,'PENGHASILAN NETO MASA SEBELUMNYA','','1','C'); 

    //     $pdf->SetFont('Arial','',7);
    //     $pdf->Cell(191,-10,number_format($netoSetahun,0,",","."),'','','R');
    //     $pdf->Line(201,191,6,191);

    //     $pdf->SetFont('Arial','',7);
    //     $pdf->Cell(-384,-10,'14.','','','C');
    //     $pdf->Cell(514,-10,'JUMLAH PENGHASILAN NETO UNTUK PENGHITUNGAN PPh PASAL 21 (SETAHUN/DISETAHUNKAN)','','1','C'); 

    //     $pdf->SetFont('Arial','',7);
    //     $pdf->Cell(191,18,number_format($ptkp,0,",","."),'','','R');
    //     $pdf->Line(201,195,6,195);

    //     $pdf->SetFont('Arial','',7);
    //     $pdf->Cell(-384,18,'15.','','','C');
    //     $pdf->Cell(449,18,'PENGHASILAN TIDAK KENA PAJAK (PTKP) ','','1','C'); 

    //     $pdf->SetFont('Arial','',7);
    //     $pdf->Cell(191,-10,number_format($pkpTotal,0,",","."),'','','R');
    //     $pdf->Line(201,199,6,199);

    //     $pdf->SetFont('Arial','',7);
    //     $pdf->Cell(-384,-10,'16.','','','C');
    //     $pdf->Cell(473,-10,'PENGHASILAN KENA PAJAK SETAHUN/DISETAHUNKAN (14-15) ','','1','C'); 

    //     $pdf->SetFont('Arial','',7);
    //     $pdf->Cell(191,18,number_format($pkpSetahun,0,",","."),'','','R');
    //     $pdf->Line(201,203,6,203);

    //     $pdf->SetFont('Arial','',7);
    //     $pdf->Cell(-384,18,'17.','','','C');
    //     $pdf->Cell(489,18,'PPh PASAL 21 ATAS PENGHASILAN KENA PAJAK SETAHUN/DISETAHUNKAN ','','1','C'); 

    //     $pdf->SetFont('Arial','',7);
    //     $pdf->Cell(191,-10,number_format($pphSebelumnya,0,",","."),'','','R');
    //     $pdf->Line(201,207,6,207);

    //     $pdf->SetFont('Arial','',7);
    //     $pdf->Cell(-384,-10,'18.','','','C');
    //     $pdf->Cell(470,-10,'PPh PASAL 21 YANG TELAH DIPOTONG MASA SEBELUMNYA','','1','C'); 

    //     $pdf->SetFont('Arial','',7);
    //     $pdf->Cell(191,18,number_format($pphTerutang,0,",","."),'','','R');
    //     $pdf->Line(201,211,6,211);

    //     $pdf->SetFont('Arial','',7);
    //     $pdf->Cell(-384,18,'19.','','','C');
    //     $pdf->Cell(429,18,'PPh PASAL 21 TERUTANG','','1','C'); 

    //     $pdf->SetFont('Arial','',7);
    //     $pdf->Cell(191,-10,number_format($pphTerutang,0,",","."),'','','R');
    //     $pdf->Line(201,215,6,215);

    //     $pdf->SetFont('Arial','',7);
    //     $pdf->Cell(-384,-10,'20.','','','C');
    //     $pdf->Cell(486,-10,'PPh PASAL 21 DAN PPh PASAL 26 YANG TELAH DIPOTONG DAN DILUNASI','','1','C'); 
    //     $pdf->Line(201,219,6,219);

    //     $pdf->SetFont('Arial','B',7);
    //     $pdf->Cell(20,26,'C. IDENTITAS PEMOTONG','','1','C'); 

    //     $pdf->Line(201,250,6,250); // garis paling bawah identitas pemotong
    //     $pdf->Line(201,230,6,230); // garis atas identitas pemotong
    //     $pdf->Line(6,230,6,250); // garis kecil kiri identitas pemotong
    //     $pdf->Line(201,230,201,250); // garis kecil kanan identitas pemotong
    //     $pdf->SetFont('Arial','B',7);
    //     $pdf->Cell(8,1,'1. NPWP :','','','C');
    //     // $pdf->Line(201,116,6,117); 

    //     $pdf->SetFont('Arial','',4.5);
    //     $pdf->Cell(10,2,'C.01', '', '','C');

    //     $pdf->SetFont('Arial','',8); 
    //     $pdf->Cell(27,1,$npwp_pemotong,'','1','C');

    //     $pdf->SetFont('Arial','B',7);
    //     $pdf->Cell(257,-1,'3. TANGGAL & TANDA TANGAN','','1','C');

    //     $pdf->SetFont('Arial','B',7);
    //     $pdf->Cell(8,15,'2. NAMA :','','','C');
    //     // $pdf->Line(201,116,6,117); 

    //     $pdf->SetFont('Arial','',4.5);
    //     $pdf->Cell(10,16,'C.02', '', '','C');

    //     $pdf->SetFont('Arial','',8); 
    //     $pdf->Cell(27,15,$nama_pemotong,'','1','C');

    //     $pdf->SetFont('Arial','',4.5);
    //     $pdf->Cell(225,-14,'C.03', '', '','C');

    //     $pdf->SetFont('Arial','',9);
    //     $pdf->Cell(-190,-14,date("d - m - Y"), '', '','C');

    //     // kotak tanda tangan //
    //     $pdf->Line(200,232,163,232); // garis atas ttd
    //     $pdf->Line(163,232,163,249); // garis kiri 
    //     $pdf->Line(200,232,200,249); // garis kanan 
    //     $pdf->Line(163,249,200,249); // garis bawah

    //     $pdf->Output();
    // }
    //END EXPORT PDF//

    function printPdfPontilSmb_new($clientName, $fullName){
        $fullName      = $this->security->xss_clean($fullName);
        $pdf     = new FPDF();
        $pdf->AddPage();

        $queryTest = " mst_spt_tax 
                       WHERE client_name = '".$clientName."' 
                       AND full_name = '".$fullName."' ";

        $mPdf = $this->db->get($queryTest)->row();
        // test($mPdf,1);

        // $pdf->Line(200, 1, 10, 1); //garis horizontal 
        
        $pdf->Line(62, 3.6, 62, 34); //garis horizontal (atas kiri)
        $pdf->Line(148, 3.1, 148, 25.5); //garis horizontal (atas kanan)
        $pdf->Line(163.5, 25.5, 163.5, 34); //garis horizontal (atas kanan)
        $pdf->Image('logo_kemenkeu.png',25,3,18,18);
        
        $pdf->SetFont('Arial','',7);
        $pdf->SetXY(9,0.4);
        $pdf->Cell(0,0,'....................................................................................................................................................................................................................................................................................',0,0,'L');

        $pdf->ln(0.8);
        $pdf->SetXY(10,1);
        $pdf->Cell(4.8,2.1,'',1,1,'',true);
        $pdf->SetFillColor(10,10,10);

        $pdf->SetXY(194,1);
        $pdf->Cell(4.8,2,'',1,1,'',true);
        $pdf->SetFillColor(10,10,10);

        $pdf->Ln(2);
        $pdf->SetFont('Arial','B',10.4);
        $pdf->Cell(190,1,'BUKTI PEMOTONGAN PAJAK PENGHASILAN', '', '1', 'C');

        $pdf->Ln(3);
        $pdf->SetFont('Arial','B',10.4);
        $pdf->Cell(190,1,'PASAL 21 BAGI PEGAWAI TETAP ATAU', '', '1', 'C');

        $pdf->Ln(3);
        $pdf->SetFont('Arial','B',10.4);
        $pdf->Cell(190,1,'PENERIMA PENSIUN ATAU TUNJANGAN HARI', '', '1', 'C');

        $pdf->Ln(3);
        $pdf->SetFont('Arial','B',10.4);
        $pdf->Cell(190,1,'TUA/JAMINAN HARI TUA BERKALA', '', '1', 'C');

        $pdf->ln(0.8);
        $pdf->SetXY(185.5,4.5);
        $pdf->Cell(2.5,2.5,'',1,0,'',false);

        $pdf->SetXY(189,4.5);
        $pdf->Cell(2.5,2.5,'',1,0,'',true);

        $pdf->SetXY(192.5,4.5);
        $pdf->Cell(2.5,2.5,'',1,0,'',false);

        $pdf->SetXY(196,4.5);
        $pdf->Cell(2.5,2.5,'',1,0,'',true);

        $pdf->Ln(6.5);
        $pdf->SetFont('Arial','B',11.5);
        $pdf->Cell(190,1,'FORMULIR 1721 - A1', '', '1', 'R');

        $pdf->Ln(3.5);
        $pdf->SetFont('Arial','',6.7);
        $pdf->Cell(190,1,'Lembar ke-1 : untuk Penerima Penghasilan', '', '1', 'R');

        $pdf->Ln(2);
        $pdf->SetFont('Arial','',6.7);
        $pdf->Cell(177,1,'Lembar ke-2 : untuk Pemotong', '', '1', 'R');

        $pdf->ln(6.5);
        $pdf->SetFont('Arial','B',9.2);
        $pdf->Cell(50,1,'KEMENTRIAN KEUANGAN RI', '', '1', 'C');

        $pdf->ln(2.5);
        $pdf->SetFont('Arial','B',9.2);
        $pdf->Cell(50,1,'DIREKTORAT JENDERAL PAJAK', '', '1', 'C');

        $pdf->SetXY(0,23.4);
        $pdf->SetFont('Arial','B',6.5);
        $pdf->Cell(193,1,'MASA PEROLEHAN', '', '1', 'R');

        $pdf->ln(1.7);
        $pdf->SetFont('Arial','B',6.5);
        $pdf->Cell(175.5,1,'PENGHASILAN', '', '1', 'R');

        $pdf->SetXY(5.5,26.5);
        $pdf->SetFont('Arial','B',5.5);
        $pdf->Cell(190.5,0,'[mm - mm]', '', '1', 'R');

        // $pdf->SetXY(7,28);
        $pdf->SetFont('Arial','B',8.3);
        $pdf->Cell(123,8.5,'NOMOR : ', '', '1', 'C');

        $pdf->SetXY(20,31.8);
        $pdf->SetFont('Arial','B',5);
        $pdf->Cell(123,0,'H.01 ', '', '1', 'C');

        $pdf->SetFont('Arial','',10);
        $pdf->Cell(91,-2.5,'1  .   1  - ', '', '1', 'R');

        $pdf->SetFont('Arial','',9.5);
        $pdf->Cell(95.5,3.3,$mPdf->masa_pajak, '', '1', 'R');
        $pdf->Cell(103.5,-3.5,' . ', '', '1', 'R');
        $pdf->Cell(108.5,3.9,substr($mPdf->tahun_pajak,2,2), '', '1', 'R');
        $pdf->Cell(115.5,-5,' - ', '', '1', 'R');
        $pdf->Cell(130.5,6,$mPdf->generate_no_spt, '', '1', 'R');


        $pdf->SetXY(134,31.8);
        $pdf->SetFont('Arial','B',5);
        $pdf->Cell(0,0,'H.02 ', '', '1', 'C');

        $pdf->SetFont('Arial','',9);
        $pdf->Cell(170,-0.5,'01 ', '', '1', 'R');
        $pdf->Cell(173.5,-1,'-', '', '1', 'R');
        $pdf->Cell(181.5,2.5,'08', '', '1', 'R');

        // Garis dibawah Nomor SPT
        $pdf->Line(110, 32.8, 100, 32.8); 
        $pdf->Line(122, 32.8, 112.5, 32.8); 
        $pdf->Line(162, 32.8, 124.5, 32.8); 
        $pdf->Line(180, 33.2, 171.2, 33.2); 
        $pdf->Line(183.5, 33.2, 192.5, 33.2);

        $pdf->Line(199, 34, 10.5, 34); 
        $pdf->Line(199, 50, 10.5, 50); 
        $pdf->Line(10.5, 34, 10.5, 50);
        $pdf->Line(199, 34, 199, 50);

        $pdf->SetXY(11,36.93);
        $pdf->SetFont('Arial','B',7.5);
        $pdf->Cell(0,0,'NPWP', '', '1', 'L');
        $pdf->SetXY(11,40);
        $pdf->Cell(0,0,'PEMOTONG', '', '1', 'L');
        $pdf->SetXY(27,40);
        $pdf->Cell(0,0,':', '', '1', 'L');
        $pdf->SetXY(28.2,40.5);
        $pdf->SetFont('Arial','B',5);
        $pdf->Cell(0,0,'H.03', '', '1', 'L');

        if($clientName = 'Pontil_Sumbawa'){
           $namaPtlSmb = 'PT SANGATI SOERYA SEJAHTERA ';
           $npwpPtlSmb = '024322661913000';
        }

        $pdf->SetXY(35,39.8);
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(0,0,substr($npwpPtlSmb,0,9), '', '1', 'L');

        $pdf->SetXY(85.3,39.4);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(0,0,substr('-',0,9), '', '1', 'L');

        $pdf->SetXY(88.8,39.8);
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(0,0,substr($npwpPtlSmb,9,3), '', '1', 'L');

        $pdf->SetXY(102.3,40);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(0,0,substr('.',0,9), '', '1', 'L');

        $pdf->SetXY(105.6,39.8);
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(0,0,substr($npwpPtlSmb,12,3), '', '1', 'L');

        $pdf->Line(34.5, 41.5, 85.5, 41.5);
        $pdf->Line(88.5, 41.5, 102.5, 41.5);
        $pdf->Line(105.3, 41.5, 119.5, 41.5);

        $pdf->SetXY(11,43.6);
        $pdf->SetFont('Arial','B',7.5);
        $pdf->Cell(0,0,'NAMA', '', '1', 'L');
        $pdf->SetXY(11,46.6);
        $pdf->Cell(0,0,'PEMOTONG', '', '1', 'L');
        $pdf->SetXY(27,46.6);
        $pdf->Cell(0,0,':', '', '1', 'L');
        $pdf->SetXY(28.2,46.8);
        $pdf->SetFont('Arial','B',5);
        $pdf->Cell(0,0,'H.04', '', '1', 'L');

        $pdf->SetXY(35,46.3);
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(0,0,$namaPtlSmb, '', '1', 'L');
        $pdf->Line(34.5, 48.3, 196.5, 48.3);

        $pdf->SetXY(9.5,56.8);
        $pdf->SetFont('Arial','B',7.5);
        $pdf->Cell(0,0,"A. IDENTITAS PENERIMA PENGHASILAN YANG DIPOTONG", '', '1', 'L');

        $pdf->Line(199, 59.6, 10.5, 59.6);       
        $pdf->Line(10.5, 59.8, 10.5, 100);      
        $pdf->Line(199, 100, 10.5, 100);       
        $pdf->Line(199, 59.8, 199, 100);      

        $pdf->SetXY(11.8,63);
        $pdf->SetFont('Arial','B',6.5);
        $pdf->Cell(0,0,"1. NPWP    :", '', '1', 'L');
        $pdf->SetXY(24.7,64.8);
        $pdf->SetFont('Arial','',5);
        $pdf->Cell(0,0,'A.01', '', '1', 'L');

        $pdf->SetXY(30.7,63.9);
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(0,0,substr($mPdf->npwp_no,0,12), '', '1', 'L');

        $pdf->SetXY(35.2,63.9);
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(0,0,"", '', '1', 'L');

        $pdf->SetXY(41.7,63.9);
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(0,0,"", '', '1', 'L');
        $pdf->Line(30.5, 65.8, 76.8, 65.8); 

        $pdf->SetXY(76.7,63.7);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(0,0,'-', '', '1', 'L');

        $pdf->SetXY(80.7,63.9);
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(0,0,substr($mPdf->npwp_no,13,3), '', '1', 'L');
        $pdf->Line(80, 65.8, 93.8, 65.8); 

        $pdf->SetXY(97.5,63.9);
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(0,0,substr($mPdf->npwp_no,17,3), '', '1', 'L');
        $pdf->Line(96.5, 65.8, 110.8, 65.8); 

        $pdf->SetXY(115.4,64.2);
        $pdf->SetFont('Arial','B',6.3);
        $pdf->Cell(0,0,'6. STATUS / JUMLAH TANGGUNGAN KELUARGA UNTUK PTKP', '', '1', 'L');
        $pdf->Line(96.5, 65.8, 110.8, 65.8);         

        $pdf->SetXY(11.8,69);
        $pdf->SetFont('Arial','B',6.5);
        $pdf->Cell(0,0,"2. NIK /NO.", '', '1', 'L');

        $pdf->SetXY(14.2,71.8);
        $pdf->SetFont('Arial','B',6.5);
        $pdf->Cell(0,0,'PASPOR:', '', '1', 'L');

        $pdf->SetXY(24.7,72.3);
        $pdf->SetFont('Arial','',5);
        $pdf->Cell(0,0,'A.02', '', '1', 'L');

        $pdf->SetXY(30.7,70.7);
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(0,0,$mPdf->nik, '', '1', 'L');
        $pdf->Line(30.5, 72.8, 110.8, 72.8); 

        if($mPdf->status_ptkp=='K'){
            $jml_k      = $mPdf->jumlah_tanggungan;
            $jml_tk     = '';
        }elseif($mPdf->status_ptkp=='TK'){
            $jml_k      = '';
            $jml_tk     = '0';
        }

        $pdf->SetXY(118.5,70.1);
        $pdf->SetFont('Arial','B',6.3);
        $pdf->Cell(0,0,'K /      '.$jml_k, '', '1', 'L');

        $pdf->SetXY(138.5,70.1);
        $pdf->Cell(0,0,'TK /      '.$jml_tk, '', '1', 'L');

        $pdf->SetXY(160.5,70.1);
        $pdf->Cell(0,0,'HB /', '', '1', 'L');

        $pdf->Line(123.5, 72.8, 129.8, 72.8); 
        $pdf->Line(144, 72.8, 150.8, 72.8); 
        $pdf->Line(167, 72.8, 173.8, 72.8); 

        $pdf->SetFont('Arial','',5);
        $pdf->SetXY(130,72.3);
        $pdf->Cell(0,0,'A.07', '', '1', 'L');
        $pdf->SetXY(151.5,72.3);
        $pdf->Cell(0,0,'A.08', '', '1', 'L');
        $pdf->SetXY(173.5,72.3);
        $pdf->Cell(0,0,'A.09', '', '1', 'L');

        $pdf->SetXY(11.8,77.5);
        $pdf->SetFont('Arial','B',6.5);
        $pdf->Cell(0,0,"3. NAMA    :", '', '1', 'L');

        $pdf->SetXY(24.7,78.5);
        $pdf->SetFont('Arial','',5);
        $pdf->Cell(0,0,'A.03', '', '1', 'L');

        $pdf->SetXY(30.7,77.2);
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(0,0,$mPdf->full_name, '', '1', 'L');
        $pdf->Line(30.2, 79.5, 110.8, 79.5); 

        $pdf->SetXY(115.5,78);
        $pdf->SetFont('Arial','B',6.5);
        $pdf->Cell(0,0,"7. NAMA JABATAN :", '', '1', 'L');

        $pdf->SetXY(145.5,77.5);
        $pdf->SetFont('Arial','',7.5);
        $pdf->Cell(0,0,$mPdf->position, '', '1', 'L');
        $pdf->Line(145.3, 79.2, 197, 79.2); 

        $pdf->SetXY(140,77.5);
        $pdf->SetFont('Arial','',5);
        $pdf->Cell(0,0,'A.10', '', '1', 'L');

        $pdf->SetXY(11.8,84);
        $pdf->SetFont('Arial','B',6.5);
        $pdf->Cell(0,0,"4. ALAMAT:", '', '1', 'L');

        $pdf->SetXY(24.7,85.5);
        $pdf->SetFont('Arial','',5);
        $pdf->Cell(0,0,'A.04', '', '1', 'L');

        $pdf->SetXY(30.7,83.2);
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(0,0,substr($mPdf->address,0,40), '', '1', 'L');
        $pdf->Line(30.2, 85.5, 110.8, 85.5); 

        $pdf->SetXY(115.5,84);
        $pdf->SetFont('Arial','B',6.5);
        $pdf->Cell(0,0,"8. KARYAWAN ASING :", '', '1', 'L');

        $pdf->SetXY(149,83.3);
        $pdf->SetFont('Arial','',5);
        $pdf->Cell(0,0,'A.11', '', '1', 'L');

        $pdf->SetXY(160.5,84);
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(0,0,'YA', '', '1', 'L');

        $pdf->Line(154.2, 81, 160.8, 81); 
        $pdf->Line(154.2, 85.5, 160.8, 85.5); 
        $pdf->Line(154.2, 85.5, 154.2, 81); 
        $pdf->Line(160.8, 85.5, 160.8, 81); 

        $pdf->Line(30.2, 92.5, 110.8, 92.5); 

        $pdf->SetXY(115.5,90.8);
        $pdf->SetFont('Arial','B',6.5);
        $pdf->Cell(0,0,"9. KODE NEGARA DOMISILI :", '', '1', 'L');

        $pdf->SetXY(149,90.2);
        $pdf->SetFont('Arial','',5);
        $pdf->Cell(0,0,'A.12', '', '1', 'L');

        $pdf->Line(155.3, 92.5, 167.8, 92.5); 

        $pdf->SetXY(11.8,97);
        $pdf->SetFont('Arial','B',6.5);
        $pdf->Cell(0,0,"5. JENIS KELAMIN :", '', '1', 'L');

        $pdf->SetXY(35,97);
        $pdf->SetFont('Arial','',5);
        $pdf->Cell(0,0,'A.05', '', '1', 'L');

        $pdf->SetXY(48,97);
        $pdf->SetFont('Arial','B',6.5);
        $pdf->Cell(0,0,"LAKI-LAKI", '', '1', 'L');

        $pdf->SetXY(62,97);
        $pdf->SetFont('Arial','',5);
        $pdf->Cell(0,0,'A.06', '', '1', 'L');

        $pdf->SetXY(74,97);
        $pdf->SetFont('Arial','B',6.5);
        $pdf->Cell(0,0,"PEREMPUAN", '', '1', 'L');

        $pdf->Line(40.5, 99, 47, 99); 
        $pdf->Line(40.5, 94.5, 47, 94.5); 
        $pdf->Line(40.5, 94.5, 40.5, 99); 
        $pdf->Line(47, 94.5, 47, 99); 

        $pdf->Line(40.5+27, 99, 47+27, 99); 
        $pdf->Line(40.5+27, 94.5, 47+27, 94.5); 
        $pdf->Line(40.5+27, 94.5, 40.5+27, 99); 
        $pdf->Line(47+27, 94.5, 47+27, 99); 

        if($mPdf->gender=='M'){
            $pdf->SetXY(42,97);
            $pdf->SetFont('Arial','',10);
            $pdf->Cell(0,0,"X", '', '1', 'L');
        }else if($mPdf->gender=='F'){
            $pdf->SetXY(69,97);
            $pdf->SetFont('Arial','',10);
            $pdf->Cell(0,0,"X", '', '1', 'L');
        }

        $pdf->SetXY(9.5,105);
        $pdf->SetFont('Arial','B',7.5);
        $pdf->Cell(0,0,"B. RINCIAN PENGHASILAN DAN PENGHITUNGAN PPh PASAL 21", '', '1', 'L');

        $pdf->Line(10.5, 99+8.5, 199, 99+8.5); 
        // $pdf->Line(40.5, 94.5, 47, 94.5); 
        // $pdf->Line(40.5, 94.5, 40.5, 99); 
        $pdf->Line(10.5, 99+8.5, 10.5, 240.5); 
        $pdf->Line(199, 99+8.5, 199, 240.5); 
        $pdf->Line(154, 99+8.5, 154, 240.5); 

        $pdf->Line(10.5, 99+13.8, 199, 99+13.8); 

        $pdf->SetXY(76,110);
        $pdf->SetFont('Arial','B',6.5);
        $pdf->Cell(0,0,"URAIAN", '', '1', 'L');
        $pdf->SetXY(167,110);
        $pdf->Cell(0,0,"JUMLAH (Rp)", '', '1', 'L');

        $pdf->Line(10.5, 118.1, 199, 118.1); 

        $jbaris     = 0;
        $tbaris     = 0;

        // test($mPdf,1);

        // lOOPING UNTUK GARIS DI BAWAH
        for ($i=1; $i <= 8; $i++) { 
            $baris          = 118.1;
            $jbaris         = $jbaris+5.3;
            $tbaris         = $baris+$jbaris;
            // test($jbaris,0);
            $pdf->Line(10.5, $tbaris, 199, $tbaris);             
        }

        $tbaris_pengurangan         = $tbaris;

        $pdf->SetXY(11,115.5);
        $pdf->SetFont('Arial','B',6.5);
        $pdf->Cell(0,0,"KODE OBJEK PAJAK:", '', '1', 'L');

        $pdf->Line(40.5, 117.5, 47, 117.5); 
        $pdf->Line(40.5, 113.5, 47, 113.5); 
        $pdf->Line(40.5, 113.5, 40.5, 117.5); 
        $pdf->Line(47, 113.5, 47, 117.5); 

        $pdf->SetXY(47,115.5);
        $pdf->SetFont('Arial','B',6.5);
        $pdf->Cell(0,0,"21-100-01", '', '1', 'L');

        $pdf->Line(65.5, 117.5, 72, 117.5); 
        $pdf->Line(65.5, 113.5, 72, 113.5); 
        $pdf->Line(65.5, 113.5, 65.5, 117.5); 
        $pdf->Line(72, 113.5, 72, 117.5); 

        if($mPdf->tax_code=='21-100-01'){
            $pdf->SetXY(42,115.7);
            $pdf->SetFont('Arial','',10);
            $pdf->Cell(0,0,"X", '', '1', 'L');
        }else{
            $pdf->SetXY(67,115.7);
            $pdf->SetFont('Arial','',10);
            $pdf->Cell(0,0,"X", '', '1', 'L');
        }

        $pdf->SetXY(72,115.5);
        $pdf->SetFont('Arial','B',6.5);
        $pdf->Cell(0,0,"21-100-02", '', '1', 'L');

        $pdf->SetXY(11,121);
        $pdf->SetFont('Arial','B',6.5);
        $pdf->Cell(0,0,"PENGHASILAN BRUTO:", '', '1', 'L');

        $pdf->Line(17, 123.5, 17, 165.5); 

        $pdf->SetXY(12,121+5.5);
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(0,0,"1.", '', '1', 'L');
        $pdf->SetXY(17,121+5.5);
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(0,0,"GAJI/PENSIUN ATAU THT/JHT", '', '1', 'L');
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(0,0,number_format($mPdf->total_1,0,',','.'), '', '1', 'R'); 

        $pdf->SetXY(12,126+5.5);
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(0,0,"2.", '', '1', 'L');
        $pdf->SetXY(17,126.5+5.5);
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(0,0,"TUNJANGAN PPh", '', '1', 'L');
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(0,0,number_format($mPdf->total_2,0,',','.'), '', '1', 'R'); 

        $pdf->SetXY(12,131.5+5.5);
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(0,0,"3.", '', '1', 'L');
        $pdf->SetXY(17,131.5+5.5);
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(0,0,"TUNJANGAN LAINNYA, UANG LEMBUR DAN SEBAGAINYA", '', '1', 'L');
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(0,0,number_format($mPdf->total_3,0,',','.'), '', '1', 'R'); 

        $pdf->SetXY(12,137+5.5);
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(0,0,"4.", '', '1', 'L');
        $pdf->SetXY(17,137+5.5);
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(0,0,"HONORARIUM DAN IMBALAN SEJENISNYA", '', '1', 'L');
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(0,0,number_format($mPdf->total_4,0,',','.'), '', '1', 'R'); 

        $pdf->SetXY(12,142+5.5);
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(0,0,"5.", '', '1', 'L');
        $pdf->SetXY(17,142+5.5);
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(0,0,"HONORARIUM DAN IMBALAN SEJENISNYA", '', '1', 'L');
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(0,0,number_format($mPdf->total_5,0,',','.'), '', '1', 'R'); 

        $pdf->SetXY(12,147.2+5.5);
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(0,0,"6.", '', '1', 'L');
        $pdf->SetXY(17,147.2+5.5);
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(0,0,"PENERIMAAN DALAM BENTUK NATURA DAN KENIKMATAN LAINNYA YANG DIKENAKAN PEMOTONGAN PPh PASAL 21", '', '1', 'L');
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(0,0,number_format($mPdf->total_6,0,',','.'), '', '1', 'R'); 

        $pdf->SetXY(12,152.5+5.5);
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(0,0,"7.", '', '1', 'L');
        $pdf->SetXY(17,152.5+5.5);
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(0,0,"TANTIEM, BONUS, GRATIFIKASI, JASA PRODUKSI DAN THR", '', '1', 'L');
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(0,0,number_format($mPdf->total_7,0,',','.'), '', '1', 'R'); 

        $pdf->SetXY(12,158+5.5);
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(0,0,"8.", '', '1', 'L');
        $pdf->SetXY(17,158+5.5);
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(0,0,"JUMLAH PENGHASILAN BRUTO (1 S.D 7)", '', '1', 'L');
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(0,0,number_format($mPdf->total_8,0,',','.'), '', '1', 'R'); 

        $tbaris_pengurangan         = $tbaris_pengurangan+5.3;    
        $pdf->Line(10.5, $tbaris_pengurangan, 199, $tbaris_pengurangan);

        $tbaris_pengurangan         = $tbaris_pengurangan+5.3;    
        $pdf->Line(10.5, $tbaris_pengurangan, 199, $tbaris_pengurangan);

        $isi_baris_pengurangan      = $tbaris_pengurangan-2.4;

        $pdf->SetXY(11,$isi_baris_pengurangan);
        $pdf->SetFont('Arial','B',6.5);
        $pdf->Cell(0,0,"PENGURANGAN:", '', '1', 'L');
        $pdf->SetXY(17,$isi_baris_pengurangan);

        $jbaris     = 0;
        $tbaris     = 0;

        for ($i=1; $i <= 3; $i++) { 
            $baris          = $tbaris_pengurangan;
            $jbaris         = $jbaris+5.3;
            $tbaris         = $baris+$jbaris;
            // test($jbaris,0);
            $pdf->Line(10.5, $tbaris, 199, $tbaris);             
        }

        $pdf->Line(17, 171.2, 17, 187); 

        $pdf->SetXY(12,174);
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(0,0,"9.", '', '1', 'L');
        $pdf->SetXY(17,174);
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(0,0,"BIAYA JABATAN PENSIUN", '', '1', 'L');
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(0,0,number_format($mPdf->total_9,0,',','.'), '', '1', 'R');

        $pdf->SetXY(11,174+5.3);
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(0,0,"10.", '', '1', 'L');
        $pdf->SetXY(17,174+5.3);
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(0,0,"IURAN PENSIUN ATAU IURAN THT/JHT", '', '1', 'L');
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(0,0,number_format($mPdf->total_10,0,',','.'), '', '1', 'R');

        $pdf->SetXY(11,179.3+5.3);
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(0,0,"11.", '', '1', 'L');
        $pdf->SetXY(17,179.3+5.3);
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(0,0,"JUMLAH PENGURANGAN (9 S.D. 10)", '', '1', 'L');
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(0,0,number_format($mPdf->total_11,0,',','.'), '', '1', 'R');

        $pdf->SetXY(11,184.6+5.3);
        $pdf->SetFont('Arial','B',6.5);
        $pdf->Cell(0,0,"PENGHITUNGAN PPH PASAL 21", '', '1', 'L');

        $baris_perhitungan_pph      = 187.5;

        $jbaris     = 0;
        $tbaris     = 0;

        for ($i=1; $i <= 10; $i++) { 
            $baris          = $baris_perhitungan_pph;
            $jbaris         = $jbaris+5.3;
            $tbaris         = $baris+$jbaris;
            // test($jbaris,0);
            $pdf->Line(10.5, $tbaris, 199, $tbaris);
        }

        $pdf->Line(17, 193, 17, 240.5); 

        $pdf->SetXY(11,190.5+5.3);
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(0,0,"12.", '', '1', 'L');
        $pdf->SetXY(17,190.5+5.3);
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(0,0,"JUMLAH PENGHASILAN NETO ( 8- 11 )", '', '1', 'L');
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(0,0,number_format($mPdf->total_12,0,',','.'), '', '1', 'R');

        $pdf->SetXY(11,195.8+5.3);
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(0,0,"13.", '', '1', 'L');
        $pdf->SetXY(17,195.8+5.3);
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(0,0,"PENGHASILAN NETO MASA SEBELUMNYA", '', '1', 'L');
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(0,0,number_format($mPdf->total_13,0,',','.'), '', '1', 'R');

        $pdf->SetXY(11,201.1+5.3);
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(0,0,"14.", '', '1', 'L');
        $pdf->SetXY(17,201.1+5.3);
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(0,0,"JUMLAH PENGHASILAN NETO UNTUK PENGHITUNGAN PPh PASAL 21 (SETAHUN/DISETAHUNKAN)", '', '1', 'L');
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(0,0,number_format($mPdf->total_14,0,',','.'), '', '1', 'R');

        $pdf->SetXY(11,206.4+5.3);
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(0,0,"15.", '', '1', 'L');
        $pdf->SetXY(17,206.4+5.3);
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(0,0,"PENGHASILAN TIDAK KENA PAJAK (PTKP)", '', '1', 'L');
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(0,0,number_format($mPdf->total_15,0,',','.'), '', '1', 'R');

        $pdf->SetXY(11,211.7+5.3);
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(0,0,"16.", '', '1', 'L');
        $pdf->SetXY(17,211.7+5.3);
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(0,0,"PENGHASILAN KENA PAJAK SETAHUN/DISETAHUNKAN (14-15)", '', '1', 'L');
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(0,0,number_format($mPdf->total_16,0,',','.'), '', '1', 'R');

        $pdf->SetXY(11,217.0+5.3);
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(0,0,"17.", '', '1', 'L');
        $pdf->SetXY(17,217.0+5.3);
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(0,0,"PPh PASAL 21 ATAS PENGHASILAN KENA PAJAK SETAHUN/DISETAHUNKAN", '', '1', 'L');
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(0,0,number_format($mPdf->total_17,0,',','.'), '', '1', 'R');

        $pdf->SetXY(11,222.3+5.3);
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(0,0,"18.", '', '1', 'L');
        $pdf->SetXY(17,222.3+5.3);
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(0,0,"PPh PASAL 21 YANG TELAH DIPOTONG MASA SEBELUMNYA'", '', '1', 'L');
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(0,0,number_format($mPdf->total_18,0,',','.'), '', '1', 'R');

        $pdf->SetXY(11,227.6+5.3);
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(0,0,"19.", '', '1', 'L');
        $pdf->SetXY(17,227.6+5.3);
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(0,0,"PPh PASAL 21 TERUTANG", '', '1', 'L');
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(0,0,number_format($mPdf->total_19,0,',','.'), '', '1', 'R');

        $pdf->SetXY(11,232.9+5.3);
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(0,0,"20.", '', '1', 'L');
        $pdf->SetXY(17,232.9+5.3);
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(0,0,"PPh PASAL 21 DAN PPh PASAL 26 YANG TELAH DIPOTONG DAN DILUNASI", '', '1', 'L');
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(0,0,number_format($mPdf->total_20,0,',','.'), '', '1', 'R');

        $pdf->SetXY(9.5,245);
        $pdf->SetFont('Arial','B',7.5);
        $pdf->Cell(0,0,"C. IDENTITAS PEMOTONG", '', '1', 'L');

        $pdf->Line(10.5, 248, 199, 248);
        $pdf->Line(10.5, 265, 199, 265);
        $pdf->Line(10.5, 248, 10.5, 265);
        $pdf->Line(199, 248, 199, 265);

        $pdf->SetXY(154,112.8);
        $pdf->SetFillColor(188,189,193);
        $pdf->Cell(45,5.3,'',1,1,'',true);

        $pdf->SetXY(154,118.1);
        $pdf->Cell(45,5.3,'',1,1,'',true);

        $pdf->SetXY(154,165.8);
        $pdf->Cell(45,5.3,'',1,1,'',true);

        $pdf->SetXY(154,187);
        $pdf->Cell(45,5.8,'',1,1,'',true);

        $pdf->SetXY(13,252);
        $pdf->SetFont('Arial','B',6.5);
        $pdf->Cell(0,0,"1. NPWP :", '', '1', 'L');

        $pdf->SetXY(13,252);
        $pdf->SetFont('Arial','B',5);
        $pdf->Cell(32,0,'C.01 ', '', '1', 'C');

        $pdf->SetXY(13,252);
        $pdf->SetFont('Arial','',10);
        $pdf->Cell(60,0,substr($mPdf->npwp_pemotong,0,2).'.'.substr($mPdf->npwp_pemotong,2,3).'.'.substr($mPdf->npwp_pemotong,5,3).'.'.substr($mPdf->npwp_pemotong,8,1), '', '1', 'C');
        $pdf->Cell(130,0,'-', '', '1', 'C');
        $pdf->Cell(145,0,substr($mPdf->npwp_pemotong,9,3), '', '1', 'C');
        $pdf->Cell(158,0,'.', '', '1', 'C');
        $pdf->Cell(168,0,substr($mPdf->npwp_pemotong,12,3), '', '1', 'C');   
        $pdf->SetFont('Arial','B',6.5);     
        $pdf->Cell(230,0,'TANGGAL & TANDA TANGAN', '', '1', 'C');        

        $pdf->SetXY(13,259);
        $pdf->SetFont('Arial','B',6.5);
        $pdf->Cell(0,0,"2. NAMA :", '', '1', 'L');

        $pdf->SetXY(13,259);
        $pdf->SetFont('Arial','B',5);
        $pdf->Cell(32,0,'C.02 ', '', '1', 'C');
        $pdf->SetFont('Arial','',10);
        $pdf->Cell(72,0,$mPdf->nama_pemotong, '', '1', 'C');

        // test($mPdf,1);


        // $pdf->Cell(36,3,'','','','C'); 
        // $pdf->Cell(93,5,'','','','C'); 
        // $pdf->Cell(28,3,' ','','','C'); 
        // $pdf->Cell(49,5,' ','','','C'); 
        // $pdf->Cell(62.5,3,' ','','','C'); 
        // $pdf->Cell(54,5,','','','C'); 
        // $pdf->Cell(8.5,3,'','','','C'); 
        // $pdf->Cell(68,5,'','','','C'); 
        
        // $pdf->Line(17, 123.5, 17, 166); 

        // $pdf->Line(10.5, 99+19.1+5.3, 199, 99+19.1+5.3); 
        // $pdf->Line(10.5, 99+19.1+5.3+5.3, 199, 99+19.1+5.3+5.3); 
        // $pdf->Line(10.5, 99+19.1+5.3+5.3+5.3, 199, 99+19.1+5.3+5.3+5.3); 
        // $pdf->Line(10.5, 99+19.1+5.3+5.3+5.3+5.3, 199, 99+19.1+5.3+5.3+5.3+5.3); 
        // $pdf->Line(10.5, 99+19.1+5.3+5.3+5.3+5.3+5.3, 199, 99+19.1+5.3+5.3+5.3+5.3+5.3); 
        // $pdf->Line(10.5, 99+19.1+5.3+5.3+5.3+5.3+5.3+5.3, 199, 99+19.1+5.3+5.3+5.3+5.3+5.3+5.3); 

        // $pdf->SetXY(145.5,77.5+6);
        // $pdf->SetFont('Arial','',7.5);
        // $pdf->Cell(0,0,$mPdf->position, '', '1', 'L');
        // $pdf->Line(145.3, 79.2, 197, 79.2); 




        $pdf->Line(163.5, 25.5, 62, 25.5); //garis horizontal 

        $pdf->Output();
    }

    function printPdfPontilSmb($clientName, $fullName){
        $fullName      = $this->security->xss_clean($fullName);
        $pdf     = new FPDF();
        $pdf->AddPage();

        $queryTest = " mst_spt_tax 
                       WHERE client_name = '".$clientName."' 
                       AND full_name = '".$fullName."' ";

        $mPdf = $this->db->get($queryTest)->result_array();
        foreach ($mPdf as $row){
            $masa_awal = $row['masa_awal'];
            $masa_pajak = $row['masa_pajak'];
            $npwp_pemotong = $row['npwp_pemotong'];
            $npwp_no   = $row['npwp_no'];
            $masaAkhir = $row['masa_akhir'];
            $nik    = $row['nik'];
            $full_name = $row['full_name'];
            $address = $row['address'];
            $marital_status = $row['status_ptkp'];
            $position  = $row['position'];
            $bsProrateTotal = $row['total_1'];
            $tunjanganPPh = $row['total_2'];
            $otBonus   = $row['total_3'];
            $honorarium = $row['total_4'];
            $premiAsuransi = $row['total_5'];
            $natura = $row['total_6'];
            $thrBonus = $row['total_7'];
            $jmlBruto = $bsProrateTotal + $tunjanganPPh + $otBonus + $honorarium + $natura + $thrBonus;
            $tunjanganJabatan = $row['total_9'];
            $iuranPensiun = $row['total_10'];
            $jmlPengurangan = $tunjanganJabatan + $iuranPensiun;
            $jmlNeto = $jmlBruto - $jmlPengurangan;
            $netoSebelumnya = $row['total_13'];
            $netoSetahun = $jmlNeto;
            $ptkp = $row['total_15'];
            $pkpTotal = $row['total_16'];
            $pkpSetahun = $row['total_17'];
            $pphSebelumnya = $row['total_18'];
            $pphTerutang = $row['total_19'];
            $pphFinal = $row['total_20'];
            $npwp_pemotong = $row['npwp_pemotong'];
            $nama_pemotong = $row['nama_pemotong'];
            $tax_code = $row['tax_code'];
            $wpLuarNegri = $row['wp_luar_negri'];
            $countryCode = $row['country_code'];
            $nomorSpt = $row['no_bukti_potong'];
            $jmlTanggungan = $row['jumlah_tanggungan'];
            $masa_akhir = $row['masa_akhir'];

        }

        // START LOGO KEMENKEU//
        //set image('filename',atur kanan kiri, atas bawah, lebar, tinggi)
        $pdf->Image('logo_kemenkeu.png',20,11,25,25);


        //set font 'arial', 'bold', 'ukuran huruf'//
        $pdf->SetFont('Arial','B',9);
        $pdf->Line(60, 12, 60, 50); //garis horizontal 
        $pdf->Cell(180,10,'BUKTI PEMOTONGAN PAJAK PENGHASILAN', '', '1', 'C');
        $pdf->Ln(0.00);
        $pdf->Cell(180,1,'PASAL 21 BAGI PEGAWAI TETAP ATAU', '', '', 'C');
        $pdf->Cell(13,1,'FORMULIR 1721 - A1', '', '1', 'R');
        $pdf->Cell(185.5,10,'PENERIMAAN PENSIUN ATAU TUNJANGAN HARI', '', '', 'C');


        $pdf->SetFont('Arial','',6); //pengaturan huruf//
        $pdf->Line(165,40, 165, 50); // garis horizontal
        
        $pdf->Cell(7,10,'Lembar ke-1 : untuk Penerima Penghasilan', '', '1','R');
        // $pdf->Ln(5);

        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(180,1,'TUA/JAMINAN HARI TUA BERKALA', '', '', 'C');
        $pdf->Line(165, 40, 60, 40); // garis vertical 
       

        $pdf->SetFont('Arial','',6); //pengaturan huruf//
        $pdf->Line(150, 12, 150, 40); //garis horizontal 
        $pdf->Cell(1,0,'Lembar ke-2 : untuk Pemotong', '', '1','R');

        $pdf->SetFont('Arial','B',6);
        $pdf->Cell(188,10,'MASA PEROLEHAN', '', '1', 'R');

        $pdf->SetFont('Arial','B',6);
        $pdf->Cell(190,0,'PENGHASILAN [mm-mm]', '', '1', 'R');

        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(45,0,'KEMENTERIAN KEUANGAN RI ', '', '1', 'C');
        
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(45,8,'DIREKTORAT JENDERAL PAJAK ', '', '', 'C');
        
        // 

        $pdf->Cell(10,4,'',0,1);
        $pdf->Cell(125,1,'NOMOR : ', '', '','C');
        $pdf->Line(201, 50, 6, 50); // garis vertical
        // $pdf->Ln(8);//break line//

        $pdf->SetFont('Arial','',4.5);
        $pdf->Cell(10,1,'',0,1);
        $pdf->Cell(141,1,'H.01', '', '','C');

        $pdf->SetFont('Arial','',8);
        $pdf->Cell(-34,-1,$nomorSpt, '', '','R');

        $pdf->SetFont('Arial','',4.5);
        $pdf->Cell(10,0,'',0,1);
        $pdf->Cell(325,1,'H.02', '', '','C');

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(10,0,'',0,1);
        $pdf->Cell(340,1,$masa_awal, '', '','C');
        $pdf->Cell(10,0,'',0,1);
        $pdf->Cell(345,1,'-', '', '','C');
        $pdf->Cell(10,0,'',0,1);
        $pdf->Cell(350,1,$masa_akhir, '', '1','C');

        $pdf->SetFont('Arial','B',7);
        $pdf->Line(6, 70, 6, 50); 
        $pdf->Ln(4);
        $pdf->Cell(1,5,'NPWP','','1','L');
        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(17,3,'PEMOTONG :','','','L');
        $pdf->SetFont('Arial','',4.5);
        $pdf->Cell(6,4,'H.03','','','');

        if($clientName = 'Pontil_Sumbawa'){
           $namaPtlSmb = 'PT. SANGATI SOERYA SEJAHTERA ';
           $npwpPtlSmb = '024322661913000';
        }

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(2,3,$npwpPtlSmb,'','1','');

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(1,6,'NAMA','','1','');

        $pdf->SetFont('Arial','B',7);
        $pdf->Line(201, 70, 6, 70);
        $pdf->Cell(17,3,'PEMOTONG :','','','');

        $pdf->SetFont('Arial','',4.5);
        $pdf->Cell(6,4,'H.04','','','');

        $pdf->SetFont('Arial','B',7);
        $pdf->Line(201, 70, 201, 50); 
        $pdf->Cell(2,3,$namaPtlSmb,'','1','');

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(60,13,'A. IDENTITAS PENERIMA PENGHASILAN YANG DIPOTONG','','1','C');
        $pdf->Line(201,78,6, 78); 

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(13,1,'1. NPWP :','','','');
        $pdf->SetFont('Arial','',4.5);
        $pdf->Cell(6,2,'A.01 ','','','');

        $string = $npwp_no;
        $pdf->SetFont('Arial','B',7);
        $pdf->Line(201,110, 201, 78); 
        $pdf->Cell(88,1,$string,'','',''); //npwp hanya angka
        $pdf->Cell(15,1,'6. STATUS / JUMLAH TANGGUNGAN KELUARGA UNTUK PTKP ','','1','');
       
        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(8,9,'2. NIK/NO.','','1','');

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(14,1,'PASPOR :','','','C');

        $pdf->SetFont('Arial','',4.5);
        $pdf->Cell(5,2,'A.02 ','','','');

        /* Start jumlah tanggungan keluarga PTKP */
        if($marital_status=='K'){
            $tanggungan_k          = $jmlTanggungan;
            $tanggungan_tk         = '';
            $tanggungan_hb         = '';
        }else if($marital_status=='TK'){
            $tanggungan_tk         = $jmlTanggungan;
            $tanggungan_k          = '';
            $tanggungan_hb         = '';
        }else if($marital_status=='HB'){
            $tanggungan_hb         = $jmlTanggungan;
            $tanggungan_k          ='';
            $tanggungan_tk         ='';
        }
        /* End jumlah tanggungan keluarga PTKP */

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(32,1,$nik,'','','');
        $pdf->Cell(65,1,'K/','','','R');
        $pdf->Cell(2,1,$tanggungan_k,'','','R');
        $pdf->Cell(9,1,'TK/','','','R');
        $pdf->Cell(2,1,$tanggungan_tk,'','','R');
        $pdf->Cell(10,1,'HB/','','','R');
        $pdf->Cell(2,1,$tanggungan_hb,'','1','R');

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(14,8,'3. NAMA : ','','','');

        $pdf->SetFont('Arial','',4.5);
        $pdf->Cell(5,9,'A.03 ','','',''); //POSITION
        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(48,8,$full_name,'','',''); //POSITION

        // $pdf->SetFont('Arial','B',7);
        // $pdf->Cell(25,1,'','','1','C');

        // $pdf->SetFont('Arial','B',7);
        // $pdf->Cell(8.5,8,'3. NAMA : ','','','L');

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(66,7,'7. NAMA JABATAN :','','','R');
        // $pdf->Cell(190,5,$position,'','1','C'); 
        $pdf->SetFont('Arial','',4.5);
        $pdf->Cell(5,8,'A.10','','','');

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(15,7,$position,'','1','');
       

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(17,3,'4. ALAMAT :','','','');
        $pdf->SetFont('Arial','',4.5);
        $pdf->Cell(12,4,'A.04','','','');
        $pdf->SetFont('Arial','B',7);
        // if($address = 'NONE'){
        //     $address = '-';
        // }
        $pdf->Cell(55,3,$address,'','','C'); // address

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(52,4,'8. KARYAWAN ASING :','','','R'); 
        $pdf->SetFont('Arial','',4.5);
        $pdf->Cell(5,5,'A.11','','',''); 
        // $pdf->Cell(183,-4,'','','1','C'); 
        // $pdf->Cell(140,5,'','','','');
        // $pdf->Cell(6,5,'','1','','R');
        if($wp_luar_negri = 'indonesia'){
            $wp_luar_negri = 'NO';
        }else{
            $wp_luar_negri = 'YA';
        }
        $pdf->Cell(4,4,'','1','','');
        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(6,4,$wp_luar_negri,'','1','R'); //$wpLuarNegri//     

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(26,4,'5. JENIS KELAMIN :','','','');
        $pdf->SetFont('Arial','',4.5);
        $pdf->Cell(6,5,'A.05','','','');

        $gender = $row['gender'];
        // test($gender,1);
        if($gender == 'M'){
            $genderL = 'X';
            $genderP = '';
        }
        else{
            $genderL = '';
            $genderP = 'X';
        }

        $pdf->SetFont('Arial','',9);
        $pdf->Cell(4,4,$genderL,'1','','');
        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(14,5,'LAKI-LAKI','','','');

        $pdf->SetFont('Arial','',4.5);
        $pdf->Cell(6,5,'A.06','','','');
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(4,4,$genderP,'1','','');//gender perempuan

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(19,5,'PEREMPUAN','','','');

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(64,5,'9. KODE NEGARA DOMISILI :','','','R'); 
        $pdf->Line(201,110,6,110); 

        $pdf->SetFont('Arial','',4.5);
        $pdf->Cell(4,6,'A.12','','','');
        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(6,6,$countryCode,'','1','');
        // $pdf->Line(201, 70, 201, 50);   
        $pdf->Line(6,110,6, 78); 

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(67,10,'B. RINCIAN PENGHASILAN DAN PENGHITUNGAN PPh PASAL 21','','1','C'); 
        $pdf->Line(201,116,6,117); 

        $pdf->Cell(140,1,'URAIAN ','','','C'); 
        $pdf->Cell(65,1,'JUMLAH (Rp) ','','1','C');
        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(20,10,'KODE OBJEK PAJAK :','','','C');
        $pdf->Cell(110,2,'','','1','');//SKIP//
        $pdf->Cell(25,2,'','','','');//SKIP// 
        $pdf->Cell(8,5,'','1','',''); 
        $pdf->Cell(12,5,$tax_code,'','','C');

        $pdf->SetFillColor(210,200,200); 
        $pdf->Cell(110,0,'','','1','R');//SKIP//
        $pdf->Cell(150,0,'','','','R');//SKIP//
        $pdf->Cell(41,5,'','1','1','R', true); 
        $pdf->Line(201,127,6,127);


        $pdf->Line(201,122,6,122); 
        // $pdf->Cell(140,10,'JUMLAH (Rp) ','','1','R');
        $pdf->Line(6,117,6,222);  //garis kiri uraian
        $pdf->Line(15,222,15,185.5); // garis kecil bawah penghitungan pph pasal 21
        $pdf->Line(15,132,15,164); // garis kecil bawah penghasilan bruto
        $pdf->Line(15,168.5,15,181); // garis kecil bawah pengurangan
        $pdf->Line(201,222,201,116); // garis paling kanan jumlah (Rp)

        $pdf->Line(160,222,160,116.5); // garis tengah jumlah (Rp)
        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(22,5,'PENGHASILAN BRUTO :','','','C');
        
        $pdf->SetFillColor(210,200,200); 
        $pdf->Cell(110,0,'','','1','R');//SKIP//
        $pdf->Cell(150,0,'','','','R');//SKIP//
        $pdf->Cell(41,4.5,'','1','1','R', true); 
        // $pdf->Line(201,127,6,127);

        $pdf->Line(6,131.5,201,131.5);

        $pdf->SetFont('Arial','',6);
        $pdf->Cell(20,5,'1.','','','');
        $pdf->Cell(3,5,'GAJI/PENSIUN ATAU THT/JHT','','','C');

        // $pdf->Cell(16,2,number_format($hasilPajak,2,".",""),'', '1', 'R'); //nilai

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(168,5,number_format($bsProrateTotal,0,",","."),'','1','R'); 
        $pdf->Line(6,136,201,136);

        $pdf->SetFont('Arial','',6);
        $pdf->Cell(16,3,'2.','','','');
        $pdf->Cell(-1,3,'TUNJANGAN PPh','','','C'); 

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(176,3,number_format($tunjanganPPh,0,",","."),'','1','R'); 
        $pdf->Line(6,140,201,140); 

        $pdf->SetFont('Arial','',6);
        $pdf->Cell(27,5,'3.','','','');
        $pdf->Cell(18,5,'TUNJANGAN LAINNYA, UANG LEMBUR DAN SEBAGAINYA','','','C'); 

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(146,5,number_format($otBonus,0,",","."),'','1','R');
        $pdf->Line(6,144,201,144);

        $pdf->SetFont('Arial','',6);
        $pdf->Cell(15,3,'4.','','','');
        $pdf->Cell(32,3,'HONORARIUM DAN IMBALAN LAIN SEJENISNYA','','','C'); 

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(144,3,number_format($honorarium,0,",","."),'','1','R');
        $pdf->Line(6,148,201,148); 

        $pdf->SetFont('Arial','',6);
        $pdf->Cell(15,5,'5.','','','');
        $pdf->Cell(36,5,'PREMI ASURANSI YANG DIBAYAR PEMBERI KERJA','','','C'); 

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(140,5,number_format($premiAsuransi,0,",",".") ,'','1','R');
        $pdf->Line(6,152,201,152); 

        $pdf->SetFont('Arial','',6);
        $pdf->Cell(42,3,'6.','','','');
        $pdf->Cell(49,3,'PENERIMAAN DALAM BENTUK NATURA DAN KENIKMATAN LAINNYA YANG DIKENAKAN PEMOTONGAN PPh PASAL 21','','','C');

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(100,3,number_format($natura,0,",","."),'','1','R');
        $pdf->Line(6,156,201,156); 
        
        $pdf->SetFont('Arial','',6);
        $pdf->Cell(20,5,'7.','','','');
        $pdf->Cell(34,5,'TANTIEM, BONUS, GRATIFIKASI, JASA PRODUKSI DAN THR','','','C');

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(137,5,number_format($thrBonus,0,",","."),'','1','R'); 
        $pdf->Line(6,160,201,160);

        $pdf->SetFont('Arial','',6);
        $pdf->Cell(15,3,'8.','','','');
        $pdf->Cell(25,3,'JUMLAH PENGHASILAN BRUTO ( 1 S.D.7 )','','','C'); 

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(151,3,number_format($jmlBruto,0,",","."),'','1','R');
        $pdf->Line(6,164,201,164);

        $pdf->Ln(2);

// // Set Background warna pada cell
//         $pdf->SetFillColor(210,221,242);
//         $pdf->SetFont( 'Arial', 'B', 10 );
//         // $pdf->Cell( 185, 5,  'hoho' ,0,1,'',true );
//         $pdf->SetFont('Arial','', 10 );
//         yg kotak di atas angka gaji 

        $pdf->SetFont('Arial','B',7);
        // $pdf->Cell(-368,18,'PENGURANGAN :','','','C');
        $pdf->Cell(14,2,'PENGURANGAN :','','','C');
       

        $pdf->SetFillColor(210,200,200); 
        $pdf->Cell(150,-1.5,'','','1','R');//SKIP//
        $pdf->Cell(150,0,'','','','R');//SKIP//
        $pdf->Cell(41,4.5,'','1','1','R', true); 
         $pdf->Line(6,168.5,201,168.5);

        $pdf->SetFont('Arial','',6);
        $pdf->Cell(10,5,'9.','','','');
        $pdf->Cell(26,5,'BIAYA JABATAN/BIAYA PENSIUN','','','C');

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(155,5,number_format($tunjanganJabatan,0,",","."),'','1','R');
        $pdf->Line(6,173,201,173);

        $pdf->SetFont('Arial','',6);
        $pdf->Cell(10,3,'10.','','','');
        $pdf->Cell(32,3,'IURAN PENSIUN ATAU IURAN THT/JHT','','','C'); 

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(149,3,number_format($iuranPensiun,0,",","."),'','1','R');
        $pdf->Line(201,177,6,177);

        $pdf->SetFont('Arial','',6);
        $pdf->Cell(10,5,'11.','','','');
        $pdf->Cell(28,5,'JUMLAH PENGURANGAN (9 S.D. 10)','','','C'); 

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(153,5,number_format($jmlPengurangan,0,",","."),'','1','R');
        $pdf->Line(201,181,6,181);

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(32,4,'PENGHITUNGAN PPh PASAL 21 :','','','C');

        $pdf->SetFillColor(210,200,200); 
        $pdf->Cell(150,-0.5,'','','1','R');//SKIP//
        $pdf->Cell(150,0,'','','','R');//SKIP//
        $pdf->Cell(41,4.5,'','1','1','R', true); 
        $pdf->Line(6,185.5,201,185.5);

        $pdf->SetFont('Arial','B',7);
        // $pdf->Cell(2,2,'test','','1','R');
        
        // $pdf->Line(6,184,201,184);

        $pdf->SetFont('Arial','',6);
        $pdf->Cell(10,5,'12.','','','');
        $pdf->Cell(31,5,'JUMLAH PENGHASILAN NETO ( 8- 11 )','','','C'); 

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(150,5,number_format($jmlNeto,0,",","."),'','1','R');
        $pdf->Line(201,190,6,190);
        
        $pdf->SetFont('Arial','',6);
        $pdf->Cell(10,3,'13.','','','');
        $pdf->Cell(36,3,'PENGHASILAN NETO MASA SEBELUMNYA','','','C'); 

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(145,3,number_format($netoSebelumnya,0,",","."),'','1','R');
        $pdf->Line(201,194,6,194);

        $pdf->SetFont('Arial','',6);
        $pdf->Cell(10,5,'14.','','','');
        $pdf->Cell(93,5,'JUMLAH PENGHASILAN NETO UNTUK PENGHITUNGAN PPh PASAL 21 (SETAHUN/DISETAHUNKAN)','','','C'); 

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(88,5,number_format($netoSetahun,0,",","."),'','1','R');
        $pdf->Line(201,198,6,198);

        $pdf->SetFont('Arial','',6);
        $pdf->Cell(14,3,'15.','','','');
        $pdf->Cell(28,3,'PENGHASILAN TIDAK KENA PAJAK (PTKP) ','','','C'); 

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(149,3,number_format($ptkp,0,",","."),'','1','R');
        $pdf->Line(201,202,6,202);

        $pdf->SetFont('Arial','',6);
        $pdf->Cell(14,5,'16.','','','');
        $pdf->Cell(49,5,'PENGHASILAN KENA PAJAK SETAHUN/DISETAHUNKAN (14-15) ','','','C'); 

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(128,5,number_format($pkpTotal,0,",","."),'','1','R');
        $pdf->Line(201,206,6,206);

        $pdf->SetFont('Arial','',6);
        $pdf->Cell(14,3,'17.','','','');
        $pdf->Cell(62.5,3,'PPh PASAL 21 ATAS PENGHASILAN KENA PAJAK SETAHUN/DISETAHUNKAN ','','','C'); 

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(114.5,3,number_format($pkpSetahun,0,",","."),'','1','R');
        $pdf->Line(201,210,6,210);

        $pdf->SetFont('Arial','',6);
        $pdf->Cell(10,5,'18.','','','');
        $pdf->Cell(54,5,'PPh PASAL 21 YANG TELAH DIPOTONG MASA SEBELUMNYA','','','C'); 

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(127,5,number_format($pphSebelumnya,0,",","."),'','1','R');
        $pdf->Line(201,214,6,214);

        $pdf->SetFont('Arial','',6);
        $pdf->Cell(15,3,'19.','','','');
        $pdf->Cell(8.5,3,'PPh PASAL 21 TERUTANG','','','C'); 

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(167.5,3,number_format($pphTerutang,0,",","."),'','1','R');
        $pdf->Line(201,218,6,218);

        $pdf->SetFont('Arial','',6);
        $pdf->Cell(10,5,'20.','','','');
        $pdf->Cell(68,5,'PPh PASAL 21 DAN PPh PASAL 26 YANG TELAH DIPOTONG DAN DILUNASI','','','C'); 

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(113,5,number_format($pphTerutang,0,",","."),'','1','R');
        $pdf->Line(201,222,6,222);

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(20,12,'C. IDENTITAS PEMOTONG','','1','C'); 

        $pdf->Line(201,252,6,252); // garis paling bawah identitas pemotong
        $pdf->Line(201,232,6,232); // garis atas identitas pemotong
        $pdf->Line(6,232,6,252); // garis kecil kiri identitas pemotong
        $pdf->Line(201,252,201,232); // garis kecil kanan identitas pemotong

        // $pdf->SetFont('Arial','B',7);
        // $pdf->Cell(17,3,'4. ALAMAT :','','','');
        // $pdf->SetFont('Arial','',4.5);
        // $pdf->Cell(12,4,'A.04','','','');
        // $pdf->SetFont('Arial','B',7);

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(17,5,'1. NPWP :','','','L');
        // $pdf->Line(201,116,6,117); 

        $pdf->SetFont('Arial','',4.5);
        $pdf->Cell(-3,7,'C.01', '', '','C');

        $pdf->SetFont('Arial','B',7); 
        $pdf->Cell(30,5,$npwp_pemotong,'','','C');
        $pdf->Cell(102,5,'3. TANGGAL & TANDA TANGAN','','1','R');

        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(8,10,'2. NAMA :','','','L');
        // $pdf->Line(201,116,6,117); 

        $pdf->SetFont('Arial','',4.5);
        $pdf->Cell(15,12,'C.02', '', '','C');

        $pdf->SetFont('Arial','B',7); 
        $pdf->Cell(12,10,$nama_pemotong,'','','C');

        $pdf->SetFont('Arial','',4.5);
        $pdf->Cell(78,12,'C.03', '', '','R');

        $pdf->SetFont('Arial','',9);
        $pdf->Cell(25,10,date("d - m - Y"), '', '1','R');

        // kotak tanda tangan //
        $pdf->Line(200,233,163,233); // garis atas ttd
        $pdf->Line(163,233,163,251); // garis kiri 
        $pdf->Line(200,233,200,251); // garis kanan 
        $pdf->Line(163,251,200,251); // garis bawah

        // $this->response->setHeader('Content-Type', 'application/pdf');
        $pdf->Output();
    }

    
}

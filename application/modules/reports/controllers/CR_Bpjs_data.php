<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    // Load library phpspreadsheet
    require('./vendor/autoload.php');
    use PhpOffice\PhpSpreadsheet\Helper\Sample;
    use PhpOffice\PhpSpreadsheet\IOFactory;
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    // End load library phpspreadsheet


class CR_Bpjs_data extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('masters/M_mst_bio_rec');
        $this->load->model('masters/M_mst_bio_education');
        $this->load->model('masters/M_mst_bio_experience');
        $this->load->model('masters/M_mst_bio_language');
        $this->load->model('masters/M_mst_bio_family');
        $this->load->model('masters/M_mst_bio_references');
        $this->load->model('masters/M_mst_bio_training');
        $this->load->model('masters/M_mst_bio_qualification');
        $this->load->model('masters/M_mst_bio_organization');
    }

    public function displayBPJS()
    {
        $client = "";
        $year = "";
        $month = "";
        $payroll = "";
        // echo $ptName;
        // exit();
        if(isset($_POST['pt'])){
            $client = $_POST['pt'];
        }
        if(isset($_POST['year'])){
            $year = $_POST['year'];
        }
        if(isset($_POST['month'])){
            $month = $_POST['month'];
        }
        if(isset($_POST['payroll'])){
            $payroll = $_POST['payroll'];
        }

        if($client=='Redpath_Timika' OR $client=='Pontil_Timika' OR $client=='Machmahon_Sumbawa'){
            $table      = 'trn_salary_slip';
        }elseif($client=='LCP_Sumbawa'){
            $table      = 'trn_slip_lcpsmb';
        }elseif($client=='Pontil_Banyuwangi'){
            $table      = 'trn_slip_ptlbwg';
        }elseif($client=='Pontil_Sumbawa'){
            $table      = 'trn_slip_ptlsmb';
        }elseif($client=='Trakindo_Sumbawa'){
            $table      = 'trn_slip_trksmb';
        }else{
            $table      = 'trn_salary_slip';
        }

        $sql  = "SELECT a.*, b.full_name,b.marital_status, b.bpjs_no, c.nie, c.payroll_group ";
        $sql .= "FROM ".$table." a ,mst_bio_rec b, mst_salary c ";
        $sql .= "WHERE a.bio_rec_id=b.bio_rec_id and a.bio_rec_id=c.bio_rec_id ";
        $sql .= "and a.client_name='".$client."' and a.year_period='".$year."' and a.month_period='".$month."' ";
        
        $data = $this->db->query($sql);
        $query= $data->result_array();
        /*return json_encode($query);*/
        $data = array();
        foreach ($query as $key => $row) 
        {
            $data[] = array
            (
                $row['full_name'],         
                $row['nie'],                     
                $row['bpjs_no'],         
                $row['payroll_group'],         
                $row['marital_status'],         
                $row['basic_salary'],         
                $row['jkk_jkm'],         
                $row['jht'],       
                $row['emp_jht'],       
                // $row['total'],       
                $row['health_bpjs'],         
                $row['emp_health_bpjs'],         
                $row['emp_jp']              
            );            
        }  
        echo json_encode($data);   
    }

    public function exportBPJS($client, $monthPeriod, $yearPeriod){

        if($client=='Redpath_Timika' OR $client=='Pontil_Timika' OR $client=='Machmahon_Sumbawa'){
            $table      = 'trn_salary_slip';
        }elseif($client=='LCP_Sumbawa'){
            $table      = 'trn_slip_lcpsmb';
        }elseif($client=='Pontil_Banyuwangi'){
            $table      = 'trn_slip_ptlbwg';
        }elseif($client=='Pontil_Sumbawa'){
            $table      = 'trn_slip_ptlsmb';
        }elseif($client=='Trakindo_Sumbawa'){
            $table      = 'trn_slip_trksmb';
        }else{
            $table      = 'trn_salary_slip';
        }

        $objPHPExcel = new Spreadsheet();

        $strSQL   = "SELECT ss.client_name,mb.full_name,mb.bpjs_no,mb.npwp_no,mb.local_foreign,ms.nie,ms.salary_level,ms.payroll_group,mb.marital_status,ss.basic_salary,ss.tax_value,ss.jkk_jkm,ss.jht AS company_jht,ss.emp_jht, ";
        $strSQL  .= "ss.year_period,ss.month_period,ss.payroll_group,";
        $strSQL  .= "(ss.jkk_jkm + ss.jht + ss.emp_jht) jk_jht_total,ss.health_bpjs,ss.emp_health_bpjs,ss.jp,ss.emp_jp,";   
        $strSQL  .= "(ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 + ss.ot_bonus + ss.remote_allowance + ss.shift_bonus + ss.thr + (ss.dev_incentive_bonus * (ss.dev_percent/100)) + ss.contract_bonus + ss.workday_adj + ss.jkk_jkm + ss.health_bpjs - ss.unpaid_total + ss.adjust_in - ss.adjust_out - ss.tax_value - ss.jkk_jkm - ss.emp_health_bpjs - ss.health_bpjs - ss.emp_jht - ss.emp_jp - ss.thr - ss.contract_bonus - ss.debt_burden) total ";    
        $strSQL  .= "FROM mst_bio_rec mb,mst_salary ms,".$table." ss ";
        $strSQL  .= "WHERE mb.bio_rec_id = ms.bio_rec_id ";
        $strSQL  .= "AND mb.bio_rec_id = ss.bio_rec_id ";
        $strSQL  .= "AND ss.client_name = '".$client."' ";
        $strSQL  .= "AND ss.year_period = '".$yearPeriod."' ";
        $strSQL  .= "AND ss.month_period = '".$monthPeriod."' ";
        $strSQL  .= "ORDER BY ss.year_period, ss.month_period, ms.payroll_group, mb.full_name";
        // $strSQL  .= "AND ss.payroll_group = '"' ";
        $query = $this->db->query($strSQL)->result_array();

        // Nama Field Baris Pertama
        $objPHPExcel->getActiveSheet()
            ->setCellValue('A1', 'PT. SANGATI SOERYA SEJAHTERA')
            ->setCellValue('A2', 'LIST BPJS  KARYAWAN PT. '.strtoupper($client) )
            ->setCellValue('A4', 'Periode : '.$monthPeriod.'-'.$yearPeriod);

        $objPHPExcel->getActiveSheet()->getStyle("A1:S1")->getFont()->setBold(true)->setSize(16);
        $objPHPExcel->getActiveSheet()->getStyle("A2:S2")->getFont()->setBold(true)->setSize(13);
        $objPHPExcel->getActiveSheet()->getStyle("A4:S4")->getFont()->setBold(true)->setSize(12); 

        $totalStyle = array(
            'font'  => array(
                'bold'  => true,
                'color' => array('rgb' => '0000FF'),
                // 'size'  => 15,
                // 'name'  => 'Verdana'
            )
        );
        
        $allBorderStyle = array(
            'borders' => array(
                'allborders' => array(
                    'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                )
            )
        );

        $outlineBorderStyle = array(
          'borders' => array(
            'outline' => array(
              'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
            )
          )
        );

        $topBorderStyle = array(
          'borders' => array(
            'top' => array(
              'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
            )
          )
        );

        $bottomBorderStyle = array(
          'borders' => array(
            'bottom' => array(
              'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
            )
          )
        );

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $objPHPExcel->getActiveSheet()->getStyle("A6:S7")
        ->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()
        ->setRGB('F2BE6B');      

        /* START PAYMENT TITLE */
        $objPHPExcel->getActiveSheet()->getStyle("A6:S6")->getFont()->setBold(true)->setSize(12);
        $objPHPExcel->getActiveSheet()->getStyle("A6:S6")->applyFromArray($outlineBorderStyle);
        /* START TITLE NO */
        $titleRowIdx = 6;
        $titleColIdx = 1;
        $objPHPExcel->getActiveSheet()->getStyle("A6:A7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("A6:A7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("A6:A7");
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'NO');
        /* END TITLE NO */

        /* START NAMA KARYAWAN  */
        $titleColIdx++; // 1
        $objPHPExcel->getActiveSheet()->getStyle("B6:B7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("B6:B7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("B6:B7");
        $objPHPExcel->getActiveSheet()->getStyle("B6:B7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'GROUP');
        /* END NAMA KARYAWAN  */

        $titleColIdx++; // 1
        $objPHPExcel->getActiveSheet()->getStyle("C6:C7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("C6:C7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("C6:C7");
        $objPHPExcel->getActiveSheet()->getStyle("C6:C7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'NAMA');

        /* START NO BPJS  */
        $titleColIdx++; // 2
        $objPHPExcel->getActiveSheet()->getStyle("D6:D7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("D6:D7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("D6:D7");
        $objPHPExcel->getActiveSheet()->getStyle("D6:D7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'NIE');
        /* END NO BPJS  */

          /* START GROUP  */
        $titleColIdx++; // 2
        $objPHPExcel->getActiveSheet()->getStyle("E6:E7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("E6:E7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("E6:E7");
        $objPHPExcel->getActiveSheet()->getStyle("E6:E7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'ETHNIC');
        /* END GROUP  */

        /* START ETHNIC */
        $titleColIdx++; // 3
        $objPHPExcel->getActiveSheet()->getStyle("F6:F7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("F6:F7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("F6:F7");
        $objPHPExcel->getActiveSheet()->getStyle("F6:F7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'MARITAL STATUS');
        /* END ETHNIC */

        /* START MARITAL STATUS  */
        $titleColIdx++; // 4
        $objPHPExcel->getActiveSheet()->getStyle("G6:G7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("G6:G7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("G6:G7");
        $objPHPExcel->getActiveSheet()->getStyle("G6:G7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'NO BPJS');
        /* END MARITAL STATUS  */

        /* START NPWP */
        $titleColIdx++; // 5
        $objPHPExcel->getActiveSheet()->getStyle("H6:H7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("H6:H7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("H6:H7");
        $objPHPExcel->getActiveSheet()->getStyle("H6:H7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'NPWP');
        /* END NPWP */

        /* START SALARY LEVEL */
        $titleColIdx++; // 6
        $objPHPExcel->getActiveSheet()->getStyle("I6:I7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("I6:I7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("I6:I7");
        $objPHPExcel->getActiveSheet()->getStyle("I6:I7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'BASIC SALARY');
        /* END SALARY LEVEL */

        /* START BASIC SALARY */
        $titleColIdx++; // 7
        $objPHPExcel->getActiveSheet()->getStyle("J6:J7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("J6:J7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("J6:J7");
        $objPHPExcel->getActiveSheet()->getStyle("J6:J7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'SALARY LEVEL');
        /* END BASIC SALARY */  

        /* START JKK-JKM(2.04%) */
        $titleColIdx++; // 7
        $objPHPExcel->getActiveSheet()->getStyle("K6:K7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("K6:K7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("K6:K7");
        $objPHPExcel->getActiveSheet()->getStyle("K6:K7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'TAX VALUE');
        /* END JKK-JKM(2.04%) */

        /* START BPJS KESEHATAN(4%) */
        $titleColIdx++; // 7
        $objPHPExcel->getActiveSheet()->getStyle("L6:L7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("L6:L7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("L6:L7");
        $objPHPExcel->getActiveSheet()->getStyle("L6:L7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'JKK-JKM(2.04%)');
        /* END BPJS KESEHATAN(4%) */  

        $titleColIdx++; // 7
        $objPHPExcel->getActiveSheet()->getStyle("M6:M7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("M6:M7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("M6:M7");
        $objPHPExcel->getActiveSheet()->getStyle("M6:M7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'BPJS KESEHATAN(1%)');
        /* END TAX VALUE */  

        $titleColIdx++; // 7
        $objPHPExcel->getActiveSheet()->getStyle("N6:N7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("N6:N7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("N6:N7");
        $objPHPExcel->getActiveSheet()->getStyle("N6:N7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'BPJS KESEHATAN(4%)');
        /* END BPJS KESEHATAN(1%) */ 

         $titleColIdx++; // 7
        $objPHPExcel->getActiveSheet()->getStyle("O6:O7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("O6:O7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("O6:O7");
        $objPHPExcel->getActiveSheet()->getStyle("O6:O7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'JHT EMPLOYEE(2%)');
        /* END JHT COMPANY(3.7%) */ 
 
        $titleColIdx++; // 7
        $objPHPExcel->getActiveSheet()->getStyle("P6:P7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("P6:P7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("P6:P7");
        $objPHPExcel->getActiveSheet()->getStyle("P6:P7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'JHT COMPANY(3.7%)');
        /* END JHT EMPLOYEE(2%) */ 

        $titleColIdx++; // 7
        $objPHPExcel->getActiveSheet()->getStyle("Q6:Q7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("Q6:Q7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("Q6:Q7");
        $objPHPExcel->getActiveSheet()->getStyle("Q6:Q7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'JAMINAN PENSIUN (1%)');
        /* END JAMINAN PENSIUN (1%) */

        $titleColIdx++; // 7
        $objPHPExcel->getActiveSheet()->getStyle("R6:R7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("R6:R7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("R6:R7");
        $objPHPExcel->getActiveSheet()->getStyle("R6:R7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'JAMINAN PENSIUN (2%)');
        /* END JAMINAN PENSIUN (1%) */ 

        $titleColIdx++; // 7
        $objPHPExcel->getActiveSheet()->getStyle("S6:S7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("S6:S7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("S6:S7");
        $objPHPExcel->getActiveSheet()->getStyle("S6:S7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'NET PAYMENT');
        /* END TOTAL */ 

        $rowIdx = 7;
        $rowNo = 0;
        foreach ($query as $row) {
            $basicsalary = $row['basic_salary'];
            $setbasic_salary = 0;
            $isfull = false;
            $jumbasic = $basicsalary * (2/100);
            if ($jumbasic <= 161880)
            {
                $setbasic_salary = $jumbasic;
            }
            else{
                $setbasic_salary = 161880;
            }

            $rowIdx++;
            $rowNo++;
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $rowIdx, $row['bio_rec_id']);
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$rowIdx, $rowNo);
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$rowIdx, $row['payroll_group'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $rowIdx, $row['full_name']);
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $rowIdx, $row['nie']);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$rowIdx, $row['full_name'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$rowIdx, $row['nie']);
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$rowIdx, $row['local_foreign']);
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$rowIdx, $row['marital_status']);
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$rowIdx, $row['bpjs_no']);
            $objPHPExcel->getActiveSheet()->setCellValue('H'.$rowIdx, $row['npwp_no']);
            $objPHPExcel->getActiveSheet()->setCellValue('I'.$rowIdx, $row['basic_salary']);
            $objPHPExcel->getActiveSheet()->setCellValue('J'.$rowIdx, $row['salary_level']);
            $objPHPExcel->getActiveSheet()->setCellValue('K'.$rowIdx, $row['tax_value']);
            $objPHPExcel->getActiveSheet()->setCellValue('L'.$rowIdx, $row['jkk_jkm']);
            $objPHPExcel->getActiveSheet()->setCellValue('M'.$rowIdx, $row['emp_health_bpjs']);
            $objPHPExcel->getActiveSheet()->setCellValue('N'.$rowIdx, $row['health_bpjs']);
            $objPHPExcel->getActiveSheet()->setCellValue('O'.$rowIdx, $row['emp_jht']);
            $objPHPExcel->getActiveSheet()->setCellValue('P'.$rowIdx, $row['company_jht']);
            $objPHPExcel->getActiveSheet()->setCellValue('Q'.$rowIdx, $row['emp_jp']);
            $objPHPExcel->getActiveSheet()->setCellValue('R'.$rowIdx, $setbasic_salary);
            $objPHPExcel->getActiveSheet()->setCellValue('S'.$rowIdx, $row['total']);

            /* SET ROW COLOR */
            if($rowIdx % 2 == 1)
            {
                $objPHPExcel->getActiveSheet()->getStyle('A'.$rowIdx.':S'.$rowIdx)
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('EAEBAF');             
            } 

        } /* end foreach ($query as $row) */       
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $rowIdx+2, "JUMLAH");
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $rowIdx+2, "=SUM(K8:K".$rowIdx.")");
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $rowIdx+2, "=SUM(L8:L".$rowIdx.")");
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $rowIdx+2, "=SUM(M8:M".$rowIdx.")");
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $rowIdx+2, "=SUM(N8:N".$rowIdx.")");
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15, $rowIdx+2, "=SUM(O8:O".$rowIdx.")");
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16, $rowIdx+2, "=SUM(P8:P".$rowIdx.")");
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(17, $rowIdx+2, "=SUM(Q8:Q".$rowIdx.")");
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(18, $rowIdx+2, "=SUM(R8:R".$rowIdx.")");
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(19, $rowIdx+2, "=SUM(S8:S".$rowIdx.")");
        $objPHPExcel->getActiveSheet()->getStyle("S6:S7")->applyFromArray($allBorderStyle);
        $totalBorder = $rowIdx+2;
        $objPHPExcel->getActiveSheet()->getStyle("A".$totalBorder.":S".$totalBorder)->applyFromArray($outlineBorderStyle);

        if($rowIdx % 2 == 1)
            {
                $objPHPExcel->getActiveSheet()->getStyle('A'.$rowIdx.':S'.$rowIdx)
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('EAEBAF');             
            } 

        $objPHPExcel->getActiveSheet()->getStyle("A".($rowIdx+2).":S".($rowIdx+2))
        ->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()
        ->setRGB('F2BE6B');

        /* SET NUMBERS FORMAT*/
        $objPHPExcel->getActiveSheet()->getStyle('I8:S'.($rowIdx+2))->getNumberFormat()->setFormatCode('#,##0.00');

        unset($allBorderStyle);
        unset($center);
        unset($right);
        unset($left);
        
        $objPHPExcel->setActiveSheetIndex(0);

        //Set Title
        $objPHPExcel->getActiveSheet()->setTitle('Payroll');

        //Save ke .xlsx, kalau ingin .xls, ubah 'Excel2007' menjadi 'Excel5'
        $writer = IOFactory::createWriter($objPHPExcel, 'Xlsx');
        $writer->setPreCalculateFormulas(true);

        //Header
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

        //Nama File
        $fileName = "BPJS".$client.$yearPeriod.$monthPeriod;
        header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx"');
        // header('Content-Disposition: attachment;filename="'.$client.$yearPeriod.$monthPeriod'.xlsx"');

        //Download
        $writer->save('php://output');
        exit(0); 
    }
}
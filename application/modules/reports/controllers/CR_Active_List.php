<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    // Load library phpspreadsheet
    require('./vendor/autoload.php');
    use PhpOffice\PhpSpreadsheet\Helper\Sample;
    use PhpOffice\PhpSpreadsheet\IOFactory;
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    // End load library phpspreadsheet

class CR_Active_List extends CI_Controller {

    public function __construct(){
        parent::__construct();
    }

    public function getListActive(){
        $pt = "";
        $year = "";
        $month = "";
        if(isset($_POST['pt']))
        {
            $pt = $_POST['pt'];
        }
        if(isset($_POST['year']))
        {
            $year = $_POST['year'];
        }
        if(isset($_POST['month']))
        {
            $month = $_POST['month'];
        }
        $strSQL  = '';
        if ($pt == 'Pontil_Sumbawa'){
            // test('oke',1);
            $strSQL  = "SELECT mb.bio_rec_id,mb.full_name,ms.nie,mb.gender,mb.position,mb.marital_status,mb.id_card_no,mb.id_card_address,mb.current_address,mb.npwp_no ";
            $strSQL .= "FROM db_recruitment.mst_bio_rec mb, db_recruitment.mst_salary ms, db_recruitment.trn_slip_ptlsmb ss ";
            $strSQL .= "WHERE mb.bio_rec_id = ms.bio_rec_id  ";
            $strSQL .= "AND mb.bio_rec_id = ss.bio_rec_id ";
            $strSQL .= "AND ms.company_name = '".$pt."' ";
            $strSQL .= "AND ss.month_period = '".$month."' ";
            $strSQL .= "AND ss.year_period = '".$year."' ";
        }
        else {
            $strSQL  = "SELECT mb.bio_rec_id,mb.full_name,ms.nie,mb.gender,mb.position,mb.marital_status,mb.id_card_no,mb.id_card_address,mb.current_address,mb.npwp_no ";
            $strSQL .= "FROM db_recruitment.mst_bio_rec mb, db_recruitment.mst_salary ms, db_recruitment.trn_salary_slip ss ";
            $strSQL .= "WHERE mb.bio_rec_id = ms.bio_rec_id  ";
            $strSQL .= "AND mb.bio_rec_id = ss.bio_rec_id ";
            $strSQL .= "AND ms.company_name = '".$pt."' ";
            $strSQL .= "AND ss.month_period = '".$month."' ";
            $strSQL .= "AND ss.year_period = '".$year."' ";
        }
        // $strSQL .= "AND mb.is_active = 1 ";       
        // test($strSQL,1);
        $query = $this->db->query($strSQL)->result_array();

        $myData = array();
        foreach ($query as $key => $row) {
            $myData[] = array(
                $row['bio_rec_id'],         
                $row['full_name'],         
                $row['nie'],         
                $row['gender'],         
                $row['marital_status'],         
                $row['id_card_no'],         
                $row['id_card_address'],         
                $row['npwp_no'],
                $row['current_address'],          
                $row['position']          
            );            
        }  
        echo json_encode($myData);  
    }

    public function printListActive($ptName, $yearPeriod, $monthPeriod)
    {
        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();

        $spreadsheet->getProperties()->setCreator('Maurice - Web - Android')
            ->setLastModifiedBy('Maurice - Web - Android')
            ->setTitle('Office 2007 XLSX Test Document')
            ->setSubject('Office 2007 XLSX Test Document')
            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Test result file');

        $boldFont = [
            'font' => [
                'bold' => true
                // 'color' => ['argb' => '0000FF'],
            ],
        ];

        $totalStyle = [
            'font' => [
                'bold' => true,
                'color' => ['argb' => '0000FF'],
            ],
        ];

        $allBorderStyle = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $outlineBorderStyle = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $topBorderStyle = [
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $bottomBorderStyle = [
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $strSQL  = '';
        if ($ptName == 'Pontil_Sumbawa'){
            $strSQL  = "SELECT mb.bio_rec_id,mb.full_name,ms.nie,mb.gender,mb.position,mb.marital_status,mb.id_card_no,mb.id_card_address,mb.current_address,mb.npwp_no ";
            $strSQL .= "FROM db_recruitment.mst_bio_rec mb, db_recruitment.mst_salary ms, db_recruitment.trn_slip_ptlsmb ss ";
            $strSQL .= "WHERE mb.bio_rec_id = ms.bio_rec_id  ";
            $strSQL .= "AND mb.bio_rec_id = ss.bio_rec_id ";
            $strSQL .= "AND ms.company_name = '".$ptName."' ";
            $strSQL .= "AND ss.month_period = '".$monthPeriod."' ";
            $strSQL .= "AND ss.year_period = '".$yearPeriod."' ";
        }
        else{
            $strSQL  = "SELECT mb.bio_rec_id,mb.full_name,ms.nie,mb.gender,mb.position,mb.marital_status,mb.id_card_no,mb.id_card_address,mb.current_address,mb.npwp_no ";
            $strSQL .= "FROM db_recruitment.mst_bio_rec mb, db_recruitment.mst_salary ms, db_recruitment.trn_salary_slip ss ";
            $strSQL .= "WHERE mb.bio_rec_id = ms.bio_rec_id  ";
            $strSQL .= "AND mb.bio_rec_id = ss.bio_rec_id ";
            $strSQL .= "AND ms.company_name = '".$ptName."' ";
            $strSQL .= "AND ss.month_period = '".$monthPeriod."' ";
            $strSQL .= "AND ss.year_period = '".$yearPeriod."' ";
        }

        $query = $this->db->query($strSQL)->result_array();

        // Add some data
        $spreadsheet->getActiveSheet()->getStyle('A6:J7')
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B'); 

        $spreadsheet->getActiveSheet()
            ->setCellValue('A1', 'PT. SANGATI SOERYA SEJAHTERA')
            ->setCellValue('A2', 'LIST KARYAWAN AKTIF PT.'.$ptName.' ')
            ->setCellValue('A4', 'Periode : '.$monthPeriod.'-'.$yearPeriod);

        $spreadsheet->getActiveSheet()->getStyle("A1:D1")->getFont()->setBold(true)->setSize(16);
        $spreadsheet->getActiveSheet()->getStyle("A2:D2")->getFont()->setBold(true)->setSize(14)->setUnderline(true);
        $spreadsheet->getActiveSheet()->getStyle("A4:G4")->getFont()->setBold(true)->setSize(12);
        $spreadsheet->getActiveSheet()->getStyle("A6:J7")->getFont()->setBold(true)->setSize(12);

        $spreadsheet->getActiveSheet()->mergeCells("A6:A7");
        $spreadsheet->getActiveSheet()->mergeCells("B6:B7");
        $spreadsheet->getActiveSheet()->mergeCells("C6:C7");
        $spreadsheet->getActiveSheet()->mergeCells("D6:D7");
        $spreadsheet->getActiveSheet()->mergeCells("E6:E7");
        $spreadsheet->getActiveSheet()->mergeCells("F6:F7");
        $spreadsheet->getActiveSheet()->mergeCells("G6:G7");
        $spreadsheet->getActiveSheet()->mergeCells("H6:H7");
        $spreadsheet->getActiveSheet()->mergeCells("I6:I7");
        $spreadsheet->getActiveSheet()->mergeCells("J6:J7");
        // $spreadsheet->getActiveSheet()->mergeCells("K6:K7");
        // $spreadsheet->getActiveSheet()->mergeCells("L6:L7");
            
        $spreadsheet->getActiveSheet()->getStyle("A6:J7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("A6:J7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()
            ->setCellValue('A6', 'INTERNAL ID')
            ->setCellValue('B6', 'NAME')
            ->setCellValue('C6', 'EXTERNAL ID')
            ->setCellValue('D6', 'GENDER')
            ->setCellValue('E6', 'MARITAL STATUS')
            ->setCellValue('F6', 'NIK')
            ->setCellValue('G6', 'NIK ADDRESS')
            ->setCellValue('H6', 'NPWP')
            ->setCellValue('I6', 'NPWP ADDRESS')
            ->setCellValue('J6', 'POSITION');
            // ->setCellValue('K6', 'ACCOUNT ON BEHALF OF')
            // ->setCellValue('L6', 'ACCOUNT NUMBER');

        /* START GET DAYS TOTAL BY ROSTER */
        $rowIdx = 7;
        // $rowNo = 0;
        foreach ($query as $row) {                      
            $rowIdx++;
            // $rowNo++;
            $spreadsheet->getActiveSheet()
                ->setCellValue('A'.($rowIdx), $row['bio_rec_id'])
                ->setCellValue('B'.($rowIdx), $row['full_name'])
                ->setCellValue('C'.($rowIdx), $row['nie'])
                ->setCellValue('D'.($rowIdx), $row['gender'])
                ->setCellValue('E'.($rowIdx), $row['marital_status'])
                ->setCellValue('F'.($rowIdx), $row['id_card_no'],\PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING)
                ->setCellValue('G'.($rowIdx), $row['id_card_address'])
                ->setCellValue('H'.($rowIdx), $row['npwp_no'],\PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING)
                ->setCellValue('I'.($rowIdx), $row['current_address'])
                ->setCellValue('J'.($rowIdx), $row['position'])
                // ->setCellValue('K'.($rowIdx), $row['account_name'])
                // ->setCellValue('L'.($rowIdx), $row['account_no'])
                ;

            /* SET ROW COLOR */
            if($rowIdx % 2 == 1)
            {
                $spreadsheet->getActiveSheet()->getStyle('A'.$rowIdx.':J'.$rowIdx)
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('EAEBAF');             
            } 
        }

        $spreadsheet->getActiveSheet()->getStyle("A6:J".($rowIdx))->applyFromArray($allBorderStyle);
        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('Report Excel '.date('d-m-Y H'));

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        // $str = 'PTLSmbInvoice';
        // $fileName = 'Employee Active List PT.'.$ptName.'';
        
        $str = 'Employee Active List PT.'.$ptName.'';
        $fileName = preg_replace('/\s+/', '', $str);
        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.Xlsx"');
        // header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        /* BY COMPOSER */
        // $writer = new Xlsx($spreadsheet);
        /* OFFLINE/ BY COPY EXCEL FOLDER  */
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit(0);   
    }    
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    // Load library phpspreadsheet
    require('./vendor/autoload.php');
    use PhpOffice\PhpSpreadsheet\Helper\Sample;
    use PhpOffice\PhpSpreadsheet\IOFactory;
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    // End load library phpspreadsheet


class CR_Closing extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('masters/M_mst_bio_rec');
        $this->load->model('masters/M_mst_bio_education');
        $this->load->model('masters/M_mst_bio_experience');
        $this->load->model('masters/M_mst_bio_language');
        $this->load->model('masters/M_mst_bio_family');
        $this->load->model('masters/M_mst_bio_references');
        $this->load->model('masters/M_mst_bio_training');
        $this->load->model('masters/M_mst_bio_qualification');
        $this->load->model('masters/M_mst_bio_organization');
    }

    public function dataClosing()
    {

        $sql  = " SELECT closing_id,client_name,close_year,close_month,is_active FROM db_recruitment.mst_process_closing WHERE closing_id<>'' ";
        if($this->input->post('pt')!=''){
            $sql .= " AND client_name='".$this->input->post('pt')."'";            
        }
        $sql .= " order by closing_id DESC";

        $data = $this->db->query($sql);
        $query= $data->result_array();

        $data = array();
        foreach ($query as $key => $row) 
        {
            $data[] = array
            (
                $row['client_name'],                     
                $row['close_year'],         
                $row['close_month'],  
                ($row['is_active']=='1')? 'Closing' : 'Open',   
                ($row['is_active']=='1')? 
                    // '<a class="btn btn-warning" onclick="return openData(this)" data-client="'.$row['client_name'].'" data-tahun="'.$row['close_year'].'" data-bulan="'.$row['close_month'].'">Open</a>'
                    '<button class="btn btn-warning btn-sm" type="button" id="oClosingData" data-toggle="modal" data-client="'.$row['client_name'].'" data-tahun="'.$row['close_year'].'" data-bulan="'.$row['close_month'].'" data-target="#openModal">Open</button>' 
                    : 
                    '<button class="btn btn-warning btn-sm" type="button" id="cClosingData" data-toggle="modal" data-client="'.$row['client_name'].'" data-tahun="'.$row['close_year'].'" data-bulan="'.$row['close_month'].'" data-target="#closingModal">Closing</button>' 
            );            
        }  
        echo json_encode($data);   
    }

    public function updateClosing() 
    {        
        
        $client_name    = $this->security->xss_clean($this->db->escape_str($_POST['client_name']));    
        $yearPeriod     = $this->security->xss_clean($this->db->escape_str($_POST['yearPeriod']));    
        $monthPeriod    = $this->security->xss_clean($this->db->escape_str($_POST['monthPeriod']));    
        $is_active      = $this->security->xss_clean($this->db->escape_str($_POST['is_active']));
        
        $this->db->query("UPDATE db_recruitment.mst_process_closing SET is_active = '".$is_active."' WHERE client_name = '".$client_name."' AND close_year = '".$yearPeriod."' AND close_month = '".$monthPeriod."'");

    }

    function insertClosing(){

        $client_name    = $this->security->xss_clean($this->db->escape_str($_POST['client_name']));    
        $yearPeriod     = $this->security->xss_clean($this->db->escape_str($_POST['yearPeriod']));    
        $monthPeriod    = $this->security->xss_clean($this->db->escape_str($_POST['monthPeriod']));
        $is_active    = $this->security->xss_clean($this->db->escape_str($_POST['is_active']));

        $cek            = $this->db->query("SELECT * FROM `mst_process_closing` WHERE client_name='".$client_name."' AND close_year='".$yearPeriod."' AND close_month='".$monthPeriod."'")->num_rows();
        if($cek>=1){
            echo 'erorr';
        }else{
            $this->db->query("INSERT INTO db_recruitment.mst_process_closing 
                                (client_name,close_year,close_month,is_active)VALUES('".$client_name."','".$yearPeriod."','".$monthPeriod."','".$is_active."')");
        }
    }



















































    public function exportdataCrews(){

        $objPHPExcel = new Spreadsheet();

        $strSQL   = " SELECT b.nie,a.full_name,a.place_of_birth,a.date_of_birth,a.id_card_no,a.id_card_address,a.npwp_no,a.marital_status,
                        a.position,b.basic_salary,b.bank_name,b.account_name,b.account_no
                        FROM mst_bio_rec a LEFT JOIN mst_salary b ON a.bio_rec_id=b.bio_rec_id
                        WHERE b.company_name='LCP_Sumbawa' ";

        $query = $this->db->query($strSQL)->result_array();

        // Nama Field Baris Pertama
        $objPHPExcel->getActiveSheet()
            ->setCellValue('A1', 'PT. SANGATI SOERYA SEJAHTERA')
            ->setCellValue('A2', 'DATA CREWS PT. '.strtoupper("LCP_Sumbawa") );
            // ->setCellValue('A4', 'Periode : '.$monthPeriod.'-'.$yearPeriod);

        $objPHPExcel->getActiveSheet()->getStyle("A1:S1")->getFont()->setBold(true)->setSize(16);
        $objPHPExcel->getActiveSheet()->getStyle("A2:S2")->getFont()->setBold(true)->setSize(13);
        $objPHPExcel->getActiveSheet()->getStyle("A4:S4")->getFont()->setBold(true)->setSize(12); 

        $totalStyle = array(
            'font'  => array(
                'bold'  => true,
                'color' => array('rgb' => '0000FF'),
                // 'size'  => 15,
                // 'name'  => 'Verdana'
            )
        );
        
        $allBorderStyle = array(
            'borders' => array(
                'allborders' => array(
                    'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                )
            )
        );

        $outlineBorderStyle = array(
          'borders' => array(
            'outline' => array(
              'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
            )
          )
        );

        $topBorderStyle = array(
          'borders' => array(
            'top' => array(
              'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
            )
          )
        );

        $bottomBorderStyle = array(
          'borders' => array(
            'bottom' => array(
              'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
            )
          )
        );

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $objPHPExcel->getActiveSheet()->getStyle("A6:M7")
        ->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()
        ->setRGB('F2BE6B');      

        /* START PAYMENT TITLE */
        $objPHPExcel->getActiveSheet()->getStyle("A6:S6")->getFont()->setBold(true)->setSize(12);
        $objPHPExcel->getActiveSheet()->getStyle("A6:S6")->applyFromArray($outlineBorderStyle);
        /* START TITLE NO */
        $titleRowIdx = 6;
        $titleColIdx = 1;
        $objPHPExcel->getActiveSheet()->getStyle("A6:A7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("A6:A7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("A6:A7");
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'BADGE ID');
        /* END TITLE NO */

        /* START NAMA KARYAWAN  */
        $titleColIdx++; // 1
        $objPHPExcel->getActiveSheet()->getStyle("B6:B7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("B6:B7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("B6:B7");
        $objPHPExcel->getActiveSheet()->getStyle("B6:B7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'NAME');
        /* END NAMA KARYAWAN  */

        $titleColIdx++; // 1
        $objPHPExcel->getActiveSheet()->getStyle("C6:C7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("C6:C7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("C6:C7");
        $objPHPExcel->getActiveSheet()->getStyle("C6:C7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'PLACE OF BIRTH');

        /* START NO BPJS  */
        $titleColIdx++; // 2
        $objPHPExcel->getActiveSheet()->getStyle("D6:D7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("D6:D7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("D6:D7");
        $objPHPExcel->getActiveSheet()->getStyle("D6:D7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'DATE OF BIRTH');
        /* END NO BPJS  */

          /* START GROUP  */
        $titleColIdx++; // 2
        $objPHPExcel->getActiveSheet()->getStyle("E6:E7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("E6:E7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("E6:E7");
        $objPHPExcel->getActiveSheet()->getStyle("E6:E7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'ID CARD NUMBER');
        /* END GROUP  */

        /* START ETHNIC */
        $titleColIdx++; // 3
        $objPHPExcel->getActiveSheet()->getStyle("F6:F7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("F6:F7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("F6:F7");
        $objPHPExcel->getActiveSheet()->getStyle("F6:F7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'ADDRESS');
        /* END ETHNIC */

        /* START MARITAL STATUS  */
        $titleColIdx++; // 4
        $objPHPExcel->getActiveSheet()->getStyle("G6:G7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("G6:G7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("G6:G7");
        $objPHPExcel->getActiveSheet()->getStyle("G6:G7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'NPWP NUMBER');
        /* END MARITAL STATUS  */

        /* START NPWP */
        $titleColIdx++; // 5
        $objPHPExcel->getActiveSheet()->getStyle("H6:H7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("H6:H7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("H6:H7");
        $objPHPExcel->getActiveSheet()->getStyle("H6:H7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'MARITAL STATUS');
        /* END NPWP */

        /* START SALARY LEVEL */
        $titleColIdx++; // 6
        $objPHPExcel->getActiveSheet()->getStyle("I6:I7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("I6:I7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("I6:I7");
        $objPHPExcel->getActiveSheet()->getStyle("I6:I7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'POSITION');
        /* END SALARY LEVEL */

        /* START BASIC SALARY */
        $titleColIdx++; // 7
        $objPHPExcel->getActiveSheet()->getStyle("J6:J7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("J6:J7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("J6:J7");
        $objPHPExcel->getActiveSheet()->getStyle("J6:J7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'BASIC SALARY');
        /* END BASIC SALARY */  

        /* START JKK-JKM(2.04%) */
        $titleColIdx++; // 7
        $objPHPExcel->getActiveSheet()->getStyle("K6:K7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("K6:K7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("K6:K7");
        $objPHPExcel->getActiveSheet()->getStyle("K6:K7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'BANK NAME');
        /* END JKK-JKM(2.04%) */

        /* START BPJS KESEHATAN(4%) */
        $titleColIdx++; // 7
        $objPHPExcel->getActiveSheet()->getStyle("L6:L7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("L6:L7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("L6:L7");
        $objPHPExcel->getActiveSheet()->getStyle("L6:L7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'ACCOUNT NAME');
        /* END BPJS KESEHATAN(4%) */  

        $titleColIdx++; // 7
        $objPHPExcel->getActiveSheet()->getStyle("M6:M7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("M6:M7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("M6:M7");
        $objPHPExcel->getActiveSheet()->getStyle("M6:M7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'ACCOUNT NUMBER');

        // $titleColIdx++; // 7
        // $objPHPExcel->getActiveSheet()->getStyle("N6:N7")->applyFromArray($allBorderStyle);
        // $objPHPExcel->getActiveSheet()->getStyle("N6:N7")->applyFromArray($center);
        // $objPHPExcel->getActiveSheet()->mergeCells("N6:N7");
        // $objPHPExcel->getActiveSheet()->getStyle("N6:N7")->getAlignment()->setWrapText(true);
        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'BPJS KESEHATAN(4%)');

        //  $titleColIdx++; // 7
        // $objPHPExcel->getActiveSheet()->getStyle("O6:O7")->applyFromArray($allBorderStyle);
        // $objPHPExcel->getActiveSheet()->getStyle("O6:O7")->applyFromArray($center);
        // $objPHPExcel->getActiveSheet()->mergeCells("O6:O7");
        // $objPHPExcel->getActiveSheet()->getStyle("O6:O7")->getAlignment()->setWrapText(true);
        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'JHT EMPLOYEE(2%)');
 
        // $titleColIdx++; // 7
        // $objPHPExcel->getActiveSheet()->getStyle("P6:P7")->applyFromArray($allBorderStyle);
        // $objPHPExcel->getActiveSheet()->getStyle("P6:P7")->applyFromArray($center);
        // $objPHPExcel->getActiveSheet()->mergeCells("P6:P7");
        // $objPHPExcel->getActiveSheet()->getStyle("P6:P7")->getAlignment()->setWrapText(true);
        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'JHT COMPANY(3.7%)');

        // $titleColIdx++; // 7
        // $objPHPExcel->getActiveSheet()->getStyle("Q6:Q7")->applyFromArray($allBorderStyle);
        // $objPHPExcel->getActiveSheet()->getStyle("Q6:Q7")->applyFromArray($center);
        // $objPHPExcel->getActiveSheet()->mergeCells("Q6:Q7");
        // $objPHPExcel->getActiveSheet()->getStyle("Q6:Q7")->getAlignment()->setWrapText(true);
        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'JAMINAN PENSIUN (1%)');

        // $titleColIdx++; // 7
        // $objPHPExcel->getActiveSheet()->getStyle("R6:R7")->applyFromArray($allBorderStyle);
        // $objPHPExcel->getActiveSheet()->getStyle("R6:R7")->applyFromArray($center);
        // $objPHPExcel->getActiveSheet()->mergeCells("R6:R7");
        // $objPHPExcel->getActiveSheet()->getStyle("R6:R7")->getAlignment()->setWrapText(true);
        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'JAMINAN PENSIUN (2%)');

        // $titleColIdx++; // 7
        // $objPHPExcel->getActiveSheet()->getStyle("S6:S7")->applyFromArray($allBorderStyle);
        // $objPHPExcel->getActiveSheet()->getStyle("S6:S7")->applyFromArray($center);
        // $objPHPExcel->getActiveSheet()->mergeCells("S6:S7");
        // $objPHPExcel->getActiveSheet()->getStyle("S6:S7")->getAlignment()->setWrapText(true);
        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'NET PAYMENT');

        $rowIdx = 7;
        $rowNo = 0;
        foreach ($query as $row) {

            $rowIdx++;
            $rowNo++;
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$rowIdx, $row['nie']);
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$rowIdx, $row['full_name'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$rowIdx, $row['place_of_birth'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$rowIdx, $row['date_of_birth']);
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$rowIdx, $row['id_card_no'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$rowIdx, $row['id_card_address']);
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$rowIdx, $row['npwp_no'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValue('H'.$rowIdx, $row['marital_status']);
            $objPHPExcel->getActiveSheet()->setCellValue('I'.$rowIdx, $row['position']);
            $objPHPExcel->getActiveSheet()->setCellValue('J'.$rowIdx, $row['basic_salary']);
            $objPHPExcel->getActiveSheet()->setCellValue('K'.$rowIdx, $row['bank_name']);
            $objPHPExcel->getActiveSheet()->setCellValue('L'.$rowIdx, $row['account_name']);
            $objPHPExcel->getActiveSheet()->setCellValue('M'.$rowIdx, $row['account_no']);
            // $objPHPExcel->getActiveSheet()->setCellValue('N'.$rowIdx, $row['health_bpjs']);
            // $objPHPExcel->getActiveSheet()->setCellValue('O'.$rowIdx, $row['emp_jht']);
            // $objPHPExcel->getActiveSheet()->setCellValue('P'.$rowIdx, $row['company_jht']);
            // $objPHPExcel->getActiveSheet()->setCellValue('Q'.$rowIdx, $row['emp_jp']);
            // $objPHPExcel->getActiveSheet()->setCellValue('R'.$rowIdx, $setbasic_salary);
            // $objPHPExcel->getActiveSheet()->setCellValue('S'.$rowIdx, $row['total']);

            /* SET ROW COLOR */
            if($rowIdx % 2 == 1)
            {
                $objPHPExcel->getActiveSheet()->getStyle('A'.$rowIdx.':M'.$rowIdx)
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('EAEBAF');             
            } 

        } /* end foreach ($query as $row) */       
        
        // $objPHPExcel->getActiveSheet()->getStyle("A".($rowIdx+2).":M".($rowIdx+2))
        // ->getFill()
        // ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        // ->getStartColor()
        // ->setRGB('F2BE6B');

        /* SET NUMBERS FORMAT*/

        unset($allBorderStyle);
        unset($center);
        unset($right);
        unset($left);
        
        $objPHPExcel->setActiveSheetIndex(0);

        //Set Title
        $objPHPExcel->getActiveSheet()->setTitle('Data Crews LCP');

        //Save ke .xlsx, kalau ingin .xls, ubah 'Excel2007' menjadi 'Excel5'
        $writer = IOFactory::createWriter($objPHPExcel, 'Xlsx');
        $writer->setPreCalculateFormulas(true);

        //Header
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

        //Nama File
        $fileName = "Data Crews LCP";
        header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx"');
        // header('Content-Disposition: attachment;filename="'.$client.$yearPeriod.$monthPeriod'.xlsx"');

        //Download
        $writer->save('php://output');
        exit(0); 
    }


}

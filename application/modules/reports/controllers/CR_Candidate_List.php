<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    // Load library phpspreadsheet
    require('./vendor/autoload.php');
    use PhpOffice\PhpSpreadsheet\Helper\Sample;
    use PhpOffice\PhpSpreadsheet\IOFactory;
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    // End load library phpspreadsheet


class CR_Candidate_List extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('masters/M_mst_bio_rec');
        $this->load->model('masters/M_mst_bio_education');
        $this->load->model('masters/M_mst_bio_experience');
        $this->load->model('masters/M_mst_bio_language');
        $this->load->model('masters/M_mst_bio_family');
        $this->load->model('masters/M_mst_bio_references');
        $this->load->model('masters/M_mst_bio_training');
        $this->load->model('masters/M_mst_bio_qualification');
        $this->load->model('masters/M_mst_bio_organization');
    }

    public function getCandidateList(){
        $sAge       = $this->input->post('sAge');
        $sPosition  = $this->input->post('sPosition');
        $sCity      = $this->input->post('sCity');
        $sName      = $this->input->post('sName');
        $sNo        = $this->input->post('sNo');

        $isActive = 0;

        $strSql  = 'SELECT bio_rec_id, full_name, date_of_birth, cell_no, id_card_address,city_address, nationality, religion, marital_status,  ';
        $strSql .= '`position`,cv_date,date_of_birth,TIMESTAMPDIFF(YEAR,date_of_birth, NOW()) AS old ';
        $strSql .= 'FROM '.$this->db->database.'.mst_bio_rec ';
        $strSql .= 'WHERE is_active = '.$isActive.' ';
        if($sAge!=''){
            $strSql .= 'AND TIMESTAMPDIFF(YEAR,date_of_birth, NOW() ) <= '.$this->security->xss_clean($sAge).' ';
        }
        if($sPosition!=''){
            $strSql .= 'AND position LIKE "%'.$this->security->xss_clean($sPosition).'%" ';
        }
        if($sCity!=''){
            $strSql .= 'AND city_address LIKE "%'.$this->security->xss_clean($sCity).'%" ';
        }
        if($sName!=''){
            $strSql .= 'AND full_name LIKE "%'.$this->security->xss_clean($sName).'%" ';
        }
        if($sNo!=''){
            $strSql .= 'AND id_card_no = "'.$sNo.'" ';
        }
        $strSql .= 'ORDER BY date_of_birth ASC ';
        // test($strSql,1);
        // echo $strSql; exit(0);
        $query = $this->db->query($strSql)->result_array();
         /*return json_encode($query);*/
        $myData = array();
        foreach ($query as $row) {
            $myData[] = array(
                $row['bio_rec_id'],         
                $row['full_name'],         
                $row['id_card_address'],         
                $row['city_address'],         
                $row['cell_no'],         
                $row['position'],
                $row['cv_date']
            );            
        }  
        echo json_encode($myData); 
    }

    public function GetBiodataJasonById(){
        $rs = $this->M_mst_bio_rec->getBiodataJasonById($this->input->post('bioId'));
        echo $rs;                  
    }

    public function Upd(){
        // test($_POST['updMstBioRec'],0);
        if(isset($_POST['updMstBioRec']))
        {        
            $id = ($this->security->xss_clean($_POST['biodataId']));
            // test($id,1);
            $this->M_mst_bio_rec->loadById($id);
            $this->M_mst_bio_rec->setFullName($this->security->xss_clean($_POST['fullName']));
            $this->M_mst_bio_rec->setGender($this->security->xss_clean($_POST['gender']));            
            $this->M_mst_bio_rec->setPlaceOfBirth($this->security->xss_clean($_POST['birthPlace']));
            $this->M_mst_bio_rec->setDateOfBirth($this->security->xss_clean($_POST['birthDate']));
            $this->M_mst_bio_rec->setNationality($this->security->xss_clean($_POST['nationality']));
            $this->M_mst_bio_rec->setClan($this->security->xss_clean($_POST['etnics']));
            $this->M_mst_bio_rec->setIdCardNo($this->security->xss_clean($_POST['idNo']));
            $this->M_mst_bio_rec->setNpwpNo($this->security->xss_clean($_POST['npwpNo']));
            $this->M_mst_bio_rec->setBpjsNo($this->security->xss_clean($_POST['bpjsNo']));
            $this->M_mst_bio_rec->setIdCardAddress($this->security->xss_clean($_POST['idAddress']));
            $this->M_mst_bio_rec->setCurrentAddress($this->security->xss_clean($_POST['currentAddress']));            
            $this->M_mst_bio_rec->setCityAddress($this->security->xss_clean($_POST['cityAddress']));
            $this->M_mst_bio_rec->setResidenceStatus($this->security->xss_clean($_POST['livingStatus']));
            $this->M_mst_bio_rec->setReligion($this->security->xss_clean($_POST['religion']));
            $this->M_mst_bio_rec->setDrivingLicense($this->security->xss_clean($_POST['driveLicence']));
            $this->M_mst_bio_rec->setMaritalStatus($this->security->xss_clean($_POST['maritalStatus']));
            $this->M_mst_bio_rec->setPosition($this->security->xss_clean($_POST['appliedPosition']));            
            $this->M_mst_bio_rec->setCvDate($this->security->xss_clean($_POST['cvDate']));
            $this->M_mst_bio_rec->setHeight($this->security->xss_clean($_POST['height']));
            $this->M_mst_bio_rec->setWeight($this->security->xss_clean($_POST['weight']));
            $this->M_mst_bio_rec->setIsColorBlind($this->security->xss_clean($_POST['colorBlindness']));
            $this->M_mst_bio_rec->setBloodType($this->security->xss_clean($_POST['bloodType']));
            $this->M_mst_bio_rec->setEmailAddress($this->security->xss_clean($_POST['emailAddress']));
            $this->M_mst_bio_rec->setTelpNo($this->security->xss_clean($_POST['telpNo']));
            $this->M_mst_bio_rec->setCellNo($this->security->xss_clean($_POST['cellNo']));
            $this->M_mst_bio_rec->setLocalForeign($this->security->xss_clean($_POST['pointOfHire']));

            // $this->M_MstBioRec->setNiiId($this->security->xss_clean($_POST['nii_id']));
            // $this->M_mst_bio_rec->setIsStaff($this->security->xss_clean($_POST['staffingStatus']));
            // $this->M_mst_bio_rec->setDateOfHire($this->security->xss_clean($_POST['dateOfHire']));
            $currentTime = date('Y-m-d H:i:s');
            $this->M_mst_bio_rec->setPicEdit($this->security->xss_clean($this->session->userdata('hris_user_id')));
            $this->M_mst_bio_rec->setEditTime($this->security->xss_clean($currentTime));
            $this->M_mst_bio_rec->updateBiorec($id);
        }
    }

    function printBioReports($sAge,$sPosition,$sCity,$sName,$sNo){
        $spreadsheet = new Spreadsheet();

        if (file_exists('assets/images/report_logo.jpg')) {
          $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
          $drawing->setName('Logo');
          $drawing->setDescription('Logo');
          $drawing->setPath('./assets/images/report_logo.jpg');
          $drawing->setCoordinates('A1');
          $drawing->setHeight(36);
          $drawing->setWorksheet($spreadsheet->getActiveSheet());
        }

        $boldFont = [
          'font' => [
            'bold' => true
              // 'color' => ['argb' => '0000FF'],
          ],
        ];

        $totalStyle = [
          'font' => [
            'bold' => true,
            'color' => ['argb' => '0000FF'],
          ],
        ];

        $allBorderStyle = [
          'borders' => [
            'allBorders' => [
              'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
              'color' => ['argb' => '00000000'],
            ],
          ],
        ];

        $outlineBorderStyle = [
          'borders' => [
            'outline' => [
              'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
              'color' => ['argb' => '00000000'],
            ],
          ],
        ];

        $topBorderStyle = [
          'borders' => [
            'top' => [
              'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
              'color' => ['argb' => '00000000'],
            ],
          ],
        ];

        $bottomBorderStyle = [  
          'borders' => [
            'bottom' => [
              'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
              'color' => ['argb' => '00000000'],
            ],
          ],
        ];

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        /* COLOURING FOOTER */
        $spreadsheet->getActiveSheet()->getStyle("A4:K5")
        ->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()
        ->setRGB('F2BE6B'); 

        // test($sAge.' '.$sPosition.' '.$sCity.' '.$sName.' '.$sNo,1);
        $isActive = 0;

        $strSql  = 'SELECT bio_rec_id, full_name, date_of_birth, cell_no, id_card_address,city_address, nationality, religion, marital_status,  ';
        $strSql .= '`position`,cv_date,date_of_birth,TIMESTAMPDIFF(YEAR,date_of_birth, NOW()) AS old ';
        $strSql .= 'FROM '.$this->db->database.'.mst_bio_rec ';
        $strSql .= 'WHERE is_active = '.$isActive.' ';
        if($sAge!='-'){
            $strSql .= 'AND TIMESTAMPDIFF(YEAR,date_of_birth, NOW() ) <= '.$this->security->xss_clean($sAge).' ';
        }
        if($sPosition!='-'){
            $strSql .= 'AND position LIKE "%'.$this->security->xss_clean($sPosition).'%" ';
        }
        if($sCity!='-'){
            $strSql .= 'AND city_address LIKE "%'.$this->security->xss_clean($sCity).'%" ';
        }
        if($sName!='-'){
            $strSql .= 'AND AND full_name LIKE "%'.$this->security->xss_clean($sName).'%" ';
        }
        if($sNo!='-'){
            $strSql .= 'AND id_card_no = "'.$sNo.'" ';
        }
        $strSql .= 'ORDER BY date_of_birth ASC ';

        // echo $strSql;
        // exit();
        $query = $this->db->query($strSql)->result_array(); 

        /* START DATA IS HERE */
        $spreadsheet->getActiveSheet()
            ->setCellValue('A1', 'PT. SANGATI SOERYA SEJAHTERA')
            ->setCellValue('A2', 'Candidate List');
        /* END DATA IS HERE */

        $spreadsheet->getActiveSheet()->mergeCells("A1:K1");
        $spreadsheet->getActiveSheet()->getStyle("A1:K1")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->mergeCells("A2:K2");
        $spreadsheet->getActiveSheet()->getStyle("A2:K2")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("A4:K4")->applyFromArray($boldFont);
        $spreadsheet->getActiveSheet()->getStyle("A4:K4")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("A1:K1")->getFont()->setBold(true)->setSize(16);
        $spreadsheet->getActiveSheet()->getStyle("A2:K2")->getFont()->setBold(true)->setSize(13);
        $spreadsheet->getActiveSheet()->getStyle("A4:K4")->getFont()->setBold(true)->setSize(12);  

        $spreadsheet->getActiveSheet()->getStyle('A4:A5')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('F2BE6B');
        // $spreadsheet->getActiveSheet()->getStyle('A4:A5')
        //         ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLUE); 
        // $spreadsheet->getActiveSheet()->getStyle("A6:H6")->applyFromArray($outlineBorderStyle);
        // $spreadsheet->getActiveSheet()->getStyle("A6:AC9")->applyFromArray($center);
        // /* START TITLE NO */
        // $spreadsheet->getActiveSheet()->getStyle("A6:AC8")->applyFromArray($allBorderStyle);

        /* START TITLE BIODATA ID */
        $spreadsheet->getActiveSheet()
            ->mergeCells("A4:A5")
            ->mergeCells("B4:B5")
            ->mergeCells("C4:C5")
            ->mergeCells("D4:D5")
            ->mergeCells("E4:E5")
            ->mergeCells("F4:F5")
            ->mergeCells("G4:G5")
            ->mergeCells("H4:H5")
            ->mergeCells("I4:I5")
            ->mergeCells("J4:J5")
            ->mergeCells("K4:K5");

        $titleRowIdx = 4;
        $spreadsheet->getActiveSheet()
            ->setCellValue('A4', 'No')
            ->setCellValue('B4', 'Biodata ID')
            ->setCellValue('C4', 'Name')
            ->setCellValue('D4', 'Birth Of Date')
            ->setCellValue('E4', 'Contact No.')
            ->setCellValue('F4', 'Address')
            ->setCellValue('G4', 'City')
            ->setCellValue('H4', 'Nationality')
            ->setCellValue('I4', 'Religion')
            ->setCellValue('J4', 'Marital Status')
            ->setCellValue('K4', 'Position');

        $spreadsheet->getActiveSheet()->getStyle("A4:K4")->applyFromArray($outlineBorderStyle);
        /* END TITLE BIODATA ID */

        $rowIdx = 5;
        $rowNo = 0;
        foreach ($query as $row) {
            $rowIdx++;
            $rowNo++;
            $spreadsheet->getActiveSheet()
                ->setCellValue('A'.($rowIdx), $rowNo)
                ->setCellValue('B'.($rowIdx), $row['bio_rec_id'])
                ->setCellValue('C'.($rowIdx), $row['full_name'])
                ->setCellValue('D'.($rowIdx), $row['date_of_birth'])
                ->setCellValue('E'.($rowIdx), $row['cell_no'])
                ->setCellValue('F'.($rowIdx), $row['id_card_address'])
                ->setCellValue('G'.($rowIdx), $row['city_address'])
                ->setCellValue('H'.($rowIdx), $row['nationality'])
                ->setCellValue('I'.($rowIdx), $row['religion'])
                ->setCellValue('J'.($rowIdx), $row['marital_status'])
                ->setCellValue('K'.($rowIdx), $row['position']);
        
            if($rowIdx % 2 == 1)
            {
                $spreadsheet->getActiveSheet()->getStyle('A'.$rowIdx.':K'.$rowIdx)
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('EAEBAF');             
            } 
        } /* end foreach ($query as $row) */ 

        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":K".($rowIdx+2))
        ->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()
        ->setRGB('F2BE6B');

        unset($allBorderStyle);
        unset($center);
        unset($right);
        unset($left);

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        if($sPosition!=''){
            $sPosition      = $sPosition;
        }else{
            $sPosition      = '-';
        }

        $str = 'Biodata'.$sPosition.'MaxAges'.$sAge;
        $fileName = preg_replace('/\s+/', '', $str);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.Xlsx"');
        // header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        /* BY COMPOSER */
        // $writer = new Xlsx($spreadsheet);
        /* OFFLINE/ BY COPY EXCEL FOLDER  */
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit(0);

    }





































    // public function getSummaryPayroll(){
    //     $pt = "";
    //     $year = "";
    //     $month = "";
    //     if(isset($_POST['pt']))
    //     {
    //         $pt = $_POST['pt'];
    //     }
    //     if(isset($_POST['year']))
    //     {
    //         $year = $_POST['year'];
    //     }
    //     if(isset($_POST['month']))
    //     {
    //         $month = $_POST['month'];
    //     }
     
    //     $myData = array();
    //     if($pt=='Pontil_Timika'){
    //         $strSQL = " SELECT ss.bio_rec_id,ss.year_period,ss.month_period,ms.payroll_group,ms.nie,ss.name,mb.npwp_no,ss.marital_status,mb.local_foreign AS Etnic,
    //                 ms.salary_level,ss.basic_salary,ss.bs_prorate,(ss.ot_1+ss.ot_2+ss.ot_3+ss.ot_4 )AS ot_total,ss.travel_bonus,ss.allowance_economy,
    //                 ss.incentive_bonus,ss.shift_bonus,ss.workday_adj,ss.adjust_in,ss.adjust_out,ss.thr,ss.cc_payment,ss.production_bonus,ss.jkk_jkm,
    //                 ss.health_bpjs,ss.unpaid_total,
    //                 (ss.bs_prorate+ss.ot_1+ss.ot_2+ss.ot_3+ss.ot_4+ss.travel_bonus+ss.allowance_economy+ss.incentive_bonus+ss.shift_bonus+ss.thr+ss.cc_payment+
    //                    ss.production_bonus+ss.workday_adj+ss.jkk_jkm+ss.health_bpjs-ss.unpaid_total+ss.adjust_in-ss.adjust_out) AS Gross,   
    //                 ss.tax_value,ss.jkk_jkm,ss.health_bpjs,ss.emp_health_bpjs,ss.emp_jht,ss.emp_jp,ss.thr,ss.cc_payment,ss.debt_burden,
    //                 (ss.bs_prorate+ss.ot_1+ss.ot_2+ss.ot_3+ss.ot_4+ss.travel_bonus+ss.allowance_economy+ss.incentive_bonus+ss.shift_bonus+ss.thr+
    //                 ss.cc_payment+ss.production_bonus+ss.workday_adj+ss.adjust_in-ss.adjust_out+ss.jkk_jkm+ss.health_bpjs-ss.unpaid_total-ss.tax_value-
    //                 ss.jkk_jkm-ss.emp_health_bpjs-ss.health_bpjs-ss.emp_jht-ss.emp_jp-ss.thr-ss.cc_payment-debt_burden) AS Net_payment
   
    //                 FROM db_recruitment.trn_salary_slip ss,db_recruitment.mst_bio_rec mb,db_recruitment.mst_salary ms
    //                 WHERE mb.bio_rec_id = ss.bio_rec_id AND mb.bio_rec_id = ms.bio_rec_id AND ss.client_name = 'Pontil_Timika'
    //                 AND ss.month_period = '".$month."'
    //                 AND ss.year_period = '".$year."'                       
    //                 ORDER BY 
    //                    ss.client_name, 
    //                    ss.year_period,
    //                    ss.month_period, 
    //                    ms.payroll_group,
    //                    ss.name ";

    //         // test($strSQL,1);
    //         $query = $this->db->query($strSQL)->result_array();

    //         foreach ($query as $key => $row) {
    //             $myData[] = array(
    //                 $row['bio_rec_id'],         
    //                 $row['name'],         
    //                 $row['nie'],         
    //                 $row['basic_salary'],         
    //                 $row['ot_total'],         
    //                 $row['travel_bonus'],         
    //                 $row['allowance_economy'],         
    //                 $row['incentive_bonus'],         
    //                 $row['shift_bonus'],         
    //                 $row['adjust_in'],         
    //                 $row['adjust_out'],         
    //                 $row['thr'],         
    //                 $row['cc_payment'],         
    //                 $row['production_bonus'],         
    //                 $row['Gross'],         
    //                 $row['Net_payment']         
    //             );            
    //         }  
    //     }
    //     else if($pt=='Redpath_Timika'){
    //         $strSQL = " SELECT ss.bio_rec_id,ss.year_period,ss.month_period,ms.`payroll_group`,ms.`nie`,ss.name,mb.`local_foreign` AS Etnic,ss.`marital_status`,
    //                     mb.`id_card_no`,mb.npwp_no,ms.`salary_level`,ss.`basic_salary`,ss.`bs_prorate`,(ss.ot_1+ss.ot_2+ss.ot_3+ss.ot_4 )AS ot_total,ss.`ot_bonus`,
    //                     ss.`shift_bonus`,ss.`remote_allowance`,ss.`thr`,ss.`dept`,(ss.`dev_incentive_bonus`*(`dev_percent`/100))AS dev_bonus,ss.`dev_percent` AS PVB,
    //                     ss.`workday_adj`,ss.`contract_bonus`,ss.`other_allowance2` AS Jumbo_Bns,ss.`jkk_jkm`,ss.`health_bpjs`,ss.`unpaid_total`,ss.`adjust_in`,
    //                     ss.`adjust_out`,
    //                     (ss.`bs_prorate`+ss.`ot_1`+ss.`ot_2`+ss.`ot_3`+ss.`ot_4`+ss.`ot_bonus`+ss.`remote_allowance`+ss.`shift_bonus`+ss.`thr`+
    //                     (ss.`dev_incentive_bonus`*(`dev_percent`/100))+ss.`contract_bonus`+ss.`workday_adj` +ss.`other_allowance2` +ss.`jkk_jkm`+ss.`health_bpjs`+
    //                     ss.`adjust_in`-ss.`adjust_out`-ss.`unpaid_total`) AS Gross,
    //                     ss.`tax_value`,ss.`jkk_jkm`,ss.`health_bpjs`,ss.`emp_health_bpjs`,ss.`emp_jht`,ss.`emp_jp`,ss.`thr`,ss.`contract_bonus`,ss.`debt_burden`,
    //                     (ss.`bs_prorate`+ss.`ot_1`+ss.`ot_2`+ss.`ot_3`+ss.`ot_4`+ss.`ot_bonus`+ss.`remote_allowance`+ss.`shift_bonus`+ss.`thr`+
    //                     (ss.`dev_incentive_bonus`*(ss.`dev_percent`/100))+ss.`contract_bonus`+ss.`workday_adj`+ss.`other_allowance2`+ss.`jkk_jkm`+ss.`health_bpjs`- 
    //                     ss.`unpaid_total`+ss.`adjust_in`-ss.`adjust_out`-ss.`tax_value`-ss.`jkk_jkm`-ss.`emp_health_bpjs`-ss.`health_bpjs`-ss.`emp_jht`-ss.`emp_jp`-
    //                     ss.`thr`-ss.`contract_bonus`-ss.`debt_burden`) AS Net_payment
    //                     FROM `db_recruitment`.trn_salary_slip ss,`db_recruitment`.mst_bio_rec mb,`db_recruitment`.mst_salary ms
    //                     WHERE 
    //                     mb.bio_rec_id = ss.bio_rec_id AND mb.bio_rec_id = ms.bio_rec_id AND ss.client_name = 'Redpath_Timika'
    //                     AND ss.month_period = '".$month."' AND ss.year_period = '".$year."'
    //                     ORDER BY ss.client_name, ss.year_period, ss.month_period, ms.`payroll_group`,ss.name";

    //         $query = $this->db->query($strSQL)->result_array();

    //         foreach ($query as $key => $row) {
    //             // test($row,1);
    //             $myData[] = array(
    //                 $row['bio_rec_id'],         
    //                 $row['name'],         
    //                 $row['nie'],         
    //                 $row['basic_salary'],         
    //                 $row['ot_total'],         
    //                 $row['ot_bonus'],         
    //                 $row['remote_allowance'],         
    //                 $row['dev_bonus'],         
    //                 $row['shift_bonus'],         
    //                 $row['contract_bonus'],         
    //                 $row['Jumbo_Bns'],         
    //                 $row['adjust_in'],         
    //                 $row['adjust_out'],         
    //                 $row['thr'],         
    //                 $row['PVB'],             
    //                 $row['debt_burden'],         
    //                 $row['Gross'],         
    //                 $row['Net_payment']         
    //             );            
    //         } 
    //     }
    //     else if($pt=='Pontil_Sumbawa'){
    //         $strSQL = " SELECT ss.*,ms.nie,(ss.ot_1+ss.ot_2+ss.ot_3+ss.ot_4)AS ot_total,
    //         (ss.bs_prorate+ss.ot_1+ss.ot_2+ss.ot_3+ss.ot_4+ss.travel_bonus+ss.flying_camp+ss.attendance_bonus+ss.thr+
    //         ss.contract_bonus+ss.drilling_bonus+ss.act_manager_bonus+ss.jkk_jkm+ss.health_bpjs-ss.unpaid_total+ss.adjust_in-
    //         ss.adjust_out) AS Gross_Payment,(ss.bs_prorate+ss.ot_1+ss.ot_2+ss.ot_3+ss.ot_4+
    //         ss.travel_bonus+ss.flying_camp+ss.attendance_bonus+ss.thr+ss.contract_bonus+ss.drilling_bonus+ss.act_manager_bonus+
    //         ss.jkk_jkm+ss.health_bpjs-ss.unpaid_total+ss.adjust_in-ss.adjust_out-ss.tax_value-ss.jkk_jkm-ss.health_bpjs-
    //         ss.emp_health_bpjs-ss.emp_jht-ss.emp_jp-ss.thr-ss.contract_bonus-ss.drilling_bonus+ss.workday_adj-ss.debt_burden) 
    //         AS Net_payment 
    //         FROM db_recruitment.trn_slip_ptlsmb ss, db_recruitment.mst_bio_rec mb, db_recruitment.mst_salary ms
    //         WHERE mb.bio_rec_id = ss.bio_rec_id 
    //         AND mb.bio_rec_id = ms.bio_rec_id 
    //         AND ss.client_name = '".$pt."' 
    //         AND ss.month_period = '".$month."' AND ss.year_period = '".$year."'
    //         ORDER BY ss.client_name, ss.year_period, ss.month_period,
    //         ms.payroll_group, ss.name";
    //         $query = $this->db->query($strSQL)->result_array();

    //         foreach ($query as $key => $row) {
    //             $myData[] = array(
    //                 $row['bio_rec_id'],         
    //                 $row['name'],         
    //                 $row['nie'],         
    //                 $row['basic_salary'],         
    //                 $row['ot_total'],         
    //                 $row['travel_bonus'],           
    //                 $row['incentive_bonus'],         
    //                 $row['shift_bonus'],         
    //                 $row['adjust_in'],         
    //                 $row['adjust_out'],         
    //                 $row['thr'],                 
    //                 $row['production_bonus'],         
    //                 $row['Gross_Payment'],         
    //                 $row['Net_payment']         
    //             );            
    //         }  
    //     }
    //     echo json_encode($myData);  
    // }

    // public function exportSummaryPayrollPontilSwq($ptName, $yearPeriod, $monthPeriod)
    // {
    //     // Create new Spreadsheet object
    //     $spreadsheet = new Spreadsheet();

    //     $spreadsheet->getProperties()->setCreator('Maurice - Web - Android')
    //         ->setLastModifiedBy('Maurice - Web - Android')
    //         ->setTitle('Office 2007 XLSX Test Document')
    //         ->setSubject('Office 2007 XLSX Test Document')
    //         ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
    //         ->setKeywords('office 2007 openxml php')
    //         ->setCategory('Test result file');

    //     $boldFont = [
    //         'font' => [
    //             'bold' => true
    //             // 'color' => ['argb' => '0000FF'],
    //         ],
    //     ];

    //     $totalStyle = [
    //         'font' => [
    //             'bold' => true,
    //             'color' => ['argb' => '0000FF'],
    //         ],
    //     ];

    //     $allBorderStyle = [
    //         'borders' => [
    //             'allBorders' => [
    //                 'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
    //                 'color' => ['argb' => '00000000'],
    //             ],
    //         ],
    //     ];

    //     $outlineBorderStyle = [
    //         'borders' => [
    //             'outline' => [
    //                 'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
    //                 'color' => ['argb' => '00000000'],
    //             ],
    //         ],
    //     ];

    //     $topBorderStyle = [
    //         'borders' => [
    //             'top' => [
    //                 'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
    //                 'color' => ['argb' => '00000000'],
    //             ],
    //         ],
    //     ];

    //     $bottomBorderStyle = [
    //         'borders' => [
    //             'bottom' => [
    //                 'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
    //                 'color' => ['argb' => '00000000'],
    //             ],
    //         ],
    //     ];

    //     $center = array();
    //     $center['alignment'] = array();
    //     $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
    //     $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

    //     $right = array();
    //     $right['alignment'] = array();
    //     $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
    //     $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

    //     $left = array();
    //     $left['alignment'] = array();
    //     $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
    //     $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

    //     // $ym = (string)$yearPeriod.'-'.$monthPeriod;

    //     $strSQL = " SELECT ss.*,ms.nie,(ss.ot_1+ss.ot_2+ss.ot_3+ss.ot_4)AS ot_total,
    //         (ss.bs_prorate+ss.ot_1+ss.ot_2+ss.ot_3+ss.ot_4+ss.travel_bonus+ss.flying_camp+ss.attendance_bonus+ss.thr+
    //         ss.contract_bonus+ss.drilling_bonus+ss.act_manager_bonus+ss.jkk_jkm+ss.health_bpjs-ss.unpaid_total+ss.adjust_in-
    //         ss.adjust_out) AS Gross_Payment,(ss.bs_prorate+ss.ot_1+ss.ot_2+ss.ot_3+ss.ot_4+
    //         ss.travel_bonus+ss.flying_camp+ss.attendance_bonus+ss.thr+ss.contract_bonus+ss.drilling_bonus+ss.act_manager_bonus+
    //         ss.jkk_jkm+ss.health_bpjs-ss.unpaid_total+ss.adjust_in-ss.adjust_out-ss.tax_value-ss.jkk_jkm-ss.health_bpjs-
    //         ss.emp_health_bpjs-ss.emp_jht-ss.emp_jp-ss.thr-ss.contract_bonus-ss.drilling_bonus+ss.workday_adj-ss.debt_burden) 
    //         AS Net_payment 
    //         FROM db_recruitment.trn_slip_ptlsmb ss, db_recruitment.mst_bio_rec mb, db_recruitment.mst_salary ms
    //         WHERE mb.bio_rec_id = ss.bio_rec_id 
    //         AND mb.bio_rec_id = ms.bio_rec_id 
    //         AND ss.client_name = '".$ptName."' 
    //         AND ss.month_period = '".$monthPeriod."' AND ss.year_period = '".$yearPeriod."'
    //         ORDER BY ss.client_name, ss.year_period, ss.month_period,
    //         ms.payroll_group, ss.name";
    //         $query = $this->db->query($strSQL)->result_array();

    //     // Add some data
    //     $spreadsheet->getActiveSheet()->getStyle('A6:AI7')
    //         ->getFill()
    //         ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    //         ->getStartColor()
    //         ->setRGB('F2BE6B'); 

    //     $spreadsheet->getActiveSheet()
    //         ->setCellValue('A1', 'PT. SANGATI SOERYA SEJAHTERA')
    //         ->setCellValue('A2', 'SUMMARY PAYROLL PT.'.$ptName.'')
    //         ->setCellValue('A4', 'Periode : '.$monthPeriod.'-'.$yearPeriod);

    //     $spreadsheet->getActiveSheet()->getStyle("A1:D1")->getFont()->setBold(true)->setSize(16);
    //     $spreadsheet->getActiveSheet()->getStyle("A2:D2")->getFont()->setBold(true)->setSize(14)->setUnderline(true);
    //     $spreadsheet->getActiveSheet()->getStyle("A4:G4")->getFont()->setBold(true)->setSize(12);
    //     $spreadsheet->getActiveSheet()->getStyle("A6:AI7")->getFont()->setBold(true)->setSize(12);

    //     $spreadsheet->getActiveSheet()->mergeCells("A6:A7");
    //     $spreadsheet->getActiveSheet()->mergeCells("B6:B7");
    //     $spreadsheet->getActiveSheet()->mergeCells("C6:C7");
    //     $spreadsheet->getActiveSheet()->mergeCells("D6:D7");
    //     $spreadsheet->getActiveSheet()->mergeCells("E6:E7");
    //     $spreadsheet->getActiveSheet()->mergeCells("F6:F7");
    //     $spreadsheet->getActiveSheet()->mergeCells("G6:G7");
    //     $spreadsheet->getActiveSheet()->mergeCells("H6:H7");
    //     $spreadsheet->getActiveSheet()->mergeCells("I6:I7");
    //     $spreadsheet->getActiveSheet()->mergeCells("J6:J7");
    //     $spreadsheet->getActiveSheet()->mergeCells("K6:K7");
    //     $spreadsheet->getActiveSheet()->mergeCells("L6:L7");
    //     $spreadsheet->getActiveSheet()->mergeCells("M6:M7");
    //     $spreadsheet->getActiveSheet()->mergeCells("N6:N7");
    //     $spreadsheet->getActiveSheet()->mergeCells("O6:O7");
    //     $spreadsheet->getActiveSheet()->mergeCells("P6:P7");
    //     $spreadsheet->getActiveSheet()->mergeCells("Q6:Q7");
    //     $spreadsheet->getActiveSheet()->mergeCells("R6:R7");
    //     $spreadsheet->getActiveSheet()->mergeCells("S6:S7");
    //     $spreadsheet->getActiveSheet()->mergeCells("T6:T7");
    //     $spreadsheet->getActiveSheet()->mergeCells("U6:U7");
    //     $spreadsheet->getActiveSheet()->mergeCells("V6:V7");
    //     $spreadsheet->getActiveSheet()->mergeCells("W6:W7");
    //     $spreadsheet->getActiveSheet()->mergeCells("X6:X7");
    //     $spreadsheet->getActiveSheet()->mergeCells("Y6:Y7");
    //     $spreadsheet->getActiveSheet()->mergeCells("Z6:Z7");
    //     $spreadsheet->getActiveSheet()->mergeCells("AA6:AA7");
    //     $spreadsheet->getActiveSheet()->mergeCells("AB6:AB7");
    //     $spreadsheet->getActiveSheet()->mergeCells("AC6:AC7");
    //     $spreadsheet->getActiveSheet()->mergeCells("AD6:AD7");
    //     $spreadsheet->getActiveSheet()->mergeCells("AE6:AE7");
    //     $spreadsheet->getActiveSheet()->mergeCells("AF6:AF7");
    //     $spreadsheet->getActiveSheet()->mergeCells("AG6:AG7");
    //     $spreadsheet->getActiveSheet()->mergeCells("AH6:AH7");
    //     $spreadsheet->getActiveSheet()->mergeCells("AI6:AI7");
            
    //     $spreadsheet->getActiveSheet()->getStyle("A6:AI7")->applyFromArray($allBorderStyle);
    //     $spreadsheet->getActiveSheet()->getStyle("A6:AI7")->applyFromArray($center);
    //     $spreadsheet->getActiveSheet()
    //         ->setCellValue('A6', 'NO')
    //         ->setCellValue('B6', 'YEAR')
    //         ->setCellValue('C6', 'MONTH')
    //         ->setCellValue('D6', 'PAYROLL GROUP')
    //         ->setCellValue('E6', 'NAMA')
    //         ->setCellValue('F6', 'EXTERNAL ID')
    //         ->setCellValue('G6', 'MARTIAL STATUS')
    //         ->setCellValue('H6', 'BASIC SALARY')
    //         ->setCellValue('I6', 'BASIC SALARY PRORATE')
    //         ->setCellValue('J6', 'OVERTIME TOTAL')
    //         ->setCellValue('K6', 'TRAVEL BONUS')
    //         ->setCellValue('L6', 'FLYING CAMP')
    //         ->setCellValue('M6', 'ATTENDANCE BONUS')
    //         ->setCellValue('N6', 'THR')
    //         ->setCellValue('O6', 'CONTRACT BONUS')
    //         ->setCellValue('P6', 'DRILLING BONUS')
    //         ->setCellValue('Q6', 'ACT MANAGER BONUS')
    //         ->setCellValue('R6', 'ADJUSTMENT WORK DAY')
    //         ->setCellValue('S6', 'ADJUSTMENT IN')
    //         ->setCellValue('T6', 'ADJUSTMENT OUT')
    //         ->setCellValue('U6', 'THR')
    //         ->setCellValue('V6', 'PRODUCTION BONUS')
    //         ->setCellValue('W6', 'JKK & JKM')
    //         ->setCellValue('X6', 'HEALTH BPJS')
    //         ->setCellValue('Y6', 'UNPAID TOTAL')
    //         ->setCellValue('Z6', 'GROSS')
    //         ->setCellValue('AA6', 'TAX VALUE')
    //         ->setCellValue('AB6', 'JKK & JKM')
    //         ->setCellValue('AC6', 'HEALTH BPJS')
    //         ->setCellValue('AD6', 'EMP HEALTH BPJS')
    //         ->setCellValue('AE6', 'EMP JHT')
    //         ->setCellValue('AF6', 'EMP JP')
    //         ->setCellValue('AG6', 'THR')
    //         ->setCellValue('AH6', 'DEBT BURDEN')
    //         ->setCellValue('AI6', 'NET PAYMENT');

    //     /* START GET DAYS TOTAL BY ROSTER */
    //     $rowIdx = 7;
    //     $rowNo = 0;
    //     foreach ($query as $row) {                      
    //         $rowIdx++;
    //          $rowNo++;
    //         $spreadsheet->getActiveSheet()
    //             ->setCellValue('A'.($rowIdx), $rowNo)
    //             ->setCellValue('B'.($rowIdx), $row['year_period'])
    //             ->setCellValue('C'.($rowIdx), $row['month_period'])
    //             ->setCellValue('D'.($rowIdx), $row['payroll_group'])
    //             ->setCellValue('E'.($rowIdx), $row['name'])
    //             ->setCellValue('F'.($rowIdx), $row['nie'])
    //             ->setCellValue('G'.($rowIdx), $row['marital_status'])
    //             ->setCellValue('H'.($rowIdx), $row['basic_salary'])
    //             ->setCellValue('I'.($rowIdx), $row['bs_prorate'])
    //             ->setCellValue('J'.($rowIdx), $row['ot_total'])
    //             ->setCellValue('K'.($rowIdx), $row['travel_bonus'])
    //             ->setCellValue('L'.($rowIdx), $row['flying_camp'])
    //             ->setCellValue('M'.($rowIdx), $row['attendance_bonus'])
    //             ->setCellValue('N'.($rowIdx), $row['thr'])
    //             ->setCellValue('O'.($rowIdx), $row['contract_bonus'])
    //             ->setCellValue('P'.($rowIdx), $row['drilling_bonus'])
    //             ->setCellValue('Q'.($rowIdx), $row['act_manager_bonus'])
    //             ->setCellValue('R'.($rowIdx), $row['workday_adj'])
    //             ->setCellValue('S'.($rowIdx), $row['adjust_in'])
    //             ->setCellValue('T'.($rowIdx), $row['adjust_out'])
    //             ->setCellValue('U'.($rowIdx), $row['thr'])
    //             ->setCellValue('V'.($rowIdx), $row['production_bonus'])
    //             ->setCellValue('W'.($rowIdx), $row['jkk_jkm'])
    //             ->setCellValue('X'.($rowIdx), $row['health_bpjs'])
    //             ->setCellValue('Y'.($rowIdx), $row['unpaid_total'])
    //             ->setCellValue('Z'.($rowIdx), $row['Gross_Payment'])
    //             ->setCellValue('AA'.($rowIdx), $row['tax_value'])
    //             ->setCellValue('AB'.($rowIdx), $row['jkk_jkm'])
    //             ->setCellValue('AC'.($rowIdx), $row['health_bpjs'])
    //             ->setCellValue('AD'.($rowIdx), $row['emp_health_bpjs'])
    //             ->setCellValue('AE'.($rowIdx), $row['emp_jht'])
    //             ->setCellValue('AF'.($rowIdx), $row['emp_jp'])
    //             ->setCellValue('AG'.($rowIdx), $row['thr'])
    //             ->setCellValue('AH'.($rowIdx), $row['debt_burden'])
    //             ->setCellValue('AI'.($rowIdx), $row['Net_payment'])
    //             ;     

    //         /* SET ROW COLOR */
    //         if($rowIdx % 2 == 1)
    //         {
    //             $spreadsheet->getActiveSheet()->getStyle('A'.$rowIdx.':AI'.$rowIdx)
    //             ->getFill()
    //             ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    //             ->getStartColor()
    //             ->setRGB('EAEBAF');             
    //         } 
    //     }

    //     $spreadsheet->getActiveSheet()->getStyle("A6:AI".($rowIdx))->applyFromArray($allBorderStyle);
    //     // Rename worksheet
    //     $spreadsheet->getActiveSheet()->setTitle('Report Excel '.date('d-m-Y H'));

    //     // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    //     $spreadsheet->setActiveSheetIndex(0);

    //     $str = 'Summary Payroll PT.Pontil_Sumbawa';
    //     $fileName = preg_replace('/\s+/', '', $str);
    //     // $str = 'PTLSmbInvoice';
    //     // $fileName = 'Summary Payroll PT.'.$ptName.'';
    //     // test($fileName,1);
    //     // Redirect output to a client’s web browser (Xlsx)
    //     header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    //     // header('Content-Disposition: attachment;filename="Summary Payroll PT.'.$ptName.'.Xlsx"');
    //     header('Content-Disposition: attachment;filename="'.$fileName.'.Xlsx"');
    //     // header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
    //     header('Cache-Control: max-age=0');
    //     // If you're serving to IE 9, then the following may be needed
    //     header('Cache-Control: max-age=1');

    //     // If you're serving to IE over SSL, then the following may be needed
    //     header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    //     header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    //     header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    //     header('Pragma: public'); // HTTP/1.0
    //     /* BY COMPOSER */
    //     // $writer = new Xlsx($spreadsheet);
    //     /* OFFLINE/ BY COPY EXCEL FOLDER  */
    //     $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
    //     $writer->save('php://output');
    //     exit(0); 
    // }    

    // public function exportSummaryPayrollPontil($ptName, $yearPeriod, $monthPeriod)
    // {
    //     // Create new Spreadsheet object
    //     $spreadsheet = new Spreadsheet();

    //     $spreadsheet->getProperties()->setCreator('Maurice - Web - Android')
    //         ->setLastModifiedBy('Maurice - Web - Android')
    //         ->setTitle('Office 2007 XLSX Test Document')
    //         ->setSubject('Office 2007 XLSX Test Document')
    //         ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
    //         ->setKeywords('office 2007 openxml php')
    //         ->setCategory('Test result file');

    //     $boldFont = [
    //         'font' => [
    //             'bold' => true
    //             // 'color' => ['argb' => '0000FF'],
    //         ],
    //     ];

    //     $totalStyle = [
    //         'font' => [
    //             'bold' => true,
    //             'color' => ['argb' => '0000FF'],
    //         ],
    //     ];

    //     $allBorderStyle = [
    //         'borders' => [
    //             'allBorders' => [
    //                 'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
    //                 'color' => ['argb' => '00000000'],
    //             ],
    //         ],
    //     ];

    //     $outlineBorderStyle = [
    //         'borders' => [
    //             'outline' => [
    //                 'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
    //                 'color' => ['argb' => '00000000'],
    //             ],
    //         ],
    //     ];

    //     $topBorderStyle = [
    //         'borders' => [
    //             'top' => [
    //                 'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
    //                 'color' => ['argb' => '00000000'],
    //             ],
    //         ],
    //     ];

    //     $bottomBorderStyle = [
    //         'borders' => [
    //             'bottom' => [
    //                 'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
    //                 'color' => ['argb' => '00000000'],
    //             ],
    //         ],
    //     ];

    //     $center = array();
    //     $center['alignment'] = array();
    //     $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
    //     $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

    //     $right = array();
    //     $right['alignment'] = array();
    //     $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
    //     $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

    //     $left = array();
    //     $left['alignment'] = array();
    //     $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
    //     $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

    //     // $ym = (string)$yearPeriod.'-'.$monthPeriod;

    //     $strSQL = " SELECT ss.bio_rec_id,ss.year_period,ss.month_period,ms.payroll_group,ms.nie,ss.name,mb.npwp_no,ss.marital_status,mb.local_foreign AS Etnic,
    //         ms.salary_level,ss.basic_salary,ss.bs_prorate,(ss.ot_1+ss.ot_2+ss.ot_3+ss.ot_4 )AS ot_total,ss.travel_bonus,ss.allowance_economy,
    //         ss.incentive_bonus,ss.shift_bonus,ss.workday_adj,ss.adjust_in,ss.adjust_out,ss.thr,ss.cc_payment,ss.production_bonus,ss.jkk_jkm,
    //         ss.health_bpjs,ss.unpaid_total,
    //         (ss.bs_prorate+ss.ot_1+ss.ot_2+ss.ot_3+ss.ot_4+ss.travel_bonus+ss.allowance_economy+ss.incentive_bonus+ss.shift_bonus+ss.thr+ss.cc_payment+
    //            ss.production_bonus+ss.workday_adj+ss.jkk_jkm+ss.health_bpjs-ss.unpaid_total+ss.adjust_in-ss.adjust_out) AS Gross,   
    //         ss.tax_value,ss.jkk_jkm,ss.health_bpjs,ss.emp_health_bpjs,ss.emp_jht,ss.emp_jp,ss.thr,ss.cc_payment,ss.debt_burden,
    //         (ss.bs_prorate+ss.ot_1+ss.ot_2+ss.ot_3+ss.ot_4+ss.travel_bonus+ss.allowance_economy+ss.incentive_bonus+ss.shift_bonus+ss.thr+
    //         ss.cc_payment+ss.production_bonus+ss.workday_adj+ss.adjust_in-ss.adjust_out+ss.jkk_jkm+ss.health_bpjs-ss.unpaid_total-ss.tax_value-
    //         ss.jkk_jkm-ss.emp_health_bpjs-ss.health_bpjs-ss.emp_jht-ss.emp_jp-ss.thr-ss.cc_payment-debt_burden) AS Net_payment

    //         FROM db_recruitment.trn_salary_slip ss,db_recruitment.mst_bio_rec mb,db_recruitment.mst_salary ms
    //         WHERE mb.bio_rec_id = ss.bio_rec_id AND mb.bio_rec_id = ms.bio_rec_id AND ss.client_name = 'Pontil_Timika'
    //         AND ss.month_period = '".$monthPeriod."'
    //         AND ss.year_period = '".$yearPeriod."'                       
    //         ORDER BY 
    //            ss.client_name, 
    //            ss.year_period,
    //            ss.month_period, 
    //            ms.payroll_group,
    //            ss.name ";
    //     $query = $this->db->query($strSQL)->result_array();

    //     // Add some data
    //     $spreadsheet->getActiveSheet()->getStyle('A6:AH7')
    //         ->getFill()
    //         ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    //         ->getStartColor()
    //         ->setRGB('F2BE6B'); 

    //     $spreadsheet->getActiveSheet()
    //         ->setCellValue('A1', 'PT. SANGATI SOERYA SEJAHTERA')
    //         ->setCellValue('A2', 'SUMMARY PAYROLL PT.'.$ptName.'')
    //         ->setCellValue('A4', 'Periode : '.$monthPeriod.'-'.$yearPeriod);

    //     $spreadsheet->getActiveSheet()->getStyle("A1:D1")->getFont()->setBold(true)->setSize(16);
    //     $spreadsheet->getActiveSheet()->getStyle("A2:D2")->getFont()->setBold(true)->setSize(14)->setUnderline(true);
    //     $spreadsheet->getActiveSheet()->getStyle("A4:G4")->getFont()->setBold(true)->setSize(12);
    //     $spreadsheet->getActiveSheet()->getStyle("A6:AH7")->getFont()->setBold(true)->setSize(12);

    //     $spreadsheet->getActiveSheet()->mergeCells("A6:A7");
    //     $spreadsheet->getActiveSheet()->mergeCells("B6:B7");
    //     $spreadsheet->getActiveSheet()->mergeCells("C6:C7");
    //     $spreadsheet->getActiveSheet()->mergeCells("D6:D7");
    //     $spreadsheet->getActiveSheet()->mergeCells("E6:E7");
    //     $spreadsheet->getActiveSheet()->mergeCells("F6:F7");
    //     $spreadsheet->getActiveSheet()->mergeCells("G6:G7");
    //     $spreadsheet->getActiveSheet()->mergeCells("H6:H7");
    //     $spreadsheet->getActiveSheet()->mergeCells("I6:I7");
    //     $spreadsheet->getActiveSheet()->mergeCells("J6:J7");
    //     $spreadsheet->getActiveSheet()->mergeCells("K6:K7");
    //     $spreadsheet->getActiveSheet()->mergeCells("L6:L7");
    //     $spreadsheet->getActiveSheet()->mergeCells("M6:M7");
    //     $spreadsheet->getActiveSheet()->mergeCells("N6:N7");
    //     $spreadsheet->getActiveSheet()->mergeCells("O6:O7");
    //     $spreadsheet->getActiveSheet()->mergeCells("P6:P7");
    //     $spreadsheet->getActiveSheet()->mergeCells("Q6:Q7");
    //     $spreadsheet->getActiveSheet()->mergeCells("R6:R7");
    //     $spreadsheet->getActiveSheet()->mergeCells("S6:S7");
    //     $spreadsheet->getActiveSheet()->mergeCells("T6:T7");
    //     $spreadsheet->getActiveSheet()->mergeCells("U6:U7");
    //     $spreadsheet->getActiveSheet()->mergeCells("V6:V7");
    //     $spreadsheet->getActiveSheet()->mergeCells("W6:W7");
    //     $spreadsheet->getActiveSheet()->mergeCells("X6:X7");
    //     $spreadsheet->getActiveSheet()->mergeCells("Y6:Y7");
    //     $spreadsheet->getActiveSheet()->mergeCells("Z6:Z7");
    //     $spreadsheet->getActiveSheet()->mergeCells("AA6:AA7");
    //     $spreadsheet->getActiveSheet()->mergeCells("AB6:AB7");
    //     $spreadsheet->getActiveSheet()->mergeCells("AC6:AC7");
    //     $spreadsheet->getActiveSheet()->mergeCells("AD6:AD7");
    //     $spreadsheet->getActiveSheet()->mergeCells("AE6:AE7");
    //     $spreadsheet->getActiveSheet()->mergeCells("AF6:AF7");
    //     $spreadsheet->getActiveSheet()->mergeCells("AG6:AG7");
    //     $spreadsheet->getActiveSheet()->mergeCells("AH6:AH7");
            
    //     $spreadsheet->getActiveSheet()->getStyle("A6:AH7")->applyFromArray($allBorderStyle);
    //     $spreadsheet->getActiveSheet()->getStyle("A6:AH7")->applyFromArray($center);
    //     $spreadsheet->getActiveSheet()
    //         ->setCellValue('A6', 'NO')
    //         ->setCellValue('B6', 'YEAR')
    //         ->setCellValue('C6', 'MONTH')
    //         ->setCellValue('D6', 'PAYROLL GROUP')
    //         ->setCellValue('E6', 'NAMA')
    //         ->setCellValue('F6', 'EXTERNAL ID')
    //         ->setCellValue('G6', 'MARTIAL STATUS')
    //         ->setCellValue('H6', 'BASIC SALARY')
    //         ->setCellValue('I6', 'BASIC SALARY PRORATE')
    //         ->setCellValue('J6', 'OVERTIME TOTAL')
    //         ->setCellValue('K6', 'TRAVEL BONUS')
    //         ->setCellValue('L6', 'ALLOWANCE ECONOMY')
    //         ->setCellValue('M6', 'INCENTIVE BONUS')
    //         ->setCellValue('N6', 'SHIFT BONUS')
    //         ->setCellValue('O6', 'ADJUSTMENT WORK DAY')
    //         ->setCellValue('P6', 'ADJUSTMENT IN')
    //         ->setCellValue('Q6', 'ADJUSTMENT OUT')
    //         ->setCellValue('R6', 'THR')
    //         ->setCellValue('S6', 'CC PAYMENT')
    //         ->setCellValue('T6', 'PRODUCTION BONUS')
    //         ->setCellValue('U6', 'JKK & JKM')
    //         ->setCellValue('V6', 'HEALTH BPJS')
    //         ->setCellValue('W6', 'UNPAID TOTAL')
    //         ->setCellValue('X6', 'GROSS')
    //         ->setCellValue('Y6', 'TAX VALUE')
    //         ->setCellValue('Z6', 'JKK & JKM')
    //         ->setCellValue('AA6', 'HEALTH BPJS')
    //         ->setCellValue('AB6', 'EMP HEALTH BPJS')
    //         ->setCellValue('AC6', 'EMP JHT')
    //         ->setCellValue('AD6', 'EMP JP')
    //         ->setCellValue('AE6', 'THR')
    //         ->setCellValue('AF6', 'CC PAYMENT')
    //         ->setCellValue('AG6', 'DEBT BURDEN')
    //         ->setCellValue('AH6', 'NET PAYMENT');

    //     /* START GET DAYS TOTAL BY ROSTER */
    //     $rowIdx = 7;
    //     $rowNo = 0;
    //     foreach ($query as $row) {                      
    //         $rowIdx++;
    //          $rowNo++;
    //         $spreadsheet->getActiveSheet()
    //             ->setCellValue('A'.($rowIdx), $rowNo)
    //             ->setCellValue('B'.($rowIdx), $row['year_period'])
    //             ->setCellValue('C'.($rowIdx), $row['month_period'])
    //             ->setCellValue('D'.($rowIdx), $row['payroll_group'])
    //             ->setCellValue('E'.($rowIdx), $row['name'])
    //             ->setCellValue('F'.($rowIdx), $row['nie'])
    //             ->setCellValue('G'.($rowIdx), $row['marital_status'])
    //             ->setCellValue('H'.($rowIdx), $row['basic_salary'])
    //             ->setCellValue('I'.($rowIdx), $row['bs_prorate'])
    //             ->setCellValue('J'.($rowIdx), $row['ot_total'])
    //             ->setCellValue('K'.($rowIdx), $row['travel_bonus'])
    //             ->setCellValue('L'.($rowIdx), $row['allowance_economy'])
    //             ->setCellValue('M'.($rowIdx), $row['incentive_bonus'])
    //             ->setCellValue('N'.($rowIdx), $row['shift_bonus'])
    //             ->setCellValue('O'.($rowIdx), $row['workday_adj'])
    //             ->setCellValue('P'.($rowIdx), $row['adjust_in'])
    //             ->setCellValue('Q'.($rowIdx), $row['adjust_out'])
    //             ->setCellValue('R'.($rowIdx), $row['thr'])
    //             ->setCellValue('S'.($rowIdx), $row['cc_payment'])
    //             ->setCellValue('T'.($rowIdx), $row['production_bonus'])
    //             ->setCellValue('U'.($rowIdx), $row['jkk_jkm'])
    //             ->setCellValue('V'.($rowIdx), $row['health_bpjs'])
    //             ->setCellValue('W'.($rowIdx), $row['unpaid_total'])
    //             ->setCellValue('X'.($rowIdx), $row['Gross'])
    //             ->setCellValue('Y'.($rowIdx), $row['tax_value'])
    //             ->setCellValue('Z'.($rowIdx), $row['jkk_jkm'])
    //             ->setCellValue('AA'.($rowIdx), $row['health_bpjs'])
    //             ->setCellValue('AB'.($rowIdx), $row['emp_health_bpjs'])
    //             ->setCellValue('AC'.($rowIdx), $row['emp_jht'])
    //             ->setCellValue('AD'.($rowIdx), $row['emp_jp'])
    //             ->setCellValue('AE'.($rowIdx), $row['thr'])
    //             ->setCellValue('AF'.($rowIdx), $row['cc_payment'])
    //             ->setCellValue('AG'.($rowIdx), $row['debt_burden'])
    //             ->setCellValue('AH'.($rowIdx), $row['Net_payment'])
    //             ;

    //         /* SET ROW COLOR */
    //         if($rowIdx % 2 == 1)
    //         {
    //             $spreadsheet->getActiveSheet()->getStyle('A'.$rowIdx.':AH'.$rowIdx)
    //             ->getFill()
    //             ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    //             ->getStartColor()
    //             ->setRGB('EAEBAF');             
    //         } 
    //     }

    //     $spreadsheet->getActiveSheet()->getStyle("A6:AH".($rowIdx))->applyFromArray($allBorderStyle);
    //     // Rename worksheet
    //     $spreadsheet->getActiveSheet()->setTitle('Report Excel '.date('d-m-Y H'));

    //     // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    //     $spreadsheet->setActiveSheetIndex(0);

    //     $str = 'Summary Payroll PT.Pontil_Timika';
    //     $fileName = preg_replace('/\s+/', '', $str);
    //     // $str = 'PTLSmbInvoice';
    //     // $fileName = 'Summary Payroll PT.'.$ptName.'';
    //     // test($fileName,1);
    //     // Redirect output to a client’s web browser (Xlsx)
    //     header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    //     // header('Content-Disposition: attachment;filename="Summary Payroll PT.'.$ptName.'.Xlsx"');
    //     header('Content-Disposition: attachment;filename="'.$fileName.'.Xlsx"');
    //     // header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
    //     header('Cache-Control: max-age=0');
    //     // If you're serving to IE 9, then the following may be needed
    //     header('Cache-Control: max-age=1');

    //     // If you're serving to IE over SSL, then the following may be needed
    //     header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    //     header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    //     header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    //     header('Pragma: public'); // HTTP/1.0
    //     /* BY COMPOSER */
    //     // $writer = new Xlsx($spreadsheet);
    //     /* OFFLINE/ BY COPY EXCEL FOLDER  */
    //     $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
    //     $writer->save('php://output');
    //     exit(0); 
    // }    

    // public function exportSummaryPayrollRedpath($ptName, $yearPeriod, $monthPeriod)
    // {
    //     // Create new Spreadsheet object
    //     $spreadsheet = new Spreadsheet();

    //     $spreadsheet->getProperties()->setCreator('Maurice - Web - Android')
    //         ->setLastModifiedBy('Maurice - Web - Android')
    //         ->setTitle('Office 2007 XLSX Test Document')
    //         ->setSubject('Office 2007 XLSX Test Document')
    //         ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
    //         ->setKeywords('office 2007 openxml php')
    //         ->setCategory('Test result file');

    //     $boldFont = [
    //         'font' => [
    //             'bold' => true
    //             // 'color' => ['argb' => '0000FF'],
    //         ],
    //     ];

    //     $totalStyle = [
    //         'font' => [
    //             'bold' => true,
    //             'color' => ['argb' => '0000FF'],
    //         ],
    //     ];

    //     $allBorderStyle = [
    //         'borders' => [
    //             'allBorders' => [
    //                 'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
    //                 'color' => ['argb' => '00000000'],
    //             ],
    //         ],
    //     ];

    //     $outlineBorderStyle = [
    //         'borders' => [
    //             'outline' => [
    //                 'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
    //                 'color' => ['argb' => '00000000'],
    //             ],
    //         ],
    //     ];

    //     $topBorderStyle = [
    //         'borders' => [
    //             'top' => [
    //                 'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
    //                 'color' => ['argb' => '00000000'],
    //             ],
    //         ],
    //     ];

    //     $bottomBorderStyle = [
    //         'borders' => [
    //             'bottom' => [
    //                 'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
    //                 'color' => ['argb' => '00000000'],
    //             ],
    //         ],
    //     ];

    //     $center = array();
    //     $center['alignment'] = array();
    //     $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
    //     $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

    //     $right = array();
    //     $right['alignment'] = array();
    //     $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
    //     $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

    //     $left = array();
    //     $left['alignment'] = array();
    //     $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
    //     $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

    //     // $ym = (string)$yearPeriod.'-'.$monthPeriod;

    //     $strSQL = " SELECT ss.bio_rec_id,ss.year_period,ss.month_period,ms.`payroll_group`,ms.`nie`,ss.name,mb.`local_foreign` AS Etnic,ss.`marital_status`,
    //         mb.`id_card_no`,mb.npwp_no,ms.`salary_level`,ss.`basic_salary`,ss.`bs_prorate`,(ss.ot_1+ss.ot_2+ss.ot_3+ss.ot_4 )AS ot_total,ss.`ot_bonus`,
    //         ss.`shift_bonus`,ss.`remote_allowance`,ss.`thr`,ss.`dept`,(ss.`dev_incentive_bonus`*(`dev_percent`/100))AS dev_bonus,ss.`dev_percent` AS PVB,
    //         ss.`workday_adj`,ss.`contract_bonus`,ss.`other_allowance2` AS Jumbo_Bns,ss.`jkk_jkm`,ss.`health_bpjs`,ss.`unpaid_total`,ss.`adjust_in`,
    //         ss.`adjust_out`,
    //         (ss.`bs_prorate`+ss.`ot_1`+ss.`ot_2`+ss.`ot_3`+ss.`ot_4`+ss.`ot_bonus`+ss.`remote_allowance`+ss.`shift_bonus`+ss.`thr`+
    //         (ss.`dev_incentive_bonus`*(`dev_percent`/100))+ss.`contract_bonus`+ss.`workday_adj` +ss.`other_allowance2` +ss.`jkk_jkm`+ss.`health_bpjs`+
    //         ss.`adjust_in`-ss.`adjust_out`-ss.`unpaid_total`) AS Gross,
    //         ss.`tax_value`,ss.`jkk_jkm`,ss.`health_bpjs`,ss.`emp_health_bpjs`,ss.`emp_jht`,ss.`emp_jp`,ss.`thr`,ss.`contract_bonus`,ss.`debt_burden`,
    //         (ss.`bs_prorate`+ss.`ot_1`+ss.`ot_2`+ss.`ot_3`+ss.`ot_4`+ss.`ot_bonus`+ss.`remote_allowance`+ss.`shift_bonus`+ss.`thr`+
    //         (ss.`dev_incentive_bonus`*(ss.`dev_percent`/100))+ss.`contract_bonus`+ss.`workday_adj`+ss.`other_allowance2`+ss.`jkk_jkm`+ss.`health_bpjs`- 
    //         ss.`unpaid_total`+ss.`adjust_in`-ss.`adjust_out`-ss.`tax_value`-ss.`jkk_jkm`-ss.`emp_health_bpjs`-ss.`health_bpjs`-ss.`emp_jht`-ss.`emp_jp`-
    //         ss.`thr`-ss.`contract_bonus`-ss.`debt_burden`) AS Net_payment
    //         FROM `db_recruitment`.trn_salary_slip ss,`db_recruitment`.mst_bio_rec mb,`db_recruitment`.mst_salary ms
    //         WHERE 
    //         mb.bio_rec_id = ss.bio_rec_id AND mb.bio_rec_id = ms.bio_rec_id AND ss.client_name = 'Redpath_Timika'
    //         AND ss.month_period = '".$monthPeriod."' AND ss.year_period = '".$yearPeriod."'
    //         ORDER BY ss.client_name, ss.year_period, ss.month_period, ms.`payroll_group`,ss.name";
    //     $query = $this->db->query($strSQL)->result_array();

    //     // Add some data
    //     $spreadsheet->getActiveSheet()->getStyle('A6:AL7')
    //         ->getFill()
    //         ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    //         ->getStartColor()
    //         ->setRGB('F2BE6B'); 

    //     $spreadsheet->getActiveSheet()
    //         ->setCellValue('A1', 'PT. SANGATI SOERYA SEJAHTERA')
    //         ->setCellValue('A2', 'SUMMARY PAYROLL PT.'.$ptName.' ')
    //         ->setCellValue('A4', 'Periode : '.$monthPeriod.'-'.$yearPeriod);

    //     $spreadsheet->getActiveSheet()->getStyle("A1:D1")->getFont()->setBold(true)->setSize(16);
    //     $spreadsheet->getActiveSheet()->getStyle("A2:D2")->getFont()->setBold(true)->setSize(14)->setUnderline(true);
    //     $spreadsheet->getActiveSheet()->getStyle("A4:G4")->getFont()->setBold(true)->setSize(12);
    //     $spreadsheet->getActiveSheet()->getStyle("A6:AL7")->getFont()->setBold(true)->setSize(12);

    //     $spreadsheet->getActiveSheet()->mergeCells("A6:A7");
    //     $spreadsheet->getActiveSheet()->mergeCells("B6:B7");
    //     $spreadsheet->getActiveSheet()->mergeCells("C6:C7");
    //     $spreadsheet->getActiveSheet()->mergeCells("D6:D7");
    //     $spreadsheet->getActiveSheet()->mergeCells("E6:E7");
    //     $spreadsheet->getActiveSheet()->mergeCells("F6:F7");
    //     $spreadsheet->getActiveSheet()->mergeCells("G6:G7");
    //     $spreadsheet->getActiveSheet()->mergeCells("H6:H7");
    //     $spreadsheet->getActiveSheet()->mergeCells("I6:I7");
    //     $spreadsheet->getActiveSheet()->mergeCells("J6:J7");
    //     $spreadsheet->getActiveSheet()->mergeCells("K6:K7");
    //     $spreadsheet->getActiveSheet()->mergeCells("L6:L7");
    //     $spreadsheet->getActiveSheet()->mergeCells("M6:M7");
    //     $spreadsheet->getActiveSheet()->mergeCells("N6:N7");
    //     $spreadsheet->getActiveSheet()->mergeCells("O6:O7");
    //     $spreadsheet->getActiveSheet()->mergeCells("P6:P7");
    //     $spreadsheet->getActiveSheet()->mergeCells("Q6:Q7");
    //     $spreadsheet->getActiveSheet()->mergeCells("R6:R7");
    //     $spreadsheet->getActiveSheet()->mergeCells("S6:S7");
    //     $spreadsheet->getActiveSheet()->mergeCells("T6:T7");
    //     $spreadsheet->getActiveSheet()->mergeCells("U6:U7");
    //     $spreadsheet->getActiveSheet()->mergeCells("V6:V7");
    //     $spreadsheet->getActiveSheet()->mergeCells("W6:W7");
    //     $spreadsheet->getActiveSheet()->mergeCells("X6:X7");
    //     $spreadsheet->getActiveSheet()->mergeCells("Y6:Y7");
    //     $spreadsheet->getActiveSheet()->mergeCells("Z6:Z7");
    //     $spreadsheet->getActiveSheet()->mergeCells("AA6:AA7");
    //     $spreadsheet->getActiveSheet()->mergeCells("AB6:AB7");
    //     $spreadsheet->getActiveSheet()->mergeCells("AC6:AC7");
    //     $spreadsheet->getActiveSheet()->mergeCells("AD6:AD7");
    //     $spreadsheet->getActiveSheet()->mergeCells("AE6:AE7");
    //     $spreadsheet->getActiveSheet()->mergeCells("AF6:AF7");
    //     $spreadsheet->getActiveSheet()->mergeCells("AG6:AG7");
    //     $spreadsheet->getActiveSheet()->mergeCells("AH6:AH7");
    //     $spreadsheet->getActiveSheet()->mergeCells("AI6:AI7");
    //     $spreadsheet->getActiveSheet()->mergeCells("AJ6:AJ7");
    //     $spreadsheet->getActiveSheet()->mergeCells("AK6:AK7");
    //     $spreadsheet->getActiveSheet()->mergeCells("AL6:AL7");
            
    //     $spreadsheet->getActiveSheet()->getStyle("A6:AL7")->applyFromArray($allBorderStyle);
    //     $spreadsheet->getActiveSheet()->getStyle("A6:AL7")->applyFromArray($center);
    //     $spreadsheet->getActiveSheet()
    //         ->setCellValue('A6', 'NO')
    //         ->setCellValue('B6', 'YEAR')
    //         ->setCellValue('C6', 'MONTH')
    //         ->setCellValue('D6', 'PAYROLL GROUP')
    //         ->setCellValue('E6', 'NAMA')
    //         ->setCellValue('F6', 'EXTERNAL ID')
    //         ->setCellValue('G6', 'MARTIAL STATUS')
    //         ->setCellValue('H6', 'DEPARTMENT')
    //         ->setCellValue('I6', 'SALARY LEVEL')
    //         ->setCellValue('J6', 'BASIC SALARY')
    //         ->setCellValue('K6', 'BASIC SALARY PRORATE')
    //         ->setCellValue('L6', 'OVERTIME TOTAL')
    //         ->setCellValue('M6', 'OVERTIME BONUS')
    //         ->setCellValue('N6', 'SHIFT BONUS')
    //         ->setCellValue('O6', 'REMOTE ALLOWANCE')
    //         ->setCellValue('P6', 'THR')
    //         ->setCellValue('Q6', 'DEV BONUS')
    //         ->setCellValue('R6', 'PVB')
    //         ->setCellValue('S6', 'ADJUSTMENT WORK DAY')
    //         ->setCellValue('T6', 'ADJUSTMENT IN')
    //         ->setCellValue('U6', 'ADJUSTMENT OUT')
    //         ->setCellValue('V6', 'THR')
    //         ->setCellValue('W6', 'CONTRACT BONUS')
    //         ->setCellValue('X6', 'JUMBO BNS')
    //         ->setCellValue('Y6', 'JKK & JKM')
    //         ->setCellValue('Z6', 'HEALTH BPJS')
    //         ->setCellValue('AA6', 'UNPAID TOTAL')
    //         ->setCellValue('AB6', 'GROSS')
    //         ->setCellValue('AC6', 'TAX VALUE')
    //         ->setCellValue('AD6', 'JKK & JKM')
    //         ->setCellValue('AE6', 'HEALTH BPJS')
    //         ->setCellValue('AF6', 'EMP HEALTH BPJS')
    //         ->setCellValue('AG6', 'EMP JHT')
    //         ->setCellValue('AH6', 'EMP JP')
    //         ->setCellValue('AI6', 'THR')
    //         ->setCellValue('AJ6', 'CONTRACT BONUS')
    //         ->setCellValue('AK6', 'DEBT BURDEN')
    //         ->setCellValue('AL6', 'NET PAYMENT');

    //     /* START GET DAYS TOTAL BY ROSTER */
    //     $rowIdx = 7;
    //     $rowNo = 0;
    //     foreach ($query as $row) {                      
    //         $rowIdx++;
    //         $rowNo++;
    //         $spreadsheet->getActiveSheet()
    //             ->setCellValue('A'.($rowIdx), $rowNo)
    //             ->setCellValue('B'.($rowIdx), $row['year_period'])
    //             ->setCellValue('C'.($rowIdx), $row['month_period'])
    //             ->setCellValue('D'.($rowIdx), $row['payroll_group'])
    //             ->setCellValue('E'.($rowIdx), $row['name'])
    //             ->setCellValue('F'.($rowIdx), $row['nie'])
    //             ->setCellValue('G'.($rowIdx), $row['marital_status'])
    //             ->setCellValue('H'.($rowIdx), $row['dept'])
    //             ->setCellValue('I'.($rowIdx), $row['salary_level'])
    //             ->setCellValue('J'.($rowIdx), $row['basic_salary'])
    //             ->setCellValue('K'.($rowIdx), $row['bs_prorate'])
    //             ->setCellValue('L'.($rowIdx), $row['ot_total'])
    //             ->setCellValue('M'.($rowIdx), $row['ot_bonus'])
    //             ->setCellValue('N'.($rowIdx), $row['shift_bonus'])
    //             ->setCellValue('O'.($rowIdx), $row['remote_allowance'])
    //             ->setCellValue('P'.($rowIdx), $row['thr'])
    //             ->setCellValue('Q'.($rowIdx), $row['dev_bonus'])
    //             ->setCellValue('R'.($rowIdx), $row['PVB'])
    //             ->setCellValue('S'.($rowIdx), $row['workday_adj'])
    //             ->setCellValue('T'.($rowIdx), $row['adjust_in'])
    //             ->setCellValue('U'.($rowIdx), $row['adjust_out'])
    //             ->setCellValue('V'.($rowIdx), $row['thr'])
    //             ->setCellValue('W'.($rowIdx), $row['contract_bonus'])
    //             ->setCellValue('X'.($rowIdx), $row['Jumbo_Bns'])
    //             ->setCellValue('Y'.($rowIdx), $row['jkk_jkm'])
    //             ->setCellValue('Z'.($rowIdx), $row['health_bpjs'])
    //             ->setCellValue('AA'.($rowIdx), $row['unpaid_total'])
    //             ->setCellValue('AB'.($rowIdx), $row['Gross'])
    //             ->setCellValue('AC'.($rowIdx), $row['tax_value'])
    //             ->setCellValue('AD'.($rowIdx), $row['jkk_jkm'])
    //             ->setCellValue('AE'.($rowIdx), $row['health_bpjs'])
    //             ->setCellValue('AF'.($rowIdx), $row['emp_health_bpjs'])
    //             ->setCellValue('AG'.($rowIdx), $row['emp_jht'])
    //             ->setCellValue('AH'.($rowIdx), $row['emp_jp'])
    //             ->setCellValue('AI'.($rowIdx), $row['thr'])
    //             ->setCellValue('AJ'.($rowIdx), $row['contract_bonus'])
    //             ->setCellValue('AK'.($rowIdx), $row['debt_burden'])
    //             ->setCellValue('AL'.($rowIdx), $row['Net_payment'])
    //             ;

    //         /* SET ROW COLOR */
    //         if($rowIdx % 2 == 1)
    //         {
    //             $spreadsheet->getActiveSheet()->getStyle('A'.$rowIdx.':AL'.$rowIdx)
    //             ->getFill()
    //             ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    //             ->getStartColor()
    //             ->setRGB('EAEBAF');             
    //         } 
    //     }

    //     $spreadsheet->getActiveSheet()->getStyle("A6:AL".($rowIdx))->applyFromArray($allBorderStyle);
    //     // Rename worksheet
    //     $spreadsheet->getActiveSheet()->setTitle('Report Excel '.date('d-m-Y H'));

    //     // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    //     $spreadsheet->setActiveSheetIndex(0);

    //     // $str = 'PTLSmbInvoice';
    //     // $fileName = 'Summary Payroll PT.'.$ptName.'';

    //     $str = 'Summary Payroll PT.Redpath_Timika';
    //     $fileName = preg_replace('/\s+/', '', $str);
    //     // Redirect output to a client’s web browser (Xlsx)
    //     header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    //     header('Content-Disposition: attachment;filename="'.$fileName.'.Xlsx"');
    //     // header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
    //     header('Cache-Control: max-age=0');
    //     // If you're serving to IE 9, then the following may be needed
    //     header('Cache-Control: max-age=1');

    //     // If you're serving to IE over SSL, then the following may be needed
    //     header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    //     header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    //     header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    //     header('Pragma: public'); // HTTP/1.0
    //     /* BY COMPOSER */
    //     // $writer = new Xlsx($spreadsheet);
    //     /* OFFLINE/ BY COPY EXCEL FOLDER  */
    //     $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
    //     $writer->save('php://output');
    //     exit(0);   
    // }    
}

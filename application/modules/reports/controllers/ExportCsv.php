<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

    // Load library phpspreadsheet
    require('./vendor/autoload.php');
    use PhpOffice\PhpSpreadsheet\Helper\Sample;
    use PhpOffice\PhpSpreadsheet\IOFactory;
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    // End load library phpspreadsheet

class ExportCsv extends CI_Controller
{
	public function __construct(){
        parent::__construct();
        $this->load->model('masters/M_roster_hist');
        $this->load->model('transactions/M_salary_slip'); 
        $this->load->model('masters/M_payroll_config'); 
        $this->load->model('masters/M_mst_salary', 'M_salary');
    }

    public function getExportCsvView()
    {
        $pt     = $this->input->post('pt');
        $year   = $this->input->post('year');
        $site   = $this->input->post('site');
        $month  = $this->input->post('month');
        $dept   = $this->input->post('dept');
        $bank   = $this->input->post('bank');
        // test($bank,1);
        
        $sql        = '';
        $sqlbank    = '';
        $sqldept    = '';
        if($bank == 'EXCPT'){
            $sqlbank .= "AND b.bank_name !='MANDIRI' AND b.bank_name !='BNI' ";
            // $sqlbank .= "AND b.bank_name ='".$bank."' ";
        }
        else{
            $sqlbank .= "AND b.bank_name ='".$bank."' ";
        }

        if($pt == 'Redpath_Timika'){
            $sql .= "AND a.client_name ='".$pt."' ";
            if($dept != 'All'){
                $sqldept .= "AND a.dept ='".$dept."' ";
            }            
        }
        else if($pt == 'Pontil_Timika'){
            $sql .= "AND a.client_name ='".$pt."' ";
            if($dept != 'All'){
                $sqldept .= "AND a.payroll_group ='".$dept."' ";
            }            
        }
        else{
            $sql = '';
        }
        
        $query  = "SELECT b.account_no,b.account_name,b.bank_name,b.bank_code, a.*
                    FROM trn_salary_slip a
                    LEFT JOIN mst_salary b ON a.bio_rec_id=b.bio_rec_id
                    LEFT JOIN mst_bio_rec c ON a.bio_rec_id=c.bio_rec_id
                    WHERE a.client_name LIKE '%".$site."%' ".$sql." ".$sqldept." AND a.year_period='".$year."' AND a.month_period ='".$month."' ".$sqlbank." ORDER BY b.account_name ASC";
        // test($query,1);
        $query = $this->db->query($query)->result_array();
        $myData = array();
        foreach ($query as $key => $row) {
            $myData[] = array(
                $row['bio_rec_id'],         
                $row['name'],         
                $row['account_no'],         
                $row['account_name'],         
                $row['bank_name'],             
                $row['client_name']
            );            
        }  
        echo json_encode($myData);  
    }

    public function exporta($site,$clientName,$yearPeriod,$dataprint,$monthPeriod)
    {
        $site       = $site;
        $pt         = $clientName;
        $year       = $yearPeriod;
        $month      = $monthPeriod;
        $dataname   = $dataprint;
        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();  
        // $spreadsheet->getProperties()->setCreator('Maurice - Web - Android')
        //     ->setLastModifiedBy('Maurice - Web - Android')
        //     ->setTitle('Office 2007 XLSX Test Document')
        //     ->setSubject('Office 2007 XLSX Test Document')
        //     ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
        //     ->setKeywords('office 2007 openxml php')
        //     ->setCategory('Test result file');      
        $strSQL = '';
        if ($pt == 'All'){
            $strSQL  = "SELECT a.* FROM 
            (SELECT 0 jumboPercent,0 jumboVal,ss.salary_slip_id,mb.bio_rec_id,mb.full_name,ms.nie,mb.position,ms.bank_code,ms.bank_name,mb.npwp_no,
            ms.account_name,ms.account_no,ss.ptkp_total,ss.thr,ss.contract_bonus,ss.emp_jht,ss.emp_jp,ss.cc_payment,ss.debt_burden,
            ms.payroll_group,ss.tax_value,ss.client_name,
            (CAST(ss.bs_prorate+ss.ot_1+ss.ot_2+ss.ot_3+ss.ot_4+  
                CASE WHEN(ms.is_travel = 1) THEN ss.travel_bonus ELSE 0 END + 
                CASE WHEN(ms.is_allowance_economy = 1) THEN ss.allowance_economy ELSE 0 END + 
                CASE WHEN(ms.is_incentive_bonus = 1) THEN ss.incentive_bonus ELSE 0 END + 
                CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END + 
                CASE WHEN(ms.is_production = 1) THEN ss.production_bonus ELSE 0 END + 
            ss.workday_adj+ ss.jkk_jkm+ ss.health_bpjs- ss.unpaid_total+ ss.adjust_in- ss.adjust_out- ss.jkk_jkm- ss.emp_jht- 
            ss.emp_health_bpjs- ss.health_bpjs- ss.emp_jp AS DECIMAL(11,2))   ) AS total,  
            ( CAST( ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 +  
                CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END + 
                CASE WHEN(ms.is_allowance_economy = 1) THEN ss.allowance_economy ELSE 0 END + 
                CASE WHEN(ms.is_incentive_bonus = 1) THEN ss.incentive_bonus ELSE 0 END + 
                CASE WHEN(ms.is_production = 1) THEN ss.production_bonus ELSE 0 END + 
                CASE WHEN(ms.is_travel = 1) THEN ss.travel_bonus ELSE 0 END + 
                CASE WHEN(ms.is_cc_payment = 1) THEN ss.cc_payment ELSE 0 END + 
            ss.workday_adj+ ss.thr+ ss.jkk_jkm+ ss.health_bpjs- ss.unpaid_total+ ss.adjust_in- ss.adjust_out AS DECIMAL(11,2)))AS brutto_tax 
            FROM mst_salary ms,mst_bio_rec mb,trn_salary_slip ss 
            WHERE ms.company_name LIKE '%".$site."' 
            AND ss.month_period = '".$monthPeriod."' AND ss.year_period = '".$yearPeriod."' AND ms.bio_rec_id = mb.bio_rec_id  
            AND mb.bio_rec_id = ss.bio_rec_id AND mb.is_active = 1 AND ms.company_name ='Pontil_Timika'
            UNION
            SELECT ss.other_allowance1 AS jumboPercent,(ss.dev_incentive_bonus*(ss.dev_percent/100))* (ss.other_allowance1/100) AS jumboVal,ss.salary_slip_id,mb1.bio_rec_id,mb1.full_name,ms.nie,mb1.position,ms.bank_code,ms.bank_name,ms.account_name,ms.account_no,ss.ptkp_total,ss.thr,ss.contract_bonus,ss.emp_jht,ss.emp_jp,ss.cc_payment,ss.debt_burden,ms.payroll_group,ss.tax_value,ss.client_name,
            (CAST( CASE WHEN(ss.basic_salary = ss.bs_prorate) THEN ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4  
                ELSE ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 END +  
                CASE WHEN(ms.is_ot_bonus = 1) THEN ss.ot_bonus ELSE 0 END + 
                CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END + 
                CASE WHEN(ms.is_remote_allowance = 1) THEN ss.remote_allowance ELSE 0 END + 
                CASE WHEN(ms.is_dev_incentive_bonus = 1) THEN ss.dev_incentive_bonus*(ss.dev_percent/100) ELSE 0 END + 
            (ss.dev_incentive_bonus*(ss.dev_percent/100))*(ss.other_allowance1/100) + ss.workday_adj + ss.no_accident_bonus+ ss.jkk_jkm + ss.health_bpjs - ss.unpaid_total + ss.adjust_in - ss.adjust_out- ss.jkk_jkm- ss.emp_jht- ss.emp_health_bpjs- ss.health_bpjs- 
            ss.emp_jp AS DECIMAL(11,2))) AS total,  
            (CAST( CASE WHEN(ss.basic_salary = ss.bs_prorate) THEN ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4  
                ELSE ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 END +  
                CASE WHEN(ms.is_ot_bonus = 1) THEN ss.ot_bonus ELSE 0 END + 
                CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END + 
                CASE WHEN(ms.is_remote_allowance = 1) THEN ss.remote_allowance ELSE 0 END + ss.thr+ 
                CASE WHEN(ms.is_dev_incentive_bonus = 1) THEN ss.dev_incentive_bonus*(ss.dev_percent/100) ELSE 0 END + 
            (ss.dev_incentive_bonus*(ss.dev_percent/100))*(ss.other_allowance1/100) + ss.contract_bonus+ ss.workday_adj+ 
            ss.no_accident_bonus+ss.jkk_jkm+ss.health_bpjs- ss.unpaid_total+ ss.adjust_in- ss.adjust_out AS DECIMAL(11,2))) AS brutto_tax
            FROM mst_salary ms,mst_bio_rec mb1,trn_salary_slip ss 
            WHERE ms.company_name LIKE '%".$site."' AND ss.month_period = '".$monthPeriod."' AND ss.year_period = '".$yearPeriod."' 
            AND ms.bio_rec_id = mb1.bio_rec_id  AND mb1.bio_rec_id = ss.bio_rec_id AND mb1.is_active = 1 
            AND ms.company_name ='Redpath_Timika') AS a
            ORDER BY a.full_name "; 
        }

        else if($pt == "Redpath_Timika")
        {
            $strSQL  = "SELECT ss.other_allowance1 AS jumboPercent,(ss.dev_incentive_bonus*(ss.dev_percent/100))*(ss.other_allowance1/100) AS jumboVal,ss.salary_slip_id,mb.bio_rec_id,mb.full_name,ms.nie,mb.position,ms.bank_code,ms.bank_name,ms.account_name,ms.account_no,ss.ptkp_total,ss.thr,ss.contract_bonus,ss.emp_jht,ss.emp_jp,ss.cc_payment,ss.debt_burden,ms.payroll_group,ss.tax_value,ss.client_name,mb.npwp_no,";
            $strSQL .= " (";
            $strSQL .= " CAST("; /*START CAST*/
            $strSQL .= " CASE WHEN(ss.basic_salary = ss.bs_prorate) THEN ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 ";
            $strSQL .= " ELSE ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 END + ";
            $strSQL .= " CASE WHEN(ms.is_ot_bonus = 1) THEN ss.ot_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_remote_allowance = 1) THEN ss.remote_allowance ELSE 0 END +";
            // $strSQL .= " ss.thr+";
            $strSQL .= " CASE WHEN(ms.is_dev_incentive_bonus = 1) THEN ss.dev_incentive_bonus*(ss.dev_percent/100) ELSE 0 END +";
            $strSQL .= " (ss.dev_incentive_bonus*(ss.dev_percent/100))*(ss.other_allowance1/100) +";
            $strSQL .= " ss.workday_adj+";
            $strSQL .= " ss.no_accident_bonus+";
            $strSQL .= " ss.jkk_jkm+";
            $strSQL .= " ss.health_bpjs-";
            $strSQL .= " ss.unpaid_total+";
            $strSQL .= " ss.adjust_in-";
            $strSQL .= " ss.adjust_out-";  
            $strSQL .= " ss.jkk_jkm-";
            $strSQL .= " ss.emp_jht-";
            $strSQL .= " ss.emp_health_bpjs-";
            $strSQL .= " ss.health_bpjs-";
            $strSQL .= " ss.emp_jp";
            $strSQL .= " AS DECIMAL(11,2))  "; /*END CAST*/
            $strSQL .= " ) AS total, ";

            /* START BRUTTO */
            $strSQL .= " (";
            $strSQL .= " CAST("; /*START CAST*/
            $strSQL .= " CASE WHEN(ss.basic_salary = ss.bs_prorate) THEN ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 ";
            $strSQL .= " ELSE ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 END + ";
            $strSQL .= " CASE WHEN(ms.is_ot_bonus = 1) THEN ss.ot_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_remote_allowance = 1) THEN ss.remote_allowance ELSE 0 END +";
            $strSQL .= " ss.thr+";
            $strSQL .= " CASE WHEN(ms.is_dev_incentive_bonus = 1) THEN ss.dev_incentive_bonus*(ss.dev_percent/100) ELSE 0 END +";
            $strSQL .= " (ss.dev_incentive_bonus*(ss.dev_percent/100))*(ss.other_allowance1/100) +";
            $strSQL .= " ss.contract_bonus+";
            $strSQL .= " ss.workday_adj+";
            $strSQL .= " ss.no_accident_bonus+";
            $strSQL .= " ss.jkk_jkm+";
            $strSQL .= " ss.health_bpjs-";
            $strSQL .= " ss.unpaid_total+";
            $strSQL .= " ss.adjust_in-";
            $strSQL .= " ss.adjust_out "; 
            $strSQL .= " AS DECIMAL(11,2))  "; /*END CAST*/
            $strSQL .= " ) AS brutto_tax ";
            /* END BRUTTO */
            $strSQL .= "FROM mst_salary ms,mst_bio_rec mb,trn_salary_slip ss ";
            $strSQL .= "WHERE ms.company_name LIKE '%".$site."' ";
            $strSQL .= "AND ss.month_period = '".$monthPeriod."' ";
            $strSQL .= "AND ss.year_period = '".$yearPeriod."' ";
            $strSQL .= "AND ms.bio_rec_id = mb.bio_rec_id  ";
            $strSQL .= "AND mb.bio_rec_id = ss.bio_rec_id ";
            $strSQL .= "AND mb.is_active = 1 ";      
            $strSQL .= "AND ms.company_name = 'Redpath_Timika' ";      
            $strSQL .= "ORDER BY mb.full_name ";      
        }
        else if($pt == "Pontil_Timika")
        {           

            $strSQL  = "SELECT 0 jumboPercent,0 jumboVal,ss.salary_slip_id,mb.bio_rec_id,mb.full_name,ms.nie,mb.position,ms.bank_code,ms.bank_name,ms.account_name,ms.account_no,ss.ptkp_total,ss.thr,ss.contract_bonus,ss.emp_jht,ss.emp_jp,ss.cc_payment,ss.debt_burden,ms.payroll_group,ss.tax_value,ss.client_name,mb.npwp_no,";
            $strSQL .= " (";
            $strSQL .= " CAST("; /*START CAST*/
            $strSQL .= " ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 + ";
            $strSQL .= " CASE WHEN(ms.is_travel = 1) THEN ss.travel_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_allowance_economy = 1) THEN ss.allowance_economy ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_incentive_bonus = 1) THEN ss.incentive_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_production = 1) THEN ss.production_bonus ELSE 0 END +";
           
            $strSQL .= " ss.workday_adj+";
            $strSQL .= " ss.jkk_jkm+";
            $strSQL .= " ss.health_bpjs-";
            $strSQL .= " ss.unpaid_total+";
            $strSQL .= " ss.adjust_in-";
            $strSQL .= " ss.adjust_out-";  
            $strSQL .= " ss.jkk_jkm-";
            $strSQL .= " ss.emp_jht-";
            $strSQL .= " ss.emp_health_bpjs-";
            $strSQL .= " ss.health_bpjs-";
            $strSQL .= " ss.emp_jp";
            $strSQL .= " AS DECIMAL(11,2))  "; /*END CAST*/
            $strSQL .= " ) AS total, ";

            /* START BRUTTO */
            $strSQL .= " (";
            $strSQL .= " CAST("; /*START CAST*/
            // $strSQL .= " ("; /*START SETAHUNKAN*/
            $strSQL .= " ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 + ";
            $strSQL .= " CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_allowance_economy = 1) THEN ss.allowance_economy ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_incentive_bonus = 1) THEN ss.incentive_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_production = 1) THEN ss.production_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_travel = 1) THEN ss.travel_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_cc_payment = 1) THEN ss.cc_payment ELSE 0 END +"; /*ok*/
            $strSQL .= " ss.workday_adj+";
            $strSQL .= " ss.thr+";
            $strSQL .= " ss.jkk_jkm+";
            $strSQL .= " ss.health_bpjs-";
            $strSQL .= " ss.unpaid_total+";
            $strSQL .= " ss.adjust_in-";
            $strSQL .= " ss.adjust_out";
            $strSQL .= " AS DECIMAL(11,2))  "; /*END CAST*/
            $strSQL .= " ) AS brutto_tax ";
            /* END BRUTTO */

            $strSQL .= "FROM mst_salary ms,mst_bio_rec mb,trn_salary_slip ss ";
            $strSQL .= "WHERE ms.company_name LIKE '%".$site."' ";
            $strSQL .= "AND ss.month_period = '".$monthPeriod."' ";
            $strSQL .= "AND ss.year_period = '".$yearPeriod."' ";
            $strSQL .= "AND ms.bio_rec_id = mb.bio_rec_id  ";
            $strSQL .= "AND mb.bio_rec_id = ss.bio_rec_id ";
            $strSQL .= "AND mb.is_active = 1 ";   
            $strSQL .= "AND ms.company_name = 'Pontil_Timika' ";   
            $strSQL .= "ORDER BY mb.full_name ";      
        } 
        $query = $this->db->query($strSQL)->result_array();    
        $boldFont = [
            'font' => [
                'bold' => true
                // 'color' => ['argb' => '0000FF'],
            ],
        ];

        $totalStyle = [
            'font' => [
                'bold' => true,
                'color' => ['argb' => '0000FF'],
            ],
        ];

        $allBorderStyle = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $outlineBorderStyle = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $topBorderStyle = [
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $bottomBorderStyle = [
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        foreach(range('A','D') as $columnID)
        {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }           

        // Nama Field Baris Pertama
        $spreadsheet->getActiveSheet()
                ->setCellValue('A1', 'PT. SANGATI SOERYA SEJAHTERA')
                ->setCellValue('A2', ''.$pt.'')
                ->setCellValue('A3', 'For the month of : '.$year.'-'.$month.'');

        $spreadsheet->getActiveSheet()->mergeCells("A1:D1");
        $spreadsheet->getActiveSheet()->mergeCells("A2:D2");
        $spreadsheet->getActiveSheet()->getStyle("A1:D1")->getFont()->setBold(true)->setSize(16);
        $spreadsheet->getActiveSheet()->getStyle("A2:D2")->getFont()->setBold(true)->setSize(13);
        $spreadsheet->getActiveSheet()->getStyle("A3:D5")->getFont()->setBold(true)->setSize(12);       

        /* SET HEADER BG COLOR*/
        $spreadsheet->getActiveSheet()->getStyle('A6:D7')
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B'); 

        /* START INVOICE TITLE */
        $spreadsheet->getActiveSheet()->getStyle("A6:D6")->getFont()->setSize(12);
        $spreadsheet->getActiveSheet()->getStyle("A6:D7")->applyFromArray($allBorderStyle);        
        $spreadsheet->getActiveSheet()->getStyle("A6:D7")->applyFromArray($center);

        $spreadsheet->getActiveSheet()->getStyle("A6:D7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->mergeCells("A6:A7");
        $spreadsheet->getActiveSheet()->mergeCells("B6:B7");
        $spreadsheet->getActiveSheet()->mergeCells("C6:C7");
        $spreadsheet->getActiveSheet()->mergeCells("D6:D7");

        $spreadsheet->getActiveSheet()
                ->setCellValue('A6', 'BANK ACCOUNT NUMBER')
                ->setCellValue('B6', 'BANK ACCOUNT NAME')
                ->setCellValue('C6', 'TOTAL PAY')
                ->setCellValue('D6', 'BANK NAME')
                ;

        /* START TOTAL WORK HOUR */
        $bruttoTax = 0;
        /* TAX CONFIG */
        $myConfigId = 0;    
        switch ($pt) {
            case 'Redpath_Timika':
                $myConfigId = 1;
                break;
            case 'Pontil_Timika':
                $myConfigId = 2;
                break;  
            case 'All':
                $myConfigId = 1;
                break;          
            default:
                break;
        }
        // $row = '';
        // if ($pt == 'All'){
        //     $a = 1;
        //     $b = 2;
        //     $row        = $this->M_payroll_config->getObjectById($a); 
        //     $row        = $this->M_payroll_config->getObjectById($b); 
        // }
        // else{
        // }
        $row            = $this->M_payroll_config->getObjectById($myConfigId); 
        $taxPercent1    = $this->M_payroll_config->getTaxPercent1(); /* Persentase Progresif Pajak I */
        $taxPercent2    = $this->M_payroll_config->getTaxPercent2(); /* Persentase Progresif Pajak II */
        $taxPercent3    = $this->M_payroll_config->getTaxPercent3(); /* Persentase Progresif Pajak III */
        $taxPercent4    = $this->M_payroll_config->getTaxPercent4(); /* Persentase Progresif Pajak IV */
        $maxTaxVal1     = $this->M_payroll_config->getTaxMaxVal1(); /* Nominal Pajak I */ 
        $maxTaxVal2     = $this->M_payroll_config->getTaxMaxVal2(); /* Nominal Pajak II */
        $maxTaxVal3     = $this->M_payroll_config->getTaxMaxVal3(); /* Nominal Pajak III */
        $maxTaxVal4     = $this->M_payroll_config->getTaxMaxVal4(); /* Nominal Pajak IV */
        $maxNonTax      = $this->M_payroll_config->getNonTaxAllowance(); /* Max Tunjangan Jabatan */
        
        $rowIdx         = 7;
        $startIdx       = $rowIdx;
        foreach ($query as $row) {
            $rowIdx++;

            /* START UPDATE TAX */
            $bruttoTax = $row['brutto_tax'];
            $pembulatanPenghasilan = $bruttoTax;
            $nonTaxAllowance = ($pembulatanPenghasilan) * (5/100);
            if($nonTaxAllowance > $maxNonTax){
                $nonTaxAllowance = $maxNonTax;
            }

            $netto = $bruttoTax - $row['emp_jht'] - $row['emp_jp'] - $nonTaxAllowance;

            $unFixedIncome = 0;
            if($pt == "Pontil_Timika"){
                $unFixedIncome = $row['thr'] + $row['cc_payment'];
            }else if($pt == "Redpath_Timika"){
                $unFixedIncome = $row['thr'] + $row['contract_bonus'];
            }

            $nettoSetahun = ( ($netto - $unFixedIncome)*12 );
            $monthlyTax = 0;

            $taxVal1 = 0;    
            $taxVal2 = 0;    
            $taxVal3 = 0;    
            $taxVal4 = 0;

            $tVal = 0;
            $tSisa = 0;
            $pembulatanPenghasilan = $nettoSetahun - $row['ptkp_total'];
            if($pembulatanPenghasilan > 0)
            {
                /* TAX 1 */
                if($maxTaxVal1 > 0)
                {
                    $tVal = $pembulatanPenghasilan/$maxTaxVal1;  
                    if($tVal >= 1){
                        $taxVal1 = $maxTaxVal1 * ($taxPercent1/100);
                    }
                    else{
                        $taxVal1 = $pembulatanPenghasilan * ($taxPercent1/100);
                    }    
                }    
                
                /* TAX 2 */
                if($maxTaxVal2 > 0)
                {
                    if($pembulatanPenghasilan > $maxTaxVal1)
                    {
                        $tSisa = $pembulatanPenghasilan - $maxTaxVal1; 
                        $tVal = $tSisa/$maxTaxVal2;
                        if($tVal >= 1){
                            $taxVal2 = $maxTaxVal2 * ($taxPercent2/100);
                        }
                        else{
                            $taxVal2 = $tSisa * ($taxPercent2/100);
                        }
                    }     
                }
                 
                /* TAX 3 */
                if($maxTaxVal3 > 0)
                {
                    if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2))
                    {
                        $tSisa = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2;
                        $tVal = $tSisa/$maxTaxVal3;
                        if($tVal >= 1){
                            $taxVal3 = $maxTaxVal3 * ($taxPercent3/100);
                        }
                        else{
                            $taxVal3 = $tSisa * ($taxPercent3/100);
                        }
                    }    
                }
                 
                /* TAX 4 */
                if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2 + $maxTaxVal3))
                {
                    $tSisa = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2 - $maxTaxVal3;
                    $taxVal4 = $tSisa * ($taxPercent4/100); 
                }               
            }

            /* START THR - BONUS TAX -- EDITED @ AUG 15, 2017 BY FINANCE  */
            // $bonusTmp = $row['thr'] + $row['contract_bonus'];
            // $bonusTax = 0;
            // if( $taxVal4 > 0){
            //     $bonusTax = $bonusTmp * ($taxPercent4/100);
            // }else if( $taxVal3 > 0 ){
            //     $bonusTax = $bonusTmp * ($taxPercent3/100);
            // }else if( $taxVal2 > 0 ){
            //     $bonusTax = $bonusTmp * ($taxPercent2/100);                
            // }else{
            //     $bonusTax = $bonusTmp * ($taxPercent1/100);             
            // }

            /* END THR - BONUS TAX -- EDITED @ AUG 15, 2017 BY FINANCE */
            $taxTotalFix = $taxVal1 + $taxVal2 + $taxVal3 + $taxVal4;
            $taxTotal = ( $taxTotalFix/12 );
            if($taxTotal > 0){
                $monthlyTax = $taxTotal; 
            }else{
                $monthlyTax = 0; 
            }


            $nettoSetahunUnFix = ( ($netto - $unFixedIncome)*12 ) + $unFixedIncome;

            $taxTotalUnFix = 0;
            $taxValUnFix1 = 0;    
            $taxValUnFix2 = 0;    
            $taxValUnFix3 = 0;    
            $taxValUnFix4 = 0;

            $tValUnFix = 0;
            $tSisaUnFix = 0;
            $pembulatanPenghasilan = $nettoSetahunUnFix - $row['ptkp_total'];
            if($pembulatanPenghasilan > 0)
            {
                /* TAX 1 */
                if($maxTaxVal1 > 0)
                {
                    $tValUnFix = $pembulatanPenghasilan/$maxTaxVal1;  
                    if($tValUnFix >= 1){
                        $taxValUnFix1 = $maxTaxVal1 * ($taxPercent1/100);
                    }
                    else{
                        $taxValUnFix1 = $pembulatanPenghasilan * ($taxPercent1/100);
                    }    
                }    
                
                /* TAX 2 */
                if($maxTaxVal2 > 0)
                {
                    if($pembulatanPenghasilan > $maxTaxVal1)
                    {
                        $tSisaUnFix = $pembulatanPenghasilan - $maxTaxVal1; 
                        $tValUnFix = $tSisaUnFix/$maxTaxVal2;
                        if($tValUnFix >= 1){
                            $taxValUnFix2 = $maxTaxVal2 * ($taxPercent2/100);
                        }
                        else{
                            $taxValUnFix2 = $tSisaUnFix * ($taxPercent2/100);
                        }
                    }     
                }
                 
                /* TAX 3 */
                if($maxTaxVal3 > 0)
                {
                    if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2))
                    {
                        $tSisaUnFix = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2;
                        $tValUnFix = $tSisaUnFix/$maxTaxVal3;
                        if($tValUnFix >= 1){
                            $taxValUnFix3 = $maxTaxVal3 * ($taxPercent3/100);
                        }
                        else{
                            $taxValUnFix3 = $tSisaUnFix * ($taxPercent3/100);
                        }
                    }    
                }
                 
                /* TAX 4 */
                if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2 + $maxTaxVal3))
                {
                    $tSisaUnFix = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2 - $maxTaxVal3;
                    $taxValUnFix4 = $tSisaUnFix * ($taxPercent4/100); 
                }               
            }

            /* END THR - BONUS TAX -- EDITED @ AUG 15, 2017 BY FINANCE */

            $taxTotalUnFix = ( ($taxValUnFix1 + $taxValUnFix2 + $taxValUnFix3 + $taxValUnFix4) - $taxTotalFix );            
            

            $npwpNo = $row['npwp_no'];
            $npwpCharge = 0;
            if( strlen($npwpNo) < 19 ){
                $npwpCharge = $this->M_payroll_config->getNpwpCharge();
                $monthlyTax = $monthlyTax + ($monthlyTax * ($npwpCharge/100) ); 
                $taxTotalUnFix = $taxTotalUnFix + ($taxTotalUnFix * ($npwpCharge/100) ); 
            }



            $taxTotal = $monthlyTax + $taxTotalUnFix; 


            
            $tmpTotal = $row['total'] - floor($taxTotal) - $row['debt_burden'];
            $totalTerima = 0;
            if ($tmpTotal > 0)
            {
                $totalTerima = $tmpTotal;
            }

            /* START UPDATE TAX VALUE TO THE TABLE */
            if($row['tax_value'] <= 0)
            {
                $slipId = $row['salary_slip_id'];
                $str = "UPDATE trn_salary_slip SET tax_value = ".$taxTotal." WHERE salary_slip_id = '".$slipId."' ";
                $this->db->query($str);                
            }
            /* END UPDATE TAX VALUE TO THE TABLE */
            $spreadsheet->getActiveSheet()
                ->setCellValue('A'.$rowIdx, $row['account_no'])
                ->setCellValue('B'.$rowIdx, $row['account_name'])
                ->setCellValue('C'.$rowIdx, round($totalTerima))
                ->setCellValue('D'.$rowIdx, $row['bank_name'])
                ;

            if($rowIdx % 2 == 1)
            {
                $spreadsheet->getActiveSheet()->getStyle('A'.($rowIdx+1).':D'.($rowIdx+1))
                    ->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB('EAEBAF');             
            } 
        }

        $spreadsheet->getActiveSheet()
            ->setCellValue('A'.($rowIdx+1), 'TOTAL')
            ->setCellValue('C'.($rowIdx+1), '=SUM(C'.$startIdx.':C'.$rowIdx.')')
            ;
        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+1).":D".($rowIdx+1))->getFont()->setBold(true)->setSize(12);
        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+1).":D".($rowIdx+1))->applyFromArray($outlineBorderStyle);

        $spreadsheet->getActiveSheet()->getStyle('C8:C'.($rowIdx+1))->getNumberFormat()->setFormatCode('#,##0.00');       
        
        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+1).":D".($rowIdx+1))
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B');

        
        unset($allBorderStyle);
        unset($center);
        unset($right);
        unset($left);
        
        $spreadsheet->setActiveSheetIndex(0);

        $str = '"'.$pt.'" PaymentList';
        $fileName = preg_replace('/\s+/', '', $str);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.Xlsx"');
        // header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        /* BY COMPOSER */
        // $writer = new Xlsx($spreadsheet);
        /* OFFLINE/ BY COPY EXCEL FOLDER  */
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit(0);   
    }

    // dicopy dari Payroll.php // exportPaymentList
    public function exportBni($site,$clientName,$yearPeriod,$bank,$dept,$dataprint,$monthPeriod)
    {   
        $site       = $site;
        $pt         = $clientName;
        $year       = $yearPeriod;
        $month      = $monthPeriod;
        $bank       = $bank;
        $dept       = $dept;
        $dataprint  = $dataprint;
        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();  
      
        $strSQL = '';
        if ($pt == 'All'){
            $strSQL  = "SELECT a.* FROM 
            (SELECT 0 jumboPercent,0 jumboVal,ss.salary_slip_id,mb.bio_rec_id,mb.full_name,ms.nie,mb.position,ms.bank_code,ms.bank_name,mb.npwp_no,
            ms.account_name,ms.account_no,ss.ptkp_total,ss.thr,ss.contract_bonus,ss.emp_jht,ss.emp_jp,ss.cc_payment,ss.debt_burden,
            ms.payroll_group,ss.tax_value,ss.client_name,
            (CAST(ss.bs_prorate+ss.ot_1+ss.ot_2+ss.ot_3+ss.ot_4+  
                CASE WHEN(ms.is_travel = 1) THEN ss.travel_bonus ELSE 0 END + 
                CASE WHEN(ms.is_allowance_economy = 1) THEN ss.allowance_economy ELSE 0 END + 
                CASE WHEN(ms.is_incentive_bonus = 1) THEN ss.incentive_bonus ELSE 0 END + 
                CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END + 
                CASE WHEN(ms.is_production = 1) THEN ss.production_bonus ELSE 0 END + 
            ss.workday_adj+ ss.jkk_jkm+ ss.health_bpjs- ss.unpaid_total+ ss.adjust_in- ss.adjust_out- ss.jkk_jkm- ss.emp_jht- 
            ss.emp_health_bpjs- ss.health_bpjs- ss.emp_jp AS DECIMAL(11,2))   ) AS total,  
            ( CAST( ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 +  
                CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END + 
                CASE WHEN(ms.is_allowance_economy = 1) THEN ss.allowance_economy ELSE 0 END + 
                CASE WHEN(ms.is_incentive_bonus = 1) THEN ss.incentive_bonus ELSE 0 END + 
                CASE WHEN(ms.is_production = 1) THEN ss.production_bonus ELSE 0 END + 
                CASE WHEN(ms.is_travel = 1) THEN ss.travel_bonus ELSE 0 END + 
                CASE WHEN(ms.is_cc_payment = 1) THEN ss.cc_payment ELSE 0 END + 
            ss.workday_adj+ ss.thr+ ss.jkk_jkm+ ss.health_bpjs- ss.unpaid_total+ ss.adjust_in- ss.adjust_out AS DECIMAL(11,2)))AS brutto_tax 
            FROM mst_salary ms,mst_bio_rec mb,trn_salary_slip ss 
            WHERE ms.company_name LIKE '%".$site."' 
            AND ss.month_period = '".$monthPeriod."' AND ss.year_period = '".$yearPeriod."' AND ms.bio_rec_id = mb.bio_rec_id  
            AND mb.bio_rec_id = ss.bio_rec_id AND mb.is_active = 1 AND ms.company_name ='Pontil_Timika'
            UNION
            SELECT ss.other_allowance1 AS jumboPercent,(ss.dev_incentive_bonus*(ss.dev_percent/100))* (ss.other_allowance1/100) AS jumboVal,ss.salary_slip_id,mb1.bio_rec_id,mb1.full_name,ms.nie,mb1.position,ms.bank_code,ms.bank_name,ms.account_name,ms.account_no,ss.ptkp_total,ss.thr,ss.contract_bonus,ss.emp_jht,ss.emp_jp,ss.cc_payment,ss.debt_burden,ms.payroll_group,ss.tax_value,ss.client_name,
            (CAST( CASE WHEN(ss.basic_salary = ss.bs_prorate) THEN ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4  
                ELSE ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 END +  
                CASE WHEN(ms.is_ot_bonus = 1) THEN ss.ot_bonus ELSE 0 END + 
                CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END + 
                CASE WHEN(ms.is_remote_allowance = 1) THEN ss.remote_allowance ELSE 0 END + 
                CASE WHEN(ms.is_dev_incentive_bonus = 1) THEN ss.dev_incentive_bonus*(ss.dev_percent/100) ELSE 0 END + 
            (ss.dev_incentive_bonus*(ss.dev_percent/100))*(ss.other_allowance1/100) + ss.workday_adj + ss.no_accident_bonus+ ss.jkk_jkm + ss.health_bpjs - ss.unpaid_total + ss.adjust_in - ss.adjust_out- ss.jkk_jkm- ss.emp_jht- ss.emp_health_bpjs- ss.health_bpjs- 
            ss.emp_jp AS DECIMAL(11,2))) AS total,  
            (CAST( CASE WHEN(ss.basic_salary = ss.bs_prorate) THEN ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4  
                ELSE ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 END +  
                CASE WHEN(ms.is_ot_bonus = 1) THEN ss.ot_bonus ELSE 0 END + 
                CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END + 
                CASE WHEN(ms.is_remote_allowance = 1) THEN ss.remote_allowance ELSE 0 END + ss.thr+ 
                CASE WHEN(ms.is_dev_incentive_bonus = 1) THEN ss.dev_incentive_bonus*(ss.dev_percent/100) ELSE 0 END + 
            (ss.dev_incentive_bonus*(ss.dev_percent/100))*(ss.other_allowance1/100) + ss.contract_bonus+ ss.workday_adj+ 
            ss.no_accident_bonus+ss.jkk_jkm+ss.health_bpjs- ss.unpaid_total+ ss.adjust_in- ss.adjust_out AS DECIMAL(11,2))) AS brutto_tax
            FROM mst_salary ms,mst_bio_rec mb1,trn_salary_slip ss 
            WHERE ms.company_name LIKE '%".$site."' AND ss.month_period = '".$monthPeriod."' AND ss.year_period = '".$yearPeriod."' 
            AND ms.bio_rec_id = mb1.bio_rec_id  AND mb1.bio_rec_id = ss.bio_rec_id AND mb1.is_active = 1 
            AND ms.company_name ='Redpath_Timika') AS a
            ORDER BY a.full_name "; 
        }

        else if($pt == "Redpath_Timika")
        {
            $strSQL  = "SELECT ss.other_allowance1 AS jumboPercent,(ss.dev_incentive_bonus*(ss.dev_percent/100))*(ss.other_allowance1/100) AS jumboVal,ss.salary_slip_id,mb.bio_rec_id,mb.full_name,ms.nie,mb.position,ms.bank_code,ms.bank_name,ms.account_name,ms.account_no,ss.ptkp_total,ss.thr,ss.contract_bonus,ss.emp_jht,ss.emp_jp,ss.cc_payment,ss.debt_burden,ms.payroll_group,ss.tax_value,ss.client_name,mb.npwp_no,";
            $strSQL .= " (";
            $strSQL .= " CAST("; /*START CAST*/
            $strSQL .= " CASE WHEN(ss.basic_salary = ss.bs_prorate) THEN ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 ";
            $strSQL .= " ELSE ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 END + ";
            $strSQL .= " CASE WHEN(ms.is_ot_bonus = 1) THEN ss.ot_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_remote_allowance = 1) THEN ss.remote_allowance ELSE 0 END +";
            // $strSQL .= " ss.thr+";
            $strSQL .= " CASE WHEN(ms.is_dev_incentive_bonus = 1) THEN ss.dev_incentive_bonus*(ss.dev_percent/100) ELSE 0 END +";
            $strSQL .= " (ss.dev_incentive_bonus*(ss.dev_percent/100))*(ss.other_allowance1/100) +";
            $strSQL .= " ss.workday_adj+";
            $strSQL .= " ss.no_accident_bonus+";
            $strSQL .= " ss.jkk_jkm+";
            $strSQL .= " ss.health_bpjs-";
            $strSQL .= " ss.unpaid_total+";
            $strSQL .= " ss.adjust_in-";
            $strSQL .= " ss.adjust_out-";  
            $strSQL .= " ss.jkk_jkm-";
            $strSQL .= " ss.emp_jht-";
            $strSQL .= " ss.emp_health_bpjs-";
            $strSQL .= " ss.health_bpjs-";
            $strSQL .= " ss.emp_jp";
            $strSQL .= " AS DECIMAL(11,2))  "; /*END CAST*/
            $strSQL .= " ) AS total, ";

            /* START BRUTTO */
            $strSQL .= " (";
            $strSQL .= " CAST("; /*START CAST*/
            $strSQL .= " CASE WHEN(ss.basic_salary = ss.bs_prorate) THEN ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 ";
            $strSQL .= " ELSE ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 END + ";
            $strSQL .= " CASE WHEN(ms.is_ot_bonus = 1) THEN ss.ot_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_remote_allowance = 1) THEN ss.remote_allowance ELSE 0 END +";
            $strSQL .= " ss.thr+";
            $strSQL .= " CASE WHEN(ms.is_dev_incentive_bonus = 1) THEN ss.dev_incentive_bonus*(ss.dev_percent/100) ELSE 0 END +";
            $strSQL .= " (ss.dev_incentive_bonus*(ss.dev_percent/100))*(ss.other_allowance1/100) +";
            $strSQL .= " ss.contract_bonus+";
            $strSQL .= " ss.workday_adj+";
            $strSQL .= " ss.no_accident_bonus+";
            $strSQL .= " ss.jkk_jkm+";
            $strSQL .= " ss.health_bpjs-";
            $strSQL .= " ss.unpaid_total+";
            $strSQL .= " ss.adjust_in-";
            $strSQL .= " ss.adjust_out "; 
            $strSQL .= " AS DECIMAL(11,2))  "; /*END CAST*/
            $strSQL .= " ) AS brutto_tax ";
            /* END BRUTTO */
            $strSQL .= "FROM mst_salary ms,mst_bio_rec mb,trn_salary_slip ss ";
            $strSQL .= "WHERE ms.company_name LIKE '%".$site."' ";
            $strSQL .= "AND ss.month_period = '".$monthPeriod."' ";
            $strSQL .= "AND ss.year_period = '".$yearPeriod."' ";
            $strSQL .= "AND ss.dept = '".$dept."' ";
            $strSQL .= "AND ms.bank_name = '".$bank."' ";
            $strSQL .= "AND ms.bio_rec_id = mb.bio_rec_id  ";
            $strSQL .= "AND mb.bio_rec_id = ss.bio_rec_id ";
            $strSQL .= "AND mb.is_active = 1 ";      
            $strSQL .= "AND ms.company_name = 'Redpath_Timika' ";      
            $strSQL .= "ORDER BY mb.full_name ";      
        }
        else if($pt == "Pontil_Timika")
        {           

            $strSQL  = "SELECT 0 jumboPercent,0 jumboVal,ss.salary_slip_id,mb.bio_rec_id,mb.full_name,ms.nie,mb.position,ms.bank_code,ms.bank_name,ms.account_name,ms.account_no,ss.ptkp_total,ss.thr,ss.contract_bonus,ss.emp_jht,ss.emp_jp,ss.cc_payment,ss.debt_burden,ms.payroll_group,ss.tax_value,ss.client_name,mb.npwp_no,";
            $strSQL .= " (";
            $strSQL .= " CAST("; /*START CAST*/
            $strSQL .= " ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 + ";
            $strSQL .= " CASE WHEN(ms.is_travel = 1) THEN ss.travel_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_allowance_economy = 1) THEN ss.allowance_economy ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_incentive_bonus = 1) THEN ss.incentive_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_production = 1) THEN ss.production_bonus ELSE 0 END +";
           
            $strSQL .= " ss.workday_adj+";
            $strSQL .= " ss.jkk_jkm+";
            $strSQL .= " ss.health_bpjs-";
            $strSQL .= " ss.unpaid_total+";
            $strSQL .= " ss.adjust_in-";
            $strSQL .= " ss.adjust_out-";  
            $strSQL .= " ss.jkk_jkm-";
            $strSQL .= " ss.emp_jht-";
            $strSQL .= " ss.emp_health_bpjs-";
            $strSQL .= " ss.health_bpjs-";
            $strSQL .= " ss.emp_jp";
            $strSQL .= " AS DECIMAL(11,2))  "; /*END CAST*/
            $strSQL .= " ) AS total, ";

            /* START BRUTTO */
            $strSQL .= " (";
            $strSQL .= " CAST("; /*START CAST*/
            // $strSQL .= " ("; /*START SETAHUNKAN*/
            $strSQL .= " ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 + ";
            $strSQL .= " CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_allowance_economy = 1) THEN ss.allowance_economy ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_incentive_bonus = 1) THEN ss.incentive_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_production = 1) THEN ss.production_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_travel = 1) THEN ss.travel_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_cc_payment = 1) THEN ss.cc_payment ELSE 0 END +"; /*ok*/
            $strSQL .= " ss.workday_adj+";
            $strSQL .= " ss.thr+";
            $strSQL .= " ss.jkk_jkm+";
            $strSQL .= " ss.health_bpjs-";
            $strSQL .= " ss.unpaid_total+";
            $strSQL .= " ss.adjust_in-";
            $strSQL .= " ss.adjust_out";
            $strSQL .= " AS DECIMAL(11,2))  "; /*END CAST*/
            $strSQL .= " ) AS brutto_tax ";
            /* END BRUTTO */

            $strSQL .= "FROM mst_salary ms,mst_bio_rec mb,trn_salary_slip ss ";
            $strSQL .= "WHERE ms.company_name LIKE '%".$site."' ";
            $strSQL .= "AND ss.month_period = '".$monthPeriod."' ";
            $strSQL .= "AND ss.year_period = '".$yearPeriod."' ";
            $strSQL .= "AND ss.payroll_group = '".$dept."' ";
            $strSQL .= "AND ms.bank_name = '".$bank."' ";
            $strSQL .= "AND ms.bio_rec_id = mb.bio_rec_id  ";
            $strSQL .= "AND mb.bio_rec_id = ss.bio_rec_id ";
            $strSQL .= "AND mb.is_active = 1 ";   
            $strSQL .= "AND ms.company_name = 'Pontil_Timika' ";   
            $strSQL .= "ORDER BY mb.full_name ";      
        } 
        $query = $this->db->query($strSQL)->result_array();    
        $jmlrow = $this->db->query($strSQL)->num_rows();    
        $boldFont = [
            'font' => [
                'bold' => true
                // 'color' => ['argb' => '0000FF'],
            ],
        ];

        $totalStyle = [
            'font' => [
                'bold' => true,
                'color' => ['argb' => '0000FF'],
            ],
        ];

        $allBorderStyle = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $outlineBorderStyle = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $topBorderStyle = [
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $bottomBorderStyle = [
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        foreach(range('A','D') as $columnID)
        {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }           
        $myCurrentDate = GetCurrentDate();
        $inputTime = $myCurrentDate['CurrentDateTime'];

        // Nama Field Baris Pertama
        $spreadsheet->getActiveSheet()
                ->setCellValue('A5', 'File Creation')
                ->setCellValue('A6', ''.$inputTime.'')
                ->setCellValue('C5', 'Nama File')
                ->setCellValue('C6', ''.$dataprint.'')
                ->setCellValue('A7', 'P')
                ->setCellValue('A8', '')
                ->setCellValue('B7', 'Tgl Transaksi')
                ->setCellValue('B8', '')
                ;     

        /* SET HEADER BG COLOR*/
        $spreadsheet->getActiveSheet()->getStyle('A5:C5')
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B'); 
        $spreadsheet->getActiveSheet()->getStyle('A7:E7')
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B'); 
        $spreadsheet->getActiveSheet()->getStyle('A9:T9')
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B'); 

        /* START INVOICE TITLE */
        $spreadsheet->getActiveSheet()->getStyle("A5:C5")->getFont()->setSize(12);
        $spreadsheet->getActiveSheet()->getStyle("A7:E7")->getFont()->setSize(12);
        $spreadsheet->getActiveSheet()->getStyle("A9:T9")->getFont()->setSize(12);
        $spreadsheet->getActiveSheet()->getStyle("A5:C5")->applyFromArray($allBorderStyle);        
        $spreadsheet->getActiveSheet()->getStyle("A7:E7")->applyFromArray($allBorderStyle);        
        $spreadsheet->getActiveSheet()->getStyle("A9:T9")->applyFromArray($allBorderStyle);        

        $spreadsheet->getActiveSheet()->mergeCells("A5:B5");

        $spreadsheet->getActiveSheet()
                ->setCellValue('A9', 'Rek. Tujuan(16)')
                ->setCellValue('B9', 'Nama Penerima(40)')
                ->setCellValue('C9', 'Amount')
                ->setCellValue('D9', 'Remark1(33)')
                ->setCellValue('E9', 'Remark2(50)')
                ->setCellValue('F9', 'Remark3(50)')
                ->setCellValue('G9', 'KODEBANK(8)--(M)')
                ->setCellValue('H9', 'NAMA BANK TUJUAN(100)--(M)')
                ->setCellValue('I9', 'NAMA CABANG(100)')
                ->setCellValue('J9', 'ALAMAT BANK1(50)')
                ->setCellValue('K9', 'ALAMAT BANK2(50)')
                ->setCellValue('L9', 'ALAMAT BANK3(50)')
                ->setCellValue('M9', 'NAMA KOTA(100)')
                ->setCellValue('N9', 'NAMA NEGARA(100)')
                ->setCellValue('O9', 'WARGA NEGARA(40)')
                ->setCellValue('P9', 'KODE WN(40)')
                ->setCellValue('Q9', 'EMAIL FLAG(1)')
                ->setCellValue('R9', 'Email(100)')
                ->setCellValue('S9', 'Reff Num(16)')
                ->setCellValue('T9', 'FLAG(1)')
                ;

        /* START TOTAL WORK HOUR */
        $bruttoTax = 0;
        /* TAX CONFIG */
        $myConfigId = 0;    
        switch ($pt) {
            case 'Redpath_Timika':
                $myConfigId = 1;
                break;
            case 'Pontil_Timika':
                $myConfigId = 2;
                break;  
            case 'All':
                $myConfigId = 1;
                break;          
            default:
                break;
        }
        $row            = $this->M_payroll_config->getObjectById($myConfigId); 
        $taxPercent1    = $this->M_payroll_config->getTaxPercent1(); /* Persentase Progresif Pajak I */
        $taxPercent2    = $this->M_payroll_config->getTaxPercent2(); /* Persentase Progresif Pajak II */
        $taxPercent3    = $this->M_payroll_config->getTaxPercent3(); /* Persentase Progresif Pajak III */
        $taxPercent4    = $this->M_payroll_config->getTaxPercent4(); /* Persentase Progresif Pajak IV */
        $maxTaxVal1     = $this->M_payroll_config->getTaxMaxVal1(); /* Nominal Pajak I */ 
        $maxTaxVal2     = $this->M_payroll_config->getTaxMaxVal2(); /* Nominal Pajak II */
        $maxTaxVal3     = $this->M_payroll_config->getTaxMaxVal3(); /* Nominal Pajak III */
        $maxTaxVal4     = $this->M_payroll_config->getTaxMaxVal4(); /* Nominal Pajak IV */
        $maxNonTax      = $this->M_payroll_config->getNonTaxAllowance(); /* Max Tunjangan Jabatan */
        
        $rowIdx         = 9;
        $startIdx       = $rowIdx;
        foreach ($query as $row) {
            $rowIdx++;

            /* START UPDATE TAX */
            $bruttoTax = $row['brutto_tax'];
            $pembulatanPenghasilan = $bruttoTax;
            $nonTaxAllowance = ($pembulatanPenghasilan) * (5/100);
            if($nonTaxAllowance > $maxNonTax){
                $nonTaxAllowance = $maxNonTax;
            }

            $netto = $bruttoTax - $row['emp_jht'] - $row['emp_jp'] - $nonTaxAllowance;

            $unFixedIncome = 0;
            if($pt == "Pontil_Timika"){
                $unFixedIncome = $row['thr'] + $row['cc_payment'];
            }else if($pt == "Redpath_Timika"){
                $unFixedIncome = $row['thr'] + $row['contract_bonus'];
            }

            $nettoSetahun = ( ($netto - $unFixedIncome)*12 );
            $monthlyTax = 0;

            $taxVal1 = 0;    
            $taxVal2 = 0;    
            $taxVal3 = 0;    
            $taxVal4 = 0;

            $tVal = 0;
            $tSisa = 0;
            $pembulatanPenghasilan = $nettoSetahun - $row['ptkp_total'];
            if($pembulatanPenghasilan > 0)
            {
                /* TAX 1 */
                if($maxTaxVal1 > 0)
                {
                    $tVal = $pembulatanPenghasilan/$maxTaxVal1;  
                    if($tVal >= 1){
                        $taxVal1 = $maxTaxVal1 * ($taxPercent1/100);
                    }
                    else{
                        $taxVal1 = $pembulatanPenghasilan * ($taxPercent1/100);
                    }    
                }    
                
                /* TAX 2 */
                if($maxTaxVal2 > 0)
                {
                    if($pembulatanPenghasilan > $maxTaxVal1)
                    {
                        $tSisa = $pembulatanPenghasilan - $maxTaxVal1; 
                        $tVal = $tSisa/$maxTaxVal2;
                        if($tVal >= 1){
                            $taxVal2 = $maxTaxVal2 * ($taxPercent2/100);
                        }
                        else{
                            $taxVal2 = $tSisa * ($taxPercent2/100);
                        }
                    }     
                }
                 
                /* TAX 3 */
                if($maxTaxVal3 > 0)
                {
                    if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2))
                    {
                        $tSisa = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2;
                        $tVal = $tSisa/$maxTaxVal3;
                        if($tVal >= 1){
                            $taxVal3 = $maxTaxVal3 * ($taxPercent3/100);
                        }
                        else{
                            $taxVal3 = $tSisa * ($taxPercent3/100);
                        }
                    }    
                }
                 
                /* TAX 4 */
                if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2 + $maxTaxVal3))
                {
                    $tSisa = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2 - $maxTaxVal3;
                    $taxVal4 = $tSisa * ($taxPercent4/100); 
                }               
            }

            /* START THR - BONUS TAX -- EDITED @ AUG 15, 2017 BY FINANCE  */
            // $bonusTmp = $row['thr'] + $row['contract_bonus'];
            // $bonusTax = 0;
            // if( $taxVal4 > 0){
            //     $bonusTax = $bonusTmp * ($taxPercent4/100);
            // }else if( $taxVal3 > 0 ){
            //     $bonusTax = $bonusTmp * ($taxPercent3/100);
            // }else if( $taxVal2 > 0 ){
            //     $bonusTax = $bonusTmp * ($taxPercent2/100);                
            // }else{
            //     $bonusTax = $bonusTmp * ($taxPercent1/100);             
            // }

            /* END THR - BONUS TAX -- EDITED @ AUG 15, 2017 BY FINANCE */
            $taxTotalFix = $taxVal1 + $taxVal2 + $taxVal3 + $taxVal4;
            $taxTotal = ( $taxTotalFix/12 );
            if($taxTotal > 0){
                $monthlyTax = $taxTotal; 
            }else{
                $monthlyTax = 0; 
            }


            $nettoSetahunUnFix = ( ($netto - $unFixedIncome)*12 ) + $unFixedIncome;

            $taxTotalUnFix = 0;
            $taxValUnFix1 = 0;    
            $taxValUnFix2 = 0;    
            $taxValUnFix3 = 0;    
            $taxValUnFix4 = 0;

            $tValUnFix = 0;
            $tSisaUnFix = 0;
            $pembulatanPenghasilan = $nettoSetahunUnFix - $row['ptkp_total'];
            if($pembulatanPenghasilan > 0)
            {
                /* TAX 1 */
                if($maxTaxVal1 > 0)
                {
                    $tValUnFix = $pembulatanPenghasilan/$maxTaxVal1;  
                    if($tValUnFix >= 1){
                        $taxValUnFix1 = $maxTaxVal1 * ($taxPercent1/100);
                    }
                    else{
                        $taxValUnFix1 = $pembulatanPenghasilan * ($taxPercent1/100);
                    }    
                }    
                
                /* TAX 2 */
                if($maxTaxVal2 > 0)
                {
                    if($pembulatanPenghasilan > $maxTaxVal1)
                    {
                        $tSisaUnFix = $pembulatanPenghasilan - $maxTaxVal1; 
                        $tValUnFix = $tSisaUnFix/$maxTaxVal2;
                        if($tValUnFix >= 1){
                            $taxValUnFix2 = $maxTaxVal2 * ($taxPercent2/100);
                        }
                        else{
                            $taxValUnFix2 = $tSisaUnFix * ($taxPercent2/100);
                        }
                    }     
                }
                 
                /* TAX 3 */
                if($maxTaxVal3 > 0)
                {
                    if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2))
                    {
                        $tSisaUnFix = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2;
                        $tValUnFix = $tSisaUnFix/$maxTaxVal3;
                        if($tValUnFix >= 1){
                            $taxValUnFix3 = $maxTaxVal3 * ($taxPercent3/100);
                        }
                        else{
                            $taxValUnFix3 = $tSisaUnFix * ($taxPercent3/100);
                        }
                    }    
                }
                 
                /* TAX 4 */
                if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2 + $maxTaxVal3))
                {
                    $tSisaUnFix = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2 - $maxTaxVal3;
                    $taxValUnFix4 = $tSisaUnFix * ($taxPercent4/100); 
                }               
            }

            /* END THR - BONUS TAX -- EDITED @ AUG 15, 2017 BY FINANCE */

            $taxTotalUnFix = ( ($taxValUnFix1 + $taxValUnFix2 + $taxValUnFix3 + $taxValUnFix4) - $taxTotalFix );            
            

            $npwpNo = $row['npwp_no'];
            $npwpCharge = 0;
            if( strlen($npwpNo) < 19 ){
                $npwpCharge = $this->M_payroll_config->getNpwpCharge();
                $monthlyTax = $monthlyTax + ($monthlyTax * ($npwpCharge/100) ); 
                $taxTotalUnFix = $taxTotalUnFix + ($taxTotalUnFix * ($npwpCharge/100) ); 
            }

            $taxTotal = $monthlyTax + $taxTotalUnFix; 

            $tmpTotal = $row['total'] - floor($taxTotal) - $row['debt_burden'];
            $totalTerima = 0;
            if ($tmpTotal > 0)
            {
                $totalTerima = $tmpTotal;
            }

            /* START UPDATE TAX VALUE TO THE TABLE */
            if($row['tax_value'] <= 0)
            {
                $slipId = $row['salary_slip_id'];
                $str = "UPDATE trn_salary_slip SET tax_value = ".$taxTotal." WHERE salary_slip_id = '".$slipId."' ";
                $this->db->query($str);                
            }
            /* END UPDATE TAX VALUE TO THE TABLE */
            $spreadsheet->getActiveSheet()
                ->setCellValue('A'.$rowIdx, $row['account_no'])
                ->setCellValue('B'.$rowIdx, $row['account_name'])
                ->setCellValue('C'.$rowIdx, round($totalTerima))
                ;

            if($rowIdx % 2 == 1)
            {
                $spreadsheet->getActiveSheet()->getStyle('A'.($rowIdx+1).':T'.($rowIdx+1))
                    ->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB('EAEBAF');             
            } 
        }

        $spreadsheet->getActiveSheet()
                ->setCellValue('A7', 'P')
                ->setCellValue('A8', '')
                ->setCellValue('B7', 'Tgl Transaksi')
                ->setCellValue('B8', '')
                ->setCellValue('C7', 'Rek. Debet(16)')
                ->setCellValue('C8', '')
                ->setCellValue('D7', 'Total Record')
                ->setCellValue('D8', ''.$jmlrow.'')
                ->setCellValue('E7', 'Total Amount')
                ->setCellValue('E8', '=SUM(C'.$startIdx.':C'.$rowIdx.')')
                ;

        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+1).":T".($rowIdx+1))->getFont()->setBold(true)->setSize(12);
        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+1).":T".($rowIdx+1))->applyFromArray($outlineBorderStyle);

        $spreadsheet->getActiveSheet()->getStyle('C8:C'.($rowIdx+1))->getNumberFormat()->setFormatCode('#,##0.00');       
        $spreadsheet->getActiveSheet()->getStyle('E8')->getNumberFormat()->setFormatCode('#,##0.00');       
        
        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+1).":T".($rowIdx+1))
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B');
        
        unset($allBorderStyle);
        unset($center);
        unset($right);
        unset($left);
        
        $spreadsheet->setActiveSheetIndex(0);

        $str = '"'.$pt.'" PaymentList';
        $fileName = preg_replace('/\s+/', '', $str);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.Xlsx"');
        // header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        /* BY COMPOSER */
        // $writer = new Xlsx($spreadsheet);
        /* OFFLINE/ BY COPY EXCEL FOLDER  */
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit(0);   
    }

    public function exporthanyaBni($site,$clientName,$yearPeriod,$bank,$dept,$dataprint,$monthPeriod)
    {   
        // test('oke',1);
        $site       = $site;
        $pt         = $clientName;
        $year       = $yearPeriod;
        $month      = $monthPeriod;
        $bank       = $bank;
        $dept       = $dept;
        $dataprint  = $dataprint;
        $sqldept    = '';
        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();  
        if($dept != 'All' && $pt == 'Redpath_Timika'){
                $sqldept .= " AND ss.dept ='".$dept."' ";
            } 
        else if($dept != 'All' && $pt == 'Pontil_Timika'){
                $sqldept .= " AND ss.payroll_group = '".$dept."' ";
            }
        $strSQL = '';
        if ($pt == 'All'){
            $strSQL  = "SELECT a.* FROM 
            (SELECT 0 jumboPercent,0 jumboVal,ss.salary_slip_id,mb.bio_rec_id,mb.full_name,ms.nie,mb.position,ms.bank_code,ms.bank_name,mb.npwp_no,
            ms.account_name,ms.account_no,ss.ptkp_total,ss.thr,ss.contract_bonus,ss.emp_jht,ss.emp_jp,ss.cc_payment,ss.debt_burden,
            ms.payroll_group,ss.tax_value,ss.client_name,
            (CAST(ss.bs_prorate+ss.ot_1+ss.ot_2+ss.ot_3+ss.ot_4+  
                CASE WHEN(ms.is_travel = 1) THEN ss.travel_bonus ELSE 0 END + 
                CASE WHEN(ms.is_allowance_economy = 1) THEN ss.allowance_economy ELSE 0 END + 
                CASE WHEN(ms.is_incentive_bonus = 1) THEN ss.incentive_bonus ELSE 0 END + 
                CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END + 
                CASE WHEN(ms.is_production = 1) THEN ss.production_bonus ELSE 0 END + 
            ss.workday_adj+ ss.jkk_jkm+ ss.health_bpjs- ss.unpaid_total+ ss.adjust_in- ss.adjust_out- ss.jkk_jkm- ss.emp_jht- 
            ss.emp_health_bpjs- ss.health_bpjs- ss.emp_jp AS DECIMAL(11,2))   ) AS total,  
            ( CAST( ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 +  
                CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END + 
                CASE WHEN(ms.is_allowance_economy = 1) THEN ss.allowance_economy ELSE 0 END + 
                CASE WHEN(ms.is_incentive_bonus = 1) THEN ss.incentive_bonus ELSE 0 END + 
                CASE WHEN(ms.is_production = 1) THEN ss.production_bonus ELSE 0 END + 
                CASE WHEN(ms.is_travel = 1) THEN ss.travel_bonus ELSE 0 END + 
                CASE WHEN(ms.is_cc_payment = 1) THEN ss.cc_payment ELSE 0 END + 
            ss.workday_adj+ ss.thr+ ss.jkk_jkm+ ss.health_bpjs- ss.unpaid_total+ ss.adjust_in- ss.adjust_out AS DECIMAL(11,2)))AS brutto_tax 
            FROM mst_salary ms,mst_bio_rec mb,trn_salary_slip ss 
            WHERE ms.company_name LIKE '%".$site."' 
            AND ss.month_period = '".$monthPeriod."' AND ss.year_period = '".$yearPeriod."' AND ms.bio_rec_id = mb.bio_rec_id  
            AND mb.bio_rec_id = ss.bio_rec_id AND mb.is_active = 1 AND ms.company_name ='Pontil_Timika'
            UNION
            SELECT ss.other_allowance1 AS jumboPercent,(ss.dev_incentive_bonus*(ss.dev_percent/100))* (ss.other_allowance1/100) AS jumboVal,ss.salary_slip_id,mb1.bio_rec_id,mb1.full_name,ms.nie,mb1.position,ms.bank_code,ms.bank_name,ms.account_name,ms.account_no,ss.ptkp_total,ss.thr,ss.contract_bonus,ss.emp_jht,ss.emp_jp,ss.cc_payment,ss.debt_burden,ms.payroll_group,ss.tax_value,ss.client_name,
            (CAST( CASE WHEN(ss.basic_salary = ss.bs_prorate) THEN ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4  
                ELSE ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 END +  
                CASE WHEN(ms.is_ot_bonus = 1) THEN ss.ot_bonus ELSE 0 END + 
                CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END + 
                CASE WHEN(ms.is_remote_allowance = 1) THEN ss.remote_allowance ELSE 0 END + 
                CASE WHEN(ms.is_dev_incentive_bonus = 1) THEN ss.dev_incentive_bonus*(ss.dev_percent/100) ELSE 0 END + 
            (ss.dev_incentive_bonus*(ss.dev_percent/100))*(ss.other_allowance1/100) + ss.workday_adj + ss.no_accident_bonus+ ss.jkk_jkm + ss.health_bpjs - ss.unpaid_total + ss.adjust_in - ss.adjust_out- ss.jkk_jkm- ss.emp_jht- ss.emp_health_bpjs- ss.health_bpjs- 
            ss.emp_jp AS DECIMAL(11,2))) AS total,  
            (CAST( CASE WHEN(ss.basic_salary = ss.bs_prorate) THEN ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4  
                ELSE ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 END +  
                CASE WHEN(ms.is_ot_bonus = 1) THEN ss.ot_bonus ELSE 0 END + 
                CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END + 
                CASE WHEN(ms.is_remote_allowance = 1) THEN ss.remote_allowance ELSE 0 END + ss.thr+ 
                CASE WHEN(ms.is_dev_incentive_bonus = 1) THEN ss.dev_incentive_bonus*(ss.dev_percent/100) ELSE 0 END + 
            (ss.dev_incentive_bonus*(ss.dev_percent/100))*(ss.other_allowance1/100) + ss.contract_bonus+ ss.workday_adj+ 
            ss.no_accident_bonus+ss.jkk_jkm+ss.health_bpjs- ss.unpaid_total+ ss.adjust_in- ss.adjust_out AS DECIMAL(11,2))) AS brutto_tax
            FROM mst_salary ms,mst_bio_rec mb1,trn_salary_slip ss 
            WHERE ms.company_name LIKE '%".$site."' AND ss.month_period = '".$monthPeriod."' AND ss.year_period = '".$yearPeriod."' 
            AND ms.bio_rec_id = mb1.bio_rec_id  AND mb1.bio_rec_id = ss.bio_rec_id AND mb1.is_active = 1 
            AND ms.company_name ='Redpath_Timika') AS a
            ORDER BY a.full_name "; 
        }

        else if($pt == "Redpath_Timika")
        {
            $strSQL  = "SELECT ss.other_allowance1 AS jumboPercent,(ss.dev_incentive_bonus*(ss.dev_percent/100))*(ss.other_allowance1/100) AS jumboVal,ss.salary_slip_id,mb.bio_rec_id,mb.full_name,ms.nie,mb.position,ms.bank_code,ms.bank_name,ms.account_name,ms.account_no,ss.ptkp_total,ss.thr,ss.contract_bonus,ss.emp_jht,ss.emp_jp,ss.cc_payment,ss.debt_burden,ms.payroll_group,ss.tax_value,ss.client_name,mb.npwp_no,";
            $strSQL .= " (";
            $strSQL .= " CAST("; /*START CAST*/
            $strSQL .= " CASE WHEN(ss.basic_salary = ss.bs_prorate) THEN ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 ";
            $strSQL .= " ELSE ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 END + ";
            $strSQL .= " CASE WHEN(ms.is_ot_bonus = 1) THEN ss.ot_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_remote_allowance = 1) THEN ss.remote_allowance ELSE 0 END +";
            // $strSQL .= " ss.thr+";
            $strSQL .= " CASE WHEN(ms.is_dev_incentive_bonus = 1) THEN ss.dev_incentive_bonus*(ss.dev_percent/100) ELSE 0 END +";
            $strSQL .= " (ss.dev_incentive_bonus*(ss.dev_percent/100))*(ss.other_allowance1/100) +";
            $strSQL .= " ss.workday_adj+";
            $strSQL .= " ss.no_accident_bonus+";
            $strSQL .= " ss.jkk_jkm+";
            $strSQL .= " ss.health_bpjs-";
            $strSQL .= " ss.unpaid_total+";
            $strSQL .= " ss.adjust_in-";
            $strSQL .= " ss.adjust_out-";  
            $strSQL .= " ss.jkk_jkm-";
            $strSQL .= " ss.emp_jht-";
            $strSQL .= " ss.emp_health_bpjs-";
            $strSQL .= " ss.health_bpjs-";
            $strSQL .= " ss.emp_jp";
            $strSQL .= " AS DECIMAL(11,2))  "; /*END CAST*/
            $strSQL .= " ) AS total, ";

            /* START BRUTTO */
            $strSQL .= " (";
            $strSQL .= " CAST("; /*START CAST*/
            $strSQL .= " CASE WHEN(ss.basic_salary = ss.bs_prorate) THEN ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 ";
            $strSQL .= " ELSE ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 END + ";
            $strSQL .= " CASE WHEN(ms.is_ot_bonus = 1) THEN ss.ot_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_remote_allowance = 1) THEN ss.remote_allowance ELSE 0 END +";
            $strSQL .= " ss.thr+";
            $strSQL .= " CASE WHEN(ms.is_dev_incentive_bonus = 1) THEN ss.dev_incentive_bonus*(ss.dev_percent/100) ELSE 0 END +";
            $strSQL .= " (ss.dev_incentive_bonus*(ss.dev_percent/100))*(ss.other_allowance1/100) +";
            $strSQL .= " ss.contract_bonus+";
            $strSQL .= " ss.workday_adj+";
            $strSQL .= " ss.no_accident_bonus+";
            $strSQL .= " ss.jkk_jkm+";
            $strSQL .= " ss.health_bpjs-";
            $strSQL .= " ss.unpaid_total+";
            $strSQL .= " ss.adjust_in-";
            $strSQL .= " ss.adjust_out "; 
            $strSQL .= " AS DECIMAL(11,2))  "; /*END CAST*/
            $strSQL .= " ) AS brutto_tax ";
            /* END BRUTTO */
            $strSQL .= "FROM mst_salary ms,mst_bio_rec mb,trn_salary_slip ss ";
            $strSQL .= "WHERE ms.company_name LIKE '%".$site."' ";
            $strSQL .= "AND ss.month_period = '".$monthPeriod."' ";
            $strSQL .= "AND ss.year_period = '".$yearPeriod."' ";
            $strSQL .= "".$sqldept." ";
            $strSQL .= "AND ms.bank_name = '".$bank."' ";
            $strSQL .= "AND ms.bio_rec_id = mb.bio_rec_id  ";
            $strSQL .= "AND mb.bio_rec_id = ss.bio_rec_id ";
            $strSQL .= "AND mb.is_active = 1 ";      
            $strSQL .= "AND ms.company_name = 'Redpath_Timika' ";      
            $strSQL .= "ORDER BY mb.full_name ";      
        }
        else if($pt == "Pontil_Timika")
        {           

            $strSQL  = "SELECT 0 jumboPercent,0 jumboVal,ss.salary_slip_id,mb.bio_rec_id,mb.full_name,ms.nie,mb.position,ms.bank_code,ms.bank_name,ms.account_name,ms.account_no,ss.ptkp_total,ss.thr,ss.contract_bonus,ss.emp_jht,ss.emp_jp,ss.cc_payment,ss.debt_burden,ms.payroll_group,ss.tax_value,ss.client_name,mb.npwp_no,";
            $strSQL .= " (";
            $strSQL .= " CAST("; /*START CAST*/
            $strSQL .= " ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 + ";
            $strSQL .= " CASE WHEN(ms.is_travel = 1) THEN ss.travel_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_allowance_economy = 1) THEN ss.allowance_economy ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_incentive_bonus = 1) THEN ss.incentive_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_production = 1) THEN ss.production_bonus ELSE 0 END +";
           
            $strSQL .= " ss.workday_adj+";
            $strSQL .= " ss.jkk_jkm+";
            $strSQL .= " ss.health_bpjs-";
            $strSQL .= " ss.unpaid_total+";
            $strSQL .= " ss.adjust_in-";
            $strSQL .= " ss.adjust_out-";  
            $strSQL .= " ss.jkk_jkm-";
            $strSQL .= " ss.emp_jht-";
            $strSQL .= " ss.emp_health_bpjs-";
            $strSQL .= " ss.health_bpjs-";
            $strSQL .= " ss.emp_jp";
            $strSQL .= " AS DECIMAL(11,2))  "; /*END CAST*/
            $strSQL .= " ) AS total, ";

            /* START BRUTTO */
            $strSQL .= " (";
            $strSQL .= " CAST("; /*START CAST*/
            // $strSQL .= " ("; /*START SETAHUNKAN*/
            $strSQL .= " ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 + ";
            $strSQL .= " CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_allowance_economy = 1) THEN ss.allowance_economy ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_incentive_bonus = 1) THEN ss.incentive_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_production = 1) THEN ss.production_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_travel = 1) THEN ss.travel_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_cc_payment = 1) THEN ss.cc_payment ELSE 0 END +"; /*ok*/
            $strSQL .= " ss.workday_adj+";
            $strSQL .= " ss.thr+";
            $strSQL .= " ss.jkk_jkm+";
            $strSQL .= " ss.health_bpjs-";
            $strSQL .= " ss.unpaid_total+";
            $strSQL .= " ss.adjust_in-";
            $strSQL .= " ss.adjust_out";
            $strSQL .= " AS DECIMAL(11,2))  "; /*END CAST*/
            $strSQL .= " ) AS brutto_tax ";
            /* END BRUTTO */

            $strSQL .= "FROM mst_salary ms,mst_bio_rec mb,trn_salary_slip ss ";
            $strSQL .= "WHERE ms.company_name LIKE '%".$site."' ";
            $strSQL .= "AND ss.month_period = '".$monthPeriod."' ";
            $strSQL .= "AND ss.year_period = '".$yearPeriod."' ";
            $strSQL .= "".$sqldept." ";
            $strSQL .= "AND ms.bank_name = '".$bank."' ";
            $strSQL .= "AND ms.bio_rec_id = mb.bio_rec_id  ";
            $strSQL .= "AND mb.bio_rec_id = ss.bio_rec_id ";
            $strSQL .= "AND mb.is_active = 1 ";   
            $strSQL .= "AND ms.company_name = 'Pontil_Timika' ";   
            $strSQL .= "ORDER BY mb.full_name ";      
        } 
        $query = $this->db->query($strSQL)->result_array();    
        $jmlrow = $this->db->query($strSQL)->num_rows();    
        $boldFont = [
            'font' => [
                'bold' => true
                // 'color' => ['argb' => '0000FF'],
            ],
        ];

        $totalStyle = [
            'font' => [
                'bold' => true,
                'color' => ['argb' => '0000FF'],
            ],
        ];

        $allBorderStyle = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $outlineBorderStyle = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $topBorderStyle = [
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $bottomBorderStyle = [
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        // foreach(range('A','D') as $columnID)
        // {
        //     $spreadsheet->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        // }           
        $myCurrentDate = GetCurrentDate();
        $year       = $myCurrentDate['CurrentYear'];
        $month      = $myCurrentDate['CurrentMonth'];
        $day        = $myCurrentDate['CurrentDay'];
        $jam        = substr($myCurrentDate['CurrentTime'], 0,2);
        $menit      = substr($myCurrentDate['CurrentTime'], 3,2);
        $detik      = substr($myCurrentDate['CurrentTime'], 6,2);
        $tambahan   = $jmlrow+2;
        $inputTime  = $year.'/'.$month.'/'.$day.'_'.$jam.'.'.$menit.'.'.$detik;
        // Nama Field Baris Pertama
        $spreadsheet->getActiveSheet()
                ->setCellValue('A1', $inputTime.','.$tambahan.','.$dataprint.',,,,,,,,,,,,,,,,,')
                ;

        /* START TOTAL WORK HOUR */
        $bruttoTax = 0;
        /* TAX CONFIG */
        $myConfigId = 0;    
        switch ($pt) {
            case 'Redpath_Timika':
                $myConfigId = 1;
                break;
            case 'Pontil_Timika':
                $myConfigId = 2;
                break;  
            case 'All':
                $myConfigId = 1;
                break;          
            default:
                break;
        }
        // $row = '';
        // if ($pt == 'All'){
        //     $a = 1;
        //     $b = 2;
        //     $row        = $this->M_payroll_config->getObjectById($a); 
        //     $row        = $this->M_payroll_config->getObjectById($b); 
        // }
        // else{
        // }
        $row            = $this->M_payroll_config->getObjectById($myConfigId); 
        $taxPercent1    = $this->M_payroll_config->getTaxPercent1(); /* Persentase Progresif Pajak I */
        $taxPercent2    = $this->M_payroll_config->getTaxPercent2(); /* Persentase Progresif Pajak II */
        $taxPercent3    = $this->M_payroll_config->getTaxPercent3(); /* Persentase Progresif Pajak III */
        $taxPercent4    = $this->M_payroll_config->getTaxPercent4(); /* Persentase Progresif Pajak IV */
        $maxTaxVal1     = $this->M_payroll_config->getTaxMaxVal1(); /* Nominal Pajak I */ 
        $maxTaxVal2     = $this->M_payroll_config->getTaxMaxVal2(); /* Nominal Pajak II */
        $maxTaxVal3     = $this->M_payroll_config->getTaxMaxVal3(); /* Nominal Pajak III */
        $maxTaxVal4     = $this->M_payroll_config->getTaxMaxVal4(); /* Nominal Pajak IV */
        $maxNonTax      = $this->M_payroll_config->getNonTaxAllowance(); /* Max Tunjangan Jabatan */
            
        $sumTotal       = 0;
        $rowIdx         = 2;
        $startIdx       = $rowIdx;
        foreach ($query as $row) {
            $rowIdx++;

            /* START UPDATE TAX */
            $bruttoTax = $row['brutto_tax'];
            $pembulatanPenghasilan = $bruttoTax;
            $nonTaxAllowance = ($pembulatanPenghasilan) * (5/100);
            if($nonTaxAllowance > $maxNonTax){
                $nonTaxAllowance = $maxNonTax;
            }

            $netto = $bruttoTax - $row['emp_jht'] - $row['emp_jp'] - $nonTaxAllowance;

            $unFixedIncome = 0;
            if($pt == "Pontil_Timika"){
                $unFixedIncome = $row['thr'] + $row['cc_payment'];
            }else if($pt == "Redpath_Timika"){
                $unFixedIncome = $row['thr'] + $row['contract_bonus'];
            }

            $nettoSetahun = ( ($netto - $unFixedIncome)*12 );
            $monthlyTax = 0;

            $taxVal1 = 0;    
            $taxVal2 = 0;    
            $taxVal3 = 0;    
            $taxVal4 = 0;

            $tVal = 0;
            $tSisa = 0;
            $pembulatanPenghasilan = $nettoSetahun - $row['ptkp_total'];
            if($pembulatanPenghasilan > 0)
            {
                /* TAX 1 */
                if($maxTaxVal1 > 0)
                {
                    $tVal = $pembulatanPenghasilan/$maxTaxVal1;  
                    if($tVal >= 1){
                        $taxVal1 = $maxTaxVal1 * ($taxPercent1/100);
                    }
                    else{
                        $taxVal1 = $pembulatanPenghasilan * ($taxPercent1/100);
                    }    
                }    
                
                /* TAX 2 */
                if($maxTaxVal2 > 0)
                {
                    if($pembulatanPenghasilan > $maxTaxVal1)
                    {
                        $tSisa = $pembulatanPenghasilan - $maxTaxVal1; 
                        $tVal = $tSisa/$maxTaxVal2;
                        if($tVal >= 1){
                            $taxVal2 = $maxTaxVal2 * ($taxPercent2/100);
                        }
                        else{
                            $taxVal2 = $tSisa * ($taxPercent2/100);
                        }
                    }     
                }
                 
                /* TAX 3 */
                if($maxTaxVal3 > 0)
                {
                    if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2))
                    {
                        $tSisa = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2;
                        $tVal = $tSisa/$maxTaxVal3;
                        if($tVal >= 1){
                            $taxVal3 = $maxTaxVal3 * ($taxPercent3/100);
                        }
                        else{
                            $taxVal3 = $tSisa * ($taxPercent3/100);
                        }
                    }    
                }
                 
                /* TAX 4 */
                if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2 + $maxTaxVal3))
                {
                    $tSisa = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2 - $maxTaxVal3;
                    $taxVal4 = $tSisa * ($taxPercent4/100); 
                }               
            }

            /* START THR - BONUS TAX -- EDITED @ AUG 15, 2017 BY FINANCE  */
            // $bonusTmp = $row['thr'] + $row['contract_bonus'];
            // $bonusTax = 0;
            // if( $taxVal4 > 0){
            //     $bonusTax = $bonusTmp * ($taxPercent4/100);
            // }else if( $taxVal3 > 0 ){
            //     $bonusTax = $bonusTmp * ($taxPercent3/100);
            // }else if( $taxVal2 > 0 ){
            //     $bonusTax = $bonusTmp * ($taxPercent2/100);                
            // }else{
            //     $bonusTax = $bonusTmp * ($taxPercent1/100);             
            // }

            /* END THR - BONUS TAX -- EDITED @ AUG 15, 2017 BY FINANCE */
            $taxTotalFix = $taxVal1 + $taxVal2 + $taxVal3 + $taxVal4;
            $taxTotal = ( $taxTotalFix/12 );
            if($taxTotal > 0){
                $monthlyTax = $taxTotal; 
            }else{
                $monthlyTax = 0; 
            }


            $nettoSetahunUnFix = ( ($netto - $unFixedIncome)*12 ) + $unFixedIncome;

            $taxTotalUnFix = 0;
            $taxValUnFix1 = 0;    
            $taxValUnFix2 = 0;    
            $taxValUnFix3 = 0;    
            $taxValUnFix4 = 0;

            $tValUnFix = 0;
            $tSisaUnFix = 0;
            $pembulatanPenghasilan = $nettoSetahunUnFix - $row['ptkp_total'];
            if($pembulatanPenghasilan > 0)
            {
                /* TAX 1 */
                if($maxTaxVal1 > 0)
                {
                    $tValUnFix = $pembulatanPenghasilan/$maxTaxVal1;  
                    if($tValUnFix >= 1){
                        $taxValUnFix1 = $maxTaxVal1 * ($taxPercent1/100);
                    }
                    else{
                        $taxValUnFix1 = $pembulatanPenghasilan * ($taxPercent1/100);
                    }    
                }    
                
                /* TAX 2 */
                if($maxTaxVal2 > 0)
                {
                    if($pembulatanPenghasilan > $maxTaxVal1)
                    {
                        $tSisaUnFix = $pembulatanPenghasilan - $maxTaxVal1; 
                        $tValUnFix = $tSisaUnFix/$maxTaxVal2;
                        if($tValUnFix >= 1){
                            $taxValUnFix2 = $maxTaxVal2 * ($taxPercent2/100);
                        }
                        else{
                            $taxValUnFix2 = $tSisaUnFix * ($taxPercent2/100);
                        }
                    }     
                }
                 
                /* TAX 3 */
                if($maxTaxVal3 > 0)
                {
                    if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2))
                    {
                        $tSisaUnFix = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2;
                        $tValUnFix = $tSisaUnFix/$maxTaxVal3;
                        if($tValUnFix >= 1){
                            $taxValUnFix3 = $maxTaxVal3 * ($taxPercent3/100);
                        }
                        else{
                            $taxValUnFix3 = $tSisaUnFix * ($taxPercent3/100);
                        }
                    }    
                }
                 
                /* TAX 4 */
                if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2 + $maxTaxVal3))
                {
                    $tSisaUnFix = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2 - $maxTaxVal3;
                    $taxValUnFix4 = $tSisaUnFix * ($taxPercent4/100); 
                }               
            }

            /* END THR - BONUS TAX -- EDITED @ AUG 15, 2017 BY FINANCE */

            $taxTotalUnFix = ( ($taxValUnFix1 + $taxValUnFix2 + $taxValUnFix3 + $taxValUnFix4) - $taxTotalFix );            
            

            $npwpNo = $row['npwp_no'];
            $npwpCharge = 0;
            if( strlen($npwpNo) < 19 ){
                $npwpCharge = $this->M_payroll_config->getNpwpCharge();
                $monthlyTax = $monthlyTax + ($monthlyTax * ($npwpCharge/100) ); 
                $taxTotalUnFix = $taxTotalUnFix + ($taxTotalUnFix * ($npwpCharge/100) ); 
            }



            $taxTotal = $monthlyTax + $taxTotalUnFix; 


            
            $tmpTotal = $row['total'] - floor($taxTotal) - $row['debt_burden'];
            $totalTerima = 0;
            if ($tmpTotal > 0)
            {
                $totalTerima = $tmpTotal;
            }

            /* START UPDATE TAX VALUE TO THE TABLE */
            if($row['tax_value'] <= 0)
            {
                $slipId = $row['salary_slip_id'];
                $str = "UPDATE trn_salary_slip SET tax_value = ".$taxTotal." WHERE salary_slip_id = '".$slipId."' ";
                $this->db->query($str);                
            }
            $email1 = '';
            $email2 = '';
            $email3 = '';
            $email4 = '';

            if($pt == "Pontil_Timika"){
                $email1 = 'payroll.fin@sangati.co';
                $email2 = '';
                $email3 = '';
                $email4 = 'r_kalangie@sangati.co';
            }
            else if($pt == "Redpath_Timika"){
                $email1 = 'payroll.fin@sangati.co';
                $email2 = '';
                $email3 = '';
                $email4 = 'skrisman@fmi.com';
            }
            /* END UPDATE TAX VALUE TO THE TABLE */
            $sumTotal = $sumTotal+round($totalTerima);
            $spreadsheet->getActiveSheet()
                ->setCellValue('A'.$rowIdx, $row['account_no'].','.$row['account_name'].','.round($totalTerima).',,,,,,,,,,,,,,Y,payroll.fin@sangati.co')
                ->setCellValue('B'.$rowIdx, 'jino@sangati.co')
                ->setCellValue('C'.$rowIdx, 'putra@sangati.co')
                ->setCellValue('D'.$rowIdx, $email4)
                ;
        }
        // $set = '=SUM(C'.$startIdx.':C'.$rowIdx.')';
        $spreadsheet->getActiveSheet()
                ->setCellValue('A2', 'P, , ,'.$jmlrow.','.($sumTotal).',,,,,,,,,,,,,,,')
                ;
        
        unset($allBorderStyle);
        unset($center);
        unset($right);
        unset($left);
        
        $spreadsheet->setActiveSheetIndex(0);

        $str = '"'.$pt.'" PaymentList';
        $fileName = preg_replace('/\s+/', '', $str);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.Xlsx"');
        // header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        /* BY COMPOSER */
        // $writer = new Xlsx($spreadsheet);
        /* OFFLINE/ BY COPY EXCEL FOLDER  */
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit(0);   
    }

    public function exportMandiri($site,$clientName,$yearPeriod,$bank,$dept,$dataprint,$monthPeriod)
    {   
        $sumTotalMandiri = 0;
        $site       = $site;
        $pt         = $clientName;
        $year       = $yearPeriod;
        $month      = $monthPeriod;
        $bank       = $bank;
        $dept       = $dept;
        $dataprint  = $dataprint;
        $sqldept    = '';
        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();  
        if($dept != 'All' && $pt == 'Redpath_Timika'){
                $sqldept .= " AND ss.dept ='".$dept."' ";
            } 
        else if($dept != 'All' && $pt == 'Pontil_Timika'){
                $sqldept .= " AND ss.payroll_group = '".$dept."' ";
            } 
        $strSQL = '';
        if ($pt == 'All'){
            $strSQL  = "SELECT a.* FROM 
            (SELECT 0 jumboPercent,0 jumboVal,ss.salary_slip_id,mb.bio_rec_id,mb.full_name,ms.nie,mb.position,ms.bank_code,ms.bank_name,mb.npwp_no,
            ms.account_name,ms.account_no,ss.ptkp_total,ss.thr,ss.contract_bonus,ss.emp_jht,ss.emp_jp,ss.cc_payment,ss.debt_burden,
            ms.payroll_group,ss.tax_value,ss.client_name,
            (CAST(ss.bs_prorate+ss.ot_1+ss.ot_2+ss.ot_3+ss.ot_4+  
                CASE WHEN(ms.is_travel = 1) THEN ss.travel_bonus ELSE 0 END + 
                CASE WHEN(ms.is_allowance_economy = 1) THEN ss.allowance_economy ELSE 0 END + 
                CASE WHEN(ms.is_incentive_bonus = 1) THEN ss.incentive_bonus ELSE 0 END + 
                CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END + 
                CASE WHEN(ms.is_production = 1) THEN ss.production_bonus ELSE 0 END + 
            ss.workday_adj+ ss.jkk_jkm+ ss.health_bpjs- ss.unpaid_total+ ss.adjust_in- ss.adjust_out- ss.jkk_jkm- ss.emp_jht- 
            ss.emp_health_bpjs- ss.health_bpjs- ss.emp_jp AS DECIMAL(11,2))   ) AS total,  
            ( CAST( ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 +  
                CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END + 
                CASE WHEN(ms.is_allowance_economy = 1) THEN ss.allowance_economy ELSE 0 END + 
                CASE WHEN(ms.is_incentive_bonus = 1) THEN ss.incentive_bonus ELSE 0 END + 
                CASE WHEN(ms.is_production = 1) THEN ss.production_bonus ELSE 0 END + 
                CASE WHEN(ms.is_travel = 1) THEN ss.travel_bonus ELSE 0 END + 
                CASE WHEN(ms.is_cc_payment = 1) THEN ss.cc_payment ELSE 0 END + 
            ss.workday_adj+ ss.thr+ ss.jkk_jkm+ ss.health_bpjs- ss.unpaid_total+ ss.adjust_in- ss.adjust_out AS DECIMAL(11,2)))AS brutto_tax 
            FROM mst_salary ms,mst_bio_rec mb,trn_salary_slip ss 
            WHERE ms.company_name LIKE '%".$site."' 
            AND ss.month_period = '".$monthPeriod."' AND ss.year_period = '".$yearPeriod."' AND ms.bio_rec_id = mb.bio_rec_id  
            AND mb.bio_rec_id = ss.bio_rec_id AND mb.is_active = 1 AND ms.company_name ='Pontil_Timika'
            UNION
            SELECT ss.other_allowance1 AS jumboPercent,(ss.dev_incentive_bonus*(ss.dev_percent/100))* (ss.other_allowance1/100) AS jumboVal,ss.salary_slip_id,mb1.bio_rec_id,mb1.full_name,ms.nie,mb1.position,ms.bank_code,ms.bank_name,ms.account_name,ms.account_no,ss.ptkp_total,ss.thr,ss.contract_bonus,ss.emp_jht,ss.emp_jp,ss.cc_payment,ss.debt_burden,ms.payroll_group,ss.tax_value,ss.client_name,
            (CAST( CASE WHEN(ss.basic_salary = ss.bs_prorate) THEN ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4  
                ELSE ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 END +  
                CASE WHEN(ms.is_ot_bonus = 1) THEN ss.ot_bonus ELSE 0 END + 
                CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END + 
                CASE WHEN(ms.is_remote_allowance = 1) THEN ss.remote_allowance ELSE 0 END + 
                CASE WHEN(ms.is_dev_incentive_bonus = 1) THEN ss.dev_incentive_bonus*(ss.dev_percent/100) ELSE 0 END + 
            (ss.dev_incentive_bonus*(ss.dev_percent/100))*(ss.other_allowance1/100) + ss.workday_adj + ss.no_accident_bonus+ ss.jkk_jkm + ss.health_bpjs - ss.unpaid_total + ss.adjust_in - ss.adjust_out- ss.jkk_jkm- ss.emp_jht- ss.emp_health_bpjs- ss.health_bpjs- 
            ss.emp_jp AS DECIMAL(11,2))) AS total,  
            (CAST( CASE WHEN(ss.basic_salary = ss.bs_prorate) THEN ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4  
                ELSE ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 END +  
                CASE WHEN(ms.is_ot_bonus = 1) THEN ss.ot_bonus ELSE 0 END + 
                CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END + 
                CASE WHEN(ms.is_remote_allowance = 1) THEN ss.remote_allowance ELSE 0 END + ss.thr+ 
                CASE WHEN(ms.is_dev_incentive_bonus = 1) THEN ss.dev_incentive_bonus*(ss.dev_percent/100) ELSE 0 END + 
            (ss.dev_incentive_bonus*(ss.dev_percent/100))*(ss.other_allowance1/100) + ss.contract_bonus+ ss.workday_adj+ 
            ss.no_accident_bonus+ss.jkk_jkm+ss.health_bpjs- ss.unpaid_total+ ss.adjust_in- ss.adjust_out AS DECIMAL(11,2))) AS brutto_tax
            FROM mst_salary ms,mst_bio_rec mb1,trn_salary_slip ss 
            WHERE ms.company_name LIKE '%".$site."' AND ss.month_period = '".$monthPeriod."' AND ss.year_period = '".$yearPeriod."' 
            AND ms.bio_rec_id = mb1.bio_rec_id  AND mb1.bio_rec_id = ss.bio_rec_id AND mb1.is_active = 1 
            AND ms.company_name ='Redpath_Timika') AS a
            ORDER BY a.full_name "; 
        }

        else if($pt == "Redpath_Timika")
        {
            $strSQL  = "SELECT ss.other_allowance1 AS jumboPercent,(ss.dev_incentive_bonus*(ss.dev_percent/100))*(ss.other_allowance1/100) AS jumboVal,ss.salary_slip_id,mb.bio_rec_id,mb.full_name,ms.nie,mb.position,ms.bank_code,ms.bank_name,ms.account_name,ms.account_no,ss.ptkp_total,ss.thr,ss.contract_bonus,ss.emp_jht,ss.emp_jp,ss.cc_payment,ss.debt_burden,ms.payroll_group,ss.tax_value,ss.client_name,mb.npwp_no,";
            $strSQL .= " (";
            $strSQL .= " CAST("; /*START CAST*/
            $strSQL .= " CASE WHEN(ss.basic_salary = ss.bs_prorate) THEN ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 ";
            $strSQL .= " ELSE ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 END + ";
            $strSQL .= " CASE WHEN(ms.is_ot_bonus = 1) THEN ss.ot_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_remote_allowance = 1) THEN ss.remote_allowance ELSE 0 END +";
            // $strSQL .= " ss.thr+";
            $strSQL .= " CASE WHEN(ms.is_dev_incentive_bonus = 1) THEN ss.dev_incentive_bonus*(ss.dev_percent/100) ELSE 0 END +";
            
            $strSQL .= " (ss.dev_incentive_bonus*(ss.dev_percent/100))*(ss.other_allowance1/100) +";

            // $strSQL .= " ss.contract_bonus+";
            $strSQL .= " ss.workday_adj+";
            $strSQL .= " ss.no_accident_bonus+";
            $strSQL .= " ss.jkk_jkm+";
            $strSQL .= " ss.health_bpjs-";
            $strSQL .= " ss.unpaid_total+";
            $strSQL .= " ss.adjust_in-";
            $strSQL .= " ss.adjust_out-";  
            // $strSQL .= " ss.tax_value-";
            $strSQL .= " ss.jkk_jkm-";
            $strSQL .= " ss.emp_jht-";
            $strSQL .= " ss.emp_health_bpjs-";
            $strSQL .= " ss.health_bpjs-";
            $strSQL .= " ss.emp_jp";
            $strSQL .= " AS DECIMAL(11,2))  "; /*END CAST*/
            $strSQL .= " ) AS total, ";


            /* START BRUTTO */
            $strSQL .= " (";
            $strSQL .= " CAST("; /*START CAST*/
            $strSQL .= " CASE WHEN(ss.basic_salary = ss.bs_prorate) THEN ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 ";
            $strSQL .= " ELSE ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 END + ";
            $strSQL .= " CASE WHEN(ms.is_ot_bonus = 1) THEN ss.ot_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_remote_allowance = 1) THEN ss.remote_allowance ELSE 0 END +";
            $strSQL .= " ss.thr+";
            $strSQL .= " CASE WHEN(ms.is_dev_incentive_bonus = 1) THEN ss.dev_incentive_bonus*(ss.dev_percent/100) ELSE 0 END +";

            $strSQL .= " (ss.dev_incentive_bonus*(ss.dev_percent/100))*(ss.other_allowance1/100) +";

            $strSQL .= " ss.contract_bonus+";
            $strSQL .= " ss.workday_adj+";
            $strSQL .= " ss.no_accident_bonus+";
            $strSQL .= " ss.jkk_jkm+";
            $strSQL .= " ss.health_bpjs-";
            $strSQL .= " ss.unpaid_total+";
            $strSQL .= " ss.adjust_in-";
            $strSQL .= " ss.adjust_out "; 
           
            $strSQL .= " AS DECIMAL(11,2))  "; /*END CAST*/
            $strSQL .= " ) AS brutto_tax ";
            /* END BRUTTO */
            $strSQL .= "FROM mst_salary ms,mst_bio_rec mb,trn_salary_slip ss ";
            $strSQL .= "WHERE ms.company_name LIKE '%".$site."' ";
            $strSQL .= "AND ss.month_period = '".$monthPeriod."' ";
            $strSQL .= "AND ss.year_period = '".$yearPeriod."' ";
            $strSQL .= "".$sqldept." ";
            $strSQL .= "AND ms.bank_name = '".$bank."' ";
            $strSQL .= "AND ms.bio_rec_id = mb.bio_rec_id  ";
            $strSQL .= "AND mb.bio_rec_id = ss.bio_rec_id ";
            $strSQL .= "AND mb.is_active = 1 ";      
            $strSQL .= "AND ms.company_name = 'Redpath_Timika' ";      
            $strSQL .= "ORDER BY mb.full_name ";      
        }
        else if($pt == "Pontil_Timika")
        {           

            $strSQL  = "SELECT 0 jumboPercent,0 jumboVal,ss.salary_slip_id,mb.bio_rec_id,mb.full_name,ms.nie,mb.position,ms.bank_code,ms.bank_name,ms.account_name,ms.account_no,ss.ptkp_total,ss.thr,ss.contract_bonus,ss.emp_jht,ss.emp_jp,ss.cc_payment,ss.debt_burden,ms.payroll_group,ss.tax_value,ss.client_name,mb.npwp_no,";
            $strSQL .= " (";
            $strSQL .= " CAST("; /*START CAST*/
            $strSQL .= " ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 + ";
            $strSQL .= " CASE WHEN(ms.is_travel = 1) THEN ss.travel_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_allowance_economy = 1) THEN ss.allowance_economy ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_incentive_bonus = 1) THEN ss.incentive_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_production = 1) THEN ss.production_bonus ELSE 0 END +";
           
            $strSQL .= " ss.workday_adj+";
            // $strSQL .= " ss.thr+";
            $strSQL .= " ss.jkk_jkm+";
            $strSQL .= " ss.health_bpjs-";
            $strSQL .= " ss.unpaid_total+";
            $strSQL .= " ss.adjust_in-";
            $strSQL .= " ss.adjust_out-";  
            // $strSQL .= " ss.tax_value-";
            $strSQL .= " ss.jkk_jkm-";
            $strSQL .= " ss.emp_jht-";
            $strSQL .= " ss.emp_health_bpjs-";
            $strSQL .= " ss.health_bpjs-";
            $strSQL .= " ss.emp_jp";
            $strSQL .= " AS DECIMAL(11,2))  "; /*END CAST*/
            $strSQL .= " ) AS total, ";

            /* START BRUTTO */
            $strSQL .= " (";
            $strSQL .= " CAST("; /*START CAST*/
            // $strSQL .= " ("; /*START SETAHUNKAN*/
            $strSQL .= " ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 + ";
            $strSQL .= " CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_allowance_economy = 1) THEN ss.allowance_economy ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_incentive_bonus = 1) THEN ss.incentive_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_production = 1) THEN ss.production_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_travel = 1) THEN ss.travel_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_cc_payment = 1) THEN ss.cc_payment ELSE 0 END +"; /*ok*/
            // $strSQL .= " ss.contract_bonus+";
            $strSQL .= " ss.workday_adj+";
            $strSQL .= " ss.thr+";
            $strSQL .= " ss.jkk_jkm+";
            $strSQL .= " ss.health_bpjs-";
            $strSQL .= " ss.unpaid_total+";
            $strSQL .= " ss.adjust_in-";
            $strSQL .= " ss.adjust_out";
            // $strSQL .= " ss.tax_value-";
            // $strSQL .= " ss.jkk_jkm-";
            /*$strSQL .= " ss.emp_jht";*/
            // $strSQL .= " ss.emp_health_bpjs ";
            // $strSQL .= " ss.health_bpjs";
            // $strSQL .= " )*12"; /*END SETAHUNKAN*/
            $strSQL .= " AS DECIMAL(11,2))  "; /*END CAST*/
            $strSQL .= " ) AS brutto_tax ";
            /* END BRUTTO */

            $strSQL .= "FROM mst_salary ms,mst_bio_rec mb,trn_salary_slip ss ";
            $strSQL .= "WHERE ms.company_name LIKE '%".$site."' ";
            $strSQL .= "AND ss.month_period = '".$monthPeriod."' ";
            $strSQL .= "AND ss.year_period = '".$yearPeriod."' ";
            $strSQL .= "".$sqldept." ";
            $strSQL .= "AND ms.bank_name = '".$bank."' ";
            $strSQL .= "AND ms.bio_rec_id = mb.bio_rec_id  ";
            $strSQL .= "AND mb.bio_rec_id = ss.bio_rec_id ";
            $strSQL .= "AND mb.is_active = 1 ";   
            $strSQL .= "AND ms.company_name = 'Pontil_Timika' ";   
            $strSQL .= "ORDER BY mb.full_name ";      
        } 
        $query = $this->db->query($strSQL)->result_array();    
        $jmlrow = $this->db->query($strSQL)->num_rows();    
        $boldFont = [
            'font' => [
                'bold' => true
                // 'color' => ['argb' => '0000FF'],
            ],
        ];

        $totalStyle = [
            'font' => [
                'bold' => true,
                'color' => ['argb' => '0000FF'],
            ],
        ];

        $allBorderStyle = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $outlineBorderStyle = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $topBorderStyle = [
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $bottomBorderStyle = [
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        foreach(range('A','D') as $columnID)
        {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }           
        $myCurrentDate = GetCurrentDate();
        $inputTime = $myCurrentDate['CurrentDateTime'];

        /* START TOTAL WORK HOUR */
        $bruttoTax = 0;
        /* TAX CONFIG */
        $myConfigId = 0;    
        switch ($pt) {
            case 'Redpath_Timika':
                $myConfigId = 1;
                break;
            case 'Pontil_Timika':
                $myConfigId = 2;
                break;  
            case 'All':
                $myConfigId = 1;
                break;          
            default:
                break;
        }
        // $row = '';
        // if ($pt == 'All'){
        //     $a = 1;
        //     $b = 2;
        //     $row        = $this->M_payroll_config->getObjectById($a); 
        //     $row        = $this->M_payroll_config->getObjectById($b); 
        // }
        // else{
        // }
        $row = $this->M_payroll_config->getObjectById($myConfigId); 
        $taxPercent1 = $this->M_payroll_config->getTaxPercent1(); /* Persentase Progresif Pajak I */
        $taxPercent2 = $this->M_payroll_config->getTaxPercent2(); /* Persentase Progresif Pajak II */
        $taxPercent3 = $this->M_payroll_config->getTaxPercent3(); /* Persentase Progresif Pajak III */
        $taxPercent4 = $this->M_payroll_config->getTaxPercent4(); /* Persentase Progresif Pajak IV */
        $maxTaxVal1 = $this->M_payroll_config->getTaxMaxVal1(); /* Nominal Pajak I */ 
        $maxTaxVal2 = $this->M_payroll_config->getTaxMaxVal2(); /* Nominal Pajak II */
        $maxTaxVal3 = $this->M_payroll_config->getTaxMaxVal3(); /* Nominal Pajak III */
        $maxTaxVal4 = $this->M_payroll_config->getTaxMaxVal4(); /* Nominal Pajak IV */
        $maxNonTax = $this->M_payroll_config->getNonTaxAllowance(); /* Max Tunjangan Jabatan */
        $namabulan      = $this->namabulan($monthPeriod+1);
        $rowIdx         = 1;
        $startIdx       = $rowIdx;
        foreach ($query as $row) {
            $rowIdx++;

            /* START UPDATE TAX */
            $bruttoTax = $row['brutto_tax'];
            $pembulatanPenghasilan = $bruttoTax;
            $nonTaxAllowance = ($pembulatanPenghasilan) * (5/100);
            if($nonTaxAllowance > $maxNonTax){
                $nonTaxAllowance = $maxNonTax;
            }

            $netto = $bruttoTax - $row['emp_jht'] - $row['emp_jp'] - $nonTaxAllowance;

            $unFixedIncome = 0;
            if($pt == "Pontil_Timika"){
                $unFixedIncome = $row['thr'] + $row['cc_payment'];
            }else if($pt == "Redpath_Timika"){
                $unFixedIncome = $row['thr'] + $row['contract_bonus'];
            }

            $nettoSetahun = ( ($netto - $unFixedIncome)*12 );
            $monthlyTax = 0;

            $taxVal1 = 0;    
            $taxVal2 = 0;    
            $taxVal3 = 0;    
            $taxVal4 = 0;

            $tVal = 0;
            $tSisa = 0;
            $pembulatanPenghasilan = $nettoSetahun - $row['ptkp_total'];
            if($pembulatanPenghasilan > 0)
            {
                /* TAX 1 */
                if($maxTaxVal1 > 0)
                {
                    $tVal = $pembulatanPenghasilan/$maxTaxVal1;  
                    if($tVal >= 1){
                        $taxVal1 = $maxTaxVal1 * ($taxPercent1/100);
                    }
                    else{
                        $taxVal1 = $pembulatanPenghasilan * ($taxPercent1/100);
                    }    
                }    
                
                /* TAX 2 */
                if($maxTaxVal2 > 0)
                {
                    if($pembulatanPenghasilan > $maxTaxVal1)
                    {
                        $tSisa = $pembulatanPenghasilan - $maxTaxVal1; 
                        $tVal = $tSisa/$maxTaxVal2;
                        if($tVal >= 1){
                            $taxVal2 = $maxTaxVal2 * ($taxPercent2/100);
                        }
                        else{
                            $taxVal2 = $tSisa * ($taxPercent2/100);
                        }
                    }     
                }
                 
                /* TAX 3 */
                if($maxTaxVal3 > 0)
                {
                    if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2))
                    {
                        $tSisa = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2;
                        $tVal = $tSisa/$maxTaxVal3;
                        if($tVal >= 1){
                            $taxVal3 = $maxTaxVal3 * ($taxPercent3/100);
                        }
                        else{
                            $taxVal3 = $tSisa * ($taxPercent3/100);
                        }
                    }    
                }
                 
                /* TAX 4 */
                if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2 + $maxTaxVal3))
                {
                    $tSisa = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2 - $maxTaxVal3;
                    $taxVal4 = $tSisa * ($taxPercent4/100); 
                }               
            }

            /* START THR - BONUS TAX -- EDITED @ AUG 15, 2017 BY FINANCE  */
            // $bonusTmp = $row['thr'] + $row['contract_bonus'];
            // $bonusTax = 0;
            // if( $taxVal4 > 0){
            //     $bonusTax = $bonusTmp * ($taxPercent4/100);
            // }else if( $taxVal3 > 0 ){
            //     $bonusTax = $bonusTmp * ($taxPercent3/100);
            // }else if( $taxVal2 > 0 ){
            //     $bonusTax = $bonusTmp * ($taxPercent2/100);                
            // }else{
            //     $bonusTax = $bonusTmp * ($taxPercent1/100);             
            // }

            /* END THR - BONUS TAX -- EDITED @ AUG 15, 2017 BY FINANCE */
            $taxTotalFix = $taxVal1 + $taxVal2 + $taxVal3 + $taxVal4;
            $taxTotal = ( $taxTotalFix/12 );
            if($taxTotal > 0){
                $monthlyTax = $taxTotal; 
            }else{
                $monthlyTax = 0; 
            }


            $nettoSetahunUnFix = ( ($netto - $unFixedIncome)*12 ) + $unFixedIncome;

            $taxTotalUnFix = 0;
            $taxValUnFix1 = 0;    
            $taxValUnFix2 = 0;    
            $taxValUnFix3 = 0;    
            $taxValUnFix4 = 0;

            $tValUnFix = 0;
            $tSisaUnFix = 0;
            $pembulatanPenghasilan = $nettoSetahunUnFix - $row['ptkp_total'];
            if($pembulatanPenghasilan > 0)
            {
                /* TAX 1 */
                if($maxTaxVal1 > 0)
                {
                    $tValUnFix = $pembulatanPenghasilan/$maxTaxVal1;  
                    if($tValUnFix >= 1){
                        $taxValUnFix1 = $maxTaxVal1 * ($taxPercent1/100);
                    }
                    else{
                        $taxValUnFix1 = $pembulatanPenghasilan * ($taxPercent1/100);
                    }    
                }    
                
                /* TAX 2 */
                if($maxTaxVal2 > 0)
                {
                    if($pembulatanPenghasilan > $maxTaxVal1)
                    {
                        $tSisaUnFix = $pembulatanPenghasilan - $maxTaxVal1; 
                        $tValUnFix = $tSisaUnFix/$maxTaxVal2;
                        if($tValUnFix >= 1){
                            $taxValUnFix2 = $maxTaxVal2 * ($taxPercent2/100);
                        }
                        else{
                            $taxValUnFix2 = $tSisaUnFix * ($taxPercent2/100);
                        }
                    }     
                }
                 
                /* TAX 3 */
                if($maxTaxVal3 > 0)
                {
                    if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2))
                    {
                        $tSisaUnFix = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2;
                        $tValUnFix = $tSisaUnFix/$maxTaxVal3;
                        if($tValUnFix >= 1){
                            $taxValUnFix3 = $maxTaxVal3 * ($taxPercent3/100);
                        }
                        else{
                            $taxValUnFix3 = $tSisaUnFix * ($taxPercent3/100);
                        }
                    }    
                }
                 
                /* TAX 4 */
                if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2 + $maxTaxVal3))
                {
                    $tSisaUnFix = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2 - $maxTaxVal3;
                    $taxValUnFix4 = $tSisaUnFix * ($taxPercent4/100); 
                }               
            }

            /* END THR - BONUS TAX -- EDITED @ AUG 15, 2017 BY FINANCE */

            $taxTotalUnFix = ( ($taxValUnFix1 + $taxValUnFix2 + $taxValUnFix3 + $taxValUnFix4) - $taxTotalFix );            
        
            $npwpNo = $row['npwp_no'];
            $npwpCharge = 0;
            if( strlen($npwpNo) < 19 ){
                $npwpCharge = $this->M_payroll_config->getNpwpCharge();
                $monthlyTax = $monthlyTax + ($monthlyTax * ($npwpCharge/100) ); 
                $taxTotalUnFix = $taxTotalUnFix + ($taxTotalUnFix * ($npwpCharge/100) ); 
            }
            $taxTotal = $monthlyTax + $taxTotalUnFix; 
            $tmpTotal = $row['total'] - floor($taxTotal) - $row['debt_burden'];
            $totalTerima = 0;
            if ($tmpTotal > 0)
            {
                $totalTerima = $tmpTotal;
            }

            /* START UPDATE TAX VALUE TO THE TABLE */
            // if($row['tax_value'] <= 0)
            // {
            $slipId = $row['salary_slip_id'];
            $str = "UPDATE trn_salary_slip SET tax_value = ".$taxTotal." WHERE salary_slip_id = '".$slipId."' ";
            $this->db->query($str);                

            $namapt = '';
            if ($pt == 'Redpath_Timika'){
                $namapt = 'RDP';
            }
            else if($pt == 'Pontil_Timika'){
                $namapt = 'PTL';
            }
            else{
                $namapt = '';
            }
            $subtahun = substr($yearPeriod, 2,2);
            $email1 = '';
            $email2 = '';
            $email3 = '';
            $email4 = '';

            if($pt == "Pontil_Timika"){
                $email1 = 'payroll.fin@sangati.co';
                $email2 = '';
                $email3 = '';
                $email4 = 'r_kalangie@sangati.co';
            }
            else if($pt == "Redpath_Timika"){
                $email1 = 'payroll.fin@sangati.co';
                $email2 = '';
                $email3 = '';
                $email4 = 'skrisman@fmi.com';
            }
            $sumTotalMandiri = $sumTotalMandiri+round($totalTerima);
            /* END UPDATE TAX VALUE TO THE TABLE */
            $spreadsheet->getActiveSheet()
                ->setCellValue('A'.$rowIdx, $row['account_no'].','.$row['account_name'].',,,,'.'IDR'.','.round($totalTerima).','.$namabulan.'-'.$subtahun.',,'.'IBU'.',,,,,,,'.'Y,'.'payroll.fin@sangati.co')
                ->setCellValue('B'.$rowIdx, 'jino@sangati.co')
                ->setCellValue('C'.$rowIdx, 'putra@sangati.co')
                ->setCellValue('D'.$rowIdx, $email4.',,,,,,,,,,,,,,,,,,,,,,,'.$dataprint.' '.$namapt.' GROUP '.$row['payroll_group'].' '.$namabulan.' '.$yearPeriod)
                ;
        }

        $spreadsheet->getActiveSheet()
                ->setCellValue('A1', 'P, , ,'.$jmlrow.','.($sumTotalMandiri).',,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,')
                ;
        unset($allBorderStyle);
        unset($center);
        unset($right);
        unset($left);
        
        $spreadsheet->setActiveSheetIndex(0);

        $str = '"'.$pt.'" PaymentList';
        $fileName = preg_replace('/\s+/', '', $str);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.Xlsx"');
        // header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        /* BY COMPOSER */
        // $writer = new Xlsx($spreadsheet);
        /* OFFLINE/ BY COPY EXCEL FOLDER  */
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit(0);   
    }

    public function exportLain($site,$clientName,$yearPeriod,$bank,$dept,$dataprint,$monthPeriod)
    {   
        // test('oke',1);
        $site       = $site;
        $pt         = $clientName;
        $year       = $yearPeriod;
        $month      = $monthPeriod;
        $bank       = $bank;
        $dept       = $dept;
        $dataprint  = $dataprint;
        $sqldept    = '';
        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();  
        
        if($dept != 'All' && $pt == 'Redpath_Timika'){
                $sqldept .= " AND ss.dept ='".$dept."' ";
            } 
        else if($dept != 'All' && $pt == 'Pontil_Timika'){
                $sqldept .= " AND ss.payroll_group = '".$dept."' ";
            }

        $strSQL = '';
        if ($pt == 'All'){
            $strSQL  = "SELECT a.* FROM 
            (SELECT 0 jumboPercent,0 jumboVal,ss.salary_slip_id,mb.bio_rec_id,mb.full_name,ms.nie,mb.position,ms.bank_code,ms.bank_name,mb.npwp_no,
            ms.account_name,ms.account_no,ss.ptkp_total,ss.thr,ss.contract_bonus,ss.emp_jht,ss.emp_jp,ss.cc_payment,ss.debt_burden,
            ms.payroll_group,ss.tax_value,ss.client_name,
            (CAST(ss.bs_prorate+ss.ot_1+ss.ot_2+ss.ot_3+ss.ot_4+  
                CASE WHEN(ms.is_travel = 1) THEN ss.travel_bonus ELSE 0 END + 
                CASE WHEN(ms.is_allowance_economy = 1) THEN ss.allowance_economy ELSE 0 END + 
                CASE WHEN(ms.is_incentive_bonus = 1) THEN ss.incentive_bonus ELSE 0 END + 
                CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END + 
                CASE WHEN(ms.is_production = 1) THEN ss.production_bonus ELSE 0 END + 
            ss.workday_adj+ ss.jkk_jkm+ ss.health_bpjs- ss.unpaid_total+ ss.adjust_in- ss.adjust_out- ss.jkk_jkm- ss.emp_jht- 
            ss.emp_health_bpjs- ss.health_bpjs- ss.emp_jp AS DECIMAL(11,2))   ) AS total,  
            ( CAST( ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 +  
                CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END + 
                CASE WHEN(ms.is_allowance_economy = 1) THEN ss.allowance_economy ELSE 0 END + 
                CASE WHEN(ms.is_incentive_bonus = 1) THEN ss.incentive_bonus ELSE 0 END + 
                CASE WHEN(ms.is_production = 1) THEN ss.production_bonus ELSE 0 END + 
                CASE WHEN(ms.is_travel = 1) THEN ss.travel_bonus ELSE 0 END + 
                CASE WHEN(ms.is_cc_payment = 1) THEN ss.cc_payment ELSE 0 END + 
            ss.workday_adj+ ss.thr+ ss.jkk_jkm+ ss.health_bpjs- ss.unpaid_total+ ss.adjust_in- ss.adjust_out AS DECIMAL(11,2)))AS brutto_tax 
            FROM mst_salary ms,mst_bio_rec mb,trn_salary_slip ss 
            WHERE ms.company_name LIKE '%".$site."' 
            AND ss.month_period = '".$monthPeriod."' AND ss.year_period = '".$yearPeriod."' AND ms.bio_rec_id = mb.bio_rec_id  
            AND mb.bio_rec_id = ss.bio_rec_id AND mb.is_active = 1 AND ms.company_name ='Pontil_Timika'
            UNION
            SELECT ss.other_allowance1 AS jumboPercent,(ss.dev_incentive_bonus*(ss.dev_percent/100))* (ss.other_allowance1/100) AS jumboVal,ss.salary_slip_id,mb1.bio_rec_id,mb1.full_name,ms.nie,mb1.position,ms.bank_code,ms.bank_name,ms.account_name,ms.account_no,ss.ptkp_total,ss.thr,ss.contract_bonus,ss.emp_jht,ss.emp_jp,ss.cc_payment,ss.debt_burden,ms.payroll_group,ss.tax_value,ss.client_name,
            (CAST( CASE WHEN(ss.basic_salary = ss.bs_prorate) THEN ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4  
                ELSE ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 END +  
                CASE WHEN(ms.is_ot_bonus = 1) THEN ss.ot_bonus ELSE 0 END + 
                CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END + 
                CASE WHEN(ms.is_remote_allowance = 1) THEN ss.remote_allowance ELSE 0 END + 
                CASE WHEN(ms.is_dev_incentive_bonus = 1) THEN ss.dev_incentive_bonus*(ss.dev_percent/100) ELSE 0 END + 
            (ss.dev_incentive_bonus*(ss.dev_percent/100))*(ss.other_allowance1/100) + ss.workday_adj + ss.no_accident_bonus+ ss.jkk_jkm + ss.health_bpjs - ss.unpaid_total + ss.adjust_in - ss.adjust_out- ss.jkk_jkm- ss.emp_jht- ss.emp_health_bpjs- ss.health_bpjs- 
            ss.emp_jp AS DECIMAL(11,2))) AS total,  
            (CAST( CASE WHEN(ss.basic_salary = ss.bs_prorate) THEN ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4  
                ELSE ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 END +  
                CASE WHEN(ms.is_ot_bonus = 1) THEN ss.ot_bonus ELSE 0 END + 
                CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END + 
                CASE WHEN(ms.is_remote_allowance = 1) THEN ss.remote_allowance ELSE 0 END + ss.thr+ 
                CASE WHEN(ms.is_dev_incentive_bonus = 1) THEN ss.dev_incentive_bonus*(ss.dev_percent/100) ELSE 0 END + 
            (ss.dev_incentive_bonus*(ss.dev_percent/100))*(ss.other_allowance1/100) + ss.contract_bonus+ ss.workday_adj+ 
            ss.no_accident_bonus+ss.jkk_jkm+ss.health_bpjs- ss.unpaid_total+ ss.adjust_in- ss.adjust_out AS DECIMAL(11,2))) AS brutto_tax
            FROM mst_salary ms,mst_bio_rec mb1,trn_salary_slip ss 
            WHERE ms.company_name LIKE '%".$site."' AND ss.month_period = '".$monthPeriod."' AND ss.year_period = '".$yearPeriod."' 
            AND ms.bio_rec_id = mb1.bio_rec_id  AND mb1.bio_rec_id = ss.bio_rec_id AND mb1.is_active = 1 
            AND ms.company_name ='Redpath_Timika') AS a
            ORDER BY a.full_name "; 
        }

        else if($pt == "Redpath_Timika")
        {
            $strSQL  = "SELECT ss.other_allowance1 AS jumboPercent,(ss.dev_incentive_bonus*(ss.dev_percent/100))*(ss.other_allowance1/100) AS jumboVal,ss.salary_slip_id,mb.bio_rec_id,mb.full_name,ms.nie,mb.position,ms.bank_code,ms.bank_name,ms.account_name,ms.account_no,ss.ptkp_total,ss.thr,ss.contract_bonus,ss.emp_jht,ss.emp_jp,ss.cc_payment,ss.debt_burden,ms.payroll_group,ss.tax_value,ss.client_name,mb.npwp_no,";
            $strSQL .= " (";
            $strSQL .= " CAST("; /*START CAST*/
            $strSQL .= " CASE WHEN(ss.basic_salary = ss.bs_prorate) THEN ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 ";
            $strSQL .= " ELSE ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 END + ";
            $strSQL .= " CASE WHEN(ms.is_ot_bonus = 1) THEN ss.ot_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_remote_allowance = 1) THEN ss.remote_allowance ELSE 0 END +";
            // $strSQL .= " ss.thr+";
            $strSQL .= " CASE WHEN(ms.is_dev_incentive_bonus = 1) THEN ss.dev_incentive_bonus*(ss.dev_percent/100) ELSE 0 END +";
            $strSQL .= " (ss.dev_incentive_bonus*(ss.dev_percent/100))*(ss.other_allowance1/100) +";
            $strSQL .= " ss.workday_adj+";
            $strSQL .= " ss.no_accident_bonus+";
            $strSQL .= " ss.jkk_jkm+";
            $strSQL .= " ss.health_bpjs-";
            $strSQL .= " ss.unpaid_total+";
            $strSQL .= " ss.adjust_in-";
            $strSQL .= " ss.adjust_out-";  
            $strSQL .= " ss.jkk_jkm-";
            $strSQL .= " ss.emp_jht-";
            $strSQL .= " ss.emp_health_bpjs-";
            $strSQL .= " ss.health_bpjs-";
            $strSQL .= " ss.emp_jp";
            $strSQL .= " AS DECIMAL(11,2))  "; /*END CAST*/
            $strSQL .= " ) AS total, ";

            /* START BRUTTO */
            $strSQL .= " (";
            $strSQL .= " CAST("; /*START CAST*/
            $strSQL .= " CASE WHEN(ss.basic_salary = ss.bs_prorate) THEN ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 ";
            $strSQL .= " ELSE ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 END + ";
            $strSQL .= " CASE WHEN(ms.is_ot_bonus = 1) THEN ss.ot_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_remote_allowance = 1) THEN ss.remote_allowance ELSE 0 END +";
            $strSQL .= " ss.thr+";
            $strSQL .= " CASE WHEN(ms.is_dev_incentive_bonus = 1) THEN ss.dev_incentive_bonus*(ss.dev_percent/100) ELSE 0 END +";
            $strSQL .= " (ss.dev_incentive_bonus*(ss.dev_percent/100))*(ss.other_allowance1/100) +";
            $strSQL .= " ss.contract_bonus+";
            $strSQL .= " ss.workday_adj+";
            $strSQL .= " ss.no_accident_bonus+";
            $strSQL .= " ss.jkk_jkm+";
            $strSQL .= " ss.health_bpjs-";
            $strSQL .= " ss.unpaid_total+";
            $strSQL .= " ss.adjust_in-";
            $strSQL .= " ss.adjust_out "; 
            $strSQL .= " AS DECIMAL(11,2))  "; /*END CAST*/
            $strSQL .= " ) AS brutto_tax ";
            /* END BRUTTO */
            $strSQL .= "FROM mst_salary ms,mst_bio_rec mb,trn_salary_slip ss ";
            $strSQL .= "WHERE ms.company_name LIKE '%".$site."' ";
            $strSQL .= "AND ss.month_period = '".$monthPeriod."' ";
            $strSQL .= "AND ss.year_period = '".$yearPeriod."' ";
            $strSQL .= "".$sqldept." ";
            $strSQL .= "AND ms.bank_name !='MANDIRI' AND ms.bank_name !='BNI' ";
            $strSQL .= "AND ms.bio_rec_id = mb.bio_rec_id  ";
            $strSQL .= "AND mb.bio_rec_id = ss.bio_rec_id ";
            $strSQL .= "AND mb.is_active = 1 ";      
            $strSQL .= "AND ms.company_name = 'Redpath_Timika' ";      
            $strSQL .= "ORDER BY mb.full_name ";      
        }
        else if($pt == "Pontil_Timika")
        {           

            $strSQL  = "SELECT 0 jumboPercent,0 jumboVal,ss.salary_slip_id,mb.bio_rec_id,mb.full_name,ms.nie,mb.position,ms.bank_code,ms.bank_name,ms.account_name,ms.account_no,ss.ptkp_total,ss.thr,ss.contract_bonus,ss.emp_jht,ss.emp_jp,ss.cc_payment,ss.debt_burden,ms.payroll_group,ss.tax_value,ss.client_name,mb.npwp_no,";
            $strSQL .= " (";
            $strSQL .= " CAST("; /*START CAST*/
            $strSQL .= " ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 + ";
            $strSQL .= " CASE WHEN(ms.is_travel = 1) THEN ss.travel_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_allowance_economy = 1) THEN ss.allowance_economy ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_incentive_bonus = 1) THEN ss.incentive_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_production = 1) THEN ss.production_bonus ELSE 0 END +";
           
            $strSQL .= " ss.workday_adj+";
            $strSQL .= " ss.jkk_jkm+";
            $strSQL .= " ss.health_bpjs-";
            $strSQL .= " ss.unpaid_total+";
            $strSQL .= " ss.adjust_in-";
            $strSQL .= " ss.adjust_out-";  
            $strSQL .= " ss.jkk_jkm-";
            $strSQL .= " ss.emp_jht-";
            $strSQL .= " ss.emp_health_bpjs-";
            $strSQL .= " ss.health_bpjs-";
            $strSQL .= " ss.emp_jp";
            $strSQL .= " AS DECIMAL(11,2))  "; /*END CAST*/
            $strSQL .= " ) AS total, ";

            /* START BRUTTO */
            $strSQL .= " (";
            $strSQL .= " CAST("; /*START CAST*/
            // $strSQL .= " ("; /*START SETAHUNKAN*/
            $strSQL .= " ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 + ";
            $strSQL .= " CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_allowance_economy = 1) THEN ss.allowance_economy ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_incentive_bonus = 1) THEN ss.incentive_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_production = 1) THEN ss.production_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_travel = 1) THEN ss.travel_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_cc_payment = 1) THEN ss.cc_payment ELSE 0 END +"; /*ok*/
            $strSQL .= " ss.workday_adj+";
            $strSQL .= " ss.thr+";
            $strSQL .= " ss.jkk_jkm+";
            $strSQL .= " ss.health_bpjs-";
            $strSQL .= " ss.unpaid_total+";
            $strSQL .= " ss.adjust_in-";
            $strSQL .= " ss.adjust_out";
            $strSQL .= " AS DECIMAL(11,2))  "; /*END CAST*/
            $strSQL .= " ) AS brutto_tax ";
            /* END BRUTTO */

            $strSQL .= "FROM mst_salary ms,mst_bio_rec mb,trn_salary_slip ss ";
            $strSQL .= "WHERE ms.company_name LIKE '%".$site."' ";
            $strSQL .= "AND ss.month_period = '".$monthPeriod."' ";
            $strSQL .= "AND ss.year_period = '".$yearPeriod."' ";
            $strSQL .= "".$sqldept." ";
            $strSQL .= "AND ms.bank_name !='MANDIRI' AND ms.bank_name !='BNI' ";
            $strSQL .= "AND ms.bio_rec_id = mb.bio_rec_id  ";
            $strSQL .= "AND mb.bio_rec_id = ss.bio_rec_id ";
            $strSQL .= "AND mb.is_active = 1 ";   
            $strSQL .= "AND ms.company_name = 'Pontil_Timika' ";   
            $strSQL .= "ORDER BY mb.full_name ";      
        } 
        $query = $this->db->query($strSQL)->result_array();    
        $jmlrow = $this->db->query($strSQL)->num_rows();    
        $boldFont = [
            'font' => [
                'bold' => true
                // 'color' => ['argb' => '0000FF'],
            ],
        ];

        $totalStyle = [
            'font' => [
                'bold' => true,
                'color' => ['argb' => '0000FF'],
            ],
        ];

        $allBorderStyle = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $outlineBorderStyle = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $topBorderStyle = [
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $bottomBorderStyle = [
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        // foreach(range('A','D') as $columnID)
        // {
        //     $spreadsheet->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        // }           
        $myCurrentDate = GetCurrentDate();
        $year       = $myCurrentDate['CurrentYear'];
        $month      = $myCurrentDate['CurrentMonth'];
        $day        = $myCurrentDate['CurrentDay'];
        $jam        = substr($myCurrentDate['CurrentTime'], 0,2);
        $menit      = substr($myCurrentDate['CurrentTime'], 3,2);
        $detik      = substr($myCurrentDate['CurrentTime'], 6,2);
        $tambahan   = $jmlrow+2;
        $inputTime  = $year.'/'.$month.'/'.$day.'_'.$jam.'.'.$menit.'.'.$detik;
        // Nama Field Baris Pertama
        $spreadsheet->getActiveSheet()
                ->setCellValue('A1', $inputTime.','.$tambahan.','.$dataprint.',,,,,,,,,,,,,,,,,')
                ;

        /* START TOTAL WORK HOUR */
        $bruttoTax = 0;
        /* TAX CONFIG */
        $myConfigId = 0;    
        switch ($pt) {
            case 'Redpath_Timika':
                $myConfigId = 1;
                break;
            case 'Pontil_Timika':
                $myConfigId = 2;
                break;  
            case 'All':
                $myConfigId = 1;
                break;          
            default:
                break;
        }
        // $row = '';
        // if ($pt == 'All'){
        //     $a = 1;
        //     $b = 2;
        //     $row        = $this->M_payroll_config->getObjectById($a); 
        //     $row        = $this->M_payroll_config->getObjectById($b); 
        // }
        // else{
        // }
        $row            = $this->M_payroll_config->getObjectById($myConfigId); 
        $taxPercent1    = $this->M_payroll_config->getTaxPercent1(); /* Persentase Progresif Pajak I */
        $taxPercent2    = $this->M_payroll_config->getTaxPercent2(); /* Persentase Progresif Pajak II */
        $taxPercent3    = $this->M_payroll_config->getTaxPercent3(); /* Persentase Progresif Pajak III */
        $taxPercent4    = $this->M_payroll_config->getTaxPercent4(); /* Persentase Progresif Pajak IV */
        $maxTaxVal1     = $this->M_payroll_config->getTaxMaxVal1(); /* Nominal Pajak I */ 
        $maxTaxVal2     = $this->M_payroll_config->getTaxMaxVal2(); /* Nominal Pajak II */
        $maxTaxVal3     = $this->M_payroll_config->getTaxMaxVal3(); /* Nominal Pajak III */
        $maxTaxVal4     = $this->M_payroll_config->getTaxMaxVal4(); /* Nominal Pajak IV */
        $maxNonTax      = $this->M_payroll_config->getNonTaxAllowance(); /* Max Tunjangan Jabatan */
            
        $sumTotal       = 0;
        $rowIdx         = 2;
        $startIdx       = $rowIdx;
        foreach ($query as $row) {
            $rowIdx++;

            /* START UPDATE TAX */
            $bruttoTax = $row['brutto_tax'];
            $pembulatanPenghasilan = $bruttoTax;
            $nonTaxAllowance = ($pembulatanPenghasilan) * (5/100);
            if($nonTaxAllowance > $maxNonTax){
                $nonTaxAllowance = $maxNonTax;
            }

            $netto = $bruttoTax - $row['emp_jht'] - $row['emp_jp'] - $nonTaxAllowance;

            $unFixedIncome = 0;
            if($pt == "Pontil_Timika"){
                $unFixedIncome = $row['thr'] + $row['cc_payment'];
            }else if($pt == "Redpath_Timika"){
                $unFixedIncome = $row['thr'] + $row['contract_bonus'];
            }

            $nettoSetahun = ( ($netto - $unFixedIncome)*12 );
            $monthlyTax = 0;

            $taxVal1 = 0;    
            $taxVal2 = 0;    
            $taxVal3 = 0;    
            $taxVal4 = 0;

            $tVal = 0;
            $tSisa = 0;
            $pembulatanPenghasilan = $nettoSetahun - $row['ptkp_total'];
            if($pembulatanPenghasilan > 0)
            {
                /* TAX 1 */
                if($maxTaxVal1 > 0)
                {
                    $tVal = $pembulatanPenghasilan/$maxTaxVal1;  
                    if($tVal >= 1){
                        $taxVal1 = $maxTaxVal1 * ($taxPercent1/100);
                    }
                    else{
                        $taxVal1 = $pembulatanPenghasilan * ($taxPercent1/100);
                    }    
                }    
                
                /* TAX 2 */
                if($maxTaxVal2 > 0)
                {
                    if($pembulatanPenghasilan > $maxTaxVal1)
                    {
                        $tSisa = $pembulatanPenghasilan - $maxTaxVal1; 
                        $tVal = $tSisa/$maxTaxVal2;
                        if($tVal >= 1){
                            $taxVal2 = $maxTaxVal2 * ($taxPercent2/100);
                        }
                        else{
                            $taxVal2 = $tSisa * ($taxPercent2/100);
                        }
                    }     
                }
                 
                /* TAX 3 */
                if($maxTaxVal3 > 0)
                {
                    if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2))
                    {
                        $tSisa = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2;
                        $tVal = $tSisa/$maxTaxVal3;
                        if($tVal >= 1){
                            $taxVal3 = $maxTaxVal3 * ($taxPercent3/100);
                        }
                        else{
                            $taxVal3 = $tSisa * ($taxPercent3/100);
                        }
                    }    
                }
                 
                /* TAX 4 */
                if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2 + $maxTaxVal3))
                {
                    $tSisa = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2 - $maxTaxVal3;
                    $taxVal4 = $tSisa * ($taxPercent4/100); 
                }               
            }

            /* START THR - BONUS TAX -- EDITED @ AUG 15, 2017 BY FINANCE  */
            // $bonusTmp = $row['thr'] + $row['contract_bonus'];
            // $bonusTax = 0;
            // if( $taxVal4 > 0){
            //     $bonusTax = $bonusTmp * ($taxPercent4/100);
            // }else if( $taxVal3 > 0 ){
            //     $bonusTax = $bonusTmp * ($taxPercent3/100);
            // }else if( $taxVal2 > 0 ){
            //     $bonusTax = $bonusTmp * ($taxPercent2/100);                
            // }else{
            //     $bonusTax = $bonusTmp * ($taxPercent1/100);             
            // }

            /* END THR - BONUS TAX -- EDITED @ AUG 15, 2017 BY FINANCE */
            $taxTotalFix = $taxVal1 + $taxVal2 + $taxVal3 + $taxVal4;
            $taxTotal = ( $taxTotalFix/12 );
            if($taxTotal > 0){
                $monthlyTax = $taxTotal; 
            }else{
                $monthlyTax = 0; 
            }


            $nettoSetahunUnFix = ( ($netto - $unFixedIncome)*12 ) + $unFixedIncome;

            $taxTotalUnFix = 0;
            $taxValUnFix1 = 0;    
            $taxValUnFix2 = 0;    
            $taxValUnFix3 = 0;    
            $taxValUnFix4 = 0;

            $tValUnFix = 0;
            $tSisaUnFix = 0;
            $pembulatanPenghasilan = $nettoSetahunUnFix - $row['ptkp_total'];
            if($pembulatanPenghasilan > 0)
            {
                /* TAX 1 */
                if($maxTaxVal1 > 0)
                {
                    $tValUnFix = $pembulatanPenghasilan/$maxTaxVal1;  
                    if($tValUnFix >= 1){
                        $taxValUnFix1 = $maxTaxVal1 * ($taxPercent1/100);
                    }
                    else{
                        $taxValUnFix1 = $pembulatanPenghasilan * ($taxPercent1/100);
                    }    
                }    
                
                /* TAX 2 */
                if($maxTaxVal2 > 0)
                {
                    if($pembulatanPenghasilan > $maxTaxVal1)
                    {
                        $tSisaUnFix = $pembulatanPenghasilan - $maxTaxVal1; 
                        $tValUnFix = $tSisaUnFix/$maxTaxVal2;
                        if($tValUnFix >= 1){
                            $taxValUnFix2 = $maxTaxVal2 * ($taxPercent2/100);
                        }
                        else{
                            $taxValUnFix2 = $tSisaUnFix * ($taxPercent2/100);
                        }
                    }     
                }
                 
                /* TAX 3 */
                if($maxTaxVal3 > 0)
                {
                    if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2))
                    {
                        $tSisaUnFix = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2;
                        $tValUnFix = $tSisaUnFix/$maxTaxVal3;
                        if($tValUnFix >= 1){
                            $taxValUnFix3 = $maxTaxVal3 * ($taxPercent3/100);
                        }
                        else{
                            $taxValUnFix3 = $tSisaUnFix * ($taxPercent3/100);
                        }
                    }    
                }
                 
                /* TAX 4 */
                if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2 + $maxTaxVal3))
                {
                    $tSisaUnFix = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2 - $maxTaxVal3;
                    $taxValUnFix4 = $tSisaUnFix * ($taxPercent4/100); 
                }               
            }

            /* END THR - BONUS TAX -- EDITED @ AUG 15, 2017 BY FINANCE */

            $taxTotalUnFix = ( ($taxValUnFix1 + $taxValUnFix2 + $taxValUnFix3 + $taxValUnFix4) - $taxTotalFix );            
            

            $npwpNo = $row['npwp_no'];
            $npwpCharge = 0;
            if( strlen($npwpNo) < 19 ){
                $npwpCharge = $this->M_payroll_config->getNpwpCharge();
                $monthlyTax = $monthlyTax + ($monthlyTax * ($npwpCharge/100) ); 
                $taxTotalUnFix = $taxTotalUnFix + ($taxTotalUnFix * ($npwpCharge/100) ); 
            }



            $taxTotal = $monthlyTax + $taxTotalUnFix; 


            
            $tmpTotal = $row['total'] - floor($taxTotal) - $row['debt_burden'];
            $totalTerima = 0;
            if ($tmpTotal > 0)
            {
                $totalTerima = $tmpTotal;
            }

            /* START UPDATE TAX VALUE TO THE TABLE */
            if($row['tax_value'] <= 0)
            {
                $slipId = $row['salary_slip_id'];
                $str = "UPDATE trn_salary_slip SET tax_value = ".$taxTotal." WHERE salary_slip_id = '".$slipId."' ";
                $this->db->query($str);                
            }
            $email1 = '';
            $email2 = '';
            $email3 = '';
            $email4 = '';

            if($pt == "Pontil_Timika"){
                $email1 = 'payroll.fin@sangati.co';
                $email2 = '';
                $email3 = '';
                $email4 = 'r_kalangie@sangati.co';
            }
            else if($pt == "Redpath_Timika"){
                $email1 = 'payroll.fin@sangati.co';
                $email2 = '';
                $email3 = '';
                $email4 = 'skrisman@fmi.com';
            }
            /* END UPDATE TAX VALUE TO THE TABLE */
            $sumTotal = $sumTotal+round($totalTerima);
            $spreadsheet->getActiveSheet()
                ->setCellValue('A'.$rowIdx, $row['account_no'].','.$row['account_name'].','.round($totalTerima).',,,,'.$row['bank_code'].','.$row['bank_name'].',,,,,,,,,Y,payroll.fin@sangati.co')
                ->setCellValue('B'.$rowIdx, 'jino@sangati.co')
                ->setCellValue('C'.$rowIdx, 'putra@sangati.co')
                ->setCellValue('D'.$rowIdx, $email4.',,N')
                ;
        }
        // $set = '=SUM(C'.$startIdx.':C'.$rowIdx.')';
        $spreadsheet->getActiveSheet()
                ->setCellValue('A2', 'P, , ,'.$jmlrow.','.round($sumTotal).',,,,,,,,,,,,,,,')
                ;
        
        unset($allBorderStyle);
        unset($center);
        unset($right);
        unset($left);
        
        $spreadsheet->setActiveSheetIndex(0);

        $str = '"'.$pt.'" PaymentList';
        $fileName = preg_replace('/\s+/', '', $str);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.Xlsx"');
        // header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        /* BY COMPOSER */
        // $writer = new Xlsx($spreadsheet);
        /* OFFLINE/ BY COPY EXCEL FOLDER  */
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit(0);   
    }

    function namabulan($tanggal){
        switch($tanggal){
            case '1': return "Jan"; break;
            case '2': return "Feb"; break;
            case '3': return "Mar"; break;
            case '4': return "Apr"; break;
            case '5': return "Mei"; break;
            case '6': return "Jun"; break;
            case '7': return "Jul"; break;
            case '8': return "Agu"; break;
            case '9': return "Sep"; break;
            case '10': return "Okt"; break;
            case '11': return "Nov"; break;
            case '12': return "Des"; break;
            case '0': return "Des"; break;
        };
    }
}
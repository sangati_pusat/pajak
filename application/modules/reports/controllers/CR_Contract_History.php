<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    // Load library phpspreadsheet
    require('./vendor/autoload.php');
    use PhpOffice\PhpSpreadsheet\Helper\Sample;
    use PhpOffice\PhpSpreadsheet\IOFactory;
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    // End load library phpspreadsheet

class CR_Contract_History extends CI_Controller {

    public function __construct(){
        parent::__construct();
    }

    public function getContractHistory(){
        $clientName = "";
        if(isset($_POST['clientName']))
        {
            $clientName = $_POST['clientName'];
        }
        // test($clientName,1);


        $stQuery  = 'SELECT bio_rec_id, employee_name, client_name, COUNT(bio_rec_id) contract_count ';
        $stQuery .= 'FROM mst_contract ';
        $stQuery .= 'WHERE client_name = "'.$clientName.'" ';
        $stQuery .= 'AND is_close = 0 ';
        $stQuery .= 'GROUP BY bio_rec_id ';
        //$stQuery .= 'HAVING COUNT(bio_rec_id) >= 9 ';
        $stQuery .= 'ORDER BY employee_name, contract_counter ';

        $data = $this->db->query($stQuery)->result_array();

        $myData = array();
        foreach ($data as $row) {
            $myData[] = array(
                $row['bio_rec_id'], 
                $row['employee_name'], 
                $row['client_name'], 
                $row['contract_count']        
            );            
        }        
        echo json_encode($myData); 
    }

    public function contractDetailDisplay(){
        $biodataId = '';
        $clientName = '';
        if(isset($_POST['biodataId']))
        {
            $biodataId = $_POST['biodataId'];
        }
        if(isset($_POST['clientName']))
        {
            $clientName = $_POST['clientName'];
        }

        $stQuery  = 'SELECT bio_rec_id, employee_name, client_name, contract_start, contract_end, contract_counter ';
        $stQuery .= 'FROM mst_contract ';
        $stQuery .= 'WHERE bio_rec_id = "'.$biodataId.'" ';
        $stQuery .= 'AND client_name = "'.$clientName.'" ';
        $stQuery .= 'AND is_close = 0 ';
        $stQuery .= 'ORDER BY employee_name, contract_counter ';  
        $data = $this->db->query($stQuery)->result_array();
        $myData = array();
        foreach ($data as $row) {
            $myData[] = array(
                $row['contract_counter'], 
                $row['contract_start'], 
                $row['contract_end']        
            );            
        }        
        echo json_encode($myData); 
    }

    public function exportContractHistory($clientName)
    {
        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();

        $boldFont = [
            'font' => [
                'bold' => true
                // 'color' => ['argb' => '0000FF'],
            ],
        ];

        $totalStyle = [
            'font' => [
                'bold' => true,
                'color' => ['argb' => '0000FF'],
            ],
        ];

        $allBorderStyle = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $outlineBorderStyle = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $topBorderStyle = [
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $bottomBorderStyle = [
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $stQuery  = 'SELECT bio_rec_id, employee_name, client_name, COUNT(bio_rec_id) contract_count ';
        $stQuery .= 'FROM mst_contract ';
        $stQuery .= 'WHERE client_name = "'.$clientName.'" ';
        $stQuery .= 'AND is_close = 0 ';
        $stQuery .= 'GROUP BY bio_rec_id ';
        // $stQuery .= 'HAVING COUNT(bio_rec_id) >= 9 ';
        $stQuery .= 'ORDER BY employee_name, contract_counter ';  
        $query = $this->db->query($stQuery)->result_array();  
        // test($clientName,1);
        // Add some data
        $spreadsheet->getActiveSheet()->getStyle('A6:G7')
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B'); 

        $spreadsheet->getActiveSheet()
            ->setCellValue('A1', 'PT. SANGATI SOERYA SEJAHTERA')
            ->setCellValue('A2', 'HISTORY OF CONTRACT PT '.$clientName.'');
            // ->setCellValue('A4', 'Periode : '.$monthPeriod.'-'.$yearPeriod);

        $spreadsheet->getActiveSheet()->getStyle("A1:D1")->getFont()->setBold(true)->setSize(16);
        $spreadsheet->getActiveSheet()->getStyle("A2:D2")->getFont()->setBold(true)->setSize(14)->setUnderline(true);
        // $spreadsheet->getActiveSheet()->getStyle("A4:G4")->getFont()->setBold(true)->setSize(12);
        $spreadsheet->getActiveSheet()->getStyle("A6:G7")->getFont()->setBold(true)->setSize(12);

        $spreadsheet->getActiveSheet()->mergeCells("A6:A7");
        $spreadsheet->getActiveSheet()->mergeCells("B6:B7");
        $spreadsheet->getActiveSheet()->mergeCells("C6:C7");
        $spreadsheet->getActiveSheet()->mergeCells("D6:D7");
        $spreadsheet->getActiveSheet()->mergeCells("E6:E7");
        $spreadsheet->getActiveSheet()->mergeCells("F6:F7");
        $spreadsheet->getActiveSheet()->mergeCells("G6:G7");
            
        $spreadsheet->getActiveSheet()->getStyle("A6:G7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("A6:G7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()
            ->setCellValue('A6', 'EMPLOYEE ID')
            ->setCellValue('B6', 'NAME')
            ->setCellValue('C6', 'CLIENT')
            ->setCellValue('D6', 'CONTRACT COUNT')
            ->setCellValue('E6', 'SEQENCES')
            ->setCellValue('F6', 'CONTRACT START')
            ->setCellValue('G6', 'CONTRACT END');

        /* START GET DAYS TOTAL BY ROSTER */
        $rowIdx = 7;
        $rowNo = 0;
        foreach ($query as $row) {                      
            $bioId          = $row['bio_rec_id'];
            $employeeName   = $row['employee_name'];
            $clientName     = $row['client_name'];
            $contractCount  = $row['contract_count'];
            
            $rowIdx++;          
            $spreadsheet->getActiveSheet()
            ->setCellValue('A'.($rowIdx), $bioId)
            ->setCellValue('B'.($rowIdx), $employeeName)
            ->setCellValue('C'.($rowIdx), $clientName)
            ->setCellValue('D'.($rowIdx), $contractCount);
            $spreadsheet->getActiveSheet()->getStyle('A'.$rowIdx.':'.'G'.$rowIdx)->getFont()->setBold(true)->getColor()->setRGB('001188');;
            $spreadsheet->getActiveSheet()->getStyle('A'.$rowIdx.':G'.$rowIdx)
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('FCF7B6');    

            $stQuery  = 'SELECT bio_rec_id, employee_name, client_name, contract_start, contract_end, contract_counter ';
            $stQuery .= 'FROM mst_contract ';
            $stQuery .= 'WHERE bio_rec_id = "'.$bioId.'" ';
            $stQuery .= 'AND client_name = "'.$clientName.'" ';
            $stQuery .= 'AND is_close = 0 ';
            $stQuery .= 'ORDER BY employee_name, contract_counter ';  
            $detailData = $this->db->query($stQuery)->result_array();
            foreach ($detailData as $dataRow) {
                $rowIdx++;
                $spreadsheet->getActiveSheet()
                ->setCellValue('E'.($rowIdx), $dataRow['contract_counter'])
                ->setCellValue('F'.($rowIdx), $dataRow['contract_start'])
                ->setCellValue('G'.($rowIdx), $dataRow['contract_end']);

            }

            /* SET ROW COLOR */
            // if($rowIdx % 2 == 1)
            // {
            //     $spreadsheet->getActiveSheet()->getStyle('A'.$rowIdx.':G'.$rowIdx)
            //     ->getFill()
            //     ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            //     ->getStartColor()
            //     ->setRGB('EAEBAF');             
            // } 
        }

        $spreadsheet->getActiveSheet()->getStyle("A6:G".($rowIdx))->applyFromArray($allBorderStyle);
        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('Report Excel '.date('d-m-Y H'));

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        // $str = 'PTLSmbInvoice';
        $str = 'Contract History PT '.$clientName.'';
        $fileName = preg_replace('/\s+/', '', $str);
        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.Xlsx"');
        // header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        /* BY COMPOSER */
        // $writer = new Xlsx($spreadsheet);
        /* OFFLINE/ BY COPY EXCEL FOLDER  */
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit(0);   
    }    
}

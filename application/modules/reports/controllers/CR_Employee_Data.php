<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    // Load library phpspreadsheet
    require('./vendor/autoload.php');
    use PhpOffice\PhpSpreadsheet\Helper\Sample;
    use PhpOffice\PhpSpreadsheet\IOFactory;
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    // End load library phpspreadsheet


class CR_Employee_Data extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('masters/M_mst_bio_rec');
        $this->load->model('masters/M_mst_bio_education');
        $this->load->model('masters/M_mst_bio_experience');
        $this->load->model('masters/M_mst_bio_language');
        $this->load->model('masters/M_mst_bio_family');
        $this->load->model('masters/M_mst_bio_references');
        $this->load->model('masters/M_mst_bio_training');
        $this->load->model('masters/M_mst_bio_qualification');
        $this->load->model('masters/M_mst_bio_organization');
    }

    public function getEmployeeData(){
        $sAge       = $this->input->post('sAge');
        $sPosition  = $this->input->post('sPosition');
        $sCity      = $this->input->post('sCity');
        $sName      = $this->input->post('sName');
        $sNo        = $this->input->post('sNo');

        $isActive = 1;

        $strSql  = 'SELECT a.bio_rec_id, a.full_name, a.date_of_birth, a.cell_no, a.id_card_address,a.city_address, a.nationality, a.religion, a.marital_status, ';
        $strSql .= 'a.position,a.cv_date,a.date_of_birth,TIMESTAMPDIFF(YEAR,a.date_of_birth, NOW()) AS old,b.company_name,c.client_value ';
        $strSql .= 'FROM '.$this->db->database.'.mst_bio_rec a ';
        $strSql .= 'LEFT JOIN db_recruitment.mst_salary b ON a.bio_rec_id=b.bio_rec_id ';
        $strSql .= 'LEFT JOIN db_admin.mst_client_inactive c ON b.company_name=c.client_value AND c.end_date <= DAY(NOW()) AND c.is_active = 1 ';
        $strSql .= 'WHERE a.is_active = '.$isActive.' ';
        if($sAge!=''){
            $strSql .= 'AND TIMESTAMPDIFF(YEAR,a.date_of_birth, NOW() ) <= '.$this->security->xss_clean($sAge).' ';
        }
        if($sPosition!=''){
            $strSql .= 'AND a.position LIKE "%'.$this->security->xss_clean($sPosition).'%" ';
        }
        if($sCity!=''){
            $strSql .= 'AND a.city_address LIKE "%'.$this->security->xss_clean($sCity).'%" ';
        }
        if($sName!=''){
            $strSql .= 'AND a.full_name LIKE "%'.$this->security->xss_clean($sName).'%" ';
        }
        if($sNo!=''){
            $strSql .= 'AND a.id_card_no = "'.$sNo.'" ';
        }
        $strSql .= 'ORDER BY a.date_of_birth ASC ';
        // test($strSql,1);
        // echo $strSql; exit(0);
        $query = $this->db->query($strSql)->result_array();
         /*return json_encode($query);*/
        $myData = array();
        foreach ($query as $row) {
            if($row['client_value']==''){
                $update     = "<button class='btn btn-primary btn-sm btnHeaderUpdate' data-toggle='modal' data-target='#editBiodataModal'>&nbsp;&nbsp;<span class='fa fa-edit'></span></button>";
            }else{
                $update     = "<button class='btn btn-primary btn-sm btnHeaderUpdate' data-toggle='modal' disabled>&nbsp;&nbsp;<span class='fa fa-edit'></span></button>";
            }
            $myData[] = array(
                $row['bio_rec_id'],         
                $row['full_name'],         
                $row['id_card_address'],         
                $row['city_address'],         
                $row['cell_no'],         
                $row['position'],
                $row['cv_date'],
                $update,                
                "<button class='btn btn-primary btn-sm btnHeaderPrint'>&nbsp;&nbsp;<span class='fa fa-print'></span></button>"
            );            
        }  
        echo json_encode($myData); 
    }

    public function GetBiodataJasonById(){
        $rs = $this->M_mst_bio_rec->getBiodataJasonById($this->input->post('bioId'));
        echo $rs;                  
    }

    public function Upd(){
        // test($_POST['updMstBioRec'],0);
        if(isset($_POST['updMstBioRec']))
        {        
            $id = ($this->security->xss_clean($_POST['biodataId']));
            // test($id,1);
            $this->M_mst_bio_rec->loadById($id);
            $this->M_mst_bio_rec->setFullName($this->security->xss_clean($_POST['fullName']));
            $this->M_mst_bio_rec->setGender($this->security->xss_clean($_POST['gender']));            
            $this->M_mst_bio_rec->setPlaceOfBirth($this->security->xss_clean($_POST['birthPlace']));
            $this->M_mst_bio_rec->setDateOfBirth($this->security->xss_clean($_POST['birthDate']));
            $this->M_mst_bio_rec->setNationality($this->security->xss_clean($_POST['nationality']));
            $this->M_mst_bio_rec->setClan($this->security->xss_clean($_POST['etnics']));
            $this->M_mst_bio_rec->setIdCardNo($this->security->xss_clean($_POST['idNo']));
            $this->M_mst_bio_rec->setNpwpNo($this->security->xss_clean($_POST['npwpNo']));
            $this->M_mst_bio_rec->setBpjsNo($this->security->xss_clean($_POST['bpjsNo']));
            $this->M_mst_bio_rec->setIdCardAddress($this->security->xss_clean($_POST['idAddress']));
            $this->M_mst_bio_rec->setCurrentAddress($this->security->xss_clean($_POST['currentAddress']));            
            $this->M_mst_bio_rec->setCityAddress($this->security->xss_clean($_POST['cityAddress']));
            $this->M_mst_bio_rec->setResidenceStatus($this->security->xss_clean($_POST['livingStatus']));
            $this->M_mst_bio_rec->setReligion($this->security->xss_clean($_POST['religion']));
            $this->M_mst_bio_rec->setDrivingLicense($this->security->xss_clean($_POST['driveLicence']));
            $this->M_mst_bio_rec->setMaritalStatus($this->security->xss_clean($_POST['maritalStatus']));
            $this->M_mst_bio_rec->setPosition($this->security->xss_clean($_POST['appliedPosition']));            
            $this->M_mst_bio_rec->setCvDate($this->security->xss_clean($_POST['cvDate']));
            $this->M_mst_bio_rec->setHeight($this->security->xss_clean($_POST['height']));
            $this->M_mst_bio_rec->setWeight($this->security->xss_clean($_POST['weight']));
            $this->M_mst_bio_rec->setIsColorBlind($this->security->xss_clean($_POST['colorBlindness']));
            $this->M_mst_bio_rec->setBloodType($this->security->xss_clean($_POST['bloodType']));
            $this->M_mst_bio_rec->setEmailAddress($this->security->xss_clean($_POST['emailAddress']));
            $this->M_mst_bio_rec->setTelpNo($this->security->xss_clean($_POST['telpNo']));
            $this->M_mst_bio_rec->setCellNo($this->security->xss_clean($_POST['cellNo']));
            $this->M_mst_bio_rec->setLocalForeign($this->security->xss_clean($_POST['pointOfHire']));

            // $this->M_MstBioRec->setNiiId($this->security->xss_clean($_POST['nii_id']));
            // $this->M_mst_bio_rec->setIsStaff($this->security->xss_clean($_POST['staffingStatus']));
            // $this->M_mst_bio_rec->setDateOfHire($this->security->xss_clean($_POST['dateOfHire']));
            $currentTime = date('Y-m-d H:i:s');
            $this->M_mst_bio_rec->setPicEdit($this->security->xss_clean($this->session->userdata('hris_user_id')));
            $this->M_mst_bio_rec->setEditTime($this->security->xss_clean($currentTime));
            $this->M_mst_bio_rec->updateBiorec($id);
        }
    }

    function printBioReports($sAge,$sPosition,$sCity,$sName,$sNo){
        $spreadsheet = new Spreadsheet();

        if (file_exists('assets/images/report_logo.jpg')) {
          $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
          $drawing->setName('Logo');
          $drawing->setDescription('Logo');
          $drawing->setPath('./assets/images/report_logo.jpg');
          $drawing->setCoordinates('A1');
          $drawing->setHeight(36);
          $drawing->setWorksheet($spreadsheet->getActiveSheet());
        }

        $boldFont = [
          'font' => [
            'bold' => true
              // 'color' => ['argb' => '0000FF'],
          ],
        ];

        $totalStyle = [
          'font' => [
            'bold' => true,
            'color' => ['argb' => '0000FF'],
          ],
        ];

        $allBorderStyle = [
          'borders' => [
            'allBorders' => [
              'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
              'color' => ['argb' => '00000000'],
            ],
          ],
        ];

        $outlineBorderStyle = [
          'borders' => [
            'outline' => [
              'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
              'color' => ['argb' => '00000000'],
            ],
          ],
        ];

        $topBorderStyle = [
          'borders' => [
            'top' => [
              'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
              'color' => ['argb' => '00000000'],
            ],
          ],
        ];

        $bottomBorderStyle = [  
          'borders' => [
            'bottom' => [
              'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
              'color' => ['argb' => '00000000'],
            ],
          ],
        ];

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        /* COLOURING FOOTER */
        $spreadsheet->getActiveSheet()->getStyle("A4:K5")
        ->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()
        ->setRGB('F2BE6B'); 

        // test($sAge.' '.$sPosition.' '.$sCity.' '.$sName.' '.$sNo,1);
        $isActive = 1;

        $strSql  = 'SELECT bio_rec_id, full_name, date_of_birth, cell_no, id_card_address,city_address, nationality, religion, marital_status,  ';
        $strSql .= '`position`,cv_date,date_of_birth,TIMESTAMPDIFF(YEAR,date_of_birth, NOW()) AS old ';
        $strSql .= 'FROM '.$this->db->database.'.mst_bio_rec ';
        $strSql .= 'WHERE is_active = '.$isActive.' ';
        if($sAge!='-'){
            $strSql .= 'AND TIMESTAMPDIFF(YEAR,date_of_birth, NOW() ) <= '.$this->security->xss_clean($sAge).' ';
        }
        if($sPosition!='-'){
            $strSql .= 'AND position LIKE "%'.$this->security->xss_clean($sPosition).'%" ';
        }
        if($sCity!='-'){
            $strSql .= 'AND city_address LIKE "%'.$this->security->xss_clean($sCity).'%" ';
        }
        if($sName!='-'){
            $strSql .= 'AND full_name LIKE "%'.$this->security->xss_clean($sName).'%" ';
        }
        if($sNo!='-'){
            $strSql .= 'AND id_card_no = "'.$sNo.'" ';
        }
        $strSql .= 'ORDER BY date_of_birth ASC ';

        // echo $strSql;
        // exit();
        $query = $this->db->query($strSql)->result_array(); 

        /* START DATA IS HERE */
        $spreadsheet->getActiveSheet()
            ->setCellValue('A1', 'PT. SANGATI SOERYA SEJAHTERA')
            ->setCellValue('A2', 'Candidate List');
        /* END DATA IS HERE */

        $spreadsheet->getActiveSheet()->mergeCells("A1:K1");
        $spreadsheet->getActiveSheet()->getStyle("A1:K1")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->mergeCells("A2:K2");
        $spreadsheet->getActiveSheet()->getStyle("A2:K2")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("A4:K4")->applyFromArray($boldFont);
        $spreadsheet->getActiveSheet()->getStyle("A4:K4")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("A1:K1")->getFont()->setBold(true)->setSize(16);
        $spreadsheet->getActiveSheet()->getStyle("A2:K2")->getFont()->setBold(true)->setSize(13);
        $spreadsheet->getActiveSheet()->getStyle("A4:K4")->getFont()->setBold(true)->setSize(12);  

        $spreadsheet->getActiveSheet()->getStyle('A4:A5')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('F2BE6B');
        // $spreadsheet->getActiveSheet()->getStyle('A4:A5')
        //         ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLUE); 
        // $spreadsheet->getActiveSheet()->getStyle("A6:H6")->applyFromArray($outlineBorderStyle);
        // $spreadsheet->getActiveSheet()->getStyle("A6:AC9")->applyFromArray($center);
        // /* START TITLE NO */
        // $spreadsheet->getActiveSheet()->getStyle("A6:AC8")->applyFromArray($allBorderStyle);

        /* START TITLE BIODATA ID */
        $spreadsheet->getActiveSheet()
            ->mergeCells("A4:A5")
            ->mergeCells("B4:B5")
            ->mergeCells("C4:C5")
            ->mergeCells("D4:D5")
            ->mergeCells("E4:E5")
            ->mergeCells("F4:F5")
            ->mergeCells("G4:G5")
            ->mergeCells("H4:H5")
            ->mergeCells("I4:I5")
            ->mergeCells("J4:J5")
            ->mergeCells("K4:K5");

        $titleRowIdx = 4;
        $spreadsheet->getActiveSheet()
            ->setCellValue('A4', 'No')
            ->setCellValue('B4', 'Biodata ID')
            ->setCellValue('C4', 'Name')
            ->setCellValue('D4', 'Birth Of Date')
            ->setCellValue('E4', 'Contact No.')
            ->setCellValue('F4', 'Address')
            ->setCellValue('G4', 'City')
            ->setCellValue('H4', 'Nationality')
            ->setCellValue('I4', 'Religion')
            ->setCellValue('J4', 'Marital Status')
            ->setCellValue('K4', 'Position');

        $spreadsheet->getActiveSheet()->getStyle("A4:K4")->applyFromArray($outlineBorderStyle);
        /* END TITLE BIODATA ID */

        $rowIdx = 5;
        $rowNo = 0;
        foreach ($query as $row) {
            $rowIdx++;
            $rowNo++;
            $spreadsheet->getActiveSheet()
                ->setCellValue('A'.($rowIdx), $rowNo)
                ->setCellValue('B'.($rowIdx), $row['bio_rec_id'])
                ->setCellValue('C'.($rowIdx), $row['full_name'])
                ->setCellValue('D'.($rowIdx), $row['date_of_birth'])
                ->setCellValue('E'.($rowIdx), $row['cell_no'])
                ->setCellValue('F'.($rowIdx), $row['id_card_address'])
                ->setCellValue('G'.($rowIdx), $row['city_address'])
                ->setCellValue('H'.($rowIdx), $row['nationality'])
                ->setCellValue('I'.($rowIdx), $row['religion'])
                ->setCellValue('J'.($rowIdx), $row['marital_status'])
                ->setCellValue('K'.($rowIdx), $row['position']);
        
            if($rowIdx % 2 == 1)
            {
                $spreadsheet->getActiveSheet()->getStyle('A'.$rowIdx.':K'.$rowIdx)
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('EAEBAF');             
            } 
        } /* end foreach ($query as $row) */ 

        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":K".($rowIdx+2))
        ->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()
        ->setRGB('F2BE6B');

        unset($allBorderStyle);
        unset($center);
        unset($right);
        unset($left);

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        if($sPosition!=''){
            $sPosition      = $sPosition;
        }else{
            $sPosition      = '-';
        }

        $str = 'Biodata'.$sPosition.'MaxAges'.$sAge;
        $fileName = preg_replace('/\s+/', '', $str);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.Xlsx"');
        // header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        /* BY COMPOSER */
        // $writer = new Xlsx($spreadsheet);
        /* OFFLINE/ BY COPY EXCEL FOLDER  */
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit(0);

    }

    public function dataCrews()
    {

        $sql  = " SELECT b.nie,a.full_name,a.place_of_birth,a.date_of_birth,a.id_card_no,a.id_card_address,a.npwp_no,a.marital_status,
                    a.position,b.basic_salary,b.bank_name,b.account_name,b.account_no
                    FROM mst_bio_rec a LEFT JOIN mst_salary b ON a.bio_rec_id=b.bio_rec_id
                    WHERE b.company_name='LCP_Sumbawa' ";
        
        $data = $this->db->query($sql);
        $query= $data->result_array();

        $data = array();
        foreach ($query as $key => $row) 
        {
            $data[] = array
            (
                $row['nie'],                     
                $row['full_name'],         
                $row['place_of_birth'],         
                $row['date_of_birth'],         
                $row['id_card_no'],         
                $row['id_card_address'],         
                $row['npwp_no'],         
                $row['marital_status'],       
                $row['position'],       
                $row['basic_salary'],       
                $row['bank_name'],         
                $row['account_name'],         
                $row['account_no']              
            );            
        }  
        echo json_encode($data);   
    }

    public function exportdataCrews(){

        $objPHPExcel = new Spreadsheet();

        $strSQL   = " SELECT b.nie,a.full_name,a.place_of_birth,a.date_of_birth,a.id_card_no,a.id_card_address,a.npwp_no,a.marital_status,
                        a.position,b.basic_salary,b.bank_name,b.account_name,b.account_no
                        FROM mst_bio_rec a LEFT JOIN mst_salary b ON a.bio_rec_id=b.bio_rec_id
                        WHERE b.company_name='LCP_Sumbawa' ";

        $query = $this->db->query($strSQL)->result_array();

        // Nama Field Baris Pertama
        $objPHPExcel->getActiveSheet()
            ->setCellValue('A1', 'PT. SANGATI SOERYA SEJAHTERA')
            ->setCellValue('A2', 'DATA CREWS PT. '.strtoupper("LCP_Sumbawa") );
            // ->setCellValue('A4', 'Periode : '.$monthPeriod.'-'.$yearPeriod);

        $objPHPExcel->getActiveSheet()->getStyle("A1:S1")->getFont()->setBold(true)->setSize(16);
        $objPHPExcel->getActiveSheet()->getStyle("A2:S2")->getFont()->setBold(true)->setSize(13);
        $objPHPExcel->getActiveSheet()->getStyle("A4:S4")->getFont()->setBold(true)->setSize(12); 

        $totalStyle = array(
            'font'  => array(
                'bold'  => true,
                'color' => array('rgb' => '0000FF'),
                // 'size'  => 15,
                // 'name'  => 'Verdana'
            )
        );
        
        $allBorderStyle = array(
            'borders' => array(
                'allborders' => array(
                    'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                )
            )
        );

        $outlineBorderStyle = array(
          'borders' => array(
            'outline' => array(
              'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
            )
          )
        );

        $topBorderStyle = array(
          'borders' => array(
            'top' => array(
              'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
            )
          )
        );

        $bottomBorderStyle = array(
          'borders' => array(
            'bottom' => array(
              'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
            )
          )
        );

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $objPHPExcel->getActiveSheet()->getStyle("A6:M7")
        ->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()
        ->setRGB('F2BE6B');      

        /* START PAYMENT TITLE */
        $objPHPExcel->getActiveSheet()->getStyle("A6:S6")->getFont()->setBold(true)->setSize(12);
        $objPHPExcel->getActiveSheet()->getStyle("A6:S6")->applyFromArray($outlineBorderStyle);
        /* START TITLE NO */
        $titleRowIdx = 6;
        $titleColIdx = 1;
        $objPHPExcel->getActiveSheet()->getStyle("A6:A7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("A6:A7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("A6:A7");
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'BADGE ID');
        /* END TITLE NO */

        /* START NAMA KARYAWAN  */
        $titleColIdx++; // 1
        $objPHPExcel->getActiveSheet()->getStyle("B6:B7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("B6:B7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("B6:B7");
        $objPHPExcel->getActiveSheet()->getStyle("B6:B7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'NAME');
        /* END NAMA KARYAWAN  */

        $titleColIdx++; // 1
        $objPHPExcel->getActiveSheet()->getStyle("C6:C7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("C6:C7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("C6:C7");
        $objPHPExcel->getActiveSheet()->getStyle("C6:C7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'PLACE OF BIRTH');

        /* START NO BPJS  */
        $titleColIdx++; // 2
        $objPHPExcel->getActiveSheet()->getStyle("D6:D7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("D6:D7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("D6:D7");
        $objPHPExcel->getActiveSheet()->getStyle("D6:D7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'DATE OF BIRTH');
        /* END NO BPJS  */

          /* START GROUP  */
        $titleColIdx++; // 2
        $objPHPExcel->getActiveSheet()->getStyle("E6:E7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("E6:E7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("E6:E7");
        $objPHPExcel->getActiveSheet()->getStyle("E6:E7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'ID CARD NUMBER');
        /* END GROUP  */

        /* START ETHNIC */
        $titleColIdx++; // 3
        $objPHPExcel->getActiveSheet()->getStyle("F6:F7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("F6:F7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("F6:F7");
        $objPHPExcel->getActiveSheet()->getStyle("F6:F7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'ADDRESS');
        /* END ETHNIC */

        /* START MARITAL STATUS  */
        $titleColIdx++; // 4
        $objPHPExcel->getActiveSheet()->getStyle("G6:G7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("G6:G7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("G6:G7");
        $objPHPExcel->getActiveSheet()->getStyle("G6:G7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'NPWP NUMBER');
        /* END MARITAL STATUS  */

        /* START NPWP */
        $titleColIdx++; // 5
        $objPHPExcel->getActiveSheet()->getStyle("H6:H7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("H6:H7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("H6:H7");
        $objPHPExcel->getActiveSheet()->getStyle("H6:H7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'MARITAL STATUS');
        /* END NPWP */

        /* START SALARY LEVEL */
        $titleColIdx++; // 6
        $objPHPExcel->getActiveSheet()->getStyle("I6:I7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("I6:I7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("I6:I7");
        $objPHPExcel->getActiveSheet()->getStyle("I6:I7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'POSITION');
        /* END SALARY LEVEL */

        /* START BASIC SALARY */
        $titleColIdx++; // 7
        $objPHPExcel->getActiveSheet()->getStyle("J6:J7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("J6:J7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("J6:J7");
        $objPHPExcel->getActiveSheet()->getStyle("J6:J7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'BASIC SALARY');
        /* END BASIC SALARY */  

        /* START JKK-JKM(2.04%) */
        $titleColIdx++; // 7
        $objPHPExcel->getActiveSheet()->getStyle("K6:K7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("K6:K7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("K6:K7");
        $objPHPExcel->getActiveSheet()->getStyle("K6:K7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'BANK NAME');
        /* END JKK-JKM(2.04%) */

        /* START BPJS KESEHATAN(4%) */
        $titleColIdx++; // 7
        $objPHPExcel->getActiveSheet()->getStyle("L6:L7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("L6:L7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("L6:L7");
        $objPHPExcel->getActiveSheet()->getStyle("L6:L7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'ACCOUNT NAME');
        /* END BPJS KESEHATAN(4%) */  

        $titleColIdx++; // 7
        $objPHPExcel->getActiveSheet()->getStyle("M6:M7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("M6:M7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("M6:M7");
        $objPHPExcel->getActiveSheet()->getStyle("M6:M7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'ACCOUNT NUMBER');

        // $titleColIdx++; // 7
        // $objPHPExcel->getActiveSheet()->getStyle("N6:N7")->applyFromArray($allBorderStyle);
        // $objPHPExcel->getActiveSheet()->getStyle("N6:N7")->applyFromArray($center);
        // $objPHPExcel->getActiveSheet()->mergeCells("N6:N7");
        // $objPHPExcel->getActiveSheet()->getStyle("N6:N7")->getAlignment()->setWrapText(true);
        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'BPJS KESEHATAN(4%)');

        //  $titleColIdx++; // 7
        // $objPHPExcel->getActiveSheet()->getStyle("O6:O7")->applyFromArray($allBorderStyle);
        // $objPHPExcel->getActiveSheet()->getStyle("O6:O7")->applyFromArray($center);
        // $objPHPExcel->getActiveSheet()->mergeCells("O6:O7");
        // $objPHPExcel->getActiveSheet()->getStyle("O6:O7")->getAlignment()->setWrapText(true);
        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'JHT EMPLOYEE(2%)');
 
        // $titleColIdx++; // 7
        // $objPHPExcel->getActiveSheet()->getStyle("P6:P7")->applyFromArray($allBorderStyle);
        // $objPHPExcel->getActiveSheet()->getStyle("P6:P7")->applyFromArray($center);
        // $objPHPExcel->getActiveSheet()->mergeCells("P6:P7");
        // $objPHPExcel->getActiveSheet()->getStyle("P6:P7")->getAlignment()->setWrapText(true);
        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'JHT COMPANY(3.7%)');

        // $titleColIdx++; // 7
        // $objPHPExcel->getActiveSheet()->getStyle("Q6:Q7")->applyFromArray($allBorderStyle);
        // $objPHPExcel->getActiveSheet()->getStyle("Q6:Q7")->applyFromArray($center);
        // $objPHPExcel->getActiveSheet()->mergeCells("Q6:Q7");
        // $objPHPExcel->getActiveSheet()->getStyle("Q6:Q7")->getAlignment()->setWrapText(true);
        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'JAMINAN PENSIUN (1%)');

        // $titleColIdx++; // 7
        // $objPHPExcel->getActiveSheet()->getStyle("R6:R7")->applyFromArray($allBorderStyle);
        // $objPHPExcel->getActiveSheet()->getStyle("R6:R7")->applyFromArray($center);
        // $objPHPExcel->getActiveSheet()->mergeCells("R6:R7");
        // $objPHPExcel->getActiveSheet()->getStyle("R6:R7")->getAlignment()->setWrapText(true);
        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'JAMINAN PENSIUN (2%)');

        // $titleColIdx++; // 7
        // $objPHPExcel->getActiveSheet()->getStyle("S6:S7")->applyFromArray($allBorderStyle);
        // $objPHPExcel->getActiveSheet()->getStyle("S6:S7")->applyFromArray($center);
        // $objPHPExcel->getActiveSheet()->mergeCells("S6:S7");
        // $objPHPExcel->getActiveSheet()->getStyle("S6:S7")->getAlignment()->setWrapText(true);
        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'NET PAYMENT');

        $rowIdx = 7;
        $rowNo = 0;
        foreach ($query as $row) {

            $rowIdx++;
            $rowNo++;
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$rowIdx, $row['nie']);
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$rowIdx, $row['full_name'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$rowIdx, $row['place_of_birth'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$rowIdx, $row['date_of_birth']);
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$rowIdx, $row['id_card_no'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$rowIdx, $row['id_card_address']);
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$rowIdx, $row['npwp_no'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValue('H'.$rowIdx, $row['marital_status']);
            $objPHPExcel->getActiveSheet()->setCellValue('I'.$rowIdx, $row['position']);
            $objPHPExcel->getActiveSheet()->setCellValue('J'.$rowIdx, $row['basic_salary']);
            $objPHPExcel->getActiveSheet()->setCellValue('K'.$rowIdx, $row['bank_name']);
            $objPHPExcel->getActiveSheet()->setCellValue('L'.$rowIdx, $row['account_name']);
            $objPHPExcel->getActiveSheet()->setCellValue('M'.$rowIdx, $row['account_no']);
            // $objPHPExcel->getActiveSheet()->setCellValue('N'.$rowIdx, $row['health_bpjs']);
            // $objPHPExcel->getActiveSheet()->setCellValue('O'.$rowIdx, $row['emp_jht']);
            // $objPHPExcel->getActiveSheet()->setCellValue('P'.$rowIdx, $row['company_jht']);
            // $objPHPExcel->getActiveSheet()->setCellValue('Q'.$rowIdx, $row['emp_jp']);
            // $objPHPExcel->getActiveSheet()->setCellValue('R'.$rowIdx, $setbasic_salary);
            // $objPHPExcel->getActiveSheet()->setCellValue('S'.$rowIdx, $row['total']);

            /* SET ROW COLOR */
            if($rowIdx % 2 == 1)
            {
                $objPHPExcel->getActiveSheet()->getStyle('A'.$rowIdx.':M'.$rowIdx)
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('EAEBAF');             
            } 

        } /* end foreach ($query as $row) */       
        
        // $objPHPExcel->getActiveSheet()->getStyle("A".($rowIdx+2).":M".($rowIdx+2))
        // ->getFill()
        // ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        // ->getStartColor()
        // ->setRGB('F2BE6B');

        /* SET NUMBERS FORMAT*/

        unset($allBorderStyle);
        unset($center);
        unset($right);
        unset($left);
        
        $objPHPExcel->setActiveSheetIndex(0);

        //Set Title
        $objPHPExcel->getActiveSheet()->setTitle('Data Crews LCP');

        //Save ke .xlsx, kalau ingin .xls, ubah 'Excel2007' menjadi 'Excel5'
        $writer = IOFactory::createWriter($objPHPExcel, 'Xlsx');
        $writer->setPreCalculateFormulas(true);

        //Header
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

        //Nama File
        $fileName = "Data Crews LCP";
        header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx"');
        // header('Content-Disposition: attachment;filename="'.$client.$yearPeriod.$monthPeriod'.xlsx"');

        //Download
        $writer->save('php://output');
        exit(0); 
    }

    public function dataListActive()
    {

        $sql  = ' SELECT ms.`payroll_group`,ms.nie AS Badge_Id,mb.`full_name` AS employee_name,mb.`place_of_birth`,mb.`date_of_birth`,
                    ct.`position`,ms.basic_salary AS basic_monthly,
                    mb.npwp_no,mb.`religion`,mb.`marital_status`,mb.`is_staff` AS employee_status,mb.`local_foreign` AS point_of_origin,ct.`contract_start`,
                    ct.`contract_end`,ms.`bank_name`,ms.`account_no`,ms.`account_name`,ct.dept,mb.date_of_hire

                    FROM `db_recruitment`.`mst_bio_rec` mb,`db_recruitment`.`mst_contract` ct,`db_recruitment`.`mst_salary` ms

                    WHERE mb.bio_rec_id = ct.bio_rec_id AND mb.bio_rec_id = ms.bio_rec_id AND ct.`is_active` = 1
                    AND ms.`company_name` = "'.$this->input->post('pt').'"
                    ORDER BY  ms.`payroll_group`,mb.`full_name` ';

        $data = $this->db->query($sql);
        $query= $data->result_array();

        $data = array();
        foreach ($query as $key => $row) 
        {
            $data[] = array
            (
                $row['payroll_group'],         
                $row['Badge_Id'],                     
                $row['employee_name'],         
                $row['date_of_birth'],       
                $row['position'],       
                $row['dept'],       
                $row['basic_monthly'],         
                $row['religion'],         
                $row['npwp_no'],         
                $row['marital_status'],       
                $row['employee_status'],       
                $row['point_of_origin'],         
                $row['date_of_hire'],         
                $row['contract_start'],         
                $row['contract_end'],                     
                $row['bank_name'],              
                $row['account_name'],              
                $row['account_no'],              
            );            
        }  
        echo json_encode($data);   
    }

    function exportDataListActive($pt){

        $objPHPExcel = new Spreadsheet();

        $strSQL   = ' SELECT ms.`payroll_group`,ms.nie AS Badge_Id,mb.`full_name` AS employee_name,mb.`place_of_birth`,mb.`date_of_birth`,
                    ct.`position`,ms.basic_salary AS basic_monthly,
                    mb.npwp_no,mb.`religion`,mb.`marital_status`,mb.`is_staff` AS employee_status,mb.`local_foreign` AS point_of_origin,ct.`contract_start`,
                    ct.`contract_end`,ms.`bank_name`,ms.`account_no`,ms.`account_name`,ct.dept,mb.date_of_hire

                    FROM `db_recruitment`.`mst_bio_rec` mb,`db_recruitment`.`mst_contract` ct,`db_recruitment`.`mst_salary` ms

                    WHERE mb.bio_rec_id = ct.bio_rec_id AND mb.bio_rec_id = ms.bio_rec_id AND ct.`is_active` = 1
                    AND ms.`company_name` = "'.$pt.'"
                    ORDER BY  ms.`payroll_group`,mb.`full_name` ';

        $query = $this->db->query($strSQL)->result_array();

        // Nama Field Baris Pertama
        $objPHPExcel->getActiveSheet()
            ->setCellValue('A1', 'PT. SANGATI SOERYA SEJAHTERA')
            ->setCellValue('A2', 'DATA List Active PT. '.strtoupper($pt) );
            // ->setCellValue('A4', 'Periode : '.$monthPeriod.'-'.$yearPeriod);

        $objPHPExcel->getActiveSheet()->getStyle("A1:S1")->getFont()->setBold(true)->setSize(16);
        $objPHPExcel->getActiveSheet()->getStyle("A2:S2")->getFont()->setBold(true)->setSize(13);
        $objPHPExcel->getActiveSheet()->getStyle("A4:S4")->getFont()->setBold(true)->setSize(12); 

        $totalStyle = array(
            'font'  => array(
                'bold'  => true,
                'color' => array('rgb' => '0000FF'),
                // 'size'  => 15,
                // 'name'  => 'Verdana'
            )
        );
        
        $allBorderStyle = array(
            'borders' => array(
                'allborders' => array(
                    'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                )
            )
        );

        $outlineBorderStyle = array(
          'borders' => array(
            'outline' => array(
              'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
            )
          )
        );

        $topBorderStyle = array(
          'borders' => array(
            'top' => array(
              'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
            )
          )
        );

        $bottomBorderStyle = array(
          'borders' => array(
            'bottom' => array(
              'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
            )
          )
        );

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $objPHPExcel->getActiveSheet()->getStyle("A6:R7")
        ->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()
        ->setRGB('F2BE6B');      


        /* START PAYMENT TITLE */
        $objPHPExcel->getActiveSheet()->getStyle("A6:S6")->getFont()->setBold(true)->setSize(12);
        $objPHPExcel->getActiveSheet()->getStyle("A6:S6")->applyFromArray($outlineBorderStyle);
        /* START TITLE NO */
        $titleRowIdx = 6;
        $titleColIdx = 1;
        $objPHPExcel->getActiveSheet()->getStyle("A6:A7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("A6:A7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("A6:A7");
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'Payroll Group');
        /* END TITLE NO */

        /* START NAMA KARYAWAN  */
        $titleColIdx++; // 1
        $objPHPExcel->getActiveSheet()->getStyle("B6:B7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("B6:B7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("B6:B7");
        $objPHPExcel->getActiveSheet()->getStyle("B6:B7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'Badge Id');
        /* END NAMA KARYAWAN  */

        $titleColIdx++; // 1
        $objPHPExcel->getActiveSheet()->getStyle("C6:C7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("C6:C7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("C6:C7");
        $objPHPExcel->getActiveSheet()->getStyle("C6:C7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'Employee Name');

        /* START NO BPJS  */
        $titleColIdx++; // 2
        $objPHPExcel->getActiveSheet()->getStyle("D6:D7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("D6:D7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("D6:D7");
        $objPHPExcel->getActiveSheet()->getStyle("D6:D7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'Date Of Birth');
        /* END NO BPJS  */

          /* START GROUP  */
        $titleColIdx++; // 2
        $objPHPExcel->getActiveSheet()->getStyle("E6:E7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("E6:E7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("E6:E7");
        $objPHPExcel->getActiveSheet()->getStyle("E6:E7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'Position');
        /* END GROUP  */

        /* START ETHNIC */
        $titleColIdx++; // 3
        $objPHPExcel->getActiveSheet()->getStyle("F6:F7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("F6:F7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("F6:F7");
        $objPHPExcel->getActiveSheet()->getStyle("F6:F7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'Department');
        /* END ETHNIC */

        /* START MARITAL STATUS  */
        $titleColIdx++; // 4
        $objPHPExcel->getActiveSheet()->getStyle("G6:G7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("G6:G7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("G6:G7");
        $objPHPExcel->getActiveSheet()->getStyle("G6:G7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'Basic Salary');
        /* END MARITAL STATUS  */

        /* START NPWP */
        $titleColIdx++; // 5
        $objPHPExcel->getActiveSheet()->getStyle("H6:H7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("H6:H7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("H6:H7");
        $objPHPExcel->getActiveSheet()->getStyle("H6:H7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'Religion');
        /* END NPWP */

        /* START SALARY LEVEL */
        $titleColIdx++; // 6
        $objPHPExcel->getActiveSheet()->getStyle("I6:I7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("I6:I7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("I6:I7");
        $objPHPExcel->getActiveSheet()->getStyle("I6:I7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'NPWP Number');
        /* END SALARY LEVEL */

        /* START BASIC SALARY */
        $titleColIdx++; // 7
        $objPHPExcel->getActiveSheet()->getStyle("J6:J7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("J6:J7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("J6:J7");
        $objPHPExcel->getActiveSheet()->getStyle("J6:J7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'Marital Status');
        /* END BASIC SALARY */  

        $titleColIdx++; // 7
        $objPHPExcel->getActiveSheet()->getStyle("K6:K7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("K6:K7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("K6:K7");
        $objPHPExcel->getActiveSheet()->getStyle("K6:K7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'Employee Status');

        $titleColIdx++; // 7
        $objPHPExcel->getActiveSheet()->getStyle("L6:L7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("L6:L7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("L6:L7");
        $objPHPExcel->getActiveSheet()->getStyle("L6:L7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'Poin Of Hire/Origin');

        /* START JKK-JKM(2.04%) */
        $titleColIdx++; // 7
        $objPHPExcel->getActiveSheet()->getStyle("M6:M7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("M6:M7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("M6:M7");
        $objPHPExcel->getActiveSheet()->getStyle("M6:M7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'Date Of Hire');
        /* END JKK-JKM(2.04%) */

        /* START BPJS KESEHATAN(4%) */
        $titleColIdx++; // 7
        $objPHPExcel->getActiveSheet()->getStyle("N6:N7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("N6:N7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("N6:N7");
        $objPHPExcel->getActiveSheet()->getStyle("N6:N7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'Contract Start');
        /* END BPJS KESEHATAN(4%) */  

        $titleColIdx++; // 7
        $objPHPExcel->getActiveSheet()->getStyle("O6:O7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("O6:O7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("O6:O7");
        $objPHPExcel->getActiveSheet()->getStyle("O6:O7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'Contract End');

        /* START JKK-JKM(2.04%) */
        $titleColIdx++; // 7
        $objPHPExcel->getActiveSheet()->getStyle("P6:P7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("P6:P7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("P6:P7");
        $objPHPExcel->getActiveSheet()->getStyle("P6:P7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'BANK NAME');
        /* END JKK-JKM(2.04%) */

        /* START BPJS KESEHATAN(4%) */
        $titleColIdx++; // 7
        $objPHPExcel->getActiveSheet()->getStyle("Q6:Q7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("Q6:Q7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("Q6:Q7");
        $objPHPExcel->getActiveSheet()->getStyle("Q6:Q7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'ACCOUNT NAME');
        /* END BPJS KESEHATAN(4%) */  

        $titleColIdx++; // 7
        $objPHPExcel->getActiveSheet()->getStyle("R6:R7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("R6:R7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("R6:R7");
        $objPHPExcel->getActiveSheet()->getStyle("R6:R7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'ACCOUNT NUMBER');

        // $titleColIdx++; // 7
        // $objPHPExcel->getActiveSheet()->getStyle("N6:N7")->applyFromArray($allBorderStyle);
        // $objPHPExcel->getActiveSheet()->getStyle("N6:N7")->applyFromArray($center);
        // $objPHPExcel->getActiveSheet()->mergeCells("N6:N7");
        // $objPHPExcel->getActiveSheet()->getStyle("N6:N7")->getAlignment()->setWrapText(true);
        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'BPJS KESEHATAN(4%)');

        //  $titleColIdx++; // 7
        // $objPHPExcel->getActiveSheet()->getStyle("O6:O7")->applyFromArray($allBorderStyle);
        // $objPHPExcel->getActiveSheet()->getStyle("O6:O7")->applyFromArray($center);
        // $objPHPExcel->getActiveSheet()->mergeCells("O6:O7");
        // $objPHPExcel->getActiveSheet()->getStyle("O6:O7")->getAlignment()->setWrapText(true);
        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'JHT EMPLOYEE(2%)');
 
        // $titleColIdx++; // 7
        // $objPHPExcel->getActiveSheet()->getStyle("P6:P7")->applyFromArray($allBorderStyle);
        // $objPHPExcel->getActiveSheet()->getStyle("P6:P7")->applyFromArray($center);
        // $objPHPExcel->getActiveSheet()->mergeCells("P6:P7");
        // $objPHPExcel->getActiveSheet()->getStyle("P6:P7")->getAlignment()->setWrapText(true);
        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'JHT COMPANY(3.7%)');

        // $titleColIdx++; // 7
        // $objPHPExcel->getActiveSheet()->getStyle("Q6:Q7")->applyFromArray($allBorderStyle);
        // $objPHPExcel->getActiveSheet()->getStyle("Q6:Q7")->applyFromArray($center);
        // $objPHPExcel->getActiveSheet()->mergeCells("Q6:Q7");
        // $objPHPExcel->getActiveSheet()->getStyle("Q6:Q7")->getAlignment()->setWrapText(true);
        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'JAMINAN PENSIUN (1%)');

        // $titleColIdx++; // 7
        // $objPHPExcel->getActiveSheet()->getStyle("R6:R7")->applyFromArray($allBorderStyle);
        // $objPHPExcel->getActiveSheet()->getStyle("R6:R7")->applyFromArray($center);
        // $objPHPExcel->getActiveSheet()->mergeCells("R6:R7");
        // $objPHPExcel->getActiveSheet()->getStyle("R6:R7")->getAlignment()->setWrapText(true);
        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'JAMINAN PENSIUN (2%)');

        // $titleColIdx++; // 7
        // $objPHPExcel->getActiveSheet()->getStyle("S6:S7")->applyFromArray($allBorderStyle);
        // $objPHPExcel->getActiveSheet()->getStyle("S6:S7")->applyFromArray($center);
        // $objPHPExcel->getActiveSheet()->mergeCells("S6:S7");
        // $objPHPExcel->getActiveSheet()->getStyle("S6:S7")->getAlignment()->setWrapText(true);
        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'NET PAYMENT');

        $rowIdx = 7;
        $rowNo = 0;
        foreach ($query as $row) { 

            $rowIdx++;
            $rowNo++;
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$rowIdx, $row['payroll_group']);
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$rowIdx, $row['Badge_Id']);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$rowIdx, $row['employee_name']);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$rowIdx, $row['date_of_birth']);
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$rowIdx, $row['position']);
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$rowIdx, $row['dept']);
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$rowIdx, $row['basic_monthly']);
            $objPHPExcel->getActiveSheet()->setCellValue('H'.$rowIdx, $row['religion']);
            $objPHPExcel->getActiveSheet()->setCellValue('I'.$rowIdx, $row['npwp_no']);
            $objPHPExcel->getActiveSheet()->setCellValue('J'.$rowIdx, $row['marital_status']);
            $objPHPExcel->getActiveSheet()->setCellValue('K'.$rowIdx, $row['employee_status']);
            $objPHPExcel->getActiveSheet()->setCellValue('L'.$rowIdx, $row['point_of_origin']);
            $objPHPExcel->getActiveSheet()->setCellValue('M'.$rowIdx, $row['date_of_hire']);
            $objPHPExcel->getActiveSheet()->setCellValue('N'.$rowIdx, $row['contract_start']);
            $objPHPExcel->getActiveSheet()->setCellValue('O'.$rowIdx, $row['contract_end']);
            $objPHPExcel->getActiveSheet()->setCellValue('P'.$rowIdx, $row['bank_name']);
            $objPHPExcel->getActiveSheet()->setCellValue('Q'.$rowIdx, $row['account_name']);
            $objPHPExcel->getActiveSheet()->setCellValue('R'.$rowIdx, $row['account_no']);

            /* SET ROW COLOR */
            if($rowIdx % 2 == 1)
            {
                $objPHPExcel->getActiveSheet()->getStyle('A'.$rowIdx.':R'.$rowIdx)
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('EAEBAF');             
            } 

        } /* end foreach ($query as $row) */       
        
        // $objPHPExcel->getActiveSheet()->getStyle("A".($rowIdx+2).":M".($rowIdx+2))
        // ->getFill()
        // ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        // ->getStartColor()
        // ->setRGB('F2BE6B');

        /* SET NUMBERS FORMAT*/

        unset($allBorderStyle);
        unset($center);
        unset($right);
        unset($left);
        
        $objPHPExcel->setActiveSheetIndex(0);

        //Set Title
        $objPHPExcel->getActiveSheet()->setTitle('Data List Active');

        //Save ke .xlsx, kalau ingin .xls, ubah 'Excel2007' menjadi 'Excel5'
        $writer = IOFactory::createWriter($objPHPExcel, 'Xlsx');
        $writer->setPreCalculateFormulas(true);

        //Header
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

        //Nama File
        $fileName = "Data List Active";
        header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx"');
        // header('Content-Disposition: attachment;filename="'.$client.$yearPeriod.$monthPeriod'.xlsx"');

        //Download
        $writer->save('php://output');
        exit(0); 
    }


}

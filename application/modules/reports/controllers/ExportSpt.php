<?php
defined('BASEPATH') OR exit('No direct script access allowed');
    // Load library phpspreadsheet
    require('./vendor/autoload.php');
    use PhpOffice\PhpSpreadsheet\Helper\Sample;
    use PhpOffice\PhpSpreadsheet\IOFactory;
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    // End load library phpspreadsheet

class ExportSpt extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('masters/M_mst_contract');
        $this->load->model('masters/M_mst_salary', 'M_salary'); 
        $this->load->model('masters/M_payroll_config');
        $this->load->model('masters/M_mst_pajak');
        $this->load->model('transactions/M_salary_slip');
    }

    public function getExprtView(){
        $pt = "";
        $yearPeriod = "";
        // $month = "";
        if(isset($_POST['pt']))
        {
            $pt = $_POST['pt'];
        }
        if(isset($_POST['year']))
        {
            $yearPeriod = $_POST['year'];
        }
      
        $strSQL  = '';
        if ($pt == 'Pontil_Sumbawa'){
            // test('oke',1);
            $strSQL  = "SELECT mb.bio_rec_id,ss.salary_slip_id,mb.full_name,ms.nie,mc.contract_start AS contract_start,mc.contract_end AS contract_end,mb.gender,mb.position,mb.marital_status,mb.id_card_no,mb.id_card_address,mb.current_address,mb.npwp_no,ss.year_period,ss.client_name,ss.bs_prorate,mb.nationality,(ss.ot_1+ss.ot_2+ss.ot_3+ss.ot_4)overtime,ss.thr,ss.contract_bonus,ss.health_bpjs,ss.jkk_jkm,ss.emp_jp,ss.emp_jht,ss.emp_health_bpjs ";
            $strSQL .= "FROM db_recruitment.mst_bio_rec mb, db_recruitment.mst_salary ms, db_recruitment.trn_slip_ptlsmb ss, db_recruitment.mst_contract mc ";
            $strSQL .= "WHERE mb.bio_rec_id = ms.bio_rec_id  ";
            $strSQL .= "AND mb.bio_rec_id = ss.bio_rec_id ";
            $strSQL .= "AND mb.bio_rec_id = mc.bio_rec_id ";
            $strSQL .= "AND ms.company_name = '".$pt."' ";
            $strSQL .= "AND ss.year_period = '".$yearPeriod."' ";
            $strSQL .= "AND YEAR(mc.contract_start) = '".$yearPeriod."' ";
            $strSQL .= "AND YEAR(mc.contract_end) = '".$yearPeriod."' ";
            $strSQL .= "AND mb.is_active = 1 ";
            // $strSQL .= "AND mc.is_active = 1 ";
            $strSQL .= "GROUP BY mb.full_name ASC ";
        }
        
        
        // $strSQL .= "AND mb.is_active = 1 ";       
        // test($strSQL,1);
        $query = $this->db->query($strSQL)->result_array();

        $myData = array();
        foreach ($query as $key => $row) {
            $myData[] = array(
                $row['salary_slip_id'],         
                $row['bio_rec_id'],         
                $row['nie'],         
                $row['client_name'],         
                $row['full_name'],         
                $row['year_period'],         
                $row['position']         
            );            
        }  
        echo json_encode($myData);  
    }

    // function cekTunjangan($max_bln,$row,$jmlMasa,$biodataId,$yearPeriod,$monthPeriod){

    //     $taxPercent1 = $this->M_payroll_config->getTaxPercent1(); /* Persentase Progresif Pajak I */
    //     $taxPercent2 = $this->M_payroll_config->getTaxPercent2(); /* Persentase Progresif Pajak II */
    //     $taxPercent3 = $this->M_payroll_config->getTaxPercent3(); /* Persentase Progresif Pajak III */
    //     $taxPercent4 = $this->M_payroll_config->getTaxPercent4(); /* Persentase Progresif Pajak IV */

    //     $maxTaxVal1 = $this->M_payroll_config->getTaxMaxVal1(); /* Nominal Pajak I */ 
    //     $maxTaxVal2 = $this->M_payroll_config->getTaxMaxVal2(); /* Nominal Pajak II */
    //     $maxTaxVal3 = $this->M_payroll_config->getTaxMaxVal3(); /* Nominal Pajak III */
    //     $maxTaxVal4 = $this->M_payroll_config->getTaxMaxVal4(); /* Nominal Pajak IV */
    //     /* End Progresif  */

    //     $tunjanganPPH   = 0; /* tunjangan pph */
    //     $maxNonTax      = $this->M_payroll_config->getNonTaxAllowance();

    //     for( $x = 1; $x < $max_bln; $x++){
    //         $queryOfmonth = " SELECT bio_rec_id,year_period,SUM(bs_prorate) AS total,SUM(ot_1+ot_2+ot_3+ot_4) AS overtime, SUM(flying_camp+travel_bonus+attendance_bonus+ other_allowance1) AS other_bonus, SUM(thr+contract_bonus) AS Thr_Cbonus,jkk_jkm, SUM(emp_jp +emp_jht + emp_health_bpjs + health_bpjs) AS ass_employee,SUM(jp+jht) AS jp,ptkp_total FROM trn_slip_ptlsmb 
    //         WHERE bio_rec_id = '".$biodataId."' 
    //         AND year_period = '".$yearPeriod."' 
    //         AND month_period = '".$monthPeriod."' ";

    //         $sqlOfMonth   = $this->db->query($queryOfmonth)->row_array();

    //         /* Start Jumlah 4 Honorarium */
    //         $jmlHonorarium  = 0;
    //         /*End Jumlah 4 Honorarium */
    //         $overtime = $row['overtime'] + $row['other_bonus'];
            

    //         /* Start Jumlah 8 Total Jumlah 1 s/d 7 */
    //         $jmlHasilBruto    = $row['total'] + $tunjanganPPH + $overtime + $jmlHonorarium + $row['ass_employee'] +  $row['Thr_Cbonus'];
    //         /* End Jumlah 8 Total Jumlah 1 s/d 7 */

    //         /* Start Biaya Jabatan Max 500.000 */         
    //         $biayaJabatan   = 0;
            
    //         if($biayaJabatan > $maxNonTax){
    //             $biayaJabatan = $maxNonTax; 
    //         }

    //         $biayaJabatan   = $jmlHasilBruto * (5/100);

    //         if($biayaJabatan > 6000000){
    //             $biayaJabatan =  6000000;
    //         }else{
    //             $biayaJabatan = $jmlHasilBruto * (5/100);
    //         }

    //         /* Start Pembulatan $biayaJabatan Round 1000 */
    //         $roundBiayaJabatan = $biayaJabatan;
    //         $roundBiayaJabatan = floor($roundBiayaJabatan);

    //         if(substr($roundBiayaJabatan,-3)<499){
    //             $biayaJabatan = round($roundBiayaJabatan,-3);
    //         }else{
    //             $biayaJabatan = round($roundBiayaJabatan,-3)-1000;
    //         }
                
    //         /* End Pembulatan $biayaJabatan Round 1000 */

    //         /* End Biaya Jabatan Max 500.0000 Jumlah 9 */

    //         /* Start Jumlah 11 */
    //         $jmlPengurangan   = $biayaJabatan + $row['jp'];
    //         /* End Jumlah 11 */

    //         /* Start Jumlah 12 */
    //         $jmlPhslNetto     = $jmlHasilBruto - $jmlPengurangan;
    //         /* End Jumlah 12 */

    //         /*Start Jumlah 13 */
    //         $jmlNettoSblmnya  = 0;
    //         /*End Jumlah 13 */

    //         /* Start Jumlah 15 */
    //         $totalPtkp  = $row['ptkp_total'];
    //         /* End Jumlah 15 */

    //         /* Start Jumlah 16 */
    //         $pkp_total = 0;
    //         if($jmlPhslNetto >= $totalPtkp){
    //             $pkp_total = $jmlPhslNetto - $totalPtkp;
    //         }else{
    //             $pkp_total = 0;
    //         }

    //         $roundPkpTotal = $pkp_total;
    //         $roundPkpTotal = floor($roundPkpTotal);

    //         if(substr($roundPkpTotal,-3)<499){
    //             $pkp_total = round($roundPkpTotal,-3);
    //         }else{
    //             $pkp_total = round($roundPkpTotal,-3)-1000;
    //         }
    //         // $pkp_total     = floor($pkp_total); 
    //         /* End Jumlah 16 */
            
            
    //         /* Start Tax terutang setahun */
    //         $taxVal1 = 0;    
    //         $taxVal2 = 0;    
    //         $taxVal3 = 0;    
    //         $taxVal4 = 0;

    //         $tVal    = 0;
    //         $tSisa   = 0;
    //         if($pkp_total > 0)
    //         {
    //             /* TAX 1 */
    //             if($maxTaxVal1 > 0)
    //             {
    //                 $tVal = $pkp_total/$maxTaxVal1;  
    //                 if($tVal >= 1){
    //                     $taxVal1 = $maxTaxVal1 * ($taxPercent1/100);

    //                 }
    //                 else{
    //                     $taxVal1 = $pkp_total * ($taxPercent1/100);
    //                 }    
    //             }    
                        
    //             /* TAX 2 */
    //             if($maxTaxVal2 > 0)
    //             {
    //                 if($pkp_total > $maxTaxVal1)
    //                 {
    //                     $tSisa = $pkp_total - $maxTaxVal1; 
    //                     $tVal = $tSisa/$maxTaxVal2;
    //                         if($tVal >= 1){
    //                             $taxVal2 = $maxTaxVal2 * ($taxPercent2/100);
    //                         }
    //                         else{
    //                             $taxVal2 = $tSisa * ($taxPercent2/100);
    //                         }
    //                 }     
    //             }
                        
    //             /* TAX 3 */
    //             if($maxTaxVal3 > 0)
    //             {
    //                 if($pkp_total > ($maxTaxVal1 + $maxTaxVal2))
    //                 {
    //                     $tSisa = $pkp_total - $maxTaxVal1 - $maxTaxVal2;
    //                     $tVal = $tSisa/$maxTaxVal3;
    //                         if($tVal >= 1){
    //                             $taxVal3 = $maxTaxVal3 * ($taxPercent3/100);
    //                         }
    //                         else{
    //                             $taxVal3 = $tSisa * ($taxPercent3/100);
    //                         }
    //                 }    
    //             }
                        
    //             /* TAX 4 */
    //             if($pkp_total > ($maxTaxVal1 + $maxTaxVal2 + $maxTaxVal3))
    //             {
    //                 $tSisa = $pkp_total - $maxTaxVal1 - $maxTaxVal2 - $maxTaxVal3;
    //                 $taxVal4 = $tSisa * ($taxPercent4/100); 
    //             }
    //         }

    //         $taxTerutangSetahun = ($taxVal1 + $taxVal2 + $taxVal3 + $taxVal4)*12/$jmlMasa;

    //         $pajakSebulan  = number_format(($taxTerutangSetahun),2,".","");  /*Pajak terutang setahun / 12 = hasil pajak sebulan*/
    //         // test($pajakSebulan,1);

    //         if($tunjanganPPH == $pajakSebulan)
    //         {
    //             break;
    //         }

    //         $tunjanganPPH       = $pajakSebulan;
    //     }

    //     return $tunjanganPPH;
    // }

    function proses_data_spt($ptName, $yearPeriod){

        if ($ptName == 'Pontil_Sumbawa'){
            $strSQL  = "SELECT c.bio_rec_id,b.nie,a.full_name,c.month_period,
                        (SELECT MIN(d.contract_start) AS contract_start FROM mst_contract d WHERE YEAR(d.contract_end) = '".$yearPeriod."' AND d.bio_rec_id=
                        c.bio_rec_id) AS contract_start,
                        (SELECT MAX(d.contract_end) AS contract_end FROM mst_contract d WHERE YEAR(d.contract_end) = '".$yearPeriod."' AND d.bio_rec_id=
                        c.bio_rec_id) AS contract_end,
                        c.year_period,
                        SUM(c.bs_prorate) AS total,
                        a.nationality,a.gender,a.position,a.marital_status,a.id_card_no,
                        a.id_card_address,a.current_address,a.npwp_no,
                        SUM(c.ot_1+c.ot_2+c.ot_3+c.ot_4) AS overtime,
                        SUM(c.flying_camp + c.travel_bonus + c.attendance_bonus + c.other_allowance1) AS other_bonus,
                        SUM(c.thr + c.contract_bonus) AS Thr_Cbonus,
                        c.jkk_jkm,
                        SUM(c.emp_jp + c.emp_jht + c.emp_health_bpjs + c.health_bpjs) AS ass_employee,
                        SUM(c.jp + c.jht) AS jp,
                        c.ptkp_total, b.is_tax_allowance,right(a.marital_status,1) stats ";
            $strSQL .= "FROM trn_slip_ptlsmb c ";
            $strSQL .= "JOIN mst_bio_rec a ON c.bio_rec_id = a.bio_rec_id ";
            $strSQL .= "LEFT JOIN mst_salary b ON c.bio_rec_id = b.bio_rec_id ";
            $strSQL .= "WHERE c.client_name = '".$ptName."' AND c.bio_rec_id='2019040360' ";
            $strSQL .= "AND a.is_active = '1' ";
            $strSQL .= "AND c.year_period = '".$yearPeriod."' ";
            $strSQL .= "GROUP BY c.bio_rec_id ";
            $strSQL .= "ORDER BY a.full_name ";
        }       
        // test($strSQL,1);
        $query = $this->db->query($strSQL)->result_array();

        $noBiodataId        = 0;
        $failedNames        = array();
        
        foreach ($query as $key => $row) {

            $maxNonTax      = $this->M_payroll_config->getNonTaxAllowance();
            $taxPercent1    = $this->M_payroll_config->getTaxPercent1(); /* Persentase Progresif Pajak I */
            $taxPercent2    = $this->M_payroll_config->getTaxPercent2(); /* Persentase Progresif Pajak II */
            $taxPercent3    = $this->M_payroll_config->getTaxPercent3(); /* Persentase Progresif Pajak III */
            $taxPercent4    = $this->M_payroll_config->getTaxPercent4(); /* Persentase Progresif Pajak IV */

            $maxTaxVal1     = $this->M_payroll_config->getTaxMaxVal1(); /* Nominal Pajak I */ 
            $maxTaxVal2     = $this->M_payroll_config->getTaxMaxVal2(); /* Nominal Pajak II */
            $maxTaxVal3     = $this->M_payroll_config->getTaxMaxVal3(); /* Nominal Pajak III */
            $maxTaxVal4     = $this->M_payroll_config->getTaxMaxVal4(); /* Nominal Pajak IV */
            $pajakSebulan   = 0;

            $yearStart      = substr($row['contract_start'],0,4);
            $pembetulan     = 0;
            if($yearStart < 2020){
                $min_bln    = '01'; 
            }else{    
                $min_bln    = substr($row['contract_start'],5,2);
            }

            $max_bln        = substr($row['contract_end'],5,2);
            
            $jmlMasa = ($max_bln-$min_bln)+1;
            if($jmlMasa < 10){
                $jmlMasa = '0'.$jmlMasa;
            }
            
            $spt_no_id              = $this->db->query("SELECT IFNULL(MAX(spt_id)+1,'1') no_id FROM mst_spt_tax")->row()->no_id;            

            // $generate_spt           = 

            $bio_rec_id             = $row['bio_rec_id'];
            $nie                    = $row['nie'];
            $jmlMasa                = isset($jmlMasa);
            $year_period            = $row['year_period'];
            $monthPeriod            = $row['month_period'];
            $pembetulan             = isset($pembetulan);

            $spt_no_urut            = $this->db->query("SELECT IFNULL(LPAD(MAX(generate_no_spt)+1,7,'0'),'0000001') nomor_dok, no_bukti_potong FROM mst_spt_tax 
                                        WHERE tahun_pajak = '".$yearPeriod."'")->row()->nomor_dok;
            $nomorSpt               = "1 . 1 - " .$max_bln. " . " .$yearPeriod. " - ".$spt_no_urut; 
            $min_bln                = $min_bln;

            $npwpLength             = strlen($row['npwp_no']);
            $npwpNo                 = ($npwpLength<=5)? '0' : $row['npwp_no'];
            $id_card_no             = $row['id_card_no'];
            $full_name              = $row['full_name'];
            $address                = ($row['id_card_address']=='None') ? ' - ' : $row['id_card_address'];
            $gender                 = ($row['gender']=="L")? 'M' : 'F';
            $maritalStatus          = preg_replace('/\d/', '',  $row['marital_status']); 
            $stats                  = $row['stats'];
            $position               = $row['position'];
            $wpLuarNegri            = 'YA';
            $countryCode            = '';
            $codeObjPajak           = ($row['nationality'] == 'indonesia') ? '21-100-01' : '21-100-02';
            $total                  = $row['total'];
            
            $masa_kerja             = $max_bln;
            $tunjanganPPH           = '0';

            if($row['is_tax_allowance'] == '1'){
                $tunjanganPPH  = 0; /* tunjangan pph */

                for( $x = 1; $x < $masa_kerja; $x++){
                    $queryOfmonth = " SELECT bio_rec_id,year_period,SUM(bs_prorate) AS total,SUM(ot_1+ot_2+ot_3+ot_4) AS overtime, SUM(flying_camp+travel_bonus+attendance_bonus+ other_allowance1) AS other_bonus, SUM(thr+contract_bonus) AS Thr_Cbonus,jkk_jkm, SUM(emp_jp +emp_jht + emp_health_bpjs + health_bpjs) AS ass_employee,SUM(jp+jht) AS jp,ptkp_total FROM trn_slip_ptlsmb 
                    WHERE bio_rec_id = '".$bio_rec_id."' AND year_period = '".$yearPeriod."' AND month_period = '".$monthPeriod."' ";

                    $sqlOfMonth   = $this->db->query($queryOfmonth)->row_array();

                    /* Start Jumlah 4 Honorarium */
                    $jmlHonorarium  = 0;
                    /*End Jumlah 4 Honorarium */
                    $overtime = $row['overtime'] + $row['other_bonus'];
                    

                    /* Start Jumlah 8 Total Jumlah 1 s/d 7 */
                    $jmlHasilBruto    = $row['total'] + $tunjanganPPH + $overtime + $jmlHonorarium + $row['ass_employee'] +  $row['Thr_Cbonus'];
                    /* End Jumlah 8 Total Jumlah 1 s/d 7 */

                    /* Start Biaya Jabatan Max 500.000 */         
                    $biayaJabatan   = 0;
                    
                    if($biayaJabatan > $maxNonTax){
                      $biayaJabatan = $maxNonTax; 
                    }

                    $biayaJabatan   = $jmlHasilBruto * (5/100);

                    if($biayaJabatan > 6000000){
                        $biayaJabatan =  6000000;
                    }else{
                        $biayaJabatan = $jmlHasilBruto * (5/100);
                    }

                    /* Start Pembulatan $biayaJabatan Round 1000 */
                    $roundBiayaJabatan = $biayaJabatan;
                    $roundBiayaJabatan = floor($roundBiayaJabatan);

                    if(substr($roundBiayaJabatan,-3)<499){
                        $biayaJabatan = round($roundBiayaJabatan,-3);
                    }else{
                        $biayaJabatan = round($roundBiayaJabatan,-3)-1000;
                    }
                     
                    /* End Pembulatan $biayaJabatan Round 1000 */

                    /* End Biaya Jabatan Max 500.0000 Jumlah 9 */

                    /* Start Jumlah 11 */
                    $jmlPengurangan   = $biayaJabatan + $row['jp'];
                    /* End Jumlah 11 */

                    /* Start Jumlah 12 */
                    $jmlPhslNetto     = $jmlHasilBruto - $jmlPengurangan;
                    /* End Jumlah 12 */

                    /*Start Jumlah 13 */
                    $jmlNettoSblmnya  = 0;
                    /*End Jumlah 13 */

                    /* Start Jumlah 15 */
                    $totalPtkp  = $row['ptkp_total'];
                    /* End Jumlah 15 */

                    /* Start Jumlah 16 */
                    $pkp_total = 0;
                    if($jmlPhslNetto >= $totalPtkp){
                        $pkp_total = $jmlPhslNetto - $totalPtkp;
                    }else{
                        $pkp_total = 0;
                    }
                    test($jmlHasilBruto.' - '.$jmlPengurangan,1);
                    $roundPkpTotal = $pkp_total;
                    $roundPkpTotal = floor($roundPkpTotal);

                    if(substr($roundPkpTotal,-3)<499){
                        $pkp_total = round($roundPkpTotal,-3);
                    }else{
                        $pkp_total = round($roundPkpTotal,-3)-1000;
                    }
                    // $pkp_total     = floor($pkp_total); 
                    /* End Jumlah 16 */
                    
                   
                    /* Start Tax terutang setahun */
                    $taxVal1 = 0;    
                    $taxVal2 = 0;    
                    $taxVal3 = 0;    
                    $taxVal4 = 0;

                    $tVal    = 0;
                    $tSisa   = 0;
                    if($pkp_total > 0)
                    {
                        /* TAX 1 */
                        if($maxTaxVal1 > 0)
                        {
                            $tVal = $pkp_total/$maxTaxVal1;  
                            if($tVal >= 1){
                               $taxVal1 = $maxTaxVal1 * ($taxPercent1/100);

                            }
                            else{
                               $taxVal1 = $pkp_total * ($taxPercent1/100);
                            }    
                        }    
                                
                        /* TAX 2 */
                        if($maxTaxVal2 > 0)
                        {
                            if($pkp_total > $maxTaxVal1)
                            {
                                $tSisa = $pkp_total - $maxTaxVal1; 
                                $tVal = $tSisa/$maxTaxVal2;
                                    if($tVal >= 1){
                                       $taxVal2 = $maxTaxVal2 * ($taxPercent2/100);
                                    }
                                    else{
                                       $taxVal2 = $tSisa * ($taxPercent2/100);
                                    }
                            }     
                        }
                             
                        /* TAX 3 */
                        if($maxTaxVal3 > 0)
                        {
                            if($pkp_total > ($maxTaxVal1 + $maxTaxVal2))
                            {
                                $tSisa = $pkp_total - $maxTaxVal1 - $maxTaxVal2;
                                $tVal = $tSisa/$maxTaxVal3;
                                    if($tVal >= 1){
                                       $taxVal3 = $maxTaxVal3 * ($taxPercent3/100);
                                    }
                                    else{
                                       $taxVal3 = $tSisa * ($taxPercent3/100);
                                    }
                            }    
                        }
                             
                        /* TAX 4 */
                        if($pkp_total > ($maxTaxVal1 + $maxTaxVal2 + $maxTaxVal3))
                        {
                            $tSisa = $pkp_total - $maxTaxVal1 - $maxTaxVal2 - $maxTaxVal3;
                            $taxVal4 = $tSisa * ($taxPercent4/100); 
                        }
                    }

                    $taxTerutangSetahun = ($taxVal1 + $taxVal2 + $taxVal3 + $taxVal4)*12/$jmlMasa;

                    $pajakSebulan  = number_format(($taxTerutangSetahun),2,".","");  /*Pajak terutang setahun / 12 = hasil pajak sebulan*/
                    // test($pajakSebulan,1);

                    if($tunjanganPPH == $pajakSebulan)
                    {
                        break;
                    }

                    $tunjanganPPH       = $pajakSebulan;
                }
            }

            $overtime               = $row['overtime'] + $row['other_bonus'];
            $jmlHonorarium          = 0;
            $premiAsuransi          = 0;
            $ass_employee           = $row['ass_employee'];
            $Thr_Cbonus             = $row['Thr_Cbonus'];
            $jmlHasilBruto          = $row['total'] + $overtime + $jmlHonorarium + $row['ass_employee']  + $row['Thr_Cbonus']; 

            /* Start Biaya Jabatan Max 500.000 */         
            $biayaJabatan   = 0;
                    
            if($biayaJabatan > $maxNonTax){
               $biayaJabatan = $maxNonTax; 
            }
            $biayaJabatan   = $jmlHasilBruto * (5/100);

            if($biayaJabatan > 6000000){
                $biayaJabatan =  6000000;
            }else{
                $biayaJabatan = ($jmlHasilBruto * (5/100));
            }

            /* Start Pembulatan $biayaJabatan Round 1000 */
            $roundBiayaJabatan = $biayaJabatan;
            $roundBiayaJabatan = floor($roundBiayaJabatan);

            if(substr($roundBiayaJabatan,-3)<499){
                $biayaJabatan = round($roundBiayaJabatan,-3);
            }else{
                $biayaJabatan = round($roundBiayaJabatan,-3)-1000;
            }

            $jp                     = $row['jp'];
            $jmlPengurangan         = $biayaJabatan + $row['jp'];
            $jmlPhslNetto           = $jmlHasilBruto - $jmlPengurangan;
            $jmlNettoSblmnya        = 0;
            $totalPtkp              = $row['ptkp_total'];
            
            $pkp_total              = 0;
            if($jmlPhslNetto >= $totalPtkp){
                $pkp_total          = $jmlPhslNetto - $totalPtkp;
            }else{
                $pkp_total          = 0;
            }

            $statusPindah     = 'Pegawai Baru';

            if($ptName = 'Pontil_Sumbawa'){
                $npwpPemotong = '092261544434000';
                $namaPemotong = 'Heri Susanto';
            }

            $tglBuktiPotong = date('d/M/y');

            $nie = preg_replace('/[\r\n]+/','', $nie);
            $nie = trim($nie);
            $clientName             = $ptName;
            $year                   = $yearPeriod;

            if($nie != '')
            {
                $this->M_mst_pajak->resetValues();                    
                $this->M_mst_pajak->setSptId($this->security->xss_clean($this->M_mst_pajak->GenerateNumber()));   
                $salaryRow = $this->M_salary->loadBioIdByNie($nie);
                
                $bioId = $salaryRow['bio_rec_id'];      

                $this->M_mst_pajak->deleteByIdPeriod($bioId, $nie, $clientName,$year); //delete bio id, nie, clientname pd saat upload agr tidak double data

                // if($bioId == "" || $bioId == null){
                //     $noBiodataId++;
                //     /* GET ERROR DATA */
                //     $failedNames[$arrIdx] = $rowData[0][9];
                //     $arrIdx++;
                // }else{
                    $this->M_mst_pajak->setBioRecId($this->security->xss_clean($bioId));
                // }     

                $this->M_mst_pajak->setBadgeNo($nie);
                $this->M_mst_pajak->setClientName($this->security->xss_clean($clientName));

                    //masa pajak
                    $tData = $jmlMasa; 
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_mst_pajak->setMasaPajak($this->security->xss_clean("0"));   
                    }else{
                        $this->M_mst_pajak->setMasaPajak($this->security->xss_clean($tData));
                    }

                    //tahun pajak
                    $tData = $year_period;
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_mst_pajak->setTahunPajak($this->security->xss_clean("0"));   
                    }else{
                        $this->M_mst_pajak->setTahunPajak($this->security->xss_clean($tData));
                    }

                    //pembetulan
                    $tData = $pembetulan; 
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_mst_pajak->setPembetulan($this->security->xss_clean("0"));   
                    }else{
                        $this->M_mst_pajak->setPembetulan($this->security->xss_clean($tData));
                    }

                    //no bukti potong
                    $tData = $nomorSpt; 
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_mst_pajak->setNoBuktiPotong($this->security->xss_clean("0"));   
                    }else{
                        $this->M_mst_pajak->setNoBuktiPotong($this->security->xss_clean($tData));
                    }

                    $tData = $spt_no_urut; 
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_mst_pajak->setGenerateNoSpt($this->security->xss_clean("0"));   
                    }else{
                        $this->M_mst_pajak->setGenerateNoSpt($this->security->xss_clean($tData));
                    }

                    // masa awal
                    $tData = $min_bln; 
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_mst_pajak->setMasaAwal($this->security->xss_clean("0"));   
                    }else{
                        $this->M_mst_pajak->setMasaAwal($this->security->xss_clean($tData));
                    }

                    // masa akhir
                    $tData = $max_bln; 
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_mst_pajak->setMasaAkhir($this->security->xss_clean("0"));   
                    }else{
                        $this->M_mst_pajak->setMasaAkhir($this->security->xss_clean($tData));
                    }                    

                    // no npwp
                    $tData = $npwpNo; 
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_mst_pajak->setNpwpNo($this->security->xss_clean("0"));   
                    }else{
                        $this->M_mst_pajak->setNpwpNo($this->security->xss_clean($tData));
                    }

                    // nik
                    $tData = $id_card_no; 
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_mst_pajak->setNik($this->security->xss_clean("0"));   
                    }else{
                        $this->M_mst_pajak->setNik($this->security->xss_clean($tData));
                    }  

                    // full name
                    $tData = $full_name; 
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_mst_pajak->setFullName($this->security->xss_clean("0"));   
                    }else{
                        $this->M_mst_pajak->setFullName($this->security->xss_clean($tData));
                    }

                    // address
                    $tData = $address; 
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_mst_pajak->setAddress($this->security->xss_clean("0"));   
                    }else{
                        $this->M_mst_pajak->setAddress($this->security->xss_clean($tData));
                    }

                    // gender
                    $tData = $gender; 
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_mst_pajak->setGender($this->security->xss_clean("0"));   
                    }else{
                        $this->M_mst_pajak->setGender($this->security->xss_clean($tData));
                    }

                    // status ptkp
                    $tData = $maritalStatus; 
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_mst_pajak->setStatusPtkp($this->security->xss_clean("0"));   
                    }else{
                        $this->M_mst_pajak->setStatusPtkp($this->security->xss_clean($tData));
                    }

                    // jumlah tanggungan
                    $tData = $stats; 
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_mst_pajak->setJumlahTanggungan($this->security->xss_clean("0"));   
                    }else{
                        $this->M_mst_pajak->setJumlahTanggungan($this->security->xss_clean($tData));
                    }

                    // position
                    $tData = $position; 
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_mst_pajak->setPosition($this->security->xss_clean("0"));   
                    }else{
                        $this->M_mst_pajak->setPosition($this->security->xss_clean($tData));
                    }

                    // wp luar negri
                    $tData = $wpLuarNegri; 
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_mst_pajak->setWpLuarNegri($this->security->xss_clean("0"));   
                    }else{
                        $this->M_mst_pajak->setWpLuarNegri($this->security->xss_clean($tData));
                    }

                    // country code
                    $tData = $countryCode;
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_mst_pajak->setCountryCode($this->security->xss_clean("0"));   
                    }else{
                        $this->M_mst_pajak->setCountryCode($this->security->xss_clean($tData));
                    }

                    // tax code
                    $tData = $codeObjPajak; 
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_mst_pajak->setTaxCode($this->security->xss_clean("0"));   
                    }else{
                        $this->M_mst_pajak->setTaxCode($this->security->xss_clean($tData));
                    }

                    // total 1
                    $tData = $total; 
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_mst_pajak->setTotal1($this->security->xss_clean("0"));   
                    }else{
                        $this->M_mst_pajak->setTotal1($this->security->xss_clean($tData));
                    }

                    // total 2
                    $tData = $tunjanganPPH; 
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_mst_pajak->setTotal2($this->security->xss_clean("0"));   
                    }else{
                        $this->M_mst_pajak->setTotal2($this->security->xss_clean($tData));
                    }

                    // total 3
                    $tData = $overtime; 
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_mst_pajak->setTotal3($this->security->xss_clean("0"));   
                    }else{
                        $this->M_mst_pajak->setTotal3($this->security->xss_clean($tData));
                    }

                    // total 4
                    $tData = $jmlHonorarium; 
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_mst_pajak->setTotal4($this->security->xss_clean("0"));   
                    }else{
                        $this->M_mst_pajak->setTotal4($this->security->xss_clean($tData));
                    }

                    // total 5
                    $tData = $premiAsuransi; 
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_mst_pajak->setTotal5($this->security->xss_clean("0"));   
                    }else{
                        $this->M_mst_pajak->setTotal5($this->security->xss_clean($tData));
                    }

                    // total 6
                    $tData = $row['ass_employee']; 
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_mst_pajak->setTotal6($this->security->xss_clean("0"));   
                    }else{
                        $this->M_mst_pajak->setTotal6($this->security->xss_clean($tData));
                    }

                    // total 7
                    $tData = $row['Thr_Cbonus']; 
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_mst_pajak->setTotal7($this->security->xss_clean("0"));   
                    }else{
                        $this->M_mst_pajak->setTotal7($this->security->xss_clean($tData));
                    }

                    // total 8
                    $tData = $jmlHasilBruto; 
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_mst_pajak->setTotal8($this->security->xss_clean("0"));   
                    }else{
                        $this->M_mst_pajak->setTotal8($this->security->xss_clean($tData));
                    }

                    // total 9
                    $tData = $biayaJabatan;
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_mst_pajak->setTotal9($this->security->xss_clean("0"));   
                    }else{
                        $this->M_mst_pajak->setTotal9($this->security->xss_clean($tData));
                    }

                    // total 10
                    $tData = $row['jp']; 
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_mst_pajak->setTotal10($this->security->xss_clean("0"));   
                    }else{
                        $this->M_mst_pajak->setTotal10($this->security->xss_clean($tData));
                    }

                    // Total 11
                    $tData = $jmlPengurangan; 
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_mst_pajak->setTotal11($this->security->xss_clean("0"));   
                    }else{
                        $this->M_mst_pajak->setTotal11($this->security->xss_clean($tData));
                    }

                    // Total 12
                    $tData = $jmlPhslNetto; 
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_mst_pajak->setTotal12($this->security->xss_clean("0"));   
                    }else{
                        $this->M_mst_pajak->setTotal12($this->security->xss_clean($tData));
                    }

                    // Total 13
                    $tData = $jmlNettoSblmnya; 
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_mst_pajak->setTotal13($this->security->xss_clean("0"));   
                    }else{
                        $this->M_mst_pajak->setTotal13($this->security->xss_clean($tData));
                    }

                    // Total 14
                    $tData = $jmlPhslNetto; 
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_mst_pajak->setTotal14($this->security->xss_clean("0"));   
                    }else{
                        $this->M_mst_pajak->setTotal14($this->security->xss_clean($tData));
                    }

                    // Total 15
                    $tData = $totalPtkp; 
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_mst_pajak->setTotal15($this->security->xss_clean("0"));   
                    }else{
                        $this->M_mst_pajak->setTotal15($this->security->xss_clean($tData));
                    }

                    // Total 16
                    $tData = $pkp_total; 
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_mst_pajak->setTotal16($this->security->xss_clean("0"));   
                    }else{
                        $this->M_mst_pajak->setTotal16($this->security->xss_clean($tData));
                    }

                    //Total 17
                    $tData =$pajakSebulan; 
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_mst_pajak->setTotal17($this->security->xss_clean("0"));   
                    }else{
                        $this->M_mst_pajak->setTotal17($this->security->xss_clean($tData));
                    }

                    // Total 18
                    $tData = 0; 
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_mst_pajak->setTotal18($this->security->xss_clean("0"));   
                    }else{
                        $this->M_mst_pajak->setTotal18($this->security->xss_clean($tData));
                    }

                    // Total 19
                    $tData =$pajakSebulan; 
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_mst_pajak->setTotal19($this->security->xss_clean("0"));   
                    }else{
                        $this->M_mst_pajak->setTotal19($this->security->xss_clean($tData));
                    }

                    // Total 20
                    $tData = $pajakSebulan; 
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_mst_pajak->setTotal20($this->security->xss_clean("0"));   
                    }else{
                        $this->M_mst_pajak->setTotal20($this->security->xss_clean($tData));
                    }

                    // status pindah
                    $tData = $statusPindah; 
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_mst_pajak->setStatusPindah($this->security->xss_clean("0"));   
                    }else{
                        $this->M_mst_pajak->setStatusPindah($this->security->xss_clean($tData));
                    }

                    // npwp pemotong
                    $tData = $npwpPemotong; 
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_mst_pajak->setNpwpPemotong($this->security->xss_clean("0"));   
                    }else{
                        $this->M_mst_pajak->setNpwpPemotong($this->security->xss_clean($tData));
                    }

                    // nama pemotong
                    $tData = $namaPemotong; 
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_mst_pajak->setNamaPemotong($this->security->xss_clean("0"));   
                    }else{
                        $this->M_mst_pajak->setNamaPemotong($this->security->xss_clean($tData));
                    }

                    // tanggal bukti pemotong
                    $tData = $tglBuktiPotong; 
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_mst_pajak->setTanggalBuktiPemotong($this->security->xss_clean("0"));   
                    }else{
                        $this->M_mst_pajak->setTanggalBuktiPemotong($this->security->xss_clean($tData));
                    }

                    
                    // test($tData,1);

                $this->M_mst_pajak->setPicProcess($this->security->xss_clean($this->session->userdata('hris_user_id')));
                $myCurrentDate = GetCurrentDate();
                $curDateTime = $myCurrentDate['CurrentDateTime'];

                $this->M_mst_pajak->setProcessTime($this->security->xss_clean($curDateTime));
                $this->M_mst_pajak->insert();
                // echo $year.'- '.$month.' '.$dataCount; exit(0);
                // $dataCount++; 
                // test($dataCount,1)
            }
            
            // $spreadsheet->getActiveSheet()
            //     ->setCellValue('AA'.($rowIdx), $jmlHasilBruto) /*Jumlah 8 */
            //     ->setCellValue('AB'.($rowIdx), round($biayaJabatan)) /* Jumlah 9 */
            //     ->setCellValue('AC'.($rowIdx), $row['jp']) /* Jumlah 10 */
            //     ->setCellValue('AD'.($rowIdx), $jmlPengurangan) /* Jumlah 11 */
            //     ->setCellValue('AE'.($rowIdx), $jmlPhslNetto) /* Jumlah 12 */
            //     ->setCellValue('AF'.($rowIdx), $jmlNettoSblmnya) /* Jumlah 13 */
            //     ->setCellValue('AG'.($rowIdx), $jmlPhslNetto) /* Jumlah 14 */
            //     ->setCellValue('AH'.($rowIdx), $totalPtkp) /* Jumlah 15 */
            //     ->setCellValue('AI'.($rowIdx), round($pkp_total)) /* Jumlah 16 */
            //     ->setCellValue('AJ'.($rowIdx), $pajakSebulan) /* Jumlah 17 */
            //     ->setCellValue('AK'.($rowIdx), 0) /* Jumlah 18 */
            //     ->setCellValue('AL'.($rowIdx), $pajakSebulan) /* Jumlah 19 */
            //     ->setCellValue('AM'.($rowIdx), $pajakSebulan) /* Jumlah 20 */
            //     ->setCellValue('AN'.($rowIdx), $statusPindah)
            //     ->setCellValue('AO'.($rowIdx), $npwpPemotong)
            //     ->setCellValue('AP'.($rowIdx), $namaPemotong)
            //     ->setCellValue('AQ'.($rowIdx), $tglBuktiPotong)
            //     ;

        }        


    }

    public function printPtlSmb($ptName, $yearPeriod)
    {
        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();

        $spreadsheet->getProperties()->setCreator('Maurice - Web - Android')
            ->setLastModifiedBy('Maurice - Web - Android')
            ->setTitle('Office 2007 XLSX Test Document')
            ->setSubject('Office 2007 XLSX Test Document')
            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Test result file');

        $boldFont = [
            'font' => [
                'bold' => true
                // 'color' => ['argb' => '0000FF'],
            ],
        ];

        $totalStyle = [
            'font' => [
                'bold' => true,
                'color' => ['argb' => '0000FF'],
            ],
        ];

        $allBorderStyle = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $outlineBorderStyle = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $topBorderStyle = [
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $bottomBorderStyle = [
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        // select month(getDate())

        $strSQL  = '';
        if ($ptName == 'Pontil_Sumbawa'){
            $strSQL  = "SELECT c.bio_rec_id,b.nie,a.full_name,c.month_period,
                        (SELECT MIN(d.contract_start) AS contract_start FROM mst_contract d WHERE YEAR(d.contract_end) = '".$yearPeriod."' AND d.bio_rec_id=
                        c.bio_rec_id) AS contract_start,
                        (SELECT MAX(d.contract_end) AS contract_end FROM mst_contract d WHERE YEAR(d.contract_end) = '".$yearPeriod."' AND d.bio_rec_id=
                        c.bio_rec_id) AS contract_end,
                        c.year_period,
                        SUM(c.bs_prorate) AS total,
                        a.nationality,a.gender,a.position,a.marital_status,a.id_card_no,
                        a.id_card_address,a.current_address,a.npwp_no,
                        SUM(c.ot_1+c.ot_2+c.ot_3+c.ot_4) AS overtime,
                        SUM(c.flying_camp + c.travel_bonus + c.attendance_bonus + c.other_allowance1) AS other_bonus,
                        SUM(c.thr + c.contract_bonus) AS Thr_Cbonus,
                        c.jkk_jkm,
                        SUM(c.emp_jp + c.emp_jht + c.emp_health_bpjs + c.health_bpjs) AS ass_employee,
                        SUM(c.jp + c.jht) AS jp,
                        c.ptkp_total, b.is_tax_allowance,right(a.marital_status,1) stats ";
            $strSQL .= "FROM trn_slip_ptlsmb c ";
            $strSQL .= "JOIN mst_bio_rec a ON c.bio_rec_id = a.bio_rec_id ";
            $strSQL .= "LEFT JOIN mst_salary b ON c.bio_rec_id = b.bio_rec_id ";
            $strSQL .= "WHERE c.client_name = '".$ptName."' ";
            $strSQL .= "AND a.is_active = '1' ";
            $strSQL .= "AND c.year_period = '".$yearPeriod."' ";
            $strSQL .= "GROUP BY c.bio_rec_id ";
            $strSQL .= "ORDER BY a.full_name ";
        }       
        // test($strSQL,1);
        $query = $this->db->query($strSQL)->result_array();
        // $rowData = $query = $this->db->get()->row_array();

        // Add some data
        $spreadsheet->getActiveSheet()->getStyle('A6:AQ7')
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B'); 

        $spreadsheet->getActiveSheet()
            ->setCellValue('A1', 'PT. SANGATI SOERYA SEJAHTERA')
            ->setCellValue('A2', 'Spt Tax - '.$ptName)
            ->setCellValue('A3', 'Period : '.$yearPeriod);

        $spreadsheet->getActiveSheet()->getStyle("A1:D1")->getFont()->setBold(true)->setSize(15);
        $spreadsheet->getActiveSheet()->getStyle("A2:D2")->getFont()->setBold(true)->setSize(14)->setUnderline(true);
        $spreadsheet->getActiveSheet()->getStyle("A3:D3")->getFont()->setBold(true)->setSize(14)->setUnderline(true);
        $spreadsheet->getActiveSheet()->getStyle("A4:G4")->getFont()->setBold(true)->setSize(12);
        $spreadsheet->getActiveSheet()->getStyle("A6:AQ7")->getFont()->setBold(true)->setSize(12);

        $spreadsheet->getActiveSheet()->mergeCells("A6:A7");
        $spreadsheet->getActiveSheet()->mergeCells("B6:B7");
        $spreadsheet->getActiveSheet()->mergeCells("C6:C7");
        $spreadsheet->getActiveSheet()->mergeCells("D6:D7");
        $spreadsheet->getActiveSheet()->mergeCells("E6:E7");
        $spreadsheet->getActiveSheet()->mergeCells("F6:F7");
        $spreadsheet->getActiveSheet()->mergeCells("G6:G7");
        $spreadsheet->getActiveSheet()->mergeCells("H6:H7");
        $spreadsheet->getActiveSheet()->mergeCells("I6:I7");
        $spreadsheet->getActiveSheet()->mergeCells("J6:J7");
        $spreadsheet->getActiveSheet()->mergeCells("K6:K7");
        $spreadsheet->getActiveSheet()->mergeCells("L6:L7");
        $spreadsheet->getActiveSheet()->mergeCells("M6:M7");
        $spreadsheet->getActiveSheet()->mergeCells("N6:N7");
        $spreadsheet->getActiveSheet()->mergeCells("O6:O7");
        $spreadsheet->getActiveSheet()->mergeCells("P6:P7");
        $spreadsheet->getActiveSheet()->mergeCells("Q6:Q7");
        $spreadsheet->getActiveSheet()->mergeCells("R6:R7");
        $spreadsheet->getActiveSheet()->mergeCells("S6:S7");
        $spreadsheet->getActiveSheet()->mergeCells("T6:T7");
        $spreadsheet->getActiveSheet()->mergeCells("U6:U7");
        $spreadsheet->getActiveSheet()->mergeCells("V6:V7");
        $spreadsheet->getActiveSheet()->mergeCells("W6:W7");
        $spreadsheet->getActiveSheet()->mergeCells("X6:X7");
        $spreadsheet->getActiveSheet()->mergeCells("Y6:Y7");
        $spreadsheet->getActiveSheet()->mergeCells("Z6:Z7");
        $spreadsheet->getActiveSheet()->mergeCells("AA6:AA7");
        $spreadsheet->getActiveSheet()->mergeCells("AB6:AB7");
        $spreadsheet->getActiveSheet()->mergeCells("AC6:AC7");
        $spreadsheet->getActiveSheet()->mergeCells("AD6:AD7");
        $spreadsheet->getActiveSheet()->mergeCells("AE6:AE7");
        $spreadsheet->getActiveSheet()->mergeCells("AF6:AF7");
        $spreadsheet->getActiveSheet()->mergeCells("AG6:AG7");
        $spreadsheet->getActiveSheet()->mergeCells("AH6:AH7");
        $spreadsheet->getActiveSheet()->mergeCells("AI6:AI7");
        $spreadsheet->getActiveSheet()->mergeCells("AJ6:AJ7");
        $spreadsheet->getActiveSheet()->mergeCells("AK6:AK7");
        $spreadsheet->getActiveSheet()->mergeCells("AL6:AL7");
        $spreadsheet->getActiveSheet()->mergeCells("AM6:AM7");
        $spreadsheet->getActiveSheet()->mergeCells("AN6:AN7");
        $spreadsheet->getActiveSheet()->mergeCells("AO6:AO7");
        $spreadsheet->getActiveSheet()->mergeCells("AP6:AP7");
        $spreadsheet->getActiveSheet()->mergeCells("AQ6:AQ7");

            
        $spreadsheet->getActiveSheet()->getStyle("A6:AQ7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("A6:AQ7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()
            ->setCellValue('A6', 'BIODATA ID')
            ->setCellValue('B6', 'NIE')
            ->setCellValue('C6', 'MASA PAJAK')
            ->setCellValue('D6', 'TAHUN PAJAK')
            ->setCellValue('E6', 'PEMBETULAN')
            ->setCellValue('F6', 'NOMOR BUKTI POTONG')
            ->setCellValue('G6', 'MASA PEROLEHAN AWAL')
            ->setCellValue('H6', 'MASA PEROLEHAN AKHIR')
            ->setCellValue('I6', 'NPWP')
            ->setCellValue('J6', 'NIK')
            ->setCellValue('K6', 'NAMA')
            ->setCellValue('L6', 'ALAMAT')
            ->setCellValue('M6', 'GENDER')
            ->setCellValue('N6', 'PTKP')
            ->setCellValue('O6', 'JUMLAH TANGGUNGAN')
            ->setCellValue('P6', 'JABATAN')
            ->setCellValue('Q6', 'WP LUAR NEGRI')
            ->setCellValue('R6', 'KODE NEGARA')
            ->setCellValue('S6', 'KODE PAJAK')
            ->setCellValue('T6', 'UPAH ')
            ->setCellValue('U6', 'TUNJANGAN PPH')
            ->setCellValue('V6', 'TUNJANGAN LEMBUR')
            ->setCellValue('W6', 'HONORARIUM')
            ->setCellValue('X6', 'PREMI ASURANSI')
            ->setCellValue('Y6', 'NATURA')
            ->setCellValue('Z6', 'THR, BONUS')
            ->setCellValue('AA6', 'JUMLAH BRUTO (1 SD 7)')
            ->setCellValue('AB6', 'BIAYA JABATAN')
            ->setCellValue('AC6', 'IURAN PENSIUN')
            ->setCellValue('AD6', 'JUMLAH PENGURANGAN (9 SD 10)')
            ->setCellValue('AE6', 'JUMLAH NETO (8-11)')
            ->setCellValue('AF6', 'NETO MASA SEBELUMNYA')
            ->setCellValue('AG6', 'NETO (SETAHUN/DISETAHUNKAN)')
            ->setCellValue('AH6', 'PTKP')
            ->setCellValue('AI6', 'PKP SETAHUN')
            ->setCellValue('AJ6', 'PPH 21 ATAS PKP SETAHUN/DISETAHUNKAN')
            ->setCellValue('AK6', 'PPH 21 YANG TELAH DIPOTONG MASA SEBELUMNYA')
            ->setCellValue('AL6', 'PPH 21 TERUTANG')
            ->setCellValue('AM6', 'PPH 21 & PPH 26 YANG TELAH DIPOTONG/DILUNASI')
            ->setCellValue('AN6', 'STATUS PINDAH')
            ->setCellValue('AO6', 'NPWP PEMOTONG')
            ->setCellValue('AP6', 'NAMA PEMOTONG')
            ->setCellValue('AQ6', 'TANGGAL BUKTI POTONG')
            ;
            
            /* Start Non Tax Allowance/ Biaya Jabatan 500.0000 */
            $myConfigId = 1;  
            $row = $this->M_payroll_config->getObjectById($myConfigId);  
            $maxNonTax = $this->M_payroll_config->getNonTaxAllowance();
            /* End Non Tax Allowance/ Biaya Jabatan 500.0000 */

            /* Start Progresif  */
            $taxPercent1 = $this->M_payroll_config->getTaxPercent1(); /* Persentase Progresif Pajak I */
            $taxPercent2 = $this->M_payroll_config->getTaxPercent2(); /* Persentase Progresif Pajak II */
            $taxPercent3 = $this->M_payroll_config->getTaxPercent3(); /* Persentase Progresif Pajak III */
            $taxPercent4 = $this->M_payroll_config->getTaxPercent4(); /* Persentase Progresif Pajak IV */

            $maxTaxVal1 = $this->M_payroll_config->getTaxMaxVal1(); /* Nominal Pajak I */ 
            $maxTaxVal2 = $this->M_payroll_config->getTaxMaxVal2(); /* Nominal Pajak II */
            $maxTaxVal3 = $this->M_payroll_config->getTaxMaxVal3(); /* Nominal Pajak III */
            $maxTaxVal4 = $this->M_payroll_config->getTaxMaxVal4(); /* Nominal Pajak IV */
            /* End Progresif  */
            
        /* START GET DAYS TOTAL BY ROSTER */
        $rowIdx = 7;
        // $rowNo = 0;
        foreach ($query as $row) {   
            $tunjanganPPH       = 0;
            $pajakSebulan       = 0;
            /*Start Tanggal Bukti Potong */
            $tglBuktiPotong = date('d/M/y');

            /* Start Query Masa Kontrak Kerja */
            $dQuery  = 'SELECT bio_rec_id, employee_name, MIN(contract_start) min_contract, MAX(contract_end) max_contract ';
            $dQuery .= 'FROM db_recruitment.mst_contract ';
            $dQuery .= 'WHERE bio_rec_id = "'.$row['bio_rec_id'].'" ';
            $dQuery .= 'GROUP BY bio_rec_id ';
            $masaKrja = $this->db->query($dQuery)->row_array();
            /* End Query Masa Kontrak Kerja */

            $min_bln_contract       = (int)substr($masaKrja['min_contract'],5,2);
            $max_bln_contract       = (int)substr($masaKrja['max_contract'],5,2);
            // test($min_contract.' '.$max_contract,1);


            // test($masa,0);
            $nomor      = 0;
            for ($i=$min_bln_contract; $i <= $max_bln_contract; $i++) { 
                if($i<10){
                    $month_periode         = '0'.$i;
                }else{
                    $month_periode         = $i;
                }
                
                // Query untuk gaji per bulan //
                $sql_gaji           = 'SELECT bio_rec_id,name,year_period,month_period,
                                       bs_prorate
                                       FROM db_recruitment.trn_slip_ptlsmb
                                       WHERE bio_rec_id = "'.$row['bio_rec_id'].'"
                                       AND year_period = "2020"
                                       AND month_period = "'.$month_periode.'"
                                       ORDER BY bio_rec_id' ;
                // test($sql_gaji,1);
                $query_gaji         = $this->db->query($sql_gaji)->num_rows();

                /* Start Mengambil Nilai Bulan Dari Masa Kerja Paling Muda s/d Terbaru */
                $min_bln_contract       = substr($masaKrja['min_contract'],5,2);
                $max_bln_contract       = substr($masaKrja['max_contract'],5,2);
                // test($min_bln_contract.' '.$max_bln_contract,1);
                $jmlMasaKerja           = ($max_bln_contract-$min_bln_contract)+1;            
                /* End Mengambil Nilai Bulan Dari Masa Kerja Paling Muda s/d Terbaru */

                if($query_gaji==1){
                    $nomor      = $nomor+1;
                }else{
                    
                    $mt_id      = '';
                    $bio_rec_id = $row['bio_rec_id'];
                    $name       = $row['full_name'];

                    $query     = " INSERT INTO db_recruitment.trn_manual_tax (mt_id, bio_rec_id, year_period, month_period, name,client_name) VALUES ('".$mt_id."','".$bio_rec_id."','".$yearPeriod."','".$month_periode."','".$name."','".$ptName."' )" ;
                    // test($query,0);
                    $this->db->query($query);
                }

            }
        
            if($nomor==$max_bln_contract){

                // query summary untuk gaji
                // $sql_gaji           = 'SELECT bio_rec_id,`name`,year_period,month_period,
                //                            bs_prorate
                //                        FROM trn_slip_ptlsmb
                //                        WHERE bio_rec_id = "'.$row['bio_rec_id'].'"
                //                        AND year_period = "'.$row['year_period'].'"
                //                        AND month_period BETWEEN "'.$min_bln.'" AND "'.$max_bln.'"
                //                        ORDER BY bio_rec_id' ;
                // // test($sql_gaji,1);
                // $query_gaji         = $this->db->query($sql_gaji)->row_array();
            
            }

             /* Start Pembetulan */
            $pembetulan = 0;
            /* End Pembetulan */ 

            /* Start Kode Pajak */
            if($nationality = 'indonesia'){
                $codeObjPajak = '21-100-01';
            }else{
                $codeObjPajak = '21-100-02';
            }
            /* End Kode Pajak */
            
            /* Start Wp Luar Negri */  
            $wpLuarNegri = 'YA';
            /* End Wp Luar Negri */

            /* Start Ambil Nilai Bulan Min & Max 2 Digit */
            $yearStart = substr($row['contract_start'],0,4);

            if($yearStart < 2020){
                $min_bln = '01'; 
            }else{    
                $min_bln   = substr($row['contract_start'],5,2);
            }

            $max_bln       = substr($row['contract_end'],5,2);
            
            /* Start Jumlah Masa Pajak */ 
            $jmlMasa = ($max_bln-$min_bln)+1;
            
            if($jmlMasa < 10){
                $jmlMasa = '0'.$jmlMasa;
            }
            
            /* End Jumlah Masa Pajak */   

            /* Start Jumlah 4 Honorarium */
            $jmlHonorarium  = 0;
            /*End Jumlah 4 Honorarium */

            $overtime = $row['overtime'] + $row['other_bonus'];
            $premiAsuransi = 0;

            /* Start Jumlah 8 Total Jumlah 1 s/d 7 */
            $jmlHasilBruto    = $row['total'] + $overtime + $jmlHonorarium + $row['ass_employee']  + $row['Thr_Cbonus']; 
            /* End Jumlah 8 Total Jumlah 1 s/d 7 */

            /* Start Biaya Jabatan Max 500.000 */         
            $biayaJabatan   = 0;
                    
            if($biayaJabatan > $maxNonTax){
               $biayaJabatan = $maxNonTax; 
            }

            $biayaJabatan   = $jmlHasilBruto * (5/100);

            if($biayaJabatan > 6000000){
                $biayaJabatan =  6000000;
            }else{
                $biayaJabatan = ($jmlHasilBruto * (5/100));
            }

            /* Start Pembulatan $biayaJabatan Round 1000 */
            $roundBiayaJabatan = $biayaJabatan;
            $roundBiayaJabatan = floor($roundBiayaJabatan);

            if(substr($roundBiayaJabatan,-3)<499){
                $biayaJabatan = round($roundBiayaJabatan,-3);
            }else{
                $biayaJabatan = round($roundBiayaJabatan,-3)-1000;
            }
             
            /* End Pembulatan $biayaJabatan Round 1000 */

            /* End Biaya Jabatan Max 500.0000 Jumlah 9 */

            /* Start Jumlah 11 */
            $jmlPengurangan   = $biayaJabatan + $row['jp'];
            /* End Jumlah 11 */

            /* Start Jumlah 12 */
            $jmlPhslNetto     = $jmlHasilBruto - $jmlPengurangan;
            /* End Jumlah 12 */

            /*Start Jumlah 13 */
            $jmlNettoSblmnya  = 0;
            /*End Jumlah 13 */

            /* Start Jumlah 15 */
            $totalPtkp  = $row['ptkp_total'];
            /* End Jumlah 15 */

            /* Start Jumlah 16 Pkp Total */
            $pkp_total = 0;
            if($jmlPhslNetto >= $totalPtkp){
               $pkp_total = $jmlPhslNetto - $totalPtkp;
            }else{
               $pkp_total = 0;
            }

            /* Start Pembulatan $pkp_total Round 1000 */
            $roundPkpTotal = $pkp_total;
            $roundPkpTotal = floor($roundPkpTotal);

            if(substr($roundPkpTotal,-3)<499){
                $pkp_total = round($roundPkpTotal,-3);
            }else{
                $pkp_total = round($roundPkpTotal,-3)-1000;
            }
             
            /* End Pembulatan $pkp_total Round 1000 */

            /* End Jumlah 16 Pkp Total */

            /* Start Tunjangan PPH */
            $masa_kerja  = $max_bln;
            $biodataId   = $row['bio_rec_id'];
            $monthPeriod = $row['month_period'];

            $iuranJabatan = $jmlHasilBruto * (5/100);
            if($iuranJabatan > $maxNonTax){
                    $iuranJabatan = $maxNonTax; 
            }      
            $iuranJabatan = $iuranJabatan;
            // $queryIsTax     = " SELECT * FROM mst_salary WHERE is_tax_allowance = '1' ";
            // $sqlIsTax       = $this->db->query($queryIsTax)->row_array();
            // test($sqlIsTax,1);
            if($row['is_tax_allowance'] == '1'){

                $tunjanganPPH  = 0; /* tunjangan pph */

                for( $x = 1; $x < $masa_kerja; $x++){
                    $queryOfmonth = " SELECT bio_rec_id,year_period,SUM(bs_prorate) AS total,SUM(ot_1+ot_2+ot_3+ot_4) AS overtime, SUM(flying_camp+travel_bonus+attendance_bonus+ other_allowance1) AS other_bonus, SUM(thr+contract_bonus) AS Thr_Cbonus,jkk_jkm, SUM(emp_jp +emp_jht + emp_health_bpjs + health_bpjs) AS ass_employee,SUM(jp+jht) AS jp,ptkp_total FROM trn_slip_ptlsmb WHERE bio_rec_id = '".$biodataId."' AND year_period = '".$yearPeriod."' AND month_period = '".$monthPeriod."' ";

                    $sqlOfMonth   = $this->db->query($queryOfmonth)->row_array();

                    /* Start Jumlah 4 Honorarium */
                    $jmlHonorarium  = 0;
                    /*End Jumlah 4 Honorarium */
                    $overtime = $row['overtime'] + $row['other_bonus'];
                    

                    /* Start Jumlah 8 Total Jumlah 1 s/d 7 */
                    $jmlHasilBruto    = $row['total'] + $tunjanganPPH + $overtime + $jmlHonorarium + $row['ass_employee'] +  $row['Thr_Cbonus'];
                    /* End Jumlah 8 Total Jumlah 1 s/d 7 */

                    /* Start Biaya Jabatan Max 500.000 */         
                    $biayaJabatan   = 0;
                    
                    if($biayaJabatan > $maxNonTax){
                      $biayaJabatan = $maxNonTax; 
                    }

                    $biayaJabatan   = $jmlHasilBruto * (5/100);

                    if($biayaJabatan > 6000000){
                        $biayaJabatan =  6000000;
                    }else{
                        $biayaJabatan = $jmlHasilBruto * (5/100);
                    }

                    /* Start Pembulatan $biayaJabatan Round 1000 */
                    $roundBiayaJabatan = $biayaJabatan;
                    $roundBiayaJabatan = floor($roundBiayaJabatan);

                    if(substr($roundBiayaJabatan,-3)<499){
                        $biayaJabatan = round($roundBiayaJabatan,-3);
                    }else{
                        $biayaJabatan = round($roundBiayaJabatan,-3)-1000;
                    }
                     
                    /* End Pembulatan $biayaJabatan Round 1000 */

                    /* End Biaya Jabatan Max 500.0000 Jumlah 9 */

                    /* Start Jumlah 11 */
                    $jmlPengurangan   = $biayaJabatan + $row['jp'];
                    /* End Jumlah 11 */

                    /* Start Jumlah 12 */
                    $jmlPhslNetto     = $jmlHasilBruto - $jmlPengurangan;
                    /* End Jumlah 12 */

                    /*Start Jumlah 13 */
                    $jmlNettoSblmnya  = 0;
                    /*End Jumlah 13 */

                    /* Start Jumlah 15 */
                    $totalPtkp  = $row['ptkp_total'];
                    /* End Jumlah 15 */

                    /* Start Jumlah 16 */
                    $pkp_total = 0;
                    if($jmlPhslNetto >= $totalPtkp){
                        $pkp_total = $jmlPhslNetto - $totalPtkp;
                    }else{
                        $pkp_total = 0;
                    }

                    $roundPkpTotal = $pkp_total;
                    $roundPkpTotal = floor($roundPkpTotal);

                    if(substr($roundPkpTotal,-3)<499){
                        $pkp_total = round($roundPkpTotal,-3);
                    }else{
                        $pkp_total = round($roundPkpTotal,-3)-1000;
                    }
                    // $pkp_total     = floor($pkp_total); 
                    /* End Jumlah 16 */
                    
                   
                    /* Start Tax terutang setahun */
                    $taxVal1 = 0;    
                    $taxVal2 = 0;    
                    $taxVal3 = 0;    
                    $taxVal4 = 0;

                    $tVal    = 0;
                    $tSisa   = 0;
                    if($pkp_total > 0)
                    {
                        /* TAX 1 */
                        if($maxTaxVal1 > 0)
                        {
                            $tVal = $pkp_total/$maxTaxVal1;  
                            if($tVal >= 1){
                               $taxVal1 = $maxTaxVal1 * ($taxPercent1/100);

                            }
                            else{
                               $taxVal1 = $pkp_total * ($taxPercent1/100);
                            }    
                        }    
                                
                        /* TAX 2 */
                        if($maxTaxVal2 > 0)
                        {
                            if($pkp_total > $maxTaxVal1)
                            {
                                $tSisa = $pkp_total - $maxTaxVal1; 
                                $tVal = $tSisa/$maxTaxVal2;
                                    if($tVal >= 1){
                                       $taxVal2 = $maxTaxVal2 * ($taxPercent2/100);
                                    }
                                    else{
                                       $taxVal2 = $tSisa * ($taxPercent2/100);
                                    }
                            }     
                        }
                             
                        /* TAX 3 */
                        if($maxTaxVal3 > 0)
                        {
                            if($pkp_total > ($maxTaxVal1 + $maxTaxVal2))
                            {
                                $tSisa = $pkp_total - $maxTaxVal1 - $maxTaxVal2;
                                $tVal = $tSisa/$maxTaxVal3;
                                    if($tVal >= 1){
                                       $taxVal3 = $maxTaxVal3 * ($taxPercent3/100);
                                    }
                                    else{
                                       $taxVal3 = $tSisa * ($taxPercent3/100);
                                    }
                            }    
                        }
                             
                        /* TAX 4 */
                        if($pkp_total > ($maxTaxVal1 + $maxTaxVal2 + $maxTaxVal3))
                        {
                            $tSisa = $pkp_total - $maxTaxVal1 - $maxTaxVal2 - $maxTaxVal3;
                            $taxVal4 = $tSisa * ($taxPercent4/100); 
                        }
                    }

                    $taxTerutangSetahun = ($taxVal1 + $taxVal2 + $taxVal3 + $taxVal4)*12/$jmlMasa;

                    $pajakSebulan  = number_format(($taxTerutangSetahun),2,".","");  /*Pajak terutang setahun / 12 = hasil pajak sebulan*/
                    // test($pajakSebulan,1);

                    if($tunjanganPPH == $pajakSebulan)
                    {
                        break;
                    }

                    $tunjanganPPH       = $pajakSebulan;
                }           
            } 

            /* End Tunjangan PPH */

            /* Start NPWP */
            $npwpNo = $row['npwp_no'];
            $npwp_   = preg_replace('/\D/', '', $npwpNo); //menghilangkan karakter symbol

            if($npwp_ == NULL){ // jika npwp NONE dibuat menjadi 0 
                $npwp_ = '0';
            }

            /* End NPWP */

            /* Start Gender */
            $gender = $row['gender'];

            if($gender = 'L'){
               $gender = 'M';
            }else{
               $gender = 'F';
            }
            /* End Gender */

            /* Start Kode Pajak Default */
            $codeObjPajak = '21-100-01';
            /* End Kode Pajak Default */

            /* Start Marital Status tanpa jumlah tanggungan */
            $status_ptkp = $row['marital_status'];
            $maritalStatus = preg_replace('/\d/', '', $status_ptkp); 

            /* End Marital Status tanpa jumlah tanggungan */ 

            /*Start Kode Negara */
            $countryCode = NULL;
            /* End Kode Negara */

            /* Start alamat */
            $address = $row['id_card_address'];
            if($address == 'None'){
                $address = '-';
            }else if($address != 'None'){
                $address = $row['id_card_address'];
            }
            /* End alamat */

            /* Start Nomor */
            $nomorSpt = "1 . 1 - " .$max_bln. " . " .$yearPeriod. " - 0000000"; 

            /* Start Status Pindah */
            $statusPindah     = 'Pegawai Baru';
            /* End Status Pindah */

            /*Start Npwp Pemotong & Nama Pemotong */
            if($ptName = 'Pontil_Sumbawa'){
                $npwpPemotong = '092261544434000';
                $namaPemotong = 'Heri Susanto';
            }
            /*End Npwp Pemotong & Nama Pemotong */

            $rowIdx++;
            // test($pembetulan.' '.$tunjanganPPH,1);
            // $rowNo++;
            $spreadsheet->getActiveSheet()
                ->setCellValue('A'.($rowIdx), $row['bio_rec_id'])
                ->setCellValue('B'.($rowIdx), $row['nie'])
                ->setCellValue('C'.($rowIdx), isset($jmlMasa))
                ->setCellValue('D'.($rowIdx), $row['year_period'])
                ->setCellValue('E'.($rowIdx), isset($pembetulan))
                ->setCellValue('F'.($rowIdx), $nomorSpt)
                ->setCellValue('G'.($rowIdx), $min_bln)
                ->setCellValue('H'.($rowIdx), $max_bln)
                ->setCellValue('I'.($rowIdx), $npwp_)
                ->setCellValue('J'.($rowIdx), $row['id_card_no'])
                ->setCellValue('K'.($rowIdx), $row['full_name'])
                ->setCellValue('L'.($rowIdx), $address)
                ->setCellValue('M'.($rowIdx), $gender)
                ->setCellValue('N'.($rowIdx), $maritalStatus)
                ->setCellValue('O'.($rowIdx), $row['stats'])
                ->setCellValue('P'.($rowIdx), $row['position'])
                ->setCellValue('Q'.($rowIdx), $wpLuarNegri)
                ->setCellValue('R'.($rowIdx), $countryCode)
                ->setCellValue('S'.($rowIdx), $codeObjPajak)
                ->setCellValue('T'.($rowIdx), $row['total']) /* Jumlah 1 */
                ->setCellValue('U'.($rowIdx), isset($tunjanganPPH))/* Jumlah 2*/
                ->setCellValue('V'.($rowIdx), $overtime)/*Jumlah 3*/
                ->setCellValue('W'.($rowIdx), $jmlHonorarium)/*Jumlah 4*/
                ->setCellValue('X'.($rowIdx), $premiAsuransi) /* Jumlah 5 */
                ->setCellValue('Y'.($rowIdx), $row['ass_employee']) /* Jumlah 6 */
                ->setCellValue('Z'.($rowIdx), $row['Thr_Cbonus']) /* Jumlah 7 */
                ->setCellValue('AA'.($rowIdx), $jmlHasilBruto) /*Jumlah 8 */
                ->setCellValue('AB'.($rowIdx), round($biayaJabatan)) /* Jumlah 9 */
                ->setCellValue('AC'.($rowIdx), $row['jp']) /* Jumlah 10 */
                ->setCellValue('AD'.($rowIdx), $jmlPengurangan) /* Jumlah 11 */
                ->setCellValue('AE'.($rowIdx), $jmlPhslNetto) /* Jumlah 12 */
                ->setCellValue('AF'.($rowIdx), $jmlNettoSblmnya) /* Jumlah 13 */
                ->setCellValue('AG'.($rowIdx), $jmlPhslNetto) /* Jumlah 14 */
                ->setCellValue('AH'.($rowIdx), $totalPtkp) /* Jumlah 15 */
                ->setCellValue('AI'.($rowIdx), round($pkp_total)) /* Jumlah 16 */
                ->setCellValue('AJ'.($rowIdx), $pajakSebulan) /* Jumlah 17 */
                ->setCellValue('AK'.($rowIdx), 0) /* Jumlah 18 */
                ->setCellValue('AL'.($rowIdx), $pajakSebulan) /* Jumlah 19 */
                ->setCellValue('AM'.($rowIdx), $pajakSebulan) /* Jumlah 20 */
                ->setCellValue('AN'.($rowIdx), $statusPindah)
                ->setCellValue('AO'.($rowIdx), $npwpPemotong)
                ->setCellValue('AP'.($rowIdx), $namaPemotong)
                ->setCellValue('AQ'.($rowIdx), $tglBuktiPotong)
                ;

            /* SET ROW COLOR */
                if($rowIdx % 2 == 1)
                {
                    $spreadsheet->getActiveSheet()->getStyle('A'.$rowIdx.':AQ'.$rowIdx)
                    ->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB('EAEBAF');   

                    // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(7, ($rowIdx+2), 'TOTAL');
                    // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(8, ($rowIdx+2), '=SUM(H8:H'.($rowIdx+2).')');
                    // $spreadsheet->getActiveSheet()->getStyle('T7:AM'.($rowIdx+2))->getNumberFormat()->setFormatCode('#,##0');

                    // $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":H".($rowIdx+2))
                    // ->getFill()
                    // ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    // ->getStartColor()
                    // ->setRGB('F2BE6B');             
                } 
                
            // }

        }

        // test('1',1);
           
        $spreadsheet->getActiveSheet()->getStyle("A6:AQ".($rowIdx))->applyFromArray($allBorderStyle);
        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('Report Excel '.date('d-m-Y H'));

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        // $str = 'PTLSmbInvoice';
        // $fileName = 'Employee Active List PT.'.$ptName.'';
        
        $str = 'SPTPTLSMB'.$yearPeriod.'';
        $fileName = preg_replace('/\s+/', '', $str);
        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.Xlsx"');
        // header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        /* BY COMPOSER */
        // $writer = new Xlsx($spreadsheet);
        /* OFFLINE/ BY COPY EXCEL FOLDER  */
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit(0);   
    }    


     
}

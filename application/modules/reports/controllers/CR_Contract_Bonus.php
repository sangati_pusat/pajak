<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    // Load library phpspreadsheet
    require('./vendor/autoload.php');
    use PhpOffice\PhpSpreadsheet\Helper\Sample;
    use PhpOffice\PhpSpreadsheet\IOFactory;
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    // End load library phpspreadsheet

class CR_Contract_Bonus extends CI_Controller {

    public function __construct(){
        parent::__construct();
    }

    public function getContractBonus(){
        // test('oke',1);
        $pt = "";
        $year = "";
        $month = "";
        // $payrollGroup = "";
        if(isset($_POST['pt']))
        {
            $pt = $_POST['pt'];
        }
        if(isset($_POST['year']))
        {
            $year = $_POST['year'];
        }
        if(isset($_POST['month']))
        {
            $month = $_POST['month'];
        }


        $ym = (string)$year.'-'.$month;

        
            $strSQL  = "SELECT mb.full_name, ms.nie, ma.bank_code, ms.salary_level, ms.basic_salary, mc.contract_start, mc.contract_end, (DATEDIFF (mc.contract_end, mc.contract_start)) AS Adendum, ms.bank_name, ms.account_name, ms.account_no ";
            $strSQL .= "FROM db_recruitment.mst_bio_rec mb, db_recruitment.mst_bank ma, db_recruitment.mst_salary ms, db_recruitment.mst_contract mc ";
            $strSQL .= "WHERE mb.bio_rec_id = ms.bio_rec_id ";
            $strSQL .= "AND mb.bio_rec_id = mc.bio_rec_id ";
            $strSQL .= "AND ms.bank_name = ma.bank_name ";
            $strSQL .= "AND mc.client_name = '".$pt."' ";
            $strSQL .= "AND DATE_FORMAT(mc.contract_end,'%Y-%m')= '".$ym."' ";
            // $strSQL .= "AND mc.is_active = 1 ";       
            $strSQL .= "ORDER BY mc.contract_end ASC ";       
            // echo $strSQL;
            // exit();
            $query = $this->db->query($strSQL)->result_array();
        // test($query,1);

        $myData = array();
        $rowNo = 0;
        foreach ($query as $key => $row) {
            $rowNo++;
            $myData[] = array(
                $rowNo,       
                $row['full_name'],         
                $row['nie'],         
                $row['salary_level'],         
                $row['basic_salary'],         
                $row['contract_start'],         
                $row['contract_end'],         
                $row['Adendum'],         
                $row['bank_code'],         
                $row['bank_name'],         
                $row['account_name'],         
                $row['account_no']     
            );            
        }  
        echo json_encode($myData);    
    }

    public function exportContractBonus($ptName, $yearPeriod, $monthPeriod)
    {
        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();

        $spreadsheet->getProperties()->setCreator('Maurice - Web - Android')
            ->setLastModifiedBy('Maurice - Web - Android')
            ->setTitle('Office 2007 XLSX Test Document')
            ->setSubject('Office 2007 XLSX Test Document')
            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Test result file');

        $boldFont = [
            'font' => [
                'bold' => true
                // 'color' => ['argb' => '0000FF'],
            ],
        ];

        $totalStyle = [
            'font' => [
                'bold' => true,
                'color' => ['argb' => '0000FF'],
            ],
        ];

        $allBorderStyle = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $outlineBorderStyle = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $topBorderStyle = [
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $bottomBorderStyle = [
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $ym = (string)$yearPeriod.'-'.$monthPeriod;

        $strSQL  = "SELECT mb.full_name, ms.nie, ma.bank_code, ms.salary_level, ms.basic_salary, mc.contract_start, mc.contract_end, (DATEDIFF (mc.contract_end, mc.contract_start)) AS Adendum, ms.bank_name, ms.account_name, ms.account_no ";
            $strSQL .= "FROM db_recruitment.mst_bio_rec mb, db_recruitment.mst_bank ma, db_recruitment.mst_salary ms, db_recruitment.mst_contract mc ";
            $strSQL .= "WHERE mb.bio_rec_id = ms.bio_rec_id ";
            $strSQL .= "AND mb.bio_rec_id = mc.bio_rec_id ";
            $strSQL .= "AND ms.bank_name = ma.bank_name ";
            $strSQL .= "AND mc.client_name = '".$ptName."' ";
            $strSQL .= "AND DATE_FORMAT(mc.contract_end,'%Y-%m')= '".$ym."' ";
        // $strSQL .= "AND mc.is_active = 1 ";       
        $strSQL .= "ORDER BY mc.contract_end ASC ";
        $query = $this->db->query($strSQL)->result_array();   

        // Add some data
        $spreadsheet->getActiveSheet()->getStyle('A6:L7')
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B'); 

        $spreadsheet->getActiveSheet()
            ->setCellValue('A1', 'PT. SANGATI SOERYA SEJAHTERA')
            ->setCellValue('A2', 'LIST CONTRACT BONUS PT.'.$ptName.' ')
            ->setCellValue('A4', 'Periode : '.$monthPeriod.'-'.$yearPeriod);

        $spreadsheet->getActiveSheet()->getStyle("A1:D1")->getFont()->setBold(true)->setSize(16);
        $spreadsheet->getActiveSheet()->getStyle("A2:D2")->getFont()->setBold(true)->setSize(14)->setUnderline(true);
        $spreadsheet->getActiveSheet()->getStyle("A4:G4")->getFont()->setBold(true)->setSize(12);
        $spreadsheet->getActiveSheet()->getStyle("A6:L7")->getFont()->setBold(true)->setSize(12);

        $spreadsheet->getActiveSheet()->mergeCells("A6:A7");
        $spreadsheet->getActiveSheet()->mergeCells("B6:B7");
        $spreadsheet->getActiveSheet()->mergeCells("C6:C7");
        $spreadsheet->getActiveSheet()->mergeCells("D6:D7");
        $spreadsheet->getActiveSheet()->mergeCells("E6:E7");
        $spreadsheet->getActiveSheet()->mergeCells("F6:F7");
        $spreadsheet->getActiveSheet()->mergeCells("G6:G7");
        $spreadsheet->getActiveSheet()->mergeCells("H6:H7");
        $spreadsheet->getActiveSheet()->mergeCells("I6:I7");
        $spreadsheet->getActiveSheet()->mergeCells("J6:J7");
        $spreadsheet->getActiveSheet()->mergeCells("K6:K7");
        $spreadsheet->getActiveSheet()->mergeCells("L6:L7");
            
        $spreadsheet->getActiveSheet()->getStyle("A6:L7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("A6:L7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()
            ->setCellValue('A6', 'NO')
            ->setCellValue('B6', 'NAME')
            ->setCellValue('C6', 'ID NUMBER')
            ->setCellValue('D6', 'GRADE / LEVEL')
            ->setCellValue('E6', 'BASIC')
            ->setCellValue('F6', 'START CONTRACT')
            ->setCellValue('G6', 'END CONTRACT')
            ->setCellValue('H6', 'CONTRACT PERIOD')
            ->setCellValue('I6', 'BANK CODE')
            ->setCellValue('J6', 'BANK ACCOUNT')
            ->setCellValue('K6', 'ACCOUNT ON BEHALF OF')
            ->setCellValue('L6', 'ACCOUNT NUMBER');

        /* START GET DAYS TOTAL BY ROSTER */
        $rowIdx = 7;
        $rowNo = 0;
        foreach ($query as $row) {                      
            $rowIdx++;
            $rowNo++;
            $spreadsheet->getActiveSheet()
                ->setCellValue('A'.($rowIdx), $rowNo)
                ->setCellValue('B'.($rowIdx), $row['full_name'])
                ->setCellValue('C'.($rowIdx), $row['nie'])
                ->setCellValue('D'.($rowIdx), $row['salary_level'])
                ->setCellValue('E'.($rowIdx), $row['basic_salary'])
                ->setCellValue('F'.($rowIdx), $row['contract_start'])
                ->setCellValue('G'.($rowIdx), $row['contract_end'])
                ->setCellValue('H'.($rowIdx), $row['Adendum'])
                ->setCellValue('I'.($rowIdx), $row['bank_code'])
                ->setCellValue('J'.($rowIdx), $row['bank_name'])
                ->setCellValue('K'.($rowIdx), $row['account_name'])
                ->setCellValue('L'.($rowIdx), $row['account_no'])
                ;

            /* SET ROW COLOR */
            if($rowIdx % 2 == 1)
            {
                $spreadsheet->getActiveSheet()->getStyle('A'.$rowIdx.':L'.$rowIdx)
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('EAEBAF');             
            } 
        }

        $spreadsheet->getActiveSheet()->getStyle("A6:L".($rowIdx))->applyFromArray($allBorderStyle);
        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('Report Excel '.date('d-m-Y H'));

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        // $str = 'PTLSmbInvoice';
        $str = 'Contract Bonus PT.'.$ptName.'';
        $fileName = preg_replace('/\s+/', '', $str);
        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.Xlsx"');
        // header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        /* BY COMPOSER */
        // $writer = new Xlsx($spreadsheet);
        /* OFFLINE/ BY COPY EXCEL FOLDER  */
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit(0);   
    }    
}

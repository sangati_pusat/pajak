<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    // Load library phpspreadsheet
    require('./vendor/autoload.php');
    use PhpOffice\PhpSpreadsheet\Helper\Sample;
    use PhpOffice\PhpSpreadsheet\IOFactory;
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    // End load library phpspreadsheet

class CR_History_Update extends CI_Controller {

    public function __construct(){
        parent::__construct();
    }

    public function getHistoryBiodata(){

        $stQuery  = 'SELECT a.bio_rec_id,a.full_name,a.gender,a.place_of_birth,a.date_of_birth,a.local_foreign,a.nationality,a.clan,a.id_card_no,a.npwp_no,a.bpjs_no,
                    a.id_card_address,a.city_address,a.residence_status,a.religion,a.driving_license,a.marital_status,a.position,a.height,a.weight,a.blood_type,
                    a.email_address,a.telp_no,a.cell_no,a.cv_date,a.edit_time
                    FROM mst_bio_rec a WHERE a.pic_edit="'.$this->input->post('user').'" AND a.edit_time BETWEEN "'.$this->input->post('startDate').' 00:00:00"  
                    AND "'.$this->input->post('endDate').' 00:00:00" ';

        $data = $this->db->query($stQuery)->result_array();

        $myDataBiodata = array();
        foreach ($data as $row) {
            $myDataBiodata[] = array(
                $row['bio_rec_id'], 
                $row['full_name'], 
                $row['gender'], 
                // $row['place_of_birth'],    
                // $row['date_of_birth'],    
                $row['local_foreign'],    
                // $row['nationality'],    
                // $row['clan'],    
                $row['id_card_no'],    
                $row['npwp_no'],    
                $row['bpjs_no'],    
                $row['id_card_address'],    
                $row['city_address'],    
                // $row['residence_status'],    
                $row['religion'],    
                // $row['driving_license'],    
                $row['marital_status'],    
                $row['position'],    
                // $row['height'],    
                // $row['weight'],    
                // $row['blood_type'],    
                // $row['email_address'],    
                // $row['telp_no'],    
                $row['cell_no'],    
                $row['cv_date'],    
                $row['edit_time']
            );            
        }    

        $stQuery2 = 'SELECT a.salary_id,a.bio_rec_id,b.full_name,a.nie,a.company_name,a.salary_level,a.basic_salary,a.bank_id,a.bank_code,a.bank_name,a.account_name,
                    a.account_no,a.payroll_group,a.pic_edit_account,a.account_edit_time,a.pic_edit,a.edit_time FROM mst_salary a 
                    LEFT JOIN mst_bio_rec b ON a.bio_rec_id=b.bio_rec_id
                    WHERE (a.pic_edit_account="'.$this->input->post('user').'" OR a.pic_edit="'.$this->input->post('user').'") 
                    AND ((a.account_edit_time BETWEEN "'.$this->input->post('startDate').' 00:00:00" AND "'.$this->input->post('endDate').' 00:00:00") 
                    OR (a.edit_time BETWEEN "'.$this->input->post('startDate').' 00:00:00" AND "'.$this->input->post('endDate').' 00:00:00")) ';  

        $data2 = $this->db->query($stQuery2)->result_array();

        $myDataSalary = array();
        foreach ($data2 as $row) {
            $myDataSalary[] = array(
                $row['salary_id'], 
                $row['bio_rec_id'], 
                $row['full_name'], 
                $row['nie'],        
                $row['company_name'],        
                $row['salary_level'],        
                $row['basic_salary'],        
                // $row['bank_id'],        
                // $row['bank_code'],        
                $row['bank_name'],        
                $row['account_name'],        
                $row['account_no'],        
                $row['payroll_group'],        
                $row['pic_edit_account'],        
                $row['account_edit_time'],        
                $row['pic_edit'],        
                $row['edit_time']      
            );            
        } 

        echo jsout(array('biodata' => json_encode($myDataBiodata), 'salary' => json_encode($myDataSalary))); 
    }

    public function contractDetailDisplay(){
        $biodataId = '';
        $clientName = '';
        if(isset($_POST['biodataId']))
        {
            $biodataId = $_POST['biodataId'];
        }
        if(isset($_POST['clientName']))
        {
            $clientName = $_POST['clientName'];
        }

        $stQuery  = 'SELECT bio_rec_id, employee_name, client_name, contract_start, contract_end, contract_counter ';
        $stQuery .= 'FROM mst_contract ';
        $stQuery .= 'WHERE bio_rec_id = "'.$biodataId.'" ';
        $stQuery .= 'AND client_name = "'.$clientName.'" ';
        $stQuery .= 'AND is_close = 0 ';
        $stQuery .= 'ORDER BY employee_name, contract_counter ';  
        $data = $this->db->query($stQuery)->result_array();
        $myData = array();
        foreach ($data as $row) {
            $myData[] = array(
                $row['contract_counter'], 
                $row['contract_start'], 
                $row['contract_end']        
            );            
        }        
        echo json_encode($myData); 
    }

    public function exportContractHistory($clientName)
    {
        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();

        $boldFont = [
            'font' => [
                'bold' => true
                // 'color' => ['argb' => '0000FF'],
            ],
        ];

        $totalStyle = [
            'font' => [
                'bold' => true,
                'color' => ['argb' => '0000FF'],
            ],
        ];

        $allBorderStyle = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $outlineBorderStyle = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $topBorderStyle = [
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $bottomBorderStyle = [
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $stQuery  = 'SELECT bio_rec_id, employee_name, client_name, COUNT(bio_rec_id) contract_count ';
        $stQuery .= 'FROM mst_contract ';
        $stQuery .= 'WHERE client_name = "'.$clientName.'" ';
        $stQuery .= 'AND is_close = 0 ';
        $stQuery .= 'GROUP BY bio_rec_id ';
        // $stQuery .= 'HAVING COUNT(bio_rec_id) >= 9 ';
        $stQuery .= 'ORDER BY employee_name, contract_counter ';  
        $query = $this->db->query($stQuery)->result_array();  
        // test($clientName,1);
        // Add some data
        $spreadsheet->getActiveSheet()->getStyle('A6:G7')
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B'); 

        $spreadsheet->getActiveSheet()
            ->setCellValue('A1', 'PT. SANGATI SOERYA SEJAHTERA')
            ->setCellValue('A2', 'HISTORY OF CONTRACT PT '.$clientName.'');
            // ->setCellValue('A4', 'Periode : '.$monthPeriod.'-'.$yearPeriod);

        $spreadsheet->getActiveSheet()->getStyle("A1:D1")->getFont()->setBold(true)->setSize(16);
        $spreadsheet->getActiveSheet()->getStyle("A2:D2")->getFont()->setBold(true)->setSize(14)->setUnderline(true);
        // $spreadsheet->getActiveSheet()->getStyle("A4:G4")->getFont()->setBold(true)->setSize(12);
        $spreadsheet->getActiveSheet()->getStyle("A6:G7")->getFont()->setBold(true)->setSize(12);

        $spreadsheet->getActiveSheet()->mergeCells("A6:A7");
        $spreadsheet->getActiveSheet()->mergeCells("B6:B7");
        $spreadsheet->getActiveSheet()->mergeCells("C6:C7");
        $spreadsheet->getActiveSheet()->mergeCells("D6:D7");
        $spreadsheet->getActiveSheet()->mergeCells("E6:E7");
        $spreadsheet->getActiveSheet()->mergeCells("F6:F7");
        $spreadsheet->getActiveSheet()->mergeCells("G6:G7");
            
        $spreadsheet->getActiveSheet()->getStyle("A6:G7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("A6:G7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()
            ->setCellValue('A6', 'EMPLOYEE ID')
            ->setCellValue('B6', 'NAME')
            ->setCellValue('C6', 'CLIENT')
            ->setCellValue('D6', 'CONTRACT COUNT')
            ->setCellValue('E6', 'SEQENCES')
            ->setCellValue('F6', 'CONTRACT START')
            ->setCellValue('G6', 'CONTRACT END');

        /* START GET DAYS TOTAL BY ROSTER */
        $rowIdx = 7;
        $rowNo = 0;
        foreach ($query as $row) {                      
            $bioId          = $row['bio_rec_id'];
            $employeeName   = $row['employee_name'];
            $clientName     = $row['client_name'];
            $contractCount  = $row['contract_count'];
            
            $rowIdx++;          
            $spreadsheet->getActiveSheet()
            ->setCellValue('A'.($rowIdx), $bioId)
            ->setCellValue('B'.($rowIdx), $employeeName)
            ->setCellValue('C'.($rowIdx), $clientName)
            ->setCellValue('D'.($rowIdx), $contractCount);
            $spreadsheet->getActiveSheet()->getStyle('A'.$rowIdx.':'.'G'.$rowIdx)->getFont()->setBold(true)->getColor()->setRGB('001188');;
            $spreadsheet->getActiveSheet()->getStyle('A'.$rowIdx.':G'.$rowIdx)
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('FCF7B6');    

            $stQuery  = 'SELECT bio_rec_id, employee_name, client_name, contract_start, contract_end, contract_counter ';
            $stQuery .= 'FROM mst_contract ';
            $stQuery .= 'WHERE bio_rec_id = "'.$bioId.'" ';
            $stQuery .= 'AND client_name = "'.$clientName.'" ';
            $stQuery .= 'AND is_close = 0 ';
            $stQuery .= 'ORDER BY employee_name, contract_counter ';  
            $detailData = $this->db->query($stQuery)->result_array();
            foreach ($detailData as $dataRow) {
                $rowIdx++;
                $spreadsheet->getActiveSheet()
                ->setCellValue('E'.($rowIdx), $dataRow['contract_counter'])
                ->setCellValue('F'.($rowIdx), $dataRow['contract_start'])
                ->setCellValue('G'.($rowIdx), $dataRow['contract_end']);

            }

            /* SET ROW COLOR */
            // if($rowIdx % 2 == 1)
            // {
            //     $spreadsheet->getActiveSheet()->getStyle('A'.$rowIdx.':G'.$rowIdx)
            //     ->getFill()
            //     ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            //     ->getStartColor()
            //     ->setRGB('EAEBAF');             
            // } 
        }

        $spreadsheet->getActiveSheet()->getStyle("A6:G".($rowIdx))->applyFromArray($allBorderStyle);
        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('Report Excel '.date('d-m-Y H'));

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        // $str = 'PTLSmbInvoice';
        $str = 'Contract History PT '.$clientName.'';
        $fileName = preg_replace('/\s+/', '', $str);
        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.Xlsx"');
        // header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        /* BY COMPOSER */
        // $writer = new Xlsx($spreadsheet);
        /* OFFLINE/ BY COPY EXCEL FOLDER  */
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit(0);   
    }    
}

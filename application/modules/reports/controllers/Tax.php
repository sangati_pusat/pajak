<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

    // Load library phpspreadsheet
    require('./vendor/autoload.php');
    use PhpOffice\PhpSpreadsheet\Helper\Sample;
    use PhpOffice\PhpSpreadsheet\IOFactory;
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    // End load library phpspreadsheet

class Tax extends CI_Controller
{
	public function __construct(){
        parent::__construct();
    }

    public function getTaxCalculationView()
    {
        $pt     = $this->input->post('pt');
        $year   = $this->input->post('year');
        $dept   = $this->input->post('dept');
        $payrollGroup   = $this->input->post('payrollGroup');

        if($pt=='Redpath_Timika'){
        $query  = "SELECT a.salary_slip_id,a.client_name,a.dept,a.position,a.bio_rec_id,a.name,b.nie,c.npwp_no,a.marital_status,a.month_period,a.year_period,
                    a.bs_prorate,(ot_1+ot_2+ot_3+ot_4)overtime,a.jkk_jkm jamsostek,a.emp_health_bpjs bpjs_kes,a.travel_bonus,a.allowance_economy,a.incentive_bonus,
                    a.shift_bonus,a.unpaid_total
                    FROM trn_salary_slip a
                    LEFT JOIN mst_salary b ON a.bio_rec_id=b.bio_rec_id
                    LEFT JOIN mst_bio_rec c ON a.bio_rec_id=c.bio_rec_id
                    WHERE a.client_name='".$pt."' AND a.year_period='".$year."' AND a.dept ='".$dept."'";

        }
        else if($pt=='Pontil_Timika'){
        $query  = "SELECT a.salary_slip_id,a.client_name,a.dept,a.position,a.bio_rec_id,a.name,b.nie,c.npwp_no,a.marital_status,a.month_period,a.year_period,
                    a.bs_prorate,(ot_1+ot_2+ot_3+ot_4)overtime,a.jkk_jkm jamsostek,a.emp_health_bpjs bpjs_kes,a.travel_bonus,a.allowance_economy,a.incentive_bonus,
                    a.shift_bonus,a.unpaid_total
                    FROM trn_salary_slip a
                    LEFT JOIN mst_salary b ON a.bio_rec_id=b.bio_rec_id
                    LEFT JOIN mst_bio_rec c ON a.bio_rec_id=c.bio_rec_id
                    WHERE a.client_name='".$pt."' AND a.year_period='".$year."' AND a.payroll_group ='".$payrollGroup."'";

        }
        else if($pt=='Pontil_Sumbawa'){
            // test('sip',1);
        $query  = "SELECT a.salary_slip_id,a.client_name,a.dept,a.position,a.bio_rec_id,a.name,b.nie,c.npwp_no,a.marital_status,a.month_period,a.year_period,
                    a.bs_prorate,(ot_1+ot_2+ot_3+ot_4)overtime,a.jkk_jkm jamsostek,a.emp_health_bpjs bpjs_kes,a.travel_bonus,a.incentive_bonus,
                    a.shift_bonus,a.unpaid_total
                    FROM trn_slip_ptlsmb a
                    LEFT JOIN mst_salary b ON a.bio_rec_id=b.bio_rec_id
                    LEFT JOIN mst_bio_rec c ON a.bio_rec_id=c.bio_rec_id
                    WHERE a.client_name='".$pt."' AND a.year_period='".$year."'";

        }
        else if($pt=='Trakindo_Sumbawa'){
            // test('sip',1);
        $query  = "SELECT a.salary_slip_id,a.client_name,a.dept,a.position,a.bio_rec_id,a.name,b.nie,c.npwp_no,a.marital_status,a.month_period,a.year_period,
                    a.bs_prorate,(ot_1+ot_2+ot_3+ot_4)overtime,a.jkk_jkm jamsostek,a.emp_health_bpjs bpjs_kes,a.travel_bonus,a.incentive_bonus,
                    a.shift_bonus,a.unpaid_total
                    FROM trn_slip_trksmb a
                    LEFT JOIN mst_salary b ON a.bio_rec_id=b.bio_rec_id
                    LEFT JOIN mst_bio_rec c ON a.bio_rec_id=c.bio_rec_id
                    WHERE a.client_name='".$pt."' AND a.year_period='".$year."'";

        }

        $query = $this->db->query($query)->result_array();
        $myData = array();
        foreach ($query as $key => $row) {
            $myData[] = array(
                $row['salary_slip_id'],         
                $row['client_name'],         
                $row['name'],             
                $row['dept'],         
                $row['position']
            );            
        }  
        echo json_encode($myData);  
    }

    function namabulan($tanggal){
        // $tgl=substr($tanggal,8,2);
        // $bln=substr($tanggal,5,2);
        // $thn=substr($tanggal,0,4);
        // $info=date('w', mktime(0,0,0,$bln,$tgl,$thn));
        switch($tanggal){
            case '1': return "Jan"; break;
            case '2': return "Feb"; break;
            case '3': return "Mar"; break;
            case '4': return "Apr"; break;
            case '5': return "Mei"; break;
            case '6': return "Jun"; break;
            case '7': return "Jul"; break;
            case '8': return "Agu"; break;
            case '9': return "Sep"; break;
            case '10': return "Okt"; break;
            case '11': return "Nov"; break;
            case '12': return "Des"; break;
            case '0': return "Des"; break;
        };
    }

    function balikbulan($bulan){
        switch($bulan){
            case '1': return "12"; break;
            case '2': return "11"; break;
            case '3': return "10"; break;
            case '4': return "9"; break;
            case '5': return "8"; break;
            case '6': return "7"; break;
            case '7': return "6"; break;
            case '8': return "5"; break;
            case '9': return "4"; break;
            case '10': return "3"; break;
            case '11': return "2"; break;
            case '12': return "1"; break;
        };
    }

    // dicopy dari Invoice.php
    public function exportTaxCalculationPontilTimika($clientName,$yearPeriod,$payrollGroup,$dept)
    {
        // test()
        $pt     = $clientName;
        $year   = $yearPeriod;
        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();  
        $spreadsheet->getProperties()->setCreator('Maurice - Web - Android')
            ->setLastModifiedBy('Maurice - Web - Android')
            ->setTitle('Office 2007 XLSX Test Document')
            ->setSubject('Office 2007 XLSX Test Document')
            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Test result file');

        $strSQL = "";
        if($pt=='Redpath_Timika'){
        $strSQL  = "SELECT a.*,b.nie,c.npwp_no,(ot_1+ot_2+ot_3+ot_4)overtime,a.jkk_jkm jamsostek,a.emp_health_bpjs bpjs_kes
                    FROM trn_salary_slip a
                    LEFT JOIN mst_salary b ON a.bio_rec_id=b.bio_rec_id
                    LEFT JOIN mst_bio_rec c ON a.bio_rec_id=c.bio_rec_id
                    WHERE a.client_name='".$pt."' AND a.year_period='".$year."' ORDER BY a.name,a.month_period ASC";

        }elseif($pt='Pontil_Timika'){
        $strSQL  = "SELECT a.*,b.nie,c.npwp_no,(ot_1+ot_2+ot_3+ot_4)overtime,a.jkk_jkm jamsostek,a.emp_health_bpjs bpjs_kes
                    FROM trn_salary_slip a
                    LEFT JOIN mst_salary b ON a.bio_rec_id=b.bio_rec_id
                    LEFT JOIN mst_bio_rec c ON a.bio_rec_id=c.bio_rec_id
                    WHERE a.client_name='".$pt."' AND a.year_period='".$year."' AND a.payroll_group ='".$payrollGroup."' ORDER BY a.name,a.month_period ASC";

        }
        // test($strSQL,1);
        $query = $this->db->query($strSQL)->result_array();  
        $boldFont = [
            'font' => [
                'bold' => true
                // 'color' => ['argb' => '0000FF'],
            ],
        ];

        $totalStyle = [
            'font' => [
                'bold' => true,
                'color' => ['argb' => '0000FF'],
            ],
        ];

        $allBorderStyle = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $outlineBorderStyle = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $topBorderStyle = [
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $bottomBorderStyle = [
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        foreach(range('B','Q') as $columnID)
        {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }           

        // Nama Field Baris Pertama
        $spreadsheet->getActiveSheet()
                ->setCellValue('A1', 'TAX CALCULATION PT. SANGATI SOERYA SEJAHTERA')
                ->setCellValue('A2', 'Employee Income Tax Calculation Pontil Timika')
                ->setCellValue('A3', 'For the month of');

        $spreadsheet->getActiveSheet()->mergeCells("A1:F1");
        $spreadsheet->getActiveSheet()->mergeCells("A2:F2");
        $spreadsheet->getActiveSheet()->getStyle("A1:D1")->getFont()->setBold(true)->setSize(16);
        $spreadsheet->getActiveSheet()->getStyle("A2:D2")->getFont()->setBold(true)->setSize(13);
        $spreadsheet->getActiveSheet()->getStyle("A3:G5")->getFont()->setBold(true)->setSize(12);       

        /* SET HEADER BG COLOR*/
        $spreadsheet->getActiveSheet()->getStyle('A6:BJ7')
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B'); 

        /* START INVOICE TITLE */
        $spreadsheet->getActiveSheet()->getStyle("A6:BJ6")->getFont()->setSize(12);
        $spreadsheet->getActiveSheet()->getStyle("A6:BJ7")->applyFromArray($allBorderStyle);        
        $spreadsheet->getActiveSheet()->getStyle("A6:BJ7")->applyFromArray($center);

        $spreadsheet->getActiveSheet()->getStyle("B6:BJ7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->mergeCells("A6:A7");
        $spreadsheet->getActiveSheet()->mergeCells("B6:B7");
        $spreadsheet->getActiveSheet()->mergeCells("C6:C7");
        $spreadsheet->getActiveSheet()->mergeCells("D6:D7");
        $spreadsheet->getActiveSheet()->mergeCells("E6:E7");
        $spreadsheet->getActiveSheet()->mergeCells("F6:F7");
        $spreadsheet->getActiveSheet()->mergeCells("G6:G7");
        $spreadsheet->getActiveSheet()->mergeCells("H6:H7");
        $spreadsheet->getActiveSheet()->mergeCells("I6:I7");
        $spreadsheet->getActiveSheet()->mergeCells("J6:J7");
        $spreadsheet->getActiveSheet()->mergeCells("K6:K7");
        $spreadsheet->getActiveSheet()->mergeCells("L6:V6");
        $spreadsheet->getActiveSheet()->mergeCells("W6:Y6");
        $spreadsheet->getActiveSheet()->mergeCells("Z6:Z7");
        $spreadsheet->getActiveSheet()->mergeCells("AA6:AA7");
        $spreadsheet->getActiveSheet()->mergeCells("AB6:AB7");
        $spreadsheet->getActiveSheet()->mergeCells("AC6:AC7");
        $spreadsheet->getActiveSheet()->mergeCells("AD6:AD7");
        $spreadsheet->getActiveSheet()->mergeCells("AE6:AE7");
        $spreadsheet->getActiveSheet()->mergeCells("AF6:AF7");
        $spreadsheet->getActiveSheet()->mergeCells("AG6:AG7");
        $spreadsheet->getActiveSheet()->mergeCells("AH6:AH7");
        $spreadsheet->getActiveSheet()->mergeCells("AI6:AI7");
        $spreadsheet->getActiveSheet()->mergeCells("AJ6:AJ7");
        $spreadsheet->getActiveSheet()->mergeCells("AK6:AK7");
        $spreadsheet->getActiveSheet()->mergeCells("AL6:AL7");
        $spreadsheet->getActiveSheet()->mergeCells("AM6:AM7");
        $spreadsheet->getActiveSheet()->mergeCells("AN6:AN7");
        $spreadsheet->getActiveSheet()->mergeCells("AO6:AO7");
        $spreadsheet->getActiveSheet()->mergeCells("AP6:AP7");
        $spreadsheet->getActiveSheet()->mergeCells("AQ6:AQ7");
        $spreadsheet->getActiveSheet()->mergeCells("AR6:AR7");
        $spreadsheet->getActiveSheet()->mergeCells("AS6:AS7");
        $spreadsheet->getActiveSheet()->mergeCells("AT6:AT7");
        $spreadsheet->getActiveSheet()->mergeCells("AU6:AU7");
        $spreadsheet->getActiveSheet()->mergeCells("AV6:AV7");
        $spreadsheet->getActiveSheet()->mergeCells("AW6:AW7");
        $spreadsheet->getActiveSheet()->mergeCells("AX6:AX7");
        $spreadsheet->getActiveSheet()->mergeCells("AY6:AY7");
        $spreadsheet->getActiveSheet()->mergeCells("AZ6:AZ7");
        $spreadsheet->getActiveSheet()->mergeCells("BA6:BA7");
        $spreadsheet->getActiveSheet()->mergeCells("BB6:BB7");
        $spreadsheet->getActiveSheet()->mergeCells("BC6:BC7");
        $spreadsheet->getActiveSheet()->mergeCells("BD6:BD7");
        $spreadsheet->getActiveSheet()->mergeCells("BE6:BE7");
        $spreadsheet->getActiveSheet()->mergeCells("BF6:BF7");
        $spreadsheet->getActiveSheet()->mergeCells("BG6:BG7");
        $spreadsheet->getActiveSheet()->mergeCells("BH6:BH7");
        $spreadsheet->getActiveSheet()->mergeCells("BI6:BI7");
        $spreadsheet->getActiveSheet()->mergeCells("BJ6:BJ7");

        $spreadsheet->getActiveSheet()
                ->setCellValue('A6', 'NO')
                ->setCellValue('B6', 'Periode Cut Off Timesheet')
                ->setCellValue('C6', 'Name')
                ->setCellValue('D6', 'EMPL Code')
                ->setCellValue('E6', 'Nomor NPWP')
                ->setCellValue('F6', 'Status')
                ->setCellValue('G6', 'First Month')
                ->setCellValue('H6', 'Last Month')
                ->setCellValue('I6', 'Number Of Month')
                ->setCellValue('J6', 'Bulan Ke')
                ->setCellValue('K6', 'Basic Salary')
                ->setCellValue('L6', 'Reguler Income')
                ->setCellValue('W6', 'Ireguler Income')
                ->setCellValue('Z6', 'Total Income This Month')
                ->setCellValue('AA6', 'Functional Cost On Reg Income')
                ->setCellValue('AB6', 'Functional Cost Ireguler')
                ->setCellValue('AC6', 'BPJS Pensiun (JP) 1%')
                ->setCellValue('AD6', 'BPJS TK (JHT) 2%')
                ->setCellValue('AE6', 'Total Deduction')
                ->setCellValue('AF6', 'Net Income On Reg Income')
                ->setCellValue('AG6', 'Net Income On Non-Reg Income')
                ->setCellValue('AH6', 'Net Income This month')
                ->setCellValue('AI6', 'Net Income Reg Previously')
                ->setCellValue('AJ6', 'Net Income Non-Reg Previously')
                ->setCellValue('AK6', 'Net Income Reg YTD')
                ->setCellValue('AL6', 'Net Income Non-Reg YTD')
                ->setCellValue('AM6', 'Annualisation')
                ->setCellValue('AN6', 'PTKP Deduction')
                ->setCellValue('AO6', 'Taxable Income')
                ->setCellValue('AP6', 'Tax Imposition Base On Reg Income (Rounding)')
                ->setCellValue('AQ6', 'Tax Due Income Per Annum')
                ->setCellValue('AR6', 'Taxable Income Reg Income')
                ->setCellValue('AS6', 'Tax Imposition Base On Reg Income (Rounding)')
                ->setCellValue('AT6', 'Tax Due Per Annum Reg Income ')
                ->setCellValue('AU6', 'Tax Due Non Reg Income YTD')
                ->setCellValue('AV6', 'Tax Already Paid Non Reg YTD')
                ->setCellValue('AW6', 'Tax Due Non Reg This Month')
                ->setCellValue('AX6', 'Tax Due On Regular Income YTD')
                ->setCellValue('AY6', 'Tax Already Paid YTD')
                ->setCellValue('AZ6', 'Tax Due Reg This Month')
                ->setCellValue('BA6', 'Total Tax Due This Month')
                ->setCellValue('BB6', 'Tax Penalty')
                ->setCellValue('BC6', 'Tax Borne By Employee')
                ->setCellValue('BD6', 'BPJS 4%')
                ->setCellValue('BE6', 'BPJS Kes. (empl.contr.) 1%')
                ->setCellValue('BF6', 'JKK & JKM')
                ->setCellValue('BG6', 'BPJS Pensiun 1%')
                ->setCellValue('BH6', 'JHT 2%')
                ->setCellValue('BI6', 'Gaji Sudah Di Bayar')
                ->setCellValue('BJ6', 'Take Home Pay')
                ;

        $spreadsheet->getActiveSheet()
                ->setCellValue('L7', 'Salary IDR')
                ->setCellValue('M7', 'Overtime')
                ->setCellValue('N7', 'Travel Bonus')
                ->setCellValue('O7', 'Allowance Economy')
                ->setCellValue('P7', 'Incentive Bonus')
                ->setCellValue('Q7', 'Shift Bonus')
                ->setCellValue('R7', 'Production Bonus')
                ->setCellValue('S7', 'Absensi')
                ->setCellValue('T7', 'Jamsostek 2,04%')
                ->setCellValue('U7', 'BPJS Kes 4%')
                ->setCellValue('V7', 'Total')
                ->setCellValue('W7', 'Bonus')
                ->setCellValue('X7', 'THR')
                ->setCellValue('Y7', 'Total')
                ;

        /* START TOTAL WORK HOUR */

        $rowIdx             = 7;
        $startIdx           = $rowIdx; 
        $rowNo              = 1;
        $monthNo            = 1;
        $no                 = '';
        $bulanke            = '0';
        $loopIDnya          = '';
        $loopID             = '';
        $tmbhth             = '';
        $prev_income        = 0;
        $taxAlreadyPaidNon  = '';
        $taxAlreadyPaidYTD  = '';
        foreach ($query as $row) {
            $tahunDB    = $row['year_period']-1;
            $tmbhth     = $row['year_period'];
            $cutPeriod  = '';
            $tanggal    = $row['month_period'];
            $namabulan  = $this->namabulan($tanggal);
            $bulan      = $this->balikbulan($tanggal);
            $tanggal1   = $row['month_period']-1;
            $namabulan1 = $this->namabulan($tanggal1);
            $status     = $row['marital_status'];
            // $tahundepanDB = $row['year_period']+1;

            if ($row['bio_rec_id'] != $loopID){
                $prev_income        = '';
                $taxAlreadyPaidNon  = '';
                $taxAlreadyPaidYTD  = '';
                $loopIDnya          = $row['bio_rec_id'];
                $namabulanperiode   = $namabulan;
                $netInRegPrev       = '';
                $netInNonRegPrev    = '';
                $numOfMonth         = $bulan;
                $bulanke            = 1;
                $no                 = $rowNo++;
                if ($pt == 'Pontil_Timika'){
                    $cutPeriod = '26 '.$namabulan1.' '.$tahunDB.' - 25 '.$namabulan.' '.$tmbhth.' ';
                }
                else{
                    $cutPeriod = '';
                }
            }
            else{
                $loopIDnya          = $loopID;
                $namabulanperiode   = $namabulanperiode;
                $numOfMonth         = $numOfMonth;
                $taxAlreadyPaidNon  = $taxDueNonReg;
                $no                 = '';
                $bulanke            = $bulanke+1;
                $netInRegPrev       = $netInRegYTD;
                $netInNonRegPrev    = $netInNonRegYTD;
                if ($pt == 'Pontil_Timika'){
                    $cutPeriod = '26 '.$namabulan1.' '.$tmbhth.' - 25 '.$namabulan.' '.$tmbhth.' ';
                }
                else{
                    $cutPeriod = '';
                }
            }
            $loopID         = $row['bio_rec_id'];

            // BPJS jht
            $bpjsJht = $row['basic_salary'] *2/100;

            $bpjsKes     = '';
            if($row['basic_salary']<12000000){
                $bpjsKes = $row['basic_salary'] * 4/100;
            }else{
                $bpjsKes = 12000000 * 4/100;
            }
            // $tmbhth    = $row['year_period'];
            $rowIdx++;
            $maxincome      = 500000;
            $incm = 0;
            $totalReg       = $row['bs_prorate'] + $row['overtime'] + $row['travel_bonus'] + $row['allowance_economy'] + $row['incentive_bonus'] + $row['shift_bonus'] + $row['production_bonus'] + $row['jkk_jkm'] + $bpjsKes - $row['unpaid_total'];
            $totalIreg      = $row['cc_payment'] + $row['thr'];
           
            // Total Income This Month
            $totIcome       = $totalReg+$totalIreg;

            // Functional Cost On Reg Income
            if($totalReg*5/100 < $maxincome){
                $incm       = $totalReg * 5/100;
            }
            else{
                $incm       = $maxincome;
            }

            // Functional Cost Ireguler
            $funCostIr      = 0;
            if($totalReg*5/100 > $maxincome){
                $funCostIr  = 0;
            }
            elseif ($totIcome*5/100 > $maxincome){
                $funCostIr  = $maxincome - $incm;
            }
            else{
                $funCostIr  = $funCostIr;
            }

            // BPJS Pensiun (JP) 1%
            $jp             = 0;
            if($row['basic_salary'] < 8512400){
                $jp         = $row['basic_salary'] * 1/100;
            }
            else{
                $jp         = $row['basic_salary'] * 1/100;
            }

            // Total Deduction
            $totDeducation  = $incm + $funCostIr + $jp + $bpjsJht;

            // Net Income On Reg Income
            $netIncomeReg   = $totalReg - $incm - $jp - $bpjsJht;

            // Net Income On Non-Reg Income
            $netIncomeNonReg = $totalIreg - $funCostIr;

            // Net Income This month
            $netIncomeMonth = $netIncomeReg + $netIncomeNonReg;

            // Net Income Reg Previously
            // Net Income Non-Reg Previously
            // Net Income Reg YTD
            // Net Income Non-Reg YTD
            // $netInRegPrev   = '';
            // $netInNonRegPrev = '';
            
            $netInRegYTD    = $netIncomeReg + $prev_income;
            $netInNonRegYTD = $netIncomeNonReg + $netInNonRegPrev;


            $lastMonth      = '';
            $firsMonth      = '';
            $tmbhth         = substr($tmbhth,2,2);
            $lastMonth      = ''.$namabulan.' '.$tmbhth.'';
            $firsMonth      = ''.$namabulanperiode.' '.$tmbhth.'';

            // Annualisation
            $annualisation  = $netInRegYTD*($numOfMonth/$bulanke)+$netInNonRegYTD;

            // PTKP Deduction 
            $ptkpDeduction  = '';
            $statstr        = substr($status,1,1);
            $statstr2       = substr($status,0,2);
            if ($statstr2 == 'TK'){
                $ptkpDeduction = 54000000;
            }
            else{
                if ($statstr == 0){
                    $ptkpDeduction = 58500000;
                }
                elseif ($statstr == 1){
                    $ptkpDeduction = 63000000;
                }
                elseif ($statstr == 2){
                    $ptkpDeduction = 67500000;
                }
                else{
                    $ptkpDeduction = 72000000;
                }
            }
            
            // if =IF(AO12<AP12;0;AO12-AP12) Taxable Income
            $taxIncome      = '';
            if($annualisation<$ptkpDeduction){
                $taxIncome  = 0;
            }
            else{
                $taxIncome  = $annualisation - $ptkpDeduction;
            }

            // Tax Imposition (Rounding)
            $taxImpRound    = '';
            if ($taxIncome > 0){
                $taxImpRound = round($taxIncome/1000-0.5,0)*1000;
            }
            else{
                $taxImpRound = 0;
            }

            // Tax Due Income Per Annum
            $taxDueIncome   = '';
            if ($taxImpRound>500000000){
                $taxDueIncome = (0.3*$taxImpRound)-55000000;
            }
            elseif($taxImpRound>250000000){
                $taxDueIncome = (0.25*$taxImpRound)-30000000;
            }
            elseif($taxImpRound>50000000){
                $taxDueIncome = (0.15*$taxImpRound)-5000000;
            }
            else{
                $taxDueIncome = $taxImpRound*0.05;
            }

            // Taxable Income Reg Income
            $taxInRegIn     = '';
            if($annualisation-$netInNonRegYTD-$ptkpDeduction<0){
                $taxInRegIn = 0;
            }
            else{
                $taxInRegIn = $annualisation-$netInNonRegYTD-$ptkpDeduction;
            }
            // Tax Imposition Base On Reg Income (Rounding)
            $taxImpBaseRound = '';
            if ($taxInRegIn > 0){
                $taxImpBaseRound = round($taxInRegIn/1000-0.5,0)*1000;
            }
            else{
                $taxImpBaseRound = 0;
            }

            // Tax Due Per Annum Reg Income 
            $taxDuePerAnnum = '';
            if($taxImpBaseRound>500000000){
                $taxDuePerAnnum = (0.3*$taxImpBaseRound)-55000000;
            }
            elseif($taxImpBaseRound>250000000){
                $taxDuePerAnnum = (0.25*$taxImpBaseRound)-30000000;
            }
            elseif($taxImpBaseRound>50000000){
                $taxDuePerAnnum = (0.15*$taxImpBaseRound)-5000000;
            }
            else{
                $taxDuePerAnnum = $taxImpBaseRound*0.05;
            }

            // Tax Due Non Reg Income YTD
            $taxDueNonReg    = 0;
            if($netInNonRegYTD<=0){
                $taxDueNonReg = $taxAlreadyPaidNon;
            }
            else{
                $taxDueNonReg = $taxDueIncome - $taxDuePerAnnum;
            }

            // Tax Due Non Reg This Month
            $taxDueNonRegThisM = $taxDueNonReg - $taxAlreadyPaidNon;
            // Tax Due On Regular Income YTD
            $taxDueOnRegIncomeYTD = $taxDuePerAnnum/($numOfMonth/$bulanke);

            // Tax Due Reg This Month
            $taxDueRegThisMonth = $taxDueOnRegIncomeYTD - $taxAlreadyPaidYTD;

            // Total Tax Due This Month
            $totalTaxDueThisMonth = $taxDueRegThisMonth + $taxDueNonRegThisM;

            // Tax Penalty
            $taxPenalty     = 0;
            if ($row['npwp_no'] == 0){
                $taxPenalty = $totalTaxDueThisMonth * 20/100;
            }
            else{
                $taxPenalty = 0;
            }

            // Tax Borne By Employee
            $taxBorneByEmployee = $totalTaxDueThisMonth + $taxPenalty;

             // BPJS Kes. (empl.contr.) 1%
            $bpjsKesEmp     = '';
            if($row['basic_salary']<12000000){
                $bpjsKesEmp = $row['basic_salary'] * 1/100;
            }else{
                $bpjsKesEmp = 12000000 * 1/100;
            }

            // Gaji Sudah Di Bayar
            $gajiSudahDiBayar = '';

            // Take Home Pay
            $takeHomePay = $totIcome-($taxBorneByEmployee+$bpjsKes+$bpjsKesEmp+$row['jkk_jkm']+$jp+$bpjsJht+$gajiSudahDiBayar);    
            // $chargeTotal = $gross + $row['uplift'];
            $spreadsheet->getActiveSheet()
                ->setCellValue('A'.$rowIdx, $no)
                ->setCellValue('B'.$rowIdx, $cutPeriod)
                ->setCellValue('C'.$rowIdx, $row['name'])
                ->setCellValue('D'.$rowIdx, $row['nie'])
                ->setCellValue('E'.$rowIdx, $row['npwp_no'])
                ->setCellValue('F'.$rowIdx, $row['marital_status'])
                ->setCellValue('G'.$rowIdx, $firsMonth)
                ->setCellValue('H'.$rowIdx, $lastMonth)
                ->setCellValue('I'.$rowIdx, $numOfMonth)
                ->setCellValue('J'.$rowIdx, $bulanke)
                ->setCellValue('K'.$rowIdx, $row['basic_salary'])
                ->setCellValue('L'.$rowIdx, $row['bs_prorate'])
                ->setCellValue('M'.$rowIdx, $row['overtime'])
                ->setCellValue('N'.$rowIdx, $row['travel_bonus'])
                ->setCellValue('O'.$rowIdx, $row['allowance_economy'])
                ->setCellValue('P'.$rowIdx, $row['incentive_bonus'])
                ->setCellValue('Q'.$rowIdx, $row['shift_bonus'])
                ->setCellValue('R'.$rowIdx, $row['production_bonus'])
                ->setCellValue('S'.$rowIdx, $row['unpaid_total'])
                ->setCellValue('T'.$rowIdx, $row['jkk_jkm'])
                ->setCellValue('U'.$rowIdx, $bpjsKes)
                ->setCellValue('V'.$rowIdx, $totalReg)
                ->setCellValue('W'.$rowIdx, $row['cc_payment'])
                ->setCellValue('X'.$rowIdx, $row['thr'])
                ->setCellValue('Y'.$rowIdx, $totalIreg)
                ->setCellValue('Z'.$rowIdx, $totIcome)
                ->setCellValue('AA'.$rowIdx, $incm)
                ->setCellValue('AB'.$rowIdx, $funCostIr)
                ->setCellValue('AC'.$rowIdx, $jp)
                ->setCellValue('AD'.$rowIdx, $bpjsJht)
                ->setCellValue('AE'.$rowIdx, $totDeducation)
                ->setCellValue('AF'.$rowIdx, $netIncomeReg)
                ->setCellValue('AG'.$rowIdx, $netIncomeNonReg)
                ->setCellValue('AH'.$rowIdx, $netIncomeMonth)
                ->setCellValue('AI'.$rowIdx, $prev_income)
                ->setCellValue('AJ'.$rowIdx, $netInNonRegPrev)
                ->setCellValue('AK'.$rowIdx, $netInRegYTD)
                ->setCellValue('AL'.$rowIdx, $netInNonRegYTD)
                ->setCellValue('AM'.$rowIdx, $annualisation)
                ->setCellValue('AN'.$rowIdx, $ptkpDeduction)
                ->setCellValue('AO'.$rowIdx, $taxIncome)
                ->setCellValue('AP'.$rowIdx, $taxImpRound)
                ->setCellValue('AQ'.$rowIdx, $taxDueIncome)
                ->setCellValue('AR'.$rowIdx, $taxInRegIn)
                ->setCellValue('AS'.$rowIdx, $taxImpBaseRound)
                ->setCellValue('AT'.$rowIdx, $taxDuePerAnnum)
                ->setCellValue('AU'.$rowIdx, $taxDueNonReg)
                ->setCellValue('AV'.$rowIdx, $taxAlreadyPaidNon)
                ->setCellValue('AW'.$rowIdx, $taxDueNonRegThisM)
                ->setCellValue('AX'.$rowIdx, $taxDueOnRegIncomeYTD)
                ->setCellValue('AY'.$rowIdx, $taxAlreadyPaidYTD)
                ->setCellValue('AZ'.$rowIdx, $taxDueRegThisMonth)
                ->setCellValue('BA'.$rowIdx, $totalTaxDueThisMonth)
                ->setCellValue('BB'.$rowIdx, $taxPenalty)
                ->setCellValue('BC'.$rowIdx, $taxBorneByEmployee)
                ->setCellValue('BD'.$rowIdx, $bpjsKes)
                ->setCellValue('BE'.$rowIdx, $bpjsKesEmp)
                ->setCellValue('BF'.$rowIdx, $row['jkk_jkm'])
                ->setCellValue('BG'.$rowIdx, $jp)
                ->setCellValue('BH'.$rowIdx, $bpjsJht)
                // ->setCellValue('BI'.$rowIdx, $row['emp_health_bpjs'])
                ->setCellValue('BJ'.$rowIdx, $takeHomePay)
                ;

            if($rowIdx % 2 == 1)
            {
                $spreadsheet->getActiveSheet()->getStyle('A'.$rowIdx.':BJ'.$rowIdx)
                    ->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB('EAEBAF');             
            } 
        $prev_income    = $netInRegYTD;
        $taxAlreadyPaidNon    = $taxDueNonReg;
        // Tax Already Paid YTD
        $taxAlreadyPaidYTD = $taxDueOnRegIncomeYTD;
        }

        $spreadsheet->getActiveSheet()
            ->setCellValue('F'.($rowIdx+2), 'TOTAL')
            ->setCellValue('BJ'.($rowIdx+2), '=SUM(BJ'.$startIdx.':BJ'.$rowIdx.')')
            ;
        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":BJ".($rowIdx+2))->getFont()->setBold(true)->setSize(12);
        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":BJ".($rowIdx+2))->applyFromArray($outlineBorderStyle);

        $spreadsheet->getActiveSheet()->getStyle('K8:BJ'.($rowIdx+2))->getNumberFormat()->setFormatCode('#,##0.00');       
        
        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":BJ".($rowIdx+2))
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B');

        
        unset($allBorderStyle);
        unset($center);
        unset($right);
        unset($left);
        
        $spreadsheet->setActiveSheetIndex(0);

        $str = 'PTLTMKSumInvoice';
        $fileName = preg_replace('/\s+/', '', $str);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.Xlsx"');
        // header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        /* BY COMPOSER */
        // $writer = new Xlsx($spreadsheet);
        /* OFFLINE/ BY COPY EXCEL FOLDER  */
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit(0);   
    }

    public function exportTaxCalculationPontilSumbawa($clientName,$yearPeriod)
    {
        $pt     = $clientName;
        $year   = $yearPeriod;
        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();  
        $spreadsheet->getProperties()->setCreator('Maurice - Web - Android')
            ->setLastModifiedBy('Maurice - Web - Android')
            ->setTitle('Office 2007 XLSX Test Document')
            ->setSubject('Office 2007 XLSX Test Document')
            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Test result file');
        
        $strSQL  = "SELECT a.*,b.nie,c.npwp_no,(ot_1+ot_2+ot_3+ot_4)overtime,a.jkk_jkm jamsostek,a.emp_health_bpjs bpjs_kes
                    FROM trn_slip_ptlsmb a
                    LEFT JOIN mst_salary b ON a.bio_rec_id=b.bio_rec_id
                    LEFT JOIN mst_bio_rec c ON a.bio_rec_id=c.bio_rec_id
                    WHERE a.client_name='".$pt."' AND a.year_period='".$year."' ORDER BY a.name,a.month_period ASC LIMIT 100";
        $query = $this->db->query($strSQL)->result_array();  
        $boldFont = [
            'font' => [
                'bold' => true
                // 'color' => ['argb' => '0000FF'],
            ],
        ];

        $totalStyle = [
            'font' => [
                'bold' => true,
                'color' => ['argb' => '0000FF'],
            ],
        ];

        $allBorderStyle = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $outlineBorderStyle = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $topBorderStyle = [
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $bottomBorderStyle = [
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        foreach(range('B','Q') as $columnID)
        {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }           

        // Nama Field Baris Pertama
        $spreadsheet->getActiveSheet()
                ->setCellValue('A1', 'TAX CALCULATION PT. SANGATI SOERYA SEJAHTERA')
                ->setCellValue('A2', 'Employee Income Tax Calculation Pontil Sumbawa')
                ->setCellValue('A3', 'For the month of');

        $spreadsheet->getActiveSheet()->mergeCells("A1:F1");
        $spreadsheet->getActiveSheet()->mergeCells("A2:F2");
        $spreadsheet->getActiveSheet()->getStyle("A1:D1")->getFont()->setBold(true)->setSize(16);
        $spreadsheet->getActiveSheet()->getStyle("A2:D2")->getFont()->setBold(true)->setSize(13);
        $spreadsheet->getActiveSheet()->getStyle("A3:G5")->getFont()->setBold(true)->setSize(12);       

        /* SET HEADER BG COLOR*/
        $spreadsheet->getActiveSheet()->getStyle('A6:BI7')
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B'); 

        /* START INVOICE TITLE */
        $spreadsheet->getActiveSheet()->getStyle("A6:BI6")->getFont()->setSize(12);
        $spreadsheet->getActiveSheet()->getStyle("A6:BI7")->applyFromArray($allBorderStyle);        
        $spreadsheet->getActiveSheet()->getStyle("A6:BI7")->applyFromArray($center);

        $spreadsheet->getActiveSheet()->getStyle("B6:BJ7")->getAlignment()->setWrapText(true);
        // $spreadsheet->getActiveSheet()->getStyle("A6:H6")->applyFromArray($outlineBorderStyle);

        $spreadsheet->getActiveSheet()->mergeCells("A6:A7");
        $spreadsheet->getActiveSheet()->mergeCells("B6:B7");
        $spreadsheet->getActiveSheet()->mergeCells("C6:C7");
        $spreadsheet->getActiveSheet()->mergeCells("D6:D7");
        $spreadsheet->getActiveSheet()->mergeCells("E6:E7");
        $spreadsheet->getActiveSheet()->mergeCells("F6:F7");
        $spreadsheet->getActiveSheet()->mergeCells("G6:G7");
        $spreadsheet->getActiveSheet()->mergeCells("H6:H7");
        $spreadsheet->getActiveSheet()->mergeCells("I6:I7");
        $spreadsheet->getActiveSheet()->mergeCells("J6:J7");
        $spreadsheet->getActiveSheet()->mergeCells("K6:K7");
        $spreadsheet->getActiveSheet()->mergeCells("L6:U6");
        $spreadsheet->getActiveSheet()->mergeCells("V6:X6");
        // $spreadsheet->getActiveSheet()->mergeCells("U6:U7");
        // $spreadsheet->getActiveSheet()->mergeCells("U6:U7");
        // $spreadsheet->getActiveSheet()->mergeCells("V6:V7");
        // $spreadsheet->getActiveSheet()->mergeCells("W6:W7");
        // $spreadsheet->getActiveSheet()->mergeCells("X6:X7");
        $spreadsheet->getActiveSheet()->mergeCells("Y6:Y7");
        $spreadsheet->getActiveSheet()->mergeCells("Z6:Z7");
        $spreadsheet->getActiveSheet()->mergeCells("AA6:AA7");
        $spreadsheet->getActiveSheet()->mergeCells("AB6:AB7");
        $spreadsheet->getActiveSheet()->mergeCells("AC6:AC7");
        $spreadsheet->getActiveSheet()->mergeCells("AD6:AD7");
        $spreadsheet->getActiveSheet()->mergeCells("AE6:AE7");
        $spreadsheet->getActiveSheet()->mergeCells("AF6:AF7");
        $spreadsheet->getActiveSheet()->mergeCells("AG6:AG7");
        $spreadsheet->getActiveSheet()->mergeCells("AH6:AH7");
        $spreadsheet->getActiveSheet()->mergeCells("AI6:AI7");
        $spreadsheet->getActiveSheet()->mergeCells("AJ6:AJ7");
        $spreadsheet->getActiveSheet()->mergeCells("AK6:AK7");
        $spreadsheet->getActiveSheet()->mergeCells("AL6:AL7");
        $spreadsheet->getActiveSheet()->mergeCells("AM6:AM7");
        $spreadsheet->getActiveSheet()->mergeCells("AN6:AN7");
        $spreadsheet->getActiveSheet()->mergeCells("AO6:AO7");
        $spreadsheet->getActiveSheet()->mergeCells("AP6:AP7");
        $spreadsheet->getActiveSheet()->mergeCells("AQ6:AQ7");
        $spreadsheet->getActiveSheet()->mergeCells("AR6:AR7");
        $spreadsheet->getActiveSheet()->mergeCells("AS6:AS7");
        $spreadsheet->getActiveSheet()->mergeCells("AT6:AT7");
        $spreadsheet->getActiveSheet()->mergeCells("AU6:AU7");
        $spreadsheet->getActiveSheet()->mergeCells("AV6:AV7");
        $spreadsheet->getActiveSheet()->mergeCells("AW6:AW7");
        $spreadsheet->getActiveSheet()->mergeCells("AX6:AX7");
        $spreadsheet->getActiveSheet()->mergeCells("AY6:AY7");
        $spreadsheet->getActiveSheet()->mergeCells("AZ6:AZ7");
        $spreadsheet->getActiveSheet()->mergeCells("BA6:BA7");
        $spreadsheet->getActiveSheet()->mergeCells("BB6:BB7");
        $spreadsheet->getActiveSheet()->mergeCells("BC6:BC7");
        $spreadsheet->getActiveSheet()->mergeCells("BD6:BD7");
        $spreadsheet->getActiveSheet()->mergeCells("BE6:BE7");
        $spreadsheet->getActiveSheet()->mergeCells("BF6:BF7");
        $spreadsheet->getActiveSheet()->mergeCells("BG6:BG7");
        $spreadsheet->getActiveSheet()->mergeCells("BH6:BH7");
        $spreadsheet->getActiveSheet()->mergeCells("BI6:BI7");
        // $spreadsheet->getActiveSheet()->mergeCells("BJ6:BJ7");

        $spreadsheet->getActiveSheet()
                ->setCellValue('A6', 'NO')
                ->setCellValue('B6', 'Periode Cut Off Timesheet')
                ->setCellValue('C6', 'Name')
                ->setCellValue('D6', 'EMPL Code')
                ->setCellValue('E6', 'Nomor NPWP')
                ->setCellValue('F6', 'Status')
                ->setCellValue('G6', 'First Month')
                ->setCellValue('H6', 'Last Month')
                ->setCellValue('I6', 'Number Of Month')
                ->setCellValue('J6', 'Bulan Ke')
                ->setCellValue('K6', 'Basic Salary')
                ->setCellValue('L6', 'Reguler Income')
                ->setCellValue('v6', 'Ireguler Income')
                ->setCellValue('Y6', 'Total Income This Month')
                ->setCellValue('Z6', 'Functional Cost On Reg Income')
                ->setCellValue('AA6', 'Functional Cost Ireguler')
                ->setCellValue('AB6', 'BPJS Pensiun (JP) 1%')
                ->setCellValue('AC6', 'BPJS TK(JHT) 2%')
                ->setCellValue('AD6', 'Total Deduction')
                ->setCellValue('AE6', 'Net Income On Reg Income')
                ->setCellValue('AF6', 'Net Income On Non-Reg Income')
                ->setCellValue('AG6', 'Net Income This month')
                ->setCellValue('AH6', 'Net Income Reg Previously')
                ->setCellValue('AI6', 'Net Income Non-Reg Previously')
                ->setCellValue('AJ6', 'Net Income Reg YTD')
                ->setCellValue('AK6', 'Net Income Non-Reg YTD')
                ->setCellValue('AL6', 'Annualisation')
                ->setCellValue('AM6', 'PTKP Deduction')
                ->setCellValue('AN6', 'Taxable Income')
                ->setCellValue('AO6', 'Tax Imposition Base On Reg Income (Rounding)')
                ->setCellValue('AP6', 'Tax Due Income Per Annum')
                ->setCellValue('AQ6', 'Taxable Income Reg Income')
                ->setCellValue('AR6', 'Tax Imposition Base On Reg Income (Rounding)')
                ->setCellValue('AS6', 'Tax Due Per Annum Reg Income ')
                ->setCellValue('AT6', 'Tax Due Non Reg Income YTD')
                ->setCellValue('AU6', 'Tax Already Paid Non Reg YTD')
                ->setCellValue('AV6', 'Tax Due Non Reg This Month')
                ->setCellValue('AW6', 'Tax Due On Regular Income YTD')
                ->setCellValue('AX6', 'Tax Already Paid YTD')
                ->setCellValue('AY6', 'Tax Due Reg This Month')
                ->setCellValue('AZ6', 'Total Tax Due This Month')
                ->setCellValue('BA6', 'Tax Penalty')
                ->setCellValue('BB6', 'Tax Borne By Employee')
                ->setCellValue('BC6', 'BPJS 4%')
                ->setCellValue('BD6', 'BPJS Kes. (empl.contr.) 1%')
                ->setCellValue('BE6', 'JKK & JKM')
                ->setCellValue('BF6', 'BPJS Pensiun 1%')
                ->setCellValue('BG6', 'JHT 2%')
                ->setCellValue('BH6', 'Gaji Sudah Di Bayar')
                ->setCellValue('BI6', 'Take Home Pay')
                ;

        $spreadsheet->getActiveSheet()
                ->setCellValue('L7', 'Salary IDR')
                ->setCellValue('M7', 'Overtime')
                ->setCellValue('N7', 'Out Camp Bonus')
                ->setCellValue('O7', 'TVR Extra Time')
                ->setCellValue('P7', 'Attendance Bonus')
                ->setCellValue('Q7', 'Act SPV Bonus')
                // ->setCellValue('R7', 'Production Bonus')
                ->setCellValue('R7', 'Absensi')
                ->setCellValue('S7', 'Jamsostek 2,04%')
                ->setCellValue('T7', 'BPJS Kes 4%')
                ->setCellValue('U7', 'Total')
                ->setCellValue('V7', 'Contract Bonus')
                ->setCellValue('W7', 'THR')
                ->setCellValue('X7', 'Total')
                ;

        /* START TOTAL WORK HOUR */

        $rowIdx             = 7;
        $startIdx           = $rowIdx; 
        $rowNo              = 1;
        $monthNo            = 1;
        $no                 = '';
        $bulanke            = '0';
        $loopIDnya          = '';
        $loopID             = '';
        $tmbhth             = '';
        $prev_income        = 0;
        $taxAlreadyPaidNon  = '';
        $taxAlreadyPaidYTD  = '';
        foreach ($query as $row) {
            $tahunDB    = $row['year_period'];
            $tmbhth     = $row['year_period'];
            $cutPeriod  = '';
            $tanggal    = $row['month_period'];
            $namabulan  = $this->namabulan($tanggal);
            $bulan      = $this->balikbulan($tanggal);
            $tanggal1   = $row['month_period']-1;
            $namabulan1 = $this->namabulan($tanggal1);
            $status     = $row['marital_status'];
            // $tahundepanDB = $row['year_period']+1;
            // $dateToTest = "2015-02-01";
            $dateToTest = ''.$row['year_period'].'-'.$row['month_period'].' ';
            $lastday    = date('t',strtotime($dateToTest));

            if ($row['bio_rec_id'] != $loopID){
                $prev_income        = '';
                $taxAlreadyPaidNon  = '';
                $taxAlreadyPaidYTD  = '';
                $loopIDnya          = $row['bio_rec_id'];
                $namabulanperiode   = $namabulan;
                $netInRegPrev       = '';
                $netInNonRegPrev    = '';
                $numOfMonth         = $bulan;
                $bulanke            = 1;
                $no                 = $rowNo++;
                if ($pt == 'Pontil_Sumbawa'){
                    $cutPeriod = '1 '.$namabulan.' '.$tahunDB.' - '.$lastday.' '.$namabulan.' '.$tahunDB.' ';
                }
                else{
                    $cutPeriod = '';
                }
            }
            else{
                $loopIDnya          = $loopID;
                $namabulanperiode   = $namabulanperiode;
                $numOfMonth         = $numOfMonth;
                $taxAlreadyPaidNon  = $taxDueNonReg;
                $no                 = '';
                $bulanke            = $bulanke+1;
                $netInNonRegPrev    = $netInNonRegYTD;
                $netInRegPrev       = $netInRegYTD;
                if ($pt == 'Pontil_Sumbawa'){
                    $cutPeriod = '1 '.$namabulan.' '.$tahunDB.' - '.$lastday.' '.$namabulan.' '.$tahunDB.' ';
                }
                else{
                    $cutPeriod = '';
                }
            }
            $loopID         = $row['bio_rec_id'];

            // BPJS jht
            $bpjsJht = $row['basic_salary'] *2/100;

            $bpjsKes     = '';
            if($row['basic_salary']<12000000){
                $bpjsKes = $row['basic_salary'] * 4/100;
            }else{
                $bpjsKes = 12000000 * 4/100;
            }
            // $tmbhth    = $row['year_period'];
            $rowIdx++;
            $maxincome      = 500000;
            $incm = 0;
            $totalReg       = $row['bs_prorate'] + $row['overtime'] + $row['flying_camp'] + $row['travel_bonus'] + $row['attendance_bonus'] + $row['act_manager_bonus'] + $row['jkk_jkm'] + $bpjsKes - $row['unpaid_total'];
            $totalIreg      = $row['contract_bonus'] + $row['thr'];
           
            // Total Income This Month
            $totIcome       = $totalReg+$totalIreg;

            // Functional Cost On Reg Income
            if($totalReg*5/100 < $maxincome){
                $incm       = $totalReg * 5/100;
            }
            else{
                $incm       = $maxincome;
            }

            // Functional Cost Ireguler
            $funCostIr      = 0;
            if($totalReg*5/100 > $maxincome){
                $funCostIr  = 0;
            }
            elseif ($totIcome*5/100 > $maxincome){
                $funCostIr  = $maxincome - $incm;
            }
            else{
                $funCostIr  = $funCostIr;
            }

            // BPJS Pensiun (JP) 1%
            $jp             = 0;
            if($row['basic_salary'] < 8512400){
                $jp         = $row['basic_salary'] * 1/100;
            }
            else{
                $jp         = $row['basic_salary'] * 1/100;
            }

            // Total Deduction
            $totDeducation  = $incm + $funCostIr + $jp + $bpjsJht;

            // Net Income On Reg Income
            $netIncomeReg   = $totalReg - $incm - $jp - $bpjsJht;

            // Net Income On Non-Reg Income
            $netIncomeNonReg = $totalIreg - $funCostIr;

            // Net Income This month
            $netIncomeMonth = $netIncomeReg + $netIncomeNonReg;

            // Net Income Reg Previously
            // Net Income Non-Reg Previously
            // Net Income Reg YTD
            // Net Income Non-Reg YTD
            // $netInRegPrev   = '';
            // $netInNonRegPrev = '';
            
            $netInRegYTD    = $netIncomeReg + $prev_income;
            $netInNonRegYTD = $netIncomeNonReg + $netInNonRegPrev;

            $lastMonth      = '';
            $firsMonth      = '';
            $tmbhth         = substr($tmbhth,2,2);
            $lastMonth      = ''.$namabulan.' '.$tmbhth.'';
            $firsMonth      = ''.$namabulanperiode.' '.$tmbhth.'';

            // Annualisation
            $annualisation  = $netInRegYTD*($numOfMonth/$bulanke)+$netInNonRegYTD;

            // PTKP Deduction 
            $ptkpDeduction  = '';
            $statstr        = substr($status,1,1);
            $statstr2       = substr($status,0,2);
            if ($statstr2 == 'TK'){
                $ptkpDeduction = 54000000;
            }
            else{
                if ($statstr == 0){
                    $ptkpDeduction = 58500000;
                }
                elseif ($statstr == 1){
                    $ptkpDeduction = 63000000;
                }
                elseif ($statstr == 2){
                    $ptkpDeduction = 67500000;
                }
                else{
                    $ptkpDeduction = 72000000;
                }
            }
            
            // if =IF(AO12<AP12;0;AO12-AP12) Taxable Income
            $taxIncome      = '';
            if($annualisation<$ptkpDeduction){
                $taxIncome  = 0;
            }
            else{
                $taxIncome  = $annualisation - $ptkpDeduction;
            }

            // Tax Imposition (Rounding)
            $taxImpRound    = '';
            if ($taxIncome > 0){
                $taxImpRound = round($taxIncome/1000-0.5,0)*1000;
            }
            else{
                $taxImpRound = 0;
            }

            // Tax Due Income Per Annum
            $taxDueIncome   = '';
            if ($taxImpRound>500000000){
                $taxDueIncome = (0.3*$taxImpRound)-55000000;
            }
            elseif($taxImpRound>250000000){
                $taxDueIncome = (0.25*$taxImpRound)-30000000;
            }
            elseif($taxImpRound>50000000){
                $taxDueIncome = (0.15*$taxImpRound)-5000000;
            }
            else{
                $taxDueIncome = $taxImpRound*0.05;
            }

            // Taxable Income Reg Income
            $taxInRegIn     = '';
            if($annualisation-$netInNonRegYTD-$ptkpDeduction<0){
                $taxInRegIn = 0;
            }
            else{
                $taxInRegIn = $annualisation-$netInNonRegYTD-$ptkpDeduction;
            }
            // Tax Imposition Base On Reg Income (Rounding)
            $taxImpBaseRound = '';
            if ($taxInRegIn > 0){
                $taxImpBaseRound = round($taxInRegIn/1000-0.5,0)*1000;
            }
            else{
                $taxImpBaseRound = 0;
            }

            // Tax Due Per Annum Reg Income 
            $taxDuePerAnnum = '';
            if($taxImpBaseRound>500000000){
                $taxDuePerAnnum = (0.3*$taxImpBaseRound)-55000000;
            }
            elseif($taxImpBaseRound>250000000){
                $taxDuePerAnnum = (0.25*$taxImpBaseRound)-30000000;
            }
            elseif($taxImpBaseRound>50000000){
                $taxDuePerAnnum = (0.15*$taxImpBaseRound)-5000000;
            }
            else{
                $taxDuePerAnnum = $taxImpBaseRound*0.05;
            }

            // Tax Due Non Reg Income YTD
            $taxDueNonReg    = 0;
            if($netInNonRegYTD<=0){
                $taxDueNonReg = $taxAlreadyPaidNon;
            }
            else{
                $taxDueNonReg = $taxDueIncome - $taxDuePerAnnum;
            }

            // Tax Due Non Reg This Month
            $taxDueNonRegThisM = $taxDueNonReg - $taxAlreadyPaidNon;

            // Tax Due On Regular Income YTD
            $taxDueOnRegIncomeYTD = $taxDuePerAnnum/($numOfMonth/$bulanke);

            // Tax Due Reg This Month
            $taxDueRegThisMonth = $taxDueOnRegIncomeYTD - $taxAlreadyPaidYTD;

            // Total Tax Due This Month
            $totalTaxDueThisMonth = $taxDueRegThisMonth + $taxDueNonRegThisM;

            // Tax Penalty
            $taxPenalty     = 0;
            if ($row['npwp_no'] == 0){
                $taxPenalty = $totalTaxDueThisMonth * 20/100;
            }
            else{
                $taxPenalty = 0;
            }

            // Tax Borne By Employee
            $taxBorneByEmployee = $totalTaxDueThisMonth + $taxPenalty;

            // BPJS Kes. (empl.contr.) 1%
            $bpjsKesEmp     = '';
            if($row['basic_salary']<12000000){
                $bpjsKesEmp = $row['basic_salary'] * 1/100;
            }else{
                $bpjsKesEmp = 12000000 * 1/100;
            }

            // Gaji Sudah Di Bayar
            $gajiSudahDiBayar = '';

            // Take Home Pay
            $takeHomePay = $totIcome-($taxBorneByEmployee+$bpjsKes+$bpjsKesEmp+$row['jkk_jkm']+$jp+$bpjsJht+$gajiSudahDiBayar);    
            // $chargeTotal = $gross + $row['uplift'];
            $spreadsheet->getActiveSheet()
                ->setCellValue('A'.$rowIdx, $no)
                ->setCellValue('B'.$rowIdx, $cutPeriod)
                ->setCellValue('C'.$rowIdx, $row['name'])
                ->setCellValue('D'.$rowIdx, $row['nie'])
                ->setCellValue('E'.$rowIdx, $row['npwp_no'])
                ->setCellValue('F'.$rowIdx, $row['marital_status'])
                ->setCellValue('G'.$rowIdx, $firsMonth)
                ->setCellValue('H'.$rowIdx, $lastMonth)
                ->setCellValue('I'.$rowIdx, $numOfMonth)
                ->setCellValue('J'.$rowIdx, $bulanke)
                ->setCellValue('K'.$rowIdx, $row['basic_salary'])
                ->setCellValue('L'.$rowIdx, $row['bs_prorate'])
                ->setCellValue('M'.$rowIdx, $row['overtime'])
                ->setCellValue('N'.$rowIdx, $row['flying_camp'])
                ->setCellValue('O'.$rowIdx, $row['travel_bonus'])
                ->setCellValue('P'.$rowIdx, $row['attendance_bonus'])
                ->setCellValue('Q'.$rowIdx, $row['act_manager_bonus'])
                ->setCellValue('R'.$rowIdx, $row['unpaid_total'])
                ->setCellValue('S'.$rowIdx, $row['jkk_jkm'])
                ->setCellValue('T'.$rowIdx, $bpjsKes)
                ->setCellValue('U'.$rowIdx, $totalReg)
                ->setCellValue('V'.$rowIdx, $row['contract_bonus'])
                ->setCellValue('W'.$rowIdx, $row['thr'])
                ->setCellValue('X'.$rowIdx, $totalIreg)
                ->setCellValue('Y'.$rowIdx, $totIcome)
                ->setCellValue('Z'.$rowIdx, $incm)
                ->setCellValue('AA'.$rowIdx, $funCostIr)
                ->setCellValue('AB'.$rowIdx, $jp)
                ->setCellValue('AC'.$rowIdx, $bpjsJht)
                ->setCellValue('AD'.$rowIdx, $totDeducation)
                ->setCellValue('AE'.$rowIdx, $netIncomeReg)
                ->setCellValue('AF'.$rowIdx, $netIncomeNonReg)
                ->setCellValue('AG'.$rowIdx, $netIncomeMonth)
                ->setCellValue('AH'.$rowIdx, $prev_income)
                ->setCellValue('AI'.$rowIdx, $netInNonRegPrev)
                ->setCellValue('AJ'.$rowIdx, $netInRegYTD)
                ->setCellValue('AK'.$rowIdx, $netInNonRegYTD)
                ->setCellValue('AL'.$rowIdx, $annualisation)
                ->setCellValue('AM'.$rowIdx, $ptkpDeduction)
                ->setCellValue('AN'.$rowIdx, $taxIncome)
                ->setCellValue('AO'.$rowIdx, $taxImpRound)
                ->setCellValue('AP'.$rowIdx, $taxDueIncome)
                ->setCellValue('AQ'.$rowIdx, $taxInRegIn)
                ->setCellValue('AR'.$rowIdx, $taxImpBaseRound)
                ->setCellValue('AS'.$rowIdx, $taxDuePerAnnum)
                ->setCellValue('AT'.$rowIdx, $taxDueNonReg)
                ->setCellValue('AU'.$rowIdx, $taxAlreadyPaidNon)
                ->setCellValue('AV'.$rowIdx, $taxDueNonRegThisM)
                ->setCellValue('AW'.$rowIdx, $taxDueOnRegIncomeYTD)
                ->setCellValue('AX'.$rowIdx, $taxAlreadyPaidYTD)
                ->setCellValue('AY'.$rowIdx, $taxDueRegThisMonth)
                ->setCellValue('AZ'.$rowIdx, $totalTaxDueThisMonth)
                ->setCellValue('BA'.$rowIdx, $taxPenalty)
                ->setCellValue('BB'.$rowIdx, $taxBorneByEmployee)
                ->setCellValue('BC'.$rowIdx, $bpjsKes)
                ->setCellValue('BD'.$rowIdx, $bpjsKesEmp)
                ->setCellValue('BE'.$rowIdx, $row['jkk_jkm'])
                ->setCellValue('BF'.$rowIdx, $jp)
                ->setCellValue('BG'.$rowIdx, $bpjsJht)
                // ->setCellValue('BH'.$rowIdx, $row['emp_health_bpjs'])
                ->setCellValue('BI'.$rowIdx, $takeHomePay)
                ;

            if($rowIdx % 2 == 1)
            {
                $spreadsheet->getActiveSheet()->getStyle('A'.$rowIdx.':BI'.$rowIdx)
                    ->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB('EAEBAF');             
            } 
        $prev_income    = $netInRegYTD;
        $taxAlreadyPaidNon    = $taxDueNonReg;
        // Tax Already Paid YTD
        $taxAlreadyPaidYTD = $taxDueOnRegIncomeYTD;
        }

        $spreadsheet->getActiveSheet()
            ->setCellValue('F'.($rowIdx+2), 'TOTAL')
            ->setCellValue('BI'.($rowIdx+2), '=SUM(BI'.$startIdx.':BI'.$rowIdx.')')
            ;
        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":BI".($rowIdx+2))->getFont()->setBold(true)->setSize(12);
        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":BI".($rowIdx+2))->applyFromArray($outlineBorderStyle);

        $spreadsheet->getActiveSheet()->getStyle('K8:BI'.($rowIdx+2))->getNumberFormat()->setFormatCode('#,##0.00');       
        
        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":BI".($rowIdx+2))
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B');

        
        unset($allBorderStyle);
        unset($center);
        unset($right);
        unset($left);
        
        $spreadsheet->setActiveSheetIndex(0);

        $str = 'PTLSMBSumInvoice';
        $fileName = preg_replace('/\s+/', '', $str);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.Xlsx"');
        // header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        /* BY COMPOSER */
        // $writer = new Xlsx($spreadsheet);
        /* OFFLINE/ BY COPY EXCEL FOLDER  */
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit(0);   
    }

    public function exportTaxCalculationRedpathTimika($clientName,$yearPeriod,$payrollGroup,$dept)
    {
        $pt     = $clientName;
        $year   = $yearPeriod;
        $dept   = $dept;
        $payrollGroup   = $payrollGroup;
        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();  
        $spreadsheet->getProperties()->setCreator('Maurice - Web - Android')
            ->setLastModifiedBy('Maurice - Web - Android')
            ->setTitle('Office 2007 XLSX Test Document')
            ->setSubject('Office 2007 XLSX Test Document')
            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Test result file');

        $strSQL = "";
        if($pt=='Redpath_Timika'){
        $strSQL  = "SELECT a.*,b.nie,c.npwp_no,(ot_1+ot_2+ot_3+ot_4)overtime,a.jkk_jkm jamsostek,a.emp_health_bpjs bpjs_kes
                    FROM trn_salary_slip a
                    LEFT JOIN mst_salary b ON a.bio_rec_id=b.bio_rec_id
                    LEFT JOIN mst_bio_rec c ON a.bio_rec_id=c.bio_rec_id
                    WHERE a.client_name='".$pt."' AND a.year_period='".$year."' AND a.dept='".$dept."' ORDER BY a.name,a.month_period ASC";

        }elseif($pt='Pontil_Timika'){
        $strSQL  = "SELECT a.*,b.nie,c.npwp_no,(ot_1+ot_2+ot_3+ot_4)overtime,a.jkk_jkm jamsostek,a.emp_health_bpjs bpjs_kes
                    FROM trn_salary_slip a
                    LEFT JOIN mst_salary b ON a.bio_rec_id=b.bio_rec_id
                    LEFT JOIN mst_bio_rec c ON a.bio_rec_id=c.bio_rec_id
                    WHERE a.client_name='".$pt."' AND a.year_period='".$year."' ORDER BY a.name,a.month_period ASC";

        }
        $query = $this->db->query($strSQL)->result_array();  
        $boldFont = [
            'font' => [
                'bold' => true
                // 'color' => ['argb' => '0000FF'],
            ],
        ];

        $totalStyle = [
            'font' => [
                'bold' => true,
                'color' => ['argb' => '0000FF'],
            ],
        ];

        $allBorderStyle = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $outlineBorderStyle = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $topBorderStyle = [
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $bottomBorderStyle = [
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        foreach(range('B','Q') as $columnID)
        {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }           

        // Nama Field Baris Pertama
        $spreadsheet->getActiveSheet()
                ->setCellValue('A1', 'TAX CALCULATION PT. SANGATI SOERYA SEJAHTERA')
                ->setCellValue('A2', 'Employee Income Tax Calculation Redpath Timika')
                ->setCellValue('A3', 'For the month of');

        $spreadsheet->getActiveSheet()->mergeCells("A1:F1");
        $spreadsheet->getActiveSheet()->mergeCells("A2:F2");
        $spreadsheet->getActiveSheet()->getStyle("A1:D1")->getFont()->setBold(true)->setSize(16);
        $spreadsheet->getActiveSheet()->getStyle("A2:D2")->getFont()->setBold(true)->setSize(13);
        $spreadsheet->getActiveSheet()->getStyle("A3:G5")->getFont()->setBold(true)->setSize(12);       

        /* SET HEADER BG COLOR*/
        $spreadsheet->getActiveSheet()->getStyle('A6:BI7')
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B'); 

        /* START INVOICE TITLE */
        $spreadsheet->getActiveSheet()->getStyle("A6:BI6")->getFont()->setSize(12);
        $spreadsheet->getActiveSheet()->getStyle("A6:BI7")->applyFromArray($allBorderStyle);        
        $spreadsheet->getActiveSheet()->getStyle("A6:BI7")->applyFromArray($center);

        $spreadsheet->getActiveSheet()->getStyle("B6:BI7")->getAlignment()->setWrapText(true);

        $spreadsheet->getActiveSheet()->mergeCells("A6:A7");
        $spreadsheet->getActiveSheet()->mergeCells("B6:B7");
        $spreadsheet->getActiveSheet()->mergeCells("C6:C7");
        $spreadsheet->getActiveSheet()->mergeCells("D6:D7");
        $spreadsheet->getActiveSheet()->mergeCells("E6:E7");
        $spreadsheet->getActiveSheet()->mergeCells("F6:F7");
        $spreadsheet->getActiveSheet()->mergeCells("G6:G7");
        $spreadsheet->getActiveSheet()->mergeCells("H6:H7");
        $spreadsheet->getActiveSheet()->mergeCells("I6:I7");
        $spreadsheet->getActiveSheet()->mergeCells("J6:J7");
        $spreadsheet->getActiveSheet()->mergeCells("K6:K7");
        $spreadsheet->getActiveSheet()->mergeCells("L6:U6");
        $spreadsheet->getActiveSheet()->mergeCells("V6:X6");
        $spreadsheet->getActiveSheet()->mergeCells("Y6:Y7");
        $spreadsheet->getActiveSheet()->mergeCells("Z6:Z7");
        $spreadsheet->getActiveSheet()->mergeCells("AA6:AA7");
        $spreadsheet->getActiveSheet()->mergeCells("AB6:AB7");
        $spreadsheet->getActiveSheet()->mergeCells("AC6:AC7");
        $spreadsheet->getActiveSheet()->mergeCells("AD6:AD7");
        $spreadsheet->getActiveSheet()->mergeCells("AE6:AE7");
        $spreadsheet->getActiveSheet()->mergeCells("AF6:AF7");
        $spreadsheet->getActiveSheet()->mergeCells("AG6:AG7");
        $spreadsheet->getActiveSheet()->mergeCells("AH6:AH7");
        $spreadsheet->getActiveSheet()->mergeCells("AI6:AI7");
        $spreadsheet->getActiveSheet()->mergeCells("AJ6:AJ7");
        $spreadsheet->getActiveSheet()->mergeCells("AK6:AK7");
        $spreadsheet->getActiveSheet()->mergeCells("AL6:AL7");
        $spreadsheet->getActiveSheet()->mergeCells("AM6:AM7");
        $spreadsheet->getActiveSheet()->mergeCells("AN6:AN7");
        $spreadsheet->getActiveSheet()->mergeCells("AO6:AO7");
        $spreadsheet->getActiveSheet()->mergeCells("AP6:AP7");
        $spreadsheet->getActiveSheet()->mergeCells("AQ6:AQ7");
        $spreadsheet->getActiveSheet()->mergeCells("AR6:AR7");
        $spreadsheet->getActiveSheet()->mergeCells("AS6:AS7");
        $spreadsheet->getActiveSheet()->mergeCells("AT6:AT7");
        $spreadsheet->getActiveSheet()->mergeCells("AU6:AU7");
        $spreadsheet->getActiveSheet()->mergeCells("AV6:AV7");
        $spreadsheet->getActiveSheet()->mergeCells("AW6:AW7");
        $spreadsheet->getActiveSheet()->mergeCells("AX6:AX7");
        $spreadsheet->getActiveSheet()->mergeCells("AY6:AY7");
        $spreadsheet->getActiveSheet()->mergeCells("AZ6:AZ7");
        $spreadsheet->getActiveSheet()->mergeCells("BA6:BA7");
        $spreadsheet->getActiveSheet()->mergeCells("BB6:BB7");
        $spreadsheet->getActiveSheet()->mergeCells("BC6:BC7");
        $spreadsheet->getActiveSheet()->mergeCells("BD6:BD7");
        $spreadsheet->getActiveSheet()->mergeCells("BE6:BE7");
        $spreadsheet->getActiveSheet()->mergeCells("BF6:BF7");
        $spreadsheet->getActiveSheet()->mergeCells("BG6:BG7");
        $spreadsheet->getActiveSheet()->mergeCells("BH6:BH7");
        $spreadsheet->getActiveSheet()->mergeCells("BI6:BI7");
        // $spreadsheet->getActiveSheet()->mergeCells("BJ6:BJ7");

        $spreadsheet->getActiveSheet()
                ->setCellValue('A6', 'NO')
                ->setCellValue('B6', 'Periode Cut Off Timesheet')
                ->setCellValue('C6', 'Name')
                ->setCellValue('D6', 'EMPL Code')
                ->setCellValue('E6', 'Nomor NPWP')
                ->setCellValue('F6', 'Status')
                ->setCellValue('G6', 'First Month')
                ->setCellValue('H6', 'Last Month')
                ->setCellValue('I6', 'Number Of Month')
                ->setCellValue('J6', 'Bulan Ke')
                ->setCellValue('K6', 'Basic Salary')
                ->setCellValue('L6', 'Reguler Income')
                ->setCellValue('V6', 'Ireguler Income')
                ->setCellValue('Y6', 'Total Income This Month')
                ->setCellValue('Z6', 'Functional Cost On Reg Income')
                ->setCellValue('AA6', 'Functional Cost Ireguler')
                ->setCellValue('AB6', 'BPJS Pensiun (JP) 1%')
                ->setCellValue('AC6', 'BPJS TK(JHT) 2%')
                ->setCellValue('AD6', 'Total Deduction')
                ->setCellValue('AE6', 'Net Income On Reg Income')
                ->setCellValue('AF6', 'Net Income On Non-Reg Income')
                ->setCellValue('AG6', 'Net Income This month')
                ->setCellValue('AH6', 'Net Income Reg Previously')
                ->setCellValue('AI6', 'Net Income Non-Reg Previously')
                ->setCellValue('AJ6', 'Net Income Reg YTD')
                ->setCellValue('AK6', 'Net Income Non-Reg YTD')
                ->setCellValue('AL6', 'Annualisation')
                ->setCellValue('AM6', 'PTKP Deduction')
                ->setCellValue('AN6', 'Taxable Income')
                ->setCellValue('AO6', 'Tax Imposition Base On Reg Income (Rounding)')
                ->setCellValue('AP6', 'Tax Due Income Per Annum')
                ->setCellValue('AQ6', 'Taxable Income Reg Income')
                ->setCellValue('AR6', 'Tax Imposition Base On Reg Income (Rounding)')
                ->setCellValue('AS6', 'Tax Due Per Annum Reg Income ')
                ->setCellValue('AT6', 'Tax Due Non Reg Income YTD')
                ->setCellValue('AU6', 'Tax Already Paid Non Reg YTD')
                ->setCellValue('AV6', 'Tax Due Non Reg This Month')
                ->setCellValue('AW6', 'Tax Due On Regular Income YTD')
                ->setCellValue('AX6', 'Tax Already Paid YTD')
                ->setCellValue('AY6', 'Tax Due Reg This Month')
                ->setCellValue('AZ6', 'Total Tax Due This Month')
                ->setCellValue('BA6', 'Tax Penalty')
                ->setCellValue('BB6', 'Tax Borne By Employee')
                ->setCellValue('BC6', 'BPJS 4%')
                ->setCellValue('BD6', 'BPJS Kes. (empl.contr.) 1%')
                ->setCellValue('BE6', 'JKK & JKM')
                ->setCellValue('BF6', 'BPJS Pensiun 1%')
                ->setCellValue('BG6', 'JHT 2%')
                ->setCellValue('BH6', 'Gaji Sudah Di Bayar')
                ->setCellValue('BI6', 'Take Home Pay')
                ;

        $spreadsheet->getActiveSheet()
                ->setCellValue('L7', 'Salary IDR')
                ->setCellValue('M7', 'Overtime')
                ->setCellValue('N7', 'Total OT * 35%')
                ->setCellValue('O7', 'Shift Bonus')
                ->setCellValue('P7', 'Remote Loc Allowance')
                ->setCellValue('Q7', 'Dev Incentive Bonus')
                ->setCellValue('R7', 'Absensi')
                ->setCellValue('S7', 'Jamsostek 2,04%')
                ->setCellValue('T7', 'BPJS Kes 4%')
                ->setCellValue('U7', 'Total')
                // ->setCellValue('R7', 'Production Bonus')
                ->setCellValue('V7', 'Contract Bonus')
                ->setCellValue('W7', 'THR')
                ->setCellValue('X7', 'Total')
                ;

        /* START TOTAL WORK HOUR */

        $rowIdx             = 7;
        $startIdx           = $rowIdx; 
        $rowNo              = 1;
        $monthNo            = 1;
        $no                 = '';
        $bulanke            = '0';
        $loopIDnya          = '';
        $loopID             = '';
        $tmbhth             = '';
        $prev_income        = 0;
        $taxAlreadyPaidNon  = '';
        $taxAlreadyPaidYTD  = '';
        foreach ($query as $row) {
            $tahunDB    = $row['year_period'];
            $tmbhth     = $row['year_period'];
            $cutPeriod  = '';
            $tanggal    = $row['month_period'];
            $namabulan  = $this->namabulan($tanggal);
            $bulan      = $this->balikbulan($tanggal);
            $tanggal1   = $row['month_period']-1;
            $namabulan1 = $this->namabulan($tanggal1);
            $status     = $row['marital_status'];
            $dateToTest = ''.$row['year_period'].'-'.$row['month_period'].' ';
            $lastday    = date('t',strtotime($dateToTest));
            // $tahundepanDB = $row['year_period']+1;

            if ($row['bio_rec_id'] != $loopID){
                $prev_income        = '';
                $taxAlreadyPaidNon  = '';
                $taxAlreadyPaidYTD  = '';
                $loopIDnya          = $row['bio_rec_id'];
                $namabulanperiode   = $namabulan;
                $numOfMonth         = $bulan;
                $bulanke            = 1;
                $netInNonRegPrev    = '';
                $netInRegPrev       = '';
                $no                 = $rowNo++;
                if ($pt == 'Redpath_Timika'){
                    $cutPeriod = '1 '.$namabulan.' '.$tahunDB.' - '.$lastday.' '.$namabulan.' '.$tahunDB.' ';
                }
                else{
                    $cutPeriod = '';
                }
            }
            else{
                $loopIDnya          = $loopID;
                $namabulanperiode   = $namabulanperiode;
                $numOfMonth         = $numOfMonth;
                $taxAlreadyPaidNon  = $taxDueNonReg;
                $no                 = '';
                $bulanke            = $bulanke+1;
                $netInNonRegPrev    = $netInNonRegYTD;
                $netInRegPrev       = $netInRegYTD;
                if ($pt == 'Redpath_Timika'){
                    $cutPeriod = '1 '.$namabulan.' '.$tahunDB.' - '.$lastday.' '.$namabulan.' '.$tahunDB.' ';
                }
                else{
                    $cutPeriod = '';
                }
            }
            $loopID         = $row['bio_rec_id'];

            // BPJS jht
            $bpjsJht = $row['basic_salary'] *2/100;

            $bpjsKes     = '';
            if($row['basic_salary']<12000000){
                $bpjsKes = $row['basic_salary'] * 4/100;
            }else{
                $bpjsKes = 12000000 * 4/100;
            }

            $rowIdx++;
            $ot35 = $row['overtime']*35/100;
            $maxincome      = 500000;
            $incm = 0;
            $totalReg       = $row['bs_prorate'] + $row['overtime'] + $ot35 + $row['shift_bonus'] + $row['remote_allowance'] + $row['dev_incentive_bonus'] + $row['jkk_jkm'] + $bpjsKes - $row['unpaid_total'];
            $totalIreg      = $row['contract_bonus'] + $row['thr'];
           
            // Total Income This Month
            $totIcome       = $totalReg+$totalIreg;

            // Functional Cost On Reg Income
            if($totalReg*5/100 < $maxincome){
                $incm       = $totalReg * 5/100;
            }
            else{
                $incm       = $maxincome;
            }

            // Functional Cost Ireguler
            $funCostIr      = 0;
            if($totalReg*5/100 > $maxincome){
                $funCostIr  = 0;
            }
            elseif ($totIcome*5/100 > $maxincome){
                $funCostIr  = $maxincome - $incm;
            }
            else{
                $funCostIr  = $funCostIr;
            }

            // BPJS Pensiun (JP) 1%
            $jp             = 0;
            if($row['basic_salary'] < 8512400){
                $jp         = $row['basic_salary'] * 1/100;
            }
            else{
                $jp         = $row['basic_salary'] * 1/100;
            }

            // Total Deduction
            $totDeducation  = $incm + $funCostIr + $jp + $bpjsJht;

            // Net Income On Reg Income
            $netIncomeReg   = $totalReg - $incm - $jp - $bpjsJht;

            // Net Income On Non-Reg Income
            $netIncomeNonReg = $totalIreg - $funCostIr;

            // Net Income This month
            $netIncomeMonth = $netIncomeReg + $netIncomeNonReg;

            // Net Income Reg Previously
            // Net Income Non-Reg Previously
            // Net Income Reg YTD
            // Net Income Non-Reg YTD
            // $netInRegPrev   = '';
            // $netInNonRegPrev = '';
            
            $netInRegYTD    = $netIncomeReg + $prev_income;
            $netInNonRegYTD = $netIncomeNonReg + $netInNonRegPrev;

            $lastMonth      = '';
            $firsMonth      = '';
            $tmbhth         = substr($tmbhth,2,2);
            $lastMonth      = ''.$namabulan.' '.$tmbhth.'';
            $firsMonth      = ''.$namabulanperiode.' '.$tmbhth.'';

            // Annualisation
            $annualisation  = $netInRegYTD*($numOfMonth/$bulanke)+$netInNonRegYTD;

            // PTKP Deduction 
            $ptkpDeduction  = '';
            $statstr        = substr($status,1,1);
            $statstr2       = substr($status,0,2);
            if ($statstr2 == 'TK'){
                $ptkpDeduction = 54000000;
            }
            else{
                if ($statstr == 0){
                    $ptkpDeduction = 58500000;
                }
                elseif ($statstr == 1){
                    $ptkpDeduction = 63000000;
                }
                elseif ($statstr == 2){
                    $ptkpDeduction = 67500000;
                }
                else{
                    $ptkpDeduction = 72000000;
                }
            }
            
            // if =IF(AO12<AP12;0;AO12-AP12) Taxable Income
            $taxIncome      = '';
            if($annualisation<$ptkpDeduction){
                $taxIncome  = 0;
            }
            else{
                $taxIncome  = $annualisation - $ptkpDeduction;
            }

            // Tax Imposition (Rounding)
            $taxImpRound    = '';
            if ($taxIncome > 0){
                $taxImpRound = round($taxIncome/1000-0.5,0)*1000;
            }
            else{
                $taxImpRound = 0;
            }

            // Tax Due Income Per Annum
            $taxDueIncome   = '';
            if ($taxImpRound>500000000){
                $taxDueIncome = (0.3*$taxImpRound)-55000000;
            }
            elseif($taxImpRound>250000000){
                $taxDueIncome = (0.25*$taxImpRound)-30000000;
            }
            elseif($taxImpRound>50000000){
                $taxDueIncome = (0.15*$taxImpRound)-5000000;
            }
            else{
                $taxDueIncome = $taxImpRound*0.05;
            }

            // Taxable Income Reg Income
            $taxInRegIn     = '';
            if($annualisation-$netInNonRegYTD-$ptkpDeduction<0){
                $taxInRegIn = 0;
            }
            else{
                $taxInRegIn = $annualisation-$netInNonRegYTD-$ptkpDeduction;
            }

            // Tax Imposition Base On Reg Income (Rounding)
            $taxImpBaseRound = '';
            if ($taxInRegIn > 0){
                $taxImpBaseRound = round($taxInRegIn/1000-0.5,0)*1000;
            }
            else{
                $taxImpBaseRound = 0;
            }

            // Tax Due Per Annum Reg Income 
            $taxDuePerAnnum = '';
            if($taxImpBaseRound>500000000){
                $taxDuePerAnnum = (0.3*$taxImpBaseRound)-55000000;
            }
            elseif($taxImpBaseRound>250000000){
                $taxDuePerAnnum = (0.25*$taxImpBaseRound)-30000000;
            }
            elseif($taxImpBaseRound>50000000){
                $taxDuePerAnnum = (0.15*$taxImpBaseRound)-5000000;
            }
            else{
                $taxDuePerAnnum = $taxImpBaseRound*0.05;
            }

            // Tax Due Non Reg Income YTD
            $taxDueNonReg    = 0;
            if($netInNonRegYTD<=0){
                $taxDueNonReg = $taxAlreadyPaidNon;
            }
            else{
                $taxDueNonReg = $taxDueIncome - $taxDuePerAnnum;
            }

            // Tax Due Non Reg This Month
            $taxDueNonRegThisM = $taxDueNonReg - $taxAlreadyPaidNon;

            // Tax Due On Regular Income YTD
            $taxDueOnRegIncomeYTD = $taxDuePerAnnum/($numOfMonth/$bulanke);

            // Tax Due Reg This Month
            $taxDueRegThisMonth = $taxDueOnRegIncomeYTD - $taxAlreadyPaidYTD;

            // Total Tax Due This Month
            $totalTaxDueThisMonth = $taxDueRegThisMonth + $taxDueNonRegThisM;

            // Tax Penalty
            $taxPenalty     = 0;
            if ($row['npwp_no'] == 0){
                $taxPenalty = $totalTaxDueThisMonth * 20/100;
            }
            else{
                $taxPenalty = 0;
            }

            // Tax Borne By Employee
            $taxBorneByEmployee = $totalTaxDueThisMonth + $taxPenalty;

            // BPJS Kes. (empl.contr.) 1%
            $bpjsKesEmp     = '';
            if($row['basic_salary']<12000000){
                $bpjsKesEmp = $row['basic_salary'] * 1/100;
            }else{
                $bpjsKesEmp = 12000000 * 1/100;
            }

            // Gaji Sudah Di Bayar
            $gajiSudahDiBayar = '';

            // Take Home Pay
            $takeHomePay = $totIcome-($taxBorneByEmployee+$bpjsKes+$bpjsKesEmp+$row['jkk_jkm']+$jp+$bpjsJht+$gajiSudahDiBayar); 

            // $chargeTotal = $gross + $row['uplift'];
            $spreadsheet->getActiveSheet()
                ->setCellValue('A'.$rowIdx, $no)
                ->setCellValue('B'.$rowIdx, $cutPeriod)
                ->setCellValue('C'.$rowIdx, $row['name'])
                ->setCellValue('D'.$rowIdx, $row['nie'])
                ->setCellValue('E'.$rowIdx, $row['npwp_no'])
                ->setCellValue('F'.$rowIdx, $row['marital_status'])
                ->setCellValue('G'.$rowIdx, $firsMonth)
                ->setCellValue('H'.$rowIdx, $lastMonth)
                ->setCellValue('I'.$rowIdx, $numOfMonth)
                ->setCellValue('J'.$rowIdx, $bulanke)
                ->setCellValue('K'.$rowIdx, $row['basic_salary'])
                ->setCellValue('L'.$rowIdx, $row['bs_prorate'])
                ->setCellValue('M'.$rowIdx, $row['overtime'])
                ->setCellValue('N'.$rowIdx, $ot35)
                ->setCellValue('O'.$rowIdx, $row['shift_bonus'])
                ->setCellValue('P'.$rowIdx, $row['remote_allowance'])
                ->setCellValue('Q'.$rowIdx, $row['dev_incentive_bonus'])
                ->setCellValue('R'.$rowIdx, $row['unpaid_total'])
                ->setCellValue('S'.$rowIdx, $row['jkk_jkm'])
                ->setCellValue('T'.$rowIdx, $bpjsKes)
                ->setCellValue('U'.$rowIdx, $totalReg)
                ->setCellValue('V'.$rowIdx, $row['contract_bonus'])
                ->setCellValue('W'.$rowIdx, $row['thr'])
                ->setCellValue('X'.$rowIdx, $totalIreg)
                ->setCellValue('Y'.$rowIdx, $totIcome)
                ->setCellValue('Z'.$rowIdx, $incm)
                ->setCellValue('AA'.$rowIdx, $funCostIr)
                ->setCellValue('AB'.$rowIdx, $jp)
                ->setCellValue('AC'.$rowIdx, $bpjsJht)
                ->setCellValue('AD'.$rowIdx, $totDeducation)
                ->setCellValue('AE'.$rowIdx, $netIncomeReg)
                ->setCellValue('AF'.$rowIdx, $netIncomeNonReg)
                ->setCellValue('AG'.$rowIdx, $netIncomeMonth)
                ->setCellValue('AH'.$rowIdx, $prev_income)
                ->setCellValue('AI'.$rowIdx, $netInNonRegPrev)
                ->setCellValue('AJ'.$rowIdx, $netInRegYTD)
                ->setCellValue('AK'.$rowIdx, $netInNonRegYTD)
                ->setCellValue('AL'.$rowIdx, $annualisation)
                ->setCellValue('AM'.$rowIdx, $ptkpDeduction)
                ->setCellValue('AN'.$rowIdx, $taxIncome)
                ->setCellValue('AO'.$rowIdx, $taxImpRound)
                ->setCellValue('AP'.$rowIdx, $taxDueIncome)
                ->setCellValue('AQ'.$rowIdx, $taxInRegIn)
                ->setCellValue('AR'.$rowIdx, $taxImpBaseRound)
                ->setCellValue('AS'.$rowIdx, $taxDuePerAnnum)
                ->setCellValue('AT'.$rowIdx, $taxDueNonReg)
                ->setCellValue('AU'.$rowIdx, $taxAlreadyPaidNon)
                ->setCellValue('AV'.$rowIdx, $taxDueNonRegThisM)
                ->setCellValue('AW'.$rowIdx, $taxDueOnRegIncomeYTD)
                ->setCellValue('AX'.$rowIdx, $taxAlreadyPaidYTD)
                ->setCellValue('AY'.$rowIdx, $taxDueRegThisMonth)
                ->setCellValue('AZ'.$rowIdx, $totalTaxDueThisMonth)
                ->setCellValue('BA'.$rowIdx, $taxPenalty)
                ->setCellValue('BB'.$rowIdx, $taxBorneByEmployee)
                ->setCellValue('BC'.$rowIdx, $bpjsKes)
                ->setCellValue('BD'.$rowIdx, $bpjsKesEmp)
                ->setCellValue('BE'.$rowIdx, $row['jkk_jkm'])
                ->setCellValue('BF'.$rowIdx, $jp)
                ->setCellValue('BG'.$rowIdx, $bpjsJht)
                // ->setCellValue('BH'.$rowIdx, $row['emp_health_bpjs'])
                ->setCellValue('BI'.$rowIdx, $takeHomePay)
                ;

            if($rowIdx % 2 == 1)
            {
                $spreadsheet->getActiveSheet()->getStyle('A'.$rowIdx.':BI'.$rowIdx)
                    ->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB('EAEBAF');             
            } 
        $prev_income    = $netInRegYTD;
        $taxAlreadyPaidNon    = $taxDueNonReg;
        // Tax Already Paid YTD
        $taxAlreadyPaidYTD = $taxDueOnRegIncomeYTD;
        }

        $spreadsheet->getActiveSheet()
            ->setCellValue('F'.($rowIdx+2), 'TOTAL')
            ->setCellValue('BI'.($rowIdx+2), '=SUM(BI'.$startIdx.':BI'.$rowIdx.')')
            ;
        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":BI".($rowIdx+2))->getFont()->setBold(true)->setSize(12);
        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":BI".($rowIdx+2))->applyFromArray($outlineBorderStyle);

        $spreadsheet->getActiveSheet()->getStyle('K8:BI'.($rowIdx+2))->getNumberFormat()->setFormatCode('#,##0.00');       
        
        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":BI".($rowIdx+2))
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B');

        unset($allBorderStyle);
        unset($center);
        unset($right);
        unset($left);
        
        $spreadsheet->setActiveSheetIndex(0);

        $str = 'RPTTMKSumInvoice';
        $fileName = preg_replace('/\s+/', '', $str);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.Xlsx"');
        // header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        /* BY COMPOSER */
        // $writer = new Xlsx($spreadsheet);
        /* OFFLINE/ BY COPY EXCEL FOLDER  */
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit(0);   
    }

    public function exportTaxCalculationTrakindoSumbawa($clientName,$yearPeriod)
    {
        $pt     = $clientName;
        $year   = $yearPeriod;
        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();  
        $spreadsheet->getProperties()->setCreator('Maurice - Web - Android')
            ->setLastModifiedBy('Maurice - Web - Android')
            ->setTitle('Office 2007 XLSX Test Document')
            ->setSubject('Office 2007 XLSX Test Document')
            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Test result file');

        $strSQL = "";
        if($pt=='Trakindo_Sumbawa'){
        $strSQL  = "SELECT a.*,b.nie,c.npwp_no,c.bpjs_no,(ot_1+ot_2+ot_3+ot_4)overtime,a.jkk_jkm jamsostek,a.emp_health_bpjs bpjs_kes
                    FROM trn_slip_trksmb a
                    LEFT JOIN mst_salary b ON a.bio_rec_id=b.bio_rec_id
                    LEFT JOIN mst_bio_rec c ON a.bio_rec_id=c.bio_rec_id
                    WHERE a.client_name='".$pt."' AND a.year_period='".$year."' ORDER BY a.name,a.month_period ASC LIMIT 100";

        }elseif($pt='Pontil_Timika'){
        $strSQL  = "SELECT a.*,b.nie,c.npwp_no,(ot_1+ot_2+ot_3+ot_4)overtime,a.jkk_jkm jamsostek,a.emp_health_bpjs bpjs_kes
                    FROM trn_salary_slip a
                    LEFT JOIN mst_salary b ON a.bio_rec_id=b.bio_rec_id
                    LEFT JOIN mst_bio_rec c ON a.bio_rec_id=c.bio_rec_id
                    WHERE a.client_name='".$pt."' AND a.year_period='".$year."' ORDER BY a.name,a.month_period ASC LIMIT 100";

        }
        $query = $this->db->query($strSQL)->result_array();  
        $boldFont = [
            'font' => [
                'bold' => true
                // 'color' => ['argb' => '0000FF'],
            ],
        ];

        $totalStyle = [
            'font' => [
                'bold' => true,
                'color' => ['argb' => '0000FF'],
            ],
        ];

        $allBorderStyle = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $outlineBorderStyle = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $topBorderStyle = [
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $bottomBorderStyle = [
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        foreach(range('B','Q') as $columnID)
        {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }           

        // Nama Field Baris Pertama
        $spreadsheet->getActiveSheet()
                ->setCellValue('A1', 'TAX CALCULATION PT. SANGATI SOERYA SEJAHTERA')
                ->setCellValue('A2', 'Employee Income Tax Calculation Trakindo Sumbawa')
                ->setCellValue('A3', 'For the month of');

        $spreadsheet->getActiveSheet()->mergeCells("A1:F1");
        $spreadsheet->getActiveSheet()->mergeCells("A2:F2");
        $spreadsheet->getActiveSheet()->getStyle("A1:D1")->getFont()->setBold(true)->setSize(16);
        $spreadsheet->getActiveSheet()->getStyle("A2:D2")->getFont()->setBold(true)->setSize(13);
        $spreadsheet->getActiveSheet()->getStyle("A3:G5")->getFont()->setBold(true)->setSize(12);       

        /* SET HEADER BG COLOR*/
        $spreadsheet->getActiveSheet()->getStyle('A6:BK7')
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B'); 

        /* START INVOICE TITLE */
        $spreadsheet->getActiveSheet()->getStyle("A6:BK6")->getFont()->setSize(12);
        $spreadsheet->getActiveSheet()->getStyle("A6:BK7")->applyFromArray($allBorderStyle);        
        $spreadsheet->getActiveSheet()->getStyle("A6:BK7")->applyFromArray($center);

        $spreadsheet->getActiveSheet()->getStyle("B6:BK7")->getAlignment()->setWrapText(true);

        $spreadsheet->getActiveSheet()->mergeCells("A6:A7");
        $spreadsheet->getActiveSheet()->mergeCells("B6:B7");
        $spreadsheet->getActiveSheet()->mergeCells("C6:C7");
        $spreadsheet->getActiveSheet()->mergeCells("D6:D7");
        $spreadsheet->getActiveSheet()->mergeCells("E6:E7");
        $spreadsheet->getActiveSheet()->mergeCells("F6:F7");
        $spreadsheet->getActiveSheet()->mergeCells("G6:G7");
        $spreadsheet->getActiveSheet()->mergeCells("H6:H7");
        $spreadsheet->getActiveSheet()->mergeCells("I6:I7");
        $spreadsheet->getActiveSheet()->mergeCells("J6:J7");
        $spreadsheet->getActiveSheet()->mergeCells("K6:K7");
        $spreadsheet->getActiveSheet()->mergeCells("L6:L7");
        $spreadsheet->getActiveSheet()->mergeCells("M6:T6");
        $spreadsheet->getActiveSheet()->mergeCells("U6:U7");
        $spreadsheet->getActiveSheet()->mergeCells("V6:V7");
        $spreadsheet->getActiveSheet()->mergeCells("W6:W7");
        $spreadsheet->getActiveSheet()->mergeCells("X6:Y6");
        $spreadsheet->getActiveSheet()->mergeCells("Z6:Z7");
        $spreadsheet->getActiveSheet()->mergeCells("AA6:AA7");
        $spreadsheet->getActiveSheet()->mergeCells("AB6:AB7");
        $spreadsheet->getActiveSheet()->mergeCells("AC6:AC7");
        $spreadsheet->getActiveSheet()->mergeCells("AD6:AD7");
        $spreadsheet->getActiveSheet()->mergeCells("AE6:AE7");
        $spreadsheet->getActiveSheet()->mergeCells("AF6:AF7");
        $spreadsheet->getActiveSheet()->mergeCells("AG6:AG7");
        $spreadsheet->getActiveSheet()->mergeCells("AH6:AH7");
        $spreadsheet->getActiveSheet()->mergeCells("AI6:AI7");
        $spreadsheet->getActiveSheet()->mergeCells("AJ6:AJ7");
        $spreadsheet->getActiveSheet()->mergeCells("AK6:AK7");
        $spreadsheet->getActiveSheet()->mergeCells("AL6:AL7");
        $spreadsheet->getActiveSheet()->mergeCells("AM6:AM7");
        $spreadsheet->getActiveSheet()->mergeCells("AN6:AN7");
        $spreadsheet->getActiveSheet()->mergeCells("AO6:AO7");
        $spreadsheet->getActiveSheet()->mergeCells("AP6:AP7");
        $spreadsheet->getActiveSheet()->mergeCells("AQ6:AQ7");
        $spreadsheet->getActiveSheet()->mergeCells("AR6:AR7");
        $spreadsheet->getActiveSheet()->mergeCells("AS6:AS7");
        $spreadsheet->getActiveSheet()->mergeCells("AT6:AT7");
        $spreadsheet->getActiveSheet()->mergeCells("AU6:AU7");
        $spreadsheet->getActiveSheet()->mergeCells("AV6:AV7");
        $spreadsheet->getActiveSheet()->mergeCells("AW6:AW7");
        $spreadsheet->getActiveSheet()->mergeCells("AX6:AX7");
        $spreadsheet->getActiveSheet()->mergeCells("AY6:AY7");
        $spreadsheet->getActiveSheet()->mergeCells("AZ6:AZ7");
        $spreadsheet->getActiveSheet()->mergeCells("BA6:BA7");
        $spreadsheet->getActiveSheet()->mergeCells("BB6:BB7");
        $spreadsheet->getActiveSheet()->mergeCells("BC6:BC7");
        $spreadsheet->getActiveSheet()->mergeCells("BD6:BD7");
        $spreadsheet->getActiveSheet()->mergeCells("BE6:BE7");
        $spreadsheet->getActiveSheet()->mergeCells("BF6:BF7");
        $spreadsheet->getActiveSheet()->mergeCells("BG6:BG7");
        $spreadsheet->getActiveSheet()->mergeCells("BH6:BH7");
        $spreadsheet->getActiveSheet()->mergeCells("BI6:BI7");
        $spreadsheet->getActiveSheet()->mergeCells("BJ6:BJ7");
        $spreadsheet->getActiveSheet()->mergeCells("BK6:BK7");
        // $spreadsheet->getActiveSheet()->mergeCells("BL6:BL7");

        $spreadsheet->getActiveSheet()
                ->setCellValue('A6', 'NO')
                ->setCellValue('B6', 'Periode Cut Off Timesheet')
                ->setCellValue('C6', 'Name')
                ->setCellValue('D6', 'EMPL Code')
                ->setCellValue('E6', 'Nomor NPWP')
                ->setCellValue('F6', 'Status BPJS Kesehatan')
                ->setCellValue('G6', 'Status')
                ->setCellValue('H6', 'First Month')
                ->setCellValue('I6', 'Last Month')
                ->setCellValue('J6', 'Number Of Month')
                ->setCellValue('K6', 'Bulan Ke')
                ->setCellValue('L6', 'Basic Salary')
                ->setCellValue('M6', 'Reguler Income')

                ->setCellValue('U6', 'BPJS JKK & JKM 2,04%')
                ->setCellValue('V6', 'BPJS Kesehatan 4%')
                ->setCellValue('W6', 'Total Reguler Income')
                ->setCellValue('X6', 'Non-Reguler Income')

                ->setCellValue('Z6', 'Total Non-Reguler Income')
                ->setCellValue('AA6', 'Total Income This Month IDR')
                ->setCellValue('AB6', 'Functional Cost')
                ->setCellValue('AC6', 'Functional Cost On Bonus')
                ->setCellValue('AD6', 'BPJS Pensiun (JP) 1%')
                ->setCellValue('AE6', 'BPJS TK(JHT) 2%')
                ->setCellValue('AF6', 'Total Deduction')
                ->setCellValue('AG6', 'Net Income On Reg Income')
                ->setCellValue('AH6', 'Net Income On Non-Reg Income')
                ->setCellValue('AI6', 'Net Income This month')
                ->setCellValue('AJ6', 'Net Income Reg Previously')
                ->setCellValue('AK6', 'Net Income Non-Reg Previously')
                ->setCellValue('AL6', 'Net Income Reg YTD')
                ->setCellValue('AM6', 'Net Income Non-Reg YTD')
                ->setCellValue('AN6', 'Annualisation')
                ->setCellValue('AO6', 'PTKP Deduction')
                ->setCellValue('AP6', 'Taxable Income')
                ->setCellValue('AQ6', 'Tax Imposition Base On Reg Income (Rounding)')
                ->setCellValue('AR6', 'Tax Due Income Per Annum')
                ->setCellValue('AS6', 'Taxable Income Reg Income')
                ->setCellValue('AT6', 'Tax Imposition Base On Reg Income (Rounding)')
                ->setCellValue('AU6', 'Tax Due Per Annum Reg Income ')
                ->setCellValue('AV6', 'Tax Due Non Reg Income YTD')
                ->setCellValue('AW6', 'Tax Already Paid Non Reg YTD')
                ->setCellValue('AX6', 'Tax Due Non Reg This Month')
                ->setCellValue('AY6', 'Tax Due On Regular Income YTD')
                ->setCellValue('AZ6', 'Tax Already Paid YTD')
                ->setCellValue('BA6', 'Tax Due Reg This Month')
                ->setCellValue('BB6', 'Total Tax Due This Month')
                ->setCellValue('BC6', 'Tax Penalty')
                ->setCellValue('BD6', 'Tax Borne By Employee')
                ->setCellValue('BE6', 'BPJS 4%')
                ->setCellValue('BF6', 'BPJS Kes. (empl.contr.) 1%')
                ->setCellValue('BG6', 'BPJS Pensiun 1%')
                ->setCellValue('BH6', 'JKK & JKM 2,04%')
                ->setCellValue('BI6', 'JHT 2%')
                ->setCellValue('BJ6', 'Gaji / Bonus yg Sudah Di Bayar')
                ->setCellValue('BK6', 'Take Home Pay')
                ;

        $spreadsheet->getActiveSheet()
                ->setCellValue('M7', 'Salary IDR')
                ->setCellValue('N7', 'Absensi')
                ->setCellValue('O7', 'Out Camp')
                ->setCellValue('P7', 'Bonus Safety')
                ->setCellValue('Q7', 'Kehadiran SSS')
                ->setCellValue('R7', 'Kehadiran Trakindo')
                ->setCellValue('S7', 'Tunjangan Kerja Malam')
                ->setCellValue('T7', 'Overtime')
                ->setCellValue('X7', 'THR')
                ->setCellValue('Y7', 'Contract Bonus')
                ;

        /* START TOTAL WORK HOUR */

        $rowIdx             = 7;
        $startIdx           = $rowIdx; 
        $rowNo              = 1;
        $monthNo            = 1;
        $no                 = '';
        $bulanke            = '0';
        $loopIDnya          = '';
        $loopID             = '';
        $tmbhth             = '';
        $prev_income        = 0;
        $taxAlreadyPaidNon  = '';
        $taxAlreadyPaidYTD  = '';
        foreach ($query as $row) {
            $tahunDB    = $row['year_period']-1;
            $tmbhth     = $row['year_period'];
            $cutPeriod  = '';
            $tanggal    = $row['month_period'];
            $namabulan  = $this->namabulan($tanggal);
            $bulan      = $this->balikbulan($tanggal);
            $tanggal1   = $row['month_period']-1;
            $namabulan1 = $this->namabulan($tanggal1);
            $status     = $row['marital_status'];
            // $tahundepanDB = $row['year_period']+1;

            if ($row['bio_rec_id'] != $loopID){
                $prev_income        = '';
                $taxAlreadyPaidNon  = '';
                $taxAlreadyPaidYTD  = '';
                $loopIDnya          = $row['bio_rec_id'];
                $namabulanperiode   = $namabulan;
                $netInRegPrev       = '';
                $netInNonRegPrev    = '';
                $numOfMonth         = $bulan;
                $bulanke            = 1;
                $no                 = $rowNo++;
                if ($pt == 'Trakindo_Sumbawa'){
                    $cutPeriod = '19 '.$namabulan1.' '.$tahunDB.' - 18 '.$namabulan.' '.$tmbhth.' ';
                }
                else{
                    $cutPeriod = '';
                }
            }
            else{
                $loopIDnya          = $loopID;
                $namabulanperiode   = $namabulanperiode;
                $numOfMonth         = $numOfMonth;
                $taxAlreadyPaidNon  = $taxDueNonReg;
                $no                 = '';
                $bulanke            = $bulanke+1;
                $netInRegPrev       = $netInRegYTD;
                $netInNonRegPrev    = $netInNonRegYTD;
                if ($pt == 'Trakindo_Sumbawa'){
                    $cutPeriod = '19 '.$namabulan1.' '.$tmbhth.' - 18 '.$namabulan.' '.$tmbhth.' ';
                }
                else{
                    $cutPeriod = '';
                }
            }
            $loopID         = $row['bio_rec_id'];

            $nobpjskes      = $row['bpjs_no'];
            $bpjsKes        = '';
            $bpjsKesEmp     = '';
            if(is_numeric($nobpjskes) || $nobpjskes !=0 ){
                $nobpjskes      = 'Ya';
                if($row['basic_salary']<12000000){
                    $bpjsKes = $row['basic_salary'] * 4/100;
                }else{
                    $bpjsKes = 12000000 * 4/100;
                }

                // BPJS Kes. (empl.contr.) 1%
                if($row['basic_salary']<12000000){
                    $bpjsKesEmp = $row['basic_salary'] * 1/100;
                }else{
                    $bpjsKesEmp = 12000000 * 1/100;
                }
            }

            else {
                $nobpjskes      = 'Tidak';
                $bpjsKes        = '';
                $bpjsKesEmp     = '';
            }

            // BPJS jht
            $bpjsJht = $row['basic_salary'] *2/100;

            // $tmbhth    = $row['year_period'];
            $rowIdx++;
            $maxincome      = 500000;
            $incm = 0;
            $totalReg       = $row['bs_prorate'] + $row['overtime'] + $row['flying_camp'] + $row['attendance_bonus'] + $row['other_allowance1'] + $row['safety_bonus'] + $row['night_shift_bonus'] + $bpjsKes + $row['jkk_jkm'] - $row['unpaid_total'];
            $totalIreg      = $row['contract_bonus'] + $row['thr'];
           
            // Total Income This Month
            $totIcome       = $totalReg+$totalIreg;

            // Functional Cost On Reg Income
            if($totalReg*5/100 < $maxincome){
                $incm       = $totalReg * 5/100;
            }
            else{
                $incm       = $maxincome;
            }

            // Functional Cost Ireguler
            $funCostIr      = 0;
            if($totalReg*5/100 > $maxincome){
                $funCostIr  = 0;
            }
            elseif ($totIcome*5/100 > $maxincome){
                $funCostIr  = $maxincome - $incm;
            }
            else{
                $funCostIr  = $funCostIr;
            }

            // BPJS Pensiun (JP) 1%
            $jp             = 0;
            if($row['basic_salary'] < 8512400){
                $jp         = $row['basic_salary'] * 1/100;
            }
            else{
                $jp         = $row['basic_salary'] * 1/100;
            }

            // Total Deduction
            $totDeducation  = $incm + $funCostIr + $jp + $bpjsJht;

            // Net Income On Reg Income
            $netIncomeReg   = $totalReg - $incm - $jp - $bpjsJht;

            // Net Income On Non-Reg Income
            $netIncomeNonReg = $totalIreg - $funCostIr;

            // Net Income This month
            $netIncomeMonth = $netIncomeReg + $netIncomeNonReg;

            // Net Income Reg Previously
            // Net Income Non-Reg Previously
            // Net Income Reg YTD
            // Net Income Non-Reg YTD
            
            $netInRegYTD    = $netIncomeReg + $prev_income;
            $netInNonRegYTD = $netIncomeNonReg + $netInNonRegPrev;


            $lastMonth      = '';
            $firsMonth      = '';
            $tmbhth         = substr($tmbhth,2,2);
            $lastMonth      = ''.$namabulan.' '.$tmbhth.'';
            $firsMonth      = ''.$namabulanperiode.' '.$tmbhth.'';

            // Annualisation
            $annualisation  = $netInRegYTD*($numOfMonth/$bulanke)+$netInNonRegYTD;

            // PTKP Deduction 
            $ptkpDeduction  = '';
            $statstr        = substr($status,1,1);
            $statstr2       = substr($status,0,2);
            if ($statstr2 == 'TK'){
                $ptkpDeduction = 54000000;
            }
            else{
                if ($statstr == 0){
                    $ptkpDeduction = 58500000;
                }
                elseif ($statstr == 1){
                    $ptkpDeduction = 63000000;
                }
                elseif ($statstr == 2){
                    $ptkpDeduction = 67500000;
                }
                else{
                    $ptkpDeduction = 72000000;
                }
            }
            
            // if =IF(AO12<AP12;0;AO12-AP12) Taxable Income
            $taxIncome      = '';
            if($annualisation<$ptkpDeduction){
                $taxIncome  = 0;
            }
            else{
                $taxIncome  = $annualisation - $ptkpDeduction;
            }

            // Tax Imposition (Rounding)
            $taxImpRound    = '';
            if ($taxIncome > 0){
                $taxImpRound = round($taxIncome/1000-0.5,0)*1000;
            }
            else{
                $taxImpRound = 0;
            }

            // Tax Due Income Per Annum
            $taxDueIncome   = '';
            if ($taxImpRound>500000000){
                $taxDueIncome = (0.3*$taxImpRound)-55000000;
            }
            elseif($taxImpRound>250000000){
                $taxDueIncome = (0.25*$taxImpRound)-30000000;
            }
            elseif($taxImpRound>50000000){
                $taxDueIncome = (0.15*$taxImpRound)-5000000;
            }
            else{
                $taxDueIncome = $taxImpRound*0.05;
            }

            // Taxable Income Reg Income
            $taxInRegIn     = '';
            if($annualisation-$netInNonRegYTD-$ptkpDeduction<0){
                $taxInRegIn = 0;
            }
            else{
                $taxInRegIn = $annualisation-$netInNonRegYTD-$ptkpDeduction;
            }
            // Tax Imposition Base On Reg Income (Rounding)
            $taxImpBaseRound = '';
            if ($taxInRegIn > 0){
                $taxImpBaseRound = round($taxInRegIn/1000-0.5,0)*1000;
            }
            else{
                $taxImpBaseRound = 0;
            }

            // Tax Due Per Annum Reg Income 
            $taxDuePerAnnum = '';
            if($taxImpBaseRound>500000000){
                $taxDuePerAnnum = (0.3*$taxImpBaseRound)-55000000;
            }
            elseif($taxImpBaseRound>250000000){
                $taxDuePerAnnum = (0.25*$taxImpBaseRound)-30000000;
            }
            elseif($taxImpBaseRound>50000000){
                $taxDuePerAnnum = (0.15*$taxImpBaseRound)-5000000;
            }
            else{
                $taxDuePerAnnum = $taxImpBaseRound*0.05;
            }

            // Tax Due Non Reg Income YTD
            $taxDueNonReg    = 0;
            if($netInNonRegYTD<=0){
                $taxDueNonReg = $taxAlreadyPaidNon;
            }
            else{
                $taxDueNonReg = $taxDueIncome - $taxDuePerAnnum;
            }

            // Tax Due Non Reg This Month
            $taxDueNonRegThisM = $taxDueNonReg - $taxAlreadyPaidNon;
            // Tax Due On Regular Income YTD
            $taxDueOnRegIncomeYTD = $taxDuePerAnnum/($numOfMonth/$bulanke);

            // Tax Due Reg This Month
            $taxDueRegThisMonth = $taxDueOnRegIncomeYTD - $taxAlreadyPaidYTD;

            // Total Tax Due This Month
            $totalTaxDueThisMonth = $taxDueRegThisMonth + $taxDueNonRegThisM;

            // Tax Penalty
            $taxPenalty     = 0;
            if ($row['npwp_no'] == 0){
                $taxPenalty = $totalTaxDueThisMonth * 20/100;
            }
            else{
                $taxPenalty = 0;
            }

            // Tax Borne By Employee
            $taxBorneByEmployee = $totalTaxDueThisMonth + $taxPenalty;

            // JKK JKM 2,04%
            $jkkjkmAwal = $row['basic_salary'] ;

            // Gaji Sudah Di Bayar
            $gajiSudahDiBayar = '';

            // Take Home Pay
            $takeHomePay = $totIcome-($taxBorneByEmployee+$bpjsKes+$bpjsKesEmp+$row['jkk_jkm']+$jp+$bpjsJht+$gajiSudahDiBayar);    
            // $chargeTotal = $gross + $row['uplift'];
            $spreadsheet->getActiveSheet()
                ->setCellValue('A'.$rowIdx, $no)
                ->setCellValue('B'.$rowIdx, $cutPeriod)
                ->setCellValue('C'.$rowIdx, $row['name'])
                ->setCellValue('D'.$rowIdx, $row['nie'])
                ->setCellValue('E'.$rowIdx, $row['npwp_no'])
                ->setCellValue('F'.$rowIdx, $nobpjskes)
                ->setCellValue('G'.$rowIdx, $row['marital_status'])
                ->setCellValue('H'.$rowIdx, $firsMonth)
                ->setCellValue('I'.$rowIdx, $lastMonth)
                ->setCellValue('J'.$rowIdx, $numOfMonth)
                ->setCellValue('K'.$rowIdx, $bulanke)
                ->setCellValue('L'.$rowIdx, $row['basic_salary'])

                ->setCellValue('M'.$rowIdx, $row['bs_prorate'])
                ->setCellValue('N'.$rowIdx, $row['unpaid_total'])
                ->setCellValue('O'.$rowIdx, $row['flying_camp'])
                ->setCellValue('P'.$rowIdx, $row['safety_bonus'])
                ->setCellValue('Q'.$rowIdx, $row['attendance_bonus'])
                ->setCellValue('R'.$rowIdx, $row['other_allowance1'])
                ->setCellValue('S'.$rowIdx, $row['night_shift_bonus'])
                ->setCellValue('T'.$rowIdx, $row['overtime'])
                ->setCellValue('U'.$rowIdx, $row['jkk_jkm'])
                ->setCellValue('V'.$rowIdx, $bpjsKes)

                ->setCellValue('W'.$rowIdx, $totalReg)
                ->setCellValue('X'.$rowIdx, $row['thr'])
                ->setCellValue('Y'.$rowIdx, $row['contract_bonus'])
                ->setCellValue('Z'.$rowIdx, $totalIreg)
                ->setCellValue('AA'.$rowIdx, $totIcome)
                ->setCellValue('AB'.$rowIdx, $incm)
                ->setCellValue('AC'.$rowIdx, $funCostIr)
                ->setCellValue('AD'.$rowIdx, $jp)
                ->setCellValue('AE'.$rowIdx, $bpjsJht)
                ->setCellValue('AF'.$rowIdx, $totDeducation)
                ->setCellValue('AG'.$rowIdx, $netIncomeReg)
                ->setCellValue('AH'.$rowIdx, $netIncomeNonReg)
                ->setCellValue('AI'.$rowIdx, $netIncomeMonth)
                ->setCellValue('AJ'.$rowIdx, $prev_income)
                ->setCellValue('AK'.$rowIdx, $netInNonRegPrev)
                ->setCellValue('AL'.$rowIdx, $netInRegYTD)
                ->setCellValue('AM'.$rowIdx, $netInNonRegYTD)
                ->setCellValue('AN'.$rowIdx, $annualisation)
                ->setCellValue('AO'.$rowIdx, $ptkpDeduction)
                ->setCellValue('AP'.$rowIdx, $taxIncome)
                ->setCellValue('AQ'.$rowIdx, $taxImpRound)
                ->setCellValue('AR'.$rowIdx, $taxDueIncome)
                ->setCellValue('AS'.$rowIdx, $taxInRegIn)
                ->setCellValue('AT'.$rowIdx, $taxImpBaseRound)
                ->setCellValue('AU'.$rowIdx, $taxDuePerAnnum)
                ->setCellValue('AV'.$rowIdx, $taxDueNonReg)
                ->setCellValue('AW'.$rowIdx, $taxAlreadyPaidNon)
                ->setCellValue('AX'.$rowIdx, $taxDueNonRegThisM)
                ->setCellValue('AY'.$rowIdx, $taxDueOnRegIncomeYTD)
                ->setCellValue('AZ'.$rowIdx, $taxAlreadyPaidYTD)
                ->setCellValue('BA'.$rowIdx, $taxDueRegThisMonth)
                ->setCellValue('BB'.$rowIdx, $totalTaxDueThisMonth)
                ->setCellValue('BC'.$rowIdx, $taxPenalty)
                ->setCellValue('BD'.$rowIdx, $taxBorneByEmployee)
                ->setCellValue('BE'.$rowIdx, $bpjsKes)
                ->setCellValue('BF'.$rowIdx, $bpjsKesEmp)
                ->setCellValue('BG'.$rowIdx, $jp)
                ->setCellValue('BH'.$rowIdx, $row['jkk_jkm'])
                ->setCellValue('BI'.$rowIdx, $bpjsJht)
                // ->setCellValue('BJ'.$rowIdx, $row['emp_health_bpjs'])
                ->setCellValue('BK'.$rowIdx, $takeHomePay)
                ;

            if($rowIdx % 2 == 1)
            {
                $spreadsheet->getActiveSheet()->getStyle('A'.$rowIdx.':BK'.$rowIdx)
                    ->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB('EAEBAF');             
            } 
        $prev_income    = $netInRegYTD;
        $taxAlreadyPaidNon    = $taxDueNonReg;
        // Tax Already Paid YTD
        $taxAlreadyPaidYTD = $taxDueOnRegIncomeYTD;
        }

        $spreadsheet->getActiveSheet()
            ->setCellValue('F'.($rowIdx+2), 'TOTAL')
            ->setCellValue('BK'.($rowIdx+2), '=SUM(BK'.$startIdx.':BK'.$rowIdx.')')
            ;
        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":BK".($rowIdx+2))->getFont()->setBold(true)->setSize(12);
        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":BK".($rowIdx+2))->applyFromArray($outlineBorderStyle);

        $spreadsheet->getActiveSheet()->getStyle('K8:BK'.($rowIdx+2))->getNumberFormat()->setFormatCode('#,##0.00');       
        
        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":BK".($rowIdx+2))
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B');

        unset($allBorderStyle);
        unset($center);
        unset($right);
        unset($left);
        
        $spreadsheet->setActiveSheetIndex(0);

        $str = 'RPTTRKSumInvoice';
        $fileName = preg_replace('/\s+/', '', $str);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.Xlsx"');
        // header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        /* BY COMPOSER */
        // $writer = new Xlsx($spreadsheet);
        /* OFFLINE/ BY COPY EXCEL FOLDER  */
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit(0);   
    }
}
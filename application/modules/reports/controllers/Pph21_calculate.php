<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

    // Load library phpspreadsheet
    require('./vendor/autoload.php');
    use PhpOffice\PhpSpreadsheet\Helper\Sample;
    use PhpOffice\PhpSpreadsheet\IOFactory;
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    // End load library phpspreadsheet

class Pph21_calculate extends CI_Controller
{
	public function __construct(){
        parent::__construct();

        $this->load->model('transactions/M_salary_slip'); 
        $this->load->model('masters/M_payroll_config'); 
    }

    public function getPph21CalculationView()
    {
        $pt             = $this->input->post('pt');
        $year           = $this->input->post('year');
        $dept           = $this->input->post('dept');
        $payrollGroup   = $this->input->post('payrollGroup');

        if($pt == 'Pontil_Timika'){
        $query  = "SELECT a.salary_slip_id,a.client_name,a.dept,a.position,a.bio_rec_id,a.name,b.nie,c.npwp_no,a.marital_status,a.month_period,a.year_period,
                    a.bs_prorate,(ot_1+ot_2+ot_3+ot_4)overtime,a.jkk_jkm jamsostek,a.emp_health_bpjs bpjs_kes,a.travel_bonus,a.allowance_economy,a.incentive_bonus,
                    a.shift_bonus,a.unpaid_total
                    FROM trn_salary_slip a
                    LEFT JOIN mst_salary b ON a.bio_rec_id=b.bio_rec_id
                    LEFT JOIN mst_bio_rec c ON a.bio_rec_id=c.bio_rec_id
                    WHERE a.client_name='".$pt."' AND a.year_period='".$year."' AND a.payroll_group ='".$payrollGroup."'";

        }

        $query = $this->db->query($query)->result_array();
        $myData = array();
        foreach ($query as $key => $row) {
            $myData[] = array(
                $row['salary_slip_id'],         
                $row['client_name'],         
                $row['name'],             
                $row['dept'],         
                $row['position']
            );            
        }  
        echo json_encode($myData);  
    }

    function namabulan($tanggal){
        // $tgl=substr($tanggal,8,2);
        // $bln=substr($tanggal,5,2);
        // $thn=substr($tanggal,0,4);
        // $info=date('w', mktime(0,0,0,$bln,$tgl,$thn));
        switch($tanggal){
            case '1': return "Jan"; break;
            case '2': return "Feb"; break;
            case '3': return "Mar"; break;
            case '4': return "Apr"; break;
            case '5': return "Mei"; break;
            case '6': return "Jun"; break;
            case '7': return "Jul"; break;
            case '8': return "Agu"; break;
            case '9': return "Sep"; break;
            case '10': return "Okt"; break;
            case '11': return "Nov"; break;
            case '12': return "Des"; break;
            case '0': return "Des"; break;
        };
    }

    function balikbulan($bulan){
        switch($bulan){
            case '1': return "12"; break;
            case '2': return "11"; break;
            case '3': return "10"; break;
            case '4': return "9"; break;
            case '5': return "8"; break;
            case '6': return "7"; break;
            case '7': return "6"; break;
            case '8': return "5"; break;
            case '9': return "4"; break;
            case '10': return "3"; break;
            case '11': return "2"; break;
            case '12': return "1"; break;
        };
    }


    public function exportTaxCalculationPontilTimika($clientName,$yearPeriod,$payrollGroup,$dept)
    {
        // test()
        $pt     = $clientName;
        $year   = $yearPeriod;
        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();  
        $spreadsheet->getProperties()->setCreator('Maurice - Web - Android')
            ->setLastModifiedBy('Maurice - Web - Android')
            ->setTitle('Office 2007 XLSX Test Document')
            ->setSubject('Office 2007 XLSX Test Document')
            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Test result file');

        $strSQL = "";
        if($pt=='Pontil_Timika'){
        $strSQL  =" SELECT a.*,b.nie,c.full_name,c.npwp_no, c.id_card_address,c.gender,c.marital_status,
                    d.contract_start,d.contract_end,c.nationality,SUM(a.contract_bonus+a.thr) bonus_thr,SUM( 
                                        bs_prorate + ot_1 + ot_2 + ot_3 + ot_4 + 
                                        flying_camp + attendance_bonus + other_allowance2+ 
                                        thr+ contract_bonus+ safety_bonus+ night_shift_bonus
                                        + jkk_jkm + health_bpjs- unpaid_total+ adjust_in- 
                                        adjust_out ) brutto_tax ,c.position,a.ot_bonus
                    FROM trn_salary_slip a
                    LEFT JOIN mst_salary b ON a.bio_rec_id=b.bio_rec_id
                    LEFT JOIN mst_bio_rec c ON a.bio_rec_id=c.bio_rec_id
                    LEFT JOIN mst_contract d ON a.bio_rec_id=d.bio_rec_id
                    WHERE a.client_name='".$pt."' AND a.year_period='".$year."' AND a.payroll_group ='".$payrollGroup."' 
                    GROUP BY a.name,d.contract_start ASC ";

        }

        // test($strSQL,1);
        $query = $this->db->query($strSQL)->result_array();  
        
        $boldFont = [
            'font' => [
                'bold' => true
                // 'color' => ['argb' => '0000FF'],
            ],
        ];

        $totalStyle = [
            'font' => [
                'bold' => true,
                'color' => ['argb' => '0000FF'],
            ],
        ];

        $allBorderStyle = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $outlineBorderStyle = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $topBorderStyle = [
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $bottomBorderStyle = [
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        // foreach(range('B','Q') as $columnID)
        // {
        //     $spreadsheet->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        // }           

         // Nama Field Baris Pertama
        $spreadsheet->getActiveSheet()
                ->setCellValue('A1', 'PPH 21 CALCULATION PT. SANGATI SOERYA SEJAHTERA - ' .$clientName)
                ->setCellValue('A2', 'PERIOD : '.$yearPeriod)
                ->setCellValue('A3', 'DATE        : ')
                ->setCellValue('A4', 'PAYROLL GROUP : '.$payrollGroup);

        $spreadsheet->getActiveSheet()->mergeCells("A1:J1");
        $spreadsheet->getActiveSheet()->mergeCells("A2:F2");
        $spreadsheet->getActiveSheet()->getStyle("A1:D1")->getFont()->setBold(true)->setSize(16);
        $spreadsheet->getActiveSheet()->getStyle("A2:D2")->getFont()->setBold(true)->setSize(13);
        $spreadsheet->getActiveSheet()->getStyle("A3:G5")->getFont()->setBold(true)->setSize(12);
        $spreadsheet->getActiveSheet()->getStyle("A4:G5")->getFont()->setBold(true)->setSize(12);

        /* SET HEADER BG COLOR*/
        $spreadsheet->getActiveSheet()->getStyle('A6:CA8')
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B'); 

        // $spreadsheet->getActiveSheet()->getStyle('A6:AC7')
        //         ->getFill()
        //         ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        //         ->getStartColor()
        //         ->setRGB('F2BE6B');
        // $spreadsheet->getActiveSheet()->getStyle('A6:AC7')
        //         ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLUE); 

        /* START INVOICE TITLE */
        $spreadsheet->getActiveSheet()->getStyle("A6:H6")->getFont()->setSize(12);
        $spreadsheet->getActiveSheet()->getStyle("A6:H6")->applyFromArray($outlineBorderStyle);
        /* START TITLE NO */
        $titleRowIdx = 6;
        $titleColIdx = 0;

        $spreadsheet->getActiveSheet()
                ->setCellValue('A6', 'NO')
                ->setCellValue('B6', 'PERIODE CUT OFF')
                ->setCellValue('C6', 'NAME')
                ->setCellValue('D6', 'NIE')
                ->setCellValue('E6', 'NPWP NO')
                ->setCellValue('F6', 'ADDRESS')
                ->setCellValue('G6', 'JABATAN')
                ->setCellValue('H6', 'GENDER')
                ->setCellValue('I6', 'STATUS KAWIN')
                ->setCellValue('J6', 'NATIONALITY')
                ->setCellValue('K6', 'START KERJA')
                ->setCellValue('L6', 'END KERJA')
                ->setCellValue('M6', 'KETERANGAN')
                ->setCellValue('N6', 'MULAI KERJA TH INI DITEMPAT SEBELUMNYA')
                ->setCellValue('O6', 'MK PERHITUNGAN')
                ->setCellValue('P6', 'MASA KERJA')
                ->setCellValue('Q6', 'FOREIGN CURRENCY')
                ->setCellValue('R6', 'GAJI')
                ->setCellValue('S6', 'RAPEL')
                ->setCellValue('T6', 'TUNJANGAN PAJAK 1')
                ->setCellValue('U6', 'TUNJANGAN LAIN LEMBUR')
                ->setCellValue('V6', 'INCENTIVE BONUS')
                ->setCellValue('W6', 'OTHER INCOME')
                ->setCellValue('X6', 'COP/ALLOWANCE')
                ->setCellValue('Y6', 'ASSURANSI DITANGGUNG PERUSAHAAN')
                ->setCellValue('Z6', 'NATURA/ASS. KESEHATAN (BPJS)')
                ->setCellValue('AA6', 'JUMLAH BL INI')
                ->setCellValue('AB6', 'PENGHASILAN TERATUR CABANG SEBELUMNYA')
                ->setCellValue('AC6', 'PH TERATUR CABANG')
                ->setCellValue('AD6', 'JUMLAH S.D BLN INI')
                ->setCellValue('AE6', 'JUMLAH PH TERATUR SETAHUN')
                ->setCellValue('AF6', 'WLB BONUS')
                ->setCellValue('AG6', 'KOMISI')
                ->setCellValue('AH6', 'BONUS, THR')
                ->setCellValue('AI6', 'TUNJANGAN PAJAK 2')
                ->setCellValue('AJ6', 'JUMLAH BL INI')
                ->setCellValue('AK6', 'JUMLAH S.D BL INI')
                ->setCellValue('AL6', 'PH BRUTO SETAHUN')
                ->setCellValue('AM6', 'B.JAB TERATUR')
                ->setCellValue('AN6', 'B.JAB TIDAK TERATUR')
                ->setCellValue('AO6', 'BPJS KESEHATAN')
                ->setCellValue('AP6', 'BPJS PENSIUN')
                ->setCellValue('AQ6', 'BIAYA PENSIUN')
                ->setCellValue('AR6', 'BIAYA PENSIUN PINDAHAN')
                ->setCellValue('AS6', 'PENSIUN PINDAHAN S.D BLN INI')
                ->setCellValue('AT6', 'PENSIUN 1 TAHUN')
                ->setCellValue('AU6', 'JUMLAH PENGURANG')
                ->setCellValue('AV6', 'PH NETO CAB LAMA')
                ->setCellValue('AW6', 'PH NETO 1 TAHUN')
                ->setCellValue('AX6', 'PTKP')
                ->setCellValue('AY6', 'PKP')
                ->setCellValue('AZ6', 'PKP PEMBULATAN')
                ->setCellValue('BA6', 'PPH 1 TH')
                ->setCellValue('BB6', 'PH NETTO')
                ->setCellValue('BC6', 'PKP TERATUR')
                ->setCellValue('BD6', 'PKP BULAT')
                ->setCellValue('BE6', '21 TERATUR 1 TH')
                ->setCellValue('BF6', '21 TERATUR S.D BLN INI')
                ->setCellValue('BG6', '21 TERATUR BLN INI')
                ->setCellValue('BH6', '21 TIDAK TERATUR')
                ->setCellValue('BI6', 'IRREGULAR INCOME TAX')
                ->setCellValue('BJ6', '21 BLN INI')
                ->setCellValue('BK6', 'TOTAL PPH21 S.D BLN INI')
                ->setCellValue('BL6', 'OT HOURS')
                ->setCellValue('BM6', 'TAX BY GOVERENMENT')
                ->setCellValue('BN6', 'JAMSOSTEK BY COMPANY')
                ->setCellValue('BO6', 'TOTAL PAYMENT TO JAMSOSTEK')
                ->setCellValue('BP6', 'AKSES BPJS')
                ->setCellValue('BQ6', 'BPJS PENSIUN')
                ->setCellValue('BR6', 'BPJS 0.5%')
                ->setCellValue('BS6', 'MEDICAL')
                ->setCellValue('BT6', 'TRANSPORT REIMBURSEMENT')
                ->setCellValue('BU6', 'OTHER INCOME (NET)')
                ->setCellValue('BV6', 'SEVERANCE PAY')
                ->setCellValue('BW6', 'THP-IDR')
                ->setCellValue('BX6', 'TOTAL HARI KERJA')
                ->setCellValue('BY6', 'COUNT')
                ->setCellValue('BZ6', 'JAMSOSTEK 2%')
                ->setCellValue('CA6', 'JAMSOSTEK 3.7%');

        $spreadsheet->getActiveSheet()
                // ->setCellValue('A8', 'A')
                // ->setCellValue('B8', 'B')
                // ->setCellValue('C8', 'C')
                // ->setCellValue('D8', 'D')
                // ->setCellValue('E8', 'E')
                // ->setCellValue('F8', 'F')
                // ->setCellValue('G8', 'G')
                // ->setCellValue('H8', 'H ')
                // ->setCellValue('I8', 'I ')
                // ->setCellValue('J8', 'J')
                // ->setCellValue('K8', 'K')
                // ->setCellValue('L8', 'L ')
                // ->setCellValue('M8', 'M')
                // ->setCellValue('N8', '(K6+1-M6)')
                // // ->setCellValue('O8', '15')
                // // ->setCellValue('P8', '16')
                // // ->setCellValue('Q8', '17')
                // // ->setCellValue('R8', '18')
                // // ->setCellValue('S8', '19')
                // // ->setCellValue('T8', '20')
                // // ->setCellValue('U8', '21')
                // // ->setCellValue('V8', '21')
                // // ->setCellValue('W8', '21')
                // ->setCellValue('X8', '((Q6+R6)*2.04%)')
                // // ->setCellValue('Y8', '21 = 12+13+19+20')
                // ->setCellValue('Z8', '(Q6:Y6)')
                // // ->setCellValue('AA8', '21 = 12+13+19+20')
                // // ->setCellValue('AB8', '21 = 12+13+19+20')
                // ->setCellValue('AC8', '(Z)')
                // ->setCellValue('AD8', '(AC * 12)')
                // // ->setCellValue('AE8', '21 = 12+13+19+20')
                // // ->setCellValue('AF8', '21 = 12+13+19+20')
                // // ->setCellValue('AG8', '21 = 12+13+19+20')
                // // ->setCellValue('AH8', '21 = 12+13+19+20')
                // ->setCellValue('AI8', '(AE : AH)')
                // ->setCellValue('AJ8', '(AI')
                // ->setCellValue('AK8', '(AD + AJ)')
                // // ->setCellValue('AL8', '21 = 12+13+19+20')
                // // ->setCellValue('AM8', '21 = 12+13+19+20')
                // ->setCellValue('AN8', '((BS*5%)*0)')
                // // ->setCellValue('AO8', '')
                // ->setCellValue('AP8', '2% * Q8')
                // // ->setCellValue('AQ8', '21 = 12+13+19+20')
                // // ->setCellValue('AR8', '21 = 12+13+19+20')
                // ->setCellValue('AS8', '((AN+AP+AO)*(O+AR))')
                // // ->setCellValue('AT8', '21 = 12+13+19+20')
                // // ->setCellValue('AU8', '21 = 12+13+19+20')
                // // ->setCellValue('AV8', '21 = 12+13+19+20')
                // // ->setCellValue('AW8', '21 = 12+13+19+20')
                // // ->setCellValue('AX8', '21 = 12+13+19+20')
                // // ->setCellValue('AY8', '21 = 12+13+19+20')
                // // ->setCellValue('AZ8', '21 = 12+13+19+20')
                // // ->setCellValue('BA8', '21 = 12+13+19+20')
                // // ->setCellValue('BB8', '21 = 12+13+19+20')
                // // ->setCellValue('BC8', '21 = 12+13+19+20')
                // // ->setCellValue('BD8', '21 = 12+13+19+20')
                // // ->setCellValue('BE8', '21 = 12+13+19+20')
                // // ->setCellValue('BF8', '21 = 12+13+19+20')
                // // ->setCellValue('BG8', '21 = 12+13+19+20')
                // // ->setCellValue('BH8', '21 = 12+13+19+20')
                // // ->setCellValue('BI8', '21 = 12+13+19+20')
                // // ->setCellValue('BJ8', '21 = 12+13+19+20')
                // // ->setCellValue('BK8', '21 = 12+13+19+20')
                // // ->setCellValue('BL8', '21 = 12+13+19+20')
                // ->setCellValue('BM8', '(Q8+R8)*3.7%')
                // // ->setCellValue('BN8', '21 = 12+13+19+20')
                // // ->setCellValue('BO8', '21 = 12+13+19+20')
                // // ->setCellValue('BP8', '21 = 12+13+19+20')
                // // ->setCellValue('BQ8', '21 = 12+13+19+20')
                // // ->setCellValue('BR8', '21 = 12+13+19+20')
                // // ->setCellValue('BS8', '21 = 12+13+19+20')
                // // ->setCellValue('BT8', '21 = 12+13+19+20')
                // // ->setCellValue('BU8', '21 = 12+13+19+20')
                // // ->setCellValue('BV8', '21 = 12+13+19+20')
                // // ->setCellValue('BW8', '21 = 12+13+19+20')
                // // ->setCellValue('BX8', '21 = 12+13+19+20')
                // ->setCellValue('BY8', '(AP+AQ)*2%')
                // // ->setCellValue('BZ8', '21 = 12+13+19+20')
                // // ->setCellValue('CA8', '21 = 12+13+19+20')
                ;

        $spreadsheet->getActiveSheet()
            ->mergeCells("A6:A7")
            ->mergeCells("B6:B7")
            ->mergeCells("C6:C7")
            ->mergeCells("D6:D7")
            ->mergeCells("E6:E7")
            ->mergeCells("F6:F7")
            ->mergeCells("G6:G7")
            ->mergeCells("H6:H7")
            ->mergeCells("I6:I7")
            ->mergeCells("J6:J7")
            ->mergeCells("K6:K7")
            ->mergeCells("L6:L7")
            ->mergeCells("M6:M7")
            ->mergeCells("N6:N7")
            ->mergeCells("O6:O7")
            ->mergeCells("P6:P7")
            ->mergeCells("Q6:Q7")
            ->mergeCells("R6:R7")
            ->mergeCells("S6:S7")
            ->mergeCells("T6:T7")
            ->mergeCells("U6:U7")
            ->mergeCells("V6:V7")
            ->mergeCells("W6:W7")
            ->mergeCells("X6:X7")
            ->mergeCells("Y6:Y7")
            ->mergeCells("Z6:Z7")
            ->mergeCells("AA6:AA7")
            ->mergeCells("AB6:AB7")
            ->mergeCells("AC6:AC7")
            ->mergeCells("AD6:AD7")
            ->mergeCells("AE6:AE7")
            ->mergeCells("AF6:AF7")
            ->mergeCells("AG6:AG7")
            ->mergeCells("AH6:AH7")
            ->mergeCells("AI6:AI7")
            ->mergeCells("AJ6:AJ7")
            ->mergeCells("AK6:AK7")
            ->mergeCells("AL6:AL7")
            ->mergeCells("AM6:AM7")
            ->mergeCells("AN6:AN7")
            ->mergeCells("AO6:AO7")
            ->mergeCells("AP6:AP7")
            ->mergeCells("AQ6:AQ7")
            ->mergeCells("AR6:AR7")
            ->mergeCells("AS6:AS7")
            ->mergeCells("AT6:AT7")
            ->mergeCells("AU6:AU7")
            ->mergeCells("AV6:AV7")
            ->mergeCells("AW6:AW7")
            ->mergeCells("AX6:AX7")
            ->mergeCells("AY6:AY7")
            ->mergeCells("AZ6:AZ7")
            ->mergeCells("BA6:BA7")
            ->mergeCells("BB6:BB7")
            ->mergeCells("BC6:BC7")
            ->mergeCells("BD6:BD7")
            ->mergeCells("BE6:BE7")
            ->mergeCells("BF6:BF7")
            ->mergeCells("BG6:BG7")
            ->mergeCells("BH6:BH7")
            ->mergeCells("BI6:BI7")
            ->mergeCells("BJ6:BJ7")
            ->mergeCells("BK6:BK7")
            ->mergeCells("BL6:BL7")
            ->mergeCells("BM6:BM7")
            ->mergeCells("BN6:BN7")
            ->mergeCells("BO6:BO7")
            ->mergeCells("BP6:BP7")
            ->mergeCells("BQ6:BQ7")
            ->mergeCells("BR6:BR7")
            ->mergeCells("BS6:BS7")
            ->mergeCells("BT6:BT7")
            ->mergeCells("BU6:BU7")
            ->mergeCells("BV6:BV7")
            ->mergeCells("BW6:BW7")
            ->mergeCells("BX6:BX7")
            ->mergeCells("BY6:BY7")
            ->mergeCells("BZ6:BZ7")
            ->mergeCells("CA6:CA7")
        ;
 

        $spreadsheet->getActiveSheet()->getStyle("A6:CA8")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("A6:CA8")->applyFromArray($center);
        // /* END TITLE NO */

        $rowIdx = 10;
        $startIdx = $rowIdx; 
        $rowNo = 1;
        $no = '';
        $loopID  = '';
        $tmbhth = '';
        $masaKerja = '0';
        foreach ($query as $row) {
            $cutPeriod  = '';
            $tanggal1   = $row['month_period']-1;
            $namabulan1 = $this->namabulan($tanggal1);
            $tahunDB    = $row['year_period']-1;
            $tanggal    = $row['month_period'];
            $namabulan  = $this->namabulan($tanggal);
            $bulan      = $this->balikbulan($tanggal);
            
            
            if ($row['bio_rec_id'] != $loopID){
                $no = $rowNo++;
                $mkOfMonth  = $bulan;
                $masaKerja = 1;
                if($pt == 'Pontil_Timika'){
                    $cutPeriod ='26'.$namabulan1.' '.$tahunDB.' - 25 '.$namabulan.' '.$tmbhth.' ';
                }else{
                    $cutPeriod = '';
                }

            }
            else{
                $no ='';
                $mkOfMonth = $mkOfMonth;
                $masaKerja = $masaKerja +1;

                if($pt == 'Pontil_Timika'){
                    $cutPeriod ='26'.$namabulan1.' '.$tahunDB.' - 25 '.$namabulan.' '.$tmbhth.' ';
                }else{
                    $cutPeriod = '';
                }
            }
            $loopID = $row['bio_rec_id'];
            $rowIdx++;


            // start naturaAssKes //
            $naturaAssKes       = '';
                if($row['basic_salary'] < 12000000){
                    $naturaAssKes =$row['basic_salary'] * 4/100;
                }else{
                    $naturaAssKes = 12000000 * 4/100;
                }
            // end naturaAssKes //

            // start rapel //
            $rapel               = 0;
            // end rapel //

            // start assuransi ditanggung perusahaan 2.04% //
            $assTanggunganPerus  = (($row['basic_salary'] + $rapel)*2.04/100);
            // end assuransi ditanggung perusahaan 2.04% //

            // start jumlah bulan ini// 
            $jmlBlnIni = $row['basic_salary']+0+0+0+$row['incentive_bonus']+0+0+$assTanggunganPerus+$naturaAssKes;
            // end jumlah bulan ini// 

            $jmlPhTrtrSetahun   = (($row['basic_salary']+0+0+0+$row['incentive_bonus']+0+0+0+0) *12);
            $jmlBlnIniTdkTrtr   = 0+0+$row['bonus_thr']+0;
            $jmlSdBlnIniTdkTrtr = $jmlBlnIniTdkTrtr;
            $phBrutoSetahun     = $jmlPhTrtrSetahun + $jmlSdBlnIniTdkTrtr;
            $phNetto1th         = $phBrutoSetahun - 0;
            $bpjsKesehatan      = (($row['basic_salary'] *5/100) *0) ;   
            $pensiun1th         = ((0+$row['jht']+$row['jp'])*(0+0));

            $pembulatanPenghasilan = $row['brutto_tax'];
            $maxNonTax          = $this->M_payroll_config->getNonTaxAllowance();
            $nonTaxAllowance    = $row['brutto_tax'] * (5/100);
                if($nonTaxAllowance > $maxNonTax){
                    $nonTaxAllowance = $maxNonTax;
                }

            $unFixedIncome      = $row['thr']+$row['contract_bonus']+$row['other_allowance2'];
            $netto              = $row['brutto_tax'] - $row['emp_jht'] - $row['emp_jp'] - $nonTaxAllowance;
            $nettoSetahun       = ( ($netto - $unFixedIncome)*12 );
            $ptkpTotal          = $row['ptkp_total'];

            $penghasilanKenaPajak  = 0;
                if($nettoSetahun >= $ptkpTotal)
                    {
                        $penghasilanKenaPajak = $nettoSetahun - $ptkpTotal;
                    }
            $pkp = floor($penghasilanKenaPajak);
            
            // $pkpPembulatanFix = floor($penghasilanKenaPajak);
            

            $jamsostekByCmpny    = (($row['basic_salary'] + $rapel)* 3.7/100);
            $biayaPensiun        = $row['basic_salary'] * 2/100;
            $jamsostek2prsn      = (($biayaPensiun + 0)*2/100);



            $spreadsheet->getActiveSheet()
                ->setCellValue('A'.($rowIdx), $no)
                ->setCellValue('B'.($rowIdx), $cutPeriod)
                ->setCellValue('C'.($rowIdx), $row['full_name'])
                ->setCellValue('D'.($rowIdx), $row['nie'])
                ->setCellValue('E'.($rowIdx), $row['npwp_no'])
                ->setCellValue('F'.($rowIdx), $row['id_card_address'])
                ->setCellValue('G'.($rowIdx), $row['position'])
                ->setCellValue('H'.($rowIdx), $row['gender'])
                ->setCellValue('I'.($rowIdx), $row['marital_status'])
                ->setCellValue('J'.($rowIdx), $row['nationality'])
                ->setCellValue('K'.($rowIdx), $row['contract_start'])
                ->setCellValue('L'.($rowIdx), $row['contract_end'])
                ->setCellValue('M'.($rowIdx), 0)
                ->setCellValue('N'.($rowIdx), 0)
                ->setCellValue('O'.($rowIdx), $mkOfMonth)
                ->setCellValue('P'.($rowIdx), $masaKerja)
                ->setCellValue('Q'.($rowIdx), 0)
                ->setCellValue('R'.($rowIdx), $row['basic_salary'])
                ->setCellValue('S'.($rowIdx), 0)
                ->setCellValue('T'.($rowIdx), 0)
                ->setCellValue('U'.($rowIdx), $row['ot_bonus'])
                ->setCellValue('V'.($rowIdx), $row['incentive_bonus'])
                ->setCellValue('W'.($rowIdx), 0)
                ->setCellValue('X'.($rowIdx), 0)
                ->setCellValue('Y'.($rowIdx), $assTanggunganPerus)
                ->setCellValue('Z'.($rowIdx), $naturaAssKes)
                ->setCellValue('AA'.($rowIdx), $jmlBlnIni)
                ->setCellValue('AB'.($rowIdx), 0)
                ->setCellValue('AC'.($rowIdx), 0)
                ->setCellValue('AD'.($rowIdx), $jmlBlnIni)
                ->setCellValue('AE'.($rowIdx), $jmlPhTrtrSetahun)
                ->setCellValue('AF'.($rowIdx), 0)
                ->setCellValue('AG'.($rowIdx), 0)
                ->setCellValue('AH'.($rowIdx), $row['bonus_thr'])
                ->setCellValue('AI'.($rowIdx), 0)
                ->setCellValue('AJ'.($rowIdx), $jmlBlnIniTdkTrtr)
                ->setCellValue('AK'.($rowIdx), $jmlSdBlnIniTdkTrtr)
                ->setCellValue('AL'.($rowIdx), $phBrutoSetahun)
                ->setCellValue('AM'.($rowIdx), 0)
                ->setCellValue('AN'.($rowIdx), 0)
                ->setCellValue('AO'.($rowIdx), 0)
                ->setCellValue('AP'.($rowIdx), $row['jp'])
                ->setCellValue('AQ'.($rowIdx), $biayaPensiun)
                ->setCellValue('AR'.($rowIdx), 0)
                ->setCellValue('AS'.($rowIdx), 0)
                ->setCellValue('AT'.($rowIdx), $pensiun1th)
                ->setCellValue('AU'.($rowIdx), 0)
                ->setCellValue('AV'.($rowIdx), 0)
                ->setCellValue('AW'.($rowIdx), $phNetto1th)
                ->setCellValue('AX'.($rowIdx), $row['ptkp_total'])
                ->setCellValue('AY'.($rowIdx), $penghasilanKenaPajak)
                ->setCellValue('AZ'.($rowIdx), $pkp)
                ->setCellValue('BA'.($rowIdx), 0)
                ->setCellValue('BB'.($rowIdx), 0)
                ->setCellValue('BC'.($rowIdx), 0)
                ->setCellValue('BD'.($rowIdx), 0)
                ->setCellValue('BE'.($rowIdx), 0)
                ->setCellValue('BF'.($rowIdx), 0)
                ->setCellValue('BG'.($rowIdx), 0)
                ->setCellValue('BH'.($rowIdx), 0)
                ->setCellValue('BI'.($rowIdx), 0)
                ->setCellValue('BJ'.($rowIdx), 0)
                ->setCellValue('BK'.($rowIdx), 0)
                ->setCellValue('BL'.($rowIdx), 0)
                ->setCellValue('BM'.($rowIdx), 0)
                ->setCellValue('BN'.($rowIdx), $jamsostekByCmpny)
                ->setCellValue('BO'.($rowIdx), 0)
                ->setCellValue('BP'.($rowIdx), 0)
                ->setCellValue('BQ'.($rowIdx), 0)
                ->setCellValue('BR'.($rowIdx), 0)
                ->setCellValue('BS'.($rowIdx), 0)
                ->setCellValue('BT'.($rowIdx), 0)
                ->setCellValue('BU'.($rowIdx), 0)
                ->setCellValue('BV'.($rowIdx), 0)
                ->setCellValue('BW'.($rowIdx), 0)
                ->setCellValue('BX'.($rowIdx), 0)
                ->setCellValue('BY'.($rowIdx), 0)
                ->setCellValue('BZ'.($rowIdx), $jamsostek2prsn)
                ->setCellValue('CA'.($rowIdx), 0);
                          

             // SET ROW COLOR 
            if($rowIdx % 2 == 1)
            {
                $spreadsheet->getActiveSheet()->getStyle('A'.$rowIdx.':CA'.$rowIdx)
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('EAEBAF');             
            } 
        //     /* END UPDATE TAX */
        } /* end foreach ($query as $row) */

        $spreadsheet->getActiveSheet()
                ->setCellValue('C'.($rowIdx+2), 'TOTAL NILAI TAGIHAN')
                ->setCellValue('H'.($rowIdx+2), '=SUM(H'.$startIdx.':H'.$rowIdx.')')
                ->setCellValue('I'.($rowIdx+2), '=SUM(I'.$startIdx.':I'.$rowIdx.')')
                ->setCellValue('J'.($rowIdx+2), '=SUM(J'.$startIdx.':J'.$rowIdx.')')
                ->setCellValue('K'.($rowIdx+2), '=SUM(K'.$startIdx.':K'.$rowIdx.')')
                ->setCellValue('L'.($rowIdx+2), '=SUM(L'.$startIdx.':L'.$rowIdx.')')
                ->setCellValue('M'.($rowIdx+2), '=SUM(M'.$startIdx.':M'.$rowIdx.')')
                ->setCellValue('N'.($rowIdx+2), '=SUM(N'.$startIdx.':N'.$rowIdx.')')
                ->setCellValue('O'.($rowIdx+2), '=SUM(O'.$startIdx.':O'.$rowIdx.')');

        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":CA".($rowIdx+2))->getFont()->setBold(true)->setSize(12); 
        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":CA".($rowIdx+2))->applyFromArray($outlineBorderStyle);

        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":CA".($rowIdx+2))
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B');

        /* SET NUMBERS FORMAT*/
        $spreadsheet->getActiveSheet()->getStyle('D9:S'.($rowIdx+2))->getNumberFormat()->setFormatCode('#,##0.00');
        $spreadsheet->getActiveSheet()->getStyle('A1')->getAlignment()->setWrapText(true);     
        
        unset($allBorderStyle);
        unset($center);
        unset($right);
        unset($left);
        
        
        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('PPH21CALCULATION');

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        //Nama File
        // $str = $rowData['name'].$rowData['bio_rec_id'];
        $str = 'PPH21Calculation'.$clientName;
        $fileName = preg_replace('/\s+/', '', $str);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.Xlsx"');
        // header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        /* BY COMPOSER */
        // $writer = new Xlsx($spreadsheet);
        /* OFFLINE/ BY COPY EXCEL FOLDER  */
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit(0);  
     }
    /* END GET PRINT DATA */

   
}
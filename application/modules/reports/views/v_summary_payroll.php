<div id="loader"></div>

<!--START FORM DATA -->
<div class="col-md-12" id="is_transaction">
  <div class="tile bg-info">
    <!-- <h3 class="tile-title">Barang Masuk</h3> -->
    <div class="tile-body">
      <form class="row is_header">
      <!-- <form class="row is_header" action="<?php #echo base_url() ?>transactions/sumbawa/Timesheet/payrollProcess"> -->
        
        <div class="form-group col-sm-12 col-md-2">
          <label class="control-label">CLIENT</label>
          <!-- <code id="docKindErr" class="errMsg"><span> : Required</span></code> -->
          <select class="form-control" id="clientName" name="clientName" required="">
    				<!-- <option value="" disabled="" selected="">Pilih</option>
    				<option value="Pontil_Timika">Pontil Timika</option>
    				<option value="Redpath_Timika">Redpath Timika</option>
            <option value="Pontil_Sumbawa">Pontil Sumbawa</option>
    				<option value="AMNT_Sumbawa">AMNT Sumbawa </option>
    				<option value="Trakindo_Sumbawa">Trakindo Sumbawa</option>
    				<option value="LCP_Sumbawa">Lamurung Cipta Persada</option>
            <option value="Pontil_Banyuwangi">Pontil Banyuwangi</option> -->
    		  </select>
        </div>

        <div class="form-group col-sm-12 col-md-2">
          <label class="control-label">YEAR</label>
          <!-- <code id="docKindErr" class="errMsg"><span> : Required</span></code> -->
          <select class="form-control" id="yearPeriod" name="yearPeriod" required="">
      			<option value="" disabled="" selected="">Pilih</option>
      			<script type="text/javascript">
      				var dt = new Date();
      				var currYear = dt.getFullYear();
      				var currMonth = dt.getMonth();
              var currDay = dt.getDate();
              var tmpDate = new Date(currYear + 1, currMonth, currDay);
              var startYear = tmpDate.getFullYear();
      				var endYear = startYear - 80;							
      				for (var i = startYear; i >= endYear; i--) 
      				{
      					document.write("<option value='"+i+"'>"+i+"</option>");						
      				}
      			</script>
      		</select>
        </div>

        <div class="form-group col-sm-12 col-md-2">
          <label class="control-label">MONTH</label>
          <!-- <code id="docKindErr" class="errMsg"><span> : Required</span></code> -->
          <select class="form-control" id="monthPeriod" name="monthPeriod" required="">
    				<option value="" disabled="" selected="">Pilih</option>
    				<script type="text/javascript">
    					var tMonth = 1;
    					for (var i = tMonth; i <= 12; i++) 
    					{
    						if(i < 10)
    						{
    							document.write("<option value='0"+i+"'>0"+i+"</option>");							
    						}
    						else
    						{
    							document.write("<option value='"+i+"'>"+i+"</option>");								
    						}
    					}
    				</script>
    			</select>
        </div>
        <!-- START CUSTOMIZE GOVERNMENT REGULATION/   -->
                       
        <div class="form-group col-md-12 align-self-end">
          <button class="btn btn-warning" id="viewSummaryPayroll" type="button"><i class="fa fa-refresh"></i>VIEW</button>
          <button class="btn btn-warning" id="printSummaryPayroll" type="button"><i class="fa fa-refresh"></i>PRINT</button>
		      <br>
		      <!-- <br> -->
    	  <!-- <h3><code id="dataProses" class="backTransparent"><span></span></code></h3>	 -->
        </div>
      </form>
    </div>
  </div>
</div>
<!-- END FORM DATA -->

<div class="col-md-12" id="rt">
  <div class="tile bg-info">
    <!-- <h3 class="tile-title">Barang Masuk</h3> -->
    <div class="tile-body">
       <!-- START DATA TABLE -->
        <div class="tile-body table-responsive">
          <table class="table table-hover table-bordered" id="paymentList_rt">
            <thead class="thead-dark">
              <tr>
                <th>Biodata Id</th>
                <th>Name</th>
                <th>NPWP</th>
                <th>External ID</th>
                <th>Basic Salary</th>
                <th>OT Total</th>
                <th>OT Bonus</th>
                <th>Remote Allowance</th>
                <th>Dev Bonus</th>
                <th>Shift Bonus</th>
                <th>Contract Bonus</th>
                <th>Jumbo BNS</th>
                <th>Adj In</th>
                <th>Adj Out</th>
                <th>THR</th>
                <th>PVB</th>
                <th>Debt Burden</th>
                <th>Gross</th>
                <th>Net Payment</th>
              </tr>
            </thead>
          </table>
        </div>
        <!-- END DATA TABLE -->
    </div>
  </div>
</div>
<div class="col-md-12" id="pon">
  <div class="tile bg-info">
    <!-- <h3 class="tile-title">Barang Masuk</h3> -->
    <div class="tile-body">
       <!-- START DATA TABLE -->
        <div class="tile-body table-responsive">
          <table class="table table-hover table-bordered" id="paymentList_pon">
            <thead class="thead-dark">
              <tr>
                <th>Biodata Id</th>
                <th>Name</th>
                <th>NPWP</th>
                <th>External ID</th>
                <th>Basic Salary</th>
                <th>Overtime</th>
                <th>Travel Bonus</th>
                <th>Allowance Economy</th>
                <th>Incentive Bonus</th>
                <th>Shift Bonus</th>
                <th>Adj In</th>
                <th>Adj Out</th>
                <th>THR</th>
                <th>CC Payment</th>
                <th>Production Bonus</th>
                <th>Gross</th>
                <th>Net Payment</th>
              </tr>
            </thead>
          </table>
        </div>
        <!-- END DATA TABLE -->
    </div>
  </div>
</div>
<div class="col-md-12" id="ponswq">
  <div class="tile bg-info">
    <!-- <h3 class="tile-title">Barang Masuk</h3> -->
    <div class="tile-body">
       <!-- START DATA TABLE -->
        <div class="tile-body table-responsive">
          <table class="table table-hover table-bordered" id="paymentList_ponswq">
            <thead class="thead-dark">
              <tr>
                <th>Biodata Id</th>
                <th>Name</th>
                <th>NPWP</th>
                <th>External ID</th>
                <th>Basic Salary</th>
                <th>Overtime</th>
                <th>Travel Bonus</th>
                <th>Incentive Bonus</th>
                <th>Shift Bonus</th>
                <th>Adj In</th>
                <th>Adj Out</th>
                <th>THR</th>
                <th>Production Bonus</th>
                <th>Others Bonus</th>
                <th>Gross</th>
                <th>Net Payment</th>
              </tr>
            </thead>
          </table>
        </div>
        <!-- END DATA TABLE -->
    </div>
  </div>
</div>
<div class="col-md-12" id="ponbwg">
  <div class="tile bg-info">
    <!-- <h3 class="tile-title">Barang Masuk</h3> -->
    <div class="tile-body">
       <!-- START DATA TABLE -->
        <div class="tile-body table-responsive">
          <table class="table table-hover table-bordered" id="paymentList_ponbwg">
            <thead class="thead-dark">
              <tr>
                <th>Biodata Id</th>
                <th>External ID</th>
                <th>Name</th>
                <th>NPWP</th>
                <th>Basic Salary</th>
                <th>Overtime</th>
                <th>Travel Bonus</th>
                <th>Workday Adjustment</th>
                <th>Flying Camp</th>
                <!-- <th>Adj In</th> -->
                <!-- <th>Adj Out</th> -->
                <th>THR</th>
                <th>Debt Burden</th>
                <th>Gross</th>
                <th>Net Payment</th>
                
              </tr>
            </thead>
          </table>
        </div>
        <!-- END DATA TABLE -->
    </div>
  </div>
</div>
<div class="col-md-12" id="lcp">
  <div class="tile bg-info">
    <!-- <h3 class="tile-title">Barang Masuk</h3> -->
    <div class="tile-body">
       <!-- START DATA TABLE -->
        <div class="tile-body table-responsive">
          <table class="table table-hover table-bordered" id="paymentList_lcp">
            <thead class="thead-dark">
              <tr>
                <th>Biodata Id</th>
                <th>Name</th>
                <th>NPWP</th>
                <th>External ID</th>
                <th>Basic Salary</th>
                <th>Overtime</th>
                <th>Camp Regional</th>
                <th>Camp Eksplorasi</th>
                <th>THR</th>
                <th>JKK & JKM</th>
                <th>Health BPJS</th>
                <th>Adj In</th>
                <th>Adj Out</th>
                <th>Gross</th>
                <th>Net Payment</th>
              </tr>
            </thead>
          </table>
        </div>
        <!-- END DATA TABLE -->
    </div>
  </div>
</div>
<div id="loader"></div>

<!--START FORM DATA -->
<div class="col-md-12" id="is_transaction">
  <div class="tile bg-info">
    <div class="tile-body">
      <form class="row is_header">        
        <div class="form-group col-sm-12 col-md-2">
          <label class="control-label">CLIENT</label>
          <code id="clientNameErr" class="errMsg"><span> : Required</span></code>
          <select class="form-control" id="clientName" name="clientName" required="">
          </select>
          <!-- <select class="form-control" id="clientName" name="clientName" required="">
      			<option value="" disabled="" selected="">Pilih</option>
      			<option value="Pontil_Timika">Pontil Timika</option>
            <option value="Redpath_Timika">Redpath Timika</option>
            <option value="Agincourt_Martabe">Agincourt Martabe</option>
            <option value="Amnt_Staff_Sumbawa">AMNT Staff Sumbawa</option>
            <option value="Trakindo_Sumbawa">Trakindo Sumbawa</option>
    		  </select> -->
        </div>
            
        <div class="form-group col-md-12 align-self-end">
    		  <button class="btn btn-warning btnProcessPanel" type="button" id="viewCsv" name="viewCsv">
        	<span class="fa fa-list"></span> VIEW
        	</button>
    		  <!-- <button class="btn btn-warning btnProcessPanel" type="button" id="printToFile" name="printToFile"> -->
      		<!-- <span class="fa fa-print"></span> DATA PRINT -->
        	<!-- </button> -->
          <button class="btn btn-warning print" type="button" id="print" name="print">
          <span class="fa fa-print"></span> PRINT
          </button>
		      <br>
        	<h3><code id="dataProses" class="backTransparent"><span></span></code></h3>	
        </div>
      </form>
    </div>
  </div>
</div>
<!-- END FORM DATA -->

<div class="col-md-12">
  <div class="tile bg-info">
    <div class="tile-body">
       <!-- START DATA TABLE -->
        <div class="tile-body">
          <table class="table table-hover table-bordered" id="contractHist">
            <thead class="thead-dark">
              <tr>
                <th>Bio ID</th>
                <th>Name</th>
                <th>Client Name</th>
                <th>Contract Count</th>
                <th>Detail</th>
              </tr>
            </thead>
          </table>
        </div>
        <!-- END DATA TABLE -->
    </div>
  </div>
</div>

<div class="modal fade" id="editPayrollModal" tabindex="-1" role="dialog" aria-labelledby="editPayrollModalLabel"> 
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <!-- Start modal-header -->
      <div class="modal-header">
        <h4 class="modal-title" id="editPayrollModalLabel">Contract History</h4>
        </div>
        <!-- End modal-header -->
        <!-- Start modal-body -->
        <div class="modal-body">
        <!-- Start Form EDIT PAYROLL -->
          <form method="post" class="my_detail">
            <div class="form-group">
              <label class="control-label" for="biodataId">Id</label>
              <input class="form-control" type="text" id="biodataId" name="biodataId" placeholder="Id" readonly="">
            </div>                     

            <div class="form-group">
              <label class="control-label" for="empName">Name</label>
              <input class="form-control" type="text" id="empName" name="empName" placeholder="Nama Karyawan" readonly="">
            </div>

            <div class="form-group">
              <label class="control-label" for="clientNameDetail">Client Name</label>
              <input class="form-control" type="text" id="clientNameDetail" name="clientNameDetail" placeholder="Nama Klien" readonly="">
            </div>

            <div class="row">
              <div class="table-responsive">
              <!-- START TABLE PENGALAMAN --> 
                <div class="table-responsive">
                  <table id="contractDetailTable" class="table table-striped dataTable no-footer" cellspacing="0" width="100%" role="grid" style="width: 100%;">
                    <thead>
                      <tr role="row">
                        <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">SEQUENCES</th>
                        <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">CONTRACT START</th>
                        <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">CONTRACT END</th>
                    </thead>                    
                  </table>  
                </div>
                   <!-- END TABLE PENGALAMAN -->            
              </div>
            </div>
            <!-- END DATA PAYROLL -->                                                                              
          </form>
          <!-- End Form EDIT PAYROLL -->  
        </div> 
        <!-- End modal-body -->
        <!-- Start modal-footer -->
        <div class="modal-footer">
          <input type="button" class="btn btn-primary btn-sm" data-dismiss="modal" value="Close"> 
        </div>
        <!-- End modal-footer -->
    </div>
  </div> 
</div> 
<!-- End editPayrollModal -->
<!-- END FORM MODAL -->
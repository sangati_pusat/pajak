<div id="loader"></div>

<!--START FORM DATA -->
<div class="col-md-12" id="is_transaction">
  <div class="tile bg-info">
    <!-- <h3 class="tile-title">Barang Masuk</h3> -->
    <div class="tile-body">
      <form class="row is_header">
           
        <div class="form-group col-sm-12 col-md-2">
          <label class="control-label">CLIENT</label>
          <select class="form-control" id="clientName" name="clientName" required="">
            <option value="" disabled="" selected="">Pilih</option>     
            <option value="Pontil_Sumbawa">Pontil Sumbawa</option>
          </select>
        </div>

        <div class="form-group col-sm-12 col-md-2">
          <label class="control-label">YEAR</label>
          <!-- <code id="docKindErr" class="errMsg"><span> : Required</span></code> -->
          <select class="form-control" id="yearPeriod" name="yearPeriod" required="">
            <option value="" disabled="" selected="">Pilih</option>
            <script type="text/javascript">
              var dt = new Date();
              var currYear = dt.getFullYear();
              var currMonth = dt.getMonth();
              var currDay = dt.getDate();
              var tmpDate = new Date(currYear + 1, currMonth, currDay);
              var startYear = tmpDate.getFullYear();
              var endYear = startYear - 80;             
              for (var i = startYear; i >= endYear; i--) 
              {
                document.write("<option value='"+i+"'>"+i+"</option>");           
              }
            </script>
          </select>
        </div>

       
        <!-- START CUSTOMIZE GOVERNMENT REGULATION/   -->
                       
        <div class="form-group col-md-12 align-self-end">
          <button class="btn btn-warning" id="viewExprtList" type="button"><i class="fa fa-refresh"></i>Display Data</button>
          <!-- <button class="btn btn-warning" id="printExprtList" type="button"><i class="fa fa-refresh"></i>Print Data</button> -->
          <button class="btn btn-warning" id="prosesExprtSpt" type="button"><i class="fa fa-refresh"></i>Process Data</button>
          <br>
          <!-- <br> -->
        <!-- <h3><code id="dataProses" class="backTransparent"><span></span></code></h3>   -->
        </div>
      </form>
    </div>
  </div>
</div>
<!-- END FORM DATA -->

<div class="col-md-12">
  <div class="tile bg-info">
    <!-- <h3 class="tile-title">Barang Masuk</h3> -->
    <div class="table-responsive">
       <!-- START DATA TABLE -->
        <!-- <div class="tile-body"> -->
          <table id="exprtList" class="table table-hover table-bordered" class="table table-striped dataTable no-footer" cellspacing="0" width="100%" role="grid" style="width: 100%;">
            <thead class="thead-dark">           
              <tr role="row">
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Slip ID</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Biodata Id</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Nie ID</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Client Name</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Name</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Year Period</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Position</th>
                <!-- <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Action</th> -->
                
              </tr>
            </thead>                    
        
          </table>
        </div>
        <!-- END DATA TABLE -->
    </div>
  </div>
</div>

<style type="text/css">
tr.selected {
  background-color: #B0BED9!important;
}
/*tr.even.selected {
  background-color: #B0BED9!important;
}*/
</style>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>

<div class="col-md-12" id="is_transaction">
  <div class="tile bg-info">
  <div class="tile">
    <div class="tile-body">
      <form class="form-horizontal">
        <div class="form-group row">

			<div class="col-xs-12 col-md-2">
				<label class="control-label" for="clientName">CLIENT NAME</label>
				<select class="form-control" id="ptName" name="ptName" required="">
				<!-- 	<option value="" disabled="" selected="">Select</option> -->
					<option value="ALL DATA" disabled="">Select </option>
					<option value="Agincourt Resources">Agincourt Resources</option>
					
				</select>
			</div>

			<div class="col-xs-12 col-md-2">
				<label class="control-label" for="startDate">START DATE</label>				
				<input class="errMsg form-control" type="text" id="startDate" name="startDate" placeholder="yyyy-mm-dd" required ="">
			</div>

			<div class="col-xs-12 col-md-2">
				<label class="control-label" for="endDate">END DATE</label>	
				<input class="errMsg form-control" type="text" id="endDate" name="endDate" placeholder="yyyy-mm-dd" required ="">
			</div>


	    </div>

			<!-- START BUTTON ADD & SAVE -->
        	<button class="btn btn-warning btnProcessPanel" type="button" id="viewPlhReports" name="viewPlhReports"><i class="fa fa-refresh"></i> Display Data </button>
        	<button class="btn btn-warning btnProcessPanel" type="button" id="printPlhReports" name="printPlhReports"><span class="fa fa-print"></span> Print </button>
        	<!-- END BUTTON ADD & SAVE -->

        	<br>
            <h3><code id="dataProses" class="backTransparent"><span></span></code></h3> 
        </div>
      </form>
    </div>
  </div>
</div>

<div class="col-md-12">
  	<div class="tile bg-info">
    <!-- <h3 class="tile-title">Barang Masuk</h3> -->
    	<div class="table-responsive">
						<table id="plhReportsTable" class="table table-hover table-bordered" class="table table-striped dataTable no-footer" cellspacing="0" width="100%" role="grid" style="width: 100%;">
				            <thead class="thead-dark">
				                <tr role="row">
				                	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Biodata Id</th>
				                	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">RTE Number </th>
				                	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Full Name </th>
				                	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Client Name</th>
				                	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">ID Card No</th>
				                	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Position</th>
				                	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">BPJS Kesehatan </th>
				                	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">BPJS Ketenagakerjaan</th>
				                	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Badge No</th>
				                	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Work Day Amount</th>
				                	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Start Date</th>
				                	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">End Date</th>
				                	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Salary</th>
				                	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Year Period</th>
				                	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Month Period</th>
				                	
				                </tr>
				            </thead>				            
				          
				        </table>	
				 </div>
	             <!-- END TABLE PENGALAMAN -->			    	
			</div>
		</div>
		<!-- END DATA PAYROLL -->
		<br>	
	</div>	
</div>


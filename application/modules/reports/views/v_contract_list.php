<style type="text/css">
tr.selected {
  background-color: #B0BED9!important;
}
/*tr.even.selected {
  background-color: #B0BED9!important;
}*/
</style>
<!--START FORM DATA -->
<div class="col-md-12" id="is_transaction">
  <div class="tile bg-info">
    <!-- <h3 class="tile-title">Barang Masuk</h3> -->
    <div class="tile-body">
      <form class="row is_header">
      <!-- <form class="row is_header" action="<?php #echo base_url() ?>transactions/sumbawa/Timesheet/payrollProcess"> -->
        
        <div class="form-group col-sm-12 col-md-2">
          <label class="control-label">MONTH</label>
          <select class="form-control" id="monthPeriod" name="monthPeriod" required="">
            <option value="" disabled="" selected="">Pilih</option>
            <script type="text/javascript">
              var tMonth = 1;
              for (var i = tMonth; i <= 12; i++){
                if(i < 10){
                  document.write("<option value='"+i+"'>0"+i+"</option>");             
                }else{
                  document.write("<option value='"+i+"'>"+i+"</option>");               
                }
              }
            </script>
          </select>
        </div>
        <div class="form-group col-sm-12 col-md-2">
          <label class="control-label">YEAR</label>
          <select class="form-control" id="yearPeriod" name="yearPeriod" required="">
            <option value="" disabled="" selected="">Pilih</option>
            <script type="text/javascript">
              var dt = new Date();
              var startYear = dt.getFullYear();
              var endYear = startYear - 10;             
              for (var i = startYear; i >= endYear; i--){
                document.write("<option value='"+i+"'>"+i+"</option>");           
              }
            </script>
          </select>
        </div>
        <div id='client' class="form-group col-sm-12 col-md-3">
          <label class="control-label">CLIENT</label>
          <select class="form-control" id="ptName" name="ptName" required="">
            <!-- <option data-table="" value="" disabled="" selected="">Pilih</option> -->
            <!-- <option data-table="" value="Agincourt_Martabe">Agincourt Martabe</option> -->
            <!-- <option data-table="trn_slip_amnsmb" value="AMNT_Sumbawa_Staff">AMNT Sumbawa Staff</option> -->
            <!-- <option data-table="" value="AEA_Sumbawa">Asih Eka Abadi</option> -->
            <!-- <option data-table="trn_slip_amnsmb" value="AMNT_Sumbawa">AMNT Sumbawa </option> -->
            <!-- <option data-table="" value="CK_Jambi">Cipta Kridatama Jambi</option>        
            <option data-table="" value="ENJ_Timika">Ekplorasi Nusa Jaya</option>
            <option data-table="" value="Indodrill_Martabe">Indodrill Martabe</option>
            <option data-table="" value="JSI_Cilacap">JSI Cilacap</option>
            <option data-table="" value="KDG_Sumbawa">Kamilas Dodo Gemilang</option>
            <option data-table="" value="LASD_Sumbawa">Lawang Sampar Dodo</option> -->
            <!-- <option data-table="trn_slip_lcpsmb" value="LCP_Sumbawa">Lamurung Cipta Persada</option> -->
            <!-- <option data-table="" value="Macmahon_Sumbawa">Macmahon Sumbawa</option>
            <option data-table="" value="MHE_Jakarta">MHE Demag Jakarta</option>
            <option data-table="" value="Kufpec_Jakarta">Kufpec Jakarta</option> -->
            <!-- <option data-table="trn_slip_ptlbwg" value="Pontil_Banyuwangi">Pontil Banyuwangi</option> -->
            <!-- <option data-table="" value="Pontil_Jakarta">Pontil Jakarta</option> -->
            <!-- <option data-table="trn_salary_slip" value="Pontil_Timika">Pontil Timika</option> -->
            <!-- <option data-table="trn_slip_ptlsmb" value="Pontil_Sumbawa">Pontil Sumbawa</option> -->
            <!-- <option data-table="trn_salary_slip" value="Redpath_Timika">Redpath Timika</option> -->
            <!-- <option data-table="" value="SODEXO_Martabe">SODEXO Martabe</option> -->
            <!-- <option data-table="trn_slip_trksmb" value="Trakindo_Sumbawa">Trakindo Sumbawa</option> -->
            <!-- <option data-table=""value="TSI_Sumbawa">Transportation Solution Indonesia</option> -->
          </select>
        </div>
        <!-- START CUSTOMIZE GOVERNMENT REGULATION/   -->
                       
        <div class="form-group col-md-12 align-self-end">
          <button class="btn btn-warning" id="viewContractList" type="button"><i class="fa fa-refresh"></i>VIEW</button>
          <button class="btn btn-warning" id="printContractList" type="button"><i class="fa fa-print"></i>PRINT</button>
		      <br>
		      <!-- <br> -->
    	  <!-- <h3><code id="dataProses" class="backTransparent"><span></span></code></h3>	 -->
        </div>
      </form>
    </div>
  </div>
</div>
<!-- END FORM DATA -->

<div class="col-md-12" id="rt">
  <div class="tile bg-info">
    <div class="tile-body">
      <div class="tile-body table-responsive employeeListTable">
        <table class="table table-hover table-bordered" id="contractListTable">
          <thead class="thead-dark">
            <tr role="row">
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Contract No</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 160px;">Employee Name </th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 160px;">Dept</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 130px;">Position</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Contract Start</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Contract End </th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Is Active</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>
<style type="text/css">
tr.selected {
  background-color: #B0BED9!important;
}
/*tr.even.selected {
  background-color: #B0BED9!important;
}*/
</style>
<!--START FORM DATA -->
<div class="col-md-12" id="is_transaction">
  <div class="tile bg-info">
    <h2>Input Closing</h2>
    <br/>
      <div id='client' class="form-group row">
        <label class="control-label col-md-1">CLIENT</label>
        <div class="col-md-2">
          <select class="form-control" id="iptName" name="ptName" required="">
          </select>
        </div>
        <?php 
        $year       = date("Y");
        $year_range = $year - 2;
        ?>
        <label class="control-label col-md-1">Year</label>
        <div class="col-md-2">
          <select class="form-control" id="iyear" name="iyear" required="">
            <option value="Pilih">Year Closing</option>
            <?php 
            for ($x = $year; $x >= $year_range; $x--) {
              echo '<option value="'.$x.'">'.$x.'</option>';
            }
            ?>
          </select>
        </div>
        <label class="control-label col-md-1">Month</label>
        <div class="col-md-2">
          <select class="form-control" id="imonth" name="ptName" required="">
            <option value="Pilih">Month Closing</option>
            <?php 
            for ($y = 12; $y >= 1; $y--) {
              if($y<10){
                echo '<option value="0'.$y.'">0'.$y.'</option>';
              }else{
                echo '<option value="'.$y.'">'.$y.'</option>';
              }
            }
            ?>
          </select>
        </div>
        <label class="control-label col-md-2"><button class="btn btn-warning" id="closingStatus" type="button">Closing</button></label>
      </div>  
  </div>
  <!-- <div class="tile bg-info">
    <div class="tile-body">      
      <h2>View Closing</h2>
        <div id='client' class="form-group col-sm-12 col-md-3">
          <label class="control-label">CLIENT</label>
          <select class="form-control" id="ptName" name="ptName" required="">
          </select>
        </div>                
        <div class="form-group col-md-12 align-self-end">
          <button class="btn btn-warning" id="viewContractList" type="button"><i class="fa fa-refresh"></i>VIEW</button>
		      <br>
        </div>
      </form>
    </div>
  </div> -->
</div>
<!-- END FORM DATA -->

<div class="col-md-12" id="rt">
  <div class="tile bg-info">
    <div class="tile-body">
      <h2>View Closing</h2>
      <br/>
      <div id='client' class="form-group row">
        <label class="control-label col-md-1">CLIENT</label>
        <div class="col-md-4">
          <select class="form-control" id="ptName" name="ptName" required="">
          </select>
        </div>
        <label class="control-label col-md-2"><button class="btn btn-warning" id="viewContractList" type="button"><i class="fa fa-refresh"></i>VIEW</button></label>
      </div>  
      <div class="tile-body table-responsive employeeListTable">
        <table class="table table-hover table-bordered" id="contractListTable">
          <thead class="thead-dark">
            <tr role="row">
              <th class="sorting_disabled" rowspan="1" colspan="1">Client Name</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 10%;">Tahun</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 10%;">Bulan</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 10%;">Status</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 15%;">Action</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="openModal" tabindex="-1" role="dialog" aria-labelledby="salaryModalLabel"> 
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header btn-warning">
        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
        <h4 class="modal-title" id="salaryModalLabel">Open Data Closing</h4>
      </div>
      <br/>
      <div>
        <div id="detail" class="panel panel-body panelButtonDetail">
          <div class="container-fluid">
            <div class="tile-body">
              <div class="form-group row">
                <label class="control-label col-md-2">Client Name</label>
                <div class="col-md-5">
                  <input class="errMsg form-control col-md-12" type="text" placeholder="Nama Pemilik Rekening" id="client" disabled="">
                  <input class="errMsg form-control col-md-12" type="hidden" placeholder="Nama Pemilik Rekening" id="client_name">
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-md-2">Tahun</label>
                <div class="col-md-2">
                  <input class="errMsg form-control col-md-12" type="text" placeholder="Nomor Rekening" id="tahun" disabled="">
                  <input class="errMsg form-control col-md-12" type="hidden" placeholder="Nomor Rekening" id="year">
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-md-2">Bulan</label>
                <div class="col-md-2">
                  <input class="errMsg form-control col-md-12" type="text" placeholder="Nomor Rekening" id="bulan" disabled="">
                  <input class="errMsg form-control col-md-12" type="hidden" placeholder="Nomor Rekening" id="month">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> 
      <div class="modal-footer">
        <input type="button" class="btn btn-info btn-sm" data-dismiss="modal" value="Open" id="openData">         
        <input type="button" class="btn btn-primary btn-sm" data-dismiss="modal" value="Close"> 
      </div>
    </div>
  </div> 
</div> 

<div class="modal fade" id="closingModal" tabindex="-1" role="dialog" aria-labelledby="salaryModalLabel"> 
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header btn-warning">
        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
        <h4 class="modal-title" id="salaryModalLabel">Closing Data</h4>
      </div>
      <br/>
      <div>
        <div id="detail" class="panel panel-body panelButtonDetail">
          <div class="container-fluid">
            <div class="tile-body">
              <div class="form-group row">
                <label class="control-label col-md-2">Client Name</label>
                <div class="col-md-5">
                  <input class="errMsg form-control col-md-12" type="text" placeholder="Nama Pemilik Rekening" id="Cclient" disabled="">
                  <input class="errMsg form-control col-md-12" type="hidden" placeholder="Nama Pemilik Rekening" id="Cclient_name">
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-md-2">Tahun</label>
                <div class="col-md-2">
                  <input class="errMsg form-control col-md-12" type="text" placeholder="Nomor Rekening" id="Ctahun" disabled="">
                  <input class="errMsg form-control col-md-12" type="hidden" placeholder="Nomor Rekening" id="Cyear">
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-md-2">Bulan</label>
                <div class="col-md-2">
                  <input class="errMsg form-control col-md-12" type="text" placeholder="Nomor Rekening" id="Cbulan" disabled="">
                  <input class="errMsg form-control col-md-12" type="hidden" placeholder="Nomor Rekening" id="Cmonth">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> 
      <div class="modal-footer">
        <input type="button" class="btn btn-info btn-sm" data-dismiss="modal" value="Closing" id="closingData">         
        <input type="button" class="btn btn-primary btn-sm" data-dismiss="modal" value="Close"> 
      </div>
    </div>
  </div> 
</div> 
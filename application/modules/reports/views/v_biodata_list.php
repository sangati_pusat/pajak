<!--START FORM DATA -->
<div class="col-md-12" id="is_transaction">
  	<div class="tile bg-info">
  		<div id="saveMsg"></div>
    	<!-- <h3 class="tile-title">Barang Masuk</h3> -->
    	<div class="tile-body">
      		<div class="row is_header">
				<div class="form-group col-sm-12 col-md-2">
          			<select class="form-control" id="agesMax" name="agesMax" required="">
					<option value="55" selected="">All Ages</option>
					<script type="text/javascript"> 				
						var age = 17;
						for (var i = age; i <= 55; i++) 
						{
							if(i < 10)
							{
								document.write("<option value='0"+i+"'>0"+i+"</option>");							
							}
							else
							{
								document.write("<option value='"+i+"'>"+i+"</option>");								
							}						
						}
					</script>
					</select>		 
        		</div>
		        <div class="form-group col-sm-12 col-md-2">
		          	<input class="form-control" type="text" id="searchByPosition" name="searchByPosition" placeholder="Position" width="100px">	 
		        </div> 
		        <div class="form-group col-sm-12 col-md-2">
		          	<input class="form-control" type="text" id="searchByCity" name="searchByCity" placeholder="City Name" width="100px">	 
		        </div>
		        <div class="form-group col-sm-12 col-md-2">
		          	<input class="form-control" type="text" id="searchByName" name="searchByName" placeholder="Employee Name" width="100px">	 
		        </div>
		        <div class="form-group col-sm-12 col-md-2">
		          	<input class="form-control" type="text" id="searchByIdNo" name="searchByIdNo" placeholder="No KTP" width="100px">	 
		        </div>   
				<div class="form-group col-sm-12 col-md-2 checkbox">
				  	<label><input type="checkbox" id="isCandidate" name="isCandidate" value="" checked=""><b>Candidate</b></label>&nbsp;&nbsp;
				  	<label><input type="checkbox" id="isStaff" name="isStaff" value=""><b>Staff</b></label>
				</div>
		    	<div class="form-group col-sm-12 col-md-2">
		        	<button class="btn btn-primary" id="printBiodata" name="printBiodata" width="100px">
		        	<span class="glyphicon fa fa-print"></span> Print</button>
		    	</div>
		    </div>
      		<table class="table table-hover table-bordered" id="biodataListTable">
            <thead class="thead-dark">
                <tr>
                  	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 139px;">Biodata Id</th>
	            	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 118px;">Name</th>
	            	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 169px;">Address</th>
	            	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 169px;">City</th>
	            	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 130px;">Contact No</th>
	            	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 176px;">Position</th>
	            	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 176px;">CV Date</th>
	            	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Edit</th>
	            	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Print</th>
                </tr>
            </thead>
            </table>
    	</div>
  	</div>
</div>
<!-- END FORM DATA -->

<div class="col-md-12">
  	<div class="tile bg-info">
    	<div class="tile-body">
        	<div id="horizontalTab">
          		<ul>
		            <li><a href="#ed">Educations</a></li>
		            <li><a href="#tr">Trainings</a></li>
		            <li><a href="#ex">Experiences</a></li>
		            <li><a href="#fa">Family Data</a></li>
		            <li><a href="#la">Languages</a></li>
		            <li><a href="#or">Organizations</a></li>
		            <li><a href="#re">References</a></li>
		            <li><a href="#qa">Qualification</a></li>
          		</ul>
       			<!-- START DATA TABLE -->
          		<div id='ed'>
	            	<input type="button" class="btn btn-primary btn-sm newEducation" id="addEducation" name="addEducation" data-toggle="modal" data-target="#educationModal" value="Add Detail">
		            <table class="table table-hover table-bordered" id="educationTable">
		              	<thead class="thead-dark">
		                	<tr>
				                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">ID</th>
				                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 139px;">Institution</th>
				                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 118px;">Major</th>
				                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 169px;">Year</th>
				                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 176px;">City</th>
				                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 176px;">Certificate</th>
				                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 138px;">Delete</th>
		                	</tr>
		              	</thead>
		            </table>
          		</div>
          		<div  id='tr'>
	            	<input type="button" class="btn btn-primary btn-sm newTraining" id="addTraining" name="addTraining" data-toggle="modal" data-target="#trainingModal" value="Add Detail">
	            	<table class="table table-hover table-bordered" id="trainingTable">
	              		<thead class="thead-dark">
	                		<tr>
	                			<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">ID</th>
			                  	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 139px;">Institution</th>
			                  	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 118px;">Major</th>
			                  	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 169px;">Year</th>
			                  	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 176px;">City</th>
			                  	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 176px;">Certificate</th>
			                  	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 138px;">Delete</th>
			                </tr>
	              		</thead>
	            	</table>
          		</div>
          		<div  id='ex'>
		            <input type="button" class="btn btn-primary btn-sm newExperience" id="addExperience" name="addExperience" data-toggle="modal" data-target="#experienceModal" value="Add Detail">
		            <table class="table table-hover table-bordered" id="experienceTable">
	              		<thead class="thead-dark">
		                	<tr>
		                		<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">ID</th>
			                  	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 139px;">Company</th>
			                  	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 118px;">Position</th>
			                  	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 130px;">Join Date</th>
			                  	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Current</th>
			                  	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 150px;">End Of Working</th>
			                  	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 150px;">Basic Salary</th>
			                  	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 130px;">Allowance</th>
			                  	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 176px;">Job Desc</th>
			                  	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 195px;">Quit Reason</th>
			                  	<!-- <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 195px;">Information</th> -->
			                  	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 120px;">Delete</th>
		                  	</tr>
	              		</thead>
            		</table>
          		</div>
          		<div  id='fa'>
		            <input type="button" class="btn btn-primary btn-sm newFamily" id="addFamily" name="addFamily" data-toggle="modal" data-target="#familyModal" value="Add Detail">
		            <table class="table table-hover table-bordered" id="familyTable">
		              	<thead class="thead-dark">
		                	<tr>
		                		<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">ID</th>
		                  		<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 139px;">Name</th>
		                  		<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 118px;">Relationship</th>
		                  		<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 169px;">Date Of Birth</th>
		                  		<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 176px;">Education</th>
		                  		<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 176px;">Occupation</th>
		                  		<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 138px;">Delete</th>
		                  	</tr>
		              	</thead>
		            </table>
	          	</div>
          		<div  id='la'>
		            <input type="button" class="btn btn-primary btn-sm" id="addLanguage" name="addLanguage" data-toggle="modal" data-target="#languageModal" value="Add Detail">
		            <table class="table table-hover table-bordered" id="languageTable">
		              	<thead class="thead-dark">
		                	<tr>
		                		<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">ID</th>
		                  		<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 139px;">Language</th>
		                  		<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 118px;">Writing</th>
		                  		<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 169px;">Oral</th>
		                  		<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 176px;">Information</th>
		                  		<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 138px;">Delete</th>
		                  	</tr>
		              	</thead>
		            </table>
		        </div>
		        <div  id='or'>
		            <input type="button" class="btn btn-primary btn-sm" id="addOrganization" name="addOrganization" data-toggle="modal" data-target="#organizationModal" value="Add Detail">
		            <table class="table table-hover table-bordered" id="organizationTable">
		              	<thead class="thead-dark">
		                	<tr>
		                		<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">ID</th>
		                  		<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 139px;">Organization</th>
		                  		<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 169px;">Year</th>
		                  		<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 176px;">Position</th>
		                  		<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 138px;">Delete</th>
		                  	</tr>
		              	</thead>
		            </table>
		        </div>
	          	<div  id='re'>
	            	<input type="button" class="btn btn-primary btn-sm" id="addReference" name="addReference" data-toggle="modal" data-target="#referenceModal" value="Add Detail">
	            	<table class="table table-hover table-bordered" id="referenceTable">
	              		<thead class="thead-dark">
	                		<tr>
	                			<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">ID</th>
	                  			<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 139px;">Name</th>
	                  			<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 118px;">Relationsship</th>
	                  			<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 169px;">Address</th>
	                  			<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 176px;">Contact No</th>
	                  			<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 138px;">Delete</th>
	                  		</tr>
	              		</thead>
	            	</table>
	          	</div>
	          	<div  id='qa'>
	            	<input type="button" class="btn btn-primary btn-sm newQualification" id="addQualification" name="addQualification" data-toggle="modal" data-target="#qualificationModal" value="Add Detail">
	            	<table class="table table-hover table-bordered" id="qualificationTable">
	              		<thead class="thead-dark">
	                		<tr>
	                			<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">ID</th>
	                  			<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 150px;">Qualification</th>
	                  			<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Year Period</th>
	                  			<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Certificate</th>
	                  			<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Delete</th></tr>
	              		</thead>
	            	</table>
	          	</div>
        	</div>
        	<!-- END DATA TABLE -->
    	</div>
  	</div>
</div>

<div class="container">
	<!-- START DETAIL BIODATA -->
		<div id="detail" class="panel panel-body panelButtonDetail">
				<!-- START FORM MODAL -->
				<!-- Start editBiodataModal -->
				<!-- <h2>Edit Biodata</h2> -->
				<div class="modal fade" id="editBiodataModal" tabindex="-1" role="dialog" aria-labelledby="editBiodataModalLabel"> 
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<!-- Start modal-header -->
							<div class="modal-header">
			                    <h4 class="modal-title" id="editBiodataModalLabel">Edit Biodata</h4>
			                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			                </div>
			                <!-- End modal-header -->
			                <!-- Start modal-body -->
			              	<div class="modal-body">
			              		<!-- Start Form EDIT PAYROLL -->
			                    <form method="post" class="my_detail">
			                        <div class="form-group">
			                            <label class="control-label" for="biodataId">Id</label>
			                            <input class="form-control" type="text" id="biodataId" name="biodataId" placeholder="Id" readonly="">
			                        </div>                     

			                        <div class="form-group">
			                            <label class="control-label" for="fullName">Name</label>
			                            <input class="form-control" type="text" id="fullName" name="fullName" placeholder="Nama Karyawan" readonly="">
			                        </div>
									
									<div class="form-group">
			                            <label class="control-label" for="gender">Gender</label>
			                            <code id="genderErr" class="errMsg"><span>*</span></code>
			                            <select class="form-control" id="gender" name="gender" required="">
											<option value="" disabled="" selected="">Pilih</option>
											<option value="L">Laki-Laki</option>
											<option value="P">Perempuan</option>
										</select>
			                        </div>

			                        <div class="form-group">
			                            <label class="control-label" for="birthPlace">Birth Place</label>
			                            <!-- <span> : Wajib Diisi</span></code> -->
			                            <code id="birthPlaceErr" class="errMsg"><span>*</span></code>	
			                            <input class="form-control" type="text" id="birthPlace" name="birthPlace" placeholder="Tempat Lahir">
			                        </div>

			                        <div class="form-group">
			                            <label class="control-label" for="birthDay">Birth Date</label>
			                            <code id="birthDayErr" class="errMsg"><span>*</span></code>	
			                            <input class="form-control" type="date" id="birthDay" name="birthDay" placeholder="Tanggal Lahir">
			                        </div>

			                        <div class="form-group">
			                            <label class="control-label" for="pointOfHire">Point Of Hire</label>
			                            <code id="pointOfHireErr" class="errMsg"><span>*</span></code>	
			                            <select class="form-control" id="pointOfHire" name="pointOfHire" required="">
											<option value="" disabled="" selected="">Pilih</option>
											<option value="L">Local</option>
											<option value="NL">Non Local</option>
											<option value="NP">Non Papua</option>
											<option value="PO">Papua Others</option>
											<option value="PO">Papua 7 Ethnic</option>
										</select>
			                        </div> 

			                        <div class="form-group">
			                            <label class="control-label" for="nationality">Nationality</label>
			                            <code id="nationalityErr" class="errMsg"><span>*</span></code>					
			                            <input type="text" class="form-control" id="nationality" name="nationality" placeholder="Warga Negara" required="">
			                        </div>

			                        <div class="form-group">
			                            <label class="control-label" for="etnics">Etnics</label>
			                            <code id="etnicsErr" class="errMsg"><span>*</span></code>					
			                            <select class="form-control" id="etnics" name="etnics" required="">
											<option value="" disabled="" selected="">Pilih</option>
											<option value="Jawa">Jawa</option>
											<option value="Sunda">Sunda</option>
											<option value="Melayu">Melayu</option>
											<option value="Menado">Menado</option>						
											<option value="Batak">Batak</option>
											<option value="Toraja">Toraja</option>
											<option value="Papua">Papua</option>
											<option value="Padang">Padang</option>
											<option value="Lain">Lain-Lain</option>
										</select>
			                        </div>

			                        <div class="form-group">
			                            <label class="control-label" for="idNo">Id No</label>
			                            <code id="idNoErr" class="errMsg"><span>*</span></code>	
			                            <input type="text" class="form-control" id="idNo" name="idNo" placeholder="Nomor KTP" required="">
			                        </div>

			                        <div class="form-group">
			                            <label class="control-label" for="idAddress">Id Address</label>
			                            <code id="idAddressErr" class="errMsg"><span>*</span></code>							
			                            <input type="text" class="form-control" id="idAddress" name="idAddress" placeholder="Alamat KTP" required="">
			                        </div>

			                        <div class="form-group">
			                            <label class="control-label" for="currentAddress">Current Address</label>
			                            <code id="currentAddressErr" class="errMsg"><span>*</span></code>								
			                            <input type="text" class="form-control" id="currentAddress" name="currentAddress" placeholder="Alamat Sekarang" required="">
			                        </div>

			                        <div class="form-group">
										<label class="control-label" for="cityAddress">City</label>								
										<select class="form-control" id="cityAddress" name="cityAddress" required="">
											<option value="" disabled="" selected="">Pilih</option>
										</select>	
									</div>

			                        <div class="form-group">
			                            <label class="control-label" for="npwpNo">Taxpayer No</label>
										<input type="text" class="form-control" id="npwpNo" name="npwpNo" placeholder="Nomor NPWP" required="">
			                        </div>

			                        <div class="form-group">
			                            <label class="control-label" for="npwpNo">BPJS No</label>
										<input type="text" class="form-control" id="bpjsNo" name="bpjsNo" placeholder="Nomor BPJS" required="">
			                        </div>

			                        <div class="form-group">
			                            <label class="control-label" for="livingStatus">Living Status</label>
			                            <code id="livingStatusErr" class="errMsg"><span>*</span></code>								
			                            <select class="form-control" id="livingStatus" name="livingStatus" required="">
											<option value="" disabled="" selected="">Pilih</option>
											<option value="Milik Pribadi">Milik Pribadi</option>
											<option value="Milik Orang Tua">Milik Orang Tua</option>
											<option value="Rumah Kontrakan">Rumah Kontrakan</option>
											<option value="Indekost">Indekost</option>				
											<option value="None">None</option>				
										</select>
			                        </div>

									<div class="form-group">
			                            <label class="control-label" for="birthPlace">Religion</label>
			                            <code id="religionErr" class="errMsg"><span>*</span></code>									
			                            <select class="form-control" id="religion" name="religion" required="">
											<option value="" disabled="" selected="">Pilih</option>
											<option value="Islam">Islam</option>
											<option value="Kristen">Kristen</option>
											<option value="Katolik">Katolik</option>
											<option value="Budha">Budha</option>				
											<option value="Hindu">Hindu</option>				
											<option value="Khonghucu">Khonghucu</option>
											<option value="Lain">Lain-Lain</option>				
										</select>
			                        </div>

									<div class="form-group">
			                            <label class="control-label" for="driveLicence">Driving License</label>
			                            <input type="text" class="form-control" id="driveLicence" name="driveLicence" placeholder="A,B,C,..">
			                        </div>

			                        <div class="form-group">
			                            <label class="control-label" for="maritalStatus">Status</label>
			                            <code id="maritalStatusErr" class="errMsg"><span>*</span></code>									
			                            <select class="form-control" id="maritalStatus" name="maritalStatus" required="">
											<option value="" disabled="" selected="">Pilih</option>
											<option value="TK0">TK-0</option>
											<option value="TK1">TK-1</option>
											<option value="TK2">TK-2</option>
											<option value="TK3">TK-3</option>
											<option value="K0">K-0</option>
											<option value="K1">K-1</option>
											<option value="K2">K-2</option>
											<option value="K3">K-3</option>
											<option value="Lain">Lain-Lain</option>											
										</select>
			                        </div>	

									<div class="form-group">
			                            <label class="control-label" for="groupPosition">Group Position</label>
			                            <code id="groupPositionErr" class="errMsg"><span>*</span></code>								
			                            <select class="form-control" id="groupPosition" name="groupPosition" required="">
											<option value="" disabled="" selected="">Pilih</option>
										</select>
			                        </div>

			                        <div class="form-group">
			                            <label class="control-label" for="groupPosition">Position</label>
			                            <code id="appliedPositionErr" class="errMsg"><span>*</span></code>								
			                            <select class="form-control" id="appliedPosition" name="appliedPosition" required="">
											<option value="" disabled="" selected="">Pilih</option>
										</select>
			                        </div>

			                        <div class="form-group">
			                            <label class="control-label" for="cvDate">CV Date</label>
			                            <code id="cvDateErr" class="errMsg"><span>*</span></code>									
			                            <input type="Date" class="form-control" id="cvDate" name="cvDate" placeholder="Tanggal CV" required="">
			                        </div>	

									<div class="form-group">
			                            <label class="control-label" for="height">Height</label>
			                            <code id="heightErr" class="errMsg"><span>*</span></code>										
			                            <input type="number" class="form-control" id="height" name="height" min="0" placeholder="Tinggi Badan" required="">
			                        </div>

			                        <div class="form-group">
			                            <label class="control-label" for="weight">Weight</label>
			                            <code id="weightErr" class="errMsg"><span>*</span></code>	
			                            <input type="number" class="form-control" id="weight" name="weight" min="0" placeholder="Berat Badan" required="">
			                        </div>

			                        <div class="form-group">
			                            <label class="control-label" for="colorBlindness">Color Blindness</label>
			                            <code id="colorBlindnessErr" class="errMsg"><span>*</span></code>
			                            <select class="form-control" id="colorBlindness" name="colorBlindness">
											<option value="" disabled="" selected="">Pilih</option>
											<option value="1">Ya</option>
											<option value="0">Tidak</option>
										</select>
			                        </div>

			                        <div class="form-group">
			                            <label class="control-label" for="bloodType">Blood Type</label>
			                            <code id="bloodTypeErr" class="errMsg"><span>*</span></code>	
			                            <select class="form-control" id="bloodType" name="bloodType" required="">
											<option value="" disabled="" selected="">Pilih</option>
											<option value="A">A</option>
											<option value="B">B</option>
											<option value="AB">AB</option>
											<option value="O">O</option>				
											<option value="None">None</option>				
										</select>
			                        </div>

			                        <div class="form-group">
			                            <label class="control-label" for="emailAddress">Email</label>
			                            <code id="emailAddressErr" class="errMsg"><span>*</span></code>			
			                            <input type="email" class="form-control" id="emailAddress" name="emailAddress" placeholder="Alamat Email" required="">
			                        </div>

			                        <div class="form-group">
			                            <label class="control-label" for="telpNo">Phone No</label>
			                            <code id="telpNoErr" class="errMsg"><span>*</span></code>				
			                            <input type="text" class="form-control" id="telpNo" name="telpNo" placeholder="Nomor Telepon" required="">
			                        </div>

			                        <div class="form-group">
			                            <label class="control-label" for="cellNo">Celular No</label>
			                            <code id="cellNoErr" class="errMsg"><span>*</span></code>	
			                            <input type="text" class="form-control" id="cellNo" name="cellNo" placeholder="Nomor HP" required="">
			                        </div>			                                             

			                        <div class="form-group">
			                            <!-- <label class="control-label" for="rowIdx">Index</label> -->
			                            <input class="form-control" type="hidden" id="rowIdx" name="rowIdx" value="" readonly="">
			                        </div>                                                                                                                                     
			                    </form>
			                    <!-- End Form EDIT PAYROLL -->  
			              	</div> 
			              <!-- End modal-body -->
			              <!-- Start modal-footer -->
			              <div class="modal-footer">
			              	<!-- <input type="hidden" id="rowIdx" name="rowIdx" value=""> -->
			                <input type="button" class="btn btn-primary btn-sm" id="saveBiodata" name="saveBiodata" data-dismiss="modal" value="Simpan"> 
			                <input type="button" class="btn btn-primary btn-sm" data-dismiss="modal" value="Tutup"> 
			              </div>
			              <!-- End modal-footer -->
						</div>
					</div> 
				</div> 
				<!-- End editBiodataModal -->
				<!-- END FORM MODAL -->


				<!-- START DATA PENDIDIKAN -->
				<!-- Start educationModal -->				
				<div class="modal fade" id="educationModal" tabindex="-1" role="dialog" aria-labelledby="educationModalLabel"> 
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<!-- Start modal-header -->
							<div class="modal-header">
		                        <h4 class="modal-title" id="educationModalLabel">Educations</h4>
		                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                        </div>
	                        <!-- End modal-header -->
	                        <!-- Start modal-body -->
	                      	<div class="modal-body">
	                      		<!-- Start Form PENDIDIKAN -->
		                        <form method="post" class="my_detail">
		                            <div class="form-group">
		                                <label class="control-label" for="schoolName">Institutions<code id="schoolNameErr" class="errMsg"><span> : Wajib Diisi</span></code></label>
		                                <input class="form-control" type="text" id="schoolName" name="schoolName" placeholder="Nama Lembaga">
		                            </div> 

		                            <div class="form-group">
		                                <label class="control-label" for="educationMajor">Major<code id="educationMajorErr" class="errMsg"><span> : Wajib Diisi</span></code></label>
		                                <input class="form-control" type="text" id="educationMajor" name="educationMajor" placeholder="Nama Jurusan">
		                            </div> 

		                            <div class="form-group">
										<label class="control-label" for="educationYear">Year<code id="educationYearErr" class="errMsg"><span> : Wajib Dipilih</span></code></label>
										<select class="form-control" id="educationYear" name="educationYear">
											<option value="" disabled="" selected="">Pilih</option>
											<script type="text/javascript">
												var startYear = <?php echo  date("Y")?>;
												var endYear = startYear - 80;							
												for (var i = startYear; i >= endYear; i--) 
												{
													document.write("<option value='"+i+"'>"+i+"</option>");						
												}
											</script>
										</select>					
									</div>
		                            
		                            <div class="form-group">
		                                <label class="control-label" for="educationCity">City<code id="educationCityErr" class="errMsg"><span> : Wajib Dipilih</span></code></label>
		                                <input class="form-control" type="text" id="educationCity" name="educationCity" placeholder="Nama Kota">
		                            </div>

		                            <div class="form-group">
		                                <label class="control-label" for="isCertified">Certificate</label><br>
		                                <input type="checkbox" checked="" id="isCertified" name="isCertified" value="1">
		                            </div>                                                                                                                       
		                        </form>
		                        <!-- End Form PENDIDIKAN -->  
	                      	</div> 
	                      <!-- End modal-body -->
	                      <!-- Start modal-footer -->
	                      <div class="modal-footer">
	                        <input type="button" class="btn btn-primary btn-sm" id="saveEducation" name="saveEducation" value="Simpan"> 
	                        <input type="button" class="btn btn-primary btn-sm" data-dismiss="modal" value="Tutup"> 
	                      </div>
	                      <!-- End modal-footer -->
						</div>
					</div> 
				</div> 
				<!-- End educationModal -->
				<!-- END DATA PENDIDIKAN -->

				<!-- START DATA TRAINING -->
				<!-- Start trainingModal -->
				<div class="modal fade" id="trainingModal" tabindex="-1" role="dialog" aria-labelledby="trainingModalLabel"> 
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<!-- Start modal-header -->
							<div class="modal-header">
				                <h4 class="modal-title" id="trainingModalLabel">Trainings</h4>
				                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				            </div>
				            <!-- End modal-header -->
				            <!-- Start modal-body -->
				          	<div class="modal-body">
				          		<!-- Start Form TRAINING -->
				                <form method="post" class="my_detail">
				                    <div class="form-group">
				                        <label class="control-label" for="instituteName">Institutions<code id="instituteNameErr" class="errMsg"><span> : Wajib Diisi</span></code></label>
				                        <input class="form-control" type="text" id="instituteName" name="instituteName" placeholder="Nama Lembaga">
				                    </div> 	

				                    <div class="form-group">
				                        <label class="control-label" for="classesField">Field<code id="classesFieldErr" class="errMsg"><span> : Wajib Diisi</span></code></label>
				                        <input class="form-control" type="text" id="classesField" name="classesField" placeholder="Bidang Training">
				                    </div> 			                    

				                    <div class="form-group">
										<label class="control-label" for="trainingYear">Year<code id="trainingYearErr" class="errMsg"><span> : Wajib Diisi</span></code></label>
										<select class="form-control" id="trainingYear" name="trainingYear">
											<option value="" disabled="" selected="">Pilih</option>
											<script type="text/javascript">
												var startYear = <?php echo  date("Y")?>;
												var endYear = startYear - 80;							
												for (var i = startYear; i >= endYear; i--) 
												{
													document.write("<option value='"+i+"'>"+i+"</option>");						
												}
											</script>
										</select>					
									</div>	                            
								
				                    <div class="form-group">
				                        <label class="control-label" for="trainingCity">City<code id="trainingCityErr" class="errMsg"><span> : Wajib Diisi</span></code></label>
				                        <input class="form-control" type="text" id="trainingCity" name="trainingCity" placeholder="Nama Kota">
				                    </div>

				                    <div class="form-group">
				                        <label class="control-label" for="isTrainingCertified">Certificate</label><br>
				                        <input type="checkbox" checked="" id="isTrainingCertified" name="isTrainingCertified">
				                    </div>                                                                                                                                
				                </form>
				                <!-- Start Form TRAINING -->  
				          	</div> 
				          <!-- End modal-body -->
				          <!-- Start modal-footer -->
				          <div class="modal-footer">
				            <input type="button" class="btn btn-primary btn-sm" id="saveTraining" name="saveTraining" value="Simpan"> 
				            <input type="button" class="btn btn-primary btn-sm" data-dismiss="modal" value="Tutup"> 
				          </div>
				          <!-- End modal-footer -->
						</div>
					</div> 
				</div> 
				<!-- End trainingModal -->
				<!-- END DATA TRAINING -->


				<!-- START DATA PENGALAMAN -->
				<!-- Start experienceModal -->
				<div class="modal fade" id="experienceModal" tabindex="-1" role="dialog" aria-labelledby="experienceModalLabel"> 
					<!-- Start modal-dialog -->
					<div class="modal-dialog" role="document">
						<!-- Start modal-content -->
						<div class="modal-content">
							<!-- Start modal-header -->
							<div class="modal-header">
		                        <h4 class="modal-title" id="experienceModalLabel">Work History</h4>
		                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                        </div>
	                        <!-- End modal-header -->
	                        <!-- Start modal-body -->
	                      	<div class="modal-body">
								<!-- Start Form PENGALAMAN -->
	                      		<form method="post" class="my_detail">
		                            <div class="form-group">
		                                <label class="control-label" for="experienceCompanyName">Company<code id="experienceCompanyNameErr" class="errMsg"><span> : Wajib Diisi</span></code></label>
		                                <input class="form-control" type="text" id="experienceCompanyName" name="experienceCompanyName" placeholder="Nama Perusahaan">
		                            </div> 

		                            <div class="form-group">
		                                <label class="control-label" for="experiencePosition">Position<code id="experiencePositionErr" class="errMsg"><span> : Wajib Diisi</span></code></label>
		                                <input class="form-control" type="text" id="experiencePosition" name="experiencePosition" placeholder="Nama Jabatan">
		                            </div>

									<div class="form-group">
		                                <label class="control-label" for="startWorkDate">Work Start<code id="startWorkDateErr" class="errMsg"><span> : Wajib Diisi</span></code></label>
		                                <input class="form-control" type="date" id="startWorkDate" name="startWorkDate" placeholder="Mulai Bekerja">
		                            </div>	

									<div class="form-group">
		                                <!-- <label class="control-label" for="isCurrentWork">Masih Bekerja</label><br> -->
		                                <input type="checkbox" checked="" id="isCurrentWork" name="isCurrentWork" value="1"><b>  Current</b>	
		                            </div> 

		                            <div class="form-group">
		                                <label class="control-label" for="endWorkDate">Ended Work<code id="endWorkDateErr" class="errMsg"><span></span></code></label>
		                                <input class="form-control" type="date" id="endWorkDate" name="endWorkDate" placeholder="Berhenti Bekerja">
		                            </div>

		                            <div class="form-group">
		                                <label class="control-label" for="experienceBasicSalary">Basic Salary<code id="experienceBasicSalaryErr" class="errMsg"><span> : Wajib Diisi Angka</span></code></label>
		                                <input class="form-control" type="number" min="0.00" step="0.50" id="experienceBasicSalary" name="experienceBasicSalary" placeholder="Gaji Pokok">
		                            </div>

		                            <div class="form-group">
		                                <label class="control-label" for="experienceAllowance">Allowance</label>
		                                <input class="form-control" type="number" min="0.00" step="0.50" id="experienceAllowance" name="experienceAllowance" placeholder="Nilai Tunjangan">
		                            </div>

		                            <div class="form-group">
		                                <label class="control-label" for="jobDesc">Job Desc<code id="jobDescErr" class="errMsg"><span> : Wajib Diisi</span></code></label>
		                                <input class="form-control" type="text" id="jobDesc" name="jobDesc" placeholder="Job Desc">
		                            </div>

		                            <div class="form-group">
		                                <label class="control-label" for="resignReason">Quit Reason<code id="resignReasonErr" class="errMsg"><span> : Wajib Diisi</span></code></label>
		                                <select class="form-control" id="resignReason" name="resignReason">
		                                	<option value="" disabled="" selected="">Pilih</option>	
		                                	<option value="Resign">Resign</option>	
		                                	<option value="Contract Completed">Contract Completed</option>	
		                                	<option value="Terminated">Terminated</option>	
		                                </select>
		                                <!-- <input class="form-control" type="text" id="resignReason" name="resignReason" placeholder="Alasan Berhenti"> -->
		                            </div>

		                            <div class="form-group">
		                                <label class="control-label" for="addInfo">Explanation</label>
		                                <input class="form-control" type="text" id="addInfo" name="addInfo" placeholder="Keterangan">
		                            </div>                                                                                                                                                                                                                                             
		                        </form>
		                        <!-- End Form PENGALAMAN -->  
	                      </div> 
	                      <!-- End modal-body -->
	                      <!-- Start modal-footer -->
	                      <div class="modal-footer">
	                        <input type="button" class="btn btn-primary btn-sm" id="saveExperience" name="saveExperience" value="Simpan"> 
	                        <input type="button" class="btn btn-primary btn-sm" data-dismiss="modal" value="Tutup"> 
	                      </div>
	                      <!-- End modal-footer -->
						</div>
						<!-- End modal-content -->
					</div>
					<!-- End modal-dialog --> 
				</div> 
				<!-- End experienceModal -->
				<!-- END DATA PENGALAMAN -->

				<!-- START DATA FAMILY -->
				<!-- Start familyModal -->
				<div class="modal fade" id="familyModal" tabindex="-1" role="dialog" aria-labelledby="familyModalLabel"> 
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<!-- Start modal-header -->
							<div class="modal-header">
		                        <h4 class="modal-title" id="familyModalLabel">Family</h4>
		                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                        </div>
	                        <!-- End modal-header -->
	                        <!-- Start modal-body -->
	                      	<div class="modal-body">
	                      		<!-- Start Form FAMILY -->
		                        <form method="post" class="my_detail">
		                            
									<div class="form-group">
				                        <label class="control-label" for="familyName">Name<code id="familyNameErr" class="errMsg"><span> : Wajib Diisi</span></code></label>
				                        <input class="form-control" type="text" id="familyName" name="familyName" placeholder="Nama Keluarga">
				                    </div>

		                            <div class="form-group">
				                        <label class="control-label" for="familyRelation">Relationship<code id="familyRelationErr" class="errMsg"><span> : Wajib Diisi</span></code></label>
				                        <input class="form-control" type="text" id="familyRelation" name="familyRelation" placeholder="Hubungan Keluarga">
				                    </div>

				                    <div class="form-group">
				                        <label class="control-label" for="familyDateOfBirth">Birth Date<code id="familyDateOfBirthErr" class="errMsg"><span> : Wajib Diisi</span></code></label>
				                        <input class="form-control" type="date" id="familyDateOfBirth" name="familyDateOfBirth" placeholder="Tanggal Lahir">
				                    </div>						

		                            <div class="form-group">
										<label class="control-label" for="familyEducation">Education<code id="familyEducationErr" class="errMsg"><span> : Wajib Dipilih</span></code></label>
										<select class="form-control" id="familyEducation" name="familyEducation">
											<option value="" disabled="" selected="">Pilih</option>
											<option value="None">None</option>
											<option value="TK">TK</option>
											<option value="SD">SD</option>
											<option value="SMP">SMP</option>
											<option value="SMU">SMU</option>
											<option value="S1">S1</option>						
											<option value="S2">S2</option>
											<option value="S3">S3</option>
										</select>	
									</div>	                            

		                            <div class="form-group">
				                        <label class="control-label" for="familyOccupation">Occupation</label>
				                        <input class="form-control" type="text" id="familyOccupation" name="familyOccupation" placeholder="Nama Pekerjaan">
				                    </div>                                                                                                                                  
		                        </form>  
		                        <!-- Start Form FAMILY -->
	                      	</div> 
	                      <!-- End modal-body -->
	                      <!-- Start modal-footer -->
	                      <div class="modal-footer">
	                        <input type="button" class="btn btn-primary btn-sm" id="saveFamily" name="saveFamily" value="Simpan"> 
	                        <input type="button" class="btn btn-primary btn-sm" data-dismiss="modal" value="Tutup"> 
	                      </div>
	                      <!-- End modal-footer -->
						</div>
					</div> 
				</div> 
				<!-- End familyModal -->
				<!-- END DATA FAMILY -->

				<!-- START DATA LANGUAGES -->
				<!-- Start languageModal -->
				<div class="modal fade" id="languageModal" tabindex="-1" role="dialog" aria-labelledby="languageModalLabel"> 
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<!-- Start modal-header -->
							<div class="modal-header">
		                        <h4 class="modal-title" id="languageModalLabel">Languages</h4>
		                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                        </div>
	                        <!-- End modal-header -->
	                        <!-- Start modal-body -->
	                      	<div class="modal-body">
	                      		<!-- Start Form BAHASA -->
		                        <form method="post" class="my_detail">		                            
									<div class="form-group">
										<label class="control-label" for="languageName">Language<code id="languageNameErr" class="errMsg"><span> : Wajib Diisi</span></code></label>
			                            <select class="form-control" id="languageName" name="languageName">
											<option value="" disabled="" selected="">Pilih</option>
											<option value="Indonesia">Indonesia</option>
											<option value="Inggris">Inggris</option>
											<option value="Mandarin">Mandarin</option>
											<option value="Lain">Lain</option>				
										</select>
									</div>

		                            <div class="form-group">
		                            	<label class="control-label" for="written">Written<code id="writtenErr" class="errMsg"><span> : Wajib Diisi</span></code></label>
			                            <select class="form-control" id="written" name="written">
											<option value="" disabled="" selected="">Pilih</option>
											<option value="S">Sedang</option>
											<option value="CB">Cukup Baik</option>
											<option value="B">Baik</option>
											<option value="SB">Sangat Baik</option>				
										</select>
									</div>

		                            <div class="form-group">
		                            	<label class="control-label" for="oral">Oral<code id="oralErr" class="errMsg"><span> : Wajib Diisi</span></code></label>
			                            <select class="form-control" id="oral" name="oral">
											<option value="" disabled="" selected="">Pilih</option>
											<option value="S">Sedang</option>
											<option value="CB">Cukup Baik</option>
											<option value="B">Baik</option>
											<option value="SB">Sangat Baik</option>				
										</select>
									</div>		                            

		                            <div class="form-group">
		                                <label class="control-label" for="languageInfo">Explanation</label>
		                                <input class="form-control" type="text" id="languageInfo" name="languageInfo" placeholder="Keterangan">
		                            </div>                                                                                                                                  
		                        </form> 
		                        <!-- End Form BAHASA --> 
	                      	</div> 
	                      <!-- End modal-body -->
	                      <!-- Start modal-footer -->
	                      <div class="modal-footer">
	                        <input type="button" class="btn btn-primary btn-sm" id="saveLanguage" name="saveLanguage" value="Simpan"> 
	                        <input type="button" class="btn btn-primary btn-sm" data-dismiss="modal" value="Tutup"> 
	                      </div>
	                      <!-- End modal-footer -->
						</div>
					</div> 
				</div> 
				<!-- End languageModal -->
				<!-- END DATA LANGUAGES -->	

				<!-- START DATA ORGANIZATION -->
				<!-- Start organizationModal -->
				<div class="modal fade" id="organizationModal" tabindex="-1" role="dialog" aria-labelledby="organizationModalLabel"> 
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<!-- Start modal-header -->
							<div class="modal-header">
		                        <h4 class="modal-title" id="organizationModalLabel">Organizations</h4>
		                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                        </div>
	                        <!-- End modal-header -->
	                        <!-- Start modal-body -->
	                      	<div class="modal-body">
	                      		<!-- Start Form ORGANISASI -->
		                        <form method="post" class="my_detail">		                            
									<div class="form-group">
										<label class="control-label" for="organizationName">Organization Name <code id="organizationNameErr" class="errMsg"><span> : Wajib Diisi</span></code></label>
										<input class="form-control" type="text" id="organizationName" name="organizationName" placeholder="Nama Organisasi">
									</div>

		                            <div class="form-group">
										<label class="control-label" for="organizationYear">Year<code id="organizationYearErr" class="errMsg"><span> : Wajib Diisi</span></code></label>
										<select class="form-control" id="organizationYear" name="organizationYear">
											<option value="" disabled="" selected="">Pilih</option>
											<script type="text/javascript">
												var startYear = <?php echo  date("Y")?>;
												var endYear = startYear - 80;							
												for (var i = startYear; i >= endYear; i--) 
												{
													document.write("<option value='"+i+"'>"+i+"</option>");						
												}
											</script>
										</select>		
									</div>		                            		                            

		                            <div class="form-group">
		                                <label class="control-label" for="organizationPosition">Position<code id="organizationPositionErr" class="errMsg"><span> : Wajib Diisi</span></code></label>
		                                <input class="form-control" type="text" id="organizationPosition" name="organizationPosition" placeholder="Posisi">
		                            </div>                                                                                                                                  
		                        </form>  
		                        <!-- End Form ORGANISASI -->
	                      	</div> 
	                      <!-- End modal-body -->
	                      <!-- Start modal-footer -->
	                      <div class="modal-footer">
	                        <input type="button" class="btn btn-primary btn-sm" id="saveOrganization" name="saveOrganization" value="Simpan"> 
	                        <input type="button" class="btn btn-primary btn-sm" data-dismiss="modal" value="Tutup"> 
	                      </div>
	                      <!-- End modal-footer -->
						</div>
					</div> 
				</div> 
				<!-- End organizationModal -->
				<!-- END DATA ORGANIZATION -->

				<!-- START DATA REFERENCE -->
				<!-- Start referenceModal -->
				<div class="modal fade" id="referencesModal" tabindex="-1" role="dialog" aria-labelledby="referencesModalLabel"> 
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<!-- Start modal-header -->
							<div class="modal-header">
		                        <h4 class="modal-title" id="referencesModalLabel">Refferences</h4>
		                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                        </div>
	                        <!-- End modal-header -->
	                        <!-- Start modal-body -->
	                      	<div class="modal-body">
	                      		<!-- Start Form REFERENSI -->
		                        <form method="post" class="my_detail">		                            
									<div class="form-group">
										<label class="control-label" for="referencesName">Name<code id="referencesNameErr" class="errMsg"><span> : Wajib Diisi</span></code></label>
										<input class="form-control" type="text" id="referencesName" name="referencesName" placeholder="Nama Referensi">
									</div>
									
									<div class="form-group">
										<label class="control-label" for="referencesRelation">Relationship<code id="referencesRelationErr" class="errMsg"><span> : Wajib Diisi</span></code></label>
										<input class="form-control" type="text" id="referencesRelation" name="referencesRelation" placeholder="Hubungan">
									</div>
									
									<div class="form-group">
										<label class="control-label" for="referencesAddress">Address</label>
										<input class="form-control" type="text" id="referencesAddress" name="referencesAddress" placeholder="Alamat Referensi">
									</div>	                            		                            

		                            <div class="form-group">
		                                <label class="control-label" for="referencesContact">Contact No<code id="referencesContactErr" class="errMsg"><span> : Wajib Diisi</span></code></label>
		                                <input class="form-control" type="text" id="referencesContact" name="referencesContact" placeholder="No Kontak">
		                            </div>                                                                                                                                  
		                        </form> 
		                        <!-- End Form REFERENSI --> 
	                      	</div> 
	                      <!-- End modal-body -->
	                      <!-- Start modal-footer -->
	                      <div class="modal-footer">
	                        <input type="button" class="btn btn-primary btn-sm" id="saveReferences" name="saveReferences" value="Simpan"> 
	                        <input type="button" class="btn btn-primary btn-sm" data-dismiss="modal" value="Tutup"> 
	                      </div>
	                      <!-- End modal-footer -->
						</div>
					</div> 
				</div> 
				<!-- End referenceModal -->
				<!-- END DATA REFERENCE -->

				<!-- START DATA QUALIFICATION -->
				<!-- Start qualificationModal -->
				<div class="modal fade" id="qualificationModal" tabindex="-1" role="dialog" aria-labelledby="qualificationModalLabel"> 
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<!-- Start modal-header -->
							<div class="modal-header">
		                        <h4 class="modal-title" id="qualificationModalLabel">Qualification</h4>
		                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                        </div>
	                        <!-- End modal-header -->
	                        <!-- Start modal-body -->
	                      	<div class="modal-body">
	                      		<!-- Start Form REFERENSI -->
		                        <form method="post" class="my_detail">		                            
									<div class="form-group">
										<label class="control-label" for="qualification">Qualification<code id="qualificationErr" class="errMsg"><span> : Wajib Diisi</span></code></label>
										<input class="form-control" type="text" id="qualification" name="qualification" placeholder="Qualification">
									</div>
									
									<div class="form-group">
										<label class="control-label" for="qualificationYear">Year<code id="qualificationYearErr" class="errMsg"><span> : Wajib Diisi</span></code></label>
										<select class="form-control" id="qualificationYear" name="qualificationYear">
											<option value="" disabled="" selected="">Pilih</option>
											<script type="text/javascript">
												var startYear = <?php echo  date("Y")?>;
												var endYear = startYear - 80;							
												for (var i = startYear; i >= endYear; i--) 
												{
													document.write("<option value='"+i+"'>"+i+"</option>");						
												}
											</script>
										</select>		
									</div>	
									
									<div class="form-group">
				                        <label class="control-label" for="isQualificationCertified">Certificate</label><br>
				                        <input type="checkbox" checked="" id="isQualificationCertified" name="isQualificationCertified">
				                    </div> 
				                    <!-- <div class="form-group">
				                        <label class="control-label" for="isTrainingCertified">Certificate</label><br>
				                        <input type="checkbox" checked="" id="isTrainingCertified" name="isTrainingCertified">
				                    </div>   -->                                                                                                              
		                        </form> 
		                        <!-- End Form REFERENSI --> 
	                      	</div> 
	                      <!-- End modal-body -->
	                      <!-- Start modal-footer -->
	                      <div class="modal-footer">
	                        <input type="button" class="btn btn-primary btn-sm" id="saveQualification" name="saveQualification" value="Simpan"> 
	                        <input type="button" class="btn btn-primary btn-sm" data-dismiss="modal" value="Tutup"> 
	                      </div>
	                      <!-- End modal-footer -->
						</div>
					</div> 
				</div> 
				<!-- End referenceModal -->
				<!-- END DATA REFERENCE -->

</div>






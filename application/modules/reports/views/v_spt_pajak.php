<style type="text/css">
tr.selected {
  background-color: #B0BED9!important;
}
</style>

<div id="loader"></div>

<!--START FORM DATA -->
<div class="col-md-12" id="is_transaction">
  <div class="tile bg-info">
    <!-- <h3 class="tile-title">Barang Masuk</h3> -->
    <div class="tile-body">
      <form class="row is_header">
      <!-- <form class="row is_header" action="<?php #echo base_url() ?>transactions/sumbawa/Timesheet/payrollProcess"> -->
        
        <div class="form-group col-sm-12 col-md-2">
          <label class="control-label">CLIENT</label>
          <code id="docKindErr" class="errMsg"><span> : Required</span></code>
          <select class="form-control" id="clientName" name="clientName" required="">
    				<option value="" disabled="" selected="">Pilih</option>
            <!-- <option value="AMNT_Sumbawa">AMNT Sumbawa</option> -->
            <option value="Pontil_Sumbawa">Pontil Sumbawa</option>
            <!-- <option value="Pontil_Banyuwangi">Pontil Banyuwangi</option> -->
    		  </select>
        </div>

        <div class="form-group col-sm-12 col-md-2">
          <label class="control-label">YEAR</label>
          <code id="docKindErr" class="errMsg"><span> : Required</span></code>
          <select class="form-control" id="yearPeriod" name="yearPeriod" required="">
      			<option value="" disabled="" selected="">Pilih</option>
      			<script type="text/javascript">
      				var dt = new Date();
      				var currYear = dt.getFullYear();
      				var currMonth = dt.getMonth();
                      var currDay = dt.getDate();
                      var tmpDate = new Date(currYear + 1, currMonth, currDay);
                      var startYear = tmpDate.getFullYear();
      				var endYear = startYear - 80;							
      				for (var i = startYear; i >= endYear; i--) 
      				{
      					document.write("<option value='"+i+"'>"+i+"</option>");						
      				}
      			</script>
      		</select>
        </div>

        <!-- <div class="form-group col-sm-12 col-md-2">
          <label class="control-label">MONTH</label>
          <code id="docKindErr" class="errMsg"><span> : Required</span></code>
          <select class="form-control" id="monthPeriod" name="monthPeriod" required="">
    				<option value="" disabled="" selected="">Pilih</option>
    				<script type="text/javascript">
    					var tMonth = 1;
    					for (var i = tMonth; i <= 12; i++) 
    					{
    						if(i < 10)
    						{
    							document.write("<option value='0"+i+"'>0"+i+"</option>");							
    						}
    						else
    						{
    							document.write("<option value='"+i+"'>"+i+"</option>");								
    						}
    						
    					}

    				</script>
    			</select>
        </div> -->

        
               
        <div class="form-group col-md-12 align-self-end">
         <!--  <button class="btn btn-warning" id="btnProsesRoster" type="button"><i class="fa fa-refresh"></i>Roster Process</button> -->
          <button class="btn btn-warning" id="btnDisplay" type="button"><i class="fa fa-refresh"></i>Display Data</button>
		      <br>
		      <!-- <br> -->
    	     <h3><code id="dataProses" class="backTransparent"><span></span></code></h3>	
        </div>

      </form>
    </div>
  </div>
</div>
<!-- END FORM DATA -->

<div class="col-md-12">
  <div class="tile bg-info">
    <!-- <h3 class="tile-title">Barang Masuk</h3> -->
    <div class="table-responsive">
       <!-- START DATA TABLE -->
        <!-- <div class="tile-body"> -->
          <table id="sptTable" class="table table-hover table-bordered" class="table table-striped dataTable no-footer" cellspacing="0" width="%" role="grid" style="width: 100%;">
            <thead class="thead-dark">           
              <tr role="row">
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Spt Id</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Client Name</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Employee Name</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Position</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Action</th>
              </tr>
            </thead>                    
        
          </table>
        </div>
        <!-- END DATA TABLE -->
    </div>
  </div>
</div>




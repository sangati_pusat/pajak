<div id="loader"></div>

<!--START FORM DATA -->
<div class="col-md-12" id="is_transaction">
  <div class="tile bg-info">
    <div class="tile-body">
      <form class="row is_header">        
        <div class="form-group col-sm-12 col-md-2">
          <label class="control-label">User</label>
          <code id="UserErr" class="errMsg"><span> : Required</span></code>
          <select class="form-control" id="user" name="user" required="">
          </select>
        </div>
        <div class="form-group col-sm-12 col-md-2">
          <label class="control-label">Start Date</label>
          <code id="StartErr" class="errMsg"><span> : Required</span></code>
          <input class="form-control" type="text" id="startDate" name="startDate" placeholder="yyyy-mm-dd" required ="">
        </div>
        <div class="form-group col-sm-12 col-md-2">
            <label class="control-label">End Date</label>
            <code id="EndErr" class="errMsg"><span> : Required</span></code>
            <input class="form-control" type="text" id="endDate" name="endDate" placeholder="yyyy-mm-dd" required ="">
        </div>
            
        <div class="form-group col-md-12 align-self-end">
    		  <button class="btn btn-warning btnProcessPanel" type="button" id="viewCsv" name="viewCsv">
        	<span class="fa fa-list"></span> VIEW
        	</button>
    		  <!-- <button class="btn btn-warning btnProcessPanel" type="button" id="printToFile" name="printToFile"> -->
      		<!-- <span class="fa fa-print"></span> DATA PRINT -->
        	<!-- </button> -->
          <button class="btn btn-warning print" type="button" id="print" name="print">
          <span class="fa fa-print"></span> PRINT
          </button>
		      <br>
        	<h3><code id="dataProses" class="backTransparent"><span></span></code></h3>	
        </div>
      </form>
    </div>
  </div>
</div>
<!-- END FORM DATA -->

<div class="col-md-12">
  <div class="tile bg-info">
    <div class="tile-body">
       <!-- START DATA TABLE -->
        <div>
          <h3><u>Biodata History Update</u></h3>
        </div>
        <div class="tile-body">
          <div class="table-responsive">
            <table class="table table-hover table-bordered" id="contractHist">
              <thead class="thead-dark">
                <tr>
                  <th>Bio ID</th>
                  <th>Name</th>
                  <th>Gender</th>
                  <th>Local Foreign</th>
                  <!-- <th>Nationality</th>
                  <th>Clan</th> -->
                  <th>Id Card No</th>
                  <th>NPWP NO</th>
                  <th>BPJS NO</th>
                  <th>Address</th>
                  <th>City Address</th>
                  <th>Religion</th>
                  <!-- <th>Driving License</th> -->
                  <th>Marital Status</th>
                  <th>Position</th>
                  <!-- <th>Height</th>
                  <th>Weight</th>
                  <th>Blood</th> -->
                  <!-- <th>Email</th> -->
                  <!-- <th>Telp No</th> -->
                  <th>Cell No</th>
                  <th>Cv Date</th>
                  <th>Edit Time</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
        <!-- END DATA TABLE -->
        <br/>
        <div>
          <h3><u>Salary History Update</u></h3>
        </div>
        <div class="tile-body">
          <div class="table-responsive">
            <table class="table table-hover table-bordered" id="salaryHist">
              <thead class="thead-dark">
                <tr>
                  <th>Salary ID</th>
                  <th>Bio ID</th>
                  <th>Name</th>
                  <th>NIE</th>
                  <th>Company Name</th>
                  <th>Salary Level</th>
                  <th>Basic Salary</th>
                  <!-- <th>Bank ID</th> -->
                  <th>Bank Name</th>
                  <th>Account Name</th>
                  <th>Account No</th>
                  <th>Payroll Group</th>
                  <th>Pic Edit Account</th>
                  <th>Edit TIme Account</th>
                  <th>PIC Edit</th>
                  <th>Edit Time</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
    </div>
  </div>
</div>

<div class="modal fade" id="editPayrollModal" tabindex="-1" role="dialog" aria-labelledby="editPayrollModalLabel"> 
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <!-- Start modal-header -->
      <div class="modal-header">
        <h4 class="modal-title" id="editPayrollModalLabel">Contract History</h4>
        </div>
        <!-- End modal-header -->
        <!-- Start modal-body -->
        <div class="modal-body">
        <!-- Start Form EDIT PAYROLL -->
          <form method="post" class="my_detail">
            <div class="form-group">
              <label class="control-label" for="biodataId">Id</label>
              <input class="form-control" type="text" id="biodataId" name="biodataId" placeholder="Id" readonly="">
            </div>                     

            <div class="form-group">
              <label class="control-label" for="empName">Name</label>
              <input class="form-control" type="text" id="empName" name="empName" placeholder="Nama Karyawan" readonly="">
            </div>

            <div class="form-group">
              <label class="control-label" for="clientNameDetail">Client Name</label>
              <input class="form-control" type="text" id="clientNameDetail" name="clientNameDetail" placeholder="Nama Klien" readonly="">
            </div>

            <div class="row">
              <div class="table-responsive">
              <!-- START TABLE PENGALAMAN --> 
                <div class="table-responsive">
                  <table id="contractDetailTable" class="table table-striped dataTable no-footer" cellspacing="0" width="100%" role="grid" style="width: 100%;">
                    <thead>
                      <tr role="row">
                        <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">SEQUENCES</th>
                        <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">CONTRACT START</th>
                        <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">CONTRACT END</th>
                    </thead>                    
                  </table>  
                </div>
                   <!-- END TABLE PENGALAMAN -->            
              </div>
            </div>
            <!-- END DATA PAYROLL -->                                                                              
          </form>
          <!-- End Form EDIT PAYROLL -->  
        </div> 
        <!-- End modal-body -->
        <!-- Start modal-footer -->
        <div class="modal-footer">
          <input type="button" class="btn btn-primary btn-sm" data-dismiss="modal" value="Close"> 
        </div>
        <!-- End modal-footer -->
    </div>
  </div> 
</div> 
<!-- End editPayrollModal -->
<!-- END FORM MODAL -->
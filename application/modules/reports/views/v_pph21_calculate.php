<div id="loader"></div>

<!--START FORM DATA -->
<div class="col-md-12" id="is_transaction">
  <div class="tile bg-info">
    <div class="tile-body">
      <form class="row is_header">        
        <div class="form-group col-sm-12 col-md-2">
          <label class="control-label">CLIENT</label>
          <code id="clientNameErr" class="errMsg"><span> : Required</span></code>
          <select class="form-control" id="clientName" name="clientName" required="">
    				<option value="" disabled="" selected="">Pilih</option>
    				<option value="Pontil_Timika">Pontil Timika</option>
         <!--    <option value="Redpath_Timika">Redpath Timika</option>
            <option value="Pontil_Sumbawa">Pontil Sumbawa</option>
            <option value="Trakindo_Sumbawa">Trakindo Sumbawa</option> -->
    		  </select>
        </div>

        <div class="form-group col-sm-12 col-md-2">
          <label class="control-label">YEAR</label>
          <code id="yearPeriodErr" class="errMsg"><span> : Required</span></code>
          <select class="form-control" id="yearPeriod" name="yearPeriod" required="">
      			<option value="" disabled="" selected="">Pilih</option>
      			<script type="text/javascript">
      				var dt = new Date();
      				var currYear = dt.getFullYear();
      				var currMonth = dt.getMonth();
              var currDay = dt.getDate();
              var tmpDate = new Date(currYear + 1, currMonth, currDay);
              var startYear = tmpDate.getFullYear();
      				var endYear = startYear - 5;							
      				for (var i = startYear; i >= endYear; i--) 
      				{
      					document.write("<option value='"+i+"'>"+i+"</option>");						
      				}
      			</script>
      		</select>
        </div>

        <div class="form-group col-sm-12 col-md-2 dept">
          <label class="control-label">DEPT </label>
          <code id="docKindErr" class="errMsg"><span> : Required</span></code>
          <select class="form-control" id="dept" name="dept" required="">
            <option value="" disabled="" selected="">Pilih</option>
          </select>
        </div>

        <div class="form-group col-sm-12 col-md-2 payrollGroup">
          <label class="control-label" for="payrollGroup">GROUP</label>
          <select class="form-control" id="payrollGroup" name="payrollGroup" required="">
            <option value="" disabled="" selected="">Pilih</option>
            <!-- <option value="All">All</option> -->
            <option value="A">A</option>
            <option value="B">B</option>
            <option value="C">C</option>
            <option value="D">D</option>
            <option value="E">E</option>
            <option value="F">F</option>
            <option value="G">G</option>
            <option value="H">H</option>
            <option value="I">I</option>
            <option value="J">J</option>
            <option value="K">K</option>
            <option value="L">L</option>
            <option value="M">M</option>
            <option value="N">N</option>
            <option value="O">O</option>
            <option value="P">P</option>
          </select>
        </div>
            
        <div class="form-group col-md-12 align-self-end">
    			<button class="btn btn-warning btnProcessPanel" type="button" id="viewPph" name="viewPph">
        		<span class="fa fa-list"></span> DATA DISPLAY
        	</button>
    			<button class="btn btn-warning btnProcessPanel" type="button" id="printPph" name="printPph">
      		  <span class="fa fa-print"></span> DATA PRINT
        	</button>
		      <br>
        	<h3><code id="dataProses" class="backTransparent"><span></span></code></h3>	
        </div>
      </form>
    </div>
  </div>
</div>
<!-- END FORM DATA -->

<div class="col-md-12">
  <div class="tile bg-info">
    <div class="tile-body">
       <!-- START DATA TABLE -->
        <div class="tile-body">
          <table class="table table-hover table-bordered" id="invoiceList">
            <thead class="thead-dark">
              <tr>
                <th>Slip Id</th>
                <th>Client Name</th>
                <th>Emp Name</th>
                <th>Dept</th>
                <th>Position</th>
              </tr>
            </thead>
          </table>
        </div>
        <!-- END DATA TABLE -->
    </div>
  </div>
</div>
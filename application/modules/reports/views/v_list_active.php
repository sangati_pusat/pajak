<style type="text/css">
tr.selected {
  background-color: #B0BED9!important;
}
/*tr.even.selected {
  background-color: #B0BED9!important;
}*/
</style>
<!--START FORM DATA -->
<div class="col-md-12" id="is_transaction">
  <div class="tile bg-info">
    <!-- <h3 class="tile-title">Barang Masuk</h3> -->
    <div class="tile-body">                       
      <div id='client' class="form-group row">
        <label class="control-label col-md-1">CLIENT</label>
        <div class="col-md-3">
          <select class="form-control" id="ptName" name="ptName" required="">
          </select>
        </div>
      </div>
      <div class="form-group col-md-12 align-self-end">
        <button class="btn btn-warning" id="viewContractList" type="button"><i class="fa fa-refresh"></i>VIEW</button>
        <button class="btn btn-warning" id="printContractList" type="button"><i class="fa fa-print"></i>PRINT</button>
	      <br>
      </div>
    </div>
  </div>
</div>
<!-- END FORM DATA -->

<div class="col-md-12" id="rt">
  <div class="tile bg-info">
    <div class="tile-body">
      <div class="tile-body table-responsive employeeListTable">
        <table class="table table-hover table-bordered" id="contractListTable">
          <thead class="thead-dark">
            <tr role="row">
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 118px;">Payroll Group</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 118px;">Badge Id</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 139px;">Employee Name</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 139px;">Date Of Birth</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 139px;">Position</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 139px;">Department</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 139px;">Basic Salary</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 139px;">Religion</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 169px;">NPWP Number</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 169px;">Marital Status</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 139px;">Employee Status</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 139px;">Poin Of Hire/Origin</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 139px;">Date Of Hire</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 139px;">Contract Start</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 139px;">Contract End</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 169px;">Bank Name</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 169px;">Account Name</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 169px;">Account Number</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>
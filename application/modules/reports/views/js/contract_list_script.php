<script type="text/javascript">
$(document).ready(function(){
    var employeeListTable = $('#contractListTable').DataTable({
        "paging":   true,
        "ordering": true,
        "info":     true,
        "filter":     true,
    });

    $("#viewContractList").on("click",function(){
        
        var ptName      = $("#ptName").val();
        var table       = $('#ptName option:selected').attr('data-table');
        var yearPeriod  = $("#yearPeriod").val();
        var monthPeriod = $("#monthPeriod").val();

        var isValid = true;
        if($('#monthPeriod option:selected').text() == "Pilih"){
            $("#monthPeriod").focus();
            alert('Bulan Harus Dipilih ');
            isValid = false;
        } 
        else if($('#yearPeriod option:selected').text() == "Pilih"){
            $("#yearPeriod").focus();
            alert('Tahun Harus Dipilih ');
            isValid = false;
        } 
        else if($('#ptName option:selected').text() == "Pilih"){
            $("#ptName").focus();
            alert('Client Harus Dipilih ');
            isValid = false;
        } 
        if(isValid == false){return false}

        $.ajax({
            method  : "POST",
            url     : "<?php echo base_url() ?>"+"reports/CR_Summary_Payroll/getListContract",
            data    : {
                pt      : ptName,
                year    : yearPeriod,
                month   : monthPeriod,
                table   : table
            },
            success : function(data){
                employeeListTable.clear().draw();
                var dataSrc = JSON.parse(data);                 
                employeeListTable.rows.add(dataSrc).draw(false);
            },
            error   : function(data){
                alert("Failed");
            }   
        });

    });

    $.ajax({
        url : "<?php echo base_url() ?>"+"masters/Mst_client/loadAll",
        method : "POST",
        async : false,
        dataType : 'json',
        success: function(data){
        var html = '';
        var i;
            html += '<option value="" disabled="" selected="">Pilih</option>';
            for(i=0; i<data.length; i++){
                html += '<option value="'+data[i].client_value+'" >'+data[i].client_name+'</option>';
            }   
        $('#ptName').html(html);
        }
    });

    $('#printContractList').click(function(e) {
        // debugger
        var ptName      = $("#ptName").val();
        var table       = $('#ptName option:selected').attr('data-table');
        var yearPeriod  = $("#yearPeriod").val();
        var monthPeriod = $("#monthPeriod").val();


            var isValid = true;
            if($('#monthPeriod option:selected').text() == "Pilih"){
                $("#monthPeriod").focus();
                alert('Bulan Harus Dipilih ');
                isValid = false;
            } 
            else if($('#yearPeriod option:selected').text() == "Pilih"){
                $("#yearPeriod").focus();
                alert('Tahun Harus Dipilih ');
                isValid = false;
            } 
            else if($('#ptName option:selected').text() == "Pilih"){
                $("#ptName").focus();
                alert('Client Harus Dipilih ');
                isValid = false;
            } 
            if(isValid == false){return false}
            
            var myUrl = "<?php echo base_url() ?>"+"reports/CR_Summary_Payroll/exportListContract/"+ptName+"/"+table+"/"+monthPeriod+"/"+yearPeriod+"/";

            $.ajax({
                method  : "POST",
                url     : myUrl,
                data    : {                     
                    ptName : ptName,
                    monthPeriod : monthPeriod,
                    yearPeriod : yearPeriod
                },                  
                success : function(response){
                    console.log(response);
                    window.open(myUrl,'_blank');
                },
                // success : function(data){
                    // alert(data);
                // },
                error : function(data){
                alert("Failed");
                }   
            }); 
            
    }); 

});
</script>
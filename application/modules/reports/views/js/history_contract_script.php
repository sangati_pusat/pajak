<script type="text/javascript">
    $(document).ready(function(){
        var rowIdx = null;
        $("#loader").hide();

        $.ajax({
            url : "<?php echo base_url() ?>"+"masters/Mst_client/loadAll",
            method : "POST",
            async : false,
            dataType : 'json',
            success: function(data){
            var html = '';
            var i;
                html += '<option value="" disabled="" selected="">Pilih</option>';
                for(i=0; i<data.length; i++){
                    html += '<option value="'+data[i].client_value+'" >'+data[i].client_name+'</option>';
                }   
            $('#clientName').html(html);
            }
        });

        var contractHist = $('#contractHist').DataTable({
                "paging":   true,
                "ordering": false,
                "info":     false,
                "filter":   true,
                "columnDefs": [{
                    "targets": -1,
                    "data": null,
                    "defaultContent": "<button class='btn btn-primary btn-xs btn_detail' data-toggle='modal' data-target='#editPayrollModal'>&nbsp;&nbsp;<span class='fa fa-server'></span></button>"
                }]
            });
        var contractDetailTable = $('#contractDetailTable').DataTable({
                "paging":   false,
                "ordering": false,
                "info":     false,
                "filter":   false
            });
        $(".errMsg").hide();

        $("#print").on("click", function(){            
            var clientName  = $("#clientName").val();            
            var dataprint   = $("#dataprint").val();
            var isValid     = true;
            if($('#clientName option:selected').text() == "Pilih")
            {
                $("#clientName").focus();
                $("#clientNameErr").show();
                isValid = false;
            } 

            if(isValid == false)
            {return false} 

            var myUrl = "<?php echo base_url() ?>"+"reports/CR_Contract_History/exportContractHistory/"+clientName;

            $.ajax({
                method  : "POST",
                url     : myUrl,
                success : function(response){
                    console.log(response);
                    window.open(myUrl,'_blank');
                },
                error : function(data){
                    $.notify({
                        title: "<h5>Informasi : </h5>",
                        message: "<strong>"+data+"</strong> </br></br> ",
                        icon: '' 
                    },
                    {
                        type: "warning",
                        delay: 3000
                    }); 
                }      
            });
        });

        /* START CLICK MAIN BUTTON */
        $("#viewCsv").on("click", function(){                
            var isValid     = true;
            var clientName = $("#clientName").val(); 
            if($('#clientName option:selected').text() == "Pilih")
            {
                $("#clientName").focus();
                $("#clientNameErr").show();
                isValid = false;
            } 

            if(isValid == false)
            {return false} 

            var myUrl = '<?php echo base_url() ?>'+'reports/CR_Contract_History/getContractHistory/';
            $("#loader").show();
            $.ajax({
                method  : "POST",
                url : myUrl,
                data    : {
                    clientName   : clientName
                },
                success : function(data){
                    contractHist.clear().draw();
                    var dataSrc = JSON.parse(data);                 
                    contractHist.rows.add(dataSrc).draw(false);
                    $("#loader").hide();
                },
                error   : function(data){
                    alert(data);
                }   
            });
        });
        /* END CLICK MAIN BUTTON */
        /* START DETAIL BUTTON CLICK */
         $('#contractHist tbody').on( 'click', '.btn_detail', function () {
            var data = contractHist.row( $(this).parents('tr') ).data();
            var biodataId = data[0];             
            var clientName = data[2];             
            $("#biodataId").val(biodataId); 
            $("#empName").val(data[1]); 
            $("#clientNameDetail").val(clientName);  
            var myUrl = "<?php echo base_url() ?>"+"reports/CR_Contract_History/contractDetailDisplay/";
            $.ajax({
                method : "POST",
                url : myUrl,
                data : {
                    biodataId : biodataId,
                    clientName : clientName        
                },
                success : function(data){
                    contractDetailTable.clear().draw();
                    var dataSrc = JSON.parse(data);                 
                    contractDetailTable.rows.add(dataSrc).draw(false);
                },
                error   : function(data){
                    alert("Failed");
                } 
            });
                      
        } );
        /* END DETAIL BUTTON CLICK */
    });
</script>
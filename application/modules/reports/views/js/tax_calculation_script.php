<!-- di Copy dari views/timika/js/timesheet_import_script.php -->
<script type="text/javascript">
    $(document).ready(function(){
        var rowIdx = null;
        var invoiceList = $('#invoiceList').DataTable({
                "paging":   true,
                "ordering": false,
                "info":     false,
                "filter":   true
            });
        $('.dept').hide();
        $('.payrollGroup').hide();
        $('#clientName').on('change', function(){
            var clientName = $('#clientName').val();
            $('.payrollGroup').hide();
            if(clientName == 'Redpath_Timika')
            {   
                $('#dept').append(new Option("All", "All"));
                $('#dept').append(new Option("Admin", "Admin"));
                $('#dept').append(new Option("Alimak", "Alimak"));
                $('#dept').append(new Option("Amole Rehab", "Amole Rehab"));
                $('#dept').append(new Option("Engineering", "Engineering"));
                $('#dept').append(new Option("Electric", "Electric"));
                $('#dept').append(new Option("DMLZ", "DMLZ"));
                $('#dept').append(new Option("Extrasion", "Extrasion"));
                $('#dept').append(new Option("GBC Area I", "GBC Area I"));
                $('#dept').append(new Option("GBC Area II", "GBC Area II"));
                $('#dept').append(new Option("GBC Area III", "GBC Area III"));
                $('#dept').append(new Option("MCM", "MCM"));
                $('#dept').append(new Option("Raisebore", "Raisebore"));
                $('#dept').append(new Option("Safety And Training", "Safety And Training"));
                $('#dept').append(new Option("Service Crew", "Service Crew"));
                $('#dept').append(new Option("STCP", "STCP"));            
                $('.dept').show();
                $('.bank').show();
            }
            else if(clientName == 'Pontil_Timika')
            {
                $('.dept').hide();
                $('.payrollGroup').show();
                $('.bank').show();
            }
            else if(clientName == 'Pontil_Sumbawa')
            {
                $('.dept').hide();
                $('.payrollGroup').hide();
                $('.bank').hide();
            }
            else if(clientName == 'Trakindo_Sumbawa')
            {
                $('.dept').hide();
                $('.payrollGroup').hide();
                $('.bank').hide();
            }
        });
        $("#loader").hide();
        var cn = '';
        $('.dept').hide();

        var dept = '';
        // var dataPrint = '';
        $('#dept').on('change', function(){
            dept = $('#dept').val();
        });

        $("#printReimbursement").hide();   
        $("#fileToPrintGroup").hide();
        $("#printInvoice").hide();

        $(".errMsg").hide();

        /* START CLICK MAIN BUTTON */
        $("#viewTax").on("click", function(){            
            var clientName      = $("#clientName").val();            
            var yearPeriod      = $("#yearPeriod").val();
            var dept            = $("#dept").val();
            var payrollGroup    = $("#payrollGroup").val();

            var isValid = true;
            if($('#clientName option:selected').text() == "Pilih")
            {
                $("#clientName").focus();
                $("#clientNameErr").show();
                isValid = false;
            } 
            else if($('#yearPeriod option:selected').text() == "Pilih")
            {
                $("#yearPeriod").focus();
                $("#yearPeriodErr").show();
                isValid = false;
            }            

            if(isValid == false)
            {return false} 

            var myUrl = '<?php echo base_url() ?>'+'reports/Tax/getTaxCalculationView/';
            $("#loader").show();
            $.ajax({
                method  : "POST",
                url : myUrl,
                data    : {
                    pt   : clientName,
                    year : yearPeriod,
                    dept : dept,
                    payrollGroup : payrollGroup,
                },
                success : function(data){
                    invoiceList.clear().draw();
                    var dataSrc = JSON.parse(data);                 
                    invoiceList.rows.add(dataSrc).draw(false);
                    $("#loader").hide();
                },
                error   : function(data){
                    alert(data);
                }   
            });
        });
        /* END CLICK MAIN BUTTON */

        // $('#dataPrint').on('change', function(){
        //     dataPrint = $("#dataPrint").val();
        // });

        /* START PRINT DATA */
        $('#printToFile').on('click', function () {
            // debugger
            var clientName = $("#clientName").val();            
            var yearPeriod = $("#yearPeriod").val();
            var dept = $("#dept").val();
            var payrollGroup = $("#payrollGroup").val();
            var myUrl = "";
            var isValid = true;
            if($('#clientName option:selected').text() == "Pilih")
            {
                $("#clientName").focus();
                $("#clientNameErr").show();
                isValid = false;
            } 
            else if($('#yearPeriod option:selected').text() == "Pilih")
            {
                $("#yearPeriod").focus();
                $("#yearPeriodErr").show();
                isValid = false;
            }            

            if(isValid == false)
            {return false} 
            if (clientName == "Pontil_Timika"){
                var myUrl = "<?php echo base_url() ?>"+"reports/Tax/exportTaxCalculationPontilTimika/"+clientName+"/"+yearPeriod+"/"+payrollGroup+"/"+dept;
            }
            else if (clientName == "Redpath_Timika"){
                var myUrl = "<?php echo base_url() ?>"+"reports/Tax/exportTaxCalculationRedpathTimika/"+clientName+"/"+yearPeriod+"/"+payrollGroup+"/"+dept;
            }
            else if (clientName == "Pontil_Sumbawa"){
                var myUrl = "<?php echo base_url() ?>"+"reports/Tax/exportTaxCalculationPontilSumbawa/"+clientName+"/"+yearPeriod;
            }
            else if (clientName == "Trakindo_Sumbawa"){
                var myUrl = "<?php echo base_url() ?>"+"reports/Tax/exportTaxCalculationTrakindoSumbawa/"+clientName+"/"+yearPeriod;
            }
            alert(myUrl);
            // $("#loader").show();
            $.ajax({
                method  : "POST",
                url     : myUrl,
                // data    : {
                //     clientName   : clientName,
                //     yearPeriod : yearPeriod
                // },
                success : function(response){
                    console.log(response);
                    window.open(myUrl,'_blank');
                },
                error : function(data){
                    $.notify({
                        title: "<h5>Informasi : </h5>",
                        message: "<strong>"+data+"</strong> </br></br> ",
                        icon: '' 
                    },
                    {
                        type: "warning",
                        delay: 3000
                    }); 
                }      
            });
        });
        /* END PRINT DATA */
    });
</script>
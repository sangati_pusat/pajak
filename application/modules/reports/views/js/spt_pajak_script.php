<script type="text/javascript">
	$(document).ready(function(){
		var sptId = "";

		/* START BIODATA TABLE */	
		var sptTable = $('#sptTable').DataTable({
	            "paging":   true,
	            "ordering": false,
	            "info":     true,
	            "filter":   true,
	            "columnDefs": [{
                    "targets": -1,
                    "data": null,
                    "defaultContent": "<button class='btn btn-warning btn-xs btn_print'><i class='fa fa-print'></i></button>"   
                }]
                
	        });
		/* END BIODATA TABLE */
		
        /* END LOAD BIODATA  */
        $("#loader").hide();
        $('.clientName').hide();
        
        // /* START SELECT BROWSE DATA */
        // var sptId = "";
        // var ptName = "";
        // var fullName = "";
        // var rowData = null;
        // $('#sptTable tbody').on( 'click', 'tr', function () {
        //     var rowData = sptTable.row( this ).data();
        //     if ( $(this).hasClass('selected') ) 
        //     {
        //         $(this).removeClass('selected');
        //     }
        //     else 
        //     {
        //         sptTable.$('tr.selected').removeClass('selected');
        //         $(this).addClass('selected');                    
        //     }
        //     sptId = rowData[0];
        //     ptName = rowData[1];
        //     fullName = rowData[2];
        // }); 
        // /* END SELECT BROWSE DATA */

		$('.errMsg').hide();
		$('#dataProses').html('');
		var isValid = true;

		$('#btnDisplay').on('click', function(){		
			$("#loader").show();
			$('.errMsg').hide();
			$('#btnDisplay').prop('disabled', true);
			if($('#clientName option:selected').text() == 'Pilih') 
			{
				$('#clientName').focus();
				$('#clientNameErr').show();
				isValid = false;
			}
			else if($('#yearPeriod option:selected').text() == 'Pilih') 
			{
				$('#yearPeriod').focus();
				$('#yearPeriodErr').show();
				isValid = false;
			}

			if(isValid == false)
			{
				$("#loader").hide();
				$('#btnDisplay').prop('disabled', false);
                $.notify({
	                title: "<h5>Informasi : </h5>",
	                message: "<strong>Check Data</strong> </br></br> ",
	                icon: '' 
	            },
	            {
	                type: "warning",
	                delay: 3000
	            });	
		
				return false;
			}
			
			var client = $('#clientName').val();
			var year = $('#yearPeriod').val();			
			var myUrl = "";
			var payrollGroup = ""; 

			if(client == 'Pontil_Sumbawa'){
				// payrollGroup = $('#payrollGroup').val(); 
				myUrl = "<?php echo base_url() ?>"+'reports/SptPajak/getViewData/'+client+'/'+year;
			}
			
			// alert(myUrl);   
			$.ajax({
				method : "POST",
				url	   : myUrl,
				data   : {
					// biodataId    : bioId,
					clientName   : client,
					yearPeriod   : year
				},
				success : function(data){

					$("#loader").hide();
					$('#btnDisplay').prop('disabled', false);
					sptTable.clear().draw();
	                var dataSrc = JSON.parse(data);                 
	                sptTable.rows.add(dataSrc).draw(false);
              
				},
				error 	: function(data){
					
					$("#loader").hide();
					$('#btnDisplay').prop('disabled', false);
                    $.notify({
		                title: "<h5>Informasi : </h5>",
		                message: "<strong>"+data+"</strong> </br></br> ",
		                icon: '' 
		            },
		            {
		                type: "warning",
		                delay: 3000
		            });	
				}

			});
		});/* $('#btnProsesPayroll').on('click', function(){ */

		/* START SELECT TABLE SPT DATA */       
        var rowData = null;
        $('#sptTable tbody').on( 'click', 'tr', function () {
            var rowData = sptTable.row( this ).data();
            if ( $(this).hasClass('selected') ) 
            {
                $(this).removeClass('selected');
            }
            else 
            {
                sptTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');                    
            }
            sptId = rowData[0];
            // alert(loanId);
        }); 
        /* END START SELECT TABLE SPT DATA */

		/* START PRINT SPT PAJAK */
        $('#sptTable tbody').on( 'click', '.btn_print', function () {
            var data = sptTable.row( $(this).parents('tr') ).data();
            var sptId = data[0]; ;  
            var clientName = data[1]; 
            var fullName = data[2];

            var myUrl = "";
            if(clientName == 'Pontil_Sumbawa'){
               myUrl = "<?php echo base_url() ?>"+"reports/SptPajak/toExport/"+clientName+"/"+fullName;
             
            }
            // if(clientName == 'Pontil_Banyuwangi'){
            //    myUrl = "<?php #echo base_url() ?>"+"reports/SptPajak/toExport/"+clientName+"/"+fullName;
               
            // }
            
            $.ajax({
                method : "POST",
                url : myUrl,
                data : {
                    sptId : sptId,
                    clientName : clientName,                 
                    fullName : fullName
                },

                success : function(response){
                    console.log(response);
                    window.open(myUrl,'_blank');
                },
                error : function(data){
                    $.notify({
		                title: "<h5>Informasi : </h5>",
		                message: "<strong>"+data+"</strong> </br></br> ",
		                icon: '' 
		            },
		            {
		                type: "warning",
		                delay: 3000
		            });	
                }   
            });
        });
		/* END PRINT PAYSLIP */

	});
</script>
<script type="text/javascript">
    $(document).ready(function(){
        var candidateListTable = $('#candidateListTable').DataTable({
            "paging":   true,
            "ordering": true,
            "info":     true,
            "filter":     true,
        });

        var employeeListTable = $('#employeeListTable').DataTable({
            "paging":   true,
            "ordering": true,
            "info":     true,
            "filter":     true,
        });

        $('.employeeListTable').hide();
        $("#client").hide();
        $('.isActive').on('click', function(){
            if($('#isActive:checked').val() == '1'){
                $('#client').show();
                $('.candidateListTable').hide();
                $('.employeeListTable').show();
            }else{
                $('#client').hide();
                $('.candidateListTable').show();
                $('.employeeListTable').hide();                
            }
        }); 

        $("#viewBiodataList").on("click", function(){
            
            var ptName = $("#ptName").val();
            var table  = $('#ptName option:selected').attr('data-table');
            var yearPeriod = $("#yearPeriod").val();
            var monthPeriod = $("#monthPeriod").val();
            var isActive = $('#isActive:checked').val();

            if(isActive == '1'){

                var isValid = true;
                if($('#monthPeriod option:selected').text() == "Pilih"){
                    $("#monthPeriod").focus();
                    alert('Bulan Harus Dipilih ');
                    isValid = false;
                } 
                else if($('#yearPeriod option:selected').text() == "Pilih"){
                    $("#yearPeriod").focus();
                    alert('Tahun Harus Dipilih ');
                    isValid = false;
                } 
                else if($('#ptName option:selected').text() == "Pilih"){
                    $("#ptName").focus();
                    alert('Client Harus Dipilih ');
                    isValid = false;
                } 
                if(isValid == false){return false}

                $.ajax({
                    method  : "POST",
                    url : "<?php echo base_url() ?>"+"masters/Mst_bio_rec/getBioAllCandidate",
                    data    : {
                        pt      : ptName,
                        year    : yearPeriod,
                        month   : monthPeriod,
                        isActive: isActive,
                        table   : table
                    },
                    success : function(data){
                        employeeListTable.clear().draw();
                        var dataSrc = JSON.parse(data);                 
                        employeeListTable.rows.add(dataSrc).draw(false);
                    },
                    error   : function(data){
                        alert("Failed");
                    }   
                });

            }else{

                var isValid = true;
                if($('#monthPeriod option:selected').text() == "Pilih"){
                    $("#monthPeriod").focus();
                    alert('Bulan Harus Dipilih ');
                    isValid = false;
                } 
                else if($('#yearPeriod option:selected').text() == "Pilih"){
                    $("#yearPeriod").focus();
                    alert('Tahun Harus Dipilih ');
                    isValid = false;
                } 
                if(isValid == false){return false}

                $.ajax({
                    method  : "POST",
                    url : "<?php echo base_url() ?>"+"masters/Mst_bio_rec/getBioAllCandidate",
                    data    : {
                        // pt   : ptName,
                        year : yearPeriod,
                        month: monthPeriod,
                        isActive:isActive
                    },
                    success : function(data){
                        candidateListTable.clear().draw();
                        var dataSrc = JSON.parse(data);                 
                        candidateListTable.rows.add(dataSrc).draw(false);
                    },
                    error   : function(data){
                        alert("Failed");
                    }   
                });
            }

        });

        $('#printBiodataList').click(function(e) {
            var ptName = $("#ptName").val();
            var table  = $('#ptName option:selected').attr('data-table')
            var yearPeriod = $("#yearPeriod").val();
            var monthPeriod = $("#monthPeriod").val();
            var isActive = $('#isActive:checked').val();

            if(isActive == '1'){

                var isValid = true;
                if($('#monthPeriod option:selected').text() == "Pilih"){
                    $("#monthPeriod").focus();
                    alert('Bulan Harus Dipilih ');
                    isValid = false;
                } 
                else if($('#yearPeriod option:selected').text() == "Pilih"){
                    $("#yearPeriod").focus();
                    alert('Tahun Harus Dipilih ');
                    isValid = false;
                } 
                else if($('#ptName option:selected').text() == "Pilih"){
                    $("#ptName").focus();
                    alert('Client Harus Dipilih ');
                    isValid = false;
                } 
                if(isValid == false){return false}
                
                var myUrl = "<?php echo base_url() ?>"+"masters/Mst_bio_rec/exportBiodataList/"+ptName+"/"+table+"/"+isActive+"/"+monthPeriod+"/"+yearPeriod+"/";

                $.ajax({
                    method  : "POST",
                    url     : myUrl,
                    data    : {                     
                        ptName : ptName,
                        monthPeriod : monthPeriod,
                        yearPeriod : yearPeriod, 
                        isActive : isActive
                    },                  
                    success : function(response){
                        console.log(response);
                        window.open(myUrl,'_blank');
                    },
                    // success : function(data){
                        // alert(data);
                    // },
                    error : function(data){
                    alert("Failed");
                    }   
                }); 

            }else{

                var isValid = true;
                if($('#monthPeriod option:selected').text() == "Pilih"){
                    $("#monthPeriod").focus();
                    alert('Bulan Harus Dipilih ');
                    isValid = false;
                } 
                else if($('#yearPeriod option:selected').text() == "Pilih"){
                    $("#yearPeriod").focus();
                    alert('Tahun Harus Dipilih ');
                    isValid = false;
                } 

                if(isValid == false)
                {return false} 

                var myUrl = "<?php echo base_url() ?>"+"masters/Mst_bio_rec/exportBiodataList/"+ptName+"/"+table+"/"+isActive+"/"+monthPeriod+"/"+yearPeriod+"/";

                $.ajax({
                    method  : "POST",
                    url     : myUrl,
                    data    : {                     
                        ptName : ptName,
                        monthPeriod : monthPeriod,
                        yearPeriod : yearPeriod, 
                        isActive : isActive
                    },                  
                    success : function(response){
                        console.log(response);
                        window.open(myUrl,'_blank');
                    },
                    // success : function(data){
                        // alert(data);
                    // },
                    error : function(data){
                    alert("Failed");
                    }   
                }); 

            }

                
        }); 






































































































    });
</script>
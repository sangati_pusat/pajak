<script type="text/javascript">
    $(document).ready(function(){
        var rowIdx = null;
        var invoiceList = $('#invoiceList').DataTable({
                "paging":   true,
                "ordering": false,
                "info":     false,
                "filter":   true
            });
        $("#loader").hide();

        $('.dept').hide();
        $('.payrollGroup').hide();
        $('.bank').hide();
        $('.print').hide();
        $(".errMsg").hide();

        $('.payrollGroup').hide();
        $('#clientName').on('change', function(){
            var clientName = $('#clientName').val();
            $('.payrollGroup').hide();
            if(clientName == 'Redpath_Timika')
            {   
                $('#dept').append(new Option("All", "All"));
                $('#dept').append(new Option("Admin", "Admin"));
                $('#dept').append(new Option("Alimak", "Alimak"));
                $('#dept').append(new Option("Amole Rehab", "Amole Rehab"));
                $('#dept').append(new Option("Engineering", "Engineering"));
                $('#dept').append(new Option("Electric", "Electric"));
                $('#dept').append(new Option("DMLZ", "DMLZ"));
                $('#dept').append(new Option("Extrasion", "Extrasion"));
                $('#dept').append(new Option("GBC Area I", "GBC Area I"));
                $('#dept').append(new Option("GBC Area II", "GBC Area II"));
                $('#dept').append(new Option("GBC Area III", "GBC Area III"));
                $('#dept').append(new Option("MCM", "MCM"));
                $('#dept').append(new Option("Raisebore", "Raisebore"));
                $('#dept').append(new Option("Safety And Training", "Safety And Training"));
                $('#dept').append(new Option("Service Crew", "Service Crew"));
                $('#dept').append(new Option("STCP", "STCP"));            
                $('.dept').show();
                $('.bank').show();
            }
            else if(clientName == 'Pontil_Timika')
            {
                $('.dept').hide();
                $('.payrollGroup').show();
                $('.bank').show();
            }
            else if(clientName == 'All')
            {
                $('.dept').hide();
                $('.payrollGroup').hide();
                $('.bank').hide();
            }
        });

        $('#bank').on('change', function(){
            var bank = $('#bank').val();
            if(bank == 'BNI')
            {             
                $('.print').show();
            }
            else if(bank == 'MANDIRI')
            {             
                $('.print').hide();
            }
            else if(bank == 'EXCPT')
            {             
                $('.print').hide();
            }
        });

        $("#print").on("click", function(){ 
            var site        = $("#site").val();            
            var clientName  = $("#clientName").val();            
            var yearPeriod  = $("#yearPeriod").val();
            var monthPeriod = $("#monthPeriod").val();
            var dept        = '';
            var bank        = $("#bank").val();
            var dataprint   = $("#dataprint").val();
            var isValid     = true;
            if($('#site option:selected').text() == "Pilih")
            {
                $("#site").focus();
                $("#siteErr").show();
                isValid = false;
            } 
            else if($('#yearPeriod option:selected').text() == "Pilih")
            {
                $("#yearPeriod").focus();
                $("#yearPeriodErr").show();
                isValid = false;
            }      
            else if($('#monthPeriod option:selected').text() == "Pilih")
            {
                $("#monthPeriod").focus();
                $("#monthPeriodErr").show();
                isValid = false;
            }     
            else if($('#clientName').val() == "Redpath_Timika")
            {
                dept = $("#dept").val();              
            }
            else if($('#clientName').val() == "Pontil_Timika")
            {
                dept = $("#payrollGroup").val();                
            }           

            if(isValid == false)
            {return false} 

            var myUrl = "<?php echo base_url() ?>"+"reports/ExportCsv/exporthanyaBni/"+site+"/"+clientName+"/"+yearPeriod+"/"+bank+"/"+dept+"/"+dataprint+"/"+monthPeriod;

            $.ajax({
                method  : "POST",
                url     : myUrl,
                success : function(response){
                    console.log(response);
                    window.open(myUrl,'_blank');
                },
                error : function(data){
                    $.notify({
                        title: "<h5>Informasi : </h5>",
                        message: "<strong>"+data+"</strong> </br></br> ",
                        icon: '' 
                    },
                    {
                        type: "warning",
                        delay: 3000
                    }); 
                }      
            });
        });

        /* START CLICK MAIN BUTTON */
        $("#viewCsv").on("click", function(){       
            var site        = $("#site").val();            
            var clientName  = $("#clientName").val();            
            var yearPeriod  = $("#yearPeriod").val();
            var monthPeriod = $("#monthPeriod").val();
            var dept        = '';
            var payrollGroup= '';
            var bank        = '';
            if(clientName != 'All'){
                var dept        = $("#dept").val();
                var payrollGroup= $("#payrollGroup").val();
                var bank        = $("#bank").val();
            }
            var isValid     = true;
            if($('#site option:selected').text() == "Pilih")
            {
                $("#site").focus();
                $("#siteErr").show();
                isValid = false;
            } 
            else if($('#yearPeriod option:selected').text() == "Pilih")
            {
                $("#yearPeriod").focus();
                $("#yearPeriodErr").show();
                isValid = false;
            }  
            else if($('#monthPeriod option:selected').text() == "Pilih")
            {
                $("#monthPeriod").focus();
                $("#monthPeriodErr").show();
                isValid = false;
            }   

            // else if($('#bank option:selected').text() == "Pilih")
            // {
            //     $("#bank").focus();
            //     $("#bankErr").show();
            //     isValid = false;
            // }   

            if(clientName == 'Redpath_Timika'){
                if($('#dept option:selected').text() == "Pilih")
                {
                    $("#dept").focus();
                    $("#deptErr").show();
                    // alert('Bulan Harus Dipilih ');
                    isValid = false;
                }  
            }         

            if(isValid == false)
            {return false} 

            if(clientName == 'Pontil_Timika'){
                dept = payrollGroup;
            }

            var myUrl = '<?php echo base_url() ?>'+'reports/ExportCsv/getExportCsvView/';
            $("#loader").show();
            $.ajax({
                method  : "POST",
                url : myUrl,
                data    : {
                    site : site,
                    pt   : clientName,
                    year : yearPeriod,
                    month: monthPeriod,
                    dept : dept,
                    bank : bank,
                },
                success : function(data){
                    invoiceList.clear().draw();
                    var dataSrc = JSON.parse(data);                 
                    invoiceList.rows.add(dataSrc).draw(false);
                    $("#loader").hide();
                },
                error   : function(data){
                    alert(data);
                }   
            });
        });
        /* END CLICK MAIN BUTTON */

        /* START PRINT DATA */
        $('#printToFile').on('click', function () {
            var site        = $("#site").val();            
            var clientName  = $("#clientName").val();            
            var yearPeriod  = $("#yearPeriod").val();
            var monthPeriod = $("#monthPeriod").val();
            var dept        = '';
            var bank        = $("#bank").val();
            var dataprint   = $("#dataprint").val();
            // alert (dataprint);
            // var myUrl       = "";
            var isValid     = true;
            if($('#site option:selected').text() == "Pilih")
            {
                $("#site").focus();
                $("#siteErr").show();
                isValid = false;
            } 
            else if($('#yearPeriod option:selected').text() == "Pilih")
            {
                $("#yearPeriod").focus();
                $("#yearPeriodErr").show();
                isValid = false;
            }      
            else if($('#monthPeriod option:selected').text() == "Pilih")
            {
                $("#monthPeriod").focus();
                $("#monthPeriodErr").show();
                isValid = false;
            }     
            // else if($('#bank option:selected').text() == "Pilih")
            // {
            //     $("#bank").focus();
            //     $("#bankErr").show();
            //     isValid = false;
            // } 
            // else if($('#dataPrint option:selected').text() == "Pilih")
            // {
            //     $("#dataPrint").focus();
            //     $("#dataPrintErr").show();
            //     isValid = false;
            // } 
            else if($('#clientName').val() == "Redpath_Timika")
            {
                dept = $("#dept").val();              
            }
            else if($('#clientName').val() == "Pontil_Timika")
            {
                dept = $("#payrollGroup").val();                
            }           

            if(isValid == false)
            {return false} 

            var myUrl = '';

            if(clientName != 'All'){
                if(bank == 'BNI'){
                    var myUrl = "<?php echo base_url() ?>"+"reports/ExportCsv/exportBni/"+site+"/"+clientName+"/"+yearPeriod+"/"+bank+"/"+dept+"/"+dataprint+"/"+monthPeriod;
                }
                else if(bank == 'MANDIRI'){
                    var myUrl = "<?php echo base_url() ?>"+"reports/ExportCsv/exportMandiri/"+site+"/"+clientName+"/"+yearPeriod+"/"+bank+"/"+dept+"/"+dataprint+"/"+monthPeriod;
                }
                else if(bank == 'EXCPT'){
                    var myUrl = "<?php echo base_url() ?>"+"reports/ExportCsv/exportLain/"+site+"/"+clientName+"/"+yearPeriod+"/"+bank+"/"+dept+"/"+dataprint+"/"+monthPeriod;
                }
            }
            else{
                var myUrl = "<?php echo base_url() ?>"+"reports/ExportCsv/exporta/"+site+"/"+clientName+"/"+yearPeriod+"/"+dataprint+"/"+monthPeriod;
            }

            $.ajax({
                method  : "POST",
                url     : myUrl,
                success : function(response){
                    console.log(response);
                    window.open(myUrl,'_blank');
                },
                error : function(data){
                    $.notify({
                        title: "<h5>Informasi : </h5>",
                        message: "<strong>"+data+"</strong> </br></br> ",
                        icon: '' 
                    },
                    {
                        type: "warning",
                        delay: 3000
                    }); 
                }      
            });
        });
        /* END PRINT DATA */
    });
</script>
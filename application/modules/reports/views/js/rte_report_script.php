<script type="text/javascript">
    $(document).ready(function(){
        var rowIdx = null;
        var rteReportsTable = $('#rteReportsTable').DataTable({
                "paging":   true,
                "ordering": false,
                "info":     false,
                "filter":   true
                // "columnDefs": [{
                //     "targets": -2,
                //     "data": null,
                //     "defaultContent": "<button class='btn btn-primary btn-xs btn_update' data-toggle='modal' data-target='#editPayrollModal'><span class='glyphicon glyphicon-edit'></span></button>"
                // },
                // {
                //     "targets": -1,
                //     "data": null,
                //     "defaultContent": "<button class='btn btn-primary btn-xs btn_print'><span class='glyphicon glyphicon-print'></span></button>"   
                // }]
            });
       
        // START SELECT RTE NUMBER //

        $.ajax({
            url : "<?php echo base_url() ?>"+"masters/Mst_rte/getRteNumber",
            method : "POST",
            data : {
                id : ""
            },
            success : function(data){
                var dataSrc = JSON.parse(data); 
                var dataLength = dataSrc.length;
                $('#rteNumber').find('option:not(:first)').remove();
                for(var i = 0; i < dataLength; i++){
                    $('#rteNumber').append(new Option(dataSrc[i]['rte_number']));

                }
            },
            error : function(){
                alert('Failed');
            }
        });

        // END SELECT RTE NUMBER //

        /* START CLICK MAIN BUTTON */
        $("#viewRteReports").on("click", function(){
            // debugger
            var rteNumber    = $("#rteNumber").val().replace(/\//g, '^');; //pengambilan RTE NO
            var dateRequired = $("#dateRequired").val();
            

            isValid = true;    
            if ($('#rteNumber option:selected').text() == "ALL DATA")
            {
                alert('RTE cannot be empty');
                $('#rteNumber').focus();
                isValid = false;
            }
            if ($('#dateRequired option:selected').text() == "ALL DATA")
            {
                alert('Month required cannot be empty');
                $('#dateRequired').focus();
                isValid = false;
            }
            

            if(isValid == false)
            {return false} 

            /* Ajax Is Here */

            var myUrl = "<?php echo base_url() ?>"+"masters/Mst_rte/getListRte/"+rteNumber+"/"+dateRequired;
            alert(myUrl); 

            $.ajax({
                method  : "POST",
                url : myUrl,
                data    : {
                    rteNumber     : rteNumber,
                    dateRequired  : dateRequired
                    // payrollGroup : payrollGroup 
                },
                success : function(data){
                    rteReportsTable.clear().draw();
                    var dataSrc = JSON.parse(data);                 
                    rteReportsTable.rows.add(dataSrc).draw(false);
                },
                error   : function(data){
                    // alert("Failed");
                    alert(data);
                }   
            });
        });
        /* END CLICK MAIN BUTTON */

        /* START SELECT DATA */
        $("#rteReportsTable tbody").on("click", "tr", function(){
            var rowData = rteReportsTable.row( this ).data();
            if ( $(this).hasClass('selected') ) 
            {
                $(this).removeClass('selected');
            }
            else 
            {
                rteReportsTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');                    
            }
            rowIdx = rteReportsTable.row( this ).index();
            $("#rowIdx").val(rowIdx); 
        });
        /* END SELECT DATA */
    
         /* START CLICK MAIN BUTTON */
        $('#printRteReports').on( 'click', function () {
            var rteNumber    = $("#rteNumber").val().replace(/\//g, '^');;
            var dateRequired = $("#dateRequired").val();

            isValid = true;    
            if ($('#rteNumber option:selected').text() == "ALL DATA")
            {
                alert('RTE cannot be empty');
                $('#rteNumber').focus();
                isValid = false;
            }
            if ($('#dateRequired option:selected').text() == "ALL DATA")
            {
                alert('Month required cannot be empty');
                $('#dateRequired').focus();
                isValid = false;
            }
            

            if(isValid == false)
            {return false} 
        
            var myUrl = "<?php echo base_url() ?>"+"masters/Mst_rte/exportRte/"+rteNumber+"/"+dateRequired;
            // alert("PRINT "+myUrl);
            $.ajax({
                method : "POST",
                url : myUrl,
                data : {
                    rteNumber    : rteNumber,
                    dateRequired : dateRequired
                },
                // success : function(data){
                    // alert(data);
                success : function(response){
                    console.log(response);
                    window.open(myUrl,'_blank');
                },
                error : function(data){
                    alert("Failed");
                    // alert(data);
                }   
            });
        });
        /* END CLICK MAIN BUTTON */
        
    });
</script>
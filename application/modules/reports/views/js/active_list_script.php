<script type="text/javascript">
    $(document).ready(function(){
        var rowIdx = null;
        $("#loader").hide();
        var paymentList = $('#paymentList').DataTable({
                "paging":   true,
                "ordering": false,
                "info":     false,
                "filter":   true
            });

        /* START CLICK MAIN BUTTON */
        $("#viewListActive").on("click", function(){
            var ptName = $("#clientName").val();
            var yearPeriod = $("#yearPeriod").val();
            var monthPeriod = $("#monthPeriod").val();
            // var payrollGroup = $("#payrollGroup").val();
            var isValid = true;
            if($('#clientName option:selected').text() == "Pilih")
            {
                $("#clientName").focus();
                alert('PT Harus Dipilih ');
                isValid = false;
            } 
            else if($('#monthPeriod option:selected').text() == "Pilih")
            {
                $("#monthPeriod").focus();
                alert('Bulan Harus Dipilih ');
                isValid = false;
            } 
            else if($('#yearPeriod option:selected').text() == "Pilih")
            {
                $("#yearPeriod").focus();
                alert('Tahun Harus Dipilih ');
                isValid = false;
            }

            if(isValid == false)
            {return false} 
            var myUrl = "";
            myUrl = "<?php echo base_url().'reports/CR_Active_List/getListActive'; ?>";
            // alert(myUrl);
            /* Ajax Is Here */
            $.ajax({
                method : "POST",
                url    : myUrl,
                data   : {
                    pt   : ptName,
                    year : yearPeriod,
                    month: monthPeriod
                },
                success : function(data){

                    $("#loader").hide();
                    $('#btnDisplay').prop('disabled', false);
                    paymentList.clear().draw();
                    var dataSrc = JSON.parse(data);                 
                    paymentList.rows.add(dataSrc).draw(false);
                },
                error   : function(data){
                    
                    $("#loader").hide();
                    $('#btnDisplay').prop('disabled', false);
                    $.notify({
                        title: "<h5>Informasi : </h5>",
                        message: "<strong>"+data+"</strong> </br></br> ",
                        icon: '' 
                    },
                    {
                        type: "warning",
                        delay: 3000
                    }); 
                }
            });
        });
        /* END CLICK MAIN BUTTON */

        $.ajax({
            url : "<?php echo base_url() ?>"+"masters/Mst_client/loadAll",
            method : "POST",
            async : false,
            dataType : 'json',
            success: function(data){
            var html = '';
            var i;
                html += '<option value="" disabled="" selected="">Pilih</option>';
                for(i=0; i<data.length; i++){
                    html += '<option value="'+data[i].client_value+'" >'+data[i].client_name+'</option>';
                }   
            $('#clientName').html(html);
            }
        });

        /* START SELECT DATA */
        $("#paymentList tbody").on("click", "tr", function(){
            var rowData = paymentList.row( this ).data();
            if ( $(this).hasClass('selected') ) 
            {
                $(this).removeClass('selected');
            }
            else 
            {
                paymentList.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');                    
            }
            rowIdx = paymentList.row( this ).index();
            $("#rowIdx").val(rowIdx); 
            // alert(rowIdx);           
            // payrollId = rowData[0];
            // alert(payrollId);
        });
        /* END SELECT DATA */

        /* START PRINT BUTTON CLICK */
        $('#printListActive').on( 'click', function () {
            var ptName = $("#clientName").val();
            var yearPeriod = $("#yearPeriod").val();
            var monthPeriod = $("#monthPeriod").val();
            // var payrollGroup = $("#payrollGroup").val();

            var isValid = true;
            if($('#clientName option:selected').text() == "Pilih")
            {
                $("#clientName").focus();
                alert('PT Harus Dipilih ');
                isValid = false;
            } 
            else if($('#monthPeriod option:selected').text() == "Pilih")
            {
                $("#monthPeriod").focus();
                alert('Bulan Harus Dipilih ');
                isValid = false;
            } 
            else if($('#yearPeriod option:selected').text() == "Pilih")
            {
                $("#yearPeriod").focus();
                alert('Tahun Harus Dipilih ');
                isValid = false;
            }
            // else if($('#payrollGroup option:selected').text() == "Pilih")
            // {
            //     $("#payrollGroup").focus();
            //     alert('Group Harus Dipilih ');
            //     isValid = false;
            // } 

            if(isValid == false)
            {return false} 
        
            var myUrl = "<?php echo base_url() ?>"+"reports/CR_Active_List/printListActive/"+ptName+"/"+yearPeriod+"/"+monthPeriod;
            // alert("PRINT "+myUrl);
            $.ajax({
                method : "POST",
                url : myUrl,
                data : {
                    ptName : ptName,
                    yearPeriod : yearPeriod,
                    monthPeriod : monthPeriod 
                },
                // success : function(data){
                    // alert(data);
                success : function(response){
                    console.log(response);
                    window.open(myUrl,'_blank');
                },
                error : function(data){
                    $.notify({
                        title: "<h5>Informasi : </h5>",
                        message: "<strong>"+data+"</strong> </br></br> ",
                        icon: '' 
                    },
                    {
                        type: "warning",
                        delay: 3000
                    }); 
                }   
            });
        });
        /* END PRINT BUTTON CLICK */
        /*$('#paymentList_').on( 'click', 'tbody td', function () {
            paymentList.cell((this)).data('MEOW!').draw();
        } );*/
    });
</script>
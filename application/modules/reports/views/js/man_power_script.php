<script type="text/javascript">
$(document).ready(function(){

    $.ajax({
        url: "<?php echo base_url() ?>"+"masters/Contract/contractHighchart",
        type: 'GET',
        async: true,
        dataType: "json",
        success: function (data_ajax){
            visitorData(data_ajax);
        }
    });

    var employeeListTable = $('#contractListTable').DataTable({
        "paging":   true,
        "ordering": true,
        "info":     true,
        "filter":     true,
    });

    var myUrl = "<?php echo base_url() ?>"+"masters/Contract/loadContractByClient";
    $.ajax({
        method  : "POST",
        url : myUrl,
        data    : {
        },
        success : function(data){
            employeeListTable.clear().draw();
            var dataSrc = JSON.parse(data);                 
            employeeListTable.rows.add(dataSrc).draw(false);
        },
        error   : function(data){
            alert(data);
        }   
    });

    Highcharts.setOptions({
    colors: ['#058DC7','#50B432','#ED561B','#DDDF00','#24CBE5','#64E572','#FF9655','#FFF263','#6AF9C4','#007bff','#6610f2','#6f42c1','#e83e8c','#dc3545','#fd7e14',
             '#20c997','#343a40','#009688','#6c757d','#28a745','#17a2b8','#ffc107','#3e1717','#eb3434','#eb5634','#eb9334','#ebbd34','#dfeb34','#a2eb34','#59eb34',
             '#34eb6e','#34ebd9','#34c6eb','#348ceb','#346beb','#3446eb','#4034eb','#8c34eb','#b134eb','#d634eb','#eb349c','#801b1b','#80651b','#63801b','#3d801b',
             '#1b8039','#1b8078','#1b5480','#1b3180','#3b1b80','#631b80','#801b78','#801b51','#785050','#786c50','#6a7850','#537850','#507869','#506b78','#555078']
    });

    function visitorData (data_ajax) {
        Highcharts.chart('container',{
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Department'
            },
            tooltip: {
                pointFormat: '{series.name}: {2f} <b>{point.percentage:.1f}%</b>'
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Dept',
                colorByPoint: true,
                data: data_ajax
            }]
        });
    }

});
</script>
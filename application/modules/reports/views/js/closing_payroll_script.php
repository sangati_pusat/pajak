<script type="text/javascript">
$(document).ready(function(){
    var employeeListTable = $('#contractListTable').DataTable({
        "paging":   true,
        "ordering": true,
        "info":     true,
        "filter":     true,
    });

    $("#viewContractList").on("click",function(){
        var ptName      = $("#ptName").val();
        $.ajax({
            method  : "POST",
            url     : "<?php echo base_url() ?>"+"reports/CR_Closing/dataClosing",
            data    : {
                pt      : ptName
            },
            success : function(data){
                employeeListTable.clear().draw();
                var dataSrc = JSON.parse(data);                 
                employeeListTable.rows.add(dataSrc).draw(false);
            },
            error   : function(data){
                alert("Failed");
            }   
        });

    });

    $.ajax({
        url : "<?php echo base_url() ?>"+"masters/Mst_client/loadAll",
        method : "POST",
        async : false,
        dataType : 'json',
        success: function(data){
        var html = '';
        var i;
            html += '<option value="" disabled="" selected="">Pilih</option>';
            html += '<option value="">ALL</option>';
            for(i=0; i<data.length; i++){
                html += '<option value="'+data[i].client_value+'" >'+data[i].client_name+'</option>';
            }   
        $('#ptName').html(html);
        }
    });

    $.ajax({
        url : "<?php echo base_url() ?>"+"masters/Mst_client/loadAll",
        method : "POST",
        async : false,
        dataType : 'json',
        success: function(data){
        var html = '';
        var i;
            html += '<option value="Pilih" selected="">Pilih Client</option>';
            html += '<option value="">ALL</option>';
            for(i=0; i<data.length; i++){
                html += '<option value="'+data[i].client_value+'" >'+data[i].client_name+'</option>';
            }   
        $('#iptName').html(html);
        }
    });

    $('#printContractList').click(function(e) {

        var myUrl = "<?php echo base_url() ?>"+"reports/CR_Employee_Data/exportdataCrews";

        $.ajax({
            method  : "POST",
            url     : "<?php echo base_url() ?>"+"reports/CR_Employee_Data/exportdataCrews",
            data    : {                     
                // ptName : ptName,
                // monthPeriod : monthPeriod,
                // yearPeriod : yearPeriod
            },                  
            success : function(response){
                console.log(response);
                window.open(myUrl,'_blank');
            },
            // success : function(data){
                // alert(data);
            // },
            error : function(data){
            alert("Failed");
            }   
        }); 
            
    }); 

    $('#contractListTable').on('click','#oClosingData', function (e) {
        var client    = $(this).data('client');
        var tahun     = $(this).data('tahun');
        var bulan     = $(this).data('bulan');

        $('#client').val(client);
        $('#tahun').val(tahun);
        $('#bulan').val(bulan);

        $('#client_name').val(client);
        $('#year').val(tahun);
        $('#month').val(bulan);
    });    

    $("#openData").on("click", function(){
        
        var client_name    = $('#client_name').val();
        var yearPeriod     = $('#year').val();
        var monthPeriod    = $('#month').val();

        /* START LOAD DATA FROM DATABASE TO TABLE */
        var myUrl = "<?php echo base_url() ?>"+"reports/CR_Closing/updateClosing";

        $.ajax({
            method : "POST",
            url : myUrl, 
            data : {
                client_name     : client_name,
                yearPeriod      : yearPeriod,
                monthPeriod     : monthPeriod,
                is_active       : 0
            },
            success : function(data){
                /* START ADD DATA TO TABLE */
                alert("Data has been Open");
                location.reload();
                // alert(data);
            },
            error : function(data){
                isValid = false;
                alert("Save failed");
            }
        });
        
    });

    $('#contractListTable').on('click','#cClosingData', function (e) {
        var client    = $(this).data('client');
        var tahun     = $(this).data('tahun');
        var bulan     = $(this).data('bulan');

        $('#Cclient').val(client);
        $('#Ctahun').val(tahun);
        $('#Cbulan').val(bulan);

        $('#Cclient_name').val(client);
        $('#Cyear').val(tahun);
        $('#Cmonth').val(bulan);
    });

    $("#closingData").on("click", function(){
        
        var client_name    = $('#Cclient_name').val();
        var yearPeriod     = $('#Cyear').val();
        var monthPeriod    = $('#Cmonth').val();

        /* START LOAD DATA FROM DATABASE TO TABLE */
        var myUrl = "<?php echo base_url() ?>"+"reports/CR_Closing/updateClosing";

        $.ajax({
            method : "POST",
            url : myUrl, 
            data : {
                client_name     : client_name,
                yearPeriod      : yearPeriod,
                monthPeriod     : monthPeriod,
                is_active       : 1
            },
            success : function(data){
                /* START ADD DATA TO TABLE */
                alert("Data has been Closing");
                location.reload();
                // alert(data);
            },
            error : function(data){
                isValid = false;
                alert("Save failed");
            }
        });
        
    });

    $("#closingStatus").on("click", function(){
        var client_name    = $('#iptName').val();
        var yearPeriod     = $('#iyear').val();
        var monthPeriod    = $('#imonth').val();

        var isValid = true;
        if($('#imonth option:selected').val() == "Pilih"){
            $("#imonth").focus();
            alert('Bulan Harus Dipilih ');
            isValid = false;
        } 
        else if($('#iyear option:selected').val() == "Pilih"){
            $("#iyear").focus();
            alert('Tahun Harus Dipilih ');
            isValid = false;
        } 
        else if($('#iptName option:selected').val() == "Pilih"){
            $("#iptName").focus();
            alert('Client Harus Dipilih ');
            isValid = false;
        } 
        if(isValid == false){return false}

        /* START LOAD DATA FROM DATABASE TO TABLE */
        var myUrl = "<?php echo base_url() ?>"+"reports/CR_Closing/insertClosing";

        $.ajax({
            method : "POST",
            url : myUrl, 
            data : {
                client_name     : client_name,
                yearPeriod      : yearPeriod,
                monthPeriod     : monthPeriod,
                is_active       : 1
            },
            success : function(data){
                if(data=='erorr'){
                    alert("Data Closing Sudah Ada.");    
                }else{
                    alert("Data has been Closing.");                    
                    // location.reload();
                }
                /* START ADD DATA TO TABLE */
                // alert(data);
            },
            error : function(data){
                debugger
                isValid = false;
                alert("Save failed");
            }
        });
        
    });

});
</script>
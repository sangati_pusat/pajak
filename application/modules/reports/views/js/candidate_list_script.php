<script type="text/javascript">
    $(document).ready(function(){

        

        $(".btnDetail").attr('disabled',true);

        var $tabs = $('#horizontalTab');
          $tabs.responsiveTabs({
          active: 0,
          rotate: false,
          startCollapsed: 'accordion',
          collapsible: 'accordion',
          setHash: true,
            // disabled: [3,4],
        });

        $('#fullNameErr').hide();
        $('#genderErr').hide();
        $('#birthPlaceErr').hide();
        $('#birthDayErr').hide();
        // $('#birthMonthErr').hide();
        // $('#birthYearErr').hide();
        $('#nationalityErr').hide();
        $('#etnicsErr').hide();
        $('#idNoErr').hide();
        $('#idAddressErr').hide();
        $('#currentAddressErr').hide();
        $('#livingStatusErr').hide();
        $('#religionErr').hide();
        // $('#driveLicenceErr').hide();
        $('#maritalStatusErr').hide();
        $('#appliedPositionErr').hide();
        $('#groupPositionErr').hide();
        $('#heightErr').hide();
        $('#weightErr').hide();
        $('#colorBlindnessErr').hide();
        $('#bloodTypeErr').hide();
        $('#emailAddressErr').hide();
        $('#telpNoErr').hide();
        $('#cellNoErr').hide();
        $('#pointOfHireErr').hide();

        function clearDetailInputs()
        {
            $('.my_detail :input').each(function(){
                $(this).val('');
            });            
        }

        var rowIdx = null;

        var biodataListTable = $('#biodataListTable').DataTable({
            "paging":   true,
            "ordering": true,
            "info":     true,
            "filter":     true,
            "columnDefs": [{
                "targets": -2,
                "className": "text-center",
                "data": null,
                "defaultContent": "<button class='btn btn-primary btn-sm btnHeaderUpdate' data-toggle='modal' data-target='#editBiodataModal'>&nbsp;&nbsp;<span class='fa fa-edit'></span></button>"
            },
            {
                "targets": -1,
                "className": "text-center",
                "data": null,
                "defaultContent": "<button class='btn btn-primary btn-sm btnHeaderPrint'>&nbsp;&nbsp;<span class='fa fa-print'></span></button>"
            }]
        });

        /* START CLICK MAIN BUTTON */
        $("#viewCandidateList").on("click", function(){
            var agesMax             = $("#agesMax").val();
            var searchByPosition    = $("#searchByPosition").val();
            var searchByCity        = $("#searchByCity").val();
            var searchByName        = $("#searchByName").val();
            var searchByIdNo        = $("#searchByIdNo").val();

            var myUrl = "";
            myUrl = "<?php echo base_url().'reports/CR_Candidate_List/getCandidateList'; ?>";
            // alert(myUrl);
            /* Ajax Is Here */
            $.ajax({
                method : "POST",
                url    : myUrl,
                data   : {
                    sAge        : agesMax,
                    sPosition   : searchByPosition,
                    sCity       : searchByCity,
                    sName       : searchByName,
                    sNo         : searchByIdNo
                },
                success : function(data){
                    biodataListTable.clear().draw();
                    var dataSrc = JSON.parse(data);                 
                    biodataListTable.rows.add(dataSrc).draw(false);
                },
                error   : function(data){
                    $.notify({
                        title: "<h5>Informasi : </h5>",
                        message: "<strong>"+data+"</strong> </br></br> ",
                        icon: '' 
                    },
                    {
                        type: "warning",
                        delay: 3000
                    }); 
                }
            });
        });
        /* END CLICK MAIN BUTTON */

        $.ajax({
            url : "<?php echo base_url() ?>"+"masters/Mst_position/getPositionGroup",
            method : "POST",
            data : {
                id : ""
            },
            success : function(data){
                var dataSrc = JSON.parse(data); 
                var dataLength = dataSrc.length;
                $('#groupPosition').find('option:not(:first)').remove();
                for(var i = 0; i < dataLength; i++){
                    $('#groupPosition').append(new Option(dataSrc[i]['pg_name'], dataSrc[i]['pg_id']));
                }
            },
            error : function(){
                alert('Failed');
            }
        });

        var gpId = '-';
        $('#groupPosition').on('change', function(){
            gpId = $('#groupPosition').val();
            var myUrl = "<?php echo base_url() ?>"+"masters/Mst_position/getPositionByPG/"+gpId; 
            $.ajax({
                url : myUrl,
                method : "POST",
                data : {
                    id : ""
                },
                success : function(data){
                    var dataSrc = JSON.parse(data); 
                    var dataLength = dataSrc.length;
                    $('#appliedPosition').find('option:not(:first)').remove();
                    for(var i = 0; i < dataLength; i++){
                        $('#appliedPosition').append(new Option(dataSrc[i]['position_name'], dataSrc[i]['position_name']));
                    }
                },
                error : function(){
                    alert('Failed');
                }
            });  
        });

        $.ajax({
            method : "POST",
            url : "<?php echo base_url() ?>"+"masters/Mst_city/getAllCity", 
            data : {
                id : "",
                val : ""
            },
            success : function(data) {
                var srcData = JSON.parse(data);
                // alert(data);
                var i;
                for (i = 0; i < srcData.length; ++i) {
                    // alert(srcData[i]);
                    $('#cityAddress').append('<option value="'+srcData[i]+'">'+srcData[i]+'</option>');
                }
            },
            error : function(data) {
                alert("Failed");
            } 
        })


        /* START SELECT DATA */
        $("#biodataListTable tbody").on("click", ".btnHeaderUpdate", function(){
            var rowData = biodataListTable.row( $(this).parents('tr') ).data();
            var bioId   = rowData[0];
            var myURL   = "<?php echo base_url().'reports/CR_Candidate_List/GetBiodataJasonById/' ?>"/*+bioId*/;
            $.ajax({
                method: "POST",
                url : myURL,
                data: {
                    bioId : bioId
                },
                error : function(data){
                    alert("Load Data Gagal");
                },
                success : function(data){
                    var dataSrc = JSON.parse(data);

                    var isActive = dataSrc[30];
                    if(isActive == '1'){
                        alert('You have no access for editing active data');
                        $('#editBiodataModal').modal('hide');
                        $('.btnDelTable').attr('disabled', true);                       
                    }
                    else
                    {
                        $('.btnDelTable').attr('disabled', true);                                      
                        $("#biodataId").val(dataSrc[0]);
                        $("#fullName").val(dataSrc[3]); 
                        $("#gender").val(dataSrc[4]); 
                        $("#birthPlace").val(dataSrc[5]); 
                        tmpDate = dataSrc[6];
                        $("#birthDay").val(tmpDate); 
                        $("#pointOfHire").val(dataSrc[8]);
                        $("#nationality").val(dataSrc[9]);
                        $("#etnics").val(dataSrc[10]);
                        $("#idNo").val(dataSrc[11]);
                        $("#npwpNo").val(dataSrc[12]);
                        $("#bpjsNo").val(dataSrc[13]);
                        $("#idAddress").val(dataSrc[14]);
                        $("#currentAddress").val(dataSrc[15]);                        
                        $("#cityAddress").val(dataSrc[16]);
                        $("#livingStatus").val(dataSrc[17]);
                        $("#religion").val(dataSrc[18]);
                        $("#driveLicence").val(dataSrc[19]);
                        $("#maritalStatus").val(dataSrc[20]);
                        var appPosition = dataSrc[21];
                        $('#appliedPosition option:contains("' + appPosition + '")').attr('selected', 'selected');                        
                        $("#cvDate").val(dataSrc[22]);                        
                        $("#height").val(dataSrc[23]);
                        $("#weight").val(dataSrc[24]);
                        $("#colorBlindness").val(dataSrc[25]);
                        $("#bloodType").val(dataSrc[26]);
                        $("#emailAddress").val(dataSrc[27]);
                        $("#telpNo").val(dataSrc[28]);
                        $("#cellNo").val(dataSrc[29]);
                    }
                }
            });
        });
        /* END SELECT DATA */

        $('#saveBiodata').on('click', function(){

            /* Start Header Form Validation */
            $('#fullNameErr').hide();
            $('#genderErr').hide();
            $('#birthPlaceErr').hide();
            $('#birthDayErr').hide();
            // $('#birthMonthErr').hide();
            // $('#birthYearErr').hide();
            $('#nationalityErr').hide();
            $('#etnicsErr').hide();
            $('#idNoErr').hide();
            $('#idAddressErr').hide();
            $('#currentAddressErr').hide();
            $('#livingStatusErr').hide();
            $('#religionErr').hide();
            // $('#driveLicenceErr').hide();
            $('#maritalStatusErr').hide();
            $('#appliedPositionErr').hide();
            $('#groupPositionErr').hide();
            $('#heightErr').hide();
            $('#weightErr').hide();
            $('#colorBlindnessErr').hide();
            $('#bloodTypeErr').hide();
            $('#emailAddressErr').hide();
            $('#telpNoErr').hide();
            $('#cellNoErr').hide();
            $('#pointOfHireErr').hide();

            var isHeaderValid = true;
            if ($('#fullName').val() == ""){
                $('#fullName').focus(); 
                $('#fullNameErr').show();
                isHeaderValid = false;
            }
            else if ($('#gender option:selected').text() == "Pilih"){
                $('#gender').focus(); 
                $('#genderErr').show();
                isHeaderValid = false;
            }
            else if ($('#birthPlace').val() == ""){
                $('#birthPlace').focus(); 
                $('#birthPlaceErr').show();
                isHeaderValid = false;
            }
            else if ($('#birthDay').val() == ""){
                $('#birthDay').focus(); 
                $('#birthDayErr').show();
                isHeaderValid = false;
            }

            else if ($('#nationality').val() == ""){
                $('#nationality').focus(); 
                $('#nationalityErr').show();
                isHeaderValid = false;
            }
            else if ($('#etnics option:selected').text() == "Pilih"){
                $('#etnics').focus(); 
                $('#etnicsErr').show();
                isHeaderValid = false;
            }
            else if ($('#idNo').val() == ""){
                $('#idNo').focus(); 
                $('#idNoErr').show();
                isHeaderValid = false;
            }
            else if ($('#idAddress').val() == ""){
                $('#idAddress').focus(); 
                $('#idAddressErr').show();
                isHeaderValid = false;
            }
            else if ($('#currentAddress').val() == ""){
                $('#currentAddress').focus(); 
                $('#currentAddressErr').show();
                isHeaderValid = false;
            }
            else if ($('#livingStatus option:selected').text() == "Pilih"){
                $('#livingStatus').focus(); 
                $('#livingStatusErr').show();
                isHeaderValid = false;
            }
            else if ($('#religion option:selected').text() == "Pilih"){
                $('#religion').focus(); 
                $('#religionErr').show();
                isHeaderValid = false;
            }            
            else if ($('#maritalStatus option:selected').text() == "Pilih"){
                $('#maritalStatus').focus(); 
                $('#maritalStatusErr').show();
                isHeaderValid = false;
            }
            else if ($('#groupPosition option:selected').text() == "Pilih"){
                $('#groupPosition').focus(); 
                $('#groupPositionErr').show();
                isHeaderValid = false;
            }
            else if ($('#appliedPosition option:selected').text() == "Pilih"){
                $('#appliedPosition').focus(); 
                $('#appliedPositionErr').show();
                isHeaderValid = false;
            }
            else if ($('#height').val() == ""){
                $('#height').focus(); 
                $('#heightErr').show();
                isHeaderValid = false;
            }
            else if ($('#weight').val() == ""){
                $('#weight').focus(); 
                $('#weightErr').show();
                isHeaderValid = false;
            }
            else if ($('#colorBlindness option:selected').text() == "Pilih"){
                $('#colorBlindness').focus(); 
                $('#colorBlindnessErr').show();
                isHeaderValid = false;
            }
            else if ($('#bloodType option:selected').text() == "Pilih"){
                $('#bloodType').focus(); 
                $('#bloodTypeErr').show();
                isHeaderValid = false;
            }
            else if ($('#emailAddress').val() == ""){
                $('#emailAddress').focus(); 
                $('#emailAddressErr').show();
                isHeaderValid = false;
            }
            else if ($('#telpNo').val() == ""){
                $('#telpNo').focus(); 
                $('#telpNoErr').show();
                isHeaderValid = false;
            }
            else if ($('#cellNo').val() == ""){
                $('#cellNo').focus(); 
                $('#cellNoErr').show();
                isHeaderValid = false;
            }
            else if ($('#pointOfHire').val() == ""){
                $('#pointOfHire').focus(); 
                $('#pointOfHireErr').show();
                isHeaderValid = false;
            }
            if(isHeaderValid == false){
                return false;
            }

            biodataId       = $('#biodataId').val();
            fullName        = $('#fullName').val();
            gender          = $('#gender').val();
            birthPlace      = $('#birthPlace').val();
            birthDate       = $('#birthDay').val();
            nationality     = $('#nationality').val();  
            etnics          = $('#etnics').val();    
            idNo            = $('#idNo').val();    
            npwpNo          = $('#npwpNo').val();    
            bpjsNo          = $('#bpjsNo').val();    
            idAddress       = $('#idAddress').val();  
            currentAddress  = $('#currentAddress').val();                
            cityAddress     = $('#cityAddress').val();              
            livingStatus    = $('#livingStatus').val();    
            religion        = $('#religion').val(); 
            driveLicence    = $('#driveLicence').val();    
            maritalStatus   = $('#maritalStatus').val();
            appliedPosition = $('#appliedPosition').val();              
            cvDate          = $('#cvDate').val();                
            height          = $('#height').val();    
            weight          = $('#weight').val();    
            colorBlindness  = $('#colorBlindness').val();    
            bloodType       = $('#bloodType').val();  
            emailAddress    = $('#emailAddress').val();    
            telpNo          = $('#telpNo').val();    
            cellNo          = $('#cellNo').val();
            pointOfHire     = $('#pointOfHire').val();
            groupPosition   = $

            $.ajax({                
                method : "POST", 
                url : "<?php echo base_url() ?>"+"reports/CR_Candidate_List/Upd/", 
                data : {
                    updMstBioRec : "1",
                    biodataId : biodataId,
                    fullName : fullName,
                    gender : gender,
                    birthPlace : birthPlace,
                    birthDate : birthDate,
                    nationality : nationality,  
                    etnics : etnics,    
                    idNo : idNo,    
                    npwpNo : npwpNo,    
                    bpjsNo : bpjsNo,
                    idAddress : idAddress,  
                    currentAddress : currentAddress,    
                    cityAddress : cityAddress,  
                    livingStatus : livingStatus,    
                    religion : religion, 
                    driveLicence : driveLicence,    
                    maritalStatus : maritalStatus,
                    appliedPosition : appliedPosition,  
                    cvDate : cvDate, 
                    height : height,    
                    weight : weight,    
                    colorBlindness : colorBlindness,    
                    bloodType : bloodType,  
                    emailAddress : emailAddress,    
                    telpNo : telpNo,    
                    cellNo : cellNo,
                    pointOfHire : pointOfHire                                                            
                }, 
                error   : function(data) {
                    alert("Failed to save");       
                },
                success : function(data) {
                    // $("#saveMsg").html("ID : <div id='succeedId'><h3>"+data+"</h3></div> Berhasil Disimpan");    
                    biodataListTable.row(rowIdx).data([
                        biodataId,fullName,idAddress,cityAddress,cellNo,appliedPosition,cvDate]);   
                    alert("Data has been saved");  
                    // alert(data);  
                    clearDetailInputs();
                    $("#gender").focus();     
                }
            }); 
        });

        var educationTable = $('#educationTable').DataTable({
          "paging"        : false,
          "responsive"    : true,
          "ordering"      : false,
          "info"          : false,
          "filter"        : false,
          "columnDefs": [{
              "targets": -1,
              "data": null,
              "defaultContent": "<button class='btn btn-danger btnEducationDelete btn-sm btnDelete'><i class='fa fa-times' aria-hidden='true'></i></button>"
          }]
        });
        var trainingTable = $('#trainingTable').DataTable({
          "paging"    :false,
          "ordering"  :false,
          "info"      :false,
          "filter"    :false,
          "columnDefs":[{
            "targets"       :-1,
            "data"          :null,
            "defaultContent": "<button class='btn btn-danger btnTrainingDelete btn-sm btnDelete'><i class='fa fa-times' aria-hidden='true'></i></button>"
          }]
        });
        var experienceTable = $('#experienceTable').DataTable({
          "paging"    : false,
          "ordering"  : false,
          "info"      : false,
          "filter"    : false,
          "columnDefs": [{
            "targets"       : -1,
            "data"          : null,
            "defaultContent": "<button class='btn btn-danger btn-sm btnExperienceDelete btnDelete'><i class='fa fa-times' aria-hidden='true'></i></button>"
          }]
        }); 
        var familyTable = $('#familyTable').DataTable({
          "paging"    : false,
          "ordering"  : false,
          "info"      : false,
          "filter"    : false,
          "columnDefs": [{
            "targets"       : -1,
            "data"          : null,
            "defaultContent": "<button class='btn btn-danger btn-sm btnFamilyDelete btnDelete'><i class='fa fa-times' aria-hidden='true'></i></button>"
          }]
        }).draw(false);
        var languageTable = $('#languageTable').DataTable({
          "paging"    : false,
          "ordering"  : false,
          "info"      : false,
          "filter"    : false,
          "columnDefs": [{
            "targets"       : -1,
            "data"          : null,
            "defaultContent": "<button class='btn btn-danger btn-sm btnLanguageDelete btnDelete'><i class='fa fa-times' aria-hidden='true'></i></button>"
           }]
        });
        var organizationTable = $('#organizationTable').DataTable({
          "paging"    : false,
          "ordering"  : false,
          "info"      : false,
          "filter"    : false,
          "columnDefs": [{
            "targets"       : -1,
            "data"          : null,
            "defaultContent": "<button class='btn btn-danger btn-sm btnOrganizationDelete btnDelete'><i class='fa fa-times' aria-hidden='true'></i></button>"
            }]
        }).draw(false);
        var referenceTable = $('#referenceTable').DataTable({
          "paging"  : false,
          "ordering": false,
          "info"    : false,
          "filter"  : false,
          "columnDefs": [{
            "targets"       : -1,
            "data"          : null,
            "defaultContent": "<button class='btn btn-danger btn-sm btnReferenceDelete btnDelete'><i class='fa fa-times' aria-hidden='true'></i></button>"
            }]
        }).draw(false);
        var qualificationTable = $('#qualificationTable').DataTable({
          "paging"  : false,
          "ordering": false,
          "info"    : false,
          "filter"  : false,
          "columnDefs": [{
            "targets"       : -1,
            "data"          : null,
            "defaultContent": "<button class='btn btn-danger btn-sm btnQualificationDelete btnDelete'><i class='fa fa-times' aria-hidden='true'></i></button>"
          }]
        }).draw(false);


        $('#biodataListTable tbody').on( 'click', 'tr', function () {

            $(".btnDetail").attr('disabled',false);

            var rowData = biodataListTable.row( this ).data();
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');
            }
            else {
                biodataListTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
            
            // if ( $(this).hasClass('selected') ) 
            // {
            //     $(this).removeClass('selected');
            // }
            // else 
            // {
            //     biodataListTable.$('tr.selected').removeClass('selected');
            //     $(this).addClass('selected');                    
            // }
            headerId = rowData[0];   
            $(".btnDetail").attr('disabled',false);  
            
            rowIdx = biodataListTable.row( this ).index();
            $("#rowIdx").val(rowIdx); 

            $.ajax({
                method  : "POST",
                url     : "<?php echo base_url().'masters/Mst_bio_rec/GetEducationByBioRecId/' ?>"+headerId,
                data    : {
                    headerId : headerId 
                },
                error   : function(data){
                    alert(data);
                },
                success : function(data){
                    // debugger
                    educationTable.clear().draw();
                    var dataSrc = JSON.parse(data); 
                    educationTable.rows.add(dataSrc).draw(false);                                           
                }
            }); 

            $.ajax({
                method  : "POST",
                url     : "<?php echo base_url().'masters/Mst_bio_rec/GetTrainingByBioRecId/' ?>"+headerId,
                data    : {
                    headerId : headerId 
                },
                error   : function(data){
                    alert(data);
                },
                success : function(data){
                    trainingTable.clear().draw();
                    var dataSrc = JSON.parse(data);                 
                    trainingTable.rows.add(dataSrc).draw(false);                                            
                }
            }); 

            $.ajax({
                method  : "POST",
                url     : "<?php echo base_url().'masters/Mst_bio_rec/GetExperienceByBioRecId/' ?>"+headerId,
                data    : {
                    headerId : headerId 
                },
                error   : function(data){
                    alert(data);
                },
                success : function(data){
                    experienceTable.clear().draw();
                    var dataSrc = JSON.parse(data);                 
                    experienceTable.rows.add(dataSrc).draw(false);                                          
                }
            }); 

            $.ajax({
                method  : "POST",
                url     : "<?php echo base_url().'masters/Mst_bio_rec/GetFamilyByBioRecId/' ?>"+headerId,
                data    : {
                    headerId : headerId 
                },
                error   : function(data){
                    alert(data);
                },
                success : function(data){
                    familyTable.clear().draw();
                    var dataSrc = JSON.parse(data);                 
                    familyTable.rows.add(dataSrc).draw(false);                                          
                }
            }); 

            $.ajax({
                method  : "POST",
                url     : "<?php echo base_url().'masters/Mst_bio_rec/GetLanguageByBioRecId/' ?>"+headerId,
                data    : {
                    headerId : headerId 
                },
                error   : function(data){
                    alert(data);
                },
                success : function(data){
                    languageTable.clear().draw();
                    var dataSrc = JSON.parse(data);                 
                    languageTable.rows.add(dataSrc).draw(false);                                            
                }
            }); 

            $.ajax({
                method  : "POST",
                url     : "<?php echo base_url().'masters/Mst_bio_rec/GetOrganizationByBioRecId/' ?>"+headerId,
                data    : {
                    headerId : headerId 
                },
                error   : function(data){
                    alert(data);
                },
                success : function(data){
                    organizationTable.clear().draw();
                    var dataSrc = JSON.parse(data);                 
                    organizationTable.rows.add(dataSrc).draw(false);                                            
                }
            }); 

            $.ajax({
                method  : "POST",
                url     : "<?php echo base_url().'masters/Mst_bio_rec/GetReferencesByBioRecId/' ?>"+headerId,
                data    : {
                    headerId : headerId 
                },
                error   : function(data){
                    alert(data);
                },
                success : function(data){
                    referenceTable.clear().draw();
                    var dataSrc = JSON.parse(data);                 
                    referenceTable.rows.add(dataSrc).draw(false);                                           
                }
            }); 

            $.ajax({
                method  : "POST",
                url     : "<?php echo base_url().'masters/Mst_bio_rec/GetQualificationByBioRecId/' ?>"+headerId,
                data    : {
                    headerId : headerId 
                },
                error   : function(data){
                    alert(data);
                },
                success : function(data){
                    qualificationTable.clear().draw();
                    var dataSrc = JSON.parse(data);                 
                    qualificationTable.rows.add(dataSrc).draw(false);                                           
                }
            }); 

        });

        $('#saveEducation').click(function(){
            var isCertified = "Y";
            if ($('#isCertified').is(":checked"))
            {   
                isCertified = "Y";
            }              
            else
            {
                isCertified = "T";
            }

            /* Start EDUCATION Form Validation */
            var isDataValid = true;
            $('#schoolNameErr').hide();
            $('#educationMajorErr').hide();
            $('#educationYearErr').hide();
            $('#educationCityErr').hide();
            
            if($('#schoolName').val() == "") 
            {
                $('#schoolName').focus();
                $('#schoolNameErr').show();
                isDataValid = false;
            }
            else if($('#educationMajor').val() == "") 
            {
                $('#educationMajor').focus();
                $('#educationMajorErr').show();
                isDataValid = false;
            }
            else if($('#educationYear option:selected').text() == "Pilih") 
            {
                $('#educationYear').focus();
                $('#educationYearErr').show();
                isDataValid = false;
            }
            else if($('#educationCity').val() == "") 
            {
                $('#educationCity').focus();
                $('#educationCityErr').show();
                isDataValid = false;
            }

            if(isDataValid == false)
            {
                return false;
            }

            $.ajax({
                method : "POST",
                url     : "<?php echo base_url().'masters/Mst_bio_rec/InsEducation/' ?>",
                data : {
                    headerId       : headerId,  
                    schoolName     : $('#schoolName').val(), 
                    educationMajor : $('#educationMajor').val(),
                    educationYear  : $('#educationYear').val(),
                    educationCity  : $('#educationCity').val(),
                    isCertified    : isCertified
                },
                success : function(data){
                    educationTable.clear().draw();
                    var dataSrc = JSON.parse(data); 
                    educationTable.rows.add(dataSrc).draw(false);        
                    alert("Data has been saved");
                    clearDetailInputs();
                    $('#schoolName').focus();
                },
                error : function(data){
                    alert("Data failed to save");
                }
            });                                          
        });

        $('#educationTable tbody').on('click', '.btnEducationDelete', function(){
            // debugger
            rowData = educationTable.row( $(this).parents('tr') ).data();
            educationId = rowData[0];  
            is_valid = confirm('Data : '+educationId+' will deleted?');
            if (is_valid) 
            {
                educationTable.row( $(this).parents('tr') ).remove().draw();
                $.ajax({
                    method : "POST",
                    url    : "<?php echo base_url() ?>"+"masters/Mst_bio_rec/DelEducationById/"+educationId,
                    data   : {
                        educationId : educationId
                    },
                    success : function(data){
                        alert("Data has been deleted");
                    },
                    error   : function(data){
                        alert("Data failed to delete");
                    }   
                });
            }
        });

        $('#saveTraining').click(function(){           
            var isTrainingCertified = "Y";
            if ($('#isTrainingCertified').is(":checked"))
            {   
                isTrainingCertified = "Y";
            }              
            else
            {
                isTrainingCertified = "T";
            }

            /* Start TRAINING Form Validation */
            $('#instituteNameErr').hide();
            $('#classesFieldErr').hide();
            $('#trainingYearErr').hide();
            $('#trainingCityErr').hide();
            var isDataValid = true;
            if($('#instituteName').val() == "") 
            {
                $('#instituteName').focus();    
                $('#instituteNameErr').show();
                isDataValid = false;    
            }
            else if($('#classesField').val() == "") 
            {
                $('#classesField').focus(); 
                $('#classesFieldErr').show();
                isDataValid = false;        
            }
            else if($('#trainingYear option:selected').text() == "Pilih") 
            {
                $('#trainingYear').focus(); 
                $('#trainingYearErr').show();
                isDataValid = false;        
            }
            else if($('#trainingCity').val() == "")
            {
                $('#trainingCity').focus(); 
                $('#trainingCityErr').show();
                isDataValid = false;            
            }

            if(isDataValid == false)
            {
                return false;
            }

            /* End TRAINING Form Validation */

            $.ajax({
                method : "POST",
                url    : "<?php echo base_url().'masters/Mst_bio_rec/InsTraining/' ?>",
                data   : {
                    headerId       : headerId,  
                    instituteName  : $('#instituteName').val(), 
                    classesField   : $('#classesField').val(),
                    trainingYear   : $('#trainingYear').val(),
                    trainingCity   : $('#trainingCity').val(),
                    isCertified    : isTrainingCertified
                },
                success : function(data){
                    trainingTable.clear().draw();
                    var dataSrc = JSON.parse(data); 
                    trainingTable.rows.add(dataSrc).draw(false);
                    alert("Data has been saved");
                    clearDetailInputs();
                    $('#instituteName').focus();
                },
                error : function(data){
                    alert("Data failed to save");
                }
            });
        });

        var isDataValid = false;
        $('#trainingTable tbody').on('click', '.btnTrainingDelete', function(){
            rowData = trainingTable.row( $(this).parents('tr') ).data();
            trainingId = rowData[0];  
            is_valid = confirm('Data : '+trainingId+' will deleted?');
            if (is_valid) 
            {
                trainingTable.row( $(this).parents('tr') ).remove().draw();
                $.ajax({
                    method : "POST",
                    url    : "<?php echo base_url() ?>"+"masters/Mst_bio_rec/DelTrainingById/"+trainingId,
                    data   : {
                        trainingId : trainingId
                    },
                    success : function(data){
                        alert("Data has been deleted");
                    },
                    error   : function(data){
                        alert("Data failed to delete");
                    }   
                });
            }
        });

        $('#saveExperience').click(function() 
        {
            /* Start EXPERIENCE Form Validation */
            $('#experienceCompanyNameErr').hide();
            $('#experiencePositionErr').hide();
            $('#startWorkDateErr').hide();
            $('#experienceYearErr').hide();
            $('#experienceBasicSalaryErr').hide();
            $('#jobDescErr').hide();
            $('#resignReasonErr').hide();

            var isDataValid = true;        
            if($('#experienceCompanyName').val() == ""){
                $('#experienceCompanyName').focus();
                $('#experienceCompanyNameErr').show();
                isDataValid = false;
            }
            else if($('#experiencePosition').val() == ""){
                $('#experiencePosition').focus();
                $('#experiencePositionErr').show();
                isDataValid = false;
            }
            else if($('#startWorkDate').val() == ""){
                $('#startWorkDate').focus();
                $('#startWorkDateErr').show();
                isDataValid = false;
            }
            else if($('#experienceYear option:selected').text() == "Pilih"){
                $('#experienceYear').focus();
                $('#experienceYearErr').show();
                isDataValid = false;
            }

            else if($('#experienceBasicSalary').val() == ""){
                $('#experienceBasicSalary').focus();
                $('#experienceBasicSalaryErr').show();
                isDataValid = false;
            }

            else if($('#jobDesc').val() == ""){
                $('#jobDesc').focus();
                $('#jobDescErr').show();
                isDataValid = false;
            }

            if(isDataValid == false){
                return false;
            }

            var startWork = $('#startWorkDate').val();
            var endWork = $('#endWorkDate').val();
            if ($('#endWorkDate').val() == ""){
                endWork = "";
            }

            var isCurrentWork = "Y";
            if ($('#isCurrentWork').is(":checked")){   
                isCurrentWork = "Y";
            }else{
                isCurrentWork = "T";
            } 

            $.ajax({
                method : "POST",
                url : "<?php echo base_url() ?>"+"masters/Mst_bio_rec/InsertExperience/", 
                data   : {
                    headerId              : headerId,   
                    experienceCompanyName : $('#experienceCompanyName').val(),
                    experiencePosition    : $('#experiencePosition').val(),
                    startWork             : startWork,
                    isCurrentWork         : isCurrentWork,
                    endWork               : endWork,
                    experienceBasicSalary : $('#experienceBasicSalary').val(),
                    jobDesc               : $('#jobDesc').val(),
                    resignReason          : $('#resignReason').val(),
                    addInfo               : $('#addInfo').val()
                },
                success : function(data){
                    // alert(data);
                    experienceTable.clear().draw();
                    var dataSrc = JSON.parse(data); 
                    experienceTable.rows.add(dataSrc).draw(false);
                    alert("Data has been saved ");
                    clearDetailInputs();
                    $('#experienceCompanyName').focus();
                },
                error : function(data){
                    alert("Failed to save");
                }
            });          
        });

        var isDataValid = false;
        $('#experienceTable tbody').on('click', '.btnExperienceDelete', function(){
            rowData = experienceTable.row( $(this).parents('tr') ).data();
            experienceId = rowData[0];  
            is_valid = confirm('Data : '+experienceId+' will deleteted?');
            if (is_valid){
                experienceTable.row( $(this).parents('tr') ).remove().draw();
                $.ajax({
                    method : "POST",
                    url    : "<?php echo base_url() ?>"+"masters/Mst_bio_rec/DelExperienceById/"+experienceId,
                    data   : {
                        experienceId : experienceId
                    },
                    success : function(data){
                        alert("Data has been deleted");
                    },
                    error   : function(data){
                        alert("Failed to delete");
                    }   
                });
            }
        });

        $('#saveFamily').click(function() 
        {       
            $('#familyNameErr').hide();
            $('#familyRelationErr').hide();
            $('#familyDateOfBirthErr').hide();
            $('#familyEducationErr').hide();

            var isDataValid = true;        
            if($('#familyName').val() == "")
            {
                $('#familyName').focus();
                $('#familyNameErr').show();
                isDataValid = false;
            }
            else if($('#familyRelation').val() == "")
            {
                $('#familyRelation').focus();
                $('#familyRelationErr').show();
                isDataValid = false;
            }

            else if($('#familyDateOfBirth').val() == "")
            {
                $('#familyDateOfBirth').focus();
                $('#familyDateOfBirthErr').show();
                isDataValid = false;
            }   

            else if($('#familyEducation option:selected').text() == "Pilih")
            {
                $('#familyEducationErr').focus();
                $('#familyEducationErr').show();
                isDataValid = false;
            }

            if(isDataValid == false)
            {
                return false;
            }   

            var birthDate = $('#familyDateOfBirth').val();  

            $.ajax({
                method  : "POST",
                url     : "<?php echo base_url() ?>"+"masters/Mst_bio_rec/InsertFamily/", 
                data    : {
                    headerId         : headerId, 
                    familyName       : $('#familyName').val(),
                    familyRelation   : $('#familyRelation').val(),
                    birthDate        : birthDate,
                    familyEducation  : $('#familyEducation').val(),
                    familyOccupation : $('#familyOccupation').val()
                },
                success : function(data){
                    familyTable.clear().draw();
                    var dataSrc = JSON.parse(data); 
                    familyTable.rows.add(dataSrc).draw(false);
                    alert("Data has been saved");
                    clearDetailInputs();
                    $('#familyName').focus();
                },
                error   : function(data){
                    alert("Failed to save");
                }
            });                                          
        });

        var is_valid = false;
        $('#familyTable tbody').on('click', '.btnFamilyDelete', function(){
            rowData = familyTable.row( $(this).parents('tr') ).data();
            familyId = rowData[0];  
            is_valid = confirm('Data dengan ID : '+familyId+' akan dihapus?');
            if (is_valid) 
            {
                familyTable.row( $(this).parents('tr') ).remove().draw();
                $.ajax({
                    method : "POST",
                    url    : "<?php echo base_url() ?>"+"masters/Mst_bio_rec/DelFamilyById/"+familyId,
                    data   : {
                        familyId : familyId
                    },
                    success : function(data){
                        alert("Data has been deleted");
                    },
                    error   : function(data){
                        alert("Failed to delete");
                    }   
                });
            }
        });

        $('#saveLanguage').click(function() 
        {
            var isDataValid = true;
            $('#languageNameErr').hide();
            $('#writtenErr').hide();
            $('#oralErr').hide();
            if($('#languageName option:selected').text() == "Pilih")
            {
                $('#languageName').focus();
                $('#languageNameErr').show();
                isDataValid = false;
            }
            else if($('#written option:selected').text() == "Pilih")
            {
                $('#written').focus();
                $('#writtenErr').show();
                isDataValid = false;    
            }
            else if($('#oral option:selected').text() == "Pilih")
            {
                $('#oral').focus();
                $('#oralErr').show();
                isDataValid = false;    
            }

            if(isDataValid == false)
            {
                return false;
            }
            
            $.ajax({
                method : "POST",
                url    : "<?php echo base_url() ?>"+"masters/Mst_bio_rec/InsertLanguages/", 
                data   : {
                    headerId      : headerId,
                    languageName  : $('#languageName').val(),
                    written       : $('#written').val(),
                    oral          : $('#oral').val(),
                    languageInfo  : $('#languageInfo').val()
                },
                success: function(data){
                    languageTable.clear().draw();
                    var dataSrc = JSON.parse(data);
                    languageTable.rows.add(dataSrc).draw(false);
                    alert("Data has been saved");
                    clearDetailInputs();
                    $('#languageName').focus();
                },
                error  : function(data){
                    alert("Failed to save");
                } 
            });             
        });

        var is_valid = false;
        $('#languageTable tbody').on('click', '.btnLanguageDelete', function(){
            rowData = languageTable.row( $(this).parents('tr') ).data();
            languageId = rowData[0];  
            is_valid = confirm('Data dengan ID : '+languageId+' akan dihapus?');
            if (is_valid){
                languageTable.row( $(this).parents('tr') ).remove().draw();
                $.ajax({
                    method : "POST",
                    url    : "<?php echo base_url() ?>"+"masters/Mst_bio_rec/DelLanguageById/"+languageId,
                    data   : {
                        languageId : languageId
                    },
                    success : function(data){
                        alert("Data has been deleted");
                    },
                    error   : function(data){
                        alert("Failed to delete");
                    }   
                });
            }

        });

        $('#saveOrganization').click(function(){       
            var isDataValid = true;
            $('#organizationNameErr').hide();
            $('#organizationYearErr').hide();
            $('#organizationPositionErr').hide();

            if($('#organizationName').val() == ""){
                $('#organizationName').focus();
                $('#organizationNameErr').show();
                isDataValid = false;
            }
            else if($('#organizationYear option:selected').text() == "Pilih"){               
                $('#organizationYear').focus();
                $('#organizationYearErr').show();
                isDataValid = false;                
            }
            else if($('#organizationPosition').val() == ""){
                $('#organizationPosition').focus();
                $('#organizationPositionErr').show();
                isDataValid = false;
            }
            if(isDataValid == false){
                return false;
            } 
            /* End ORGANIZATION Form Validation */

            $.ajax({
                method : "POST",
                url    : "<?php echo base_url() ?>"+"masters/Mst_bio_rec/InsertOrganizations/", 
                data   : {
                    headerId             : headerId,
                    organizationName     : $('#organizationName').val(),
                    organizationYear     : $('#organizationYear').val(),
                    organizationPosition : $('#organizationPosition').val()
                },
                success : function(data){
                    organizationTable.clear().draw();
                    var dataSrc = JSON.parse(data);
                    organizationTable.rows.add(dataSrc).draw(false);
                    alert("Data has been saved");
                    clearDetailInputs();
                    $('#organizationName').focus();
                },
                error    : function(data){
                    alert("Failed to save");
                }
            });     
        });

        var isDataValid = false;
        $('#organizationTable tbody').on('click', '.btnOrganizationDelete', function(){     
            rowData = organizationTable.row( $(this).parents('tr') ).data();
            organizationId = rowData[0];  
            is_valid = confirm('Data dengan ID : '+organizationId+' akan dihapus?');
            if (is_valid) 
            {
                organizationTable.row( $(this).parents('tr') ).remove().draw();
                $.ajax({
                    method : "POST",
                    url    : "<?php echo base_url() ?>"+"masters/Mst_bio_rec/DelOrganizationById/"+organizationId,
                    data   : {
                        organizationId : organizationId
                    },
                    success : function(data){
                        alert("Data has been deleted");
                    },
                    error   : function(data){
                        alert("Failed to delete");
                    }   
                });
            }
        });

        $('#saveReference').click(function(){   
            var isDataValid = true; 
            $('#referenceNameErr').hide();
            $('#referenceRelationErr').hide();
            $('#referenceContactErr').hide();                       
            if($('#referenceName').val() == ""){
                $('#referenceName').focus();    
                $('#referenceNameErr').show();
                isDataValid = false;
            }                       
            else if($('#referenceRelation').val() == ""){
                $('#referenceRelation').focus();
                $('#referenceRelationErr').show();
                isDataValid = false;
            }
            else if($('#referenceContact').val() == ""){
                $('#referenceContact').focus();
                $('#referenceContactErr').show();
                isDataValid = false;
            }   
            if(isDataValid == false){
                return false;
            } 

            $.ajax({
                method : "POST",
                url    : "<?php echo base_url() ?>"+"masters/Mst_bio_rec/InsertReference/", 
                data   : {
                    headerId : headerId,
                    referenceName     : $('#referenceName').val(),
                    referenceRelation : $('#referenceRelation').val(),
                    referenceAddress  : $('#referenceAddress').val(),
                    referenceContact  : $('#referenceContact').val()
                },
                success : function(data){
                    referenceTable.clear().draw();
                    var dataSrc = JSON.parse(data);
                    referenceTable.rows.add(dataSrc).draw(false);
                    alert("Data has been saved");
                    clearDetailInputs();
                    $('#referenceName').focus();
                },
                error : function(data){
                    alert("Failed to save");
                }   
            });                         
        });

        var is_valid = false;
        $('#referenceTable tbody').on( 'click', '.btnReferenceDelete', function () 
        {   
            rowData = referenceTable.row( $(this).parents('tr') ).data();
            referenceId = rowData[0];  
            is_valid = confirm('Data dengan ID : '+referenceId+' akan dihapus?');
            if (is_valid) 
            {
                referenceTable.row( $(this).parents('tr') ).remove().draw();
                $.ajax({
                    method : "POST",
                    url    : "<?php echo base_url() ?>"+"masters/Mst_bio_rec/DelReferenceById/"+referenceId,
                    data   : {
                        referenceId : referenceId
                    },
                    success : function(data){
                        alert("Data has been deleted");
                    },
                    error   : function(data){
                        alert("Failed to delete");
                    }   
                });
            }

        });

        $('#saveQualification').click(function(){   
            // debugger
            var isDataValid = true; 
            $('#qualificationErr').hide();
            $('#qualificationYearErr').hide();     
            // debugger
            var isQualificationCertified = "Y";
            if ($('#isQualificationCertified').is(":checked")){   
                isQualificationCertified = "Y";
            }else{
                isQualificationCertified = "T";
            }
            if($('#qualification').val() == ""){
                $('#qualification').focus();    
                $('#qualificationErr').show();
                isDataValid = false;
            }                       
            else if($('#qualificationYear option:selected').text() == "Pilih"){
                $('#qualificationYear').focus();
                $('#qualificationYearErr').show();
                isDataValid = false;
            }
            if(isDataValid == false){
                return false;
            } 

            $.ajax({
                method : "POST",
                url    : "<?php echo base_url() ?>"+"masters/Mst_bio_rec/InsertQualification/", 
                data   : {
                    headerId : headerId,
                    qualification     : $('#qualification').val(),
                    qualificationYear : $('#qualificationYear').val(),
                    isCertified       : isQualificationCertified
                },
                success : function(data){
                    qualificationTable.clear().draw();
                    var dataSrc = JSON.parse(data);
                    qualificationTable.rows.add(dataSrc).draw(false);
                    alert("Data has been saved");
                    clearDetailInputs();
                    $('#referenceName').focus();
                },
                error : function(data){
                    alert("Failed to save");
                }   
            });                         
        });

        var is_valid = false;
        $('#qualificationTable tbody').on( 'click', '.btnQualificationDelete', function (){   
            rowData = qualificationTable.row( $(this).parents('tr') ).data();
            referenceId = rowData[0];  
            is_valid = confirm('Data dengan ID : '+referenceId+' akan dihapus?');
            if (is_valid) 
            {
                qualificationTable.row( $(this).parents('tr') ).remove().draw();
                $.ajax({
                    method : "POST",
                    url    : "<?php echo base_url() ?>"+"masters/Mst_bio_rec/DelQualificationById/"+referenceId,
                    data   : {
                        referenceId : referenceId
                    },
                    success : function(data){
                        alert("Data has been deleted");
                    },
                    error   : function(data){
                        alert("Failed to delete");
                    }   
                });
            }
        });

        $('#biodataListTable tbody').on( 'click', '.btnHeaderPrint', function () {
            var data = biodataListTable.row( $(this).parents('tr') ).data();
            var bioId = data[0]; 
            var myUrl = "<?php echo base_url().'masters/Mst_bio_rec/biodataExport/' ?>"+bioId;
            // alert(myUrl);

            $.ajax({
                method : "POST",
                url : myUrl,
                data : {
                    biodataId : bioId
                },
                success : function(response){
                    console.log(response);
                    window.open(myUrl,'_blank');
                },
                error : function(data){
                    alert("Failed");
                } 
            });
        });

        $('#printCandidateList').click(function(e) {
            // debugger
            var agesMax             = $("#agesMax").val();
            var searchByPosition    = $("#searchByPosition").val();
            var searchByCity        = $("#searchByCity").val();
            var searchByName        = $("#searchByName").val();
            var searchByIdNo        = $("#searchByIdNo").val();

            if(agesMax!=''){
                agesMax             = agesMax;
            }else{
                agesMax             = '-';
            }

            if(searchByPosition!=''){
                searchByPosition    = searchByPosition;
            }else{
                searchByPosition    = '-';
            }

            if(searchByCity!=''){
                searchByCity        = searchByCity;
            }else{
                searchByCity        = '-';
            }

            if(searchByName!=''){
                searchByName        = searchByName;
            }else{
                searchByName        = '-';
            }

            if(searchByIdNo!=''){
                searchByIdNo        = searchByIdNo;
            }else{
                searchByIdNo        = '-';
            }

            if(agesMax == '') {
                alert('Filter cannot be empty');
                $('#searchByPosition').focus();
                return false;
            }

            var myUrl = "<?php echo base_url() ?>"+"reports/CR_Candidate_List/printBioReports/"+agesMax+"/"+searchByPosition+"/"+searchByCity+"/"+searchByName+"/"+searchByIdNo;

            $.ajax({
                method  : "POST",
                url     : myUrl,             
                success : function(response){
                    console.log(response);
                    window.open(myUrl,'_blank');
                },
                error : function(data){
                    alert("Failed");
                }   
            }); 
        }); 

        var listBioMenu = "<?php echo $this->uri->segment(3) ?>";
        var idCardNo = "<?php echo $this->uri->segment(4) ?>";
        // debugger
        if(listBioMenu == 'clr' && idCardNo != ''){
            $('#searchByIdNo').val(idCardNo);
            $('#searchByIdNo').focus();
            $('#searchByIdNo').select();

            var agesMax             = $("#agesMax").val();
            var searchByPosition    = $("#searchByPosition").val();
            var searchByCity        = $("#searchByCity").val();
            var searchByName        = $("#searchByName").val();
            var searchByIdNo        = $("#searchByIdNo").val();
            
            // var myURL = "<?php echo base_url() ?>"+"MstBioRec/GetByIdCardNo/"+idCardNo;

            var myUrl = "";
            myUrl = "<?php echo base_url().'reports/CR_Candidate_List/getCandidateList'; ?>";
            // alert(myUrl);
            /* Ajax Is Here */
            $.ajax({
                method : "POST",
                url    : myUrl,
                data   : {
                    sAge        : agesMax,
                    sPosition   : searchByPosition,
                    sCity       : searchByCity,
                    sName       : searchByName,
                    sNo         : searchByIdNo
                },
                success : function(data){
                    biodataListTable.clear().draw();
                    var dataSrc = JSON.parse(data);                 
                    biodataListTable.rows.add(dataSrc).draw(false);
                },
                error   : function(data){
                    $.notify({
                        title: "<h5>Informasi : </h5>",
                        message: "<strong>"+data+"</strong> </br></br> ",
                        icon: '' 
                    },
                    {
                        type: "warning",
                        delay: 3000
                    }); 
                }
            });

            // $.ajax({
            //     method  : "POST",
            //     url     : myURL,
            //     data    : {
            //         idCardNo : idCardNo  
            //     },
            //     success : function(data){   
            //         educationTable.clear().draw();
            //         trainingTable.clear().draw();
            //         experienceTable.clear().draw();
            //         familyTable.clear().draw();
            //         languageTable.clear().draw();
            //         organizationTable.clear().draw();
            //         referenceTable.clear().draw();
            //         biodataListTable.clear().draw();
            //         var dataSrc = JSON.parse(data); 
            //         biodataListTable.rows.add(dataSrc).draw(false);     
            //     },
            //     error   : function(data){
            //         alert("Failed to get data");
            //     }
            // }); 
        }




















































































        /* START PRINT BUTTON CLICK */
        $('#printSummaryPayroll').on( 'click', function () {
            var ptName = $("#clientName").val();
            var yearPeriod = $("#yearPeriod").val();
            var monthPeriod = $("#monthPeriod").val();
            // var payrollGroup = $("#payrollGroup").val();

            var isValid = true;
            if($('#clientName option:selected').text() == "Pilih")
            {
                $("#clientName").focus();
                alert('PT Harus Dipilih ');
                isValid = false;
            } 
            else if($('#monthPeriod option:selected').text() == "Pilih")
            {
                $("#monthPeriod").focus();
                alert('Bulan Harus Dipilih ');
                isValid = false;
            } 
            else if($('#yearPeriod option:selected').text() == "Pilih")
            {
                $("#yearPeriod").focus();
                alert('Tahun Harus Dipilih ');
                isValid = false;
            }

            if(isValid == false)
            {return false} 
            if(ptName=='Pontil_Timika'){
                var myUrl = "<?php echo base_url() ?>"+"reports/CR_Summary_Payroll/exportSummaryPayrollPontil/"+ptName+"/"+yearPeriod+"/"+monthPeriod;
            }
            else if(ptName=='Redpath_Timika'){
                var myUrl = "<?php echo base_url() ?>"+"reports/CR_Summary_Payroll/exportSummaryPayrollRedpath/"+ptName+"/"+yearPeriod+"/"+monthPeriod;
            }
            else if(ptName=='Pontil_Sumbawa'){
                var myUrl = "<?php echo base_url() ?>"+"reports/CR_Summary_Payroll/exportSummaryPayrollPontilSwq/"+ptName+"/"+yearPeriod+"/"+monthPeriod;
            }
            $.ajax({
                method : "POST",
                url : myUrl,
                data : {
                    ptName : ptName,
                    yearPeriod : yearPeriod,
                    monthPeriod : monthPeriod 
                },
                // success : function(data){
                    // alert(data);
                success : function(response){
                    console.log(response);
                    window.open(myUrl,'_blank');
                },
                error : function(data){
                    $.notify({
                        title: "<h5>Informasi : </h5>",
                        message: "<strong>"+data+"</strong> </br></br> ",
                        icon: '' 
                    },
                    {
                        type: "warning",
                        delay: 3000
                    }); 
                }   
            });
        });
        /* END PRINT BUTTON CLICK */
        /*$('#paymentList_').on( 'click', 'tbody td', function () {
            paymentList.cell((this)).data('MEOW!').draw();
        } );*/
    });
</script>
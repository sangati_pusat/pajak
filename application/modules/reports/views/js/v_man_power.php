<style type="text/css">
.highcharts-figure, .highcharts-data-table table {
  min-width: 320px; 
  max-width: 660px;
  margin: 1em auto;
}

.highcharts-data-table table {
  font-family: Verdana, sans-serif;
  border-collapse: collapse;
  border: 1px solid #EBEBEB;
  margin: 10px auto;
  text-align: center;
  width: 100%;
  max-width: 500px;
}
.highcharts-data-table caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
}
.highcharts-data-table th {
  font-weight: 600;
    padding: 0.5em;
}
.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
    padding: 0.5em;
}
.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
    background: #f8f8f8;
}
.highcharts-data-table tr:hover {
    background: #f1f7ff;
}

</style>

<!-- <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script> -->
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>

<div class="col-md-12" id="rt">
  <div class="tile bg-info">
    <div class="tile-body">
      <div class="tile-body">
        <!-- <div id="container" style="min-width: 510px; height: 600px; max-width: 800px; margin: 0 auto"></div> -->
      </div>
      <div class="tile-body table-responsive employeeListTable">
        <div id="container" style="min-width: 510px; height: 600px; max-width: 800px; margin: 0 auto"></div>
        <table class="table table-hover table-bordered" id="contractListTable">
          <thead class="thead-dark">
            <tr role="row">
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Contract Id</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 160px;">Badge Id</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 160px;">Name</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 160px;">Client Name</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 160px;">Dept</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 160px;">Position</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 160px;">Contract No</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 130px;">Date Of Hire</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Point Of Hire</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Start</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">End</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  // $(document).ready(function(){
  //   $.ajax({
  //     url: "<?php echo base_url() ?>"+"masters/Contract/contractChart",
  //     type: 'GET',
  //     async: true,
  //     dataType: "json",
  //     success: function (data){
  //       visitorData(data);
  //     }
  //   });
  // });
  // Build the chart
  
</script>
<script type="text/javascript">
	$(document).ready(function(){
		var rowIdx = 0;
		var headerId = ''; 
		var rowEducationId = 0;
		var rowTrainingId = 0;
		var rowExperienceId = 0;
		var rowFamilyId = 0;
		var rowLanguageId = 0;
		var rowOrganizationId = 0;
		var rowReferenceId = 0;
		var rowQualificationId = 0;

		var $tabs = $('#horizontalTab');
	     $tabs.responsiveTabs({
	      active: 0,
	      rotate: false,
	      startCollapsed: 'accordion',
	      collapsible: 'accordion',
	      setHash: true,
	        // disabled: [3,4],
	      });
				

		/* START DETAIL GLOBAL */	
		function clearDetailInputs()
        {
            $('.my_detail :input').each(function(){
                $(this).val('');
            });            
        }
        /* END DETAIL GLOBAL */	

        /* START GET ALL POSITION */
        $.ajax({
        	url : "<?php echo base_url() ?>"+"masters/Mst_position/getPosition",
            method : "POST",
            data : {
                id : ""
            },
            success : function(data){
                // alert(data);
                var dataSrc = JSON.parse(data); 
                var dataLength = dataSrc.length;
                $('#appliedPosition').find('option:not(:first)').remove();
                for(var i = 0; i < dataLength; i++){
                    $('#appliedPosition').append(new Option(dataSrc[i]['position_name'], dataSrc[i]['position_name']));
                }
            },
            error : function(){
                alert('Failed');
            }
        });  
		/* END GET ALL POSITION */

        $.ajax({
            method : "POST",
            url : "<?php echo base_url() ?>"+"masters/Mst_city/getAllCity", 
            data : {
                id : "",
                val : ""
            },
            success : function(data) {
                var srcData = JSON.parse(data);
                // alert(data);
                var i;
                for (i = 0; i < srcData.length; ++i) {
                    // alert(srcData[i]);
                    $('#cityAddress').append('<option value="'+srcData[i]+'">'+srcData[i]+'</option>');
                }
            },
            error : function(data) {
                alert("Failed");
            } 
        })

		var biodataListTable = $('#biodataListTable').DataTable({
            "paging":   true,
            "ordering": true,
            "info":     true,
            "filter":     true,
            "columnDefs": [{
                "targets": -2,
                "data": null,
                "defaultContent": "<button class='btn btn-primary fa fa-pencil-square-o btnHeaderUpdate' data-toggle='modal' data-target='#editBiodataModal'><span class='glyphicon glyphicon-edit'></span></button>"
            },
            {
                "targets": -1,
                "data": null,
                "defaultContent": "<button class='btn btn-primary fa fa-print btnHeaderPrint' ><span class='glyphicon glyphicon-print'></span></button>"
            }]
        });

		var educationTable = $('#educationTable').DataTable({
            "paging":   false,
            "ordering": false,
            "info":     false,
            "filter":     false,
            "columnDefs": [{
                "targets": -1,
                "data": null,
                "defaultContent": "<button class='btn btn-danger fa fa-times btnEducationDelete btnDelTable'><span class='glyphicon glyphicon-remove'></span></button>"
            }]
        });

		var trainingTable = $('#trainingTable').DataTable({
            "paging":   false,
            "ordering": false,
            "info":     false,
            "filter":     false,
            "columnDefs": [{
                "targets": -1,
                "data": null,
                "defaultContent": "<button class='btn btn-danger fa fa-times btnTrainingDelete btnDelTable'><span class='glyphicon glyphicon-remove'></span></button>"
            }]
        });

        var experienceTable = $('#experienceTable').DataTable({
            "paging":   false,
            "ordering": false,
            "info":     false,
            "filter":     false,
            "columnDefs": [{
                "targets": -1,
                "data": null,
                "defaultContent": "<button class='btn btn-danger fa fa-times btnExperienceDelete btnDelTable'><span class='glyphicon glyphicon-remove'></span></button>"
            }]
        });

        var familyTable = $('#familyTable').DataTable({
            "paging":   false,
            "ordering": false,
            "info":     false,
            "filter":     false,
            "columnDefs": [{
                "targets": -1,
                "data": null,
                "defaultContent": "<button class='btn btn-danger fa fa-times btnFamilyDelete btnDelTable'><span class='glyphicon glyphicon-remove'></span></button>"
            }]
        });

        var languageTable = $('#languageTable').DataTable({
            "paging":   false,
            "ordering": false,
            "info":     false,
            "filter":     false,
            "columnDefs": [{
                "targets": -1,
                "data": null,
                "defaultContent": "<button class='btn btn-danger fa fa-times btnLanguageDelete btnDelTable'><span class='glyphicon glyphicon-remove'></span></button>"
            }]
        });

        var organizationTable = $('#organizationTable').DataTable({
            "paging":   false,
            "ordering": false,
            "info":     false,
            "filter":     false,
            "columnDefs": [{
                "targets": -1,
                "data": null,
                "defaultContent": "<button class='btn btn-danger fa fa-times btnOrganizationDelete btnDelTable'><span class='glyphicon glyphicon-remove'></span></button>"
            }]
        });

        var referenceTable = $('#referenceTable').DataTable({
            "paging":   false,
            "ordering": false,
            "info":     false,
            "filter":     false,
            "columnDefs": [{
                "targets": -1,
                "data": null,
                "defaultContent": "<button class='btn btn-danger fa fa-times btnReferenceDelete btnDelTable'><span class='glyphicon glyphicon-remove'></span></button>"
            }]
        });	

        var qualificationTable = $('#qualificationTable').DataTable({
            "paging":   false,
            "ordering": false,
            "info":     false,
            "filter":     false,
            "columnDefs": [{
                "targets": -1,
                "data": null,
                "defaultContent": "<button class='btn btn-danger fa fa-times btnQualificationDelete btnDelTable'><span class='glyphicon glyphicon-remove'></span></button>"
            }]
        });	

		var isActive = 1;
    	if ($('#isCandidate').is(":checked"))
    	{	
    		isActive = 0;
    	}     
		$('#isCandidate').on('change', function(){	    	
	    	isActive = 1;
	    	if ($('#isCandidate').is(":checked"))
	    	{	
	    		isActive = 0;
	    	}
		});

		var isStaff = 0;
    	if ($('#isStaff').is(":checked"))
    	{	
    		isStaff = 1;
    	}     
		$('#isStaff').on('change', function(){	    	
	    	isStaff = 0;
	    	if ($('#isStaff').is(":checked"))
	    	{	
	    		isStaff = 1;
	    	}
		});  

		$('#searchByName').focus(function(){
			$('#searchByPosition').val('');
			$('#searchByCity').val('');
		});	
	    // debugger
		$('#searchByPosition').keyup(function(e){
			// debugger
			if(e.keyCode == 13)
			{
				var searchByPosition = $('#searchByPosition').val();
				var agesMax = $('#agesMax').val();
				var searchByCity  = $('#searchByCity').val();
				var dataSrc;

				if(searchByPosition == '' && searchByCity == '') {
					alert('Filter cannot be empty');
					$('#searchByPosition').focus();
					return false;
				}
				
				if(searchByPosition == '') {
				   searchByPosition = "-";
				}

				if(searchByCity == '') {
				   searchByCity = "-";
				}

				var myURL = "<?php echo base_url() ?>"+"masters/Mst_bio_rec/GetByPosition/"+agesMax+"/"+searchByPosition+"/"+searchByCity;
				/* Do Something Here */	
				$.ajax({
					method 	: "POST",
					url		: myURL,
					data 	: {
						positionName : searchByPosition,
						agesMax : agesMax,
						isActive : isActive,
						isStaff : isStaff  
					},
					error	: function(data){
						alert("Load Data Gagal");
					},
					success	: function(data){	
						// alert(data);
						educationTable.clear().draw();
						trainingTable.clear().draw();
						experienceTable.clear().draw();
						familyTable.clear().draw();
						languageTable.clear().draw();
						organizationTable.clear().draw();
						referenceTable.clear().draw();
						qualificationTable.clear().draw();
						biodataListTable.clear().draw();
						var dataSrc = JSON.parse(data);	
						biodataListTable.rows.add(dataSrc).draw(false);		
					}
				});				
			}			
		});
		
		$('#searchByCity').keyup(function(e){
			if(e.keyCode == 13)
			{
				var searchByPosition = $('#searchByPosition').val();
				var agesMax 		 = $('#agesMax').val();
				var searchByCity     = $('#searchByCity').val();
				var dataSrc;

				if(searchByPosition == '' && searchByCity == '') {
					alert('Filter cannot be empty');
					$('#searchByCity').focus();
					return false;
				}
				
				if(searchByPosition == '') {
				   searchByPosition = "-";
				}

				if(searchByCity == '') {
				   searchByCity = "-";
				}

				var myURL = "<?php echo base_url() ?>"+"masters/Mst_bio_rec/GetByCity/"+agesMax+"/"+searchByPosition+"/"+searchByCity;
				/* Do Something Here */	
				$.ajax({
					method 	: "POST",
					url		: myURL,
					data 	: {
						cityName : searchByCity,
						isActive : isActive,
						isStaff : isStaff  
					},
					error	: function(data){
						alert("Load Data Gagal");
					},
					success	: function(data){						
						educationTable.clear().draw();
						trainingTable.clear().draw();
						experienceTable.clear().draw();
						familyTable.clear().draw();
						languageTable.clear().draw();
						organizationTable.clear().draw();
						referenceTable.clear().draw();
						qualificationTable.clear().draw();
						biodataListTable.clear().draw();
						var dataSrc = JSON.parse(data);	
						biodataListTable.rows.add(dataSrc).draw(false);		
					}
				});		
			}
		});

		$('#searchByName').keyup(function(e){
			if(e.keyCode == 13)
			{
			// debugger
				var searchByPosition = $('#searchByPosition').val();
				var agesMax = $('#agesMax').val();
				var searchByCity  = $('#searchByCity').val();
				var searchByName  = $('#searchByName').val();
				var dataSrc;

				if(searchByPosition == '' && searchByName == '') {
					alert('Filter cannot be empty');
					$('#searchByName').focus();
					return false;
				}
				
				if(searchByPosition == '') {
				   searchByPosition = "-";
				}

				if(searchByCity == '') {
				   searchByCity = "-";
				}

				if(searchByName == '') {
				   searchByName = "-";
				}

				var myURL = "<?php echo base_url() ?>"+"masters/Mst_bio_rec/GetListBiodataByName/"+agesMax+"/"+searchByName+"/"+searchByPosition+"/"+searchByCity;
				/* Do Something Here */	
				$.ajax({
					method 	: "POST",
					url		: myURL,
					data 	: {
						candidateName : searchByName,
						isActive : isActive,
						isStaff : isStaff  
					},
					error	: function(data){
						alert("Load Data Gagal");
					},
					success	: function(data){						
						educationTable.clear().draw();
						trainingTable.clear().draw();
						experienceTable.clear().draw();
						familyTable.clear().draw();
						languageTable.clear().draw();
						organizationTable.clear().draw();
						referenceTable.clear().draw();
						qualificationTable.clear().draw();
						biodataListTable.clear().draw();
						var dataSrc = JSON.parse(data);	
						biodataListTable.rows.add(dataSrc).draw(false);		
					}
				});		
			}
		});


		 $('#biodataListTable tbody').on( 'click', '.btnHeaderUpdate', function () {
		 	var data = biodataListTable.row( $(this).parents('tr') ).data();
	        var bioId = data[0]; 
	        var tDay; 
	        var tMonth; 
	        var tYear; 
	        var tmpDate;
	        var myURL = "<?php echo base_url().'masters/Mst_bio_rec/GetBiodataJasonById/' ?>"+bioId;
	        $.ajax({
	        	method: "POST",
	        	url : myURL,
	        	data: {
	        		bioId : bioId
	        	},
	        	error : function(data){
	        		alert("Load Data Gagal");
	        	},
	        	success : function(data){
	        		var dataSrc = JSON.parse(data);

	        		var isActive = dataSrc[30];
	        		if(isActive == '1'){
	        			alert('You have no access for editing active data');
	        			$('#editBiodataModal').modal('hide');
	        			$('.btnDelTable').attr('disabled', true);	        			
	        		}
	        		else
	        		{
	        			$('.btnDelTable').attr('disabled', true);  
	        			  			
		        		$("#biodataId").val(dataSrc[0]);
		        		$("#fullName").val(dataSrc[3]); 
				        $("#gender").val(dataSrc[4]); 
				        $("#birthPlace").val(dataSrc[5]); 

				        tmpDate = dataSrc[6];
				        $("#birthDay").val(tmpDate); 
				        $("#pointOfHire").val(dataSrc[8]);
				        $("#nationality").val(dataSrc[9]);
				        $("#etnics").val(dataSrc[10]);
				        $("#idNo").val(dataSrc[11]);
				        $("#npwpNo").val(dataSrc[12]);
				        $("#bpjsNo").val(dataSrc[13]);
				        $("#idAddress").val(dataSrc[14]);
				        $("#currentAddress").val(dataSrc[15]);
				        
				        $("#cityAddress").val(dataSrc[16]);

				        $("#livingStatus").val(dataSrc[17]);
				        $("#religion").val(dataSrc[18]);
				        $("#driveLicence").val(dataSrc[19]);
				        $("#maritalStatus").val(dataSrc[20]);

				        var appPosition = dataSrc[21];

				        alert(appPosition);
				        // $("#appliedPosition option[value=" + appPosition + "]").attr('selected', 'selected');
				        $('#appliedPosition option:contains("' + appPosition + '")').attr('selected', 'selected');

				        
				        $("#cvDate").val(dataSrc[22]);
				        
				        $("#height").val(dataSrc[23]);
				        $("#weight").val(dataSrc[24]);
				        $("#colorBlindness").val(dataSrc[25]);
				        $("#bloodType").val(dataSrc[26]);
				        $("#emailAddress").val(dataSrc[27]);
				        $("#telpNo").val(dataSrc[28]);
				        $("#cellNo").val(dataSrc[29]);
				        // $("#idAddress").val(dataSrc[13]);
				        // $("#idAddress").val(dataSrc[13]);
				        // $("#idAddress").val(dataSrc[13]);
	        		}

	        	}
	        });
        });

		$('#biodataListTable tbody').on( 'click', '.btnHeaderPrint', function () {
			// debugger
			var data = biodataListTable.row( $(this).parents('tr') ).data();
	        var bioId = data[0]; 
	        var myUrl = "<?php echo base_url().'masters/Mst_bio_rec/biodataExport/' ?>"+bioId;
	        // alert(myUrl);

	        $.ajax({
	        	method : "POST",
	        	url : myUrl,
	        	data : {
	        		biodataId : bioId
	        	},
	        	success : function(response){
                    console.log(response);
                    window.open(myUrl,'_blank');
	        	},
	        	error : function(data){
	        		alert("Failed");
	        	} 
	        });
		}); 

		$('#saveBiodata').on('click', function(){
			/* Start Header Form Validation */
            $('#fullNameErr').hide();
			$('#genderErr').hide();
			$('#birthPlaceErr').hide();
			$('#birthDayErr').hide();
			// $('#birthMonthErr').hide();
			// $('#birthYearErr').hide();
			$('#nationalityErr').hide();
			$('#etnicsErr').hide();
			$('#idNoErr').hide();
			$('#idAddressErr').hide();
			$('#currentAddressErr').hide();
			$('#livingStatusErr').hide();
			$('#religionErr').hide();
			// $('#driveLicenceErr').hide();
			$('#maritalStatusErr').hide();
			$('#appliedPositionErr').hide();
			$('#groupPositionErr').hide();
			$('#heightErr').hide();
			$('#weightErr').hide();
			$('#colorBlindnessErr').hide();
			$('#bloodTypeErr').hide();
			$('#emailAddressErr').hide();
			$('#telpNoErr').hide();
            $('#cellNoErr').hide();
			$('#pointOfHireErr').hide();

			var isHeaderValid = true;
			if ($('#fullName').val() == "") 
			{
				$('#fullName').focus(); 
				$('#fullNameErr').show();
				isHeaderValid = false;
			}
            else if ($('#gender option:selected').text() == "Pilih") 
            {
                $('#gender').focus(); 
                $('#genderErr').show();
                isHeaderValid = false;
            }
			else if ($('#birthPlace').val() == "") 
			{
				$('#birthPlace').focus(); 
				$('#birthPlaceErr').show();
				isHeaderValid = false;
			}
			else if ($('#birthDay').val() == "") 
			{
				$('#birthDay').focus(); 
				$('#birthDayErr').show();
				isHeaderValid = false;
			}
			else if ($('#nationality').val() == "") 
			{
				$('#nationality').focus(); 
				$('#nationalityErr').show();
				isHeaderValid = false;
			}
			else if ($('#etnics option:selected').text() == "Pilih") 
			{
				$('#etnics').focus(); 
				$('#etnicsErr').show();
				isHeaderValid = false;
			}
			else if ($('#idNo').val() == "") 
			{
				$('#idNo').focus(); 
				$('#idNoErr').show();
				isHeaderValid = false;
			}
			else if ($('#idAddress').val() == "") 
			{
				$('#idAddress').focus(); 
				$('#idAddressErr').show();
				isHeaderValid = false;
			}
			else if ($('#currentAddress').val() == "") 
			{
				$('#currentAddress').focus(); 
				$('#currentAddressErr').show();
				isHeaderValid = false;
			}
			else if ($('#livingStatus option:selected').text() == "Pilih") 
			{
				$('#livingStatus').focus(); 
				$('#livingStatusErr').show();
				isHeaderValid = false;
			}
			else if ($('#religion option:selected').text() == "Pilih") 
			{
				$('#religion').focus(); 
				$('#religionErr').show();
				isHeaderValid = false;
			}
			
			else if ($('#maritalStatus option:selected').text() == "Pilih") 
			{
				$('#maritalStatus').focus(); 
				$('#maritalStatusErr').show();
				isHeaderValid = false;
			}
			else if ($('#groupPosition option:selected').text() == "Pilih") 
			{
				$('#groupPosition').focus(); 
				$('#groupPositionErr').show();
				isHeaderValid = false;
			}
			else if ($('#appliedPosition option:selected').text() == "Pilih") 
			{
				$('#appliedPosition').focus(); 
				$('#appliedPositionErr').show();
				isHeaderValid = false;
			}
			else if ($('#height').val() == "") 
			{
				$('#height').focus(); 
				$('#heightErr').show();
				isHeaderValid = false;
			}
			else if ($('#weight').val() == "") 
			{
				$('#weight').focus(); 
				$('#weightErr').show();
				isHeaderValid = false;
			}
			else if ($('#colorBlindness option:selected').text() == "Pilih") 
			{
				$('#colorBlindness').focus(); 
				$('#colorBlindnessErr').show();
				isHeaderValid = false;
			}
			else if ($('#bloodType option:selected').text() == "Pilih") 
			{
				$('#bloodType').focus(); 
				$('#bloodTypeErr').show();
				isHeaderValid = false;
			}
			else if ($('#emailAddress').val() == "") 
			{
				$('#emailAddress').focus(); 
				$('#emailAddressErr').show();
				isHeaderValid = false;
			}
			else if ($('#telpNo').val() == "") 
			{
				$('#telpNo').focus(); 
				$('#telpNoErr').show();
				isHeaderValid = false;
			}
			else if ($('#cellNo').val() == "") 
            {
                $('#cellNo').focus(); 
                $('#cellNoErr').show();
                isHeaderValid = false;
            }
            else if ($('#pointOfHire').val() == "") 
			{
				$('#pointOfHire').focus(); 
				$('#pointOfHireErr').show();
				isHeaderValid = false;
			}

			if(isHeaderValid == false)
			{
				return false;
			}

            biodataId 		= $('#biodataId').val();
            fullName 		= $('#fullName').val();
			gender 			= $('#gender').val();
			birthPlace 		= $('#birthPlace').val();
			birthDate 		= $('#birthDay').val();
			nationality 	= $('#nationality').val(); 	
			etnics 			= $('#etnics').val(); 	
            idNo 			= $('#idNo').val();    
			npwpNo 			= $('#npwpNo').val(); 	
			bpjsNo 			= $('#bpjsNo').val(); 	
			idAddress 		= $('#idAddress').val(); 	
			currentAddress 	= $('#currentAddress').val(); 	
			cityAddress 	= $('#cityAddress').val(); 	
			livingStatus 	= $('#livingStatus').val(); 	
			religion 		= $('#religion').val(); 
			driveLicence 	= $('#driveLicence').val(); 	
			maritalStatus 	= $('#maritalStatus').val();
			appliedPosition = $('#appliedPosition').val(); 	
			cvDate 			= $('#cvDate').val(); 	
			height 			= $('#height').val(); 	
			weight 			= $('#weight').val(); 	
			colorBlindness 	= $('#colorBlindness').val(); 	
			bloodType 		= $('#bloodType').val(); 	
			emailAddress 	= $('#emailAddress').val(); 	
			telpNo 			= $('#telpNo').val(); 	
            cellNo 			= $('#cellNo').val();
			pointOfHire 	= $('#pointOfHire').val();

			$.ajax({                
                method : "POST", 
                url : "<?php echo base_url() ?>"+"masters/Mst_bio_rec/Upd", 
                data : {
                	updMstBioRec 	: "1",
                   	biodataId 		: biodataId,
	                fullName 		: fullName,
					gender 			: gender,
					birthPlace 		: birthPlace,
					birthDate 		: birthDate,
					nationality 	: nationality, 	
					etnics 			: etnics, 	
	                idNo 			: idNo,    
					npwpNo 			: npwpNo, 	
					bpjsNo 			: bpjsNo,
					idAddress 		: idAddress, 	
					currentAddress 	: currentAddress, 	
					cityAddress 	: cityAddress, 	
					livingStatus 	: livingStatus, 	
					religion 		: religion, 
					driveLicence 	: driveLicence, 	
					maritalStatus 	: maritalStatus,
					appliedPosition : appliedPosition, 	
					cvDate 			: cvDate, 
					height 			: height, 	
					weight 			: weight, 	
					colorBlindness 	: colorBlindness, 	
					bloodType 		: bloodType, 	
					emailAddress 	: emailAddress, 	
					telpNo 			: telpNo, 	
	                cellNo 			: cellNo,
					pointOfHire 	: pointOfHire                       			 	                     
                }, 
                error 	: function(data) {
                	alert("Failed to save");       
                },
                success : function(data) {
                    // $("#saveMsg").html("ID : <div id='succeedId'><h3>"+data+"</h3></div> Berhasil Disimpan");    
                    biodataListTable.row(rowIdx).data([
                        biodataId,fullName,idAddress,cityAddress,cellNo,appliedPosition,cvDate]);   
                	alert("Data has been saved");  
                	// alert(data);  
                	clearDetailInputs();
                	$("#gender").focus();     
                }
            });	
		});        
		/* End HEADER */	

        $(".btnDetail").attr('disabled',true);  
		/* Start Select HEADER */
        $('#biodataListTable tbody').on( 'click', 'tr', function () {
            var rowData = biodataListTable.row( this ).data();
            if ( $(this).hasClass('selected') ) 
            {
                $(this).removeClass('selected');
            }
            else 
            {
                biodataListTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');                    
            }
            headerId = rowData[0];   
        	$(".btnDetail").attr('disabled',false);  
            
            rowIdx = biodataListTable.row( this ).index();
            $("#rowIdx").val(rowIdx); 

            /*  Start Load Details  */

            /* Start Load HEADER */
        	/*$.ajax({
				method 	: "POST",
				url		: "<?php #echo base_url().'Mst_bio_rec/GetEducationByBioRecId/' ?>"+headerId,
				data 	: {
					headerId : headerId 
				},
				error	: function(data){
					alert(data);
				},
				success	: function(data){
					educationTable.clear().draw();
					var dataSrc = JSON.parse(data);	
					educationTable.rows.add(dataSrc).draw(false);											
				}
			});*/
			/* End Select HEADER */

			/* Start Load PENDIDIKAN */
        	$.ajax({
				method 	: "POST",
				url		: "<?php echo base_url().'masters/Mst_bio_rec/GetEducationByBioRecId/' ?>"+headerId,
				data 	: {
					headerId : headerId 
				},
				error	: function(data){
					alert(data);
				},
				success	: function(data){
					educationTable.clear().draw();
					var dataSrc = JSON.parse(data);	
					educationTable.rows.add(dataSrc).draw(false);											
				}
			});	
			/* End Load PENDIDIKAN */

			/* Start Load TRAINING */
        	$.ajax({
				method 	: "POST",
				url		: "<?php echo base_url().'masters/Mst_bio_rec/GetTrainingByBioRecId/' ?>"+headerId,
				data 	: {
					headerId : headerId 
				},
				error	: function(data){
					alert(data);
				},
				success	: function(data){
					trainingTable.clear().draw();
					var dataSrc = JSON.parse(data);					
					trainingTable.rows.add(dataSrc).draw(false);											
				}
			});	
			/* End Load TRAINING */

			/* Start Load EXPERIENCE */
        	$.ajax({
				method 	: "POST",
				url		: "<?php echo base_url().'masters/Mst_bio_rec/GetExperienceByBioRecId/' ?>"+headerId,
				data 	: {
					headerId : headerId 
				},
				error	: function(data){
					alert(data);
				},
				success	: function(data){
					experienceTable.clear().draw();
					var dataSrc = JSON.parse(data);					
					experienceTable.rows.add(dataSrc).draw(false);											
				}
			});	

			/* Start Load FAMILY */
        	$.ajax({
				method 	: "POST",
				url		: "<?php echo base_url().'masters/Mst_bio_rec/GetFamilyByBioRecId/' ?>"+headerId,
				data 	: {
					headerId : headerId 
				},
				error	: function(data){
					alert(data);
				},
				success	: function(data){
					familyTable.clear().draw();
					var dataSrc = JSON.parse(data);					
					familyTable.rows.add(dataSrc).draw(false);											
				}
			});	
			/* Start Load FAMILY */	

			/* Start Load LANGUAGE */
        	$.ajax({
				method 	: "POST",
				url		: "<?php echo base_url().'masters/Mst_bio_rec/GetLanguageByBioRecId/' ?>"+headerId,
				data 	: {
					headerId : headerId 
				},
				error	: function(data){
					alert(data);
				},
				success	: function(data){
					languageTable.clear().draw();
					var dataSrc = JSON.parse(data);					
					languageTable.rows.add(dataSrc).draw(false);											
				}
			});	
			/* Start Load LANGUAGE */

			/* Start Load ORGANIZATION */
        	$.ajax({
				method 	: "POST",
				url		: "<?php echo base_url().'masters/Mst_bio_rec/GetOrganizationByBioRecId/' ?>"+headerId,
				data 	: {
					headerId : headerId 
				},
				error	: function(data){
					alert(data);
				},
				success	: function(data){
					organizationTable.clear().draw();
					var dataSrc = JSON.parse(data);					
					organizationTable.rows.add(dataSrc).draw(false);											
				}
			});	
			/* Start Load ORGANIZATION */

			/* Start Load REFERENCE */
        	$.ajax({
				method 	: "POST",
				url		: "<?php echo base_url().'masters/Mst_bio_rec/GetReferencesByBioRecId/' ?>"+headerId,
				data 	: {
					headerId : headerId 
				},
				error	: function(data){
					alert(data);
				},
				success	: function(data){
					referenceTable.clear().draw();
					var dataSrc = JSON.parse(data);					
					referenceTable.rows.add(dataSrc).draw(false);											
				}
			});	
			/* Start Load REFERENCE */	

			/* Start Load Qualification */
        	$.ajax({
				method 	: "POST",
				url		: "<?php echo base_url().'masters/Mst_bio_rec/GetQualificationByBioRecId/' ?>"+headerId,
				data 	: {
					headerId : headerId 
				},
				error	: function(data){
					alert(data);
				},
				success	: function(data){
					qualificationTable.clear().draw();
					var dataSrc = JSON.parse(data);					
					qualificationTable.rows.add(dataSrc).draw(false);											
				}
			});	
			/* end Load Qualification */	
            /*  End Load Details  */      
         }); 
        /* End Select PENDIDIKAN */

        /* START PENDIDIKAN */        
        /* Start Select PENDIDIKAN */
        $('#educationTable tbody').on( 'click', 'tr', function () {
            var rowData = educationTable.row( this ).data();
            if ( $(this).hasClass('selected') ) 
            {
                $(this).removeClass('selected');
            }
            else 
            {
                educationTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');                    
            }
            rowEducationId = rowData[0];
        });
        /* End Select PENDIDIKAN */

        /* Start Save PENDIDIKAN Table */
       	$('#schoolNameErr').hide();
	    $('#educationMajorErr').hide();
	    $('#educationYearErr').hide();
	    $('#educationCityErr').hide();
        $('#saveEducation').click(function() 
        {
        	/* Start PENDIDIKAN Form Validation */
			
			/* End PENDIDIKAN Form Validation */
        	var isCertified = "Y";
        	if ($('#isCertified').is(":checked"))
        	{	
        		isCertified = "Y";
        	}              
        	else
        	{
        		isCertified = "T";
        	}

        	/* Start EDUCATION Form Validation */
        	var isDataValid = true;
        	$('#schoolNameErr').hide();
		    $('#educationMajorErr').hide();
		    $('#educationYearErr').hide();
		    $('#educationCityErr').hide();
		    
		    if($('#schoolName').val() == "") 
		    {
		    	$('#schoolName').focus();
		    	$('#schoolNameErr').show();
		    	isDataValid = false;
		    }
		    else if($('#educationMajor').val() == "") 
		    {
		    	$('#educationMajor').focus();
		    	$('#educationMajorErr').show();
		    	isDataValid = false;
		    }
		    else if($('#educationYear option:selected').text() == "Pilih") 
		    {
		    	$('#educationYear').focus();
		    	$('#educationYearErr').show();
		    	isDataValid = false;
		    }
		    else if($('#educationCity').val() == "") 
		    {
		    	$('#educationCity').focus();
		    	$('#educationCityErr').show();
		    	isDataValid = false;
		    }

		    if(isDataValid == false)
		    {
		    	return false;
		    }

		    /* End EDUCATION Form Validation */

		    $.ajax({
		    	method : "POST",
		    	url		: "<?php echo base_url().'masters/Mst_bio_rec/InsEducation/' ?>",
		    	data : {
		    		headerId 	   : headerId,	
		    		schoolName     : $('#schoolName').val(), 
		    		educationMajor : $('#educationMajor').val(),
		            educationYear  : $('#educationYear').val(),
		            educationCity  : $('#educationCity').val(),
		            isCertified    : isCertified
		    	},
		    	success : function(data){
		    		educationTable.clear().draw();
					var dataSrc = JSON.parse(data);	
					educationTable.rows.add(dataSrc).draw(false);					
		 
		    		alert("Data has been saved");
		    		clearDetailInputs();
		            $('#schoolName').focus();
		    	},
		    	error : function(data){
		    		alert("Data failed to save");
		    	}
		    });                                          
        });
        /* End Save PENDIDIKAN Table */
       
            
        /* Start DELETE Education */
        var isDataValid = false;
        $('#educationTable tbody').on('click', '.btnEducationDelete', function(){
        	rowData = educationTable.row( $(this).parents('tr') ).data();
            educationId = rowData[0];  
            is_valid = confirm('Data : '+educationId+' will deleted?');
            if (is_valid) 
            {
                educationTable.row( $(this).parents('tr') ).remove().draw();
            	$.ajax({
            		method : "POST",
            		url    : "<?php echo base_url() ?>"+"masters/Mst_bio_rec/DelEducationById/"+educationId,
            		data   : {
            			educationId : educationId
            		},
            		success : function(data){
            			alert("Data has been deleted");
            		},
            		error   : function(data){
            			alert("Data failed to delete");
            		}   
            	});
            }
        });
        /* End DELETE Education */
        /* END PENDIDIKAN */

        /* START TRAINING */  
        /* Start Save TRAINING Table */
        $('#instituteNameErr').hide();
        $('#classesFieldErr').hide();
        $('#trainingYearErr').hide();
        $('#trainingCityErr').hide();
        $('#saveTraining').click(function() 
        {        	
        	var isTrainingCertified = "Y";
        	if ($('#isTrainingCertified').is(":checked"))
        	{	
        		isTrainingCertified = "Y";
        	}              
        	else
        	{
        		isTrainingCertified = "T";
        	}

        	/* Start TRAINING Form Validation */
        	$('#instituteNameErr').hide();
	        $('#classesFieldErr').hide();
	        $('#trainingYearErr').hide();
	        $('#trainingCityErr').hide();
	        var isDataValid = true;
	        if($('#instituteName').val() == "") 
	        {
	        	$('#instituteName').focus();	
	        	$('#instituteNameErr').show();
	        	isDataValid = false;	
	        }
	        else if($('#classesField').val() == "") 
	        {
	        	$('#classesField').focus();	
	        	$('#classesFieldErr').show();
	        	isDataValid = false;		
	        }
	        else if($('#trainingYear option:selected').text() == "Pilih") 
	        {
	        	$('#trainingYear').focus();	
	        	$('#trainingYearErr').show();
	        	isDataValid = false;		
	        }
	        else if($('#trainingCity').val() == "")
	        {
	        	$('#trainingCity').focus();	
	        	$('#trainingCityErr').show();
	        	isDataValid = false;			
	        }

	        if(isDataValid == false)
	        {
	        	return false;
	        }

	        /* End TRAINING Form Validation */

	        $.ajax({
	        	method : "POST",
	        	url		: "<?php echo base_url().'masters/Mst_bio_rec/InsTraining/' ?>",
	        	data   : {
	        		headerId 	   : headerId,	
		    		instituteName  : $('#instituteName').val(), 
		    		classesField   : $('#classesField').val(),
		            trainingYear   : $('#trainingYear').val(),
		            trainingCity   : $('#trainingCity').val(),
		            isCertified    : isTrainingCertified
	        	},
	        	success : function(data){
	        		trainingTable.clear().draw();
					var dataSrc = JSON.parse(data);	
					trainingTable.rows.add(dataSrc).draw(false);
	        		alert("Data has been saved");
	        		clearDetailInputs();
	        		$('#instituteName').focus();
	        	},
	        	error : function(data){
	        		alert("Data failed to save");
	        	}
	        });
	    });
        /* End Save TRAINING Table */

        /* Start Select TRAINING */
        $('#trainingTable tbody').on( 'click', 'tr', function () {
            var rowData = trainingTable.row( this ).data();
            if ( $(this).hasClass('selected') ) 
            {
                $(this).removeClass('selected');
            }
            else 
            {
                trainingTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');                    
            }
            rowTrainingId = rowData[0];
        });
        /* End Select TRAINING */

        /* Start DELETE Education */
        var isDataValid = false;
        $('#trainingTable tbody').on('click', '.btnTrainingDelete', function(){
        	rowData = trainingTable.row( $(this).parents('tr') ).data();
            trainingId = rowData[0];  
            is_valid = confirm('Data : '+trainingId+' will deleted?');
            if (is_valid) 
            {
                trainingTable.row( $(this).parents('tr') ).remove().draw();
            	$.ajax({
            		method : "POST",
            		url    : "<?php echo base_url() ?>"+"masters/Mst_bio_rec/DelTrainingById/"+trainingId,
            		data   : {
            			trainingId : trainingId
            		},
            		success : function(data){
            			alert("Data has been deleted");
            		},
            		error   : function(data){
            			alert("Data failed to delete");
            		}   
            	});
            }
        });
        /* End DELETE Education */
        /* END TRAINING */

        /* START EXPERIENCE */
        /* Start Select EXPERIENCE */
        $('#experienceTable tbody').on( 'click', 'tr', function () {
            var rowData = experienceTable.row( this ).data();
            if ( $(this).hasClass('selected') ) 
            {
                $(this).removeClass('selected');
            }
            else 
            {
                experienceTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');                    
            }
            rowTrainingId = rowData[0];
        });
        /* End Select EXPERIENCE */

        /* Start Clear end_of_work */
        $("#endWorkDate").attr('disabled',true);     
        $("#resignReason").attr('disabled',true);    
        $('#isCurrentWork').on('change', function()
        {
            if ($('#isCurrentWork').is(":checked"))
            {
            	$('#endWorkDate').val("");
                $("#endWorkDate").attr('disabled',true);
                $("#resignReason").attr('disabled',true);    
            }
            else
            {
                /* Enable Select End Work */
                $("#endWorkDate").attr('disabled',false);       
                $("#resignReason").attr('disabled',false);       
            }
            
        });
        /* End Clear end_of_work */

        /* Start Save EXPERIENCE Table */
        $('#experienceCompanyNameErr').hide();
        $('#experiencePositionErr').hide();
        $('#startWorkDateErr').hide();
        $('#experienceYearErr').hide();
        $('#experienceBasicSalaryErr').hide();
        $('#jobDescErr').hide();
        $('#resignReasonErr').hide();
        $('#saveExperience').click(function() 
        {
        	/* Start EXPERIENCE Form Validation */
        	$('#experienceCompanyNameErr').hide();
	        $('#experiencePositionErr').hide();
            $('#startWorkDateErr').hide();
	        $('#experienceYearErr').hide();
	        $('#experienceBasicSalaryErr').hide();
	        $('#jobDescErr').hide();
	        $('#resignReasonErr').hide();
	        /* End EXPERIENCE Form Validation */


	        var isDataValid = true;        
        	if($('#experienceCompanyName').val() == "")
        	{
        		$('#experienceCompanyName').focus();
        		$('#experienceCompanyNameErr').show();
        		isDataValid = false;
        	}
        	else if($('#experiencePosition').val() == "") 
        	{
        		$('#experiencePosition').focus();
        		$('#experiencePositionErr').show();
        		isDataValid = false;
        	}
        	else if($('#startWorkDate').val() == "") 
        	{
        		$('#startWorkDate').focus();
        		$('#startWorkDateErr').show();
        		isDataValid = false;
        	}
            else if($('#experienceYear option:selected').text() == "Pilih") 
            {
                $('#experienceYear').focus();
                $('#experienceYearErr').show();
                isDataValid = false;
            }

        	else if($('#experienceBasicSalary').val() == "") 
        	{
        		$('#experienceBasicSalary').focus();
        		$('#experienceBasicSalaryErr').show();
        		isDataValid = false;
        	}

        	else if($('#jobDesc').val() == "") 
        	{
        		$('#jobDesc').focus();
        		$('#jobDescErr').show();
        		isDataValid = false;
        	}

        	if(isDataValid == false)
        	{
        		return false;
        	}
        	/* Start EXPERIENCE Form Validation */

            var startWork = $('#startWorkDate').val();
            var endWork = $('#endWorkDate').val();
            if ($('#endWorkDate').val() == "")
            {
                endWork = "";
            }

            var isCurrentWork = "Y";
            if ($('#isCurrentWork').is(":checked"))
            {   
                isCurrentWork = "Y";
            }              
            else
            {
                isCurrentWork = "T";
            } 

            $.ajax({
	        	method : "POST",
	        	url : "<?php echo base_url() ?>"+"masters/Mst_bio_rec/InsertExperience/", 
	        	data   : {
	        		headerId 	          : headerId,	
		    		experienceCompanyName : $('#experienceCompanyName').val(),
		    		experiencePosition    : $('#experiencePosition').val(),
		            startWork   		  : startWork,
		            isCurrentWork   	  : isCurrentWork,
		            endWork   	          : endWork,
		            experienceBasicSalary : $('#experienceBasicSalary').val(),
		            jobDesc 		      : $('#jobDesc').val(),
		            resignReason 		  : $('#resignReason').val(),
                	addInfo 		      : $('#addInfo').val()
	        	},
	        	success : function(data){
	        		// alert(data);
	        		experienceTable.clear().draw();
					var dataSrc = JSON.parse(data);	
					experienceTable.rows.add(dataSrc).draw(false);
	        		alert("Data has been saved ");
	        		clearDetailInputs();
	        		$('#experienceCompanyName').focus();
	        	},
	        	error : function(data){
	        		alert("Failed to save");
	        	}
	        });          
        });
        /* End Save EXPERIENCE Table */

        /* Start DELETE Experience */
        var isDataValid = false;
        $('#experienceTable tbody').on('click', '.btnExperienceDelete', function(){
        	rowData = experienceTable.row( $(this).parents('tr') ).data();
            experienceId = rowData[0];  
            is_valid = confirm('Data : '+experienceId+' will deleteted?');
            if (is_valid) 
            {
                experienceTable.row( $(this).parents('tr') ).remove().draw();
            	$.ajax({
            		method : "POST",
            		url    : "<?php echo base_url() ?>"+"masters/Mst_bio_rec/DelExperienceById/"+experienceId,
            		data   : {
            			experienceId : experienceId
            		},
            		success : function(data){
            			alert("Data has been deleted");
            		},
            		error   : function(data){
            			alert("Failed to delete");
            		}   
            	});
            }
        });
        /* End DELETE Experience */
        /* END EXPERIENCE */

        /* START LANGUAGES */
        /* Start Save LANGUAGE Table */
        $('#languageNameErr').hide();
        $('#writtenErr').hide();
        $('#oralErr').hide();

        $('#saveLanguage').click(function() 
        {
        	/* Start LANGUAGE Form Validation */
        	var isDataValid = true;
        	$('#languageNameErr').hide();
	        $('#writtenErr').hide();
	        $('#oralErr').hide();
	        if($('#languageName option:selected').text() == "Pilih")
	        {
	        	$('#languageName').focus();
	        	$('#languageNameErr').show();
	        	isDataValid = false;
	        }
	        else if($('#written option:selected').text() == "Pilih")
	        {
	        	$('#written').focus();
	        	$('#writtenErr').show();
	        	isDataValid = false;	
	        }
	        else if($('#oral option:selected').text() == "Pilih")
	        {
	        	$('#oral').focus();
	        	$('#oralErr').show();
	        	isDataValid = false;	
	        }

	        if(isDataValid == false)
	        {
	        	return false;
	        }
	        /* End LANGUAGE Form Validation */
			
			$.ajax({
				method : "POST",
				url    : "<?php echo base_url() ?>"+"masters/Mst_bio_rec/InsertLanguages/", 
				data   : {
					headerId 	  : headerId,
					languageName  : $('#languageName').val(),
                	written 	  : $('#written').val(),
                	oral 		  : $('#oral').val(),
                	languageInfo  : $('#languageInfo').val()
				},
				success: function(data){
					languageTable.clear().draw();
					var dataSrc = JSON.parse(data);
					languageTable.rows.add(dataSrc).draw(false);
					alert("Data has been saved");
					clearDetailInputs();
					$('#languageName').focus();
				},
				error  : function(data){
					alert("Failed to save");
				} 
			});		        
        });
        /* End Save LANGUAGE Table */

        /* Start Select LANGUAGE */
        $('#languageTable tbody').on( 'click', 'tr', function () {
            var rowData = languageTable.row( this ).data();
            if ( $(this).hasClass('selected') ) 
            {
                $(this).removeClass('selected');
            }
            else 
            {
                languageTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');                    
            }
            rowLanguageId = rowData[0];
        });
        /* End Select LANGUAGE */

        /* Start DELETE Language */
        var is_valid = false;
        $('#languageTable tbody').on('click', '.btnLanguageDelete', function(){    	

            rowData = languageTable.row( $(this).parents('tr') ).data();
            languageId = rowData[0];  
            is_valid = confirm('Data dengan ID : '+languageId+' akan dihapus?');
            if (is_valid) 
            {
                languageTable.row( $(this).parents('tr') ).remove().draw();
            	$.ajax({
            		method : "POST",
            		url    : "<?php echo base_url() ?>"+"masters/Mst_bio_rec/DelLanguageById/"+languageId,
            		data   : {
            			languageId : languageId
            		},
            		success : function(data){
            			alert("Data has been deleted");
            		},
            		error   : function(data){
            			alert("Failed to delete");
            		}   
            	});
            }
        });
        /* End DELETE Language */
        /* END LANGUAGE */

        /* START ORGANIZATION */
        /* Start Save ORGANIZATION Table */
        $('#organizationNameErr').hide();
        $('#organizationYearErr').hide();
        $('#organizationPositionErr').hide();
        
        $('#saveOrganization').click(function() 
        {       
        	/* Start ORGANIZATION Form Validation */
        	var isDataValid = true;
        	$('#organizationNameErr').hide();
	        $('#organizationYearErr').hide();
	        $('#organizationPositionErr').hide();

	        if($('#organizationName').val() == "")
	        {
	        	$('#organizationName').focus();
	        	$('#organizationNameErr').show();
	        	isDataValid = false;
	        }
	        else if($('#organizationYear option:selected').text() == "Pilih")
	        {	        	
	        	$('#organizationYear').focus();
	        	$('#organizationYearErr').show();
	        	isDataValid = false;	        	
	        }
	        else if($('#organizationPosition').val() == "")
	        {
	        	$('#organizationPosition').focus();
	        	$('#organizationPositionErr').show();
	        	isDataValid = false;
	        }
	        if(isDataValid == false)
	        {
	        	return false;
	        } 
        	/* End ORGANIZATION Form Validation */

        	$.ajax({
        		method : "POST",
        		url    : "<?php echo base_url() ?>"+"masters/Mst_bio_rec/InsertOrganizations/", 
        		data   : {
        			headerId 		     : headerId,
        			organizationName     : $('#organizationName').val(),
                	organizationYear     : $('#organizationYear').val(),
                	organizationPosition : $('#organizationPosition').val()
        		},
        		success : function(data){
        			organizationTable.clear().draw();
					var dataSrc = JSON.parse(data);
					organizationTable.rows.add(dataSrc).draw(false);
					alert("Data has been saved");
					clearDetailInputs();
					$('#organizationName').focus();
        		},
        		error    : function(data){
        			alert("Failed to save");
        		}
        	});  	
        });
        /* End Save ORGANIZATION Table */
        /* Start Select ORGANIZATION */
        $('#organizationTable tbody').on( 'click', 'tr', function () {
            var rowData = organizationTable.row( this ).data();
            if ( $(this).hasClass('selected') ) 
            {
                $(this).removeClass('selected');
            }
            else 
            {
                organizationTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');                    
            }
            rowOrganizationId = rowData[0];
        });
        /* End Select ORGANIZATION */

        /* Start DELETE Organization */
        var isDataValid = false;
        $('#organizationTable tbody').on('click', '.btnOrganizationDelete', function(){   	
            rowData = organizationTable.row( $(this).parents('tr') ).data();
            organizationId = rowData[0];  
            is_valid = confirm('Data dengan ID : '+organizationId+' akan dihapus?');
            if (is_valid) 
            {
                organizationTable.row( $(this).parents('tr') ).remove().draw();
            	$.ajax({
            		method : "POST",
            		url    : "<?php echo base_url() ?>"+"masters/Mst_bio_rec/DelOrganizationById/"+organizationId,
            		data   : {
            			organizationId : organizationId
            		},
            		success : function(data){
            			alert("Data has been deleted");
            		},
            		error   : function(data){
            			alert("Failed to delete");
            		}   
            	});
            }
        });
        /* End DELETE Organization */
        /* END ORGANIZATION */

        /* START REFERENCE */
        /* Start Save REFERENCE Table */
        $('#referenceNameErr').hide();
		$('#referenceRelationErr').hide();			
		$('#referenceContactErr').hide();			
        
        $('#saveReference').click(function() 
        {   
        	/* Start REFERENCE Form Validation */
        	var isDataValid = true; 
        	$('#referenceNameErr').hide();
			$('#referenceRelationErr').hide();
			$('#referenceContactErr').hide();						
        	if($('#referenceName').val() == "")
			{
				$('#referenceName').focus();	
				$('#referenceNameErr').show();
				isDataValid = false;
			}			
			
			else if($('#referenceRelation').val() == "")
			{
				$('#referenceRelation').focus();
				$('#referenceRelationErr').show();
				isDataValid = false;
			}

			else if($('#referenceContact').val() == "")
			{
				$('#referenceContact').focus();
				$('#referenceContactErr').show();
				isDataValid = false;
			}	

			if(isDataValid == false)
			{
				return false;
			} 
			/* End REFERENCE Form Validation */

			$.ajax({
				method : "POST",
				url    : "<?php echo base_url() ?>"+"masters/Mst_bio_rec/InsertReference/", 
				data   : {
					headerId : headerId,
					referenceName     : $('#referenceName').val(),
	                referenceRelation : $('#referenceRelation').val(),
	                referenceAddress  : $('#referenceAddress').val(),
	                referenceContact  : $('#referenceContact').val()
				},
				success : function(data){
					referenceTable.clear().draw();
					var dataSrc = JSON.parse(data);
					referenceTable.rows.add(dataSrc).draw(false);
					alert("Data has been saved");
					clearDetailInputs();
					$('#referenceName').focus();
				},
				error : function(data){
					alert("Failed to save");
				}	
			});                          
        });
        /* End Save REFERENCE Table */

        /* Start Select REFERENCE */
        $('#referenceTable tbody').on( 'click', 'tr', function () {
            var rowData = referenceTable.row( this ).data();
            if ( $(this).hasClass('selected') ) 
            {
                $(this).removeClass('selected');
            }
            else 
            {
                referenceTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');                    
            }
        }); 
        /* End Select REFERENCE */

        /* Start Delete REFERENCE  */	
        var is_valid = false;
        $('#referenceTable tbody').on( 'click', '.btnReferenceDelete', function () 
        {   
            rowData = referenceTable.row( $(this).parents('tr') ).data();
            referenceId = rowData[0];  
            is_valid = confirm('Data dengan ID : '+referenceId+' akan dihapus?');
            if (is_valid) 
            {
                referenceTable.row( $(this).parents('tr') ).remove().draw();
            	$.ajax({
            		method : "POST",
            		url    : "<?php echo base_url() ?>"+"masters/Mst_bio_rec/DelReferenceById/"+referenceId,
            		data   : {
            			referenceId : referenceId
            		},
            		success : function(data){
            			alert("Data has been deleted");
            		},
            		error   : function(data){
            			alert("Failed to delete");
            		}   
            	});
            }

        });
        /* End Delete REFERENCE  */
        /* END REFERENCE */

        /* START Qualification */
        /* Start Save Qualification Table */
        $('#qualificationErr').hide();
		$('#qualificationYearErr').hide();			
        
        $('#saveQualification').click(function() 
        {  
        	debugger
        	var isQualificationCertified = "Y";
        	if ($('#isQualificationCertified').is(":checked"))
        	{	
        		isQualificationCertified = "Y";
        	}              
        	else
        	{
        		isQualificationCertified = "T";
        	} 
        	/* Start qualification Form Validation */
        	var isDataValid = true; 
        	$('#qualificationErr').hide();
			$('#qualificationYearErr').hide();					
        	if($('#qualification').val() == "")
			{
				$('#qualification').focus();	
				$('#qualificationErr').show();
				isDataValid = false;
			}			
			else if($('#qualificationYear').val() == "")
			{
				$('#qualificationYear').focus();
				$('#qualificationYearErr').show();
				isDataValid = false;
			}

			if(isDataValid == false)
			{
				return false;
			} 
			/* End Qualification Form Validation */

			$.ajax({
				method : "POST",
				url    : "<?php echo base_url() ?>"+"masters/Mst_bio_rec/InsertQualification/", 
				data   : {
					headerId : headerId,
					qualification     : $('#qualification').val(),
	                qualificationYear : $('#qualificationYear').val(),
	                isCertified    	  : isQualificationCertified
				},
				success : function(data){
					qualificationTable.clear().draw();
					var dataSrc = JSON.parse(data);
					qualificationTable.rows.add(dataSrc).draw(false);
					alert("Data has been saved");
					clearDetailInputs();
					$('#qualification').focus();
				},
				error : function(data){
					alert("Failed to save");
				}	
			});
                                        
        });
        /* End Save Qualification Table */

        /* Start Select qualification */
        $('#qualificationTable tbody').on( 'click', 'tr', function () {
            var rowData = qualificationTable.row( this ).data();
            if ( $(this).hasClass('selected') ) 
            {
                $(this).removeClass('selected');
            }
            else 
            {
                qualificationTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');                    
            }
        }); 
        /* End Select qualification */

        /* Start Delete qualification  */	
        var is_valid = false;
        $('#qualificationTable tbody').on( 'click', '.btnQualificationDelete', function () 
        {   
            rowData = qualificationTable.row( $(this).parents('tr') ).data();
            qualificationId = rowData[0];  
            is_valid = confirm('Data dengan ID : '+qualificationId+' akan dihapus?');
            if (is_valid) 
            {
                qualificationTable.row( $(this).parents('tr') ).remove().draw();
            	$.ajax({
            		method : "POST",
            		url    : "<?php echo base_url() ?>"+"masters/Mst_bio_rec/DelQualificationById/"+qualificationId,
            		data   : {
            			qualificationId : qualificationId
            		},
            		success : function(data){
            			alert("Data has been deleted");
            		},
            		error   : function(data){
            			alert("Failed to delete");
            		}   
            	});
            }

        });
        /* End Delete qualification  */
        /* END qualification */        

        $('#printBiodata').click(function(e) {
        	// debugger
        	// debugger
			var agesMax  			= $('#agesMax').val();
			var searchByPosition 	= $('#searchByPosition').val();
			var searchByCity  		= $('#searchByCity').val();
			var dataSrc;
			alert(searchByPosition);

			if(searchByPosition == '' && searchByCity == '') {
				alert('Filter cannot be empty');
				$('#searchByPosition').focus();
				return false;
			}
			
			if(searchByPosition == '') {
			   searchByPosition = "-";
			}

			if(searchByCity == '') {
			   searchByCity = "-";
			}

			var myUrl = "<?php echo base_url() ?>"+"masters/Mst_bio_rec/printBioReports/"+agesMax+"/"+isActive+"/"+searchByPosition+"/"+searchByCity+"/"+isStaff;
			// var myUrl = "<?php #echo base_url() ?>"+"MstBioRec/printBioReports/";
			// alert (myUrl);
			/* Do Something Here */	
			$.ajax({
				method 	: "POST",
				url		: myUrl,
				data 	: {						
					positionName : searchByPosition,
					agesMax 	 : agesMax,
					city 		 : searchByCity, 
					isActive 	 : isActive,
					isStaff 	 : isStaff 
				},					
           		success : function(response){
                    console.log(response);
                    window.open(myUrl,'_blank');
            	},
            	// success : function(data){
            	// 	alert(data);
            	// },
            	error : function(data){
                alert("Failed");
                }   
			});	
		}); 
        /* End Table Detail */

        $('#printImage').click(function(e) {
				var myUrl = "<?php echo base_url() ?>"+"masters/Mst_bio_rec/AddImageExcel/";
				// alert (myUrl);
				$.ajax({
					method 	: "POST",
					url		: myUrl,
					data 	: {						
						positionName : searchByPosition,
						agesMax 	 : agesMax,
						city 		 : searchByCity, 
						isActive 	 : isActive,
						isStaff 	 : isStaff 
					},					
               		success : function(response){
	                    console.log(response);
	                    window.open(myUrl,'_blank');
                	},
                	// success : function(data){
                	// 	alert(data);
                	// },
                	error : function(data){
                    alert("Failed");
	                }   
				});	
		});

		var listBioMenu = "<?php echo $this->uri->segment(3) ?>";
		var idCardNo = "<?php echo $this->uri->segment(4) ?>";

		$('#searchByIdNo').keyup(function(e){
			var idNoInput = $('#searchByIdNo').val();
			if(e.keyCode == 13){
				/* GET DATA BY ID CARD NO */
				// window.location.href="<?php #echo base_url() ?>"+"content/detail/bdl/";   
				var idNoInput = $('#searchByIdNo').val();
				if(idNoInput != ''){
					idCardNo = idNoInput;
				}
				var myURL = "<?php echo base_url() ?>"+"masters/Mst_bio_rec/GetByIdCardNo/"+idCardNo;
				// alert(myURL);
				$.ajax({
					method 	: "POST",
					url		: myURL,
					data 	: {
						idCardNo : idCardNo  
					},
					success	: function(data){	
						// alert(data);
						var dataSrc = JSON.parse(data);	
						educationTable.clear().draw();
						trainingTable.clear().draw();
						experienceTable.clear().draw();
						familyTable.clear().draw();
						languageTable.clear().draw();
						organizationTable.clear().draw();
						referenceTable.clear().draw();
						biodataListTable.clear().draw();
						biodataListTable.rows.add(dataSrc).draw(false);		
					},
					error	: function(data){
						alert("Failed to get data");
					}
				});
			}
		});
		
		// debugger
		$('#searchByPosition').focus();	
		if(listBioMenu == 'nbl' && idCardNo != ''){
			$('#searchByIdNo').val(idCardNo);
			$('#searchByIdNo').focus();
			$('#searchByIdNo').select();
			var myURL = "<?php echo base_url() ?>"+"masters/Mst_bio_rec/GetByIdCardNo/"+idCardNo;
			$.ajax({
				method 	: "POST",
				url		: myURL,
				data 	: {
					idCardNo : idCardNo  
				},
				success	: function(data){	
					educationTable.clear().draw();
					trainingTable.clear().draw();
					experienceTable.clear().draw();
					familyTable.clear().draw();
					languageTable.clear().draw();
					organizationTable.clear().draw();
					referenceTable.clear().draw();
					biodataListTable.clear().draw();
					var dataSrc = JSON.parse(data);	
					biodataListTable.rows.add(dataSrc).draw(false);		
				},
				error	: function(data){
					alert("Failed to get data");
				}
			});	
		}

		$.ajax({
            url : "<?php echo base_url() ?>"+"masters/Mst_position/getPositionGroup",
            method : "POST",
            data : {
                id : ""
            },
            success : function(data){
                var dataSrc = JSON.parse(data); 
                var dataLength = dataSrc.length;
                $('#groupPosition').find('option:not(:first)').remove();
                for(var i = 0; i < dataLength; i++){
                    $('#groupPosition').append(new Option(dataSrc[i]['pg_name'], dataSrc[i]['pg_id']));
                }
            },
            error : function(){
                alert('Failed');
            }
        });

        var gpId = '-';
        $('#groupPosition').on('change', function(){
            gpId = $('#groupPosition').val();
            var myUrl = "<?php echo base_url() ?>"+"masters/Mst_position/getPositionByPG/"+gpId; 
            // alert(myUrl);
            $.ajax({
                url : myUrl,
                method : "POST",
                data : {
                    id : ""
                },
                success : function(data){
                    var dataSrc = JSON.parse(data); 
                    var dataLength = dataSrc.length;
                    $('#appliedPosition').find('option:not(:first)').remove();
                    for(var i = 0; i < dataLength; i++){
                        $('#appliedPosition').append(new Option(dataSrc[i]['position_name'], dataSrc[i]['position_name']));
                    }
                },
                error : function(){
                    alert('Failed');
                }
            });  
        });
	});
</script>
<script type="text/javascript">
    $(document).ready(function(){
        var rowIdx = null;
        $("#loader").hide();
        var exprtList = $('#exprtList').DataTable({
                "paging":   true,
                "ordering": false,
                "info":     false,
                "filter":   true
        });

        /* START CLICK MAIN BUTTON */
        $("#viewExprtList").on("click", function(){
            var ptName = $("#clientName").val();
            var yearPeriod = $("#yearPeriod").val();
          
            var isValid = true;
            if($('#clientName option:selected').text() == "Pilih")
            {
                $("#clientName").focus();
                alert('PT Harus Dipilih ');
                isValid = false;
            } 
          
            else if($('#yearPeriod option:selected').text() == "Pilih")
            {
                $("#yearPeriod").focus();
                alert('Tahun Harus Dipilih ');
                isValid = false;
            }

            if(isValid == false)
            {return false} 
            var myUrl = "";
            myUrl = "<?php echo base_url().'reports/ExportSpt/getExprtView'; ?>";
            // alert(myUrl);

            /* Ajax Is Here */
            $.ajax({
                method : "POST",
                url    : myUrl,
                data   : {
                    pt   : ptName,
                    year : yearPeriod
                 
                },
                success : function(data){

                    $("#loader").hide();
                    $('#btnDisplay').prop('disabled', false);
                    exprtList.clear().draw();
                    var dataSrc = JSON.parse(data);                 
                    exprtList.rows.add(dataSrc).draw(false);
                },
                error   : function(data){
                    
                    $("#loader").hide();
                    $('#btnDisplay').prop('disabled', false);
                    $.notify({
                        title: "<h5>Informasi : </h5>",
                        message: "<strong>"+data+"</strong> </br></br> ",
                        icon: '' 
                    },
                    {
                        type: "warning",
                        delay: 3000
                    }); 
                }
            });
        });
        /* END CLICK MAIN BUTTON */

        // $.ajax({
        //     url : "<?php #echo base_url() ?>"+"masters/Mst_client/loadAll",
        //     method : "POST",
        //     async : false,
        //     dataType : 'json',
        //     success: function(data){
        //     var html = '';
        //     var i;
        //         html += '<option value="" disabled="" selected="">Pilih</option>';
        //         for(i=0; i<data.length; i++){
        //             html += '<option value="'+data[i].client_value+'" >'+data[i].client_name+'</option>';
        //         }   
        //     $('#clientName').html(html);
        //     }
        // });

        /* START SELECT DATA */
        $("#exprtList tbody").on("click", "tr", function(){
            var rowData = exprtList.row( this ).data();
            if ( $(this).hasClass('selected') ) 
            {
                $(this).removeClass('selected');
            }
            else 
            {
                exprtList.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');                    
            }
            rowIdx = exprtList.row( this ).index();
            $("#rowIdx").val(rowIdx); 
            // alert(rowIdx);           
            // payrollId = rowData[0];
            // alert(payrollId);
        });
        /* END SELECT DATA */

        /* START PRINT BUTTON CLICK */
        $('#printExprtList').on( 'click', function () {
            var ptName = $("#clientName").val();
            var yearPeriod = $("#yearPeriod").val();
            // var monthPeriod = $("#monthPeriod").val();

            var isValid = true;
            if($('#clientName option:selected').text() == "Pilih")
            {
                $("#clientName").focus();
                alert('PT Harus Dipilih ');
                isValid = false;
            } 
            // else if($('#monthPeriod option:selected').text() == "Pilih")
            // {
            //     $("#monthPeriod").focus();
            //     alert('Bulan Harus Dipilih ');
            //     isValid = false;
            // } 
            else if($('#yearPeriod option:selected').text() == "Pilih")
            {
                $("#yearPeriod").focus();
                alert('Tahun Harus Dipilih ');
                isValid = false;
            }
            

            if(isValid == false)
            {return false} 
            
            if($ptName = 'Pontil_Sumbawa'){

            var myUrl = "<?php echo base_url() ?>"+"reports/ExportSpt/printPtlSmb/"+ptName+"/"+yearPeriod;
            }
            // if($ptName = 'Pontil_Banyuwangi'){
            //     var myUrl = "<?php #echo base_url() ?>"+"reports/ExportSpt/printPtlBwg/"+ptName+"/"+yearPeriod;
            // }   
            // alert("PRINT "+myUrl);

            $.ajax({
                method : "POST",
                url : myUrl,
                data : {
                    ptName : ptName,
                    yearPeriod : yearPeriod
                    // monthPeriod : monthPeriod 
                },
                // success : function(data){
                    // alert(data);
                success : function(response){
                    console.log(response);
                    window.open(myUrl,'_blank');
                },
                error : function(data){
                    $.notify({
                        title: "<h5>Informasi : </h5>",
                        message: "<strong>"+data+"</strong> </br></br> ",
                        icon: '' 
                    },
                    {
                        type: "warning",
                        delay: 3000
                    }); 
                }   
            });
        });
        /* END PRINT BUTTON CLICK */

        // Start Proses Spt
        $('#prosesExprtSpt').on( 'click', function () {
            var ptName = $("#clientName").val();
            var yearPeriod = $("#yearPeriod").val();
            // var monthPeriod = $("#monthPeriod").val();

            var isValid = true;
            if($('#clientName option:selected').text() == "Pilih")
            {
                $("#clientName").focus();
                alert('PT Harus Dipilih ');
                isValid = false;
            } 
            // else if($('#monthPeriod option:selected').text() == "Pilih")
            // {
            //     $("#monthPeriod").focus();
            //     alert('Bulan Harus Dipilih ');
            //     isValid = false;
            // } 
            else if($('#yearPeriod option:selected').text() == "Pilih")
            {
                $("#yearPeriod").focus();
                alert('Tahun Harus Dipilih ');
                isValid = false;
            }
            

            if(isValid == false)
            {return false} 
            
            if($ptName = 'Pontil_Sumbawa'){

            var myUrl = "<?php echo base_url() ?>"+"reports/ExportSpt/proses_data_spt/"+ptName+"/"+yearPeriod;
            }
            // if($ptName = 'Pontil_Banyuwangi'){
            //     var myUrl = "<?php #echo base_url() ?>"+"reports/ExportSpt/printPtlBwg/"+ptName+"/"+yearPeriod;
            // }   
            // alert("PRINT "+myUrl);

            $.ajax({
                method : "POST",
                url : myUrl,
                data : {
                    ptName : ptName,
                    yearPeriod : yearPeriod
                    // monthPeriod : monthPeriod 
                },
                // success : function(data){
                    // alert(data);
                success : function(response){
                    console.log(response);
                    window.open(myUrl,'_blank');
                },
                error : function(data){
                    $.notify({
                        title: "<h5>Informasi : </h5>",
                        message: "<strong>"+data+"</strong> </br></br> ",
                        icon: '' 
                    },
                    {
                        type: "warning",
                        delay: 3000
                    }); 
                }   
            });
        });
        // End Proses SPT
       
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    var salaryTable = $('#salaryTable').DataTable({
        "paging":   true,
        "ordering": true,
        "info":     true,
        "filter":     true,
    });

    $.ajax({
        url : "<?php echo base_url() ?>"+"masters/Mst_client/loadAll",
        method : "POST",
        async : false,
        dataType : 'json',
        success: function(data){
        var html = '';
        var i;
            html += '<option value="" disabled="" selected="">Pilih</option>';
            for(i=0; i<data.length; i++){
                html += '<option value="'+data[i].client_value+'" >'+data[i].client_name+'</option>';
            }   
        $('#ptName').html(html);
        }
    });

    // $.ajax({
    //     method : "POST",
    //      url : "<?php echo base_url() ?>"+"masters/Mst_salary/loadSalaryDataReport", 
    //     data : {
    //         id :""
    //     },
    //     success : function(data){
    //         salaryTable.clear().draw();
    //         var dataSrc = JSON.parse(data); 
    //         salaryTable.rows.add(dataSrc).draw(false);
    //         // alert("Succeed");    
    //     },
    //     error : function(){
    //         alert("Failed Load Data");
    //     }
    // });

    $("#viewSalaryList").on("click",function(){
        var ptName      = $("#ptName").val();
        $.ajax({
            method  : "POST",
            url : "<?php echo base_url() ?>"+"masters/Mst_salary/loadSalaryDataReport", 
            data    : {
                pt      : ptName
            },
            success : function(data){
                salaryTable.clear().draw();
                var dataSrc = JSON.parse(data);                 
                salaryTable.rows.add(dataSrc).draw(false);
            },
            error   : function(data){
                alert("Failed");
            }   
        });

    });

    $('#printSalaryList').click(function(e) {
        var ptName      = $("#ptName").val();
        var myUrl = "<?php echo base_url() ?>"+"masters/Mst_salary/exportdataSalary/"+ptName;

        $.ajax({
            method  : "POST",
            url     : "<?php echo base_url() ?>"+"masters/Mst_salary/exportdataSalary/"+ptName,
            data    : {                     
                // ptName : ptName,
                // monthPeriod : monthPeriod,
                // yearPeriod : yearPeriod
            },                  
            success : function(response){
                console.log(response);
                window.open(myUrl,'_blank');
            },
            // success : function(data){
                // alert(data);
            // },
            error : function(data){
            alert("Failed");
            }   
        }); 
            
    }); 

});
</script>
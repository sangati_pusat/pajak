
<style type="text/css">
tr.selected {
  background-color: #B0BED9!important;
}
/*tr.even.selected {
  background-color: #B0BED9!important;
}*/
</style>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>

<div class="col-md-12" id="is_transaction">
  <div class="tile bg-info">
  <div class="tile">
    <div class="tile-body">
      <form class="form-horizontal">
        <div class="form-group row">

			<div class="col-xs-12 col-md-2">
				<label class="control-label" for="rteNumber">RTE Number</label>
				<select class="form-control" id="rteNumber" name="rteNumber" required="">
					<option value="ALL DATA">All Data </option>				
				</select>
			</div>

			<div class="col-xs-12 col-md-2">
				<label class="control-label" for="dateRequired">Month Required</label>				
				<select class="form-control" id="dateRequired" name="dateRequired" required="">
    				<option value="ALL DATA">All Data </option>
    				<script type="text/javascript">
    					var tMonth = 1;
    					for (var i = tMonth; i <= 12; i++) 
    					{
    						if(i < 10)
    						{
    							document.write("<option value='0"+i+"'>0"+i+"</option>");							
    						}
    						else
    						{
    							document.write("<option value='"+i+"'>"+i+"</option>");								
    						}
    					}
    				</script>
    			</select>
			</div>

	    </div>

			<!-- START BUTTON ADD & SAVE -->
        	<button class="btn btn-warning btnProcessPanel" type="button" id="viewRteReports" name="viewRteReports"><i class="fa fa-refresh"></i> Display Data </button>
        	<button class="btn btn-warning btnProcessPanel" type="button" id="printRteReports" name="printRteReports"><span class="fa fa-print"></span> Print </button>
        	<!-- END BUTTON ADD & SAVE -->

        	<br>
            <h3><code id="dataProses" class="backTransparent"><span></span></code></h3> 
        </div>
      </form>
    </div>
  </div>
</div>

<div class="col-md-12">
  	<div class="tile bg-info">
    <!-- <h3 class="tile-title">Barang Masuk</h3> -->
    	<div class="table-responsive">
						<table id="rteReportsTable" class="table table-hover table-bordered" class="table table-striped dataTable no-footer" cellspacing="0" width="100%" role="grid" style="width: 100%;">
				            <thead class="thead-dark">
				                <tr role="row">
				                	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">RTE Number </th>
				                	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Dept </th>
				                	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Month Required</th>
				                	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Company Employing The Labour</th>
				                	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Labour Source</th>
				                	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Position</th>
				                	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Rate Per Day</th>
				                	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Total Labour</th>
				                	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Start Date</th>
				                	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">End Date</th>
				                	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Total Date</th>
                          <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Work Location</th>
                          <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Remarks</th>
				                	
				                </tr>
				            </thead>				            
				          
				        </table>	
				 </div>
	             <!-- END TABLE PENGALAMAN -->			    	
			</div>
		</div>
		<!-- END DATA PAYROLL -->
		<br>	
	</div>	
</div>


<script type="text/javascript">
	$(document).ready(function(){
		/* START BIODATA TABLE */	
		var slipTable = $('#slipTable').DataTable({
	            "paging":   true,
	            "ordering": false,
	            "info":     true,
	            "filter":   true,
	            "columnDefs": [{
                    "targets": -3,
                    "data": null,
                    "defaultContent": "<button class='btn btn-warning btn-xs btn_update' data-toggle='modal' data-target='#editPayrollModal'><i class='fa fa-edit'></i></button>"
                },
                {
                    "targets": -2,
                    "data": null,
                    "defaultContent": "<button class='btn btn-warning btn-xs btn_print'><i class='fa fa-print'></i></button>"
                },
                {
                    "targets": -1,
                    "data": null,
                    // "defaultContent": "<button class='btn btn-warning btn-xs btn_process'><i class='fa fa-refresh'></i></button>"  
                    "defaultContent": "<button class='btn btn-warning btn-xs btn_process'><i class='fa fa-refresh'></i></button>" 
                }]
	        });
		/* END BIODATA TABLE */

		/* START LOAD BIODATA  */
        $("#processFilter").on("click", function(){
        	$("#idEksternalNo").val("");
        	$.ajax({
        		method : "POST",
        		url : "<?php echo base_url() ?>"+"MstBioRec/GetIdName",  
        		data : {
        			id :""
        		},
        		success : function(data){
    				slipTable.clear().draw();
					var dataSrc = JSON.parse(data);	
					slipTable.rows.add(dataSrc).draw(false);	
        		},
        		error : function(data){
        			$.notify({
		                title: "<h5>Informasi : </h5>",
		                message: "<strong>"+data+"</strong> </br></br> ",
		                icon: '' 
		            },
		            {
		                type: "warning",
		                delay: 3000
		            });	
        		}
        	});
        });
        /* END LOAD BIODATA  */
        $("#loader").hide();
        $('.dept').hide();
        $('.devData').hide();

        /* START SELECT BROWSE DATA */
        var internalId = "";
        var name = "";
        var rowData = null;
        $('#slipTable tbody').on( 'click', 'tr', function () {
            var rowData = slipTable.row( this ).data();
            if ( $(this).hasClass('selected') ) 
            {
                $(this).removeClass('selected');
            }
            else 
            {
                slipTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');                    
            }
            internalId = rowData[0];
            name = rowData[1];
        }); 
        /* END SELECT BROWSE DATA */
        
        /* START ADD FILTER PROCESS */
        $("#idFilter").val("");
        $("#nameFilter").val("");
        $('.rosterFilter').hide();
        $("#chooseBiodata").on("click", function(){
        	$('.rosterFilter').show();
        	$("#idFilter").val(internalId);
        	$("#nameFilter").val(name);
        });
        /* END ADD FILTER PROCESS */

        /* START CANCEL FILTER */
        $("#cancelFilter").on("click", function(){
        	$("#idFilter").val("");
	        $("#nameFilter").val("");
	        $('.rosterFilter').hide();
        });
        /* END CANCEL FILTER */

		$('.errMsg').hide();
		$('#dataProses').html('');
		var isValid = true;

		$('#btnProsesRoster').on('click', function(){		
			
			confirmDialog("Are You sure to do process?", (ans) => {
			  if (ans) {
			    // console.log("yes");
				$("#loader").show();
				$('.errMsg').hide();
				$('#btnProsesRoster').prop('disabled', true);
				if($('#clientName option:selected').text() == 'Pilih') 
				{
					$('#clientName').focus();
					$('#clientNameErr').show();
					isValid = false;
				}
				else if($('#monthPeriod option:selected').text() == 'Pilih') 
				{
					$('#monthPeriod').focus();
					$('#monthPeriodErr').show();
					isValid = false;
				}
				else if($('#yearPeriod option:selected').text() == 'Pilih') 
				{
					$('#yearPeriod').focus();
					$('#yearPeriodErr').show();
					isValid = false;
				}
				else if($('#rosterMaster option:selected').text() == 'Pilih') 
				{
					$('#rosterMaster').focus();
					$('#rosterMasterErr').show();
					isValid = false;
				}

				if(isValid == false)
				{
					$("#loader").hide();
					$('#btnProsesRoster').prop('disabled', false);
	                $.notify({
		                title: "<h5>Informasi : </h5>",
		                message: "<strong>Check Data</strong> </br></br> ",
		                icon: '' 
		            },
		            {
		                type: "warning",
		                delay: 3000
		            });	
					return false;
				}



				/*var isConfirm = window.confirm("Proses ini akan menimpa data sebelumnya, yakin?") 
				if(isConfirm == false) 
				{
					return false;
				}*/
				var client = $('#clientName').val();
				var dept = $('#dept').val();
				var month = $('#monthPeriod').val();
				var year = $('#yearPeriod').val();
				var roster = $('#rosterMaster').val();
				var otOffCount = $('#otInOffCount').val();
				var bioId = $('#idFilter').val(); 
				var myUrl = "<?php echo base_url().'transactions/timika/Timesheet/rosterProcess' ?>";
				// alert(myUrl);   
				$.ajax({
					method : "POST",
					url	   : myUrl,
					data   : {
						biodataId    : bioId,
						clientName   : client,
						dept   		 : dept,
						monthPeriod  : month,
						yearPeriod   : year,
						rosterMaster : roster,
						otInOffCount : otOffCount
					},
					success : function(data){
						// alert(data);
						$("#loader").hide();
						$('#btnProsesRoster').prop('disabled', false);
	                    $.notify({
			                title: "<h5>Informasi : </h5>",
			                message: "<strong>"+data+"</strong> </br></br> ",
			                // message: "<strong>Data Processed</strong> </br></br> ",
			                icon: '' 
			            },
			            {
			                type: "success",
			                delay: 2000
			            });	
			            slipTable.clear().draw();
		                var dataSrc = JSON.parse(data);                 
		                slipTable.rows.add(dataSrc).draw(false);
					},
					error 	: function(data){
						
						$("#loader").hide();
						$('#btnProsesRoster').prop('disabled', false);
	                    $.notify({
			                title: "<h5>Informasi : </h5>",
			                message: "<strong>"+data+"</strong> </br></br> ",
			                icon: '' 
			            },
			            {
			                type: "warning",
			                delay: 3000
			            });	
					}

				});
			  }else {
			    // console.log("no");
				return false;
			  }
			});

		});/* $('#btnProsesRoster').on('click', function(){	 */

		$('#btnDisplay').on('click', function(){		
			$("#loader").show();
			$('.errMsg').hide();
			$('#btnDisplay').prop('disabled', true);
			if($('#clientName option:selected').text() == 'Pilih') 
			{
				$('#clientName').focus();
				$('#clientNameErr').show();
				isValid = false;
			}
			else if($('#monthPeriod option:selected').text() == 'Pilih') 
			{
				$('#monthPeriod').focus();
				$('#monthPeriodErr').show();
				isValid = false;
			}
			else if($('#yearPeriod option:selected').text() == 'Pilih') 
			{
				$('#yearPeriod').focus();
				$('#yearPeriodErr').show();
				isValid = false;
			}
			else if($('#rosterMaster option:selected').text() == 'Pilih') 
			{
				$('#rosterMaster').focus();
				$('#rosterMasterErr').show();
				isValid = false;
			}

			if(isValid == false)
			{
				$("#loader").hide();
				$('#btnDisplay').prop('disabled', false);
                $.notify({
	                title: "<h5>Informasi : </h5>",
	                message: "<strong>Check Data</strong> </br></br> ",
	                icon: '' 
	            },
	            {
	                type: "warning",
	                delay: 3000
	            });	
				// $('.loading').html('<img src="<?php #echo base_url(); ?>loading.gif"><b></b>').slideUp(1000, function(){
				// 	$('#btnProsesPayroll').prop('disabled', false);
				// });
				return false;
			}
			/*var isConfirm = window.confirm("Proses ini akan menimpa data sebelumnya, yakin?") 
			if(isConfirm == false) 
			{
				return false;
			}*/
			var client = $('#clientName').val();
			var dept = $('#dept').val();
			var payrollGroup = $('#payrollGroup').val();
			var month = $('#monthPeriod').val();
			var year = $('#yearPeriod').val();
			var roster = $('#rosterMaster').val();
			var otOffCount = $('#otInOffCount').val();
			var bioId = $('#idFilter').val(); 
			var myUrl = "<?php echo base_url() ?>"+'transactions/timika/Timesheet/getPayrollList/'+client+'/'+year+'/'+month+'/'+payrollGroup;
			// alert(payrollGroup);   
			$.ajax({
				method : "POST",
				url	   : myUrl,
				data   : {
					biodataId    : bioId,
					clientName   : client,
					dept   		 : dept,
					monthPeriod  : month,
					yearPeriod   : year,
					rosterMaster : roster,
					otInOffCount : otOffCount
				},
				success : function(data){

					$("#loader").hide();
					$('#btnDisplay').prop('disabled', false);
					slipTable.clear().draw();
	                var dataSrc = JSON.parse(data);                 
	                slipTable.rows.add(dataSrc).draw(false);
                    /*$.notify({
		                title: "<h5>Informasi : </h5>",
		                message: "<strong>"+data+"</strong> </br></br> ",
		                icon: '' 
		            },
		            {
		                type: "success",
		                delay: 2000
		            });	*/
				},
				error 	: function(data){
					
					$("#loader").hide();
					$('#btnDisplay').prop('disabled', false);
                    $.notify({
		                title: "<h5>Informasi : </h5>",
		                message: "<strong>"+data+"</strong> </br></br> ",
		                icon: '' 
		            },
		            {
		                type: "warning",
		                delay: 3000
		            });	
				}

			});
		});/* $('#btnProsesPayroll').on('click', function(){ */


		/* START PRINT PAYSLIP */
        $('#slipTable tbody').on( 'click', '.btn_print', function () {
            var data = slipTable.row( $(this).parents('tr') ).data();
            var dataId = data[0]; 
            var ptName = $("#clientName").val(); 
            var calDate = $("#startDate").val(); 
            var payrollGroup = $("#payrollGroup").val(); 
            var isHealthBPJS = '0';
            var isJHT = '0';
            var isJP = '0';
            var isJKKM = '0';

            if ($("#cbHealthBPJS").is(':checked')) {
                isHealthBPJS = '1';
            }
            if ($("#cbJHT").is(':checked')) {
                isJHT = '1';
            }
            if ($("#cbJP").is(':checked')) {
                isJP = '1';
            }
            if ($("#cbJKKM").is(':checked')) {
                isJKKM = '1';
            }

            // var myUrl = "<?php #echo base_url() ?>"+"transactions/timika/Payroll/my_report/";
            var myUrl = "<?php echo base_url() ?>"+"transactions/timika/Payroll/toexcel/"+dataId+"/"+ptName+"/"+calDate+"/"+payrollGroup+"/"+isHealthBPJS+"/"+isJHT+"/"+isJP+"/"+isJKKM;
            // alert(myUrl);
            
            $.ajax({
                method : "POST",
                url : myUrl,
                data : {
                    slipId : dataId,
                    clientName : ptName,
                    calendarStart : calDate,
                    isHealthBPJS : isHealthBPJS,
                    isJHT : isJHT,
                    isJP : isJP,
                    isJKKM : isJKKM 
                },                
                // success : function(data){
                // 	alert(data);
                // },
                success : function(response){
                    console.log(response);
                    window.open(myUrl,'_blank');
                },
                error : function(data){
                    $.notify({
		                title: "<h5>Informasi : </h5>",
		                message: "<strong>"+data+"</strong> </br></br> ",
		                icon: '' 
		            },
		            {
		                type: "warning",
		                delay: 3000
		            });	
                }   
            });
        });
		/* END PRINT PAYSLIP */

		$(".govReg").hide();
        $('.dept').hide();
        $('.invoiceOnly').hide();

        /* START UPDATE BUTTON CLICK */
        /* START UPDATE BUTTON CLICK */
         $('#slipTable tbody').on( 'click', '.btn_update', function () {
            var data = slipTable.row( $(this).parents('tr') ).data();    
            var slipId = data[0];
            // alert('Hello');
            $(".ccPayment").hide();
            $(".contractBonus").hide();
            $(".productionBonus").hide();
            $(".outCamp").hide();
            $(".safetyBonus").hide();
            $(".otherAllowance1").hide(); 
            $(".otherAllowance2").hide();
            $(".jumboPercent").hide();
            $(".noAccidentFee").hide();
            $(".salaryStatus").hide();
            $(".statusInfo").hide();
            if($("#clientName").val() == "Pontil_Timika")            
            {
                $(".ccPayment").show();
                $(".productionBonus").show();
            }
            else if($("#clientName").val() == "Redpath_Timika")
            {
                $(".contractBonus").show();
                $(".noAccidentFee").show();
                $(".salaryStatus").show();
                $(".statusInfo").show();
                $(".jumboPercent").show();
            }

            $("#payrollId").val(slipId);
            var myUrl = "<?php echo base_url() ?>"+"transactions/timika/Payroll/getSlipByIid/";
            // alert(myUrl);
            $.ajax({
                url : myUrl,
                method : "POST",
                data : {
                    slipId : slipId
                },
                success : function(data) {
                	// alert(data);
                    var dataSrc = JSON.parse(data);
                    $("#payrollName").val(dataSrc[1]); 
                    $("#productionBonus").val(dataSrc[2]); 
                    $("#workAdjustment").val(dataSrc[3]); 
                    $("#adjustmentIn").val(dataSrc[4]); 
                    $("#adjustmentOut").val(dataSrc[5]); 
                    $("#contractBonus").val(dataSrc[6]); 
                    $("#outCamp").val(dataSrc[7]); 
                    $("#safetyBonus").val(dataSrc[8]); 
                    $("#attendanceBonus").val(dataSrc[9]); 
                    $("#otherAllowance1").val(dataSrc[10]); 
                    $("#jumboPercent").val(dataSrc[10]); 
                    $("#otherAllowance2").val(dataSrc[11]); 
                    $("#noAccidentFee").val(dataSrc[12]);
                    $("#ccPayment").val(dataSrc[13]); 
                    $("#thr").val(dataSrc[14]); 
                    $("#debtBurden").val(dataSrc[15]); 
                    $("#debtExplanation").val(dataSrc[16]);                     
                    $("#salaryStatus").val(dataSrc[17]);                     
                    $("#statusInfo").val(dataSrc[18]);
                    $("#debtBurden").val(dataSrc[19]);
                    $("#debtExplanation").val(dataSrc[20]);
                },
                error : function(data) {
                    // alert('Failed');
                    $.notify({
		                title: "<h5>Informasi : </h5>",
		                message: "<strong>Failed</strong> </br></br> ",
		                icon: '' 
		            },
		            {
		                type: "warning",
		                delay: 3000
		            });
                }
            });
         });


        // } );
        /* END UPDATE BUTTON CLICK */

        /* START SAVE UPDATE */
        $("#savePayroll").on("click", function(){            
            var clientName = $("#clientName").val();
            var pi = $("#payrollId").val(); 
            var pn = $("#payrollName").val(); 
            var pb = $("#productionBonus").val(); 
            var wa = $("#workAdjustment").val(); 
            var ai = $("#adjustmentIn").val(); 
            var ao = $("#adjustmentOut").val(); 
            var cb = $("#contractBonus").val();
            var oc = $("#outCamp").val();             
            var sb = $("#safetyBonus").val();             
            var ab = $("#attendanceBonus").val();             
            var oa1 = $("#otherAllowance1").val(); 
            var oa2 = $("#otherAllowance2").val();   
            var jumboPercent = $("#jumboPercent").val();   
            /* Start Allowance Other Trakindo */             
            var oat1 = $("#oAllowanceTra1").val();   
            var oat2 = $("#oAllowanceTra2").val();   
            /* End Allowance Other Trakindo */             
            /* Start Invoice Only */
            var general1 = $("#general1").val();    
            var general2 = $("#general2").val();    
            var general3 = $("#general3").val();    
            var general4 = $("#general4").val();    
            var general5 = $("#general5").val();    
            var general6 = $("#general6").val();    
            /* End Invoice Only */
            var na = $("#noAccidentFee").val();             
            var cp = $("#ccPayment").val(); 
            var thr = $("#thr").val();             
            var db = $("#debtBurden").val();             
            var dx = $("#debtExplanation").val();             
            /* Start Allowance KPI */             
            var kb = $("#kpiBenefits").val();             
            /* End Allowance KPI */             
            var ss = $("#salaryStatus").val();             
            var si = $("#statusInfo").val();             
            
            /* Start Ajax Insert Is Here */
            $.ajax({
                method : "POST",
                url    : "<?php echo base_url() ?>"+"transactions/timika/Payroll/updatePayroll",
                data   : {
                    clientName : clientName,
                    payrollId : pi,
                    productionBonus : pb,
                    workAdjustment : wa,
                    adjustmentIn : ai,
                    adjustmentOut : ao,
                    outCamp : oc,
                    safetyBonus : sb,
                    attendanceBonus : ab,
                    contractBonus : cb,
                    otherAllowance1 : oa1,
                    otherAllowance2 : oa2,
                    oAllowanceTra1 : oat1,
                    oAllowanceTra2 : oat2,
                    jumboPercent : jumboPercent,
                    general1 : general1,
                    general2 : general2,
                    general3 : general3,
                    general4 : general4,
                    general5 : general5,
                    general6 : general6,
                    noAccidentFee : na,
                    kpiBenefits : kb,
                    ccPayment : cp,
                    thr : thr, 
                    debtBurden : db,
                    debtExplanation : dx,
                    salaryStatus : ss,
                    statusInfo : si 
                },
                success : function(data){
                	// alert(data);
                	// alert(db);
                    $.notify({
		                title: "<h5>Informasi : </h5>",
		                message: "<strong>Data has been saved </strong> </br></br> ",
		                icon: '' 
		            },
		            {
		                type: "success",
		                delay: 3000
		            });	
                },
                error   : function(data){
                	$.notify({
		                title: "<h5>Informasi : </h5>",
		                message: "<strong>"+data+"</strong> </br></br> ",
		                icon: '' 
		            },
		            {
		                type: "warning",
		                delay: 3000
		            });	
                }
            });
            /* End Ajax Insert Is Here */
        });
        /* END SAVE UPDATE */

        $('#cbAccidentFee').on('change', function(){
            var slipId = $('#payrollId').val();
            var clientName = $('#clientName').val();
            var myUrl = "<?php echo base_url() ?>"+"transactions/timika/Payroll/getAccidentBonus";
            // alert(myUrl);
            if ($(this).is(':checked')) {

                $.ajax({
                    url    : myUrl,
                    method : "POST",
                    data : {
                        slipId : slipId,
                        clientName : clientName
                    },
                    success : function(data) {
                        $('#noAccidentFee').val(data);
                    },
                    error : function(data) {
                        // alert('Failed');
                        $.notify({
			                title: "<h5>Informasi : </h5>",
			                message: "<strong>Failed </strong> </br></br> ",
			                icon: '' 
			            },
			            {
			                type: "danger",
			                delay: 3000
			            });	
                    }
                });


            } else {
                $('#noAccidentFee').val(0.00);
            }
        });

        $('#payrollGroup').find('option:not(:first)').remove();
        $('#clientName').on('change', function(){
        	var clientName = $("#clientName").val();
        	if (clientName == 'Redpath_Timika') {
        		$('.devData').show();
        		$(".govReg").show();

        		$('#payrollGroup').find('option:not(:first)').remove();            
                $('#payrollGroup').append(new Option("All", "All"));
                $('#payrollGroup').append(new Option("Admin", "Admin"));
                $('#payrollGroup').append(new Option("Alimak", "Alimak"));
                $('#payrollGroup').append(new Option("Amole Rehab", "Amole Rehab"));
                $('#payrollGroup').append(new Option("Engineering", "Engineering"));
                $('#payrollGroup').append(new Option("Electric", "Electric"));
                $('#payrollGroup').append(new Option("DMLZ", "DMLZ"));
                $('#payrollGroup').append(new Option("Extrasion", "Extrasion"));
                $('#payrollGroup').append(new Option("GBC Area I", "GBC Area I"));
                $('#payrollGroup').append(new Option("GBC Area II", "GBC Area II"));
                $('#payrollGroup').append(new Option("GBC Area III", "GBC Area III"));
                $('#payrollGroup').append(new Option("MCM", "MCM"));
                $('#payrollGroup').append(new Option("Raisebore", "Raisebore"));
                $('#payrollGroup').append(new Option("Safety And Training", "Safety And Training"));
                $('#payrollGroup').append(new Option("Service Crew", "Service Crew"));
                $('#payrollGroup').append(new Option("STCP", "STCP")); 

        	}else{
        		$('.devData').hide();
        		$(".govReg").show();

        		$('#payrollGroup').find('option:not(:first)').remove();            
                $('#payrollGroup').append(new Option("All", "All"));

                $('#payrollGroup').append(new Option("A", "A"));
                $('#payrollGroup').append(new Option("B", "B"));
                $('#payrollGroup').append(new Option("C", "C"));
                $('#payrollGroup').append(new Option("D", "D"));
                $('#payrollGroup').append(new Option("E", "E"));
                // $('#payrollGroup').append(new Option("F", "F"));
                // $('#payrollGroup').append(new Option("G", "G"));
                // $('#payrollGroup').append(new Option("H", "H"));
                // $('#payrollGroup').append(new Option("I", "I"));
                // $('#payrollGroup').append(new Option("J", "J"));
                // $('#payrollGroup').append(new Option("K", "K"));
                // $('#payrollGroup').append(new Option("L", "L"));
                // $('#payrollGroup').append(new Option("M", "M"));
                // $('#payrollGroup').append(new Option("N", "N"));
                // $('#payrollGroup').append(new Option("O", "O"));
        	}
        });

        $("#btnUpdateDevPercent").on("click", function(){
			var client = $('#clientName').val();
			var deptName = $('#dept').val();
			var year = $('#yearPeriod').val();			
			var month = $('#monthPeriod').val();
			var percentVal = $('#devPercent').val();
			// alert(deptName);

			if(client == null || client == ''){
				$('#clientName').focus();
				$.notify({
	                title: "<h5>Informasi : </h5>",
	                message: "<strong>Client cannot be empty </strong> </br></br> ",
	                icon: '' 
	            },
	            {
	                type: "danger",
	                delay: 3000
	            });	
				return false;
			}

			if(deptName == null || deptName == ''){
				$('#dept').focus();
				$.notify({
	                title: "<h5>Informasi : </h5>",
	                message: "<strong>Dept cannot be empty </strong> </br></br> ",
	                icon: '' 
	            },
	            {
	                type: "danger",
	                delay: 3000
	            });
				return false;
			}

			if(year == null || year == ''){
				$('#yearPeriod').focus();
				$.notify({
	                title: "<h5>Informasi : </h5>",
	                message: "<strong>Year cannot be empty </strong> </br></br> ",
	                icon: '' 
	            },
	            {
	                type: "danger",
	                delay: 3000
	            });
				return false;
			}

			if(month == null || month == ''){
				$('#monthPeriod').focus();
				$.notify({
	                title: "<h5>Informasi : </h5>",
	                message: "<strong>Month cannot be empty </strong> </br></br> ",
	                icon: '' 
	            },
	            {
	                type: "danger",
	                delay: 3000
	            });
				return false;
			}

			if(percentVal == null || percentVal == ''){
				$('#devPercent').focus();
				$.notify({
	                title: "<h5>Informasi : </h5>",
	                message: "<strong>Percent Value cannot be empty </strong> </br></br> ",
	                icon: '' 
	            },
	            {
	                type: "danger",
	                delay: 3000
	            });
				return false;
			}

			$.ajax({
				method : "POST",
				url    : "<?php echo base_url() ?>"+"transactions/timika/Payroll/updateSlipByDept", 
				data   : {
					clientName : client,
					dept : deptName,
					yearPeriod : year,
					monthPeriod : month,
					devPercent : percentVal
				},
				success : function(data){
					// alert(data);
					// alert("Update Persentase Dev Incentive Berhasil");
					$.notify({
		                title: "<h5>Informasi : </h5>",
		                message: "<strong>Dev Incentive Percent Updated </strong> </br></br> ",
		                icon: '' 
		            },
		            {
		                type: "success",
		                delay: 3000
		            });	
				},
				error   : function(data){
					// alert("Update Persentase Dev Incentive Gagal");
					$.notify({
		                title: "<h5>Informasi : </h5>",
		                message: "<strong>Update Failed </strong> </br></br> ",
		                icon: '' 
		            },
		            {
		                type: "danger",
		                delay: 3000
		            });	
				}	
			});
		});

		$('#slipTable tbody').on( 'click', '.btn_process', function () {
			var data = slipTable.row( $(this).parents('tr') ).data();    
            var slipId = data[0];
            // alert(slipId);

            confirmDialog("Yakin melakukan proses?", (ans) => {
			  if (ans) {
			    // console.log("yes");
				$("#loader").show();
				$('.errMsg').hide();
				$('#btnProsesRoster').prop('disabled', true);
				if($('#clientName option:selected').text() == 'Pilih') 
				{
					$('#clientName').focus();
					$('#clientNameErr').show();
					isValid = false;
				}
				else if($('#monthPeriod option:selected').text() == 'Pilih') 
				{
					$('#monthPeriod').focus();
					$('#monthPeriodErr').show();
					isValid = false;
				}
				else if($('#yearPeriod option:selected').text() == 'Pilih') 
				{
					$('#yearPeriod').focus();
					$('#yearPeriodErr').show();
					isValid = false;
				}
				else if($('#rosterMaster option:selected').text() == 'Pilih') 
				{
					$('#rosterMaster').focus();
					$('#rosterMasterErr').show();
					isValid = false;
				}

				if(isValid == false)
				{
					$("#loader").hide();
					$('#btnProsesRoster').prop('disabled', false);
	                $.notify({
		                title: "<h5>Informasi : </h5>",
		                message: "<strong>Check Data</strong> </br></br> ",
		                icon: '' 
		            },
		            {
		                type: "warning",
		                delay: 3000
		            });	
					return false;
				}

				/*var isConfirm = window.confirm("Proses ini akan menimpa data sebelumnya, yakin?") 
				if(isConfirm == false) 
				{
					return false;
				}*/
				var client = $('#clientName').val();
				var dept = $('#dept').val();
				var month = $('#monthPeriod').val();
				var year = $('#yearPeriod').val();
				var roster = $('#rosterMaster').val();
				var otOffCount = $('#otInOffCount').val();
				// var bioId = $('#idFilter').val(); 
				var myUrl = "<?php echo base_url().'transactions/timika/Timesheet/rosterProcess' ?>";
				// alert(myUrl);   
				$.ajax({
					method : "POST",
					url	   : myUrl,
					data   : {
						slipId 		 : slipId,
						biodataId    : 'BySlipId',
						clientName   : client,
						dept   		 : dept,
						monthPeriod  : month,
						yearPeriod   : year,
						rosterMaster : roster,
						otInOffCount : otOffCount
					},
					success : function(data){
						alert(data);
						$("#loader").hide();
						$('#btnProsesRoster').prop('disabled', false);
	                    $.notify({
			                title: "<h5>Informasi : </h5>",
			                message: "<strong>Data Processed</strong> </br></br> ",
			                icon: '' 
			            },
			            {
			                type: "success",
			                delay: 2000
			            });	
			            slipTable.clear().draw();
		                var dataSrc = JSON.parse(data);                 
		                slipTable.rows.add(dataSrc).draw(false);
					},
					error 	: function(data){
						
						$("#loader").hide();
						$('#btnProsesRoster').prop('disabled', false);
	                    $.notify({
			                title: "<h5>Informasi : </h5>",
			                message: "<strong>"+data+"</strong> </br></br> ",
			                icon: '' 
			            },
			            {
			                type: "warning",
			                delay: 3000
			            });	
					}

				});
			  }else {
			    // console.log("no");
				return false;
			  }
			});


		});


		function confirmDialog(message, handler){
		  $(`<div class="modal fade" id="myModal" role="dialog"> 
		     <div class="modal-dialog"> 
		       <!-- Modal content--> 
		        <div class="modal-content"> 
		           <div class="modal-body" style="padding:10px;"> 
		             <h4 class="text-center">${message}</h4> 
		             <div class="text-center"> 
		               <a class="btn btn-danger btn-yes">yes</a> 
		               <a class="btn btn-default btn-no">no</a> 
		             </div> 
		           </div> 
		       </div> 
		    </div> 
		  </div>`).appendTo('body');
		 
		  //Trigger the modal
		  $("#myModal").modal({
		     backdrop: 'static',
		     keyboard: false
		  });
		  
		   //Pass true to a callback function
		   $(".btn-yes").click(function () {
		       handler(true);
		       $("#myModal").modal("hide");
		   });
		    
		   //Pass false to callback function
		   $(".btn-no").click(function () {
		       handler(false);
		       $("#myModal").modal("hide");
		   });

		   //Remove the modal once it is closed.
		   $("#myModal").on('hidden.bs.modal', function () {
		      $("#myModal").remove();
		   });
		}

	});
</script>
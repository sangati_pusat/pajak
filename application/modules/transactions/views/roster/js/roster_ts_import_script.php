<script type="text/javascript">
	$(document).ready(function(){
  		$("#loader").hide();
  		$("#myDiv").hide();

		$('#uploadRoster').on("click", function(){
			$("#loader").show();
  			$("#myDiv").hide();
			$('#uploadRoster').prop('disabled', true);
			var formData = new FormData($("#rosterUploadForm")[0]);
			var myUrl = "<?php echo base_url() ?>"+"transactions/roster/Timesheet_roster/upload/";
			$.ajax({
			    url: myUrl,
			    type: "POST",
			    data : formData,
			    processData: false,
			    contentType: false,
			    beforeSend: function() {
			    },
			    success: function(data){
					$("#myDiv").show();
					$("#loader").hide();
					$('#uploadRoster').prop('disabled', false);
                    alert(data);
			    },
			    error: function(data){
					alert(data);
			    }
			    /*error: function(xhr, ajaxOptions, thrownError) {
			       console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			    }*/
			});

		});


	});
</script>
<script type="text/javascript">
	$(document).ready(function(){
		/* START BIODATA TABLE */	
		var slipTable = $('#slipTable').DataTable({
	            "paging":   true,
	            "ordering": false,
	            "info":     true,
	            "filter":   true,
	            "columnDefs": [{
                    "targets": -3,
                    "data": null,
                    "defaultContent": "<button class='btn btn-warning btn-xs btn_update' data-toggle='modal' data-target='#editPayrollModal'><i class='fa fa-edit'></i></button>"
                },
                {
                    "targets": -2,
                    "data": null,
                    "defaultContent": "<button class='btn btn-warning btn-xs btn_print'><i class='fa fa-print'></i></button>"   
                },
                {
                    "targets": -1,
                    "data": null,
                    // "defaultContent": "<button class='btn btn-warning btn-xs btn_process'><i class='fa fa-refresh'></i></button>"  
                    "defaultContent": "<button class='btn btn-warning btn-xs btn_process'><i class='fa fa-refresh'></i></button>" 
                }]
	        });
		/* END BIODATA TABLE */

		/* NIE FILTER FOR PROCESS */
		$("#nie").hide();
		$("#btnGetNie").on("click", function(){
			$("#nie").show();
			$("#nie").focus();
		});
		$("#btnCancelNie").on("click", function(){
			$("#nie").val("") ;
			$("#nie").hide();
		});


		/* START LOAD BIODATA  */
        $("#processFilter").on("click", function(){
        	$("#idEksternalNo").val("");
        	$.ajax({
        		method : "POST",
        		url : "<?php echo base_url() ?>"+"MstBioRec/GetIdName",  
        		data : {
        			id :""
        		},
        		success : function(data){
    				slipTable.clear().draw();
					var dataSrc = JSON.parse(data);	
					slipTable.rows.add(dataSrc).draw(false);	
        		},
        		error : function(data){
        			$.notify({
		                title: "<h5>Informasi : </h5>",
		                message: "<strong>"+data+"</strong> </br></br> ",
		                icon: '' 
		            },
		            {
		                type: "warning",
		                delay: 3000
		            });	
        		}
        	});
        });
        /* END LOAD BIODATA  */
        $("#loader").hide();
        $('.dept').hide();
        $('#clientName').on('change', function(){
        	var cn = $('#clientName').val();
        	if(cn == 'AMNT_Sumbawa' || cn == 'Trakindo_Sumbawa' || cn == 'Machmahon_Sumbawa' || cn == 'Pontil_Sumbawa' || cn == 'Pontil_Banyuwangi'){
        		$('.dept').show();
        		var clientName = $('#clientName').val();
	            $('#dept').find('option:not(:first)').remove();
	            if(clientName == 'AMNT_Sumbawa')
	            {   
	                $('#dept').find('option:not(:first)').remove();
	                $('#dept').append(new Option("FIXED PLANT", "FIXED PLANT"));
	                $('#dept').append(new Option("LINESMAN", "LINESMAN"));
	                $('#dept').append(new Option("POWER PLANT", "POWER PLANT"));  
	                $('#dept').append(new Option("WELDER PLANT", "WELDER PLANT"));              
	            }
	            // else if(clientName == 'Trakindo_Sumbawa')
	            // {
	            //     $('#dept').find('option:not(:first)').remove();            
	            //     $('#dept').append(new Option("CAT RENTAL", "CAT RENTAL"));
	            //     $('#dept').append(new Option("FACILITY MMA", "FACILITY MMA"));
	            //     $('#dept').append(new Option("MAN HAUL", "MAN HAUL"));
	            //     $('#dept').append(new Option("ENGINEERING", "ENGINEERING"));
	            // }
	            else if(clientName == 'Machmahon_Sumbawa')
	            {
	                $('#dept').find('option:not(:first)').remove();            
	                $('#dept').append(new Option("DRY SEASON", "DRY SEASON"));
	                $('#dept').append(new Option("MEWS", "MEWS"));
	                $('#dept').append(new Option("MINNING PROJECT", "MINNING PROJECT"));
	                $('#dept').append(new Option("PROCESS MAINTENANCE", "PROCESS MAINTENANCE"));
	            }
        	} else {
        		$('.dept').hide();
        	}
        });       

        /* START SELECT BROWSE DATA */
        var internalId = "";
        var name = "";
        var rowData = null;
        $('#slipTable tbody').on( 'click', 'tr', function () {
            var rowData = slipTable.row( this ).data();
            if ( $(this).hasClass('selected') ) 
            {
                $(this).removeClass('selected');
            }
            else 
            {
                slipTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');                    
            }
            internalId = rowData[0];
            name = rowData[1];
        }); 
        /* END SELECT BROWSE DATA */
        
        /* START ADD FILTER PROCESS */
        $("#idFilter").val("");
        $("#nameFilter").val("");
        $('.rosterFilter').hide();
        $("#chooseBiodata").on("click", function(){
        	$('.rosterFilter').show();
        	$("#idFilter").val(internalId);
        	$("#nameFilter").val(name);
        });
        /* END ADD FILTER PROCESS */

        /* START CANCEL FILTER */
        $("#cancelFilter").on("click", function(){
        	$("#idFilter").val("");
	        $("#nameFilter").val("");
	        $('.rosterFilter').hide();
        });
        /* END CANCEL FILTER */

		$('.errMsg').hide();
		$('#dataProses').html('');
		var isValid = true;

			
		$('#btnProsesRoster').on('click', function(){	

			$('#dataGroup').find('option:not(:first)').remove(); 
            // $('#dataGroup option[value="A"]').remove();
        	// $('#dataGroup').append(new Option("ALL", "A"));
        	$('#dataGroup').append(new Option("LOCAL", "L"));
            $('#dataGroup').append(new Option("NON LOCAL", "N"));				

			confirmDialog("Are You sure to do process?", (ans) => {
			  if (ans) {
			  		$("#loader").show();
					$('.errMsg').hide();
					$('#btnProsesRoster').prop('disabled', true);
					if($('#clientName option:selected').text() == 'Pilih') 
					{
						$('#clientName').focus();
						$('#clientNameErr').show();
						isValid = false;
					}
					else if($('#monthPeriod option:selected').text() == 'Pilih') 
					{
						$('#monthPeriod').focus();
						$('#monthPeriodErr').show();
						isValid = false;
					}
					else if($('#yearPeriod option:selected').text() == 'Pilih') 
					{
						$('#yearPeriod').focus();
						$('#yearPeriodErr').show();
						isValid = false;
					}
					else if($('#rosterMaster option:selected').text() == 'Pilih') 
					{
						$('#rosterMaster').focus();
						$('#rosterMasterErr').show();
						isValid = false;
					}

					if(isValid == false)
					{
						$("#loader").hide();
						$('#btnProsesRoster').prop('disabled', false);
		                $.notify({
			                title: "<h5>Informasi : </h5>",
			                message: "<strong>Check Data</strong> </br></br> ",
			                icon: '' 
			            },
			            {
			                type: "warning",
			                delay: 3000
			            });	
						return false;
					}
					/*var isConfirm = window.confirm("Proses ini akan menimpa data sebelumnya, yakin?") 
					if(isConfirm == false) 
					{
						return false;
					}*/
					var client = $('#clientName').val();
					var dept = $('#dept').val();
					var month = $('#monthPeriod').val();
					var year = $('#yearPeriod').val();
					var roster = $('#rosterMaster').val();
					var otOffCount = $('#otInOffCount').val();
					// alert(otOffCount);
					var bioId = $('#idFilter').val(); 
					var nie = $('#nie').val();
					// alert(nie);
					// return false;

					// var myUrl = "<?php #echo base_url().'transactions/sumbawa/Timesheet/rosterProcess' ?>";
					var myUrl = "";
					if(client == 'Pontil_Sumbawa'){
						myUrl = "<?php echo base_url().'transactions/sumbawa/Timesheet_ptlsmb/rosterProcess' ?>";
					}
					else if(client == 'Pontil_Banyuwangi'){
						myUrl = "<?php echo base_url().'transactions/sumbawa/Timesheet_ptlbwg/rosterProcess' ?>";
					}
					else if(client == 'LCP_Sumbawa'){
						myUrl = "<?php echo base_url().'transactions/sumbawa/Timesheet_lcpsmb/rosterProcess' ?>";
					}
					else if(clientName == 'Trakindo_Sumbawa'){
						myUrl = "<?php echo base_url().'transactions/sumbawa/Timesheet_trksmb/rosterProcessTrksmb' ?>";
					}
					// alert(myUrl);   
					// alert(dept);   
					// alert(nie);   
					$.ajax({
						method : "POST",
						url	   : myUrl,
						data   : {
							biodataId    : bioId,
							badgeNo    	 : nie,
							clientName   : client,
							dept   		 : dept,
							monthPeriod  : month,
							yearPeriod   : year,
							rosterMaster : roster,
							otInOffCount : otOffCount
						},
						success : function(data){
							// alert(data);
							$("#loader").hide();
							$('#btnProsesRoster').prop('disabled', false);
		                    /*$.notify({
				                title: "<h5>Informasi : </h5>",
				                message: "<strong>"+data+"</strong> </br></br> ",
				                icon: '' 
				            },
				            {
				                type: "success",
				                delay: 2000
				            });	*/
				            slipTable.clear().draw();
			                var dataSrc = JSON.parse(data);                 
			                slipTable.rows.add(dataSrc).draw(false);
							$('#nie').val("");
							// $('#nie').hide();
						},
						error 	: function(data){
							
							$("#loader").hide();
							$('#btnProsesRoster').prop('disabled', false);
		                    $.notify({
				                title: "<h5>Informasi : </h5>",
				                message: "<strong>"+data+"</strong> </br></br> ",
				                icon: '' 
				            },
				            {
				                type: "warning",
				                delay: 3000
				            });	

						}

					});		
			  }
			});

			
		});/* $('#btnProsesRoster').on('click', function(){	 */

		
		$('#btnDisplay').on('click', function(){		
			$("#loader").show();
			$('.errMsg').hide();
			$('#btnDisplay').prop('disabled', true);
			if($('#clientName option:selected').text() == 'Pilih') 
			{
				$('#clientName').focus();
				$('#clientNameErr').show();
				isValid = false;
			}
			else if($('#monthPeriod option:selected').text() == 'Pilih') 
			{
				$('#monthPeriod').focus();
				$('#monthPeriodErr').show();
				isValid = false;
			}
			else if($('#yearPeriod option:selected').text() == 'Pilih') 
			{
				$('#yearPeriod').focus();
				$('#yearPeriodErr').show();
				isValid = false;
			}
			else if($('#rosterMaster option:selected').text() == 'Pilih') 
			{
				$('#rosterMaster').focus();
				$('#rosterMasterErr').show();
				isValid = false;
			}

			if(isValid == false)
			{
				$("#loader").hide();
				$('#btnDisplay').prop('disabled', false);
                $.notify({
	                title: "<h5>Informasi : </h5>",
	                message: "<strong>Check Data</strong> </br></br> ",
	                icon: '' 
	            },
	            {
	                type: "warning",
	                delay: 3000
	            });	
				// $('.loading').html('<img src="<?php #echo base_url(); ?>loading.gif"><b></b>').slideUp(1000, function(){
				// 	$('#btnProsesPayroll').prop('disabled', false);
				// });
				return false;
			}
			/*var isConfirm = window.confirm("Proses ini akan menimpa data sebelumnya, yakin?") 
			if(isConfirm == false) 
			{
				return false;
			}*/
			var client = $('#clientName').val();
			var dept = $('#dept').val();
			var month = $('#monthPeriod').val();
			var year = $('#yearPeriod').val();
			var roster = $('#rosterMaster').val();
			var otOffCount = $('#otInOffCount').val();
			var bioId = $('#idFilter').val(); 
			
			var myUrl = "";
			var dataGroup = ""; 
			if(client == 'Pontil_Sumbawa'){
				dataGroup = $('#dataGroup').val(); 
				myUrl = "<?php echo base_url() ?>"+'transactions/sumbawa/Timesheet_ptlsmb/getPayrollList/'+client+'/'+dept+'/'+year+'/'+month+'/'+dataGroup;
			}
			else if(client == 'Pontil_Banyuwangi'){
				dataGroup = $('#dataGroup').val(); 
				myUrl = "<?php echo base_url() ?>"+'transactions/sumbawa/Timesheet_ptlbwg/getPayrollList/'+client+'/'+dept+'/'+year+'/'+month+'/'+dataGroup;
			}else if(client == 'LCP_Sumbawa'){
				dataGroup = $('#dataGroup').val(); 
				myUrl = "<?php echo base_url() ?>"+'transactions/sumbawa/Timesheet_lcpsmb/getPayrollList/'+client+'/'+dept+'/'+year+'/'+month+'/'+dataGroup;
			}
			else if(client == 'Trakindo_Sumbawa'){
				var dataGroup = $('#dataGroup').val();
				myUrl = "<?php echo base_url() ?>"+'transactions/sumbawa/Timesheet_trksmb/getPayrollList/'+client+'/'+year+'/'+month;
			}

			
			// alert(myUrl);   
			$.ajax({
				method : "POST",
				url	   : myUrl,
				data   : {
					biodataId    : bioId,
					clientName   : client,
					dept   		 : dept,
					monthPeriod  : month,
					yearPeriod   : year,
					rosterMaster : roster,
					otInOffCount : otOffCount
				},
				success : function(data){

					$("#loader").hide();
					$('#btnDisplay').prop('disabled', false);
					slipTable.clear().draw();
	                var dataSrc = JSON.parse(data);                 
	                slipTable.rows.add(dataSrc).draw(false);
                    /*$.notify({
		                title: "<h5>Informasi : </h5>",
		                message: "<strong>"+data+"</strong> </br></br> ",
		                icon: '' 
		            },
		            {
		                type: "success",
		                delay: 2000
		            });	*/
				},
				error 	: function(data){
					
					$("#loader").hide();
					$('#btnDisplay').prop('disabled', false);
                    $.notify({
		                title: "<h5>Informasi : </h5>",
		                message: "<strong>"+data+"</strong> </br></br> ",
		                icon: '' 
		            },
		            {
		                type: "warning",
		                delay: 3000
		            });	
				}

			});
		});/* $('#btnProsesPayroll').on('click', function(){ */


		/* START PRINT PAYSLIP */
        $('#slipTable tbody').on( 'click', '.btn_print', function () {
            var data = slipTable.row( $(this).parents('tr') ).data();
            var dataId = data[0]; 
            var ptName = $("#clientName").val(); 
            var calDate = $("#startDate").val(); 
            var payrollGroup = $("#payrollGroup").val(); 
            var isHealthBPJS = '0';
            var isJHT = '0';
            var isJP = '0';
            var isJKKM = '0';
            // debugger;
            if ($("#cbHealthBPJS").is(':checked')) {
                isHealthBPJS = '1';
            }
            if ($("#cbJHT").is(':checked')) {
                isJHT = '1';
            }
            if ($("#cbJP").is(':checked')) {
                isJP = '1';
            }
            if ($("#cbJKKM").is(':checked')) {
                isJKKM = '1';
            }

            // var myUrl = "<?php #echo base_url() ?>"+"transactions/sumbawa/Payroll/my_report/";
            var myUrl = "";
            if(ptName == 'Pontil_Sumbawa'){
               myUrl = "<?php echo base_url() ?>"+"transactions/sumbawa/Payroll_ptlsmb/toexcel/"+dataId+"/"+ptName+"/"+calDate+"/"+payrollGroup+"/"+isHealthBPJS+"/"+isJHT+"/"+isJP+"/"+isJKKM;
            }

            else if(ptName == 'Pontil_Banyuwangi'){
               myUrl = "<?php echo base_url() ?>"+"transactions/sumbawa/Payroll_ptlbwg/toexcel/"+dataId+"/"+ptName+"/"+calDate+"/"+payrollGroup+"/"+isHealthBPJS+"/"+isJHT+"/"+isJP+"/"+isJKKM;
            }

            else if(ptName == 'LCP_Sumbawa'){
               myUrl = "<?php echo base_url() ?>"+"transactions/sumbawa/Payroll_lcpsmb/toexcel/"+dataId+"/"+ptName+"/"+calDate+"/"+payrollGroup+"/"+isHealthBPJS+"/"+isJHT+"/"+isJP+"/"+isJKKM;
            }
            else if(ptName == 'Trakindo_Sumbawa'){
               myUrl = "<?php echo base_url() ?>"+"transactions/sumbawa/Payroll_trksmb/trakindoSmbExcel/"+dataId+"/"+ptName+"/"+calDate+"/"+isHealthBPJS+"/"+isJHT+"/"+isJP+"/"+isJKKM;
            }
            // alert(myUrl); 
            // return false;
            
            $.ajax({
                method : "POST",
                url : myUrl,
                data : {
                    slipId : dataId,
                    clientName : ptName,
                    calendarStart : calDate,
                    isHealthBPJS : isHealthBPJS,
                    isJHT : isJHT,
                    isJP : isJP,
                    isJKKM : isJKKM 
                },

                // success : function(data){
                // 	alert(data);
                // },
                success : function(response){
                    console.log(response);
                    window.open(myUrl,'_blank');
                },
                error : function(data){
                    $.notify({
		                title: "<h5>Informasi : </h5>",
		                message: "<strong>"+data+"</strong> </br></br> ",
		                icon: '' 
		            },
		            {
		                type: "warning",
		                delay: 3000
		            });	
                }   
            });
        });
		/* END PRINT PAYSLIP */
		/* START PROCESS BY ID */
		$('#slipTable tbody').on( 'click', '.btn_process', function () {
			var data = slipTable.row( $(this).parents('tr') ).data();    
            var slipId = data[0];
            // alert(slipId);

            confirmDialog("Yakin melakukan proses?", (ans) => {
			  if (ans) {
			    // console.log("yes");
				$("#loader").show();
				$('.errMsg').hide();
				$('#btnProsesRoster').prop('disabled', true);
				if($('#clientName option:selected').text() == 'Pilih') 
				{
					$('#clientName').focus();
					$('#clientNameErr').show();
					isValid = false;
				}
				else if($('#monthPeriod option:selected').text() == 'Pilih') 
				{
					$('#monthPeriod').focus();
					$('#monthPeriodErr').show();
					isValid = false;
				}
				else if($('#yearPeriod option:selected').text() == 'Pilih') 
				{
					$('#yearPeriod').focus();
					$('#yearPeriodErr').show();
					isValid = false;
				}
				// else if($('#rosterMaster option:selected').text() == 'Pilih') 
				// {
				// 	$('#rosterMaster').focus();
				// 	$('#rosterMasterErr').show();
				// 	isValid = false;
				// }

				if(isValid == false)
				{
					$("#loader").hide();
					$('#btnProsesRoster').prop('disabled', false);
	                $.notify({
		                title: "<h5>Informasi : </h5>",
		                message: "<strong>Check Data</strong> </br></br> ",
		                icon: '' 
		            },
		            {
		                type: "warning",
		                delay: 3000
		            });	
					return false;
				}

				/*var isConfirm = window.confirm("Proses ini akan menimpa data sebelumnya, yakin?") 
				if(isConfirm == false) 
				{
					return false;
				}*/
				var client = $('#clientName').val();
				var dept = $('#dept').val();
				var month = $('#monthPeriod').val();
				var year = $('#yearPeriod').val();
				var roster = $('#rosterMaster').val();
				var otOffCount = $('#otInOffCount').val();
				// var bioId = $('#idFilter').val(); 
				var myUrl = "";
				if(client == 'Pontil_Sumbawa'){
					myUrl = "<?php echo base_url().'transactions/sumbawa/Timesheet_ptlsmb/rosterProcess' ?>";
				}
				else if(client == 'Pontil_Banyuwangi'){
					myUrl = "<?php echo base_url().'transactions/sumbawa/Timesheet_ptlbwg/rosterProcess' ?>";
				}
				else if(client == 'Trakindo_Sumbawa'){
					myUrl = "<?php echo base_url().'transactions/sumbawa/Timesheet_trksmb/rosterProcessTrksmb' ?>";
				}
				// alert(myUrl);   
				$.ajax({
					method : "POST",
					url	   : myUrl,
					data   : {
						slipId 		 : slipId,
						biodataId    : 'BySlipId',
						clientName   : client,
						dept   		 : dept,
						monthPeriod  : month,
						yearPeriod   : year,
						// rosterMaster : roster,
						otInOffCount : otOffCount
					},
					success : function(data){
						// alert(data);
						$("#loader").hide();
						$('#btnProsesRoster').prop('disabled', false);
	                    $.notify({
			                title: "<h5>Informasi : </h5>",
			                message: "<strong>Data Processed</strong> </br></br> ",
			                icon: '' 
			            },
			            {
			                type: "success",
			                delay: 2000
			            });	
			            slipTable.clear().draw();
		                var dataSrc = JSON.parse(data);                 
		                slipTable.rows.add(dataSrc).draw(false);
					},
					error 	: function(data){
						
						$("#loader").hide();
						$('#btnProsesRoster').prop('disabled', false);
	                    $.notify({
			                title: "<h5>Informasi : </h5>",
			                message: "<strong>"+data+"</strong> </br></br> ",
			                icon: '' 
			            },
			            {
			                type: "warning",
			                delay: 3000
			            });	
					}

				});
			  }else {
			    // console.log("no");
				return false;
			  }
			});


		});
		/* END PROCESS BY ID */
		$('.dataGroup').hide();
		$(".govReg").hide();
        $('.dept').hide();
        $("#clientName").on("change", function(){
            slipTable.clear().draw();
            var cn = $("#clientName").val();
            $(".govReg").hide();                
            $('.dept').hide();
			$('.dataGroup').hide();

            // $('#dataGroup').children('option:not(:first)').remove();
            // $("#dataGroup").append('<option value="A" disabled="" selected="">ALL</option>');

            if(cn == "AMNT_Sumbawa" || cn == "Trakindo_Sumbawa" || cn == "Machmahon_Sumbawa")
            {
                $(".govReg").show();
                if(cn == 'AMNT_Sumbawa' || cn == 'Trakindo_Sumbawa' || cn == "Machmahon_Sumbawa"){
                    $('.dept').show();
                    $('#dept').find('option:not(:first)').remove();
                    if(cn == 'AMNT_Sumbawa')
                    {   
                        $('#dept').find('option:not(:first)').remove();
                        $('#dept').append(new Option("FIXED PLANT", "FIXED PLANT"));
                        $('#dept').append(new Option("LINESMAN", "LINESMAN"));
                        $('#dept').append(new Option("POWER PLANT", "POWER PLANT"));                
                        $('#dept').append(new Option("WELDER PLANT", "WELDER PLANT"));                
                    }
                    else if(cn == 'Trakindo_Sumbawa')
                    {
                        $('#dept').find('option:not(:first)').remove();            
                        $('#dept').append(new Option("CAT RENTAL", "CAT RENTAL"));
                        $('#dept').append(new Option("FACILITY MMA", "FACILITY MMA"));
                        $('#dept').append(new Option("MAN HAUL", "MAN HAUL"));
                        $('#dept').append(new Option("ENGINEERING", "ENGINEERING"));
                    }
                    else if(cn == 'Machmahon_Sumbawa')
                    {
                        $('#dept').find('option:not(:first)').remove();            
                        $('#dept').append(new Option("DRY SEASON", "DRY SEASON"));
                        $('#dept').append(new Option("MEWS", "MEWS"));
                        $('#dept').append(new Option("MINNING PROJECT", "MINNING PROJECT"));
                        $('#dept').append(new Option("PROCESS MAINTENANCE", "PROCESS MAINTENANCE"));
                    }
                } else {
                    $('.dept').hide();
                }
            } else if(cn == 'Pontil_Sumbawa' || cn == 'Pontil_Banyuwangi'){
            	$(".govReg").show();
            	// $('#dataGroup').children('option:not(:first)').remove();
                $('#dataGroup').find('option:not(:first)').remove(); 
                $('#dataGroup option[value="A"]').remove();
            	$('#dataGroup').append(new Option("ALL", "A"));
            	$('#dataGroup').append(new Option("LOCAL", "L"));
                $('#dataGroup').append(new Option("NON LOCAL", "N"));
            	$('.dataGroup').show();
            } else if(cn == 'LCP_Sumbawa'){
            	$(".govReg").show();
            }
        });

        /* START UPDATE BUTTON CLICK */
         $('#slipTable tbody').on( 'click', '.btn_update', function () {
            var data = slipTable.row( $(this).parents('tr') ).data();    
            var slipId = data[0];
            var myUrl = "";
            var clientName = $("#clientName").val();
            var yearPeriod = $("#yearPeriod").val();
            var monthPeriod = $("#monthPeriod").val();
            var myUrl = "<?php echo base_url() ?>"+"masters/Process_Closing/checkClosing/"+clientName+"/"+yearPeriod+"/"+monthPeriod;


            // var closeStatus = '';
            // alert(myUrl);
            $('#savePayroll').prop('disabled', false);
            /*Checking Close Status*/	
            $.ajax({
            	url : myUrl,
            	method : 'POST',
            	data : {
            		cn : clientName,
            		yp : yearPeriod,
            		mp : monthPeriod
            	},
            	success : function(data){
            		// closeStatus = data;
            		// alert(closeStatus);		            
		            if (data == '1'){
		            	$('#savePayroll').prop('disabled', true);
		            	$.notify({
			                title: "<h5>Informasi : </h5>",
			                message: "<strong>Data Sudah Closing, Hubungi Admin</strong> </br></br> ",
			                icon: '' 
			            },
			            {
			                type: "warning",
			                delay: 3000
			            });	
			            // return false;
		            }
            	}
            });

            // alert(closeStatus);
            // return false;


            $(".drillingBonus").hide();
            $(".actManagerBonus").hide();
            $(".contractBonus").hide();
            $(".productionBonus").hide();
            $(".outCamp").hide();
            $(".safetyBonus").hide();
            $(".otherAllowance1").hide(); 
            $(".otherAllowance2").hide();
            $(".invoiceOnly").hide();
            $(".noAccidentFee").hide();
            $(".attendanceBonus").hide();
            $(".oAllowanceTra1").hide();
            $(".oAllowanceTra2").hide();
            $(".kpiBenefits").hide();            
            $(".travelBonus").hide();    

            if(clientName == "Pontil_Sumbawa")
            {
                myUrl = "<?php echo base_url() ?>"+"transactions/sumbawa/Payroll_ptlsmb/getSlipByIid/";
                $(".workAdjustment").show();
                // $(".ccPayment").hide();
                $(".contractBonus").show();
                $(".productionBonus").hide();
                $(".outCamp").show();
                $(".safetyBonus").hide();
                $(".adjustmentIn").hide(); 
                $(".adjustmentOut").hide(); 
                $(".kpiBenefits").hide();
                $(".otherAllowance2").hide();
                $(".attendanceBonus").show();
                $(".travelBonus").show();
                $(".drillingBonus").show();
            	$(".actManagerBonus").show();
            	$(".othrAllowanceTrakindo").hide();
            	$(".otherAllowance2").hide();
            }

            else if(clientName == "Trakindo_Sumbawa")
            {
                myUrl = "<?php echo base_url() ?>"+"transactions/sumbawa/Payroll_trksmb/getSlipByIid/";

                $(".workAdjustment").hide();
                // $(".ccPayment").hide();
                // $(".contractBonus").show();
                $(".productionBonus").hide();
                $(".outCamp").show();
                // $(".safetyBonus").hide();
                $(".adjustmentIn").hide(); 
                $(".adjustmentOut").hide(); 
                $(".kpiBenefits").hide();
                $(".otherAllowance2").show();
                // $(".attendanceBonus").show();
                $(".travelBonus").hide();
                $(".drillingBonus").hide();
            	$(".actManagerBonus").hide();

                // $(".flyingCamp").show();
                $(".attendanceBonus").show();
                $(".othrAllowanceTrakindo").hide();
                $(".thr").show();
                $(".safetyBonus").show();
                $(".nightShiftBonus").hide();
                $(".contractBonus").show();              

            }

            else if(clientName == "Pontil_Banyuwangi")
            {
                myUrl = "<?php echo base_url() ?>"+"transactions/sumbawa/Payroll_ptlbwg/getSlipByIid/";
                $(".workAdjustment").show();
                // $(".ccPayment").hide();
                $(".contractBonus").show();
                $(".productionBonus").hide();
                $(".outCamp").show();
                $(".safetyBonus").hide();
                $(".adjustmentIn").hide(); 
                $(".adjustmentOut").hide(); 
                $(".kpiBenefits").hide();
                $(".otherAllowance2").hide();
                $(".attendanceBonus").show();
                $(".travelBonus").show();
                $(".drillingBonus").show();
            	$(".actManagerBonus").show();

            }

            else if(clientName == "LCP_Sumbawa")
            {
                myUrl = "<?php echo base_url() ?>"+"transactions/sumbawa/Payroll_lcpsmb/getSlipByIid/";
                $(".workAdjustment").hide();
             	$(".adjustmentIn").show(); 
                $(".adjustmentOut").show(); 
                $(".debtBurden").show(); 
                $(".otherAllowance2").hide(); 
                $(".othrAllowanceTrakindo").hide(); 

            }
            // else if($("#clientName").val() == "Machmahon_Sumbawa")
            // {
            //     $(".workAdjustment").hide();
            //     $(".ccPayment").hide();
            //     $(".contractBonus").show();
            //     $(".productionBonus").hide();
            //     $(".outCamp").show();
            //     $(".safetyBonus").show();
            //     $(".adjustmentIn").hide(); 
            //     $(".adjustmentOut").hide(); 
            //     $(".kpiBenefits").show();
            //     $(".otherAllowance2").show();
            //     // $(".invoiceOnly").show();
            //     $(".attendanceBonus").show();
            //     $(".general1").show();/* Mobilization */
            //     $(".general3").show();/* MCU */
            //     $(".general6").show();/* Meals */
            // }

            var dept = $("#dept").val();
            var position = "";

            $("#payrollId").val(slipId);
            $.ajax({
                url : myUrl,
                method : "POST",
                data : {
                    slipId : slipId
                },
                success : function(data) {
                    var dataSrc = JSON.parse(data);

                    $("#payrollName").val(dataSrc[1]); 
                    $("#contractBonus").val(dataSrc[6]); 
                    $("#thr").val(dataSrc[13]); 
                    $("#debtBurden").val(dataSrc[14]); 
                    $("#debtExplanation").val(dataSrc[15]);                     

                    if(clientName == "Pontil_Sumbawa")
            		{
                    	$("#workAdjustment").val(dataSrc[3]); 
                    	$("#outCamp").val(dataSrc[7]); 
                    	$("#travelBonus").val(dataSrc[18]); 
                    	$("#attendanceBonus").val(dataSrc[9]); 
                    	$("#drillingBonus").val(dataSrc[19]); 
                    	$("#actManagerBonus").val(dataSrc[20]); 

            		}
            		else if(clientName == "Trakindo_Sumbawa")
            		{
            			$("#outCamp").val(dataSrc[2]);
            			$("#attendanceBonus").val(dataSrc[3]);
            			$("#otherAllowance2").val(dataSrc[4]);
            			$("#thr").val(dataSrc[5]);
            			$("#safetyBonus").val(dataSrc[6]);
            			$("#nightShiftBonus").val(dataSrc[7]);
            			$("#contractBonus").val(dataSrc[8]);
            		}

            		else if(clientName == "LCP_Sumbawa")
            		{
            			$("#outCamp").val(dataSrc[2]);
            			$("#adjustmentIn").val(dataSrc[4]);; 
                		$("#adjustmentOut").val(dataSrc[5]);; 
            			// $("#attendanceBonus").val(dataSrc[9]);
            			// $("#otherAllowance2").val(dataSrc[4]);
            			// $("#safetyBonus").val(dataSrc[6]);
            			// $("#nightShiftBonus").val(dataSrc[7]);
            			// $("#contractBonus").val(dataSrc[8]);
            			$("#thr").val(dataSrc[13]);
            			$("#debtBurden").val(dataSrc[14]);
            			$("#debtExplanation").val(dataSrc[15]);
            		}

                    // $("#productionBonus").val(dataSrc[2]); 
                    // $("#workAdjustment").val(dataSrc[3]); 
                    // $("#adjustmentIn").val(dataSrc[4]); 
                    // $("#adjustmentOut").val(dataSrc[5]); 
                    // $("#safetyBonus").val(dataSrc[8]); 
                    // $("#otherAllowance1").val(dataSrc[10]); 
                    // $("#otherAllowance2").val(dataSrc[11]); 

                    // $("#kpiBenefits").val(dataSrc[10]); 
                    
                    // $("#oAllowanceTra1").val(dataSrc[10]); 
                    // $("#oAllowanceTra2").val(dataSrc[11]);

                    // $("#noAccidentFee").val(dataSrc[12]);
                    // $("#ccPayment").val(dataSrc[13]); 
                    // $("#general1").val(dataSrc[17]); /*MOBILIZATION*/                     
                    // $("#general2").val(dataSrc[18]); /*SERAGAM*/                    
                    // $("#general3").val(dataSrc[19]); /*MCU*/                    
                    // $("#general4").val(dataSrc[20]); /*Ticket*/                    
                    // $("#general5").val(dataSrc[21]); /*APD*/                    
                    // $("#general6").val(dataSrc[22]); /*Meals*/

                    // position = dataSrc[24]; 
                },
                error : function(data) {
                    $.notify({
		                title: "<h5>Informasi : </h5>",
		                message: "<strong>"+data+"</strong> </br></br> ",
		                icon: '' 
		            },
		            {
		                type: "warning",
		                delay: 3000
		            });	
                }
            });
         });

        // } );
        /* END UPDATE BUTTON CLICK */

        /* START SAVE UPDATE */
        $("#savePayroll").on("click", function(){
            
            var clientName = $("#clientName").val();
            var pi = $("#payrollId").val(); 
            var pn = $("#payrollName").val(); 
            var pb = $("#productionBonus").val(); 
            var wa = $("#workAdjustment").val(); 
            var ai = $("#adjustmentIn").val(); 
            var ao = $("#adjustmentOut").val(); 
            var cb = $("#contractBonus").val();
            var oc = $("#outCamp").val();             
            var sb = $("#safetyBonus").val();             
            var ab = $("#attendanceBonus").val();             
            var oa1 = $("#otherAllowance1").val(); 
            var oa2 = $("#otherAllowance2").val();   
            /* Start Allowance Other Trakindo */  
            var fc = $("#flyingCamp").val();
            var th = $("#thr").val();
            var ns = $("#nightShiftBonus").val();
            // var oat = $("#othrAllowanceTrakindo").val();
            var oat1 = $("#oAllowanceTra1").val();   
            var oat2 = $("#oAllowanceTra2").val(); 
            /* End Allowance Other Trakindo */       
            /* Start Travel Bonus Pontil */ 
            var tb = $("#travelBonus").val();    
            /* End Travel Bonus Pontil */      
            /* Start Invoice Only */
            var general1 = $("#general1").val();    
            var general2 = $("#general2").val();    
            var general3 = $("#general3").val();    
            var general4 = $("#general4").val();    
            var general5 = $("#general5").val();    
            var general6 = $("#general6").val();    
            /* End Invoice Only */
            var na = $("#noAccidentFee").val();             
            var cp = $("#ccPayment").val(); 
            var thr = $("#thr").val();             
            var db = $("#debtBurden").val();             
            var dx = $("#debtExplanation").val();             
            /* Start Allowance KPI */             
            var kb = $("#kpiBenefits").val();             
            /* End Allowance KPI */          
            var dlb = $("#drillingBonus").val();             
            var amb = $("#actManagerBonus").val();             

            var myUrl = "";   
            if(clientName == 'Pontil_Sumbawa'){
            	myUrl = "<?php echo base_url() ?>"+"transactions/sumbawa/Payroll_ptlsmb/updatePayroll";
            }
            if(clientName == 'LCP_Sumbawa'){
            	myUrl = "<?php echo base_url() ?>"+"transactions/sumbawa/Payroll_lcpsmb/updatePayroll";
            }
            else if(clientName == 'Pontil_Banyuwangi'){
            	myUrl = "<?php echo base_url() ?>"+"transactions/sumbawa/Payroll_ptlbwg/updatePayroll";
            }
            else if(clientName == 'Trakindo_Sumbawa'){
            	myUrl = "<?php echo base_url() ?>"+"transactions/sumbawa/Payroll_trksmb/updatePayroll";
            }

            // alert(myUrl);

            /* Start Ajax Insert Is Here */
            $.ajax({
                method : "POST",
                url    : myUrl,
                data   : {
                    clientName : clientName,
                    payrollId : pi,
                    productionBonus : pb,
                    workAdjustment : wa,
                    adjustmentIn : ai,
                    adjustmentOut : ao,
                    outCamp : oc,
                    safetyBonus : sb,
                    attendanceBonus : ab,
                    contractBonus : cb,
                    otherAllowance1 : oa1,
                    otherAllowance2 : oa2,
                    oAllowanceTra1 : oat1,
                    oAllowanceTra2 : oat2,
                    travelBonus : tb,
                    general1 : general1,
                    general2 : general2,
                    general3 : general3,
                    general4 : general4,
                    general5 : general5,
                    general6 : general6,
                    noAccidentFee : na,
                    kpiBenefits : kb,
                    ccPayment : cp,
                    thr : thr, 
                    debtBurden : db,
                    debtExplanation : dx,
                    drillingBonus : dlb,
                    actManagerBonus : amb 
                },
                success : function(data){
                    $.notify({
		                title: "<h5>Informasi : </h5>",
		                message: "<strong>Data has been saved </strong> </br></br> ",
		                icon: '' 
		            },
		            {
		                type: "success",
		                delay: 3000
		            });	
                },
                error   : function(data){
                	$.notify({
		                title: "<h5>Informasi : </h5>",
		                message: "<strong>"+data+"</strong> </br></br> ",
		                icon: '' 
		            },
		            {
		                type: "warning",
		                delay: 3000
		            });	
                }
            });
            /* End Ajax Insert Is Here */
        });
        /* END SAVE UPDATE */

        /* START CONFIRM BOX */
        function confirmDialog(message, handler){
		  $(`<div class="modal fade" id="myModal" role="dialog"> 
		     <div class="modal-dialog"> 
		       <!-- Modal content--> 
		        <div class="modal-content"> 
		           <div class="modal-body" style="padding:10px;"> 
		             <h4 class="text-center">${message}</h4> 
		             <div class="text-center"> 
		               <a class="btn btn-danger btn-yes">yes</a> 
		               <a class="btn btn-default btn-no">no</a> 
		             </div> 
		           </div> 
		       </div> 
		    </div> 
		  </div>`).appendTo('body');
		 
		  //Trigger the modal
		  $("#myModal").modal({
		     backdrop: 'static',
		     keyboard: false
		  });
		  
		   //Pass true to a callback function
		   $(".btn-yes").click(function () {
		       handler(true);
		       $("#myModal").modal("hide");
		   });
		    
		   //Pass false to callback function
		   $(".btn-no").click(function () {
		       handler(false);
		       $("#myModal").modal("hide");
		   });

		   //Remove the modal once it is closed.
		   $("#myModal").on('hidden.bs.modal', function () {
		      $("#myModal").remove();
		   });
		}
        /* END CONFIRM BOX */


	});
</script>
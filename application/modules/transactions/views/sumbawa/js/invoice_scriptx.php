<script type="text/javascript">
    $(document).ready(function(){
        var rowIdx = null;
        var invoiceList = $('#invoiceList').DataTable({
                "paging":   true,
                "ordering": false,
                "info":     false,
                "filter":   true
                // "columnDefs": [{
                //     "targets": -2,
                //     "data": null,
                //     "defaultContent": "<button class='btn btn-primary btn-xs btn_update' data-toggle='modal' data-target='#editPayrollModal'><span class='glyphicon glyphicon-edit'></span></button>"
                // },
                // {
                //     "targets": -1,
                //     "data": null,
                //     "defaultContent": "<button class='btn btn-primary btn-xs btn_print'><span class='glyphicon glyphicon-print'></span></button>"   
                // }]
            });

        // var cn = '';
        // $('.dept').hide();
        // $('#clientName').on('change', function(){
        //     cn = $('#clientName').val();
        //     if(cn == 'AMNT_Sumbawa' || cn == 'Trakindo_Sumbawa' || cn == 'Machmahon_Sumbawa'){
        //         $('.dept').show();
        //     } else {
        //         $('.dept').hide();
        //     }
        // });
        $(".payrollGroup").show();
        $('.dept').hide();
        $('#dept').find('option:not(:first)').remove();
        $('#dataPrint').find('option:not(:first)').remove();
        $('#clientName').on('change', function(){
            var clientName = $('#clientName').val();
            $('#dataPrint').find('option:not(:first)').remove();
            $('.dept').hide();
            $('#dept').find('option:not(:first)').remove();
            if(clientName == 'AMNT_Sumbawa')
            {   
                $('.dept').show();
                $('#dept').find('option:not(:first)').remove();
                $('#dept').append(new Option("FIXED PLANT", "FIXED PLANT"));
                $('#dept').append(new Option("LINESMAN", "LINESMAN"));
                $('#dept').append(new Option("POWER PLANT", "POWER PLANT")); 
                $('#dept').append(new Option("WELDER PLANT", "WELDER PLANT"));     

                $('#dataPrint').find('option:not(:first)').remove();
                $('#dataPrint').append(new Option("AMNT Summary Invoice", "ASI"));
                $('#dataPrint').append(new Option("AMNT Invoice", "AIV"));
                $('#dataPrint').append(new Option("AMNT Receipt Note", "ARN"));                
            }
            else if(clientName == 'Trakindo_Sumbawa')
            {
                $('.dept').hide();
                $('#dept').find('option:not(:first)').remove();            
                $('#dept').append(new Option("CAT RENTAL", "CAT RENTAL"));
                $('#dept').append(new Option("ENGINEERING", "ENGINEERING"));
                $('#dept').append(new Option("MAINTENANCE AND SERVICE", "MAINTENANCE AND SERVICE"));
                $('#dept').append(new Option("MAN HAUL", "MAN HAUL"));
                $('#dept').append(new Option("SAFETY", "SAFETY"));
                $('#dept').append(new Option("WAREHOUSE", "WAREHOUSE"));                

                $('#dataPrint').find('option:not(:first)').remove();            
                $('#dataPrint').append(new Option("Trakindo Summary Invoice", "TSI"));
                $('#dataPrint').append(new Option("Trakindo Payment List", "TPL"));

                // perubahan 2020/03/12

                // $('.dept').show();
                // $('#dept').find('option:not(:first)').remove();            
                // $('#dept').append(new Option("CAT RENTAL", "CAT RENTAL"));
                // $('#dept').append(new Option("FACILITY MMA", "FACILITY MMA"));
                // $('#dept').append(new Option("MAN HAUL", "MAN HAUL"));
                // $('#dept').append(new Option("ENGINEERING", "ENGINEERING"));
                // $('#dataPrint').find('option:not(:first)').remove();            
                // $('#dataPrint').append(new Option("Trakindo Summary Invoice", "TSI"));
                // $('#dataPrint').append(new Option("Trakindo Invoice", "TIV"));
                // $('#dataPrint').append(new Option("Trakindo Receipt Note", "TRN"));
            }
            else if(clientName == 'Machmahon_Sumbawa')
            {
                $('.dept').show();
                $('#dept').find('option:not(:first)').remove();            
                $('#dept').append(new Option("DRY SEASON", "DRY SEASON"));
                $('#dept').append(new Option("MEWS", "MEWS"));
                $('#dept').append(new Option("MINNING PROJECT", "MINNING PROJECT"));
                $('#dept').append(new Option("PROCESS MAINTENANCE", "PROCESS MAINTENANCE"));

                $('#dataPrint').find('option:not(:first)').remove();            
                $('#dataPrint').append(new Option("Machmahon Summary Invoice", "MSI"));
                $('#dataPrint').append(new Option("Machmahon Invoice", "MIV"));
                $('#dataPrint').append(new Option("Machmahon Receipt Note", "MRN"));
            }
            else if(clientName == 'Pontil_Sumbawa')
            {
                $('#dataPrint').find('option:not(:first)').remove();            
                $('#dataPrint').append(new Option("Pontil Summary Invoice", "PSI"));
                $('#dataPrint').append(new Option("Pontil Payment List", "PPL"));
                // $('#dataPrint').append(new Option("Machmahon Receipt Note", "MRN"));
            }

            else if(clientName == 'LCP_Sumbawa')
            {
                $(".payrollGroup").hide();
                $('#dataPrint').find('option:not(:first)').remove();            
                $('#dataPrint').append(new Option("LCP Summary Invoice", "LSI"));
                $('#dataPrint').append(new Option("LCP Payment List", "LPL"));
            }

        });

        var dept = '';
        $('#dept').on('change', function(){
            dept = $('#dept').val();
        });

        $("#printReimbursement").hide();   
        $("#fileToPrintGroup").hide();
        $("#printInvoice").hide();

        $(".errMsg").hide();

        /* START CLICK MAIN BUTTON */
        $("#viewInvoiceList").on("click", function(){            
            var clientName = $("#clientName").val();            
            var yearPeriod = $("#yearPeriod").val();
            var monthPeriod = $("#monthPeriod").val();
            var payrollGroup = $("#payrollGroup").val();
            dept = $('#dept').val();
            var isValid = true;
            if($('#clientName option:selected').text() == "Pilih")
            {
                $("#clientName").focus();
                $("#clientNameErr").show();
                // alert('PT Harus Dipilih ');
                isValid = false;
            } 
            else if($('#yearPeriod option:selected').text() == "Pilih")
            {
                $("#yearPeriod").focus();
                $("#yearPeriodErr").show();
                // alert('Tahun Harus Dipilih ');
                isValid = false;
            }
            else if($('#monthPeriod option:selected').text() == "Pilih")
            {
                $("#monthPeriod").focus();
                $("#monthPeriodErr").show();
                // alert('Bulan Harus Dipilih ');
                isValid = false;
            } 
            
            /*else if($('#payrollGroup option:selected').text() == "Pilih")
            {
                $("#payrollGroup").focus();
                alert('Group Harus Dipilih ');
                isValid = false;
            }*/

            if(isValid == false)
            {return false} 

            var myUrl = '';
            /* Ajax Is Here */
            if(clientName == 'Machmahon_Sumbawa'){
                myUrl = "<?php echo base_url() ?>"+"transactions/sumbawa/Invoice/getDataList/"+clientName+'/'+yearPeriod+'/'+monthPeriod+'/';

            }
            else if(clientName == 'Pontil_Sumbawa'){
                myUrl = "<?php echo base_url() ?>"+"transactions/sumbawa/Invoice_ptlsmb/getDataList/"+clientName+'/'+yearPeriod+'/'+monthPeriod+'/'+payrollGroup+'/';
            }
            else if(clientName == 'LCP_Sumbawa'){
                myUrl = "<?php echo base_url() ?>"+"transactions/sumbawa/Invoice_lcpsmb/getDataList/"+clientName+'/'+yearPeriod+'/'+monthPeriod+'/'+payrollGroup+'/';
            }
            else if(clientName == 'Trakindo_Sumbawa'){
                myUrl = "<?php echo base_url() ?>"+"transactions/sumbawa/Invoice_trksmb/getDataList/"+clientName+'/'+yearPeriod+'/'+monthPeriod+'/';
            }

            // alert(myUrl); 
            $.ajax({
                method  : "POST",
                url : myUrl,
                data    : {
                    pt   : clientName,
                    year : yearPeriod,
                    month: monthPeriod,
                    payrollGroup : payrollGroup,
                    dept : dept 
                },
                success : function(data){
                    // alert(data);
                    invoiceList.clear().draw();
                    var dataSrc = JSON.parse(data);                 
                    invoiceList.rows.add(dataSrc).draw(false);
                },
                error   : function(data){
                    alert(data);
                }   
            });
        });
        /* END CLICK MAIN BUTTON */

        /* START SELECT DATA */
        $("#invoiceList tbody").on("click", "tr", function(){
            var rowData = invoiceList.row( this ).data();
            if ( $(this).hasClass('selected') ) 
            {
                $(this).removeClass('selected');
            }
            else 
            {
                invoiceList.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');                    
            }
            rowIdx = invoiceList.row( this ).index();
            $("#rowIdx").val(rowIdx); 
        });
        /* END SELECT DATA */
        

        


        /* START PRINT DATA */
        $('#printToFile').on('click', function(){
            var clientName = $("#clientName").val();
            var myDept = $("#dept").val();
            var yearPeriod = $("#yearPeriod").val();
            var monthPeriod = $("#monthPeriod").val();
            var payrollGroup = $("#payrollGroup").val();
            var dataPrint = $("#dataPrint").val();

            var isValid = true;            

            if($('#clientName option:selected').text() == "Pilih")
            {
                $("#clientName").focus();
                $('#clientNameErr').show();
                // alert('Client Name cannot be empty ');
                isValid = false;
            } 
            else if($('#yearPeriod option:selected').text() == "Pilih")
            {
                $("#yearPeriod").focus();
                $('#yearPeriodErr').show();
                // alert('Year cannot be empty ');
                isValid = false;
            } 
            else if($('#monthPeriod option:selected').text() == "Pilih")
            {
                $("#monthPeriod").focus();
                $('#monthPeriodErr').show();
                // alert('Month cannot be empty ');
                isValid = false;
            } 
            
            else if($('#payrollGroup option:selected').text() == "Pilih")
            {
                $("#payrollGroup").focus();
                $('#payrollGroupErr').show();
                // alert('Group cannot be empty ');
                isValid = false;
            } 
            else if($('#clientName').val() == "Pontil_Timika")
            {                
                if($('#fileToPrint option:selected').text() == "Pilih")
                {
                    $("#fileToPrint").focus();
                    $('#fileToPrintErr').show();
                    // alert('File cannot be empty');
                    isValid = false;
                }  
            }
            else if($('#dataPrint option:selected').text() == "Pilih")
            {
                $("#dataPrint").focus();
                $('#dataPrintErr').show();
                // alert('Data Print cannot be empty ');
                isValid = false;
            }
            var myUrl = '';
            /* TRAKINDO */

            // Perubahan 2020/03/12
            // if (dataPrint == 'TSI') {
            //     myUrl = "<?php echo base_url() ?>"+"transactions/sumbawa/Invoice/exportInvoiceSummaryTrakindo/"+clientName+"/"+yearPeriod+"/"+monthPeriod+"/"+myDept;
            // }

            // else if (dataPrint == 'TIV') {
            //     myUrl = "<?php echo base_url() ?>"+"transactions/sumbawa/Invoice/exportInvoiceTrakindo/"+clientName+"/"+yearPeriod+"/"+monthPeriod+"/"+myDept;
            // }

            // else if (dataPrint == 'TRN') {
            //     myUrl = "<?php echo base_url() ?>"+"transactions/sumbawa/Invoice/exportReceiveNoteTrakindo/"+clientName+"/"+yearPeriod+"/"+monthPeriod+"/"+myDept;
            // }
            
            if (dataPrint == 'TSI') {
                myUrl = "<?php echo base_url() ?>"+"transactions/sumbawa/Invoice_trksmb/exportInvoiceSummaryTrakindo/"+clientName+"/"+yearPeriod+"/"+monthPeriod;
            }

            else if (dataPrint == 'TPL') {
                myUrl = "<?php echo base_url() ?>"+"transactions/sumbawa/Payroll_trksmb/exportPaymentListTrakindo/"+clientName+"/"+yearPeriod+"/"+monthPeriod;
            }

            /* MACHMAHON */
            else if (dataPrint == 'MSI') {
                
                myUrl = "<?php echo base_url() ?>"+"transactions/sumbawa/Invoice/exportInvoiceSummaryMachmahon/"+yearPeriod+"/"+monthPeriod+"/"+myDept;
            }

            /* PONTIL */
            else if (dataPrint == 'PSI') {
                
                myUrl = "<?php echo base_url() ?>"+"transactions/sumbawa/Invoice_ptlsmb/exportSummaryInvoicePontil/"+clientName+"/"+yearPeriod+"/"+monthPeriod+"/"+payrollGroup;
            }

            else if (dataPrint == 'PPL') {
                
                myUrl = "<?php echo base_url() ?>"+"transactions/sumbawa/Payroll_ptlsmb/exportPaymentList/"+clientName+"/"+yearPeriod+"/"+monthPeriod+"/"+payrollGroup;
            }
            /* LCP Sumbawa */
            else if (dataPrint == 'LSI') {
                
                myUrl = "<?php echo base_url() ?>"+"transactions/sumbawa/Invoice_lcpsmb/exportSummaryInvoiceLcp/"+clientName+"/"+yearPeriod+"/"+monthPeriod+"/"+payrollGroup;
            }
            else if (dataPrint == 'LPL') {
                
                myUrl = "<?php echo base_url() ?>"+"transactions/sumbawa/Payroll_lcpsmb/exportPaymentList/"+clientName+"/"+yearPeriod+"/"+monthPeriod+"/"+payrollGroup;
            }

            /* AMNT */
            else if (dataPrint == 'ASI') {
                var myMethod = '';
                if(dept == 'FIXED PLANT')
                {
                    myMethod = 'exportFixedPlantSummaryAMNT';                    
                }

                if(dept == 'LINESMAN')
                {
                    myMethod = 'exportLMSummaryAMNT';                    
                    // myMethod = 'exportFixedPlantSummaryAMNT';                    
                }

                else if(dept == 'WELDER PLANT')
                {
                    myMethod = 'exportWelderSummaryAMNT';                    
                }

                else if(dept == 'POWER PLANT')
                {
                    myMethod = 'exportPowerPlantSummaryAMNT';                    
                }
                
                
                myUrl = "<?php echo base_url() ?>"+"transactions/sumbawa/Invoice/"+myMethod+"/"+clientName+"/"+yearPeriod+"/"+monthPeriod+"/"+dept;
                // alert(myUrl);
            }
            else if (dataPrint == 'AIV') {
                myUrl = "<?php echo base_url() ?>"+"transactions/sumbawa/Invoice/exportInvoiceAMNT/"+clientName+"/"+yearPeriod+"/"+monthPeriod+"/"+payrollGroup+"/"+dept;
            }
            
            else if (dataPrint == 'ARN') {
                myUrl = "<?php echo base_url() ?>"+"transactions/sumbawa/Invoice/exportReceiveNoteAMNT/"+clientName+"/"+yearPeriod+"/"+monthPeriod+"/"+payrollGroup+"/"+dept;
            }            
            // alert(myUrl);
            if(isValid == false)
            {return false} 
            
            $.ajax({
                method : "POST",
                url : myUrl,
                data : {
                    clientName : clientName,
                    yearPeriod : yearPeriod,
                    monthPeriod : monthPeriod,
                    payrollGroup : payrollGroup 
                },
                // success : function(data){
                //     alert(data)
                // },
                success : function(response){
                    console.log(response);
                    window.open(myUrl,'_blank');
                },                
                error : function(data){
                    alert("Failed");
                }   
            });
        });
        /* END PRINT DATA */
        
    });
</script>
<style type="text/css">
tr.selected {
  background-color: #B0BED9!important;
}
</style>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>

<div class="col-md-12">
  <div class="tile bg-info-bio">
    <!-- <h3 class="tile-title">Barang Masuk</h3> -->
    <div class="tile-body">
      <form class="form-horizontal">
        <div class="form-group row">
          <label class="control-label col-md-2">Biodata ID</label>
           <div class="col-md-4">
            <input class="errMsg form-control" id="bioRecIdPlh" name="bioRecIdPlh" placeholder="Biodata ID" readonly="" required ="">
          </div>     

          <label class="control-label col-md-2">RTE ID</label>
          <div class="col-md-4">
            <input class="errMsg form-control" id="rteNumber" name="rteNumber" placeholder="RTE ID" readonly="" required ="">
          </div>
        </div>

        <div class="form-group row">
          <label class="control-label col-md-2">Demob Status</label>
          <div class="col-md-4">
            <select class="errMsg form-control" id="demobStatus" name="demobStatus" required="">
           
                  <option value="" disabled="" selected="">Select</option>
                  <option value="Active">Active</option>
                  <option value="Blacklist">Blacklist </option>
                  <option value="Contract Completion">Contract Completion </option>
                  <option value="Resignation">Resignation</option>
                  <option value="Termination">Termination</option>
                  <option value="Unfit Location">Unfit Location</option>
                  <option value="Transfer To">Transfer To</option>
                  <option value="Lay Off">Lay Off</option>
                  <option value="Standby">Standby</option>
                  <option value="Passed Away">Passed Away</option>
            
            </select>
          </div>
                  
        </div>

        <div class="tile-footer">
          <input type="button" class="btn btn-warning" id="addBiodata" name="addBiodata" data-toggle="modal" data-target="#contractModal" value="Add">
          <input type="button" class="btn btn-warning" id="saveRteDemob" name="saveRteDemob" value="Save">
          <input type="button" class="btn btn-warning" id="editRteDemob" name="editRteDemob" value="Edit">
        </div>

   <br>
            <h3><code id="dataProses" class="backTransparent"><span></span></code></h3> 
        </div>
      </form>
    </div>
  </div>
</div>

<div class="col-md-14">
  <div class="tile">
    <!-- <h3 class="tile-title">Barang Masuk</h3> -->
    <div class="table-responsive">

       <!-- START DATA TABLE -->
        <!-- <div class="tile-body"> -->
          <table id="demobTable" class="table table-hover table-bordered" class="table table-striped dataTable no-footer" cellspacing="0" width="100%" role="grid" style="width: 100%;">
            <thead class="thead-dark">           
              <tr role="row">
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Biodata Id</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">RTE Number</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Demob Status</th>
              </tr>
            </thead>                    
            <tbody>                      
            </tbody>
          </table>  
         
        </div>

       
    </div>
  </div>
</div><!-- 

 Start salaryModal UPDATE DATA  -->
    <div class="modal fade" id="contractModal" tabindex="-1" role="dialog" aria-labelledby="contractModalLabel"> 
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <!-- Start modal-header -->
          <div class="modal-header">
                      
              <h4 class="modal-title" id="contractModalLabel"></i> Master Demobilization </h4>
          </div>
          <!-- End modal-header -->

          <!-- Start modal-body -->
          <div class="modal-body">
          <!-- START DETAIL BIODATA -->
          <div id="detail" class="panel panel-body panelButtonDetail">
          <!-- END DETAIL BIODATA --> 

          <!-- START Table-Responsive --> 
              <div class="table-responsive">
                <table id="biodataTable" class="table table-hover table-bordered" cellspacing="0" width="100%" role="grid" style="width: 100%;">
                        <thead class="thead-dark">
                            <tr role="row">
                              <th>Biodata ID</th>
                              <th>RTE ID</th>
                            
                        </thead>                    
                       
                </table>  
              </div>
          <!-- END TABLE SALARY -->

          </div>
          <!-- END DETAIL BIODATA -->   
          </div> 
          <!-- End modal-body -->

          <!-- Start modal-footer -->
          <div class="modal-footer">
              <input type="button" class="btn btn-primary btn-sm" id="chooseBiodata" name="chooseBiodata" data-dismiss="modal" value="Choose"> 
              <input type="button" class="btn btn-primary btn-sm" data-dismiss="modal" value="Close"> 
          </div>
          <!-- End modal-footer -->
        </div>
      </div> 
    </div> 
    <!-- End salaryModal UPDATE DATA -->

  </div>      
  <!-- END PANEL BIODATA -->
</div> <!-- /container 

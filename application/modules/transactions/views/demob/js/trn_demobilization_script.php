<script type="text/javascript">   
    $(document).ready(function(){
    var contractId = "";
        /* START BIODATA TABLE */   
        var biodataTable = $('#biodataTable').DataTable({
                "paging":   true,
                "ordering": false,
                "info":     false,
                "filter":   true
        });
        /* START demobTable */   
        var demobTable = $('#demobTable').DataTable({
                "paging":   true,
                "ordering": false,
                "info":     false,
                "filter":   true,
                "columnDefs"     : [{
                // "targets": -2,
                    // "data": null,
                    // "defaultContent": "<button class='btn btn-primary btn-xs btn_update' data-toggle='modal' data-target='#editPayrollModal'><span class='glyphicon glyphicon-edit'></span></button>"
                },

                {

                    "targets": -1,
                    "data": null,
                    "defaultContent": "<button class='btn btn-warning btn-xs btn_delete'><i class='fa fa-close'></i></button>"   
                }]
                
            });
        /* END BIODATA TABLE */  

         /* START DETAIL GLOBAL */   
        function clearInputs()
        {
            $('.contract_input :input').each(function(){
                $(this).val('');
            });            
        }
        /* END DETAIL GLOBAL */ 


        $("#editPlh").attr('disabled',true);
        // $("#addBiodata").attr('disabled',true);

        /* START LOAD DATA FROM DATABASE TO TABLE */
        $.ajax({
            method : "POST",
             url : "<?php echo base_url() ?>"+"transactions/demob/Trn_demobilization/loadData", 
             // url : "<?php #echo base_url() ?>"+"masters/Mst_plh/loadPlhData", 
            data : {
                bioRecId :""
            },
            success : function(data){
                demobTable.clear().draw();
                var dataSrc = JSON.parse(data); 
                demobTable.rows.add(dataSrc).draw(false);
                // alert("Succeed");    
            },
            error : function(){
                alert("Failed Load Data");
            }
        });
        /* END LOAD DATA FROM DATABASE TO TABLE */

         /* START SELECT BROWSE DATA */
        var bioRecId   = "";
        var fullName   = "";
        var position   = "";
        var clientName = "";
        var rowData = null;
        $('#biodataTable tbody').on( 'click', 'tr', function () {
            var rowData = biodataTable.row( this ).data();
            if ( $(this).hasClass('selected') ) 
            {
                $(this).removeClass('selected');
            }
            else 
            {
                biodataTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');                    
            }
            bioRecId   = rowData[0];
            fullName   = rowData[1];
            position   = rowData[2];
            clientName = rowData[3];
        }); 
        /* END SELECT BROWSE DATA */

        $("#chooseBiodata").on("click", function(){
            $("#bioRecId").val(bioRecId);
            $("#fullName").val(fullName);
            $("#position").val(position);
            $("#clientName").val(clientName);
        });

        // var client  = "<?php #echo $this->session->userdata('hris_user_group'); ?>";
        // $.ajax({
        //     url : "<?php #echo base_url() ?>"+"masters/Mst_client/loadAll",
        //     method : "POST",
        //     async : false,
        //     dataType : 'json',
        //     success: function(data){
        //     var html = '';
        //     var i;
        //         if(client=='Pusat'){
        //             html += '<option value="" disabled="" selected="">Select</option>';
        //             for(i=0; i<data.length; i++){
        //               html += '<option value="'+data[i].client_value+'" >'+data[i].client_name+'</option>';
        //             }
        //         }else{
        //             html += '<option value="'+client+'" >'+client+'</option>';
        //         }
                    
        //     $('#ptName').html(html);
        //     }
        // });

        /* START SAVE DATA */
        $("#saveDemob").on("click", function(){
            $(".errMsg").css({"border": "2px solid #ced4da"});     
            // debugger
            var bioRecId         = $("#bioRecId").val();
            var fullName         = $("#fullName").val();           
            var position         = $("#position").val();           
            var clientName       = $("#clientName").val();
            var demobDate        = $("#demobDate").val();
            var demobSite        = $("#demobSite").val();
            var demobStatus      = $('#demobStatus').val();
            var remarks          = $('#remarks').val();              
            
            var isValid = true;
            
            if($("#bioRecId").val() == "")
            {
                isValid = false;
                $("#bioRecId").css({"border": "2px solid red"});    
                $("#bioRecId").focus(); 
            }
            else if($('#fullName').val() == "")
            {
                isValid = false;
                $("#fullName").css({"border": "2px solid red"}); 
                $('#fullName').focus();
            } 
            else if($('#position').val() == "")
            {
                isValid = false;
                $("#position").css({"border": "2px solid red"}); 
                $('#position').focus();
            } 
            else if($("#clientName option:selected").text() == "Select")
            {
                isValid = false;
                $("#clientName").css({"border": "2px solid red"});    
                $("#clientName").focus(); 
            }
            
            else if($('#demobDate').val() == "")
            {
                isValid = false;
                $("#demobDate").css({"border": "2px solid red"}); 
                $('#demobDate').focus();
            }
            else if($("#demobSite option:selected").text() == "Select")
            {
                isValid = false;
                $("#demobSite").css({"border": "2px solid red"}); 
                $('#demobSite').focus();
            }
            else if($("#demobStatus option:selected").text() == "Select")
            {
                isValid = false;
                $("#demobStatus").css({"border": "2px solid red"}); 
                $('#demobStatus').focus();
            }
            else if($('#remarks').val() == "")
            {
                isValid = false;
                $("#remarks").css({"border": "2px solid red"}); 
                $('#remarks').focus();
            }
            

            if(isValid == false)
            {
                return false;
            }

            /* START LOAD DATA FROM DATABASE TO TABLE */
            var myUrl = "<?php echo base_url() ?>"+"transactions/demob/Trn_demobilization/ins";
            $.ajax({
                method : "POST",
                url : myUrl, 
                data : {

                    bioRecId       : bioRecId,       
                    fullName       : fullName,
                    position       : position,
                    clientName     : clientName,       
                    demobDate      : demobDate,
                    demobSite      : demobSite,
                    demobStatus    : demobStatus,
                    remarks        : remarks
                    
                },
                success : function(data){
                    alert("Data has been saved");
                    location.reload();
                },
                error : function(data){
                    isValid = false;
                    alert("Failed save data");
                }
            });
            /* END LOAD DATA FROM DATABASE TO TABLE */

            if(isValid == false)
            {
                return false;
            }


            /* START ADD DATA TO DATABASE */
            /* END ADD DATA TO DATABASE */
        });
        /* END SAVE DATA */

        /* START SELECT CONTRACT DATA */
        var rowIdx = 0;
        var rowContractt = null;
        $('#demobTable tbody').on( 'click', 'tr', function () {
            var rowContractt = demobTable.row( this ).data();
            if ( $(this).hasClass('selected') ) 
            {
                $(this).removeClass('selected');
            }
            else 
            {
                demobTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');                    
            }
            bioRecId = rowContractt[0];
            
            $("#bioRecId").attr('disabled',true);
            $("#fullName").attr('disabled',true);
            $("#position").attr('disabled',true);
            $("#clientName").attr('disabled',true);
            $("#demobDate").attr('disabled',false);
            $("#demobSite").attr('disabled',true);
            $("#demobStatus").attr('disabled',false);
            $("#remarks").attr('disabled',false);
            $("#saveDemob").attr('disabled',true);
            $("#addBiodata").attr('disabled',true);
            $("#editPlh").attr('disabled',false);

            rowIdx        = demobTable.row( this ).index();
            bioRecId      = rowContractt['0'];
            fullName      = rowContractt['1'];
            position      = rowContractt['2'];
            clientName    = rowContractt['3'];
            demobDate     = rowContractt['4'];
            demobSite     = rowContractt['5'];
            demobStatus   = rowContractt['6'];
            remarks       = rowContractt['7'];
            
            $('#bioRecId').val(bioRecId);
            $('#fullName').val(fullName);
            $('#position').val(position);
            $('#clientName').val(clientName);
            $('#demobDate').val(demobDate);
            $('#demobSite').val(demobSite);
            $('#demobStatus').val(demobStatus);
            $('#remarks').val(remarks);

        }); 

        /* START LOAD BIODATA  */
        $("#addBiodata").on("click", function(){
            $("#bioRecId").val("");
            $.ajax({
                method : "POST",
                url : "<?php echo base_url() ?>"+"transactions/demob/Trn_demobilization",
                data : {
                    id :""
                },
                success : function(data){
                    biodataTable.clear().draw();
                    var dataSrc = JSON.parse(data); 
                    biodataTable.rows.add(dataSrc).draw(false); 
                },
                error : function(){
                    alert("Failed");
                }
            });
        });
        /* END LOAD BIODATA  */

        /* START DELETE */
        var is_valid = false;   
        $("#demobTable").on('click', '.btn_delete', function(){
            var idDelete = bioRecId; 
            if(idDelete == "")
            {
                alert("Please select data first");
                return false;
            }

            is_valid = confirm('Data : '+idDelete+' will be deleted?');
            if (is_valid) 
            {
                idDelete = "";
                demobTable.row('.selected').remove().draw( false );

                /* START AJAX DELETE */
                $.ajax({
                    method : "POST",
                    url : "<?php echo base_url() ?>"+"transactions/demob/Trn_demobilization/del", 
                    data : {
                        idDelete : bioRecId
                    },
                    success : function(data){
                        alert('Data has been deleted');
                        location.reload();
                    },
                    error : function(){
                        alert("Delete data failed");
                    }

                });
                /* END AJAX DELETE */
            }
        });
              
        /* END DELETE */ 

        /* START SELECT BROWSE DATA */
        var bioRecId   = "";
        var fullName   = "";
        var position   = "";
        var clientName = "";
        var rowData = null;
        $('#demobTable tbody').on( 'click', 'tr', function () {
            var rowData = biodataTable.row( this ).data();
            if ( $(this).hasClass('selected') ) 
            {
                $(this).removeClass('selected');
            }
            else 
            {
                biodataTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');                    
            }
            bioRecId   = rowData[0];
            fullName   = rowData[1];
            position   = rowData[2];
            clientName = rowData[3];

        }); 
        /* END SELECT BROWSE DATA */

        $("#chooseBiodata").on("click", function(){
            $("#bioRecId").val(bioRecId);
            $("#fullName").val(fullName);
            $("#position").val(position);
            $("#clientName").val(clientName);
            
        });

        /* START EDIT CONTRACT */
        $("#editPlh").on("click", function(){
        $(".errMsg").css({"border": "2px solid #ced4da"});
        // debugger
            // var contractId = $("#contractId").val();
            var bioRecId     = $("#bioRecId").val();
            var fullName     = $('#fullName').val();
            var position     = $('#position').val();
            var clientName   = $('#clientName').val();
            var demobDate    = $('#demobDate').val();
            var demobSite    = $('#demobSite').val();
            var demobStatus  = $('#demobStatus').val();
            var remarks      = $("#remarks").val(); 

            var isValid      = true;
            

            if($('#bioRecId').val() == "")
            {
                isValid = false;
                $("#bioRecId").css({"border": "2px solid red"}); 
                $('#bioRecId').focus();
            } 
            else if($("#fullName").val() == "")
            {
                isValid = false;
                $("#fullName").css({"border": "2px solid red"});    
                $("#fullName").focus(); 
            }
            else if($("#position").val() == "")
            {
                isValid = false;
                $("#position").css({"border": "2px solid red"});    
                $("#position").focus(); 
            }
            else if($("#clientName option:selected").val() == "Select")
            {
                isValid = false;
                $("#clientName").css({"border": "2px solid red"});    
                $("#clientName").focus(); 
            }           
            else if($('#demobDate').val() == "")
            {
                isValid = false;
                $("#demobDate").css({"border": "2px solid red"}); 
                $('#demobDate').focus();
            }
            else if($("#demobSite option:selected").val() == "Selected")
            {
                isValid = false;
                $("#demobSite").css({"border": "2px solid red"}); 
                $('#demobSite').focus();
            }
            else if($("#demobStatus option:selected").val() == "Selected")
            {
                isValid = false;
                $("#demobStatus").css({"border": "2px solid red"}); 
                $('#demobStatus').focus();
            }
            else if($('#remarks').val() == "")
            {
                isValid = false;
                $("#remarks").css({"border": "2px solid red"}); 
                $('#remarks').focus();
            }
           

            if(isValid == false)
            {
                return false;
            }
           
            /* START LOAD DATA FROM DATABASE TO TABLE */
            var myUrl = "<?php echo base_url() ?>"+"transactions/demob/Trn_demobilization/Upd"; 
            $.ajax({
                method : "POST",
                url : myUrl, 
                data : {
                    bioRecId    : bioRecId,
                    fullName    : fullName,                 
                    position    : position,                 
                    clientName  : clientName,                 
                    demobDate   : demobDate,                 
                    demobSite   : demobSite,
                    demobStatus : demobStatus,
                    remarks     : remarks              
                },
                success : function(data){
                    /* START ADD DATA TO TABLE */
                    // clearInputs();
                    location.reload();               
                    alert("Data has been edited");
                    /* END ADD DATA TO TABLE */
                },
                error : function(data){
                    isValid = false;
                    alert("Failed Edit data");
                }
            });
            /* END LOAD DATA FROM DATABASE TO TABLE */
          

        });
        // END EDIT PLH

    });
</script>
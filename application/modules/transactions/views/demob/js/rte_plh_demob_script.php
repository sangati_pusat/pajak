<script type="text/javascript">   
    $(document).ready(function(){
    var contractId = "";
        /* START BIODATA TABLE */   
        var biodataTable = $('#biodataTable').DataTable({
                "paging":   true,
                "ordering": false,
                "info":     false,
                "filter":   true
        });
        /* START demobTable */   
        var demobTable = $('#demobTable').DataTable({
                "paging":   true,
                "ordering": false,
                "info":     false,
                "filter":   true
                
            });
        /* END BIODATA TABLE */  

         /* START DETAIL GLOBAL */   
        function clearInputs()
        {
            $('.contract_input :input').each(function(){
                $(this).val('');
            });            
        }
        /* END DETAIL GLOBAL */ 


        $("#editRteDemob").attr('disabled',true);
        // $("#addBiodata").attr('disabled',true);

        /* START LOAD DATA FROM DATABASE TO TABLE */
        $.ajax({
            method : "POST",
             url : "<?php echo base_url() ?>"+"transactions/demob/Trn_rte_plh_demob/loadData", 
             // url : "<?php #echo base_url() ?>"+"masters/Mst_plh/loadPlhData", 
            data : {
                bioRecId :""
            },
            success : function(data){
                demobTable.clear().draw();
                var dataSrc = JSON.parse(data); 
                demobTable.rows.add(dataSrc).draw(false);
                // alert("Succeed");    
            },
            error : function(){
                alert("Failed Load Data");
            }
        });
        /* END LOAD DATA FROM DATABASE TO TABLE */

         /* START SELECT BROWSE DATA */
        var bioRecIdPlh   = "";
        var rteNumber     = "";
        
        var rowData = null;
        $('#biodataTable tbody').on( 'click', 'tr', function () {
            var rowData = biodataTable.row( this ).data();
            if ( $(this).hasClass('selected') ) 
            {
                $(this).removeClass('selected');
            }
            else 
            {
                biodataTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');                    
            }
            bioRecIdPlh   = rowData[0];
            rteNumber     = rowData[1];
            
        }); 
        /* END SELECT BROWSE DATA */

        $("#chooseBiodata").on("click", function(){
            $("#bioRecIdPlh").val(bioRecIdPlh);
            $("#rteNumber").val(rteNumber);
            
        });

        /* START SAVE DATA */
        $("#saveRteDemob").on("click", function(){
            $(".errMsg").css({"border": "2px solid #ced4da"});     
            // debugger
            var bioRecIdPlh         = $("#bioRecIdPlh").val();
            var rteNumber           = $("#rteNumber").val();           
            var demobStatus         = $("#demobStatus").val();           
               
            var isValid = true;
            
            if($("#bioRecIdPlh").val() == "")
            {
                isValid = false;
                $("#bioRecIdPlh").css({"border": "2px solid red"});    
                $("#bioRecIdPlh").focus(); 
            }
            else if($('#rteNumber').val() == "")
            {
                isValid = false;
                $("#rteNumber").css({"border": "2px solid red"}); 
                $('#rteNumber').focus();
            } 
            else if($("#demobStatus option:selected").text() == "Select")
            {
                isValid = false;
                $("#demobStatus").css({"border": "2px solid red"}); 
                $('#demobStatus').focus();
            } 
                     

            if(isValid == false)
            {
                return false;
            }

            /* START LOAD DATA FROM DATABASE TO TABLE */
            var myUrl = "<?php echo base_url() ?>"+"transactions/demob/Trn_rte_plh_demob/ins";
            $.ajax({
                method : "POST",
                url : myUrl, 
                data : {

                    bioRecIdPlh       : bioRecIdPlh,       
                    rteNumber         : rteNumber,
                    demobStatus       : demobStatus
                    
                    
                },
                success : function(data){
                    alert("Data has been saved");
                    location.reload();
                },
                error : function(data){
                    isValid = false;
                    alert("Failed save data");
                }
            });
            /* END LOAD DATA FROM DATABASE TO TABLE */

            if(isValid == false)
            {
                return false;
            }


            /* START ADD DATA TO DATABASE */
            /* END ADD DATA TO DATABASE */
        });
        /* END SAVE DATA */

        /* START SELECT CONTRACT DATA */
        var rowIdx = 0;
        var rowContractt = null;
        $('#demobTable tbody').on( 'click', 'tr', function () {
            var rowContractt = demobTable.row( this ).data();
            if ( $(this).hasClass('selected') ) 
            {
                $(this).removeClass('selected');
            }
            else 
            {
                demobTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');                    
            }
            bioRecIdPlh = rowContractt[0];

            $("#bioRecIdPlh").attr('disabled',true);
            $("#rteNumber").attr('disabled',true);
            $("#demobStatus").attr('disabled',false);
            $("#saveRteDemob").attr('disabled',true);
            $("#addBiodata").attr('disabled',true);
            $("#editRteDemob").attr('disabled',false);

            rowIdx        = demobTable.row( this ).index();
            bioRecIdPlh   = rowContractt['0'];
            rteNumber     = rowContractt['1'];
            demobStatus   = rowContractt['2'];
            
            
            $('#bioRecIdPlh').val(bioRecIdPlh);
            $('#rteNumber').val(rteNumber);
            $('#demobStatus').val(demobStatus);    

        }); 

        /* START LOAD BIODATA  */
        $("#addBiodata").on("click", function(){
            $("#bioRecIdPlh").val("");
            $.ajax({
                method : "POST",
                url : "<?php echo base_url() ?>"+"transactions/demob/Trn_rte_plh_demob",
                data : {
                    id :""
                },
                success : function(data){
                    biodataTable.clear().draw();
                    var dataSrc = JSON.parse(data); 
                    biodataTable.rows.add(dataSrc).draw(false); 
                },
                error : function(){
                    alert("Failed");
                }
            });
        });
        /* END LOAD BIODATA  */

        /* START DELETE */
        var is_valid = false;   
        $("#demobTable").on('click', '.btn_delete', function(){
            var idDelete = bioRecId; 
            if(idDelete == "")
            {
                alert("Please select data first");
                return false;
            }

            is_valid = confirm('Data : '+idDelete+' will be deleted?');
            if (is_valid) 
            {
                idDelete = "";
                demobTable.row('.selected').remove().draw( false );

                /* START AJAX DELETE */
                $.ajax({
                    method : "POST",
                    url : "<?php echo base_url() ?>"+"transactions/demob/Trn_demob/del", 
                    data : {
                        idDelete : bioRecId
                    },
                    success : function(data){
                        alert('Data has been deleted');
                        location.reload();
                    },
                    error : function(){
                        alert("Delete data failed");
                    }

                });
                /* END AJAX DELETE */
            }
        });
              
        /* END DELETE */ 

        /* START SELECT BROWSE DATA */
        var bioRecIdPlh   = "";
        var rteNumber     = "";
        // var demobStatus   = "";
        var rowData       = null;
        $('#demobTable tbody').on( 'click', 'tr', function () {
            var rowData = biodataTable.row( this ).data();
            if ( $(this).hasClass('selected') ) 
            {
                $(this).removeClass('selected');
            }
            else 
            {
                biodataTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');                    
            }
            bioRecIdPlh   = rowData[0];
            rteNumber     = rowData[1];         
            // demobStatus   = rowData[2];         

        }); 
        /* END SELECT BROWSE DATA */

        $("#chooseBiodata").on("click", function(){
            $("#bioRecIdPlh").val(bioRecIdPlh);
            $("#rteNumber").val(rteNumber);
            // $("#demobStatus").val(demobStatus);
            
            
        });

        /* START EDIT CONTRACT */
        $("#editRteDemob").on("click", function(){
        $(".errMsg").css({"border": "2px solid #ced4da"});
        // debugger
            // var contractId = $("#contractId").val();
            var bioRecIdPlh  = $("#bioRecIdPlh").val();
            var rteNumber    = $('#rteNumber').val();
            var demobStatus  = $('#demobStatus').val();

            var isValid      = true;
            

            if($('#bioRecIdPlh').val() == "")
            {
                isValid = false;
                $("#bioRecIdPlh").css({"border": "2px solid red"}); 
                $('#bioRecIdPlh').focus();
            } 
            else if($("#rteNumber").val() == "")
            {
                isValid = false;
                $("#rteNumber").css({"border": "2px solid red"});    
                $("#rteNumber").focus(); 
            }
            else if($("#demobStatus").val() == "")
            {
                isValid = false;
                $("#demobStatus").css({"border": "2px solid red"});    
                $("#demobStatus").focus(); 
            }
            

            if(isValid == false)
            {
                return false;
            }
           
            /* START LOAD DATA FROM DATABASE TO TABLE */
            var myUrl = "<?php echo base_url() ?>"+"transactions/demob/Trn_rte_plh_demob/Upd"; 
            $.ajax({
                method : "POST",
                url : myUrl, 
                data : {
                    bioRecIdPlh : bioRecIdPlh,
                    rteNumber   : rteNumber,                 
                    demobStatus : demobStatus             
                },
                success : function(data){
                    /* START ADD DATA TO TABLE */
                    // clearInputs();
                    location.reload();               
                    alert("Data has been edited");
                    /* END ADD DATA TO TABLE */
                },
                error : function(data){
                    isValid = false;
                    alert("Failed Edit data");
                }
            });
            /* END LOAD DATA FROM DATABASE TO TABLE */
          

        });
        // END EDIT PLH

    });
</script>
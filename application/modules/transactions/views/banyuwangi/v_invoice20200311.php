<!-- <div id="loader"></div> -->

<!--START FORM DATA -->
<div class="col-md-12" id="is_transaction">
  <div class="tile bg-info">
    <!-- <h3 class="tile-title">Barang Masuk</h3> -->
    <div class="tile-body">
      <form class="row is_header">
      <!-- <form class="row is_header" action="<?php #echo base_url() ?>transactions/sumbawa/Timesheet/payrollProcess"> -->
        
        <div class="form-group col-sm-12 col-md-2">
          <label class="control-label">CLIENT</label>
          <code id="docKindErr" class="errMsg"><span> : Required</span></code>
          <select class="form-control" id="clientName" name="clientName" required="">
    				<option value="" disabled="" selected="">Pilih</option>
    				<!-- <option value="Pontil_Timika">Pontil Timika</option> -->
    				<!-- <option value="Redpath_Timika">Redpath Timika</option> -->
    				<!-- <option value="Agincourt_Martabe">Agincourt Martabe</option> -->
    				<!-- <option value="AMNT_Sumbawa">AMNT Sumbawa </option> -->
    				<!-- <option value="AMNT_Sumbawa_Staff">Staff AMNT Sumbawa </option> -->
    				<!-- <option value="Trakindo_Sumbawa">Trakindo Sumbawa</option> -->
    				<!-- <option value="Machmahon_Sumbawa">Machmahon Sumbawa</option> -->
            <option value="Pontil_Sumbawa">Pontil Banyuwangi</option>
    				<!-- <option value="LCP_Sumbawa">Lamurung Cipta Persada</option> -->
    		  </select>
        </div>

        <div class="form-group col-sm-12 col-md-2">
          <label class="control-label">YEAR</label>
          <code id="docKindErr" class="errMsg"><span> : Required</span></code>
          <select class="form-control" id="yearPeriod" name="yearPeriod" required="">
      			<option value="" disabled="" selected="">Pilih</option>
      			<script type="text/javascript">
      				var dt = new Date();
      				var currYear = dt.getFullYear();
      				var currMonth = dt.getMonth();
                      var currDay = dt.getDate();
                      var tmpDate = new Date(currYear + 1, currMonth, currDay);
                      var startYear = tmpDate.getFullYear();
      				var endYear = startYear - 80;							
      				for (var i = startYear; i >= endYear; i--) 
      				{
      					document.write("<option value='"+i+"'>"+i+"</option>");						
      				}
      			</script>
      		</select>
        </div>

        <div class="form-group col-sm-12 col-md-2">
          <label class="control-label">MONTH</label>
          <code id="docKindErr" class="errMsg"><span> : Required</span></code>
          <select class="form-control" id="monthPeriod" name="monthPeriod" required="">
    				<option value="" disabled="" selected="">Pilih</option>
    				<script type="text/javascript">
    					var tMonth = 1;
    					for (var i = tMonth; i <= 12; i++) 
    					{
    						if(i < 10)
    						{
    							document.write("<option value='0"+i+"'>0"+i+"</option>");							
    						}
    						else
    						{
    							document.write("<option value='"+i+"'>"+i+"</option>");								
    						}
    						
    					}

    				</script>
    			</select>
        </div>

        <div class="form-group col-sm-12 col-md-2">
    			<label class="control-label" for="dataPrint">DATA PRINT</label>	
          		<code id="docKindErr" class="errMsg"><span> : Required</span></code>
    			<select class="form-control" id="dataPrint" name="dataPrint" required="">
    				<option value="" selected="" disabled="">Pilih</option>				
    			</select>
    		</div>

        <div class="form-group col-sm-12 col-md-2 dept">
          <label class="control-label">DEPT </label>
          <code id="docKindErr" class="errMsg"><span> : Required</span></code>
          <select class="form-control" id="dept" name="dept" required="">
            <option value="" disabled="" selected="">Pilih</option>
            
          </select>
        </div>

        <div class="form-group col-sm-12 col-md-2 payrollGroup">
          <label class="control-label">Group </label>
          <code id="payrollGroupErr" class="errMsg"><span> : Required</span></code>
          <select class="form-control" id="payrollGroup" name="payrollGroup" required="">
            <option value="L" selected="">LOCAL</option>
            <option value="N">NON LOCAL</option>            
          </select>
        </div>

        <div class="form-group col-sm-12 col-md-1">
          <label class="control-label" for="startDate">START</label>
          <select class="form-control" id="startDate" name="startDate" required="">
            <!-- <option value="" disabled="" selected="">Pilih</option> -->
            <script type="text/javascript">
              var tDay = 1;
              for (var i = tDay; i <= 31; i++) 
              {
                document.write("<option value='"+i+"'>"+i+"</option>");               
              }
            </script>
          </select> 
        </div>
            
        <div class="form-group col-md-12 align-self-end">
			<button class="btn btn-warning btnProcessPanel" type="button" id="viewInvoiceList" name="viewInvoiceList">
	        		<span class="fa fa-list"></span> DISPLAY DATA
        	</button>
			<button class="btn btn-warning btnProcessPanel" type="button" id="printToFile" name="printToFile">
        		<span class="fa fa-print"></span> PRINT
        	</button>
		    <br>
		      <!-- <br> -->
    	  <h3><code id="dataProses" class="backTransparent"><span></span></code></h3>	
        </div>
      </form>
    </div>
  </div>
</div>
<!-- END FORM DATA -->

<div class="col-md-12">
  <div class="tile bg-info">
    <!-- <h3 class="tile-title">Barang Masuk</h3> -->
    <div class="tile-body">
       <!-- START DATA TABLE -->
        <div class="tile-body">
          <table class="table table-hover table-bordered" id="invoiceList">
            <thead class="thead-dark">
              <tr>
                <th>Slip Id</th>
                <th>Client Name</th>
                <!-- <th>Badge No</th> -->
                <th>Emp Name</th>
                <th>Dept</th>
                <th>Position</th>
                <!-- <th>Update</th> -->
                <!-- <th>Print</th> -->
              </tr>
            </thead>
            <tbody>
              <!-- <tr>
                <td>Tiger Nixon</td>
                <td>System Architect</td>
                <td>Edinburgh</td>
                <td>61</td>
                <td>2011/04/25</td>
                <td>$320,800</td>
              </tr>  -->       
            </tbody>
          </table>
        </div>
        <!-- END DATA TABLE -->
    </div>
  </div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
  		$("#loader").hide();
  		$("#myDiv").hide();

		$('#uploadSPT').on("click", function(){
			$("#loader").show();
  			$("#myDiv").hide();
			$('#uploadSPT').prop('disabled', true);
			var formData = new FormData($("#sptUploadForm")[0]);
			var myUrl = "<?php echo base_url() ?>"+"transactions/spt/SptUpload/upload/";
			// alert(myUrl);
			$.ajax({
			    url: myUrl,
			    type: "POST",
			    data : formData,
			    processData: false,
			    contentType: false,
			    beforeSend: function() {
			    },
			    success: function(data){
					$("#myDiv").show();
					$("#loader").hide();
					$('#uploadSPT').prop('disabled', false);
                    // alert(data);
                    $.notify({
		                title: "<h5>Informasi : </h5>",
		                message: "<strong>"+data+"</strong> </br></br> ",
		                icon: '' 
		            },
		            {
		                type: "success",
		                delay: 3000
		            });	
			    },
			    error: function(data){
					// alert(data);
					$.notify({
		                title: "<h5>Informasi : </h5>",
		                message: "<strong>"+data+"</strong> </br></br> ",
		                icon: '' 
		            },
		            {
		                type: "success",
		                delay: 3000
		            });
			    }
			    /*error: function(xhr, ajaxOptions, thrownError) {
			       console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			    }*/
			});

		});


	});
</script>
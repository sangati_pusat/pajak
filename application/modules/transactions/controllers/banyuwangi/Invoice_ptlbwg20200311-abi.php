<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    // Load library phpspreadsheet
    require('./vendor/autoload.php');
    use PhpOffice\PhpSpreadsheet\Helper\Sample;
    use PhpOffice\PhpSpreadsheet\IOFactory;
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    // End load library phpspreadsheet

class Invoice_ptlbwg extends CI_Controller {
	
	public function index()
	{
		$this->load->view('welcome_message');
	}

    /* START PONTIL INVOICE LIST EXPORT */
    public function exportSummaryInvoicePontil($clientName, $yearPeriod, $monthPeriod, $group)
    {
        //membuat objek
        // $objPHPExcel = new PHPExcel();
        // test($group,1);
        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();  

        $spreadsheet->getProperties()->setCreator('Maurice - Web - Android')
            ->setLastModifiedBy('Maurice - Web - Android')
            ->setTitle('Office 2007 XLSX Test Document')
            ->setSubject('Office 2007 XLSX Test Document')
            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Test result file');

        if (file_exists('assets/images/report_logo.png')) {
            $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing->setName('Logo');
            $drawing->setDescription('Logo');
            $drawing->setPath('./assets/images/report_logo.png');
            $drawing->setCoordinates('A1');
            $drawing->setHeight(38);
            $drawing->setWorksheet($spreadsheet->getActiveSheet());
        }

        $strSQL = "";
        $strFilter = "";
        if($group != 'A')
        {
            $strFilter = " AND ss.payroll_group = '".$group."' ";    
        }
        // $dataGroup = '';
        // if($group == 'L'){
        //     $dataGroup = 'LOCAL';
        // }else if($group == 'N'){
        //     $dataGroup = 'NON LOCAL';
        // }

        $strSQL  = " SELECT ";   
        $strSQL .= "   ms.bio_rec_id, ms.nie, ss.name, ss.position job_desc, ss.basic_salary,ss.bs_prorate, ss.act_manager_bonus,";
        $strSQL .= "   (ss.bs_prorate - unpaid_total) current_salary, ";
        $strSQL .= "   TRUNCATE((ss.basic_salary/173),1) rate, ";
        $strSQL .= "   ss.normal_time, ss.ot_count1, ss.ot_count2, ss.ot_count3, ss.ot_count4, ";
        $strSQL .= "   ss.ot_1, ss.ot_2, ss.ot_3, ss.ot_4, ";
        $strSQL .= "   (ss.normal_time+ss.ot_count1+ss.ot_count2+ss.ot_count3+ss.ot_count4) worked_hours, ss.travel_bonus, ss.attendance_bonus,ss.flying_camp,";
        $strSQL .= "   CASE WHEN(ss.normal_time+ss.ot_count1+ss.ot_count2+ss.ot_count3+ss.ot_count4) <= 312 THEN ";
        $strSQL .= "      TRUNCATE((ss.normal_time+ss.ot_count1+ss.ot_count2+ss.ot_count3+ss.ot_count4) * TRUNCATE((ss.basic_salary/173),1) * (64/100),2) ";
        $strSQL .= "   ELSE TRUNCATE(312 * TRUNCATE((ss.basic_salary/173),1) * (64/100),2) END uplift, ";
        $strSQL .= "    TRUNCATE( (ss.normal_time)+(ss.ot_count1*1.5)+(ss.ot_count2*2)+(ss.ot_count3*3)+(ss.ot_count4*4),1 ) paid_hours, ";
        $strSQL .= "   (ot_1+ot_2+ot_3+ot_4) ot_total, ";
        $strSQL .= "   TRUNCATE((ot_1+ot_2+ot_3+ot_4)*(35/100),2) ot_bonus, ";
        $strSQL .= "   ss.travel_bonus, ss.shift_bonus, ss.incentive_bonus, ss.drilling_bonus, ss.act_manager_bonus, ss.production_bonus ";
        $strSQL .= "   FROM mst_salary ms, trn_slip_ptlbwg ss ";
        $strSQL .= "   WHERE ms.bio_rec_id = ss.bio_rec_id ";
        $strSQL .= "   AND ss.client_name = 'Pontil_Banyuwangi'  ";       
        $strSQL .= "   AND ss.year_period = '".$yearPeriod."' ";       
        $strSQL .= "   AND ss.month_period = '".$monthPeriod."' ";       
        // $strSQL .= "   AND ss.payroll_group = '".$group."' ";       
        $strSQL .= $strFilter;
        $strSQL .= "    ORDER BY ss.name  ;";    
        // test($strSQL,1);
        $query = $this->db->query($strSQL)->result_array();  

        
        $boldFont = [
            'font' => [
                'bold' => true
                // 'color' => ['argb' => '0000FF'],
            ],
        ];

        $totalStyle = [
            'font' => [
                'bold' => true,
                'color' => ['argb' => '0000FF'],
            ],
        ];

        $allBorderStyle = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $outlineBorderStyle = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $topBorderStyle = [
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $bottomBorderStyle = [
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        // foreach(range('B','Q') as $columnID)
        // {
        //     $spreadsheet->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        // }           

        // Nama Field Baris Pertama
        $spreadsheet->getActiveSheet()
                ->setCellValue('A1', 'SUMMARY INVOICE PT SANGATI SOERYA SEJAHTERA - PT PONTIL INDONESIA (BANYUWANGI)')
                ->setCellValue('A2', 'PERIOD : '.$monthPeriod.'-'.$yearPeriod)
                ->setCellValue('A3', 'DATE        : ')
                ->setCellValue('A4', 'INVOICE NO  : ')
                ->setCellValue('A5', 'CONTRACT NO : ');

        $spreadsheet->getActiveSheet()->mergeCells("A1:T1");

        $spreadsheet->getActiveSheet()->getStyle("A1:S1")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->mergeCells("A2:S2");
        $spreadsheet->getActiveSheet()->getStyle("A2:S2")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("A1:D1")->getFont()->setBold(true)->setSize(16);
        $spreadsheet->getActiveSheet()->getStyle("A2:D2")->getFont()->setBold(true)->setSize(13);
        $spreadsheet->getActiveSheet()->getStyle("A3:G5")->getFont()->setBold(true)->setSize(12); 

        /* SET HEADER BG COLOR*/
        $spreadsheet->getActiveSheet()->getStyle('A6:S7')
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B'); 

        // $spreadsheet->getActiveSheet()->getStyle('A6:AC7')
        //         ->getFill()
        //         ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        //         ->getStartColor()
        //         ->setRGB('F2BE6B');
        // $spreadsheet->getActiveSheet()->getStyle('A6:AC7')
        //         ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLUE); 

        /* START INVOICE TITLE */
        $spreadsheet->getActiveSheet()->getStyle("A6:H6")->getFont()->setSize(12);
        $spreadsheet->getActiveSheet()->getStyle("A6:H6")->applyFromArray($outlineBorderStyle);
        /* START TITLE NO */
        $titleRowIdx = 6;
        $titleColIdx = 0;

        $spreadsheet->getActiveSheet()
                ->setCellValue('A6', 'NO')
                ->setCellValue('B6', 'NAME')
                ->setCellValue('C6', 'POSITION')
                ->setCellValue('D6', 'SALARY')
                ->setCellValue('F6', 'NT')
                ->setCellValue('G6', 'X 1.5')
                ->setCellValue('H6', 'X 2.0')
                ->setCellValue('I6', 'X 3.0')
                ->setCellValue('J6', 'X 4.0')
                ->setCellValue('K6', 'WORK HOURS')
                ->setCellValue('L6', 'SALARY THIS MONTH')
                ->setCellValue('M6', 'OVER TIME')
                ->setCellValue('N6', 'OUTCAMP/BBM ALLOWANCE')
                ->setCellValue('O6', 'TRAVEL ALLOWANCE')
                ->setCellValue('P6', 'ACTING SUPERVISOR')
                ->setCellValue('Q6', 'GROSS SALARY')
                ->setCellValue('R6', 'CONTRACTOR FEE')
                ->setCellValue('S6', 'TOTAL INVOICE CHARGE');

        $spreadsheet->getActiveSheet()
                ->setCellValue('D7', 'BASE MONTH')
                ->setCellValue('E7', 'BASE HOUR')
                ->setCellValue('G7', 'HOURS')
                ->setCellValue('H7', 'HOURS')
                ->setCellValue('I7', 'HOURS')
                ->setCellValue('J7', 'HOURS')
                // ->setCellValue('N7', 'TRAVEL')
                // ->setCellValue('O7', 'ATTENDANCE')
                // ->setCellValue('P7', 'HOUSING')
                // ->setCellValue('Q7', 'ACT MANAGER')
                // ->setCellValue('R7', 'DRILLING')
                ;

        // $spreadsheet->getActiveSheet()
        //         ->setCellValue('A8', 'A')
        //         ->setCellValue('B8', 'B')
        //         ->setCellValue('C8', 'C')
        //         ->setCellValue('D8', 'D')
        //         ->setCellValue('E8', 'E')
        //         ->setCellValue('F8', 'F')
        //         ->setCellValue('G8', 'G')
        //         ->setCellValue('H8', 'H')
        //         ->setCellValue('I8', 'I')
        //         ->setCellValue('J8', 'J')
        //         ->setCellValue('K8', 'K')
        //         ->setCellValue('L8', 'L')
        //         ->setCellValue('M8', 'M')
        //         ->setCellValue('N8', 'N')
        //         ->setCellValue('O8', 'O')
        //         ->setCellValue('P8', 'P')
        //         ->setCellValue('Q8', 'Q')
        //         ->setCellValue('R8', 'R')
        //         ->setCellValue('S8', 'S')
        //         ->setCellValue('T8', 'T')
        //         ->setCellValue('U8', 'U');

        $spreadsheet->getActiveSheet()
            ->mergeCells("A6:A7")
            ->mergeCells("B6:B7")
            ->mergeCells("C6:C7")
            ->mergeCells("F6:F7")
            ->mergeCells("K6:K7")
            ->mergeCells("L6:L7")
            ->mergeCells("M6:M7")
            ->mergeCells("N6:N7")
            ->mergeCells("O6:O7")
            ->mergeCells("P6:P7")
            ->mergeCells("Q6:Q7")
            ->mergeCells("R6:R7")
            ->mergeCells("S6:S7")
        ;

        $spreadsheet->getActiveSheet()
            ->mergeCells("D6:E6");

        $spreadsheet->getActiveSheet()->getStyle("A6:S7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("A6:S7")->applyFromArray($center);
        // /* END TITLE NO */

        $rowIdx = 7;
        $startIdx = $rowIdx; 
        $rowNo = 0;
        foreach ($query as $row) {
            $rowIdx++;
            $rowNo++;

            $gross = $row['current_salary'] + $row['ot_total'] + $row['travel_bonus'] + $row['flying_camp'] + $row['act_manager_bonus'] ;
            $chargeTotal = $gross + $row['uplift'];
            $spreadsheet->getActiveSheet()
                ->setCellValue('A'.($rowIdx), $rowNo)
                ->setCellValue('B'.($rowIdx), $row['name'])
                ->setCellValue('C'.($rowIdx), $row['job_desc'])
                ->setCellValue('D'.($rowIdx), $row['basic_salary'])
                ->setCellValue('E'.($rowIdx), $row['rate'])
                ->setCellValue('F'.($rowIdx), $row['normal_time'])
                ->setCellValue('G'.($rowIdx), $row['ot_count1'])
                ->setCellValue('H'.($rowIdx), $row['ot_count2'])
                ->setCellValue('I'.($rowIdx), $row['ot_count3'])
                ->setCellValue('J'.($rowIdx), $row['ot_count4'])
                ->setCellValue('K'.($rowIdx), $row['worked_hours'])
                ->setCellValue('L'.($rowIdx), $row['current_salary'])
                ->setCellValue('M'.($rowIdx), $row['ot_total'])
                ->setCellValue('N'.($rowIdx), $row['flying_camp'])
                ->setCellValue('O'.($rowIdx), $row['travel_bonus'])
                ->setCellValue('P'.($rowIdx), $row['act_manager_bonus'])
                // ->setCellValue('Q'.($rowIdx), $row['gros'])
                // ->setCellValue('R'.($rowIdx), $row['drilling_bonus'])
                // ->setCellValue('R'.($rowIdx), $gross)
                // ->setCellValue('U'.($rowIdx), '=SUM(R'.$rowIdx.':S'.$rowIdx.')')
                ->setCellValue('Q'.($rowIdx), '=SUM(L'.$rowIdx.':P'.$rowIdx.')')
                ->setCellValue('R'.($rowIdx), '=IF(K'.$rowIdx.'<=312,((E'.$rowIdx.'*K'.$rowIdx.')*64%),((E'.$rowIdx.'*312)*64%))')
                ->setCellValue('S'.($rowIdx), $chargeTotal)
                ;

            /* SET ROW COLOR */
            if($rowIdx % 2 == 1)
            {
                $spreadsheet->getActiveSheet()->getStyle('A'.$rowIdx.':S'.$rowIdx)
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('EAEBAF');             
            } 
            /* END UPDATE TAX */
        } /* end foreach ($query as $row) */

        $spreadsheet->getActiveSheet()
                ->setCellValue('K'.($rowIdx+2), 'TOTAL')
                ->setCellValue('S'.($rowIdx+2), '=SUM(S'.$startIdx.':S'.$rowIdx.')');

        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":S".($rowIdx+2))->getFont()->setBold(true)->setSize(12); 
        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":S".($rowIdx+2))->applyFromArray($outlineBorderStyle);

        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":S".($rowIdx+2))
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B');

        /* SET NUMBERS FORMAT*/
        $spreadsheet->getActiveSheet()->getStyle('D8:S'.($rowIdx+2))->getNumberFormat()->setFormatCode('#,##0.00');
        $spreadsheet->getActiveSheet()->getStyle('A1')->getAlignment()->setWrapText(true);     
        
        unset($allBorderStyle);
        unset($center);
        unset($right);
        unset($left);
        
        $spreadsheet->setActiveSheetIndex(0);

        $str = 'PTLBwgInvoice';
        $fileName = preg_replace('/\s+/', '', $str);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.Xlsx"');
        // header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        /* BY COMPOSER */
        // $writer = new Xlsx($spreadsheet);
        /* OFFLINE/ BY COPY EXCEL FOLDER  */
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit(0);   
    }
    /* END PONTIL INVOICE LIST EXPORT */

    public function getDataList($pt, $year, $month, $group)
    {   
        $strFilter = "";
        if($group != 'A')
        {
            $strFilter = " AND payroll_group = '".$group."' ";    
        }

        $sql = 'SELECT * FROM trn_slip_ptlbwg WHERE client_name = "'.$pt.'"'.$strFilter.' AND year_period = "'.$this->security->xss_clean($year).'" AND month_period = "'.$this->security->xss_clean($month).'" ORDER BY name ASC';
        $query = $this->db->query($sql)->result_array();
        /*return json_encode($query);*/
        $myData = array();
        foreach ($query as $key => $row) {
            $myData[] = array(
                $row['salary_slip_id'],         
                $pt,         
                $row['name'],         
                // $row['production_bonus'],         
                // $row['workday_adj'],         
                // $row['adjust_in'],         
                // $row['adjust_out'],         
                $row['dept'],         
                $row['position']         
                // $row['attendance_bonus'],         
                // $row['other_allowance1'],         
                // $row['other_allowance2'],         
                // $row['cc_payment'],         
                // $row['thr'],         
                // $row['debt_burden'],
                // $row['debt_explanation']
            );            
        }  
        echo json_encode($myData);   
    }

}

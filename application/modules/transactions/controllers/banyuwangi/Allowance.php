<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	// Load library phpspreadsheet
    require('./vendor/autoload.php');
    use PhpOffice\PhpSpreadsheet\Helper\Sample;
    use PhpOffice\PhpSpreadsheet\IOFactory;
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    // End load library phpspreadsheet

class Allowance extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct(); 
		$this->load->model('masters/M_process_closing'); 
		$this->load->model('masters/M_allowance'); 
		// $this->load->model('masters/M_salary');
        $this->load->model('masters/M_mst_salary', 'M_salary'); 
		$this->load->model('M_salary_slip');
	}

	public function upload()
    {
    	/* ALWRPTSMB201904 */
        $originalName = $_FILES['file']['name'];  
        $shortName = substr($originalName, 0, 9);
        $year = substr($originalName, 9, 4);
        $month = substr($originalName, 13, 2);
        /* Database's Year */
        // $dbYear = right($this->db->database,4);
        // $shortName = substr($originalName, 0, 6);
        /* File's Year */
        // $year = substr($originalName, 6, 4);
        // $month = substr($originalName, 10, 2);
        $clientName = '';
        switch ($shortName) {            
            case 'ALWMCMSMB':
                $clientName = 'Machmahon_Sumbawa';
                break;
            case 'ALWPTLBWG':
                $clientName = 'Pontil_Banyuwangi';
                break;     
            case 'ALWLCPSMB':
                $clientName = 'LCP_Sumbawa';
                break;         
            default:
                $clientName = '';
                break;
        } 
        
        if($clientName == '')
        {
            echo 'Import Data Failed, Please Check File Format';
            exit(0);
        }

        $dataCount = $this->M_process_closing->getCountByClientPeriod($clientName, $year, $month);
        if($dataCount >= 1)
        {
            echo 'Data with the same period has been uploaded before';
            exit(0);
            // redirect('content/detail/ird');
        }
        $fileDir = realpath(APPPATH.'../uploads/'); //buat folder dengan nama uploads di root folder 
        // $fileDir = '../uploads/'; //buat folder dengan nama uploads di root folder 
        // $fileDir = $_SERVER['DOCUMENT_ROOT']; //buat folder dengan nama uploads di root folder 
        // echo FCPATH; exit(0);
        $fileDir = FCPATH."/uploads/";

        $fileName = time().$originalName;         
        // $config['upload_path'] = './assets/'; //buat folder dengan nama assets di root folder
        $config['upload_path'] = $fileDir; 
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 10000;         

        $this->load->library('upload');
        $this->upload->initialize($config);         
        // $this->upload->do_upload('file');

        if(! $this->upload->do_upload('file'))
        {
            $this->upload->display_errors();
        }
             
        $media = $this->upload->data('file');
        /* WINDOWS PATH */
        // $inputFileName = $fileDir.'\\'.$fileName;
        $inputFileName = $fileDir.$fileName;
        // $inputFileName = $fileDir.'/'.$fileName;
        // echo $inputFileName; exit(0);
        try {
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                // $reader->setReadFilter( new MyReadFilter() );
                $objPHPExcel = $reader->load($inputFileName);
                // $inputFileType = IOFactory::identify($inputFileName);                
                // $objReader = IOFactory::createReader($inputFileType);
                // $objPHPExcel = $objReader->load($inputFileName);                

        } catch(Exception $e) {
            die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
        }
 
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
        $dataArr = array();

        $failedNames = array();
        $arrIdx = 0;
          
        $dataCount = 0;
        $noBiodataId = 0;

        $tData = '';
        $tmpData = '';
        $nie = '';
        /* Start Of TRANSACTION */  
        $this->db->trans_begin();
        /* START UPLOAD AMNT Sumbawa Staff, Trakindo Sumbawa */
        // if($clientName == 'Machmahon_Sumbawa')
        { 

            $dataCount = 0;
            $noBiodataId = 0;
            for ($row = 4; $row <= $highestRow; $row++)  //  Read a row of data into an array
            {
                 $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                            NULL,
                                            TRUE,
                                            FALSE);
                
                $nie = $rowData[0][1];
                $nie = preg_replace('/[\r\n]+/','', $nie);
                $nie = trim($nie);
                if($nie != '')
                {
                    $this->M_allowance->resetValues();                    
                    $this->M_allowance->setAllowanceId($this->security->xss_clean($this->M_allowance->GenerateNumber()));                    
                    $salaryRow = $this->M_salary->loadBioIdByNie($nie);

                    
                    $bioId = $salaryRow['bio_rec_id'];                 
   
                    $this->M_allowance->deleteByIdPeriod($bioId, $clientName, $year, $month);                     

                    if($bioId == "" || $bioId == null){
                        $noBiodataId++;
                        /* GET ERROR DATA */
                        $failedNames[$arrIdx] = $rowData[0][2];
                        $arrIdx++;
                    }else{
                        $this->M_allowance->setBiodataId($this->security->xss_clean($bioId));
                    }                     
                    $this->M_allowance->setBadgeNo($nie);

                    // $dept = $sheet->getCell('A4')->getValue();
                    // $dept = preg_replace('/[\r\n]+/','', $dept);
                    // $dept = trim(strtoupper($dept));
                    // $this->M_timesheet->setDept($dept);
                    // $tmpData .= $clientName;

                    $this->M_allowance->setClientName($this->security->xss_clean($clientName));

                    $tData = $rowData[0][2]; 
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    $this->M_allowance->setEmpName($this->security->xss_clean($tData));

                    $myCurrentDate = GetCurrentDate();
                    $curMonth = $myCurrentDate['CurrentMonth'];
                    $curYear = $myCurrentDate['CurrentYear'];
                    $this->M_allowance->setMonthPeriod($this->security->xss_clean($month));
                    $this->M_allowance->setYearPeriod($this->security->xss_clean($year));

                    if($clientName != 'LCP_Sumbawa'){
                        $tData = $rowData[0][3]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_allowance->setContractBonus($this->security->xss_clean("0"));   
                        }else{
                            $this->M_allowance->setContractBonus($this->security->xss_clean($tData));
                        }
                    }

                    $tData = $rowData[0][4]; 
                    $tData = preg_replace('/[\r\n]+/','', $tData);
                    $tData = trim(strtoupper($tData));
                    if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_allowance->setThr($this->security->xss_clean("0"));   
                    }else{
                        $this->M_allowance->setThr($this->security->xss_clean($tData));
                    }

                    if($clientName == 'Machmahon_Sumbawa'){

                        $tData = $rowData[0][5]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_allowance->setProductionBonus($this->security->xss_clean("0"));   
                        }else{
                            $this->M_allowance->setProductionBonus($this->security->xss_clean($tData));
                        }

                        $tData = $rowData[0][6]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_allowance->setKpiBonus($this->security->xss_clean("0"));   
                        }else{
                            $this->M_allowance->setKpiBonus($this->security->xss_clean($tData));
                        }
                    }

                    if($clientName == 'Pontil_Banyuwangi'){
                        $tData = $rowData[0][7]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_allowance->setOutCamp($this->security->xss_clean("0"));   
                        }else{
                            $this->M_allowance->setOutCamp($this->security->xss_clean($tData));
                        }                    

                        $tData = $rowData[0][9]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_allowance->setDrillingBonus($this->security->xss_clean("0"));   
                        }else{
                            $this->M_allowance->setDrillingBonus($this->security->xss_clean($tData));
                        }

                        $tData = $rowData[0][10]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_allowance->setActManagerBonus($this->security->xss_clean("0"));   
                        }else{
                            $this->M_allowance->setActManagerBonus($this->security->xss_clean($tData));
                        }  
                    }

                    $this->M_allowance->setPicProcess($this->security->xss_clean($this->session->userdata('hris_user_id')));
                    $myCurrentDate = GetCurrentDate();
                    $curDateTime = $myCurrentDate['CurrentDateTime'];
                    $this->M_allowance->setProcessTime($this->security->xss_clean($curDateTime));
                    $this->M_allowance->insert();
        			// echo $year.'- '.$month.' '.$dataCount; exit(0);
                    $dataCount++; 

                }                                
                
            } // for ($row = 2; $row <= $highestRow; $row++)   
            // echo $tmpData;                        
            
        } //if $clientName = 'AMNT_Sumbawa_Staf'

        /* End Of TRANSACTION */  
        if ($this->db->trans_status() === FALSE)
        {
           $this->db->trans_rollback();
           // $this->session->set_flashdata('rosterMsg', 'Gagal Import Data, Cek Kembali Format File');
           $this->load->M_process_closing->resetValues();
           $this->load->M_timesheet->resetValues();     
           echo 'Import Data Failed, Please Check File Format';
        }
        else
        {
           $this->db->trans_commit();
           // $this->session->set_flashdata('rosterMsg', 'Import Data Berhasil : '.$dataCount.', Tanpa Biodata Id : '.$noBiodataId);
           echo 'Import Data Succeed : '.$dataCount.', No Biodata Id : '.$noBiodataId;
        }  

            
    }
}
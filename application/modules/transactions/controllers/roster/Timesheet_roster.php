<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    // Load library phpspreadsheet
    require('./vendor/autoload.php');
    use PhpOffice\PhpSpreadsheet\Helper\Sample;
    use PhpOffice\PhpSpreadsheet\IOFactory;
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    // End load library phpspreadsheet
 
class Timesheet_roster extends CI_Controller {

    function __construct(){
        parent::__construct();
        // $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
        $this->load->model('M_timesheet'); 
        $this->load->model('M_overtime'); 
        $this->load->model('masters/M_mst_bio_rec'); 
        $this->load->model('masters/M_mst_salary', 'M_salary');  
        $this->load->model('masters/M_overtime'); 
        $this->load->model('M_slip_ptlsmb','M_salary_slip'); 
        $this->load->model('masters/M_process_closing'); 
        $this->load->model('masters/M_payroll_config'); 
        $this->load->model('masters/M_mst_roster_check');
    }

    public function index()
    {
        $this->load->view('excel');
    }

    public function upload()
    {
        $originalName = $_FILES['file']['name'];  
        /*$shortName = substr($originalName, 0, 3);
        $year = substr($originalName, 3, 4);
        $month = substr($originalName, 7, 2);*/
        /* Database's Year */
        $dbYear = right($this->db->database,4);
        $shortName = substr($originalName, 0, 6);
        /* File's Year */
        $year = substr($originalName, 6, 4);
        $month = substr($originalName, 10, 2);

        $clientName = '';
        switch ($shortName) {
            case 'PTLTMK':
                $clientName = 'Pontil_Timika';
                break;
            case 'RPTTMK':
                $clientName = 'Redpath_Timika';
                break;   
            case 'TRKSMB':
                $clientName = 'Trakindo_Sumbawa';
                break;
            case 'PTLSMB':
                $clientName = 'Pontil_Sumbawa';
                break;
            case 'LCPSMB':
                $clientName = 'LCP_Sumbawa';
                break;
            case 'PTLBWG':
                $clientName = 'Pontil_Banyuwangi';
                break;
                
            default:
                $clientName = '';
                break;
        }
       
        
        if($clientName == '')
        {
            // $this->session->set_flashdata('rosterMsg', 'Client has not registered');
            echo 'Client has not registered';
            exit(0);
            // redirect('content/detail/ird');
        }

        $dataCount = $this->M_process_closing->getCountByClientPeriod($clientName, $year, $month);
        if($dataCount >= 1)
        {
            // $this->session->set_flashdata('rosterMsg', 'Data with the same period has been uploaded before');
            echo 'Data with the same period has been uploaded before';
            exit(0);
            // redirect('content/detail/ird');
        }
        // echo $dataCount;
        $fileDir = realpath(APPPATH.'../uploads/'); //buat folder dengan nama uploads di root folder 
        // $fileDir = '../uploads/'; //buat folder dengan nama uploads di root folder 
        // $fileDir = $_SERVER['DOCUMENT_ROOT']; //buat folder dengan nama uploads di root folder 
        // echo FCPATH; exit(0);
        $fileDir = FCPATH."/uploads/";

        $fileName = time().$originalName;         
        // $config['upload_path'] = './assets/'; //buat folder dengan nama assets di root folder
        $config['upload_path'] = $fileDir; 
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 10000;         

        $this->load->library('upload');
        $this->upload->initialize($config);         
        // $this->upload->do_upload('file');

        // chmod($fileDir, 777);

        if(! $this->upload->do_upload('file'))
        {
            $this->upload->display_errors();
        }
             
        $media = $this->upload->data('file');
        /* WINDOWS PATH */
        // $inputFileName = $fileDir.'\\'.$fileName;
        $inputFileName = $fileDir.$fileName;
        // $inputFileName = $fileDir.'/'.$fileName;
        // echo $inputFileName; exit(0);
        try {
                $inputFileType = IOFactory::identify($inputFileName);                
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
                

            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }
 
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
            $dataArr = array();

            $failedNames = array();
            $arrIdx = 0;
              
            $dataCount = 0;
            $noBiodataId = 0;
            /* Start Of TRANSACTION */  
            $this->db->trans_begin();
            
            if($clientName == 'Pontil_Sumbawa')
            {                                   
                $dataCount = 0;
                $noBiodataId = 0;
                for ($row = 7; $row <= $highestRow; $row++)  //  Read a row of data into an array
                {                     
                     $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);


                     $badgeNo = $rowData[0][1]; 
                     $badgeNo = preg_replace('/[\r\n]+/','', $badgeNo);
                     $badgeNo = trim($badgeNo);
                     
                     if($badgeNo != '')
                     {
                         $this->M_timesheet->resetValues();
                         $this->M_timesheet->setRosterId($this->security->xss_clean($this->M_timesheet->GenerateNumber()));
                         $salaryRow = $this->M_salary->loadBioIdByNie($badgeNo);
                         $bioId = $salaryRow['bio_rec_id'];                    

                         $this->M_salary_slip->deleteByIdPeriod($bioId, $clientName, $year, $month);
                         $str = $this->M_overtime->deleteByIdPeriod($bioId, $clientName, $year, $month);      
                         $this->M_timesheet->deleteByIdPeriod($bioId, $clientName, $year, $month);

                         // $this->M_MstBioRec->loadByNie($nie);
                         // $bioId = $this->M_MstBioRec->getBioRecId();
                         if($bioId == ""){
                            $noBiodataId++;
                            
                            /* GET ERROR DATA */
                            $failedNames[$arrIdx] = $rowData[0][2];
                            $arrIdx++;

                         }
                         else{
                            $this->M_mst_bio_rec->setBioRecId($this->security->xss_clean($bioId));
                         }

                         $this->M_mst_roster_check->setBadgeNo($badgeNo);
                      
                         $this->M_mst_roster_check->setEmployeeName($this->security->xss_clean($rowData[0][2]));
                         $this->M_mst_roster_check->setClientName($this->security->xss_clean($clientName));
                         $currFullDate = GetCurrentDate();
                         $curMonth = $currFullDate['CurrentYear'];
                         $curYear = $currFullDate['CurrentMonth'];
                         $this->M_mst_roster_check->setMonthPeriod($this->security->xss_clean($month));
                         $this->M_mst_roster_check->setYearPeriod($this->security->xss_clean($year));
                         
                         $this->M_mst_roster_check->setPicProcess($this->security->xss_clean($this->session->userdata('hris_user_id')));
                         $currFullDate = GetCurrentDate();
                         $curDateTime = $currFullDate['CurrentDateTime'];
                         $this->M_mst_roster_check->setProcessTime($this->security->xss_clean($curDateTime));

                         $this->M_mst_roster_check->insert();
                         $dataCount++;
                           
                     }                 
                                         
                } // for ($row = 2; $row <= $highestRow; $row++)               
                
            } //if $clientName = 'Pontil_Sumbawa'

            else if($clientName == 'Redpath_Timika')
            {                                   
                $dataCount = 0;
                $noBiodataId = 0;
                for ($row = 3; $row <= $highestRow; $row++)  //  Read a row of data into an array
                {                     
                     $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);


                     $badgeNo = $rowData[0][3]; 
                     $badgeNo = preg_replace('/[\r\n]+/','', $badgeNo);
                     $badgeNo = trim($badgeNo);
                     
                     if($badgeNo != '')
                     {
                         $this->M_timesheet->resetValues();
                         $this->M_timesheet->setRosterId($this->security->xss_clean($this->M_timesheet->GenerateNumber()));
                         $salaryRow = $this->M_salary->loadBioIdByNie($badgeNo);
                         $bioId = $salaryRow['bio_rec_id'];                    

                         $this->M_salary_slip->deleteByIdPeriod($bioId, $clientName, $year, $month);
                         $str = $this->M_overtime->deleteByIdPeriod($bioId, $clientName, $year, $month);      
                         $this->M_timesheet->deleteByIdPeriod($bioId, $clientName, $year, $month); 

                         // $this->M_MstBioRec->loadByNie($nie);
                         // $bioId = $this->M_MstBioRec->getBioRecId();
                         if($bioId == ""){
                            $noBiodataId++;
                            
                            /* GET ERROR DATA */
                            $failedNames[$arrIdx] = $rowData[0][1];
                            $arrIdx++;

                         }
                         else{
                            $this->M_mst_bio_rec->setBioRecId($this->security->xss_clean($bioId));
                         }

                         $this->M_mst_roster_check->setBadgeNo($badgeNo);                        
                         $this->M_mst_roster_check->setEmployeeName($this->security->xss_clean($rowData[0][1]));
                         $this->M_mst_roster_check->setClientName($this->security->xss_clean($clientName));

                         $currFullDate = GetCurrentDate();
                         $curMonth = $currFullDate['CurrentYear'];
                         $curYear = $currFullDate['CurrentMonth'];
                         $this->M_mst_roster_check->setMonthPeriod($this->security->xss_clean($month));
                         $this->M_mst_roster_check->setYearPeriod($this->security->xss_clean($year));
                         // $this->M_MstRoster->setIsActive($this->security->xss_clean('1'));
                         $this->M_mst_roster_check->setPicProcess($this->security->xss_clean($this->session->userdata('hris_user_id')));
                         $currFullDate = GetCurrentDate();
                         $curDateTime = $currFullDate['CurrentDateTime'];
                         $this->M_mst_roster_check->setProcessTime($this->security->xss_clean($curDateTime));

                         $this->M_mst_roster_check->insert();
                         $dataCount++;
                           
                     }                 
                                         
                } // for ($row = 2; $row <= $highestRow; $row++)               
                
            } //if $clientName = 'Redpath_Timika' 

            else if($clientName == 'Trakindo_Sumbawa' || $clientName == 'LCP_Sumbawa')
            {                                   
                $dataCount = 0;
                $noBiodataId = 0;
                for ($row = 7; $row <= $highestRow; $row++)  //  Read a row of data into an array
                {                     
                     $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);


                     $badgeNo = $rowData[0][1]; 
                     $badgeNo = preg_replace('/[\r\n]+/','', $badgeNo);
                     $badgeNo = trim($badgeNo);
                     
                     if($badgeNo != '')
                     {
                         $this->M_timesheet->resetValues();
                         $this->M_timesheet->setRosterId($this->security->xss_clean($this->M_timesheet->GenerateNumber()));
                         $salaryRow = $this->M_salary->loadBioIdByNie($badgeNo);
                         $bioId = $salaryRow['bio_rec_id'];                    

                         $this->M_salary_slip->deleteByIdPeriod($bioId, $clientName, $year, $month);
                         $str = $this->M_overtime->deleteByIdPeriod($bioId, $clientName, $year, $month);      
                         $this->M_timesheet->deleteByIdPeriod($bioId, $clientName, $year, $month); 

                         // $this->M_MstBioRec->loadByNie($nie);
                         // $bioId = $this->M_MstBioRec->getBioRecId();
                         if($bioId == ""){
                            $noBiodataId++;
                            
                            /* GET ERROR DATA */
                            $failedNames[$arrIdx] = $rowData[0][1];
                            $arrIdx++;

                         }
                         else{
                            $this->M_mst_bio_rec->setBioRecId($this->security->xss_clean($bioId));
                         }

                         $this->M_mst_roster_check->setBadgeNo($badgeNo);                        
                         $this->M_mst_roster_check->setEmployeeName($this->security->xss_clean($rowData[0][2]));
                         $this->M_mst_roster_check->setClientName($this->security->xss_clean($clientName));

                         $currFullDate = GetCurrentDate();
                         $curMonth = $currFullDate['CurrentYear'];
                         $curYear = $currFullDate['CurrentMonth'];
                         $this->M_mst_roster_check->setMonthPeriod($this->security->xss_clean($month));
                         $this->M_mst_roster_check->setYearPeriod($this->security->xss_clean($year));
                         // $this->M_MstRoster->setIsActive($this->security->xss_clean('1'));
                         $this->M_mst_roster_check->setPicProcess($this->security->xss_clean($this->session->userdata('hris_user_id')));
                         $currFullDate = GetCurrentDate();
                         $curDateTime = $currFullDate['CurrentDateTime'];
                         $this->M_mst_roster_check->setProcessTime($this->security->xss_clean($curDateTime));

                         $this->M_mst_roster_check->insert();
                         $dataCount++;
                           
                     }                 
                                         
                } // for ($row = 2; $row <= $highestRow; $row++)               
                
            } //if $clientName = 'Redpath_Timika'

            else if($clientName == 'Pontil_Banyuwangi')
            {                                   
                $dataCount = 0;
                $noBiodataId = 0;
                for ($row = 7; $row <= $highestRow; $row++)  //  Read a row of data into an array
                {                     
                     $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);


                     $badgeNo = $rowData[0][1]; 
                     $badgeNo = preg_replace('/[\r\n]+/','', $badgeNo);
                     $badgeNo = trim($badgeNo);
                     
                     if($badgeNo != '')
                     {
                         $this->M_timesheet->resetValues();
                         $this->M_timesheet->setRosterId($this->security->xss_clean($this->M_timesheet->GenerateNumber()));
                         $salaryRow = $this->M_salary->loadBioIdByNie($badgeNo);
                         $bioId = $salaryRow['bio_rec_id'];                    

                         $this->M_salary_slip->deleteByIdPeriod($bioId, $clientName, $year, $month);
                         $str = $this->M_overtime->deleteByIdPeriod($bioId, $clientName, $year, $month);      
                         $this->M_timesheet->deleteByIdPeriod($bioId, $clientName, $year, $month); 

                         // $this->M_MstBioRec->loadByNie($nie);
                         // $bioId = $this->M_MstBioRec->getBioRecId();
                         if($bioId == ""){
                            $noBiodataId++;
                            
                            /* GET ERROR DATA */
                            $failedNames[$arrIdx] = $rowData[0][1];
                            $arrIdx++;

                         }
                         else{
                            $this->M_mst_bio_rec->setBioRecId($this->security->xss_clean($bioId));
                         }

                         $this->M_mst_roster_check->setBadgeNo($badgeNo);                        
                         $this->M_mst_roster_check->setEmployeeName($this->security->xss_clean($rowData[0][2]));
                         $this->M_mst_roster_check->setClientName($this->security->xss_clean($clientName));

                         $currFullDate = GetCurrentDate();
                         $curMonth = $currFullDate['CurrentYear'];
                         $curYear = $currFullDate['CurrentMonth'];
                         $this->M_mst_roster_check->setMonthPeriod($this->security->xss_clean($month));
                         $this->M_mst_roster_check->setYearPeriod($this->security->xss_clean($year));
                         // $this->M_MstRoster->setIsActive($this->security->xss_clean('1'));
                         $this->M_mst_roster_check->setPicProcess($this->security->xss_clean($this->session->userdata('hris_user_id')));
                         $currFullDate = GetCurrentDate();
                         $curDateTime = $currFullDate['CurrentDateTime'];
                         $this->M_mst_roster_check->setProcessTime($this->security->xss_clean($curDateTime));

                         $this->M_mst_roster_check->insert();
                         $dataCount++;
                           
                     }                 
                                         
                } // for ($row = 2; $row <= $highestRow; $row++)               
                
            } //if $clientName = 'Pontil_Banyuwangi' 

            else if($clientName == 'Pontil_Timika')
            {                                   
                $dataCount = 0;
                $noBiodataId = 0;
                for ($row = 2; $row <= $highestRow; $row++)  //  Read a row of data into an array
                {                     
                     $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);


                     $badgeNo = $rowData[0][1]; 
                     $badgeNo = preg_replace('/[\r\n]+/','', $badgeNo);
                     $badgeNo = trim($badgeNo);
                     
                     if($badgeNo != '')
                     {
                         $this->M_timesheet->resetValues();
                         $this->M_timesheet->setRosterId($this->security->xss_clean($this->M_timesheet->GenerateNumber()));
                         $salaryRow = $this->M_salary->loadBioIdByNie($badgeNo);
                         $bioId = $salaryRow['bio_rec_id'];                    

                         $this->M_salary_slip->deleteByIdPeriod($bioId, $clientName, $year, $month);
                         $str = $this->M_overtime->deleteByIdPeriod($bioId, $clientName, $year, $month);      
                         $this->M_timesheet->deleteByIdPeriod($bioId, $clientName, $year, $month); 

                         if($bioId == ""){
                            $noBiodataId++;
                            
                            /* GET ERROR DATA */
                            $failedNames[$arrIdx] = $rowData[0][1];
                            $arrIdx++;

                         }
                         else{
                            $this->M_mst_bio_rec->setBioRecId($this->security->xss_clean($bioId));
                         }

                         $this->M_mst_roster_check->setBadgeNo($badgeNo);                        
                         $this->M_mst_roster_check->setEmployeeName($this->security->xss_clean($rowData[0][2]));
                         $this->M_mst_roster_check->setClientName($this->security->xss_clean($clientName));

                         $currFullDate = GetCurrentDate();
                         $curMonth = $currFullDate['CurrentYear'];
                         $curYear = $currFullDate['CurrentMonth'];
                         $this->M_mst_roster_check->setMonthPeriod($this->security->xss_clean($month));
                         $this->M_mst_roster_check->setYearPeriod($this->security->xss_clean($year));
                         // $this->M_MstRoster->setIsActive($this->security->xss_clean('1'));
                         $this->M_mst_roster_check->setPicProcess($this->security->xss_clean($this->session->userdata('hris_user_id')));
                         $currFullDate = GetCurrentDate();
                         $curDateTime = $currFullDate['CurrentDateTime'];
                         $this->M_mst_roster_check->setProcessTime($this->security->xss_clean($curDateTime));

                         $this->M_mst_roster_check->insert();
                         $dataCount++;
                           
                     }                 
                                         
                } // for ($row = 2; $row <= $highestRow; $row++)               
                
            } //if $clientName = 'Pontil_Timika'
            
            /* End Of TRANSACTION */  

            if ($this->db->trans_status() === FALSE)
            {
                    $this->db->trans_rollback();
               // $this->session->set_flashdata('rosterMsg', 'Gagal Import Data, Cek Kembali Format File');
               $this->load->M_MstProcessClosing->resetValues();
               $this->load->M_MstRoster->resetValues();     
               echo 'Import Data Failed, Please Check File Format';
            }
            else
            {
                    $this->db->trans_commit();
               // $this->session->set_flashdata('rosterMsg', 'Import Data Berhasil : '.$dataCount.', Tanpa Biodata Id : '.$noBiodataId);
               echo 'Import Data Succeed : '.$dataCount.', No Id : '.$noBiodataId;
            }        
            
            // $this->session->set_flashdata('failedDatas', $failedNames);
            // redirect('content/detail/ird');
    } 

}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    // Load library phpspreadsheet
    require('./vendor/autoload.php');
    use PhpOffice\PhpSpreadsheet\Helper\Sample;
    use PhpOffice\PhpSpreadsheet\IOFactory;
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    // End load library phpspreadsheet

class Invoice_trksmb extends CI_Controller {
	
	public function index()
	{
		$this->load->view('welcome_message');
	}

     /* START TRAKINDO SUMMARY INVOICE LIST EXPORT */
    public function exportInvoiceSummaryTrakindo($clientName, $yearPeriod, $monthPeriod)
    {
        // $dept = $this->security->xss_clean($dept);

        //membuat objek
        // $objPHPExcel = new PHPExcel();

        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();  

        $spreadsheet->getProperties()->setCreator('Maurice - Web - Android')
            ->setLastModifiedBy('Maurice - Web - Android')
            ->setTitle('Office 2007 XLSX Test Document')
            ->setSubject('Office 2007 XLSX Test Document')
            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Test result file');

        if (file_exists('assets/images/report_logo.png')) {
            $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing->setName('Logo');
            $drawing->setDescription('Logo');
            $drawing->setPath('./assets/images/report_logo.png');
            $drawing->setCoordinates('A1');
            $drawing->setHeight(36);
            $drawing->setWorksheet($spreadsheet->getActiveSheet());
        }

        $strSQL = "";
        $strFilter = "";
        // if($payrollGroup != 'All')
        // {
        //     $strFilter = " AND ss.payroll_group = '".$payrollGroup."' ";    
        // }

        $strSQL  = " SELECT ";   
        $strSQL .= "   ms.bio_rec_id, ms.nie, trk.dept, trk.position, trk.basic_salary, mp.service_fee, mp.payroll_level, trk.flying_camp, trk.other_allowance2, trk.night_shift_bonus, trk.safety_bonus, trk.contract_bonus, trk.name, ";
        $strSQL .= "   TRUNCATE((trk.basic_salary/173),1) rate,
                       (trk.normal_time+trk.ot_count1+trk.ot_count2+trk.ot_count3+trk.ot_count4) hours_total,
                       (mp.service_fee * (trk.normal_time+trk.ot_count1+trk.ot_count2+trk.ot_count3+trk.ot_count4)) total_charge_service ";
        $strSQL .= "   FROM mst_salary ms, trn_slip_trksmb trk , mst_payroll_level mp ";
        $strSQL .= "   WHERE ms.bio_rec_id = trk.bio_rec_id  ";
        $strSQL .= "   AND trk.basic_salary = mp.basic_salary  ";
        $strSQL .= "   AND trk.client_name = '".$clientName."' ";       
        $strSQL .= "   AND trk.year_period = '".$yearPeriod."' ";       
        $strSQL .= "   AND trk.month_period = '".$monthPeriod."' ";       
        // $strSQL .= "   AND trk.dept = '".$dept."' ";       
        // $strSQL .= $strFilter;
        $strSQL .= "   GROUP BY trk.name  ;";    
        $query = $this->db->query($strSQL)->result_array();  
        $boldFont = [
            'font' => [
                'bold' => true
                // 'color' => ['argb' => '0000FF'],
            ],
        ];

        $totalStyle = [
            'font' => [
                'bold' => true,
                'color' => ['argb' => '0000FF'],
            ],
        ];

        $allBorderStyle = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $outlineBorderStyle = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $topBorderStyle = [
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $bottomBorderStyle = [
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        // foreach(range('B','Q') as $columnID)
        // {
        //     $spreadsheet->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        // }           

         // Nama Field Baris Pertama
        $spreadsheet->getActiveSheet()
                ->setCellValue('A1', 'SUMMARY INVOICE PT. SANGATI SOERYA SEJAHTERA - PT TRAKINDO UTAMA INDONESIA (SUMBAWA)')
                ->setCellValue('A2', 'PERIOD : '.$monthPeriod.'-'.$yearPeriod)
                ->setCellValue('A3', 'DATE        : ')
                ->setCellValue('A4', 'INVOICE NO  : ')
                ->setCellValue('A5', 'CONTRACT NO : ')
                ->setCellValue('A9', 'PT. TRAKINDO UTAMA' );

        $spreadsheet->getActiveSheet()->mergeCells("A1:S1");

        $spreadsheet->getActiveSheet()->getStyle("A1:O1")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->mergeCells("A2:O2");
        $spreadsheet->getActiveSheet()->getStyle("A2:O2")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("A1:D1")->getFont()->setBold(true)->setSize(16);
        $spreadsheet->getActiveSheet()->getStyle("A2:D2")->getFont()->setBold(true)->setSize(13);
        $spreadsheet->getActiveSheet()->getStyle("A3:G5")->getFont()->setBold(true)->setSize(12); 
        $spreadsheet->getActiveSheet()->getStyle("A9:A9")->getFont()->setBold(true)->setSize(12); 

        /* SET HEADER BG COLOR*/
        $spreadsheet->getActiveSheet()->getStyle('A6:O8')
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B'); 

        // $spreadsheet->getActiveSheet()->getStyle('A6:AC7')
        //         ->getFill()
        //         ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        //         ->getStartColor()
        //         ->setRGB('F2BE6B');
        // $spreadsheet->getActiveSheet()->getStyle('A6:AC7')
        //         ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLUE); 

        /* START INVOICE TITLE */
        $spreadsheet->getActiveSheet()->getStyle("A6:O6")->getFont()->setBold(true)->setSize(12);
        $spreadsheet->getActiveSheet()->getStyle("I7:N7")->getFont()->setBold(true)->setSize(12);
        $spreadsheet->getActiveSheet()->getStyle("A6:O6")->applyFromArray($outlineBorderStyle);
        /* START TITLE NO */
        $titleRowIdx = 6;
        $titleColIdx = 0;

        $spreadsheet->getActiveSheet()
                ->setCellValue('A6', 'NO')
                ->setCellValue('B6', 'NAME')
                ->setCellValue('C6', 'NO BADGE')
                ->setCellValue('D6', 'POSITION')
                ->setCellValue('E6', 'BASIC SALARY')
                ->setCellValue('F6', 'LEVEL')
                ->setCellValue('G6', 'SERVICE CHARGE RATE PER HOURS')
                ->setCellValue('H6', 'TOTAL HOURS / MONTH')
                ->setCellValue('I6', 'TOTAL SERVICE CHARGE')
                ->setCellValue('J6', 'ALLOWANCE EXPENSES (AT COST)')
                ->setCellValue('O6', 'TOTAL INVOICE PER MONTH');

        $spreadsheet->getActiveSheet()
        
                ->setCellValue('J7', 'TUNJANGAN KEHADIRAN')
                ->setCellValue('K7', 'ACOMODATION OUT CAMP')
                ->setCellValue('L7', 'SAFETY BONUS')
                ->setCellValue('M7', 'TUNJANGAN KERJA MALAM')
                ->setCellValue('N7', 'BONUS KONTRAK');

        $spreadsheet->getActiveSheet()
                ->setCellValue('A8', '1')
                ->setCellValue('B8', '2')
                ->setCellValue('C8', '3')
                ->setCellValue('D8', '4')
                ->setCellValue('E8', '5')
                ->setCellValue('F8', '6')
                ->setCellValue('G8', '7')
                ->setCellValue('H8', '8')
                ->setCellValue('I8', '9')
                ->setCellValue('J8', '10')
                ->setCellValue('K8', '11')
                ->setCellValue('L8', '12')
                ->setCellValue('M8', '13')
                ->setCellValue('N8', '14')
                ->setCellValue('O8', '15 =(9+10+11+12+13+14)');

        $spreadsheet->getActiveSheet()
                ->mergeCells("A6:A7")
                ->mergeCells("B6:B7")
                ->mergeCells("C6:C7")
                ->mergeCells("D6:D7")
                ->mergeCells("E6:E7")
                ->mergeCells("F6:F7")
                ->mergeCells("G6:G7")
                ->mergeCells("H6:H7")
                ->mergeCells("I6:I7")            
                ->mergeCells("O6:O7");

        $spreadsheet->getActiveSheet()
                ->mergeCells("J6:N6");

        $spreadsheet->getActiveSheet()->getStyle("A6:O8")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("A6:O8")->applyFromArray($center);
        // // /* END TITLE NO */

        $rowIdx = 10;
        $startIdx = $rowIdx; 
        $rowNo = 0;
        foreach ($query as $row) {
            $rowIdx++;
            $rowNo++;

            $chargeRate = 'SELECT `service_fee`  FROM `mst_payroll_level` WHERE `basic_salary` = "'.$row['basic_salary'].'" ';
            $serviceFee = $this->db->query($chargeRate)->row()->service_fee;  
            
            $totalService = $serviceFee * $row['hours_total'];

            $totalInvoice = $totalService + $row['other_allowance2'] + $row['flying_camp'] + $row['safety_bonus'] + $row['contract_bonus'] + $row['night_shift_bonus'];
            $basikAja = '';
           
            // if ($dept== 'CAT RENTAL'){
            //     $basikAja = $row['payroll_level'];
            // }
            // else{
            //     $basikAja = $row['basic_salary'];
            // }

            $spreadsheet->getActiveSheet()
                ->setCellValue('A'.($rowIdx), $rowNo)
                ->setCellValue('B'.($rowIdx), $row['name'])
                ->setCellValue('C'.($rowIdx), $row['nie'])
                ->setCellValue('D'.($rowIdx), $row['position'])
                ->setCellValue('E'.($rowIdx), $row['basic_salary'])
                ->setCellValue('F'.($rowIdx), $row['payroll_level'])
                ->setCellValue('G'.($rowIdx), $serviceFee)
                ->setCellValue('H'.($rowIdx), $row['hours_total'])
                ->setCellValue('I'.($rowIdx), $totalService)
                ->setCellValue('J'.($rowIdx), $row['other_allowance2'])
                ->setCellValue('K'.($rowIdx), $row['flying_camp'])
                ->setCellValue('L'.($rowIdx), $row['safety_bonus'])
                ->setCellValue('M'.($rowIdx), $row['night_shift_bonus'])
                ->setCellValue('N'.($rowIdx), $row['contract_bonus'])
                ->setCellValue('O'.($rowIdx), $totalInvoice);
            

             // SET ROW COLOR 
            if($rowIdx % 2 == 1)
            {
                $spreadsheet->getActiveSheet()->getStyle('A'.$rowIdx.':O'.$rowIdx)
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('EAEBAF');             
            } 
        //     /* END UPDATE TAX */
        } /* end foreach ($query as $row) */

        $spreadsheet->getActiveSheet()
                ->setCellValue('C'.($rowIdx+2), 'TOTAL NILAI TAGIHAN')
                ->setCellValue('H'.($rowIdx+2), '=SUM(H'.$startIdx.':H'.$rowIdx.')')
                ->setCellValue('I'.($rowIdx+2), '=SUM(I'.$startIdx.':I'.$rowIdx.')')
                ->setCellValue('J'.($rowIdx+2), '=SUM(J'.$startIdx.':J'.$rowIdx.')')
                ->setCellValue('K'.($rowIdx+2), '=SUM(K'.$startIdx.':K'.$rowIdx.')')
                ->setCellValue('L'.($rowIdx+2), '=SUM(L'.$startIdx.':L'.$rowIdx.')')
                ->setCellValue('M'.($rowIdx+2), '=SUM(M'.$startIdx.':M'.$rowIdx.')')
                ->setCellValue('N'.($rowIdx+2), '=SUM(N'.$startIdx.':N'.$rowIdx.')')
                ->setCellValue('O'.($rowIdx+2), '=SUM(O'.$startIdx.':O'.$rowIdx.')');

        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":O".($rowIdx+2))->getFont()->setBold(true)->setSize(12); 
        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":O".($rowIdx+2))->applyFromArray($outlineBorderStyle);

        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":O".($rowIdx+2))
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B');

        /* SET NUMBERS FORMAT*/
        $spreadsheet->getActiveSheet()->getStyle('D9:S'.($rowIdx+2))->getNumberFormat()->setFormatCode('#,##0.00');
        $spreadsheet->getActiveSheet()->getStyle('A1')->getAlignment()->setWrapText(true);     
        
        unset($allBorderStyle);
        unset($center);
        unset($right);
        unset($left);
        
        $spreadsheet->setActiveSheetIndex(0);

        $str = 'TRKSumInvoice';
        $fileName = preg_replace('/\s+/','', $str);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.Xlsx"');
        // header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        /* BY COMPOSER */
        // $writer = new Xlsx($spreadsheet);
        /* OFFLINE/ BY COPY EXCEL FOLDER  */
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit(0);   
    }
    /* END TRAKINDO SUMMARY INVOICE LIST EXPORT */

    public function getDataList($pt, $year, $month)
    {

        $strSQL  = " SELECT * ";
        $strSQL .= " FROM trn_slip_trksmb ";
        $strSQL .= " WHERE client_name = '".$pt."' "; 
        $strSQL .= " AND year_period = '".$year."' "; 
        $strSQL .= " AND month_period = '".$month."' "; 
        // $strSQL .= " AND dept = '".$dept."' "; 
        $strSQL .= " GROUP BY name ASC "; 
  
        $query = $this->db->query($strSQL)->result_array();
        // echo $this->db->last_query(); exit(0);
        /*return json_encode($query);*/
        $myData = array();
        foreach ($query as $key => $row) {
            $myData[] = array(
                $row['salary_slip_id'],         
                $pt,         
                $row['name'],                
                $row['dept'],         
                $row['position']         
            );            
        }  
        echo json_encode($myData);   
    }

    

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    // Load library phpspreadsheet
    require('./vendor/autoload.php');
    use PhpOffice\PhpSpreadsheet\Helper\Sample;
    use PhpOffice\PhpSpreadsheet\IOFactory;
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    // End load library phpspreadsheet

class Invoice_trksmb extends CI_Controller {
	
	public function index()
	{
		$this->load->view('welcome_message');
	}

     /* START TRAKINDO SUMMARY INVOICE LIST EXPORT */
    public function exportInvoiceSummaryTrakindo($clientName, $yearPeriod, $monthPeriod, $dept)
    {
        $dept = $this->security->xss_clean($dept);

        //membuat objek
        // $objPHPExcel = new PHPExcel();

        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();  

        $spreadsheet->getProperties()->setCreator('Maurice - Web - Android')
            ->setLastModifiedBy('Maurice - Web - Android')
            ->setTitle('Office 2007 XLSX Test Document')
            ->setSubject('Office 2007 XLSX Test Document')
            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Test result file');

        if (file_exists('assets/images/report_logo.png')) {
            $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing->setName('Logo');
            $drawing->setDescription('Logo');
            $drawing->setPath('./assets/images/report_logo.png');
            $drawing->setCoordinates('A1');
            $drawing->setHeight(36);
            $drawing->setWorksheet($spreadsheet->getActiveSheet());
        }

        // $strSQL = "";
        // $strFilter = "";
        // if($payrollGroup != 'L')
        // {
        //     $strFilter = " AND trk.payroll_group = '".$payrollGroup."' ";    
        // }

        $strSQL  = " SELECT ";   
        $strSQL .= "   ms.bio_rec_id, ms.nie, ss.position, ss.basic_salary, ss.bs_prorate,IFNULL(mp.service_fee,0)service_fee, IFNULL(mp.payroll_level,0)payroll_level, ss.flying_camp, ss.other_allowance2,ss.other_allowance3, ss.night_shift_bonus, ss.safety_bonus, ss.contract_bonus, ss.name,ss.ot_1,ss.ot_2,ss.ot_3,ss.ot_4,ss.health_bpjs,ss.emp_health_bpjs,ss.jp,ss.thr,ss.attendance_bonus, ";
        $strSQL .= "   TRUNCATE((ss.basic_salary/173),1) rate,
                       (ss.normal_time+ss.ot_count1+ss.ot_count2+ss.ot_count3+ss.ot_count4) hours_total,
                       (IFNULL(mp.service_fee,0) * (ss.normal_time+ss.ot_count1+ss.ot_count2+ss.ot_count3+ss.ot_count4)) total_charge_service ";
        $strSQL .= "   FROM trn_slip_trksmb ss ";
        $strSQL .= "   LEFT JOIN  mst_salary ms ON ms.bio_rec_id = ss.bio_rec_id ";
        $strSQL .= "   LEFT JOIN  mst_payroll_level mp ON ss.basic_salary = mp.basic_salary ";
        // $strSQL .= "   FROM trn_slip_trksmb ss ";
        // $strSQL .= "   WHERE ms.bio_rec_id = ss.bio_rec_id  ";
        // $strSQL .= "   AND ss.basic_salary = mp.basic_salary  ";
        $strSQL .= "   WHERE ss.client_name = '".$clientName."' ";       
        $strSQL .= "   AND ss.year_period = '".$yearPeriod."' ";       
        $strSQL .= "   AND ss.month_period = '".$monthPeriod."' ";       
        // $strSQL .= "   AND ss.payroll_group = '".$group."' "; 

        if($dept =='MIX'){
            $strSQL .= " AND ss.dept IN ('MAINTENANCE & SERVICE','SPECIAL PROJECT','SERVICE ACCOUNT','PLANNING','SAFETY') ";
        }elseif($dept =='ALL'){

        }else{
            $strSQL .= " AND ss.dept = '".$dept."' ";
        }      
        // $strSQL .= $strFilter;
        $strSQL .= " GROUP BY ss.name ASC ";    
        $query = $this->db->query($strSQL)->result_array();  

        $boldFont = [
            'font' => [
                'bold' => true
                // 'color' => ['argb' => '0000FF'],
            ],
        ];

        $totalStyle = [
            'font' => [
                'bold' => true,
                'color' => ['argb' => '0000FF'],
            ],
        ];

        $allBorderStyle = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $outlineBorderStyle = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $topBorderStyle = [
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $bottomBorderStyle = [
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        // foreach(range('B','Q') as $columnID)
        // {
        //     $spreadsheet->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        // }           

         // Nama Field Baris Pertama
        $spreadsheet->getActiveSheet()
                ->setCellValue('A1', 'SUMMARY INVOICE PT. SANGATI SOERYA SEJAHTERA - PT TRAKINDO UTAMA INDONESIA (SUMBAWA)')
                ->setCellValue('A2', 'PERIOD : '.$monthPeriod.'-'.$yearPeriod)
                ->setCellValue('A3', 'DATE        : ')
                ->setCellValue('A4', 'INVOICE NO  : ')
                ->setCellValue('A5', 'CONTRACT NO : ')
                ->setCellValue('A9', ''.$dept);

        $spreadsheet->getActiveSheet()->mergeCells("A1:U1");

        $spreadsheet->getActiveSheet()->getStyle("A1:S1")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->mergeCells("A2:U2");
        $spreadsheet->getActiveSheet()->getStyle("A2:S2")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("A1:D1")->getFont()->setBold(true)->setSize(16);
        $spreadsheet->getActiveSheet()->getStyle("A2:D2")->getFont()->setBold(true)->setSize(13);
        $spreadsheet->getActiveSheet()->getStyle("A3:G5")->getFont()->setBold(true)->setSize(12); 

        /* SET HEADER BG COLOR*/
        $spreadsheet->getActiveSheet()->getStyle('A6:U8')
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B'); 

        // $spreadsheet->getActiveSheet()->getStyle('A6:AC7')
        //         ->getFill()
        //         ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        //         ->getStartColor()
        //         ->setRGB('F2BE6B');
        // $spreadsheet->getActiveSheet()->getStyle('A6:AC7')
        //         ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLUE); 

        /* START INVOICE TITLE */
        $spreadsheet->getActiveSheet()->getStyle("A6:H6")->getFont()->setSize(12);
        $spreadsheet->getActiveSheet()->getStyle("A6:H6")->applyFromArray($outlineBorderStyle);
        /* START TITLE NO */
        $titleRowIdx = 6;
        $titleColIdx = 0;

        $spreadsheet->getActiveSheet()
                ->setCellValue('A6', 'NO')
                ->setCellValue('B6', 'NAME')
                ->setCellValue('C6', 'NO BADGE')
                ->setCellValue('D6', 'POSITION')
                ->setCellValue('E6', 'BASIC SALARY')
                ->setCellValue('F6', 'SALARY THIS MONTH')
                ->setCellValue('G6', 'OVERTIME')
                ->setCellValue('H6', 'BPJS KESEHATAN')
                ->setCellValue('I6', 'BPJS TENAGA KERJA')
                ->setCellValue('J6', 'BPJS PENSIUN')
                ->setCellValue('K6', 'TUNJANGAN HARI RAYA (THR)')
                ->setCellValue('L6', 'TOTAL GAJI, OT, BPJS & THR')
                ->setCellValue('M6', 'MANAGEMENT FEE')
                ->setCellValue('N6', 'ALLOWANCE EXPENSES  ( AT COST )')
                ->setCellValue('S6', 'TOTAL ALLOWANCE')
                ->setCellValue('T6', 'HOLDING TAX & HANDLING ADMIN COST')
                ->setCellValue('U6', 'TOTAL INVOICE PER MONTH');

        $spreadsheet->getActiveSheet()
                ->setCellValue('N7', 'ACCOMODATION OUT CAMP')
                ->setCellValue('O7', 'SAFETY BONUS')
                ->setCellValue('P7', 'TUNJANGAN KEHADIRAN')
                ->setCellValue('Q7', 'TUNJANGAN KERJA MALAM')
                ->setCellValue('R7', 'BONUS KONTRAK')
                ;

        $spreadsheet->getActiveSheet()
                ->setCellValue('A8', '1')
                ->setCellValue('B8', '2')
                ->setCellValue('C8', '3')
                ->setCellValue('D8', '4')
                ->setCellValue('E8', '5')
                ->setCellValue('F8', '6')
                ->setCellValue('G8', '7')
                ->setCellValue('H8', '8 = 5*4%')
                ->setCellValue('I8', '9 = 5*5,74%')
                ->setCellValue('J8', '10 = 5*2%')
                ->setCellValue('K8', '11')
                ->setCellValue('L8', '12 = 6+7+8+9+10+11')
                ->setCellValue('M8', '13 = 12*15%')
                ->setCellValue('N8', '14')
                ->setCellValue('O8', '15')
                ->setCellValue('P8', '16')
                ->setCellValue('Q8', '17')
                ->setCellValue('R8', '18')
                ->setCellValue('S8', '19 = 14+15+16+17+18')
                ->setCellValue('T8', '20 = 19*5%')
                ->setCellValue('U8', '21 = 12+13+19+20');

        $spreadsheet->getActiveSheet()
            ->mergeCells("A6:A7")
            ->mergeCells("B6:B7")
            ->mergeCells("C6:C7")
            ->mergeCells("D6:D7")
            ->mergeCells("E6:E7")
            ->mergeCells("F6:F7")
            ->mergeCells("G6:G7")
            ->mergeCells("H6:H7")
            ->mergeCells("I6:I7")
            ->mergeCells("J6:J7")
            ->mergeCells("K6:K7")
            ->mergeCells("L6:L7")
            ->mergeCells("M6:M7")
            // ->mergeCells("R6:R7")
            ->mergeCells("S6:S7")
            ->mergeCells("T6:T7")
            ->mergeCells("U6:U7")
        ;

        $spreadsheet->getActiveSheet()
            // ->mergeCells("D6:E6")
            ->mergeCells("N6:R6");

        $spreadsheet->getActiveSheet()->getStyle("A6:U8")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("A6:U8")->applyFromArray($center);
        // /* END TITLE NO */

        $rowIdx = 10;
        $startIdx = $rowIdx; 
        $rowNo = 0;
        foreach ($query as $row) {
            $rowIdx++;
            $rowNo++;

            $chargeRate = 'SELECT IFNULL(service_fee,0)service_fee FROM `mst_payroll_level` WHERE `basic_salary` = "'.$row['basic_salary'].'" ';
            $cekService = $this->db->query($chargeRate)->num_rows();
            if($cekService>=1){
                $serviceFee = $this->db->query($chargeRate)->row()->service_fee;
            }else{
                $serviceFee = '0';
            }
            // $serviceFee = $this->db->query($chargeRate)->row()->service_fee; 
            
            $totalService = $serviceFee * $row['hours_total'];

            $totalInvoice = $totalService + $row['other_allowance2'] + $row['flying_camp'] + $row['safety_bonus'] + $row['contract_bonus'] + $row['night_shift_bonus'];
           
            $basikAja = '';
            // if ($dept== 'CAT RENTAL'){
            //     $basikAja = $row['payroll_level'];
            // }
            // else{
            //     $basikAja = $row['basic_salary'];
            // }
            
            $holdingTaxAdmin    = 0;
            $bpjsTK             = ($row['basic_salary'] * (5.74/100));    
            $allOt              = $row['ot_1']+$row['ot_2']+$row['ot_3']+$row['ot_4'];
            $allGajiOtBpjsThr   = $row['basic_salary']+$allOt+$row['health_bpjs']+$bpjsTK+$row['jp']+$row['thr'];
            $allAllowance       = $row['flying_camp']+$row['safety_bonus']+$row['attendance_bonus']+$row['night_shift_bonus']+$row['contract_bonus']+$row['other_allowance3'];
            $managmntFee        = ($allGajiOtBpjsThr * (15/100));
            $holdingTaxAdmin    = ($allAllowance * (5/100));
            $allInvoicePerMonth = $allGajiOtBpjsThr+$managmntFee+$allAllowance+$holdingTaxAdmin;
            $spreadsheet->getActiveSheet()
                ->setCellValue('A'.($rowIdx), $rowNo)
                ->setCellValue('B'.($rowIdx), $row['name'])
                ->setCellValue('C'.($rowIdx), $row['nie'])
                ->setCellValue('D'.($rowIdx), $row['position'])
                ->setCellValue('E'.($rowIdx), $row['basic_salary'])
                ->setCellValue('F'.($rowIdx), $row['bs_prorate'])
                ->setCellValue('G'.($rowIdx), $allOt)
                ->setCellValue('H'.($rowIdx), $row['health_bpjs'])
                ->setCellValue('I'.($rowIdx), $bpjsTK)
                ->setCellValue('J'.($rowIdx), $row['jp'])
                ->setCellValue('K'.($rowIdx), $row['thr'])
                ->setCellValue('L'.($rowIdx), $allGajiOtBpjsThr)
                ->setCellValue('M'.($rowIdx), $managmntFee)
                ->setCellValue('N'.($rowIdx), $row['flying_camp'])
                ->setCellValue('O'.($rowIdx), $row['safety_bonus'])
                ->setCellValue('P'.($rowIdx), $row['attendance_bonus'])
                ->setCellValue('Q'.($rowIdx), $row['night_shift_bonus'])
                ->setCellValue('R'.($rowIdx), $row['contract_bonus'])
                ->setCellValue('S'.($rowIdx), $allAllowance)
                ->setCellValue('T'.($rowIdx), $holdingTaxAdmin)
                ->setCellValue('U'.($rowIdx), $allInvoicePerMonth);
            

             // SET ROW COLOR 
            if($rowIdx % 2 == 1)
            {
                $spreadsheet->getActiveSheet()->getStyle('A'.$rowIdx.':U'.$rowIdx)
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('EAEBAF');             
            } 
        //     /* END UPDATE TAX */
        } /* end foreach ($query as $row) */

        $spreadsheet->getActiveSheet()
                ->setCellValue('C'.($rowIdx+2), 'TOTAL NILAI TAGIHAN')
                ->setCellValue('H'.($rowIdx+2), '=SUM(H'.$startIdx.':H'.$rowIdx.')')
                ->setCellValue('I'.($rowIdx+2), '=SUM(I'.$startIdx.':I'.$rowIdx.')')
                ->setCellValue('J'.($rowIdx+2), '=SUM(J'.$startIdx.':J'.$rowIdx.')')
                ->setCellValue('K'.($rowIdx+2), '=SUM(K'.$startIdx.':K'.$rowIdx.')')
                ->setCellValue('L'.($rowIdx+2), '=SUM(L'.$startIdx.':L'.$rowIdx.')')
                ->setCellValue('M'.($rowIdx+2), '=SUM(M'.$startIdx.':M'.$rowIdx.')')
                ->setCellValue('N'.($rowIdx+2), '=SUM(N'.$startIdx.':N'.$rowIdx.')')
                ->setCellValue('O'.($rowIdx+2), '=SUM(O'.$startIdx.':O'.$rowIdx.')')
                ->setCellValue('T'.($rowIdx+2), '=SUM(T'.$startIdx.':T'.$rowIdx.')')
                ->setCellValue('U'.($rowIdx+2), '=SUM(U'.$startIdx.':U'.$rowIdx.')');

        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":U".($rowIdx+2))->getFont()->setBold(true)->setSize(12); 
        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":U".($rowIdx+2))->applyFromArray($outlineBorderStyle);

        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":U".($rowIdx+2))
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B');

        /* SET NUMBERS FORMAT*/
        $spreadsheet->getActiveSheet()->getStyle('D9:S'.($rowIdx+2))->getNumberFormat()->setFormatCode('#,##0.00');
        $spreadsheet->getActiveSheet()->getStyle('A1')->getAlignment()->setWrapText(true);     
        
        unset($allBorderStyle);
        unset($center);
        unset($right);
        unset($left);
        
        // $spreadsheet->setActiveSheetIndex(0);

         // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('TRKSumInvoice');

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);


        $str = 'TRKSumInvoice';
        $fileName = preg_replace('/\s+/','', $str);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.Xlsx"');
        // header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        /* BY COMPOSER */
        // $writer = new Xlsx($spreadsheet);
        /* OFFLINE/ BY COPY EXCEL FOLDER  */
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit(0);   
    }
    /* END TRAKINDO SUMMARY INVOICE LIST EXPORT */

    public function getDataList($clientName, $yearPeriod, $monthPeriod, $dept)
    {

        $dept     = $this->security->xss_clean($dept);

        $strSQL  = " SELECT * ";
        $strSQL .= " FROM trn_slip_trksmb ";
        $strSQL .= " WHERE client_name = '".$clientName."' "; 
        $strSQL .= " AND year_period = '".$yearPeriod."' "; 
        $strSQL .= " AND month_period = '".$monthPeriod."' "; 
        // $strSQL .= " AND payroll_group = '".$payrollGroup."' "; 
        if($dept =='MIX'){
            $strSQL .= " AND dept IN ('MAINTENANCE & SERVICE','SPECIAL PROJECT','SERVICE ACCOUNT','PLANNING','SAFETY') ";
        }elseif($dept =='ALL'){

        }else{
            $strSQL .= " AND dept= '".$dept."' ";
        }

   
        $strSQL .= " GROUP BY name ASC "; 
  
        $query = $this->db->query($strSQL)->result_array();
        // echo $this->db->last_query(); exit(0);
        /*return json_encode($query);*/
        $myData = array();
        foreach ($query as $key => $row) {
            $myData[] = array(
                $row['salary_slip_id'],         
                $clientName,         
                $row['name'],                
                $row['dept'],         
                $row['position']         
            );            
        }  
        echo json_encode($myData);   
    }

    

}

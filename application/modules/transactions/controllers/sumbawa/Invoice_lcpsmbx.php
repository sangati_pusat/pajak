<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    // Load library phpspreadsheet
    require('./vendor/autoload.php');
    use PhpOffice\PhpSpreadsheet\Helper\Sample;
    use PhpOffice\PhpSpreadsheet\IOFactory;
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    // End load library phpspreadsheet

class Invoice_lcpsmb extends CI_Controller {
	
	public function index()
	{
		$this->load->view('welcome_message');
	}

     public function getDataList($pt, $year, $month, $group)
    {

        $this->db->select('*');
        $this->db->from($this->db->database.'.trn_slip_lcpsmb');
        $this->db->where('client_name', $pt);
        // $this->db->where('payroll_group', $group);
        // if($pt == "Pontil_Sumbawa")
        // {
        //     if(isset($dept)){
        //         $dept = $_POST['dept'];
        //         $this->db->where('dept', $this->security->xss_clean($dept));
        //     }
        // }

        $this->db->where('year_period', $this->security->xss_clean($year));
        $this->db->where('month_period', $this->security->xss_clean($month));
        $this->db->order_by('name',"asc");
        $query = $this->db->get()->result_array();
        // echo $this->db->last_query(); exit(0);
        /*return json_encode($query);*/
        $myData = array();
        foreach ($query as $key => $row) {
            $myData[] = array(
                $row['salary_slip_id'],         
                $pt,         
                $row['name'],                  
                $row['dept'],         
                $row['position']         
              
            );            
        }  
        echo json_encode($myData);   
    }

    /* START PONTIL INVOICE LIST EXPORT */
    public function exportSummaryInvoiceLcp($clientName, $yearPeriod, $monthPeriod, $group)
    {
        // test('oke',1);
        //membuat objek
        // $objPHPExcel = new PHPExcel();

        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();  

        $spreadsheet->getProperties()->setCreator('Maurice - Web - Android')
            ->setLastModifiedBy('Maurice - Web - Android')
            ->setTitle('Office 2007 XLSX Test Document')
            ->setSubject('Office 2007 XLSX Test Document')
            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Test result file');

        if (file_exists('assets/images/report_logo.png')) {
            $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing->setName('Logo');
            $drawing->setDescription('Logo');
            $drawing->setPath('./assets/images/report_logo.png');
            $drawing->setCoordinates('A1');
            $drawing->setHeight(38);
            $drawing->setWorksheet($spreadsheet->getActiveSheet());
        }

        $strSQL = "";
        $strFilter = "";
        // if($payrollGroup != 'All')
        // {
        //     $strFilter = " AND ss.payroll_group = '".$payrollGroup."' ";    
        // }
        $dataGroup = '';
        if($group == 'L'){
            $dataGroup = 'LOCAL';
        }else if($group == 'N'){
            $dataGroup = 'NON LOCAL';
        }

        $strSQL  = " SELECT ";   
        $strSQL .= " ((ss.basic_salary+ss.ot_1+ss.ot_2+ss.ot_3+ss.ot_4+ss.regional_camp+ss.exp_camp+ss.attendance_bonus+ss.thr+ss.contract_bonus)+((ss.basic_salary*4)/100)+((ss.basic_salary*2.04)/100)+ss.adjust_in)-(ss.adjust_out+ss.unpaid_total) tot_kotor, ";
        $strSQL .= "   ms.bio_rec_id, ms.nie, ss.name, ss.position job_desc, ss.basic_salary,ss.bs_prorate, ss.act_manager_bonus,ss.thr,ss.contract_bonus,ss.regional_camp,ss.exp_camp,ss.health_bpjs,";
        $strSQL .= "   (ss.bs_prorate - unpaid_total) current_salary, ";
        $strSQL .= "   TRUNCATE((ss.basic_salary/173),1) rate, ";
        $strSQL .= "   ss.normal_time, ss.ot_count1, ss.ot_count2, ss.ot_count3, ss.ot_count4, ";
        $strSQL .= "   ss.ot_1, ss.ot_2, ss.ot_3, ss.ot_4, ";
        $strSQL .= "   (ss.normal_time+ss.ot_count1+ss.ot_count2+ss.ot_count3+ss.ot_count4) worked_hours, ss.travel_bonus, ss.attendance_bonus,";
        $strSQL .= "    TRUNCATE( (ss.normal_time)+(ss.ot_count1*1.5)+(ss.ot_count2*2)+(ss.ot_count3*3)+(ss.ot_count4*4),1 ) paid_hours, ";
        $strSQL .= "   (ot_1+ot_2+ot_3+ot_4) ot_total, ";
        $strSQL .= "   TRUNCATE((ot_1+ot_2+ot_3+ot_4)*(35/100),2) ot_bonus, ";
        $strSQL .= "   ss.travel_bonus, ss.shift_bonus, ss.incentive_bonus, ss.drilling_bonus, ss.act_manager_bonus, ss.production_bonus ";
        $strSQL .= "   FROM mst_salary ms, trn_slip_lcpsmb ss ";
        $strSQL .= "   WHERE ms.bio_rec_id = ss.bio_rec_id ";
        $strSQL .= "   AND ss.client_name = 'LCP_Sumbawa'  ";       
        $strSQL .= "   AND ss.year_period = '".$yearPeriod."' ";       
        $strSQL .= "   AND ss.month_period = '".$monthPeriod."' ";       
        $strSQL .= "   AND ss.payroll_group = '".$group."' ";       
        // $strSQL .= $strFilter;
        $strSQL .= "    ORDER BY ss.name  ;";    
        // echo $strSQL; exit(0);
        $query = $this->db->query($strSQL)->result_array();  

        
        $boldFont = [
            'font' => [
                'bold' => true
                // 'color' => ['argb' => '0000FF'],
            ],
        ];

        $totalStyle = [
            'font' => [
                'bold' => true,
                'color' => ['argb' => '0000FF'],
            ],
        ];

        $allBorderStyle = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $outlineBorderStyle = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $topBorderStyle = [
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $bottomBorderStyle = [
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        // foreach(range('B','Q') as $columnID)
        // {
        //     $spreadsheet->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        // }           

        // Nama Field Baris Pertama
        $spreadsheet->getActiveSheet()
                ->setCellValue('A1', 'SUMMARY INVOICE PT SANGATI SOERYA SEJAHTERA - PT LCP INDONESIA (SUMBAWA/NTB)')
                ->setCellValue('A2', 'PERIOD : '.$monthPeriod.'-'.$yearPeriod)
                ->setCellValue('A3', 'DATE        : ')
                ->setCellValue('A4', 'INVOICE NO  : ')
                ->setCellValue('A5', 'CONTRACT NO : ');

        $spreadsheet->getActiveSheet()->mergeCells("A1:W1");

        $spreadsheet->getActiveSheet()->getStyle("A1:S1")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->mergeCells("A2:X2");
        $spreadsheet->getActiveSheet()->getStyle("A2:S2")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("A1:D1")->getFont()->setBold(true)->setSize(16);
        $spreadsheet->getActiveSheet()->getStyle("A2:D2")->getFont()->setBold(true)->setSize(13);
        $spreadsheet->getActiveSheet()->getStyle("A3:G5")->getFont()->setBold(true)->setSize(12); 

        /* SET HEADER BG COLOR*/
        $spreadsheet->getActiveSheet()->getStyle('A6:X8')
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B'); 

        // $spreadsheet->getActiveSheet()->getStyle('A6:AC7')
        //         ->getFill()
        //         ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        //         ->getStartColor()
        //         ->setRGB('F2BE6B');
        // $spreadsheet->getActiveSheet()->getStyle('A6:AC7')
        //         ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLUE); 

        /* START INVOICE TITLE */
        $spreadsheet->getActiveSheet()->getStyle("A6:H6")->getFont()->setSize(12);
        $spreadsheet->getActiveSheet()->getStyle("A6:H6")->applyFromArray($outlineBorderStyle);
        /* START TITLE NO */
        $titleRowIdx = 6;
        $titleColIdx = 0;

        $spreadsheet->getActiveSheet()
                ->setCellValue('A6', 'NO')
                ->setCellValue('B6', 'NIE')
                ->setCellValue('C6', 'NAME')
                ->setCellValue('D6', 'POSITION')
                ->setCellValue('E6', 'PAYMENTS')
                ->setCellValue('G6', 'NT')
                ->setCellValue('H6', 'X 1.5')
                ->setCellValue('I6', 'X 2.0')
                ->setCellValue('J6', 'X 3.0')
                ->setCellValue('K6', 'X 4.0')
                ->setCellValue('L6', 'WORK HOURS')
                ->setCellValue('M6', 'SALARY THIS MONTH')
                // ->setCellValue('N6', 'OVER TIME')
                ->setCellValue('N6', 'ALLOWANCE PAYMENTS')
                ->setCellValue('Q6', 'THR')
                ->setCellValue('R6', 'CONTRACT BONUS')
                ->setCellValue('S6', 'BPJS KESEHATAN 4%')
                ->setCellValue('T6', 'BPJS TK 7.74%')
                ->setCellValue('U6', 'GROSS SALARY')
                ->setCellValue('V6', 'CONTRACTOR FEE 10%')
                ->setCellValue('W6', 'CONTRACTOR FEE 2%')
                ->setCellValue('X6', 'TOTAL INVOICE');

        $spreadsheet->getActiveSheet()
                ->setCellValue('E7', 'BASIC SALARY')
                ->setCellValue('F7', 'BASE HOUR')
                ->setCellValue('H7', 'HOURS')
                ->setCellValue('I7', 'HOURS')
                ->setCellValue('J7', 'HOURS')
                ->setCellValue('K7', 'HOURS')
                ->setCellValue('N7', 'CAMP EKSPLORASI')
                ->setCellValue('O7', 'CAMP REGIONAL')
                ->setCellValue('P7', 'OVERTIMES');

        $spreadsheet->getActiveSheet()
                ->setCellValue('A8', 'A')
                ->setCellValue('B8', 'B')
                ->setCellValue('C8', 'C')
                ->setCellValue('D8', 'D')
                ->setCellValue('E8', 'E')
                ->setCellValue('F8', 'F')
                ->setCellValue('G8', 'G')
                ->setCellValue('H8', 'H')
                ->setCellValue('I8', 'I')
                ->setCellValue('J8', 'J')
                ->setCellValue('K8', 'K')
                ->setCellValue('L8', 'L')
                ->setCellValue('M8', 'M')
                ->setCellValue('N8', 'N')
                ->setCellValue('O8', 'O')
                ->setCellValue('P8', 'P')
                ->setCellValue('Q8', 'Q')
                ->setCellValue('R8', 'R')
                ->setCellValue('S8', 'S')
                ->setCellValue('T8', 'T')
                ->setCellValue('U8', 'U')
                ->setCellValue('V8', 'V')
                ->setCellValue('W8', 'W')
                ->setCellValue('X8', '(M+N+O+P+Q+R+S+T+V+W)');

        $spreadsheet->getActiveSheet()
            ->mergeCells("A6:A7")
            ->mergeCells("B6:B7")
            ->mergeCells("C6:C7")
            ->mergeCells("D6:D7")
            ->mergeCells("G6:G7")
            ->mergeCells("L6:L7")
            ->mergeCells("M6:M7")
            // ->mergeCells("N6:N7")
            ->mergeCells("Q6:Q7")
            ->mergeCells("R6:R7")
            ->mergeCells("S6:S7")
            ->mergeCells("T6:T7")
            ->mergeCells("U6:U7")
            ->mergeCells("V6:V7")
            ->mergeCells("W6:W7")
            ->mergeCells("X6:X7");

        $spreadsheet->getActiveSheet()
            ->mergeCells("E6:F6")
            ->mergeCells("N6:P6");

        $spreadsheet->getActiveSheet()->getStyle("A6:X8")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("A6:X8")->applyFromArray($center);
        // /* END TITLE NO */
        // test('test',1);
        $rowIdx = 9;
        $startIdx = $rowIdx; 
        $rowNo = 0;
        foreach ($query as $row) {
            $rowIdx++;
            $rowNo++;

            $grossSalary         = $row['basic_salary'] /2 + $row['exp_camp'] + $row['regional_camp'] + $row['ot_total'];
            $bpjskes             = $row['basic_salary'] * 4/100;
            $bpjstk              = $row['basic_salary'] * 7.74/100;
            $contractFee10       = $row['current_salary'] + $row['ot_total'] + $bpjstk + $bpjskes + $row['thr'] * 10/100;
            $contractFee2        = $row['contract_bonus'] + $row['exp_camp'] + $row['regional_camp'] * 2/100;
           
            $invoiceTotal        = $row['current_salary'] + $row['exp_camp'] +  $row['regional_camp'] + $row['ot_total'] + $row['thr'] + $row['contract_bonus'] + $bpjstk +  $bpjskes + $contractFee2 + $contractFee10 ;
            // test($contractFee10,1);
            $spreadsheet->getActiveSheet()
                ->setCellValue('A'.($rowIdx), $rowNo)
                ->setCellValue('B'.($rowIdx), $row['nie'])
                ->setCellValue('C'.($rowIdx), $row['name'])
                ->setCellValue('D'.($rowIdx), $row['job_desc'])
                ->setCellValue('E'.($rowIdx), $row['basic_salary'])
                ->setCellValue('F'.($rowIdx), $row['rate'])
                ->setCellValue('G'.($rowIdx), $row['normal_time'])
                ->setCellValue('H'.($rowIdx), $row['ot_count1'])
                ->setCellValue('I'.($rowIdx), $row['ot_count2'])
                ->setCellValue('J'.($rowIdx), $row['ot_count3'])
                ->setCellValue('K'.($rowIdx), $row['ot_count4'])
                ->setCellValue('L'.($rowIdx), $row['worked_hours'])
                ->setCellValue('M'.($rowIdx), $row['current_salary'])
                ->setCellValue('N'.($rowIdx), $row['exp_camp'])
                ->setCellValue('O'.($rowIdx), $row['regional_camp'])
                ->setCellValue('P'.($rowIdx), $row['ot_total'])
                ->setCellValue('Q'.($rowIdx), $row['thr'])
                ->setCellValue('R'.($rowIdx), $row['contract_bonus'])
                ->setCellValue('S'.($rowIdx), $bpjskes) //BPJS KESEHATAN 
                ->setCellValue('T'.($rowIdx), $bpjstk) //BPJS TK
                ->setCellValue('U'.($rowIdx), $row['tot_kotor'])
                ->setCellValue('V'.($rowIdx), $contractFee10)
                ->setCellValue('W'.($rowIdx), $contractFee2)
                ->setCellValue('X'.($rowIdx), $invoiceTotal)
                ;

            /* SET ROW COLOR */
            if($rowIdx % 2 == 1)
            {
                $spreadsheet->getActiveSheet()->getStyle('A'.$rowIdx.':X'.$rowIdx)
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('EAEBAF');             
            } 
            /* END UPDATE TAX */
        } /* end foreach ($query as $row) */

        $spreadsheet->getActiveSheet()
                ->setCellValue('U'.($rowIdx+2), 'TOTAL')
                ->setCellValue('X'.($rowIdx+2), '=SUM(X'.$startIdx.':X'.$rowIdx.')');

        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":X".($rowIdx+2))->getFont()->setBold(true)->setSize(12); 
        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":X".($rowIdx+2))->applyFromArray($outlineBorderStyle);

        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":X".($rowIdx+2))
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B');

        /* SET NUMBERS FORMAT*/
        $spreadsheet->getActiveSheet()->getStyle('D9:X'.($rowIdx+2))->getNumberFormat()->setFormatCode('#,##0.00');
        $spreadsheet->getActiveSheet()->getStyle('A1')->getAlignment()->setWrapText(true);     
        
        unset($allBorderStyle);
        unset($center);
        unset($right);
        unset($left);
        
        $spreadsheet->setActiveSheetIndex(0);

        $str = 'LCPSmbInvoice';
        $fileName = preg_replace('/\s+/', '', $str);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.Xlsx"');
        // header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        /* BY COMPOSER */
        // $writer = new Xlsx($spreadsheet);
        /* OFFLINE/ BY COPY EXCEL FOLDER  */
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit(0);   
    }
    /* END PONTIL INVOICE LIST EXPORT */

}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    // Load library phpspreadsheet
    require('./vendor/autoload.php');
    use PhpOffice\PhpSpreadsheet\Helper\Sample;
    use PhpOffice\PhpSpreadsheet\IOFactory;
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    // End load library phpspreadsheet
 
class Timesheet extends CI_Controller {

    function __construct(){
        parent::__construct();
        // $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
        $this->load->model('M_timesheet'); 
        $this->load->model('M_overtime'); 
        $this->load->model('masters/M_mst_salary', 'M_salary'); 
        $this->load->model('masters/M_allowance'); 
        $this->load->model('masters/M_overtime'); 
        $this->load->model('M_salary_slip'); 
        $this->load->model('masters/M_process_closing'); 
        $this->load->model('masters/M_payroll_config'); 
    }

    public function index()
    {
        $this->load->view('excel');
    }

    public function upload()
    {
        $originalName = $_FILES['file']['name'];  
        // $shortName = substr($originalName, 0, 3);
        // $year = substr($originalName, 3, 4);
        // $month = substr($originalName, 7, 2);
        /* Database's Year */
        $dbYear = right($this->db->database,4);
        $shortName = substr($originalName, 0, 6);
        /* File's Year */
        $year = substr($originalName, 6, 4);
        $month = substr($originalName, 10, 2);
        $clientName = '';
        switch ($shortName) {            
            case 'MCMSMB':
                $clientName = 'Machmahon_Sumbawa';
                break;
            case 'PTLSMB':
                $clientName = 'Pontil_Sumbawa';
                break;    
            case 'LCPSMB':
                $clientName = 'LCP_Sumbawa';
                break;         
            default:
                $clientName = '';
                break;
        } 


        
        if($clientName == '')
        {
            echo 'Import Data Failed, Please Check File Format';
            exit(0);
        }

        $dataCount = $this->M_process_closing->getCountByClientPeriod($clientName, $year, $month);
        if($dataCount >= 1)
        {
            echo 'Data with the same period has been uploaded before';
            exit(0);
            // redirect('content/detail/ird');
        }
        // echo $dataCount;
        $fileDir = realpath(APPPATH.'../uploads/'); //buat folder dengan nama uploads di root folder 
        // $fileDir = '../uploads/'; //buat folder dengan nama uploads di root folder 
        // $fileDir = $_SERVER['DOCUMENT_ROOT']; //buat folder dengan nama uploads di root folder 
        // echo FCPATH; exit(0);
        $fileDir = FCPATH."/uploads/";

        $fileName = time().$originalName;         
        // $config['upload_path'] = './assets/'; //buat folder dengan nama assets di root folder
        $config['upload_path'] = $fileDir; 
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 10000;         

        $this->load->library('upload');
        $this->upload->initialize($config);         
        // $this->upload->do_upload('file');

        if(! $this->upload->do_upload('file'))
        {
            $this->upload->display_errors();
        }
             
        $media = $this->upload->data('file');
        /* WINDOWS PATH */
        // $inputFileName = $fileDir.'\\'.$fileName;
        $inputFileName = $fileDir.$fileName;
        // $inputFileName = $fileDir.'/'.$fileName;
        // echo $inputFileName; exit(0);
        try {
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                // $reader->setReadFilter( new MyReadFilter() );
                $objPHPExcel = $reader->load($inputFileName);
                // $inputFileType = IOFactory::identify($inputFileName);                
                // $objReader = IOFactory::createReader($inputFileType);
                // $objPHPExcel = $objReader->load($inputFileName);                

        } catch(Exception $e) {
            die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
        }
 
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
        $dataArr = array();

        $failedNames = array();
        $arrIdx = 0;
          
        $dataCount = 0;
        $noBiodataId = 0;

        $tData = '';
        $tmpData = '';
        $nie = '';
        /* Start Of TRANSACTION */  
        $this->db->trans_begin();
        /* START UPLOAD AMNT Sumbawa Staff, Trakindo Sumbawa */
        // if($clientName == 'Machmahon_Sumbawa')
        

        $dataCount = 0;
        $noBiodataId = 0;
        for ($row = 7; $row <= $highestRow; $row++)  //  Read a row of data into an array
        {
             $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                        NULL,
                                        TRUE,
                                        FALSE);
            
            $nie = $rowData[0][1];
            $nie = preg_replace('/[\r\n]+/','', $nie);
            $nie = trim($nie);
            if($nie != '')
            {
                $this->M_timesheet->resetValues();
                $this->M_timesheet->setRosterId($this->security->xss_clean($this->M_timesheet->GenerateNumber()));
                $salaryRow = $this->M_salary->loadBioIdByNie($nie);
                $bioId = $salaryRow['bio_rec_id'];                    

                $this->M_salary_slip->deleteByIdPeriod($bioId, $clientName, $year, $month);
                $str = $this->M_overtime->deleteByIdPeriod($bioId, $clientName, $year, $month);      
                $this->M_timesheet->deleteByIdPeriod($bioId, $clientName, $year, $month);                     

                // $colNumber = 1;
                // $tData .= $rowData[0][$colNumber]; 
                // $tData = preg_replace('/[\r\n]+/','', $tData);
                // $colNumber++;

                // $tData1 .= $rowData[0][$colNumber]; 
                // $tData1 = preg_replace('/[\r\n]+/','', $tData1);
                // $colNumber++;

                if($bioId == "" || $bioId == null){
                    $noBiodataId++;
                    /* GET ERROR DATA */
                    $failedNames[$arrIdx] = $rowData[0][2];
                    $arrIdx++;
                }else{
                    $this->M_timesheet->setBioRecId($this->security->xss_clean($bioId));
                }
                 
                $this->M_timesheet->setNie($rowData[0][1]);

                $dept = $sheet->getCell('A4')->getValue();
                $dept = preg_replace('/[\r\n]+/','', $dept);
                $dept = trim(strtoupper($dept));
                $this->M_timesheet->setDept($dept); 

                $tmpData .= $clientName;

                /* CANNOT BE NULL */
                $this->M_timesheet->setRosterBase($this->security->xss_clean('0'));
                $this->M_timesheet->setRosterFormat($this->security->xss_clean('0'));

                $this->M_timesheet->setClientName($this->security->xss_clean($clientName));
                $this->M_timesheet->setEmployeeName($this->security->xss_clean($rowData[0][2]));
                $myCurrentDate = GetCurrentDate();
                $curMonth = $myCurrentDate['CurrentMonth'];
                $curYear = $myCurrentDate['CurrentYear'];
                $this->M_timesheet->setMonthProcess($this->security->xss_clean($month));
                $this->M_timesheet->setYearProcess($this->security->xss_clean($year));

                $attendCount = 0;
                $colNumber = 4;
                $tData = $rowData[0][$colNumber]; 
                $tData = preg_replace('/[\r\n]+/','', $tData);
                $tData = trim(strtoupper($tData));
                if( ($tData == null) || (trim($tData) == "") ){
                    $this->M_timesheet->setD01($this->security->xss_clean("U"));   
                }else{
                    $this->M_timesheet->setD01($this->security->xss_clean($tData));
                }
                $colNumber++;
                 
                $tData = $rowData[0][$colNumber]; 
                $tData = preg_replace('/[\r\n]+/','', $tData);
                $tData = trim(strtoupper($tData));
                if( ($tData == null) || (trim($tData) == "") ){
                    $this->M_timesheet->setD02($this->security->xss_clean("U"));
                }else{
                    $this->M_timesheet->setD02($this->security->xss_clean($tData));
                }
                $colNumber++;


                $tData = $rowData[0][$colNumber]; 
                $tData = preg_replace('/[\r\n]+/','', $tData);
                $tData = trim(strtoupper($tData));
                if( ($tData == null) || (trim($tData) == "") ){
                    $this->M_timesheet->setD03($this->security->xss_clean("U"));
                }else{
                    $this->M_timesheet->setD03($this->security->xss_clean($tData));
                }
                $colNumber++;

                $tData = $rowData[0][$colNumber]; 
                $tData = preg_replace('/[\r\n]+/','', $tData);
                $tData = trim(strtoupper($tData));
                if( ($tData == null) || (trim($tData) == "") ){
                    $this->M_timesheet->setD04($this->security->xss_clean("U"));
                }else{
                    $this->M_timesheet->setD04($this->security->xss_clean($tData));
                }
                $colNumber++;

                $tData = $rowData[0][$colNumber]; 
                $tData = preg_replace('/[\r\n]+/','', $tData);
                $tData = trim(strtoupper($tData));
                if( ($tData == null) || (trim($tData) == "") ){
                    $this->M_timesheet->setD05($this->security->xss_clean("U"));
                }else{
                    $this->M_timesheet->setD05($this->security->xss_clean($tData));
                }
                $colNumber++;

                $tData = $rowData[0][$colNumber]; 
                $tData = preg_replace('/[\r\n]+/','', $tData);
                $tData = trim(strtoupper($tData));
                if( ($tData == null) || (trim($tData) == "") ){
                    $this->M_timesheet->setD06($this->security->xss_clean("U"));
                }else{
                    $this->M_timesheet->setD06($this->security->xss_clean($tData));
                }
                $colNumber++;

                $tData = $rowData[0][$colNumber]; 
                $tData = preg_replace('/[\r\n]+/','', $tData);
                $tData = trim(strtoupper($tData));
                if( ($tData == null) || ($tData == "") ){
                    $this->M_timesheet->setD07($this->security->xss_clean("U"));
                }else{
                    $this->M_timesheet->setD07($this->security->xss_clean($tData));
                }
                $colNumber++;

                $tData = $rowData[0][$colNumber]; 
                $tData = preg_replace('/[\r\n]+/','', $tData);
                $tData = trim(strtoupper($tData));
                if( ($tData == null) || (trim($tData) == "") ){
                    $this->M_timesheet->setD08($this->security->xss_clean("U"));
                }else{
                    $this->M_timesheet->setD08($this->security->xss_clean($tData));
                }
                $colNumber++;

                $tData = $rowData[0][$colNumber]; 
                $tData = preg_replace('/[\r\n]+/','', $tData);
                $tData = trim(strtoupper($tData));
                if( ($tData == null) || ($tData == "") ){
                    $this->M_timesheet->setD09($this->security->xss_clean("U"));
                }else{
                    $this->M_timesheet->setD09($this->security->xss_clean($tData));
                }
                $colNumber++;

                $tData = $rowData[0][$colNumber]; 
                $tData = preg_replace('/[\r\n]+/','', $tData);
                $tData = trim(strtoupper($tData));
                if( ($tData == null) || (trim($tData) == "") ){
                    $this->M_timesheet->setD10($this->security->xss_clean("U"));
                }else{
                    $this->M_timesheet->setD10($this->security->xss_clean($tData));
                }
                $colNumber++;

                $tData = $rowData[0][$colNumber]; 
                $tData = preg_replace('/[\r\n]+/','', $tData);
                $tData = trim(strtoupper($tData));
                if( ($tData == null) || (trim($tData) == "") ){
                    $this->M_timesheet->setD11($this->security->xss_clean("U"));
                }else{
                    $this->M_timesheet->setD11($this->security->xss_clean($tData));
                }
                $colNumber++;                         

                $tData = $rowData[0][$colNumber]; 
                $tData = preg_replace('/[\r\n]+/','', $tData);
                $tData = trim(strtoupper($tData));
                if( ($tData == null) || (trim($tData) == "") ){
                    $this->M_timesheet->setD12($this->security->xss_clean("U"));
                }else{
                    $this->M_timesheet->setD12($this->security->xss_clean($tData));
                }
                $colNumber++;

                $tData = $rowData[0][$colNumber]; 
                $tData = preg_replace('/[\r\n]+/','', $tData);
                $tData = trim(strtoupper($tData));
                if( ($tData == null) || (trim($tData) == "") ){
                    $this->M_timesheet->setD13($this->security->xss_clean("U"));
                }else{
                    $this->M_timesheet->setD13($this->security->xss_clean($tData));
                }
                $colNumber++;

                $tData = $rowData[0][$colNumber]; 
                $tData = preg_replace('/[\r\n]+/','', $tData);
                $tData = trim(strtoupper($tData));
                if( ($tData == null) || (trim($tData) == "") ){
                    $this->M_timesheet->setD14($this->security->xss_clean("U"));
                }else{
                    $this->M_timesheet->setD14($this->security->xss_clean($tData));
                }
                $colNumber++;

                $tData = $rowData[0][$colNumber]; 
                $tData = preg_replace('/[\r\n]+/','', $tData);
                $tData = trim(strtoupper($tData));
                if( ($tData == null) || (trim($tData) == "") ){
                    $this->M_timesheet->setD15($this->security->xss_clean("U"));
                }else{
                    $this->M_timesheet->setD15($this->security->xss_clean($tData));
                }
                $colNumber++;

                $tData = $rowData[0][$colNumber]; 
                $tData = preg_replace('/[\r\n]+/','', $tData);
                $tData = trim(strtoupper($tData));
                if( ($tData == null) || (trim($tData) == "") ){
                    $this->M_timesheet->setD16($this->security->xss_clean("U"));
                }else{
                    $this->M_timesheet->setD16($this->security->xss_clean($tData));
                }
                $colNumber++;

                $tData = $rowData[0][$colNumber]; 
                $tData = preg_replace('/[\r\n]+/','', $tData);
                $tData = trim(strtoupper($tData));
                if( ($tData == null) || (trim($tData) == "") ){
                    $this->M_timesheet->setD17($this->security->xss_clean("U"));
                }else{
                    $this->M_timesheet->setD17($this->security->xss_clean($tData));
                }
                $colNumber++;

                $tData = $rowData[0][$colNumber]; 
                $tData = preg_replace('/[\r\n]+/','', $tData);
                $tData = trim(strtoupper($tData));
                if( ($tData == null) || (trim($tData) == "") ){
                    $this->M_timesheet->setD18($this->security->xss_clean("U"));
                }else{
                    $this->M_timesheet->setD18($this->security->xss_clean($tData));
                }
                $colNumber++;

                $tData = $rowData[0][$colNumber]; 
                $tData = preg_replace('/[\r\n]+/','', $tData);
                $tData = trim(strtoupper($tData));
                if( ($tData == null) || (trim($tData) == "") ){
                    $this->M_timesheet->setD19($this->security->xss_clean("U"));
                }else{
                    $this->M_timesheet->setD19($this->security->xss_clean($tData));
                }
                $colNumber++;

                $tData = $rowData[0][$colNumber]; 
                $tData = preg_replace('/[\r\n]+/','', $tData);
                $tData = trim(strtoupper($tData));
                if( ($tData == null) || (trim($tData) == "") ){
                    $this->M_timesheet->setD20($this->security->xss_clean("U"));
                }else{
                    $this->M_timesheet->setD20($this->security->xss_clean($tData));
                }
                $colNumber++;

                $tData = $rowData[0][$colNumber]; 
                $tData = preg_replace('/[\r\n]+/','', $tData);
                $tData = trim(strtoupper($tData));
                if( ($tData == null) || ($tData == "") ){
                    $this->M_timesheet->setD21($this->security->xss_clean("U"));
                }else{
                    $this->M_timesheet->setD21($this->security->xss_clean($tData));
                }
                $colNumber++;

                $tData = $rowData[0][$colNumber]; 
                $tData = preg_replace('/[\r\n]+/','', $tData);
                $tData = trim(strtoupper($tData));
                if( ($tData == null) || (trim($tData) == "") ){
                    $this->M_timesheet->setD22($this->security->xss_clean("U"));
                }else{
                    $this->M_timesheet->setD22($this->security->xss_clean($tData));
                }
                $colNumber++;

                $tData = $rowData[0][$colNumber]; 
                $tData = preg_replace('/[\r\n]+/','', $tData);
                $tData = trim(strtoupper($tData));
                if( ($tData == null) || (trim($tData) == "") ){
                    $this->M_timesheet->setD23($this->security->xss_clean("U"));
                }else{
                    $this->M_timesheet->setD23($this->security->xss_clean($tData));
                }
                $colNumber++;

                $tData = $rowData[0][$colNumber]; 
                $tData = preg_replace('/[\r\n]+/','', $tData);
                $tData = trim(strtoupper($tData));
                if( ($tData == null) || (trim($tData) == "") ){
                    $this->M_timesheet->setD24($this->security->xss_clean("U"));
                }else{
                    $this->M_timesheet->setD24($this->security->xss_clean($tData));
                }
                $colNumber++;

                $tData = $rowData[0][$colNumber]; 
                $tData = preg_replace('/[\r\n]+/','', $tData);
                $tData = trim(strtoupper($tData));
                if( ($tData == null) || (trim($tData) == "") ){
                    $this->M_timesheet->setD25($this->security->xss_clean("U"));
                }else{
                    $this->M_timesheet->setD25($this->security->xss_clean($tData));
                }
                $colNumber++;

                $tData = $rowData[0][$colNumber]; 
                $tData = preg_replace('/[\r\n]+/','', $tData);
                $tData = trim(strtoupper($tData));
                if( ($tData == null) || (trim($tData) == "") ){
                    $this->M_timesheet->setD26($this->security->xss_clean("U"));
                }else{
                    $this->M_timesheet->setD26($this->security->xss_clean($tData));
                }
                $colNumber++;

                $tData = $rowData[0][$colNumber]; 
                $tData = preg_replace('/[\r\n]+/','', $tData);
                $tData = trim(strtoupper($tData));
                if( ($tData == null) || (trim($tData) == "") ){
                    $this->M_timesheet->setD27($this->security->xss_clean("U"));
                }else{
                    $this->M_timesheet->setD27($this->security->xss_clean($tData));
                }
                $colNumber++;

                $tData = $rowData[0][$colNumber]; 
                $tData = preg_replace('/[\r\n]+/','', $tData);
                $tData = trim(strtoupper($tData));
                if( ($tData == null) || (trim($tData) == "") ){
                    $this->M_timesheet->setD28($this->security->xss_clean("U"));
                }else{
                    $this->M_timesheet->setD28($this->security->xss_clean($tData));
                }
                $colNumber++;

                $tData = $rowData[0][$colNumber]; 
                $tData = preg_replace('/[\r\n]+/','', $tData);
                $tData = trim(strtoupper($tData));
                if( ($tData == null) || (trim($tData) == "") ){
                    $this->M_timesheet->setD29($this->security->xss_clean("U"));
                }else{
                    $this->M_timesheet->setD29($this->security->xss_clean($tData));
                }
                $colNumber++;

                $tData = $rowData[0][$colNumber]; 
                $tData = preg_replace('/[\r\n]+/','', $tData);
                $tData = trim(strtoupper($tData));
                if( ($tData == null) || (trim($tData) == "") ){
                    $this->M_timesheet->setD30($this->security->xss_clean("U"));
                }else{
                    $this->M_timesheet->setD30($this->security->xss_clean($tData));
                }
                $colNumber++;

                $tData = $rowData[0][$colNumber]; 
                $tData = preg_replace('/[\r\n]+/','', $tData);
                $tData = trim(strtoupper($tData));
                if( ($tData == null) || (trim($tData) == "") ){
                    $this->M_timesheet->setD31($this->security->xss_clean("U"));
                }else{
                    $this->M_timesheet->setD31($this->security->xss_clean($tData));
                }

                $this->M_timesheet->setIsActive($this->security->xss_clean('1'));
                $this->M_timesheet->setPicProcess($this->security->xss_clean($this->session->userdata('hris_user_id')));
                $myCurrentDate = GetCurrentDate();
                $curDateTime = $myCurrentDate['CurrentDateTime'];
                $this->M_timesheet->setProcessTime($this->security->xss_clean($curDateTime));
                $this->M_timesheet->insert();
                $dataCount++; 

            }                                
            
        } // for ($row = 2; $row <= $highestRow; $row++)   
        // echo $tmpData;                        

        /* End Of TRANSACTION */  
        if ($this->db->trans_status() === FALSE || $noBiodataId > 0)
        {
           $this->db->trans_rollback();
           // $this->session->set_flashdata('rosterMsg', 'Gagal Import Data, Cek Kembali Format File');
           $this->load->M_process_closing->resetValues();
           $this->load->M_timesheet->resetValues();     
           echo 'Unknown Data : '.$noBiodataId.', Import Data Failed, Please Check File Format';
        }
        else
        {
           $this->db->trans_commit();
           // $this->session->set_flashdata('rosterMsg', 'Import Data Berhasil : '.$dataCount.', Tanpa Biodata Id : '.$noBiodataId);
           echo 'Import Data Succeed : '.$dataCount;
        }
    }

    public function rosterProcess()
    {
        $biodataId = '';
        $badgeNo = '';
        $clientName = '';
        $dept = '';
        $month = '0';
        $year = '0';
        $rosterMaster = '';
        $rosterColumn = '';
        $ot01Column = '';
        $ot02Column = '';
        $ot03Column = '';
        $ot04Column = '';
        $isAlpaColumn = '';
        $isPermitColumn = '';
        $isSickColumn = '';
        $isEmergencyColumn = '';
        $offColumn = '';

        $otInOffCount1 = 0;

        $failMessage = "Process Failed";

        if(isset($_POST['biodataId']) && $this->security->xss_clean($_POST['biodataId']) == 'BySlipId')
        {
            /* SET BIODATA ID */
            $slipId = $this->security->xss_clean($_POST['slipId']);
            $this->M_salary_slip->getObjectById($slipId);            
            $biodataId = $this->M_salary_slip->getBioRecId();
            if($biodataId == ''){
                exit();
            }
        } 

        // if(isset($_POST['biodataId']))
        // {
        //     $biodataId = $this->security->xss_clean($_POST['biodataId']); 
        // }

        if(isset($_POST['clientName']))
        {
            $clientName = $this->security->xss_clean($_POST['clientName']); 
        }

        if(isset($_POST['dept']))
        {
            $dept = $this->security->xss_clean($_POST['dept']); 
        }

        if(isset($_POST['monthPeriod']))
        {
            $month = $this->security->xss_clean($_POST['monthPeriod']); 
        }

        if(isset($_POST['yearPeriod']))
        {
            $year = $this->security->xss_clean($_POST['yearPeriod']); 
        }

        if(isset($_POST['badgeNo']))
        {
            $badgeNo = $this->security->xss_clean($_POST['badgeNo']);
            $salaryRow = $this->M_salary->loadBioIdByNie($badgeNo);
            $biodataId = $salaryRow['bio_rec_id'];    
        }  
        // if(isset($_POST['otInOffCount']))
        // {
        //     $otInOffCount = $this->security->xss_clean($_POST['otInOffCount']); 
        // }

        /* DATA CHECKING IS HERE */
              

        $dataCount = $this->M_process_closing->getCountByClientPeriod($clientName, $year, $month);
        if($dataCount >= 1)
        {
            $failMessage = "Data with the same period has been processed before";
            echo $failMessage;
            exit(0);
        }

        $myDate = $year."-".$month."-"."01"; 
        $daysCountMonth = GetDbLastDayOfMonth($myDate);
        // echo $daysCountMonth; exit(0);
        // $tMonth = (int) $month;
        // $tYear = (int) $year;
        // $daysCountMonth = cal_days_in_month(CAL_GREGORIAN, $tMonth, $tYear);




        if($biodataId == "") {
            if($clientName == 'AMNT_Sumbawa' || $clientName == 'Trakindo_Sumbawa' || $clientName == 'Machmahon_Sumbawa')
            {
                $data = $this->M_timesheet->loadByClientPeriodDept($clientName, $year, $month, $dept);
            }
            else
            {
                $data = $this->M_timesheet->loadByClientPeriod($clientName, $year, $month);  
                // echo $this->db->last_query();              
            } 
        } else {
            $data = $this->M_timesheet->loadByIdClientPeriod($biodataId, $clientName, $year, $month); 
        }


        /* NILAI LEMBUR PERJAM (NLP) = GAJI POKOK/173 */
        $tmp51 = 0;
        $tmp52 = 0;
        $tmp61 = 0;
        $tmp62 = 0;
        $tmp63 = 0;
        $startDay = 0;

        /* START TRANSACTION */
        // $this->db->trans_start();
        $this->db->trans_begin();

        /* CLEAR DATA OVER TIME AND SALARY SLIP */
        // $this->M_overtime->deleteByPtPeriod($clientName, $year, $month);
        // $this->M_TrnAttendance->deleteByPtPeriod($clientName, $year, $month);
        // $this->M_salary_slip->deleteByPtPeriod($clientName, $year, $month);        
        
        /* END ROSTER AMNT SUMBAWA */
        /* START ROSTER TRAKINDO */
        if($clientName == 'AMNT_Sumbawa' || $clientName == 'Trakindo_Sumbawa' || $clientName == 'Machmahon_Sumbawa' || $clientName == 'Pontil_Sumbawa') 
        {     
            $tmp = '';

            /* START FOREACH */
            foreach ($data as $row) 
            {
                $st = '';
                $workDay = '';
                $offDay = '';           
                $bioId = $row['bio_rec_id'];

                /* CLEAR DATA OVER TIME AND SALARY SLIP */
                $this->M_salary_slip->deleteByIdPeriod($bioId, $clientName, $year, $month);
                $this->M_overtime->deleteByIdPeriod($bioId, $clientName, $year, $month);
                // $this->M_TrnAttendance->deleteByIdPeriod($bioId, $clientName, $year, $month);
            // echo "Hello"; exit(0);       

                $rosterFormat = '';
                $z = 0;
                $dayIndex = 0;

                $offAttendCount = 0;

                $otCount1 = 0;
                $otCount2 = 0;
                $otCount3 = 0;
                $otCount4 = 0;
                
                $offCount = 0;
                $emergencyCount = 0;
                $sickCount = 0;
                $unpaidPermitCount = 0;
                $unpaidCount = 0;
                $attendCount = 0;
                $attendPHCount = 0; /* Attend in Public Holiday */
                $phCountInMonth = 0; /* Public Holiday Count */
                $vacationDate = '';
                $vacationCount = 0;
                
                $standByCount = 0;
                $paidPermitCount = 0;
                $paidVacation = 0;
                
                $unpaidDate = '';
                $otTmp = 0; 
                $wdCode = '';

                $ntDefault = 0;
                $startOT = 0;

                $sixthDayTmp = 0;                
                $this->M_overtime->resetValues();
                // $this->M_TrnAttendance->resetValues();
                /* START SHIFT BREAKDOWN */
            // for ($x=1; $x <= $rosterMasterCount; $x++) 
            // {       
                /* OVERTIME VARIABLE  */
                $otInOffCount1 = $_POST['otInOffCount'];
                $otInOffCount2 = $otInOffCount1 + 1;
                $otInOffCount3 = $otInOffCount2 + 1;

                /* START GET WORK DAYS BY SHIFT */
                for ($wd=1; $wd <= $daysCountMonth; $wd++) { 
                    $dayIndex++; 
                    if($dayIndex < 10){
                        $rosterColumn = 'd0'.$dayIndex; 
                        $ot01Column = 'setOt1D0'.$dayIndex;    
                        $ot02Column = 'setOt2D0'.$dayIndex;    
                        $ot03Column = 'setOt3D0'.$dayIndex;    
                        $ot04Column = 'setOt4D0'.$dayIndex;  
                        $isAlpaColumn = 'setIsAlpa0'.$dayIndex;  
                        $isPermitColumn = 'setIsPermit0'.$dayIndex;  
                        $isSickColumn = 'setIsSick0'.$dayIndex;  
                        $isEmergencyColumn = 'setIsEmergency0'.$dayIndex;  
                        $offColumn = 'setIsOff0'.$dayIndex;  
                    }
                    else {
                        $rosterColumn = 'd'.$dayIndex;       
                        $ot01Column = 'setOt1D'.$dayIndex;    
                        $ot02Column = 'setOt2D'.$dayIndex;    
                        $ot03Column = 'setOt3D'.$dayIndex;    
                        $ot04Column = 'setOt4D'.$dayIndex;    
                        $isAlpaColumn = 'setIsAlpa'.$dayIndex;  
                        $isPermitColumn = 'setIsPermit'.$dayIndex;  
                        $isSickColumn = 'setIsSick'.$dayIndex;  
                        $isEmergencyColumn = 'setIsEmergency'.$dayIndex;  
                        $offColumn = 'setIsOff'.$dayIndex;  
                    }                       
                    $otTmp = trim($row[$rosterColumn]);
                    /* PUBLIC HOLIDAY */
                    $shiftCodeTmp = substr($otTmp,0,2);
                    $shiftHoursTmp = substr($otTmp,2,strlen($otTmp)-2);
                    $st .= $shiftCodeTmp; 
                    if( (strtoupper($shiftCodeTmp) == "PH") )
                    {
                        if( (is_numeric($shiftHoursTmp)) && ($shiftHoursTmp > 0) )
                        {
                            $attendPHCount++;
                        } 
                        $phCountInMonth++;
                    }

                    if( ($clientName == 'AMNT_Sumbawa' || $clientName == 'Machmahon_Sumbawa' || $clientName == 'Pontil_Sumbawa') && (strtoupper($shiftCodeTmp) == "SP") )
                    {
                        if( (is_numeric($shiftHoursTmp)) && ($shiftHoursTmp > 0) )
                        {
                            $attendPHCount++;
                        } 
                        $phCountInMonth++;
                    }

                    if( ($clientName == 'Trakindo_Sumbawa') && (strtoupper($shiftCodeTmp) == "PN") )
                    {
                        if( (is_numeric($shiftHoursTmp)) && ($shiftHoursTmp > 0) )
                        {
                            $attendPHCount++;
                        } 
                        $phCountInMonth++;
                    }

                    if( (strtoupper($shiftCodeTmp) == "RO") )
                    {
                        if( (is_numeric($shiftHoursTmp)) && ($shiftHoursTmp > 0) )
                        {
                            $attendCount++;
                            $offAttendCount++;
                        } 
                        $offCount++;
                    }

                    if( ($clientName == 'Trakindo_Sumbawa') && (strtoupper($shiftCodeTmp) == "RN") )
                    {
                        if( (is_numeric($shiftHoursTmp)) && ($shiftHoursTmp > 0) )
                        {
                            $attendCount++;
                            $offAttendCount++;
                        } 
                        $offCount++;
                    }
                    /*if($shiftCodeTmp == 'SD')
                    {
                        echo $otTmp; exit(0); 
                    }*/
                    /* EXCLUDE OF DATA IN ARRAY WILL BE TREATED AS  */
                    $arrTmpCode = array('PH','SB','PV','PP','RO','NS','ID','RN','PN');
                    if($clientName == 'AMNT_Sumbawa' || $clientName == 'Machmahon_Sumbawa' || $clientName == 'Pontil_Sumbawa') {
                        $arrTmpCode = array('PH','SB','PV','PP','RO','NS','SD','ID','SP');
                    }
                    $arrOtTmp = array('A','U','I','S','V', 'E', 'STR', 'END');
                    if( (!in_array( strtoupper($otTmp), $arrOtTmp)) && (!in_array( strtoupper($shiftCodeTmp), $arrTmpCode)) && !is_numeric($otTmp))
                    {
                        $tmp .= $otTmp;                             
                        $unpaidCount++;
                        if($clientName == 'AMNT_Sumbawa' || $clientName == 'Machmahon_Sumbawa' || $clientName == 'Pontil_Sumbawa'){
                            $unpaidDate .= '6';
                        }
                        else if( $clientName == 'Trakindo_Sumbawa' ){
                            $unpaidDate .= '5';                                
                        }
                    }

                    /* RESET OVERTIME PROPERTIES IN WORKDAY */
                    $this->M_overtime->$ot01Column(0);
                    $this->M_overtime->$ot02Column(0);
                    $this->M_overtime->$ot03Column(0);
                    $this->M_overtime->$ot04Column(0);                        
                                            
                    /* GET ATTEND COUNT SHIFT DAY */
                    if(is_numeric($otTmp) && $otTmp > 0)
                    {
                        $attendCount++;

                        if($clientName == 'AMNT_Sumbawa' || $clientName == 'Machmahon_Sumbawa') {
                            # 7 JAM PERTAMA NORMAL    
                            # JAM KE 8 (LEMBUR 1 x 1.5)     
                            # JAM KE >= 9 (LEMBUR 1 x 2)
                            $ntDefault = $otInOffCount1;
                            // $ntDefault = 7;
                        }
                        if($clientName == 'Trakindo_Sumbawa' || $clientName == 'Pontil_Sumbawa') {
                            # 8 JAM PERTAMA NORMAL    
                            # JAM KE 9 (LEMBUR 1 x 1.5)     
                            # JAM KE >= 10 (LEMBUR 1 x 2)
                            $ntDefault = $otInOffCount1;
                            // $ntDefault = 8;
                        }
                        $startOT = $ntDefault + 1;

                        if(is_numeric($otTmp)) {
                            if($otTmp > $ntDefault) {
                                if($otTmp < $startOT){
                                    $tVal = $startOT - $otTmp;
                                } else {
                                    $tVal = 1;
                                }
                                $this->M_overtime->$ot01Column($tVal);
                                $otCount1 += $tVal;
                            }
                            if($otTmp > $startOT) {
                                $tVal = $otTmp - $startOT;
                                $otCount2 += $tVal;
                                $this->M_overtime->$ot02Column($tVal);
                            }     
                        } 
                    }   

                    /* GET STAND BY COUNT */
                    else if( strtoupper($otTmp) == "STR" || strtoupper($otTmp) == "END")
                    {
                        $standByCount++;
                    }

                    /* START PUBLIC HOLIDAY  */                    
                    else if( (strtoupper($shiftCodeTmp) == "PH" || strtoupper($shiftCodeTmp) == "RO") && (is_numeric($shiftHoursTmp)) && ($shiftHoursTmp > 0) )
                    {
                        if($shiftHoursTmp >= $otInOffCount1){
                            $this->M_overtime->$ot02Column($otInOffCount1);
                            $otCount2 += $otInOffCount1;
                        }else{
                            $this->M_overtime->$ot02Column($shiftHoursTmp);
                            $otCount2 += $shiftHoursTmp;
                        }

                        if($shiftHoursTmp > $otInOffCount1){
                            if($shiftHoursTmp < $otInOffCount2){
                                $tVal = $otInOffCount2 - $shiftHoursTmp; 
                            } else {
                                $tVal = 1;
                            }

                            $this->M_overtime->$ot03Column($tVal);
                            $otCount3 += $tVal;
                        }

                        if($shiftHoursTmp > $otInOffCount2){
                            $tVal = $shiftHoursTmp - $otInOffCount2;
                            $this->M_overtime->$ot04Column($tVal);
                            $otCount4 += $tVal;
                        }
                    }


                    /* START PUBLIC HOLIDAY SIXTH DAY */                    
                    else if( (strtoupper($shiftCodeTmp) == "SP") )
                    {
                        if($clientName == 'AMNT_Sumbawa' || $clientName == 'Machmahon_Sumbawa' || $clientName == 'Pontil_Sumbawa') {
                            if(is_numeric($shiftHoursTmp))
                            {                                
                                if ($shiftHoursTmp > 0)
                                {                            
                                /* PUBLIC HOLIDAY IN SIXTH DAY */
                                    # JAM KE 5 (LEMBUR 1 x 1)     
                                    # JAM KE >= 6 (LEMBUR 1 x 2)
                                    # JAM KE >= 7 (LEMBUR 1 x 2)
                                    $otInPHSixth1 = 5;
                                    $otInPHSixth2 = $otInPHSixth1 + 1;
                                    $otInPHSixth3 = $otInPHSixth2 + 1;
                                    if($shiftHoursTmp >= $otInPHSixth1){
                                        $this->M_overtime->$ot02Column($otInPHSixth1);
                                        $otCount2 += $otInPHSixth1;
                                    }else{
                                        $this->M_overtime->$ot02Column($shiftHoursTmp);
                                        $otCount2 += $shiftHoursTmp;
                                    }

                                    if($shiftHoursTmp > $otInPHSixth1){
                                        if($shiftHoursTmp < $otInPHSixth2){
                                            $tVal = $$otInPHSixth2 - $shiftHoursTmp; 
                                        } else {
                                            $tVal = 1;
                                        }

                                        $this->M_overtime->$ot03Column($tVal);
                                        $otCount3 += $tVal;
                                    }

                                    if($shiftHoursTmp > $otInPHSixth2){
                                        $tVal = $shiftHoursTmp - $otInPHSixth2;
                                        $this->M_overtime->$ot04Column($tVal);
                                        $otCount4 += $tVal;
                                    }
                                }                        
                                else {
                                    $unpaidCount++;
                                    $unpaidDate .= '6';
                                }
                            }
                            else {
                                $unpaidCount++;
                                $unpaidDate .= '6';
                            }
                        }
                    }                        
                    /* END PUBLIC HOLIDAY SIXTH DAY */
                    /* START PUBLIC HOLIDAY  */                    
                    else if( ($clientName == 'Trakindo_Sumbawa') && (strtoupper($shiftCodeTmp) == "PN" || strtoupper($shiftCodeTmp) == "RN") && (is_numeric($shiftHoursTmp)) && ($shiftHoursTmp > 0) )
                    {
                        /*  */
                        $otInOffCountTmp1 = $otInOffCount1;    
                        $otInOffCountTmp2 = $otInOffCount2;    
                        if($shiftHoursTmp >= $otInOffCountTmp1){
                            $this->M_overtime->$ot02Column($otInOffCountTmp1);
                            $otCount2 += $otInOffCountTmp1;
                        }else{
                            $this->M_overtime->$ot02Column($shiftHoursTmp);
                            $otCount2 += $shiftHoursTmp;
                        }

                        if($shiftHoursTmp > $otInOffCountTmp1){
                            if($shiftHoursTmp < $otInOffCountTmp2){
                                $tVal = $otInOffCountTmp2 - $shiftHoursTmp; 
                            } else {
                                $tVal = 1;
                            }

                            $this->M_overtime->$ot03Column($tVal);
                            $otCount3 += $tVal;
                        }

                        if($shiftHoursTmp > $otInOffCountTmp2){
                            $tVal = $shiftHoursTmp - $otInOffCountTmp2;
                            $this->M_overtime->$ot04Column($tVal);
                            $otCount4 += $tVal;
                        }
                    }  

                    else if( (strtoupper($shiftCodeTmp) == "NS") && (is_numeric($shiftHoursTmp)) && ($shiftHoursTmp > 0) )
                    {                        
                        $attendCount++;
                        if($clientName == 'AMNT_Sumbawa' || $clientName == 'Machmahon_Sumbawa' || $clientName == 'Pontil_Sumbawa') {
                            # 7 JAM PERTAMA NORMAL    
                            # JAM KE 8 (LEMBUR 1 x 1.5)     
                            # JAM KE >= 9 (LEMBUR 1 x 2)
                            $ntDefault = 7;
                        }
                        if($clientName == 'Trakindo_Sumbawa') {
                            # 8 JAM PERTAMA NORMAL    
                            # JAM KE 9 (LEMBUR 1 x 1.5)     
                            # JAM KE >= 10 (LEMBUR 1 x 2)
                            $ntDefault = 7;
                        }
                        $startOT = $ntDefault + 1;

                        if(is_numeric($shiftHoursTmp)){
                            if($shiftHoursTmp > $ntDefault){
                                if($shiftHoursTmp < $startOT){
                                    $tVal = $startOT - $shiftHoursTmp;
                                } else {
                                    $tVal = 1;
                                }
                                $this->M_overtime->$ot01Column($tVal);
                                $otCount1 += $tVal;
                            }
                            if($shiftHoursTmp > $startOT){
                                $tVal = $shiftHoursTmp - $startOT;
                                $otCount2 += $tVal;
                                $this->M_overtime->$ot02Column($tVal);
                            }     
                        } 
                    }
                    /* THE SIXTH DAY */
                    else if( (strtoupper($shiftCodeTmp) == "SD")  )
                    {
                        if($clientName == 'AMNT_Sumbawa' || $clientName == 'Machmahon_Sumbawa' || $clientName == 'Pontil_Sumbawa')
                        {                            
                            if (is_numeric($shiftHoursTmp))
                            {                            
                                if( $shiftHoursTmp > 0 )
                                {
                                    # 5 JAM PERTAMA NORMAL    
                                    # JAM KE 6 (LEMBUR 1 x 1.5)     
                                    # JAM KE >= 7 (LEMBUR 1 x 2)
                                    $attendCount++;
                                    if(is_numeric($shiftHoursTmp)){
                                        if($shiftHoursTmp > 5){
                                            if($shiftHoursTmp < 6){
                                                $tVal = 6 - $shiftHoursTmp;
                                            } else {
                                                $tVal = 1;
                                            }
                                            $this->M_overtime->$ot01Column($tVal);
                                            $otCount1 += $tVal;
                                        }
                                        if($shiftHoursTmp > 6){
                                            $tVal = $shiftHoursTmp - 6;
                                            $otCount2 += $tVal;
                                            $this->M_overtime->$ot02Column($tVal);
                                        }     
                                    } 
                                }
                                else {
                                    $unpaidCount++;
                                    $unpaidDate .= '6';
                                }
                            }
                            else {
                                $unpaidCount++;
                                $unpaidDate .= '6';
                            }                        
                        }
                    }                     

                    /* GET PAID PERMIT COUNT */
                    else if( strtoupper($shiftCodeTmp) == "ID")
                    {
                        $paidPermitCount++;
                    }

                    /* GET PAID VACATION COUNT */
                    else if( strtoupper($shiftCodeTmp) == "PV")
                    {
                        $paidVacation++;
                    }                        

                    /* GET ALPA COUNT OFF DAY */
                    else if( strtoupper($otTmp) == "A" || strtoupper($otTmp) == "U" )
                    {
                        $unpaidCount++;
                        if($clientName == 'AMNT_Sumbawa' || $clientName == 'Machmahon_Sumbawa' || $clientName == 'Pontil_Sumbawa'){
                            $unpaidDate .= '6';
                        }
                        else if( $clientName == 'Trakindo_Sumbawa' ){
                            $unpaidDate .= '5';                                
                        }                   
                    }

                    /* GET PERMIT COUNT OFF DAY */
                    else if( strtoupper($otTmp) == "I")
                    {
                        $unpaidPermitCount++;
                        $unpaidCount++;
                        if($clientName == 'AMNT_Sumbawa' || $clientName == 'Machmahon_Sumbawa' || $clientName == 'Pontil_Sumbawa'){
                            $unpaidDate .= '6';
                        }
                        else if( $clientName == 'Trakindo_Sumbawa' ){
                            $unpaidDate .= '5';                                
                        }   
                    }

                    /* GET SICK COUNT OFF DAY */
                    else if( strtoupper($otTmp) == "S")
                    {
                        $sickCount++;
                    }

                    /* GET VACATION COUNT OFF DAY */
                    else if( strtoupper($otTmp) == "V")
                    {
                        $vacationDate .= $dayIndex.'.';
                        $vacationCount++;
                    } 

                    /* GET ALPA COUNT */
                    else if($otTmp == '0' || $otTmp == '')
                    {
                        $unpaidCount++;
                        if($clientName == 'AMNT_Sumbawa' || $clientName == 'Machmahon_Sumbawa' || $clientName == 'Pontil_Sumbawa'){
                            $unpaidDate .= '6';
                        }
                        else if( $clientName == 'Trakindo_Sumbawa' ){
                            $unpaidDate .= '5';                                
                        }                         
                    }

                    else if( strtoupper($otTmp) == "E")
                    {
                        $emergencyCount++;
                    }
                                                                                  
                }
                /* END GET WORK DAYS BY SHIFT */           
                
            // } 
                /* for ($x=1; $x <= $rosterMasterCount; $x++) */
                /* END SHIFT BREAKDOWN */       

                /* START SET DATA INSERT TRN_OVERTIME */
                $otId = $this->M_overtime->GenerateNumber();                 
                $this->M_overtime->setOvertimeId($otId);
                $this->M_overtime->setBioRecId($row['bio_rec_id']);
                $this->M_overtime->setClientName($clientName);
                $this->M_overtime->setRosterId($row['roster_id']);
                $this->M_overtime->setYearPeriod($row['year_process']);
                $this->M_overtime->setMonthPeriod($row['month_process']);                
                /* Start Dynamic Data*/
                
                $this->M_overtime->setOffTotal($offCount);                
                $this->M_overtime->setEmergencyTotal($emergencyCount);                
                $this->M_overtime->setSickTotal($sickCount);                
                $this->M_overtime->setPermitTotal($unpaidPermitCount);                
                $this->M_overtime->setAlpaTotal($unpaidCount);                
                $this->M_overtime->setAttendTotal($attendCount);                
                $this->M_overtime->setUnpaidDays($unpaidDate);                
                $this->M_overtime->setVacationDays($vacationDate);  
                $this->M_overtime->setVacationTotal($vacationCount);  
                $this->M_overtime->setAttendInOff($offAttendCount);                
                $this->M_overtime->setPhTotal($phCountInMonth);                
                $this->M_overtime->setInPhTotal($attendPHCount);              
                $this->M_overtime->setStandbyTotal($standByCount);                
                $this->M_overtime->setPaidVacationTotal($paidVacation);                
                $this->M_overtime->setPaidPermitTotal($paidPermitCount);                
                /* End Dynamic Data*/                
                $this->M_overtime->setIsActive($row['is_active']);
                $this->M_overtime->setPicProcess($this->session->userdata('hris_user_id'));
                $myCurrentDate = GetCurrentDate();
                $this->M_overtime->setProcessTime($myCurrentDate['CurrentDateTime']);
                $this->M_overtime->insert();           

            } /* END FOREACH */
            /* COMMIT TRANSACTION */

            $this->payrollProcess($biodataId, $clientName, $dept, $year, $month);
            $this->allowanceProcess($clientName, $year, $month);
            $this->taxProcess($clientName, $year, $month, $dept);
            // exit(0);
            $this->db->trans_complete();
            if($this->db->trans_status() === FALSE)
            {
              // $this->session->set_flashdata('rosterMsg', 'Update Data Gagal');
                echo $failMessage;
            }
            else
            {
              // $this->session->set_flashdata('rosterMsg', 'Update Data Berhasil');
                // echo "Process Succeed...";
                echo $this->getPayrollList($clientName, $dept, $year, $month);
            } 
        }
        /* END ROSTER TRAKINDO */
    }

    // public function payrollProcess()
    public function payrollProcess($biodataId, $clientName, $department, $yearPeriod, $monthPeriod)
    {        
        $tst = ''; 
        $year = '';
        $month = '';
        $dept = $this->security->xss_clean($department); 
        if($clientName == 'AMNT_Sumbawa' || $clientName == 'Trakindo_Sumbawa' || $clientName == 'Machmahon_Sumbawa' || $clientName == 'Pontil_Sumbawa')
        {
            $queryFilter = '';
            if($biodataId != '')
            {
                $queryFilter = 'AND mr.bio_rec_id = "'.$biodataId.'" ';    
            }            

            $this->db->select('mr.*');
            $this->db->select('tro.*');
            $this->db->select('mbr.bio_rec_id, mbr.full_name, mbr.marital_status, mbr.npwp_no, mbr.position,mr.roster_id, mr.roster_base, mr.dept');
            $this->db->select('tro.attend_total, tro.attend_in_off, tro.ph_total, tro.in_ph_total, tro.vacation_days, tro.permit_total');
            $this->db->select('ms.basic_salary,ms.positional_allowance,ms.transport_housing,ms.is_remote_allowance,ms.is_shift_bonus,ms.payroll_group');
            $this->db->select('ms.is_allowance_economy,ms.is_ot_bonus,ms.is_incentive_bonus,ms.is_dev_incentive_bonus, ms.is_overtime, ms.is_travel,ms.salary_level');
            $this->db->select('d01,d02,d03,d04,d05,d06,d07,d08,d09,d10,d11,d12,d13,d14,d15,d16,d17,d18,d19,d20,d21,d22,d23,d24,d25,d26,d27,d28,d29,d30,d31');
            $this->db->from('mst_salary ms, mst_bio_rec mbr');
            $this->db->join('mst_roster mr',' mbr.bio_rec_id = mr.bio_rec_id', 'left');
            $this->db->join('trn_overtime tro','mbr.bio_rec_id = tro.bio_rec_id', 'left');
            $this->db->where('ms.bio_rec_id = mbr.bio_rec_id');  
            if($biodataId != '')
            {
                $this->db->where('mr.bio_rec_id = "'.$biodataId.'" ');
            }
            if( ($clientName == 'AMNT_Sumbawa' || $clientName == 'Trakindo_Sumbawa' || $clientName == 'Machmahon_Sumbawa') && $dept != '')
            {
                $this->db->where('mr.dept = "'.$dept.'" ');
            }        
            $this->db->where('mr.year_process = "'.$yearPeriod.'" ');
            $this->db->where('mr.month_process = "'.$monthPeriod.'" ');
            $this->db->where('tro.year_period = "'.$yearPeriod.'" ');
            $this->db->where('tro.month_period = "'.$monthPeriod.'" ');
            $this->db->where('ms.company_name = "'.$clientName.'" ');
            $this->db->where('mbr.is_active = 1');
            $query = $this->db->get()->result_array();
            /* START GET CONFIG FROM mst_payroll_config TABLE */
            $myConfigId = 0;    
            switch ($clientName) {                
                case 'AMNT_Sumbawa_Staff':
                    # code...
                    $myConfigId = 3;
                    break;
                case 'AMNT_Sumbawa':
                    # code...
                    $myConfigId = 4;
                    break;
                case 'Trakindo_Sumbawa':
                    # code...
                    $myConfigId = 5;
                    break;
                case 'Machmahon_Sumbawa':
                    # code...
                    $myConfigId = 6;
                    break;
                case 'Pontil_Sumbawa':
                    # code...
                    $myConfigId = 7;
                    break;                            
                default:
                    # code...
                    break;
            }
            /* MAKE SURE DATA SUDAH ADA DI CONFIG AGAR TIDAK ERROR */
            $row = $this->M_payroll_config->getObjectById($myConfigId);        
            // echo $this->db->last_query(); exit(0);
            /* END GET CONFIG FROM mst_payroll_config TABLE */

            /* START LOOP DATABASE */
            $myData = array();
            foreach ($query as $key => $row) {
                /* Set Default Variable Values */
                $salaryHourly = 0;
                $otTotal01 = 0;
                $otTotal02 = 0;
                $otTotal03 = 0;
                $otTotal04 = 0;

                $trvValue = 0;
                $allowanceEconomy = 0;
                $incentiveBonus = 0;
                $shiftBonus = 0;
                $remoteAllowance = 0;
                $positionalAllowance = 0;
                $allOverTimeTotal = 0;
                $otBonus = 0;
                $transHousingAllowance = 0;
                
                $nonTaxAllowance = 0;
                $attendTotal = 0;
                $attendInPH = 0;
                $phTotal = 0;
                $attendInOff = 0;
                $sickTotal = 0;
                $emergencyTotal = 0;
                $offTotal = 0;
                $alpaTotal = 0;
                $vacationTotal = 0;

                $this->M_salary_slip->resetValues();

                /* Start Data Values */
                $bioRecId = $row['bio_rec_id'];
                $rosterId = $row['roster_id'];
                $payrollGroup = $row['payroll_group'];
                $payrollLevel = $row['salary_level'];
                $rosterBase = $row['roster_base'];
                /* BASIC SALARY */
                $basicSalary = $row['basic_salary'];
                $fullName = $row['full_name'];
                $dept = $row['dept'];

                $maritalStatus = $row['marital_status'];
                $positionalAllowance = $row['positional_allowance'];
                $transHousingAllowance = $row['transport_housing'];
                $phTotal = $row['ph_total'];
                $attendInPH = $row['in_ph_total'];
                $attendInOff = $row['attend_in_off'];
                $attendTotal = $row['attend_total'] + $phTotal;
                $permitTotal = $row['permit_total']; 
                $offTotal = $row['off_total'];
                $sickTotal = $row['sick_total'];
                $alpaTotal = $row['alpa_total'];
                $vacationTotal = $row['vacation_total'];
                $emergencyTotal = $row['emergency_total'];
                $unpaidDays = $row['unpaid_days'];
                $position = $row['position'];               
                $isTravel = $row['is_travel'];
                $isRemoteAllowance = $row['is_remote_allowance'];
                $isShiftBonus = $row['is_shift_bonus'];
                $isAllowanceEconomy = $row['is_allowance_economy'];
                $isOtBonus = $row['is_ot_bonus'];
                $isIncentiveBonus = $row['is_incentive_bonus'];
                $isDevIncentiveBonus = $row['is_dev_incentive_bonus'];
                $isOvertime = $row['is_overtime'];
                /* Add By Maurice @28-12-2017 */
                $standByCount = $row['standby_total'];
                $paidPermitCount = $row['paid_permit_total'];
                $paidVacation = $row['paid_vacation_total'];
                /* End Data Values */

                $shiftAttend = $row['attend_total'] - $row['attend_in_off'];
                $offAttend = $row['attend_in_off'];
                $phAttend = $row['in_ph_total'];                
                $npwpNo = $row['npwp_no'];

                /* Start Get Config Values */
                $remotePercentConfig = $this->M_payroll_config->getRemotePercent();           
                 
                $shiftBonusConfig = $this->M_payroll_config->getShiftBonus();
                
                $otBonusConfig = $this->M_payroll_config->getOtBonus();
                $incentiveBonusConfig = $this->M_payroll_config->getIncentiveBonus();  
                $healthBpjsConfig = $this->M_payroll_config->getHealthBpjs();
                $maxHealthBpjsConfig = $this->M_payroll_config->getMaxHealthBpjs();
                $jkkJkmConfig = $this->M_payroll_config->getJkkJkm();
                $jhtConfig = $this->M_payroll_config->getJht();
                $empJhtConfig = $this->M_payroll_config->getEmpJht();             
                $empHealthBpjsConfig = $this->M_payroll_config->getEmpHealthBpjs();    
                $jpConfig = $this->M_payroll_config->getJp();
                $empJpConfig = $this->M_payroll_config->getEmpJp();           
                $salaryDividerConfig = $this->M_payroll_config->getSalaryDivider();
                $otMultiplierConfig01 = $this->M_payroll_config->getOt01Multiplier();
                $otMultiplierConfig02 = $this->M_payroll_config->getOt02Multiplier();
                $otMultiplierConfig03 = $this->M_payroll_config->getOt03Multiplier();
                $otMultiplierConfig04 = $this->M_payroll_config->getOt04Multiplier();                     
                $nonTaxAllowanceConfig = $this->M_payroll_config->getNonTaxAllowance();                                   
                $isProrateConfig = $this->M_payroll_config->getIsProrate();
                $noAccidentPercent = $this->M_payroll_config->getNoAccidentPercent(); 
                // $npwpCharge = $this->M_payroll_config->getNpwpCharge(); 

                /* START OVER TIME TOTAL */
                $otCount1 = 0;
                $otCount2 = 0;
                $otCount3 = 0;
                $otCount4 = 0;
                $timeTotal = 0;
                $ntTotal = 0;

                /* Only More Than 12 Hours Will Counted  */
                $trvDayCount = 0;

                for ($i=1; $i <= 31; $i++) { 
                    # code...
                    $idx = '';
                    if($i < 10){
                        $idx = '0'.$i; 
                    }else{
                        $idx = $i; 
                    }
                    $otCount1 += $row['ot1_d'.$idx];
                    $otCount2 += $row['ot2_d'.$idx];
                    $otCount3 += $row['ot3_d'.$idx];
                    $otCount4 += $row['ot4_d'.$idx];

                    /* COUNTING OF WORKED HOUR */    
                    $tmpTime = trim($row['d'.$idx]);
                    if($clientName == 'AMNT_Sumbawa' || $clientName == 'Machmahon_Sumbawa' || $clientName == 'Pontil_Sumbawa')
                    {
                        if(is_numeric($tmpTime)) {
                            $timeTotal += $row['d'.$idx];

                            if($tmpTime >= 12){
                               $trvDayCount++;  
                            }
                        } else {
                            $tmpCode = substr($tmpTime, 0, 2);
                            if($tmpCode == "PH" || $tmpCode == "RO" || $tmpCode == "NS" || $tmpCode == "SD" || $tmpCode == "SP") {
                                $tmpHour = substr($tmpTime, 2, strlen($tmpTime)-2);
                                $timeTotal += floatval($tmpHour);

                                if( floatval($tmpHour) >= 12 ){
                                   $trvDayCount++;  
                                }
                            }
                        }
                        
                    }

                    else if($clientName == 'Trakindo_Sumbawa')
                    {
                        if(is_numeric($tmpTime)) {
                            $timeTotal += $row['d'.$idx];
                        } else {
                            $tmpCode = substr($tmpTime, 0, 2);
                            if($tmpCode == "PH" || $tmpCode == "RO" || $tmpCode == "PN" || $tmpCode == "RN" || $tmpCode == "NS" || $tmpCode == "SD") {
                                $tmpHour = substr($tmpTime, 2, strlen($tmpTime)-2);
                                $timeTotal += floatval($tmpHour);
                            }
                        }
                        
                    }

                }

                $ntTotal = $timeTotal - $otCount1 - $otCount2 - $otCount3 - $otCount4;
                /* OVER TIME HOURLY */
                $salaryHourly = $basicSalary / $salaryDividerConfig;                
                $otTotal01 = $salaryHourly * $otMultiplierConfig01 * $otCount1; /* Rate Per Jam x Nilai OT 1 x Total Jam Lembur */
                $salaryHourly = $basicSalary / $salaryDividerConfig; 
                $otTotal02 = $salaryHourly * $otMultiplierConfig02 * $otCount2; /* Rate Per Jam x Nilai OT 2 x Total Jam Lembur */
                $otTotal03 = $salaryHourly * $otMultiplierConfig03 * $otCount3; /* Rate Per Jam x Nilai OT 3 x Total Jam Lembur */
                $otTotal04 = $salaryHourly * $otMultiplierConfig04 * $otCount4; /* Rate Per Jam x Nilai OT 4 x Total Jam Lembur */
                  
                /* START TRAVEL */
                $trvMultiplierConfig = $this->M_payroll_config->getTrvMultiplier();
                
                // $trvValue = ( ($shiftAttend + $offAttend + $phAttend) * $trvMultiplierConfig ) * $salaryHourly;
                /* END TRAVEL */

                /* START ALLOWANCE ECONOMY */
                $allowanceEconomy = $this->M_payroll_config->getAllowanceEconomy(); /* Nilai Konstana mst_payroll_config.allowance_economy */
                /* END ALLOWANCE ECONOMY */

                $incentiveBonus = $incentiveBonusConfig * ($shiftAttend + $offAttend + $phAttend); /* Nilai mst_payroll_config.incentive_bonus x Jumlah Masuk Hari Kerja  */
                /* END INCENTIVE BONUS */
                
                /* START ALPA TOTAL */             
                $totalAlpa = 0;
                $unpaidLength = strlen($unpaidDays);
                for ($ud=0; $ud < $unpaidLength; $ud++) { 
                    $unpaidChar = substr($unpaidDays, $ud, 1);                  

                    if($unpaidChar == '5')
                    {
                        $totalAlpa += (1/21) * $basicSalary;
                    }
                    else if($unpaidChar == '6')
                    {
                        $totalAlpa += (1/25) * $basicSalary;
                    }
                }   
                /* END ALPA TOTAL */

                /* START OVER TIME BONUS */
                $allOverTimeTotal = $otTotal01 + $otTotal02 + $otTotal03 + $otTotal04; 

                $otBonus = ($otBonusConfig/100) * $allOverTimeTotal; /* Nilai mst_payroll_config.ot_bonus(%) x Total Nilai Over Time */
                /* START OVER TIME BONUS */
                /* END OVER TIME BONUS */
                /* START DEVELOPMENT INCENTIVE */

                /* EXCLUDE If Work Hours Less Than 8 Hours */
                $excludeCountDay = 0;
                $colTmp = '';
                for($z=1; $z <= 31; $z++){
                    if($z < 10){
                        $colTmp = 'd0'.$z;
                    }else{
                        $colTmp = 'd'.$z;
                    }
                    if( is_numeric($row[$colTmp]) && ($row[$colTmp] > 0) && ($row[$colTmp] < 8) ){
                        $excludeCountDay++;
                    } 

                    if( substr($row[$colTmp],0,2) == 'PH' ){
                        $phVal = substr($row[$colTmp],2, strlen($row[$colTmp]-2));
                        if( is_numeric($phVal) && ($phVal > 0) && ($phVal < 8) ){
                            $excludeCountDay++; 
                        }
                    }                    
                }


                $alpaSickPercent = 0;
                for ($m=0; $m < ($alpaTotal-$permitTotal); $m++) { 
                    $alpaSickPercent += 50;
                }
                for ($m=0; $m < $sickTotal; $m++) { 
                    $alpaSickPercent += 15;
                }
                $devIncentiveBonus = 0;
                if($alpaSickPercent < 100)
                {
                    /* FORMULA CHANGE (35/100)/27) to (35/100)/26) BASE ON  */
                    $tmp = $basicSalary * ((35/100)/26) * ($row['attend_total'] + $row['in_ph_total'] - $excludeCountDay);
                    /* (35%)/27 x Jumlah Masuk Kerja Yang Lebih Dari 7 Jam (Jika ada alpa, potong 50% perhari, Sakit 15% perhari ) */
                    $devIncentiveBonus = $tmp - ($tmp * ($alpaSickPercent/100));    
                }  

                if($devIncentiveBonus <= 0) {
                   $devIncentiveBonus = 0; 
                }           
                /* END DEVELOPMENT INCENTIVE */             

                /* START GOVERNMENT REGULATION */
                $nonTaxAllowance = $this->M_payroll_config->getNonTaxAllowance(); /* Nilai Tunjangan Non Pajak */
                $taxPercent1 = $this->M_payroll_config->getTaxPercent1(); /* Persentase Progresif Pajak I */
                $taxPercent2 = $this->M_payroll_config->getTaxPercent2(); /* Persentase Progresif Pajak II */
                $taxPercent3 = $this->M_payroll_config->getTaxPercent3(); /* Persentase Progresif Pajak III */
                $taxPercent4 = $this->M_payroll_config->getTaxPercent4(); /* Persentase Progresif Pajak IV */
                $taxVal1 = $this->M_payroll_config->getTaxMaxVal1(); /* Nominal Pajak I */ 
                $taxVal2 = $this->M_payroll_config->getTaxMaxVal2(); /* Nominal Pajak II */
                $taxVal3 = $this->M_payroll_config->getTaxMaxVal3(); /* Nominal Pajak III */
                $taxVal4 = $this->M_payroll_config->getTaxMaxVal4(); /* Nominal Pajak IV */
                $ptkpBasic = $this->M_payroll_config->getPtkp(); /* Nominal PTKP */
                $ptkpDependent = $this->M_payroll_config->getPtkpDependent(); /* Nominal PTKP Tanggungan Per Orang */              
                /* START GET PTKP TOTAL  */
                $ptkpMultiplier = 0;
                switch ($maritalStatus) {
                    case 'TK0':
                        $ptkpMultiplier = 0;
                        break;
                    case 'TK1':
                        $ptkpMultiplier = 1;
                        break;
                    case 'TK2':
                        $ptkpMultiplier = 2;
                        break;
                    case 'TK3':
                        $ptkpMultiplier = 3;
                        break;
                    case 'K0':
                        $ptkpMultiplier = 1;
                        break;
                    case 'K1':
                        $ptkpMultiplier = 2;
                        break;
                    case 'K2':
                        $ptkpMultiplier = 3;
                        break;
                    case 'K3':
                        $ptkpMultiplier = 4;
                        break;  
                    
                    default:
                        $ptkpMultiplier = 0;
                        break;
                }

                $totalPTKP = $ptkpBasic + ($ptkpDependent * $ptkpMultiplier);

                /* END GET PTKP TOTAL */
                
                $tmpSalary = $basicSalary; 
                // $standByCount = $row['vacation_days'];
                $wdAttendCount = 0;
                $bsProrate = 0;

                /* START GET PRORATE */
                if( ($isProrateConfig == true) ){
                    if( ($clientName == 'AMNT_Sumbawa' || $clientName == 'Machmahon_Sumbawa' || $clientName == 'Pontil_Sumbawa' || $clientName == 'Trakindo_Sumbawa') && ($standByCount > 0) )
                    {
                        // echo $wdAttendCount; exit();

                        $wdAttendCount += $row['attend_total']; // 5
                        $wdAttendCount += $row['in_ph_total']; //2
                        $wdAttendCount -= $row['attend_in_off'];//2
                        $wdAttendCount += $row['sick_total']; //0
                        $wdAttendCount += $row['emergency_total']; //1
                        $wdAttendCount += $row['paid_permit_total']; //0
                        $wdAttendCount += $row['paid_vacation_total']; //0
                        $wdAttendCount += $row['alpa_total']; /* Include $row['permit_total'] */ //5
                        if( $clientName == 'Trakindo_Sumbawa' || $clientName == 'Pontil_Sumbawa'){
                            if($wdAttendCount > 21) 
                            {
                                $wdAttendCount = 21;   
                            }
                            $bsProrate = ($wdAttendCount/21) * $basicSalary;
                        }else if( $clientName == 'AMNT_Sumbawa'){
                            if($wdAttendCount > 25) 
                            {
                                $wdAttendCount = 25;   
                            }
                            $bsProrate = ($wdAttendCount/25) * $basicSalary;
                        }else if($clientName == 'Machmahon_Sumbawa'){
                            $wdAttendCount += $offTotal;
                            if($wdAttendCount > 25) 
                            {
                                $wdAttendCount = 25;   
                            }
                            $bsProrate = ($wdAttendCount/25) * $basicSalary;
                        } 
                        /* Request By Ilham & Bambang @15-12-2017 */
                        $tmpSalary = $bsProrate;         
                    }

                    
                }
                // echo $row['attend_total']; exit(0);
                $this->M_salary_slip->setBsProrate($tmpSalary);
                
                /* START REMOTE LOCATION BONUS */
                $remoteAllowance = ($remotePercentConfig/100) * $tmpSalary; 
                /* Aturan Baru 26 Jan 2018 (20% Dari Basic), Dari mst_payroll_config.remote_allowance */
                /* By Addesant, terjadi perubahan 02 Feb 2018 (20% Dari Prorate), Dari mst_payroll_config.remote_allowance */
                /* END REMOTE LOCATION BONUS */

                /* START POSITIONAL ALLOWANCE/ NON TAX */
                $nonTaxTmp = $basicSalary * (5/100);
                if($nonTaxTmp < $nonTaxAllowance){
                    $nonTaxAllowance = $nonTaxTmp;                    
                }                
                /* END POSITIONAL ALLOWANCE/ NON TAX */

                /* START HEALTH BPJS */
                $healthBpjs = ($healthBpjsConfig/100) * $basicSalary; /* Nilai mst_payroll_config.health_bpjs(%) x Basic Salary (Max 8 Juta) */
                if($healthBpjs > $maxHealthBpjsConfig){
                    $healthBpjs = $maxHealthBpjsConfig; 
                }
                /* END HEALTH BPJS */

                /* START JKK-JKM */
                $jkkJkm = ($jkkJkmConfig/100) * $basicSalary; /* Nilai mst_payroll_config.jkk_jkm(%) x Basic Salary */
                /* END JKK-JKM */

                /* START JHT */
                $jht = ($jhtConfig/100) * $basicSalary; /* Nilai mst_payroll_config.jht(%) x Basic Salary */        
                /* END JHT */
                
                /* START JHT */
                $empJht = ($empJhtConfig/100) * $basicSalary; /* Nilai mst_payroll_config.emp_jht(%) x Basic Salary */      
                /* END JHT */

                /* START JP COMPANY */
                $maxJp = $this->M_payroll_config->getMaxJp(); /* Nilai Max Untuk Iuran JP Perusahaan  */
                $jp = ($jpConfig/100) * $basicSalary; /* Nilai mst_payroll_config.jp(%) x Basic Salary */ 
                if($jp > $maxJp) {
                    $jp = $maxJp;
                }      
                /* END JP COMPANY */

                /* START JP EMPLOYEE */
                $maxEmpJp = $this->M_payroll_config->getMaxEmpJp(); /* Nilai Max Untuk Iuran JP Karyawan  */
                $empJp = ($empJpConfig/100) * $basicSalary; /* Nilai mst_payroll_config.emp_jp(%) x Basic Salary */ 
                if($empJp > $maxEmpJp) {
                   $empJp = $maxEmpJp;
                }       
                /* END JP EMPLOYEE */

                /* START BPJS KARYAWAN */
                $maxEmpHealthBpjs = $this->M_payroll_config->getMaxEmpBpjs(); /* Nilai Max Gaji Untuk Iuran BPJS Karyawan  */ 
                $empHealthBpjs = ($empHealthBpjsConfig/100) * $basicSalary; /* Nilai mst_payroll_config.emp_health_bpjs(%) x Basic Salary */
                if($empHealthBpjs > $maxEmpHealthBpjs)
                {
                    $empHealthBpjs = $maxEmpHealthBpjs; 
                }     
                /* END BPJS KARYAWAN */

                /* START ALL ALLOWANCE */
                $allAllowance = 0;
                
                /* END ALL ALLOWANCE */

                /* START BRUTO INCOME */
                $bruto = $tmpSalary + $allOverTimeTotal + $allAllowance;  
                /* END BRUTO INCOME */          

                /* START NETTO INCOME */
                $nettoTax = $bruto - $empJht - $nonTaxAllowance;  
                /* END NETTO INCOME */
                
                /* END GOVERNMENT REGULATION */

                /* START INSERT TABEL DATA SLIP */

                $this->M_salary_slip->setSalarySlipId($this->M_salary_slip->GenerateNumber());
                $this->M_salary_slip->setBioRecId($bioRecId);
                $this->M_salary_slip->setRosterId($rosterId);
                $this->M_salary_slip->setYearPeriod($yearPeriod);
                $this->M_salary_slip->setMonthPeriod($monthPeriod);
                
                $fullName = preg_replace('~[\r\n]+~', '', $fullName);
                $this->M_salary_slip->setName($fullName);
                $this->M_salary_slip->setDept($dept);
                $this->M_salary_slip->setPosition($position);
                $this->M_salary_slip->setMaritalStatus($maritalStatus);
                $this->M_salary_slip->setClientName($clientName);
                $this->M_salary_slip->setBasicSalary($basicSalary); 

                $this->M_salary_slip->setNormalTime($ntTotal);
                if($isOvertime == '1'){
                    $this->M_salary_slip->setOt1($otTotal01);
                    $this->M_salary_slip->setOt2($otTotal02);
                    $this->M_salary_slip->setOt3($otTotal03);
                    $this->M_salary_slip->setOt4($otTotal04);
                    
                    $this->M_salary_slip->setOtCount1($otCount1);
                    $this->M_salary_slip->setOtCount2($otCount2);
                    $this->M_salary_slip->setOtCount3($otCount3);
                    $this->M_salary_slip->setOtCount4($otCount4);
                }
                // if($isTravel == '1'){

                if($isRemoteAllowance == '1'){
                    $this->M_salary_slip->setRemoteAllowance($remoteAllowance);
                }
                if($isShiftBonus == '1'){
                    $this->M_salary_slip->setShiftBonus($shiftBonus);
                }
                if($isAllowanceEconomy == '1'){
                    $this->M_salary_slip->setAllowanceEconomy($allowanceEconomy);
                }
                if($isOtBonus == '1'){
                    $this->M_salary_slip->setOtBonus($otBonus);
                }
                if($isIncentiveBonus == '1'){
                    $this->M_salary_slip->setIncentiveBonus($incentiveBonus);
                }
                if($isDevIncentiveBonus == '1'){
                    $this->M_salary_slip->setDevIncentiveBonus($devIncentiveBonus);
                    $this->M_salary_slip->setDevPercent(100);
                }

                $this->M_salary_slip->setHealthBpjs($healthBpjs);
                $this->M_salary_slip->setJkkJkm($jkkJkm);
                $this->M_salary_slip->setJht($jht);
                $this->M_salary_slip->setJp($jp);
                $this->M_salary_slip->setEmpHealthBpjs($empHealthBpjs);
                $this->M_salary_slip->setEmpJht($empJht);
                $this->M_salary_slip->setEmpJp($empJp);
                /* START IN SHIFT, IN OFF, IN PH  */
                $shiftAttend = $row['attend_total'] - $row['attend_in_off'];
                $offAttend = $row['attend_in_off'];
                $phAttend = $row['in_ph_total'];                
                $this->M_salary_slip->setInShift($shiftAttend);
                $this->M_salary_slip->setInOff($offAttend);
                $this->M_salary_slip->setInPh($phAttend);

                if($clientName == 'Pontil_Sumbawa'){

                    /*$tAttendTotal = $shiftAttend+$offAttend+$phAttend;
                    if($tAttendTotal > 21){
                        $tAttendTotal = 21;
                    } 

                    $trvValue = ($tAttendTotal*2)*($basicSalary/173);
                    $this->M_salary_slip->setTravelBonus($trvValue);
                    $attendBonus = (250000/21) * $tAttendTotal;
                    $this->M_salary_slip->setAttendanceBonus($attendBonus);*/

                    $tAttendTotal = $shiftAttend+$offAttend+$phAttend;
                    if($tAttendTotal > 0){
                        $attendBonus = 250000;
                        $this->M_salary_slip->setAttendanceBonus($attendBonus); 
                    }
                    $trvValue = ($basicSalary/173) * 2 * $trvDayCount;
                    $this->M_salary_slip->setTravelBonus($trvValue);


                }

                /* END IN SHIFT, IN OFF, IN PH  */
                $this->M_salary_slip->setUnpaidCount($unpaidLength);
                $this->M_salary_slip->setUnpaidTotal($totalAlpa);
                $this->M_salary_slip->setNonTaxAllowance($nonTaxAllowance);
                $this->M_salary_slip->setPtkpTotal($totalPTKP);          
                $this->M_salary_slip->setPayrollGroup($payrollGroup);
                $this->M_salary_slip->setPayrollLevel($payrollLevel);
                $this->M_salary_slip->setPicInput($this->session->userdata('hris_user_id'));
                $myCurrentDate = GetCurrentDate();
                $this->M_salary_slip->setInputTime($myCurrentDate['CurrentDateTime']);
                $this->M_salary_slip->insert();                 
                /* END INSERT TABEL DATA SLIP */      
                // echo $this->M_salary_slip->insert(); exit(0);
            } 
            /* END LOOP DATABASE */
            // if( $clientName == 'AMNT_Sumbawa' || $clientName == 'Machmahon_Sumbawa' || $clientName == 'Trakindo_Sumbawa' )
            // {
            //    $this->loadSlipByPtYearMonthDept($clientName, $yearPeriod, $monthPeriod, $dept);     
            // }
            // else {
            //     $this->loadSlipByPtYearMonth($clientName, $yearPeriod, $monthPeriod);                       
            // }            
        }   
    }

    /* START ALLOWANCE PROCESS */
    public function allowanceProcess($ptName, $yearPeriod, $monthPeriod)
    {
        $rows = $this->M_allowance->getByClientPeriod($ptName, $yearPeriod, $monthPeriod);
        foreach ($rows as $row) 
        {            
            $biodataId = $row['biodata_id'];
            $clientName = $row['client_name'];
            $year = $row['year_period'];
            $month = $row['month_period'];

            $thr = $row['thr'];
            $contractBonus = $row['contract_bonus'];
            $kpiBonus = $row['kpi_bonus'];
            $outCamp = $row['out_camp'];

            $this->M_salary_slip->getObjectByClientPeriod($biodataId, $clientName, $year, $month);
            $slipId = $this->M_salary_slip->getSalarySlipId();
            $this->M_salary_slip->setThr($thr);
            if($clientName == 'Machmahon_Sumbawa'){
                $this->M_salary_slip->setContractBonus($contractBonus);
                $this->M_salary_slip->setOtherAllowance1($kpiBonus);
            }
            else if($clientName == 'Pontil_Sumbawa'){
                $this->M_salary_slip->setOutCamp($outCamp);
                $this->M_salary_slip->setCcPayment($contractBonus);
            }
            $this->M_salary_slip->update($slipId);
        }
    }
    /* END ALLOWANCE PROCESS */

    /* START PAYMENT EXPORT */
    public function taxProcess($ptName, $yearPeriod, $monthPeriod, $department)
    {
        $dept = $this->security->xss_clean($department);
        $strDept = '';
        if($dept != ''){
            $strDept .= "AND ss.dept = '".$dept."' ";
        }
        // echo "Hello"; exit(0);
        $strSQL = ''; 
        if($ptName == "AMNT_Sumbawa" || $ptName == 'Machmahon_Sumbawa')
        {
            $strSQL  = "SELECT ss.salary_slip_id,mb.bio_rec_id,mb.npwp_no,mb.full_name,ms.nie,mb.position,ms.bank_name,ms.account_name,ms.account_no,ss.ptkp_total,ss.thr,ss.contract_bonus,ss.emp_jht,ss.emp_jp,ss.cc_payment,ss.debt_burden,ms.payroll_group,ss.tax_value,";
            $strSQL .= " (";
            $strSQL .= " CAST("; /*START CAST*/
            $strSQL .= " ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 + ";
            $strSQL .= " ss.flying_camp + ss.safety_bonus + ss.attendance_bonus + ss.other_allowance1 + ss.other_allowance2 + ss.thr + ss.contract_bonus +";
            $strSQL .= " ss.jkk_jkm + ss.health_bpjs - ss.unpaid_total + ss.adjust_in - ss.adjust_out -  ";
            $strSQL .= " ss.jkk_jkm - ss.emp_jht - ss.emp_health_bpjs - ss.health_bpjs - ss.emp_jp - ";
            $strSQL .= " ss.thr - ss.contract_bonus ";           
            $strSQL .= " AS DECIMAL(11,2))  "; /*END CAST*/
            $strSQL .= " ) AS total, ";
            /* START BRUTTO */
            $strSQL .= " (";
            $strSQL .= " CAST("; /*START CAST*/
            $strSQL .= " ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 + ";
            $strSQL .= " ss.flying_camp + ss.safety_bonus + ss.attendance_bonus + ss.other_allowance1 + ss.other_allowance2 + ss.thr + ss.contract_bonus +";
            $strSQL .= " ss.jkk_jkm + ss.health_bpjs - ss.unpaid_total + ss.adjust_in - ss.adjust_out ";            
            $strSQL .= " AS DECIMAL(11,2))  "; /*END CAST*/
            $strSQL .= " ) AS brutto_tax ";
            /* END BRUTTO */
            $strSQL .= "FROM mst_salary ms,mst_bio_rec mb,trn_salary_slip ss ";
            $strSQL .= "WHERE ms.company_name = '".$ptName."' ";
            $strSQL .= "AND ss.month_period = '".$monthPeriod."' ";
            $strSQL .= "AND ss.year_period = '".$yearPeriod."' ";
            $strSQL .= $strDept;
            // $strSQL .= "AND ms.payroll_group = '".$payrollGroup."' ";
            // if($dept != ''){
            //     $strSQL .= "AND ss.dept = '".$dept."' ";
            // }
            $strSQL .= "AND ms.bio_rec_id = mb.bio_rec_id  ";
            $strSQL .= "AND mb.bio_rec_id = ss.bio_rec_id ";
            $strSQL .= "AND mb.is_active = 1 ";   
            $strSQL .= "ORDER BY mb.full_name "; 
        }
        else if($ptName == 'Pontil_Sumbawa')
        {
            $strSQL  = "SELECT ss.salary_slip_id,mb.bio_rec_id,mb.npwp_no,mb.full_name,ms.nie,mb.position,ms.bank_name,ms.account_name,ms.account_no,ss.ptkp_total,ss.thr,ss.contract_bonus,ss.emp_jht,ss.emp_jp,ss.cc_payment,ss.debt_burden,ms.payroll_group,ss.tax_value,ss.travel_bonus,";
            $strSQL .= " (";
            $strSQL .= " CAST("; /*START CAST*/
            $strSQL .= " ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 + ";
            $strSQL .= " ss.flying_camp + ss.travel_bonus + ss.attendance_bonus + ss.other_allowance1 + ss.other_allowance2 + ss.thr + ss.contract_bonus +";
            $strSQL .= " ss.jkk_jkm + ss.health_bpjs - ss.unpaid_total + ss.adjust_in - ss.adjust_out -  ";
            $strSQL .= " ss.jkk_jkm - ss.emp_jht - ss.emp_health_bpjs - ss.health_bpjs - ss.emp_jp - ";
            $strSQL .= " ss.thr - ss.contract_bonus ";           
            $strSQL .= " AS DECIMAL(11,2))  "; /*END CAST*/
            $strSQL .= " ) AS total, ";
            /* START BRUTTO */
            $strSQL .= " (";
            $strSQL .= " CAST("; /*START CAST*/
            $strSQL .= " ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 + ";
            $strSQL .= " ss.flying_camp + ss.travel_bonus + ss.attendance_bonus + ss.other_allowance1 + ss.other_allowance2 + ss.thr + ss.contract_bonus +";
            $strSQL .= " ss.jkk_jkm + ss.health_bpjs - ss.unpaid_total + ss.adjust_in - ss.adjust_out ";            
            $strSQL .= " AS DECIMAL(11,2))  "; /*END CAST*/
            $strSQL .= " ) AS brutto_tax ";
            /* END BRUTTO */
            $strSQL .= "FROM mst_salary ms,mst_bio_rec mb,trn_salary_slip ss ";
            $strSQL .= "WHERE ms.company_name = '".$ptName."' ";
            $strSQL .= "AND ss.month_period = '".$monthPeriod."' ";
            $strSQL .= "AND ss.year_period = '".$yearPeriod."' ";
            $strSQL .= $strDept;
            // $strSQL .= "AND ms.payroll_group = '".$payrollGroup."' ";
            // if($dept != ''){
            //     $strSQL .= "AND ss.dept = '".$dept."' ";
            // }
            $strSQL .= "AND ms.bio_rec_id = mb.bio_rec_id  ";
            $strSQL .= "AND mb.bio_rec_id = ss.bio_rec_id ";
            $strSQL .= "AND mb.is_active = 1 ";   
            $strSQL .= "ORDER BY mb.full_name "; 
        }
        else if($ptName == "Trakindo_Sumbawa")
        {           

            $strSQL  = "SELECT ss.salary_slip_id,mb.bio_rec_id,mb.full_name,mb.npwp_no,ms.nie,mb.position,ms.bank_name,ms.account_name,ms.account_no,ss.ptkp_total,ss.thr,ss.contract_bonus,ss.emp_jht,ss.emp_jp,ss.cc_payment,ss.debt_burden,ms.payroll_group,ss.tax_value,";
            $strSQL .= " (";
            $strSQL .= " CAST("; /*START CAST*/
            $strSQL .= " ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 + ";
            $strSQL .= " ss.flying_camp + ss.safety_bonus + ss.attendance_bonus + ss.other_allowance1 + ss.other_allowance2 + ss.thr + ss.contract_bonus +";
            $strSQL .= " ss.jkk_jkm + ss.health_bpjs - ss.unpaid_total + ss.adjust_in - ss.adjust_out -  ";
            $strSQL .= " ss.jkk_jkm - ss.emp_jht - ss.emp_health_bpjs - ss.health_bpjs - ss.emp_jp - ";
            $strSQL .= " ss.thr - ss.contract_bonus ";           

            $strSQL .= " AS DECIMAL(11,2))  "; /*END CAST*/
            $strSQL .= " ) AS total, ";

            /* START BRUTTO */
            $strSQL .= " (";
            $strSQL .= " CAST("; /*START CAST*/
            $strSQL .= " ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 + ";
            $strSQL .= " ss.flying_camp + ss.safety_bonus + ss.attendance_bonus + ss.other_allowance1 + ss.other_allowance2 + ss.thr + ss.contract_bonus +";
            $strSQL .= " ss.jkk_jkm + ss.health_bpjs - ss.unpaid_total + ss.adjust_in - ss.adjust_out ";
            // $strSQL .= " ss.jkk_jkm-";
            /*$strSQL .= " ss.emp_jht";*/
            // $strSQL .= " ss.emp_health_bpjs ";
            // $strSQL .= " ss.health_bpjs";
            // $strSQL .= " )*12"; /*END SETAHUNKAN*/
            $strSQL .= " AS DECIMAL(11,2))  "; /*END CAST*/
            $strSQL .= " ) AS brutto_tax ";
            /* END BRUTTO */

            $strSQL .= "FROM mst_salary ms,mst_bio_rec mb,trn_salary_slip ss ";
            $strSQL .= "WHERE ms.company_name = '".$ptName."' ";
            $strSQL .= "AND ss.month_period = '".$monthPeriod."' ";
            $strSQL .= "AND ss.year_period = '".$yearPeriod."' ";
            $strSQL .= "AND ss.dept = '".$this->security->xss_clean($dept)."' ";
            // $strSQL .= "AND ms.payroll_group = '".$payrollGroup."' ";
            $strSQL .= "AND ms.bio_rec_id = mb.bio_rec_id  ";
            $strSQL .= "AND mb.bio_rec_id = ss.bio_rec_id ";
            $strSQL .= "AND mb.is_active = 1 ";   
            $strSQL .= "ORDER BY mb.full_name ";      
        }
        $query = $this->db->query($strSQL)->result_array();   

        $bruttoTax = 0;
        /* TAX CONFIG */
        $this->load->model('M_payroll_config');
        $myConfigId = 0;    
        switch ($ptName) {
            case 'AMNT_Sumbawa':
                # code...
                $myConfigId = 4;
                break;
            case 'Trakindo_Sumbawa':
                # code...
                $myConfigId = 5;
                break;
            case 'Machmahon_Sumbawa':
                # code...
                $myConfigId = 6;
                break;
            case 'Pontil_Sumbawa':
                # code...
                $myConfigId = 7;
                break;              
            default:
                # code...
                break;
        }
        
        $taxPercent1 = $this->M_payroll_config->getTaxPercent1(); /* Persentase Progresif Pajak I */
        $taxPercent2 = $this->M_payroll_config->getTaxPercent2(); /* Persentase Progresif Pajak II */
        $taxPercent3 = $this->M_payroll_config->getTaxPercent3(); /* Persentase Progresif Pajak III */
        $taxPercent4 = $this->M_payroll_config->getTaxPercent4(); /* Persentase Progresif Pajak IV */
        $maxTaxVal1 = $this->M_payroll_config->getTaxMaxVal1(); /* Nominal Pajak I */ 
        $maxTaxVal2 = $this->M_payroll_config->getTaxMaxVal2(); /* Nominal Pajak II */
        $maxTaxVal3 = $this->M_payroll_config->getTaxMaxVal3(); /* Nominal Pajak III */
        $maxTaxVal4 = $this->M_payroll_config->getTaxMaxVal4(); /* Nominal Pajak IV */
        $maxNonTax = $this->M_payroll_config->getNonTaxAllowance(); /* Max Tunjangan Jabatan */
        $row = $this->M_payroll_config->getObjectById($myConfigId); 

        $rowIdx = 8;
        $rowNo = 0;
        foreach ($query as $row) {
            $rowIdx++;
            $rowNo++;            

            /* START UPDATE TAX */
            $bruttoTax = $row['brutto_tax'];
            $pembulatanPenghasilan = $bruttoTax;
            $nonTaxAllowance = ($pembulatanPenghasilan) * (5/100);
            if($nonTaxAllowance > $maxNonTax){
                $nonTaxAllowance = $maxNonTax;
            }

            $netto = $bruttoTax - $row['emp_jht'] - $row['emp_jp'] - $nonTaxAllowance;
            $unFixedIncome = $row['thr'] + $row['contract_bonus'];
            $nettoSetahun = ( ($netto - $unFixedIncome)*12 ) + $unFixedIncome;
            
            $monthlyTax = 0;

            $taxVal1 = 0;    
            $taxVal2 = 0;    
            $taxVal3 = 0;    
            $taxVal4 = 0;

            $tVal = 0;
            $tSisa = 0;
            $pembulatanPenghasilan = $nettoSetahun - $row['ptkp_total'];
            if($pembulatanPenghasilan > 0)
            {
                /* TAX 1 */
                if($maxTaxVal1 > 0)
                {
                    $tVal = $pembulatanPenghasilan/$maxTaxVal1;  
                    if($tVal >= 1){
                        $taxVal1 = $maxTaxVal1 * ($taxPercent1/100);
                    }
                    else{
                        $taxVal1 = $pembulatanPenghasilan * ($taxPercent1/100);
                    }    
                }    
                
                /* TAX 2 */
                if($maxTaxVal2 > 0)
                {
                    if($pembulatanPenghasilan > $maxTaxVal1)
                    {
                        $tSisa = $pembulatanPenghasilan - $maxTaxVal1; 
                        $tVal = $tSisa/$maxTaxVal2;
                        if($tVal >= 1){
                            $taxVal2 = $maxTaxVal2 * ($taxPercent2/100);
                        }
                        else{
                            $taxVal2 = $tSisa * ($taxPercent2/100);
                        }
                    }     
                }
                 
                /* TAX 3 */
                if($maxTaxVal3 > 0)
                {
                    if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2))
                    {
                        $tSisa = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2;
                        $tVal = $tSisa/$maxTaxVal3;
                        if($tVal >= 1){
                            $taxVal3 = $maxTaxVal3 * ($taxPercent3/100);
                        }
                        else{
                            $taxVal3 = $tSisa * ($taxPercent3/100);
                        }
                    }    
                }
                 
                /* TAX 4 */
                if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2 + $maxTaxVal3))
                {
                    $tSisa = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2 - $maxTaxVal3;
                    $taxVal4 = $tSisa * ($taxPercent4/100); 
                }               
            }
            
            /* END THR - BONUS TAX -- EDITED @ AUG 15, 2017 BY FINANCE */
            $npwpNo = $row['npwp_no'];
            $yearlyTax = $taxVal1 + $taxVal2 + $taxVal3 + $taxVal4;
            $npwpCharge = 0;
            if( strlen($npwpNo) < 19 ){
                $npwpCharge = $this->M_payroll_config->getNpwpCharge();
                $yearlyTax = $yearlyTax + ($yearlyTax * ($npwpCharge/100) ); 
            }

            $taxTotal = ( $yearlyTax/12 );
            if($taxTotal > 0){
                $monthlyTax = $taxTotal; 
            }else{
                $monthlyTax = 0; 
            }
            
            $tmpTotal = $row['total'] - floor($monthlyTax) - $row['debt_burden'];
            $totalTerima = 0;
            if ($tmpTotal > 0)
            {
                $totalTerima = $tmpTotal;
            }         

            /* START UPDATE TAX VALUE TO THE TABLE */
            if($row['tax_value'] <= 0)
            {
                $slipId = $row['salary_slip_id'];
                $str = "UPDATE trn_salary_slip SET tax_value = ".$monthlyTax." WHERE salary_slip_id = '".$slipId."' ";
                $this->db->query($str);                
            }
            /* END UPDATE TAX VALUE TO THE TABLE */

        } /* end foreach ($query as $row) */       
           
    }
    /* END PAYMENT EXPORT */

    public function getPayrollList($pt, $dept, $year, $month)
    {
        $this->db->select('*');
        $this->db->from($this->db->database.'.trn_salary_slip');
        $this->db->where('client_name', $pt);
        if( ($pt == "AMNT_Sumbawa" || $pt == 'Machmahon_Sumbawa' || $pt == "Trakindo_Sumbawa") && $dept != "")
        {
            $this->db->where('dept', $this->security->xss_clean($dept));
        }

        $this->db->where('year_period', $this->security->xss_clean($year));
        $this->db->where('month_period', $this->security->xss_clean($month));
        $this->db->order_by('name',"asc");
        $query = $this->db->get()->result_array();
        // echo $this->db->last_query(); exit(0);
        /*return json_encode($query);*/
        $myData = array();
        foreach ($query as $key => $row) {
            $myData[] = array(
                $row['salary_slip_id'],         
                $pt,         
                $row['name'],         
                // $row['production_bonus'],         
                // $row['workday_adj'],         
                // $row['adjust_in'],         
                // $row['adjust_out'],         
                $row['dept'],         
                $row['position']         
                // $row['attendance_bonus'],         
                // $row['other_allowance1'],         
                // $row['other_allowance2'],         
                // $row['cc_payment'],         
                // $row['thr'],         
                // $row['debt_burden'],
                // $row['debt_explanation']
            );            
        }  
        echo json_encode($myData);   
    }




}
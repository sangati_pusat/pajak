<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    // Load library phpspreadsheet
    require('./vendor/autoload.php');
    use PhpOffice\PhpSpreadsheet\Helper\Sample;
    use PhpOffice\PhpSpreadsheet\IOFactory;
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    // End load library phpspreadsheet

class Payroll extends CI_Controller {

    public function __construct(){
        parent::__construct();
        // $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
        // $this->load->model('M_MstProcessClosing');
        // $this->load->model('masters/M_salary_slip');
        $this->load->model('masters/M_roster_hist');
        $this->load->model('M_salary_slip'); 
        $this->load->model('masters/M_payroll_config'); 
        // $this->load->model('masters/M_salary');
        $this->load->model('masters/M_mst_salary', 'M_salary'); 
    }

    public function toExcel($slipId, $clientName, $calendarStart, $payrollGroup, $isHealthBPJS, $isJHT, $isJP, $isJKKM)
    {             
        /* CUSTOMIZE GOVERNMENT REGULATION */
        if ($clientName == "Redpath_Timika") {    
            $this->redpathExcel($slipId, $calendarStart, $payrollGroup, $isHealthBPJS, $isJHT, $isJP, $isJKKM);        
        }
        else if ($clientName == 'Pontil_Timika') {            
            $this->pontilExcel($slipId, $calendarStart, $payrollGroup, $isHealthBPJS, $isJHT, $isJP, $isJKKM);        
        }
        // else if ($clientName == 'Pontil_Sumbawa') {            
        //     $this->pontilSmbExcel($slipId, $calendarStart, $payrollGroup, $isHealthBPJS, $isJHT, $isJP, $isJKKM);
        // }   
        // elseif ($clientName == "Trakindo_Sumbawa") {
        //     $this->trakindoSmbExcel($slipId, $calendarStart, $payrollGroup, $isHealthBPJS, $isJHT, $isJP, $isJKKM);
        // }        
    }

    public function getSlipByIid(){
        $slipId = "";
        if(isset($_POST['slipId']))
        {
            $slipId = $this->db->escape_str($_POST['slipId']);
        }

        $this->db->select('*');
        $this->db->from($this->db->database.'.trn_salary_slip');
        $this->db->where('salary_slip_id', $slipId);
        $query = $this->db->get()->row_array();
        $myData = array();
        $myData[0] = $query['salary_slip_id']; 
        $myData[1] = $query['name'];
        $myData[2] = $query['production_bonus'];
        $myData[3] = $query['workday_adj'];
        $myData[4] = $query['adjust_in'];
        $myData[5] = $query['adjust_out'];
        $myData[6] = $query['contract_bonus'];
        $myData[7] = $query['flying_camp'];
        $myData[8] = $query['safety_bonus'];
        $myData[9] = $query['attendance_bonus'];
        $myData[10] = $query['other_allowance1'];
        $myData[11] = $query['other_allowance2'];
        $myData[12] = $query['no_accident_bonus'];
        $myData[13] = $query['cc_payment'];
        $myData[14] = $query['thr'];
        $myData[15] = $query['debt_burden'];
        $myData[16] = $query['debt_explanation'];         
        $myData[17] = $query['salary_status'];         
        $myData[18] = $query['status_remarks']; 

        /* START GET EMP LOAN */
        $biodataId = $query['bio_rec_id']; 
        $yearPeriod = $query['year_period']; 
        $monthPeriod = $query['month_period']; 
        $clientName = $query['client_name'];
        $strQuery  = 'SELECT hl.bio_rec_id,hl.client_name,dl.loan_id,dl.numbering,dl.month_period,dl.year_period,SUM(dl.amount) amount,hl.remarks ';
        $strQuery .= 'FROM trn_loan hl, trn_loan_detail dl ';
        $strQuery .= 'WHERE hl.loan_id = dl.loan_id ';
        $strQuery .= 'AND hl.client_name = "'.$clientName.'" ';
        $strQuery .= 'AND hl.bio_rec_id = "'.$biodataId.'" ';
        $strQuery .= 'AND dl.year_period = "'.$yearPeriod.'" ';
        $strQuery .= 'AND dl.month_period = "'.$monthPeriod.'" ';
        $strQuery .= 'GROUP BY bio_rec_id';

        // echo $strQuery; exit();

        $loanRow = $this->db->query($strQuery)->row_array();
        $loanVal = $loanRow['amount'];
        $loanRemarks = $loanRow['remarks'];
        if($loanRemarks != ''){
           $loanRemarks = 'Pot ('.$loanRow['remarks'].') ke : '.$loanRow['numbering']; 
        }
        /* END GET EMP LOAN */

        $myData[19] = $loanVal; 
        $myData[20] = $loanRemarks; 
        echo json_encode($myData);   
    }  

    public function updatePayroll()
    {
        $clientName = "";
        $payrollId = "";
        $productionBonus = 0;
        $workAdjustment = 0;
        $adjustmentIn = 0;
        $adjustmentOut = 0;
        $contractBonus = 0;
        $outCamp = 0;
        $safetyBonus = 0;
        $attendanceBonus = 0;
        $otherAllowance1 = 0;
        $otherAllowance2 = 0;
        $noAccidentFee = 0;
        $ccPayment = 0;
        $thr = 0;
        $debtBurden = 0;
        $jumboPercent = 0;
        $jumboVal = 0;
        $debtExplanation = '';
        $salaryStatus = '';
        $statusInfo = '';


        if(isset($_POST['clientName']))
        {
            $clientName = $this->security->xss_clean($_POST['clientName']);            
        }
        if(isset($_POST['payrollId']))
        {
            $payrollId = $this->security->xss_clean($_POST['payrollId']);            
        }
        if(isset($_POST['productionBonus']))
        {
            $productionBonus = $this->security->xss_clean($_POST['productionBonus']);            
        }
        if(isset($_POST['workAdjustment']))
        {
            $workAdjustment = $this->security->xss_clean($_POST['workAdjustment']);            
        }

        if(isset($_POST['adjustmentIn']))
        {
            $adjustmentIn = $this->security->xss_clean($_POST['adjustmentIn']);            
        }

        if(isset($_POST['adjustmentOut']))
        {
            $adjustmentOut = $this->security->xss_clean($_POST['adjustmentOut']);            
        }

        if(isset($_POST['contractBonus']))
        {
            $contractBonus = $this->security->xss_clean($_POST['contractBonus']);            
        }

        if(isset($_POST['outCamp']))
        {
            $outCamp = $this->security->xss_clean($_POST['outCamp']);            
        }

        if(isset($_POST['safetyBonus']))
        {
            $safetyBonus = $this->security->xss_clean($_POST['safetyBonus']);            
        }

        if(isset($_POST['attendanceBonus']))
        {
            $attendanceBonus = $this->security->xss_clean($_POST['attendanceBonus']);            
        }

        if(isset($_POST['otherAllowance1']))
        {
            $otherAllowance1 = $this->security->xss_clean($_POST['otherAllowance1']);            
        }

        if(isset($_POST['otherAllowance2']))
        {
            $otherAllowance2 = $this->security->xss_clean($_POST['otherAllowance2']);            
        }
        
        if(isset($_POST['noAccidentFee']))
        {
            $noAccidentFee = $this->security->xss_clean($_POST['noAccidentFee']);            
        }
        if(isset($_POST['ccPayment']))
        {
            $ccPayment = $this->security->xss_clean($_POST['ccPayment']);            
        }

        if(isset($_POST['thr']))
        {
            $thr = $this->security->xss_clean($_POST['thr']);            
        }

        if(isset($_POST['debtBurden']))
        {
            $debtBurden = $this->security->xss_clean($_POST['debtBurden']);            
        }

        if(isset($_POST['debtExplanation']))
        {
            $debtExplanation = $this->security->xss_clean($_POST['debtExplanation']);            
        }

        if(isset($_POST['salaryStatus']))
        {
            $salaryStatus = $this->security->xss_clean($_POST['salaryStatus']);            
        }

        if(isset($_POST['statusInfo']))
        {
            $statusInfo = $this->security->xss_clean($_POST['statusInfo']);            
        }
        
        if(isset($_POST['jumboPercent']))
        {
            $jumboPercent = $this->security->xss_clean($_POST['jumboPercent']);            
        }

        $this->M_salary_slip->getObjectById($payrollId);
        $this->M_salary_slip->setProductionBonus($productionBonus);
        $this->M_salary_slip->setWorkdayAdj($workAdjustment);
        $this->M_salary_slip->setAdjustIn($adjustmentIn);
        $this->M_salary_slip->setAdjustOut($adjustmentOut);
        $this->M_salary_slip->setContractBonus($contractBonus);
        $this->M_salary_slip->setFlyingCamp($outCamp);
        $this->M_salary_slip->setSafetyBonus($safetyBonus);
        $this->M_salary_slip->setAttendanceBonus($attendanceBonus);
        $this->M_salary_slip->setOtherAllowance1($otherAllowance1);
        $this->M_salary_slip->setOtherAllowance2($otherAllowance2);
        $this->M_salary_slip->setNoAccidentBonus($noAccidentFee);
        $this->M_salary_slip->setCcPayment($ccPayment);
        $this->M_salary_slip->setThr($thr);
        $this->M_salary_slip->setDebtBurden($debtBurden);
        $this->M_salary_slip->setDebtExplanation($debtExplanation);

        if($clientName == 'Redpath_Timika')
        {
            $this->M_salary_slip->setOtherAllowance1($jumboPercent);
            $devIncentiveBonus = 0;
            if($salaryStatus == 'Warning'){
                      
            }else{
                $this->db->select('mr.*, tro.bio_rec_id, tro.attend_total, tro.in_ph_total,tro.alpa_total,tro.permit_total,tro.sick_total,tro.attend_total');
                $this->db->from('mst_roster mr, trn_overtime tro');  
                $this->db->where('mr.bio_rec_id = tro.bio_rec_id');               
                $this->db->where('mr.bio_rec_id = "'.$this->M_salary_slip->getBioRecId().'" ');
                $this->db->where('mr.year_process = "'.$this->M_salary_slip->getYearPeriod().'" ');
                $this->db->where('mr.month_process = "'.$this->M_salary_slip->getMonthPeriod().'" ');
                $this->db->where('tro.year_period = "'.$this->M_salary_slip->getYearPeriod().'" ');
                $this->db->where('tro.month_period = "'.$this->M_salary_slip->getMonthPeriod().'" ');
                $this->db->where('tro.client_name = "'.$this->M_salary_slip->getClientName().'" ');
                $row = $this->db->get()->row_array();

                $alpaTotal = $row['alpa_total'];
                $permitTotal = $row['permit_total']; 
                $sickTotal = $row['sick_total'];
                $basicSalary = $this->M_salary_slip->getBasicSalary();
                $attendTotal = $row['attend_total'];

                /* EXCLUDE If Work Hours Less Than 8 Hours */
                $excludeCountDay = 0;
                $colTmp = '';
                for($z=1; $z <= 31; $z++){
                    if($z < 10){
                        $colTmp = 'd0'.$z;
                    }else{
                        $colTmp = 'd'.$z;
                    }
                    if( is_numeric($row[$colTmp]) && ($row[$colTmp] > 0) && ($row[$colTmp] < 8) ){
                        $excludeCountDay++;
                    } 

                    if( substr($row[$colTmp],0,2) == 'PH' ){
                        $phVal = substr($row[$colTmp],2, strlen($row[$colTmp]-2));
                        if( is_numeric($phVal) && ($phVal > 0) && ($phVal < 8) ){
                            $excludeCountDay++; 
                        }
                    }                    
                }
                // echo $excludeCountDay; exit(0);

                $alpaSickPercent = 0;
                for ($m=0; $m < ($alpaTotal-$permitTotal); $m++) { 
                    $alpaSickPercent += 50;
                }
                for ($m=0; $m < $sickTotal; $m++) { 
                    $alpaSickPercent += 15;
                }

                $devIncentiveBonus = 0;
                if($alpaSickPercent < 100)
                {
                    /* FORMULA CHANGE (35/100)/27) to (35/100)/26) BASE ON  */
                    /* FORMULA CHANGE (Max Day Count is 26) BASE ON Ntus Email @ 26-11-2018 */
                    $tdayCount = $row['attend_total'] + $row['in_ph_total'] - $excludeCountDay;
                    if($tdayCount > 26){
                       $tdayCount = 26; 
                    }
                    // $tmp = $basicSalary * ((35/100)/26) * $tdayCount;
                    $tmp = $basicSalary * ((50/100)/26) * $tdayCount;
                    /* (35%)/27 x Jumlah Masuk Kerja Yang Lebih Dari 7 Jam (Jika ada alpa, potong 50% perhari, Sakit 15% perhari ) */
                    $devIncentiveBonus = $tmp - ($tmp * ($alpaSickPercent/100));    
                }  

                if($devIncentiveBonus <= 0) {
                   $devIncentiveBonus = 0; 
                }  
                $devPercent = $this->M_salary_slip->getDevPercent();
                $jumboVal = ($devIncentiveBonus * ($devPercent/100)) * ($jumboPercent/100);  

                /* END DEVELOPMENT INCENTIVE */ 
            }
            $this->M_salary_slip->setSalaryStatus($salaryStatus);
            $this->M_salary_slip->setStatusRemarks($statusInfo);  
            $this->M_salary_slip->setDevIncentiveBonus($devIncentiveBonus);  
            $this->M_salary_slip->setOtherAllowance1($jumboPercent);  
            $this->M_salary_slip->setOtherAllowance2($jumboVal);  
        }
        $this->M_salary_slip->update($payrollId);
        // echo $payrollId; exit();
        // $this->M_salary_slip->loadById($payrollId);
        // $rowData = array(
        //     'payrollId' => $payrollId,
        //     'devPercent' => $this->M_salary_slip->getDevPercent()
        // ); 
        // echo json_encode($rowData);
    }

    public function getAccidentBonus() {
        $clientName = '';
        $slipId = '';
        if(isset($_POST['slipId'])) {
            $slipId = $_POST['slipId'];
        }

        if(isset($_POST['clientName'])) {
            $clientName = $_POST['clientName'];
        }
        
        $strQuery  = 'SELECT ss.salary_slip_id, ss.client_name, TRUNCATE(((cnf.no_accident_percent/100) * ss.basic_salary),2) AS no_acc_fee ';
        $strQuery .= ' FROM mst_payroll_config cnf, trn_salary_slip ss ';
        $strQuery .= ' WHERE  ss.salary_slip_id = "'.$slipId.'" ';
        $strQuery .= ' AND cnf.client_name = "'.$clientName.'" ';
        $strQuery .= ' AND cnf.client_name = ss.client_name ';

        $data = $this->db->query($strQuery)->row_array();
        echo $data['no_acc_fee']; 
    }

    /* START REDPATH TO EXCEL */
    public function redpathExcel($slipId, $calendarStart, $payrollGroup, $isHealthBPJS, $isJHT, $isJP, $isJKKM)
    {
        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();

        $spreadsheet->getProperties()->setCreator('Maurice - Web - Android')
            ->setLastModifiedBy('Maurice - Web - Android')
            ->setTitle('Office 2007 XLSX Test Document')
            ->setSubject('Office 2007 XLSX Test Document')
            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Test result file');

        $boldFont = [
            'font' => [
                'bold' => true
                // 'color' => ['argb' => '0000FF'],
            ],
        ];

        $totalStyle = [
            'font' => [
                'bold' => true,
                'color' => ['argb' => '0000FF'],
            ],
        ];

        $allBorderStyle = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $outlineBorderStyle = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $topBorderStyle = [
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $bottomBorderStyle = [
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        // $center = [
        //     'alignment' => [
        //         'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
        //         'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
        //     ],
        // ];

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        /* CALENDAR SETUP */
        $dateCal = $calendarStart;

        /* START VALIDATE & UPDATE GOVERNMENT REGULATION */     
        $this->M_payroll_config->loadByClient('Pontil_Timika');
        $myConfigId = $this->M_payroll_config->getPayrollConfigId();
        // $this->M_payroll_config->loadById($myConfigId); 
        $healthBpjsConfig = $this->M_payroll_config->getHealthBpjs();
        $maxHealthBpjsConfig = $this->M_payroll_config->getMaxHealthBpjs();
        $jkkJkmConfig = $this->M_payroll_config->getJkkJkm();
        $jhtConfig = $this->M_payroll_config->getJht();
        $empJhtConfig = $this->M_payroll_config->getEmpJht();             
        $empHealthBpjsConfig = $this->M_payroll_config->getEmpHealthBpjs();    
        $jpConfig = $this->M_payroll_config->getJp();
        $empJpConfig = $this->M_payroll_config->getEmpJp();
        // echo $empHealthBpjsConfig; exit(0);
        $npwpCharge = $this->M_payroll_config->getNpwpCharge();
        $this->M_salary_slip->getObjectById($slipId);
        $basicSalary = $this->M_salary_slip->getBasicSalary();
        // echo $healthBpjsConfig; exit(0);
        /* START HEALTH BPJS */
        $healthBpjs = ($healthBpjsConfig/100) * $basicSalary; /* Nilai mst_payroll_config.health_bpjs(%) x Basic Salary (Max 8 Juta) */
        if($healthBpjs > $maxHealthBpjsConfig){
            $healthBpjs = $maxHealthBpjsConfig; 
        }
        /* END HEALTH BPJS */
        /* START JKK-JKM */
        $jkkJkm = ($jkkJkmConfig/100) * $basicSalary; /* Nilai mst_payroll_config.jkk_jkm(%) x Basic Salary */ 
        /* END JKK-JKM */                
        /* START JHT */
        $jht = ($jhtConfig/100) * $basicSalary;  /*Nilai mst_payroll_config.jht(%) x Basic Salary  */       
        /* END JHT */                
        /* START JHT */
        $empJht = ($empJhtConfig/100) * $basicSalary; /* Nilai mst_payroll_config.emp_jht(%) x Basic Salary */      
        /* END JHT */
        /* START JP COMPANY */
        // $jp = ($jpConfig/100) * $basicSalary; /* Nilai mst_payroll_config.jp(%) x Basic Salary */        
        /* END JP COMPANY */
        /* START JP EMPLOYEE */
        $empJp = ($empJpConfig/100) * $basicSalary; /* Nilai mst_payroll_config.emp_jp(%) x Basic Salary */        
        /* END JP EMPLOYEE */               
        
        /* START BPJS KARYAWAN */
        $maxEmpHealthBpjs = $this->M_payroll_config->getMaxEmpBpjs(); /* Nilai Max Gaji Untuk Iuran BPJS Karyawan  */ 
        $empHealthBpjs = ($empHealthBpjsConfig/100) * $basicSalary; /* Nilai mst_payroll_config.emp_health_bpjs(%) x Basic Salary */
        if($empHealthBpjs > $maxEmpHealthBpjs)
        {
            $empHealthBpjs = $maxEmpHealthBpjs; 
        }     
        /* END BPJS KARYAWAN */        

        if($isHealthBPJS == '0')
        {
            $healthBpjs = 0;
            $empHealthBpjs = 0;
        }
        if($isJHT == '0')
        {
            $jht = 0;
            $empJht = 0;
        }
        if($isJP == '0')
        {
            $empJp = 0;
        }
        if($isJKKM == '0')
        {
            $jkkJkm = 0;
        }

        $this->M_salary_slip->setHealthBpjs($healthBpjs);
        $this->M_salary_slip->setEmpHealthBpjs($empHealthBpjs);
        $this->M_salary_slip->setJht($jht);
        $this->M_salary_slip->setEmpJht($empJht);
        $this->M_salary_slip->setEmpJp($empJp);
        $this->M_salary_slip->setJkkJkm($jkkJkm);
        $this->M_salary_slip->update($slipId);
        /* END VALIDATE & UPDATE GOVERNMENT REGULATION */

        $this->db->select('ss.*, mb.bio_rec_id biodata_id, ss.basic_salary salary_basic,ss.bs_prorate, mb.npwp_no, tro.*, mr.*');
        $this->db->from('trn_salary_slip ss, mst_bio_rec mb');
        $this->db->join('mst_roster mr','mb.bio_rec_id = mr.bio_rec_id');
        $this->db->join('trn_overtime tro','mb.bio_rec_id = tro.bio_rec_id');
        $this->db->where('ss.bio_rec_id = mb.bio_rec_id AND ss.client_name = "Redpath_Timika"');
        $this->db->where('salary_slip_id', $slipId);
        $this->db->where('ss.year_period = tro.year_period AND ss.month_period = tro.month_period');        
        $this->db->where('ss.year_period = mr.year_process AND ss.month_period = mr.month_process');        
        $rowData = $query = $this->db->get()->row_array();  // Produces: SELECT * FROM trn_salary_slip (one row only) 
        $ptName = $rowData['client_name'];  
        $monthPeriod = $rowData['month_period'];
        $yearPeriod = $rowData['year_period'];     
        $biodataId = $rowData['biodata_id'];     
        $nie = $rowData['nie'];     
        $dept = $rowData['dept'];     
        $rosterFormat = $rowData['roster_format'];     
        $rosterBase = $rowData['roster_base']; 
        $payrollLevel = $rowData['payroll_level']; 
        $npwpNo = $rowData['npwp_no'];
        

        $tMonth = (int) $monthPeriod;
        $tYear = (int) $yearPeriod;
        // echo $tYear.' - '.$tMonth; exit();
        // echo $this->db->last_query();exit();
        $dayCountInMonth = cal_days_in_month(CAL_GREGORIAN, $tMonth, $tYear);  
        // $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('A')->setAutoSize(false);
        // $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('A')->setWidth('2');
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(6);             
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(9);             
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(7);             
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(8);         
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(5);             
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(5);             
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(5);             
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(5);             
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(5);             
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(3);             
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(8);   

        $spreadsheet->getActiveSheet()
            ->setCellValue('A1', 'PT. SANGATI SOERYA SEJAHTERA')
            ->setCellValue('A2', 'Slip Gaji Karyawan')
            ->setCellValue('A4', 'PT : '.$ptName)
            ->setCellValue('A5', 'Periode : '.$monthPeriod.' - '.$yearPeriod);

        $spreadsheet->getActiveSheet()
            ->setCellValue('N4', 'Id')
            ->setCellValue('O4', $nie)
            ->setCellValue('N5', 'Dept')
            ->setCellValue('O5', $dept)
            ->setCellValue('N6', 'Level')
            ->setCellValue('O6', $payrollLevel)
            ->setCellValue('S6', 'Base   : '.$rosterBase)
            ->setCellValue('S7', 'Format : '.$rosterFormat);

        $spreadsheet->getActiveSheet()->getStyle("A1:D1")->getFont()->setBold(true)->setSize(16);
        $spreadsheet->getActiveSheet()->getStyle("A2:D2")->getFont()->setBold(true)->setSize(14)->setUnderline(true);
        $spreadsheet->getActiveSheet()->getStyle("A4:G4")->getFont()->setBold(true)->setSize(12);     

        $spreadsheet->getActiveSheet()->getStyle('M7:M8')->getNumberFormat()->setFormatCode('#,##0.00');
        $spreadsheet->getActiveSheet()->getStyle('N9:O68')->getNumberFormat()->setFormatCode('#,##0.00');

        $spreadsheet->getActiveSheet()
            ->setCellValue('A6', 'DATE')
            ->setCellValue('B6', 'ROSTER DAY')
            ->setCellValue('C6', 'SHIFT DAY')
            ->setCellValue('D6', 'HOURS TOTAL')
            ->setCellValue('E6', 'NT')
            ->setCellValue('F6', 'OVER TIME')
            ->setCellValue('F7', '1')
            ->setCellValue('G7', '2')
            ->setCellValue('H7', '3')
            ->setCellValue('I7', '4');
        
        /* END OVER TIME  */
        /* END ROSTER TITLE */

        $rosterFormat = $rowData['roster_format'];
        $tmpRosterLength = strlen($rosterFormat);
        $strNumRoster = '';
        $tmp = '';
        $dayNo = '';
        $groupCount = 0;
        $strNum = '';
        $dayTotal = 0;
        /* START GET DAYS TOTAL BY ROSTER */
        for ($i=0; $i < $tmpRosterLength; $i++) {
            $strNum = substr($rosterFormat,$i,1);
            /* START MAKE SURE DATA IS NUMBER */ 
            if(is_numeric($strNum))
            {
                $strNumRoster .= $strNum;
                $dayTotal += $strNum;
            }
            /* END MAKE SURE DATA IS NUMBER */ 
        }

        $numChar = '';
        $rdData = '';
        $dataIdx = 8;
        $tIdx = 0;
        for ($m=0; $m < strlen($strNumRoster); $m++) { 
            $numChar = substr($strNumRoster,$m,1);

            for ($k=1; $k <= $numChar; $k++) { 
                # code...
                $dataIdx++;

                /* START GET PUBLIC HOLIDAY BY ROSTER MASTER */
                $tIdx++;
                $tColIdx = '';
                if($tIdx < 10){
                    $tColIdx = 'd0'.$tIdx;
                }else{
                    $tColIdx = 'd'.$tIdx;
                }
                $tVal = substr($rowData[$tColIdx],0,2);

                if($tVal == 'PH'){
                    $rdData = 'PH';
                }else{
                    $rdData = $k;
                }
                /* END GET PUBLIC HOLIDAY BY ROSTER MASTER */


                if($m % 2 == 0){
                    $tmp .= $k."  ";                 
                    // $rdData = $k;
                }else{
                    $tmp .= "RO";
                    $rdData = "RO";
                }
                /* START ROSTER DAY TITLE */
                // $dayCountInMonth = cal_days_in_month(CAL_GREGORIAN, $monthPeriod, $yearPeriod);
                if($dataIdx <= ($dayCountInMonth+8))
                {
                    $spreadsheet->getActiveSheet()->getStyle("B".$dataIdx.":B".$dataIdx)->applyFromArray($allBorderStyle);
                    $spreadsheet->getActiveSheet()->getStyle("B".$dataIdx.":B".$dataIdx)->applyFromArray($right);
                    $spreadsheet->getActiveSheet()
                        ->setCellValue('B'.($dataIdx), $rdData);

                }
                /* END ROSTER DAY TITLE */
            }
        }
            // echo $tmp; exit(0);

            /* END GET DAYS TOTAL BY ROSTER */ 

        $ot01Count = 0;
        $ot02Count = 0;
        $ot03Count = 0;
        $ot04Count = 0;

        $normalTotal = 0;
        $allTimeTotal = 0;
        /* START SUMMARY ROSTER */
        $rowIdx = 8;
        /* START CALENDAR VALUE */
        // $dayCount = cal_days_in_month(CAL_GREGORIAN, $monthPeriod, $yearPeriod);   
        $shiftCount = 0;
        for ($i=1; $i <= $dayCountInMonth; $i++) { 
            /* START NUMBER TITLE */
            $rowIdx++;

            $ot01Column = '';
            $ot02Column = '';
            $ot03Column = '';
            $ot04Column = '';

            if($i < 10){
                $ot01Column = 'ot1_d0'.$i;
                $ot02Column = 'ot2_d0'.$i;
                $ot03Column = 'ot3_d0'.$i;
                $ot04Column = 'ot4_d0'.$i;
                $timePerday = 'd0'.$i;
            }else{
                $ot01Column = 'ot1_d'.$i;
                $ot02Column = 'ot2_d'.$i;
                $ot03Column = 'ot3_d'.$i;
                $ot04Column = 'ot4_d'.$i;
                $timePerday = 'd'.$i;
            }
            /* OVER TIME */
            $ot01Val = $rowData[$ot01Column];
            $ot02Val = $rowData[$ot02Column];
            $ot03Val = $rowData[$ot03Column];
            $ot04Val = $rowData[$ot04Column];
            
            $timeTotal = 0;
            $strTimePerday = $rowData[$timePerday];
            $tTimePerday = 0;

            /* START PUBLIC HOLIDAY  */
            $phCodeTmp = substr($strTimePerday,0,2);
            $phHoursTmp = substr($strTimePerday,2,strlen($strTimePerday)-2);
            if( (strtoupper($phCodeTmp) == "PH") && (is_numeric($phHoursTmp)) && ($phHoursTmp > 0) )
            {
                $tTimePerday = $phHoursTmp;
            }
            else
            {
                $tTimePerday = $rowData[$timePerday];
            }

            if(is_numeric($tTimePerday))
            {
                $timeTotal = $tTimePerday;
            }

            $normalTime = $timeTotal - $ot01Val - $ot02Val - $ot03Val - $ot04Val; 
            $normalTotal += $normalTime;
            $allTimeTotal += $timeTotal; 
            $ot01Count += $ot01Val;
            $ot02Count += $ot02Val;
            $ot03Count += $ot03Val;
            $ot04Count += $ot04Val;
                         
            if($dateCal > $dayCountInMonth){
               $dateCal = 1; 
            }

            $spreadsheet->getActiveSheet()->getStyle("A".$rowIdx.":A".$rowIdx)->applyFromArray($allBorderStyle);
            $spreadsheet->getActiveSheet()->getStyle("A".$rowIdx.":A".$rowIdx)->applyFromArray($right);
            $spreadsheet->getActiveSheet()
                        ->setCellValue('A'.($rowIdx), $dateCal);
            $dateCal++;
            /* END NUMBER TITLE */

            /* START SHIFT DAY TITLE */
            if( strtoupper($phHoursTmp) == 'PH')
            {
                $attendStatus = 'PH';
            }
            else if(is_numeric($tTimePerday)){
                if($tTimePerday > 0 ){
                    $attendStatus = 1;
                    $shiftCount++;
                }else{
                    $attendStatus = 0;
                }
            }else if($tTimePerday == ''){
                $attendStatus = 0;        
            }else {
                $attendStatus = $tTimePerday;        
            }
            $spreadsheet->getActiveSheet()->getStyle("C".$rowIdx.":C".$rowIdx)->applyFromArray($allBorderStyle);
            $spreadsheet->getActiveSheet()->getStyle("C".$rowIdx.":C".$rowIdx)->applyFromArray($center);
            $spreadsheet->getActiveSheet()
                        ->setCellValue('C'.($rowIdx), $attendStatus);
            /* END SHIFT DAY TITLE */

            /* START HOURS TOTAL TITLE */
            $spreadsheet->getActiveSheet()->getStyle("D".$rowIdx.":D".$rowIdx)->applyFromArray($allBorderStyle);
            $spreadsheet->getActiveSheet()->getStyle("D".$rowIdx.":D".$rowIdx)->applyFromArray($right);
            $spreadsheet->getActiveSheet()
                        ->setCellValue('D'.($rowIdx), $timeTotal);
            /* END HOURS TOTAL TITLE */

            /* START NORMAL TIME TITLE */
            $spreadsheet->getActiveSheet()->getStyle("E".$rowIdx.":E".$rowIdx)->applyFromArray($allBorderStyle);
            $spreadsheet->getActiveSheet()->getStyle("E".$rowIdx.":E".$rowIdx)->applyFromArray($right);
            $spreadsheet->getActiveSheet()
                        ->setCellValue('E'.($rowIdx), $normalTime);
            /* END NORMAL TIME TITLE */

            /* START OVERTIME 1 TITLE */
            $spreadsheet->getActiveSheet()->getStyle("F".$rowIdx.":I".$rowIdx)->applyFromArray($allBorderStyle);
            $spreadsheet->getActiveSheet()->getStyle("F".$rowIdx.":I".$rowIdx)->applyFromArray($right);
            $spreadsheet->getActiveSheet()
                        ->setCellValue('F'.($rowIdx), $ot01Val)
                        ->setCellValue('G'.($rowIdx), $ot02Val)
                        ->setCellValue('H'.($rowIdx), $ot03Val)
                        ->setCellValue('I'.($rowIdx), $ot04Val)
                        ;
        } // for ($i=1; $i <= 31 ; $i++)
        /* END SUMMARY ROSTER */        

        /* START TOTAL */
        /* START TITLE */
        // $totalTitle = 40;
        $spreadsheet->getActiveSheet()->getStyle('A40:A41')->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle('A40:A41')->getFont()->setBold(true)->setSize(13);
        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(0, $totalTitle, "TOTAL");
        $otSum = $ot01Count + $ot02Count + $ot03Count + $ot04Count;
        $spreadsheet->getActiveSheet()
                        ->setCellValue('A40', 'TOTAL')
                        ->setCellValue('A41', 'TOTAL OT')
                        ->setCellValue('C40', $shiftCount)
                        ->setCellValue('D40', $allTimeTotal)
                        ->setCellValue('E40', $normalTotal)
                        ->setCellValue('F40', $ot01Count)
                        ->setCellValue('G40', $ot02Count)
                        ->setCellValue('H40', $ot03Count)
                        ->setCellValue('I40', $ot04Count)
                        ->setCellValue('C41', $otSum)
                        ;
        /* END TITLE */

        /* START TOTAL ROSTER DATA */
        /* Shift Day */
        
        /* Data */
        
        
        /* END TOTAL OVER TIME  */
        /* END TOTAL */

        /* START DATA PAYROLL */
        /* START PERSONAL DATA */

        // $this->load->model('M_payroll_config');
        $basicSalary = $rowData['salary_basic'];
        $bsProrate = $rowData['bs_prorate'];
        if($bsProrate > $basicSalary)
        {
            $bsProrate = $basicSalary;   
        }

        $this->M_payroll_config->loadByClient($ptName);
        $salaryDividerConfig = $this->M_payroll_config->getSalaryDivider();
        $salaryHourly = $basicSalary / $salaryDividerConfig;
        // $salaryHourly = number_format($salaryHourly,2);

        $otMultiplierConfig01 = $this->M_payroll_config->getOt01Multiplier();
        // $otMultiplierConfig01 = number_format($otMultiplierConfig01,1);
        $otMultiplierConfig02 = $this->M_payroll_config->getOt02Multiplier();
        // $otMultiplierConfig02 = number_format($otMultiplierConfig02,1);
        $otMultiplierConfig03 = $this->M_payroll_config->getOt03Multiplier();
        // $otMultiplierConfig03 = number_format($otMultiplierConfig03,1);
        $otMultiplierConfig04 = $this->M_payroll_config->getOt04Multiplier(); 
        // $otMultiplierConfig04 = number_format($otMultiplierConfig04,1);

        // echo $salaryHourly; exit(0);
        $spreadsheet->getActiveSheet()->getStyle('A40:A40')->getFont()->setBold(true)->setSize(13);
        $spreadsheet->getActiveSheet()->getStyle('C40:I40')->getFont()->setBold(true)->setSize(13);
        $spreadsheet->getActiveSheet()->getStyle("C41:I41")->getFont()->setBold(true)->setSize(13);
        $spreadsheet->getActiveSheet()->getStyle("A6:I7")->getFont()->setBold(true)->setSize(12);

        $spreadsheet->getActiveSheet()->getStyle('C40:I40')->applyFromArray($right);
        $spreadsheet->getActiveSheet()->getStyle('C41:I41')->applyFromArray($right);

        $spreadsheet->getActiveSheet()->getStyle("C41:I41")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle('C40:I40')->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle('A6:I7')->applyFromArray($allBorderStyle);

        $spreadsheet->getActiveSheet()->getStyle('A40:A41')->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle('A6:I7')->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle('A48:I66')->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle('K50:O50')->applyFromArray($center);

        $spreadsheet->getActiveSheet()->getStyle('K4:L4')->applyFromArray($left);

        $spreadsheet->getActiveSheet()->getStyle('K4:O68')->getFont()->setSize(13);
        $spreadsheet->getActiveSheet()->getStyle('K48:O48')->getFont()->setSize(13);

        $spreadsheet->getActiveSheet()->getStyle('K11:O11')->applyFromArray($totalStyle);
        $spreadsheet->getActiveSheet()->getStyle('K34:N34')->applyFromArray($totalStyle);
        $spreadsheet->getActiveSheet()->getStyle('M7:M8')->applyFromArray($totalStyle);
        $spreadsheet->getActiveSheet()->getStyle('K16:O16')->applyFromArray($totalStyle);
        $spreadsheet->getActiveSheet()->getStyle('K27:O27')->applyFromArray($totalStyle);                  
        $spreadsheet->getActiveSheet()->getStyle('K35:O35')->applyFromArray($totalStyle);
        $spreadsheet->getActiveSheet()->getStyle('K65:O66')->applyFromArray($totalStyle);
        $spreadsheet->getActiveSheet()->getStyle('K43:O43')->applyFromArray($totalStyle);
        $spreadsheet->getActiveSheet()->getStyle('K48:O48')->applyFromArray($totalStyle);
        $spreadsheet->getActiveSheet()->getStyle('K50:O50')->applyFromArray($totalStyle);

        $spreadsheet->getActiveSheet()->getStyle('K16:O16')->applyFromArray($topBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle('K65:O65')->applyFromArray($topBorderStyle);                   
        $spreadsheet->getActiveSheet()->getStyle('K27:O27')->applyFromArray($topBorderStyle); 
        $spreadsheet->getActiveSheet()->getStyle('K35:O35')->applyFromArray($topBorderStyle); 
        $spreadsheet->getActiveSheet()->getStyle('K43:O43')->applyFromArray($topBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle('K48:O48')->applyFromArray($topBorderStyle);

        $spreadsheet->getActiveSheet()->getStyle('K50:O50')->applyFromArray($bottomBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle('K54:O54')->applyFromArray($bottomBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle('K57:O57')->applyFromArray($bottomBorderStyle);

        $spreadsheet->getActiveSheet()->getStyle('A48:I66')->applyFromArray($outlineBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle('K50:O66')->applyFromArray($outlineBorderStyle);      
        $spreadsheet->getActiveSheet()->getStyle('K68:O68')->applyFromArray($topBorderStyle);      
        $spreadsheet->getActiveSheet()->getStyle('K68:O68')->applyFromArray($totalStyle);      

        $spreadsheet->getActiveSheet()->getStyle('B6')->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('C6')->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('D6')->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('E6')->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('F6')->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('F7')->getAlignment()->setWrapText(true);

        $spreadsheet->getActiveSheet()
            ->mergeCells("A6:A7")
            ->mergeCells("B6:B7")
            ->mergeCells("C6:C7")
            ->mergeCells("D6:D7")
            ->mergeCells("E6:E7");

        $spreadsheet->getActiveSheet()
            ->mergeCells("A40:B40")
            ->mergeCells("A41:B41")
            ->mergeCells("C41:I41")
            ->mergeCells("F6:I6")
            ->mergeCells("A49:I49")
            ->mergeCells("A53:I53")
            ->mergeCells("A57:I57")
            ->mergeCells("A61:I61")
            ->mergeCells("K50:O50")
            ;

        $spreadsheet->getActiveSheet()
            ->setCellValue('K4', 'NAMA')
            ->setCellValue('K5', 'JABATAN')
            ->setCellValue('K6', 'STATUS')
            ->setCellValue('K7', 'GAJI POKOK')
            ->setCellValue('K8', 'OT RATE/HOUR')
            ->setCellValue('K9', 'NPWP')
            ->setCellValue('K11', 'UPAH')
            ->setCellValue('M4', $rowData['name'])
            ->setCellValue('M5', $rowData['position'])
            ->setCellValue('M6', $rowData['marital_status'])
            ->setCellValue('M7', $basicSalary)
            ->setCellValue('M8', $salaryHourly)
            ->setCellValue('M9', $rowData['npwp_no'])
            ->setCellValue('O11', $bsProrate);
                
        
        /* END PERSONAL DATA */
        $spreadsheet->getActiveSheet()
            ->setCellValue('K12', 'OT 1')
            ->setCellValue('K13', 'OT 2')
            ->setCellValue('K14', 'OT 3')
            ->setCellValue('K15', 'OT 4');

        $otTotal1 = $rowData['ot_1']; 
        $otTotal2 = $rowData['ot_2']; 
        $otTotal3 = $rowData['ot_3']; 
        $otTotal4 = $rowData['ot_4']; 

        $allOt = $otTotal1+$otTotal2+$otTotal3+$otTotal4; 
        $otIdx = 12;

        $spreadsheet->getActiveSheet()
            ->setCellValue('M12', $rowData['ot_count1']." x ".$otMultiplierConfig01." x ".$salaryHourly." = Rp.")
            ->setCellValue('N12', $otTotal1)
            ->setCellValue('M13', $rowData['ot_count2']." x ".$otMultiplierConfig02." x ".$salaryHourly." = Rp.")
            ->setCellValue('N13', $otTotal2)
            ->setCellValue('M14', $rowData['ot_count3']." x ".$otMultiplierConfig03." x ".$salaryHourly." = Rp.")
            ->setCellValue('N14', $otTotal3)
            ->setCellValue('M15', $rowData['ot_count4']." x ".$otMultiplierConfig04." x ".$salaryHourly." = Rp.")
            ->setCellValue('N15', $otTotal4);

        /* START OVER TIME TOTAL */
        $spreadsheet->getActiveSheet()
            ->setCellValue('K16', 'OT TOTAL')
            ->setCellValue('O16', $allOt);        

        /* START BONUS, INCENTIVE */
        $spreadsheet->getActiveSheet()
            ->setCellValue('K18', 'Total OT * 35%')
            ->setCellValue('K19', 'Shift Bonus')
            ->setCellValue('K20', 'Remote Loc Allowance')
            ->setCellValue('K21', 'THR')
            ->setCellValue('K22', 'Dev Incentive Bonus')
            ->setCellValue('K23', 'Contract Bonus')
            ->setCellValue('K24', 'Remaining Contract(Sisa Kontrak)')
            ->setCellValue('K25', 'Accident Free')
            ->setCellValue('K26', 'Jumbo Bonus');

        $this->M_salary->loadByBiodataIdNie($biodataId, $nie);
        $isOtBonus = $this->M_salary->getIsOtBonus(); 
        $isShiftBonus = $this->M_salary->getIsShiftBonus();
        $isRemoteAllowance = $this->M_salary->getIsRemoteAllowance();
        $isDevIncentiveBonus = $this->M_salary->getIsDevIncentiveBonus();

        /* START BONUS TOTAL */
        $bonusTotal = 0;        

        if( $isOtBonus == True ) {
            $bonusTotal += $rowData['ot_bonus'];
            $spreadsheet->getActiveSheet()
                ->setCellValue('N18', $rowData['ot_bonus']);
        } else {
            $spreadsheet->getActiveSheet()
                ->setCellValue('N18', 0);
        } 

        if( $isShiftBonus == True ) {
            $bonusTotal += $rowData['shift_bonus'];
            $spreadsheet->getActiveSheet()
                ->setCellValue('N19', $rowData['shift_bonus']);
        } else {
            $spreadsheet->getActiveSheet()
                ->setCellValue('N19', 0);
        }

        if( $isRemoteAllowance == True ) {
            $bonusTotal += $rowData['remote_allowance'];
            $spreadsheet->getActiveSheet()
                ->setCellValue('N20', $rowData['remote_allowance']);
        } else {
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, ($bonusIdx+2), 0);
            $spreadsheet->getActiveSheet()
                ->setCellValue('N20', 0);
        }

        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, ($bonusIdx+3), $rowData['thr']);
        $spreadsheet->getActiveSheet()
                ->setCellValue('N21', $rowData['thr']);

        if( $isDevIncentiveBonus == True) {
            $devBonusVal = $rowData['dev_incentive_bonus'];
            $devPercent = $rowData['dev_percent'];
            $devResult = $devBonusVal * ($devPercent/100); 
            $bonusTotal += $devResult;
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, ($bonusIdx+4), $devResult);
            $spreadsheet->getActiveSheet()
                ->setCellValue('N22', $devResult);
        } else {
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, ($bonusIdx+4), 0);
            $spreadsheet->getActiveSheet()
                ->setCellValue('N22', 0);
        }

         $spreadsheet->getActiveSheet()
            ->setCellValue('N23', $rowData['contract_bonus'])
            ->setCellValue('N24', $rowData['workday_adj'])
            ->setCellValue('N25', $rowData['no_accident_bonus'])
            ->setCellValue('N26', $rowData['other_allowance2']);/* Jumbo Bonus */
        /* END BONUS, INCENTIVE */

        $bonusTotal += $rowData['thr'];
        $bonusTotal += $rowData['contract_bonus'];
        $bonusTotal += $rowData['workday_adj'];
        $bonusTotal += $rowData['no_accident_bonus'];
        $bonusTotal += $rowData['other_allowance2'];

        /* START ALPA */
        $unpaidTotal = $rowData['unpaid_total'];
        $tUnpaidCount = $rowData['unpaid_count'];
        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, ($regulationIdx+2), "Potongan Absensi ".$tUnpaidCount." Hari");
        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, ($regulationIdx+2), $unpaidTotal);
        /* END ALPA */
        $subTotalVal = $rowData['jkk_jkm'] + $rowData['health_bpjs'] + $rowData['jp'] - $unpaidTotal + $rowData['adjust_in'] - $rowData['adjust_out'];
        $brutto = $bsProrate + $allOt + $bonusTotal + $rowData['jkk_jkm'] + $rowData['health_bpjs'] +$rowData['jp']- $unpaidTotal + $rowData['adjust_in'] - $rowData['adjust_out'];

         /* TAX CONFIG */
        $myConfigId = 1; 
        $row = $this->M_payroll_config->getObjectById($myConfigId);       
        $maxNonTax = $this->M_payroll_config->getNonTaxAllowance(); /* Max Tunjangan Jabatan */

        $nonTaxAllowance = $brutto * (5/100);
        if($nonTaxAllowance > $maxNonTax){
           $nonTaxAllowance = $maxNonTax; 
        }

        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $taxCoordinate+4, $nonTaxAllowance);
        /* END TAX */
        $netto = $brutto - $rowData['emp_jht'] - $rowData['emp_jp'] - $nonTaxAllowance;

        $ptkpTotal = $rowData['ptkp_total'];
        /* Perhitungan pajak yang disetahunkan, hanya penghasilan tetap saja. Tunjangan tidak tetap seperti THR dihitung terpisah */
        $nettoSetahun = ( ($netto - $rowData['thr']-$rowData['contract_bonus']) * 12);
        $penghasilanPajak = 0;
        if($nettoSetahun >= $ptkpTotal)
        {
            $penghasilanPajak = $nettoSetahun - $ptkpTotal;
        }
        $pembulatanPenghasilan = floor($penghasilanPajak);
        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(14, ($yearNettoCoordinate+3), $pembulatanPenghasilan);

        $spreadsheet->getActiveSheet()
            ->setCellValue('K27', 'BONUS TOTAL')
            ->setCellValue('O27', $bonusTotal)
            ->setCellValue('K29', 'JKK & JKM(2.04%)')
            ->setCellValue('N29', $rowData['jkk_jkm'])
            ->setCellValue('K30', 'Iuran BPJS Kesehatan(4%)')
            ->setCellValue('N30', $rowData['health_bpjs'])
            ->setCellValue('K31', 'Potongan Absensi '.$tUnpaidCount.' Hari')
            ->setCellValue('N31', $unpaidTotal)
            ->setCellValue('K32', 'Adjustment In')
            ->setCellValue('N32', $rowData['adjust_in'])
            ->setCellValue('K33', 'Adjustment Out')
            ->setCellValue('N33', $rowData['adjust_out'])
            ->setCellValue('K34', 'Subtotal')
            ->setCellValue('N34', $subTotalVal)
            ->setCellValue('K35', 'TOTAL KOTOR')
            ->setCellValue('O35', $brutto)
            ->setCellValue('K50', 'PERHITUNGAN PAJAK PENGHASILAN')
            ->setCellValue('K51', 'Penghasilan Kotor')
            ->setCellValue('O51', $brutto)
            ->setCellValue('K52', 'Iuran JP TK(1%)')
            ->setCellValue('O52', $rowData['emp_jp'])
            ->setCellValue('K53', 'Iuran JHT')
            ->setCellValue('O53', $rowData['emp_jht'])
            ->setCellValue('K54', 'Tunjangan Jabatan')
            ->setCellValue('O54', $nonTaxAllowance)
            ->setCellValue('K55', 'Netto')
            ->setCellValue('O55', $netto)
            ->setCellValue('K56', 'Netto Disetahunkan(Tetap)')
            ->setCellValue('O56', $nettoSetahun)
            ->setCellValue('K57', 'PTKP '.$rowData['marital_status'])
            ->setCellValue('O57', $ptkpTotal)
            ->setCellValue('K58', 'Penghasilan Kena Pajak')
            ->setCellValue('O58', $penghasilanPajak)
            ->setCellValue('K59', 'Pembulatan')
            ->setCellValue('O59', $pembulatanPenghasilan);

        
        /* TAX CONFIG */
        // $this->load->model('M_payroll_config');
        $taxPercent1 = $this->M_payroll_config->getTaxPercent1(); /* Persentase Progresif Pajak I */
        $taxPercent2 = $this->M_payroll_config->getTaxPercent2(); /* Persentase Progresif Pajak II */
        $taxPercent3 = $this->M_payroll_config->getTaxPercent3(); /* Persentase Progresif Pajak III */
        $taxPercent4 = $this->M_payroll_config->getTaxPercent4(); /* Persentase Progresif Pajak IV */
        $maxTaxVal1 = $this->M_payroll_config->getTaxMaxVal1(); /* Nominal Pajak I */ 
        $maxTaxVal2 = $this->M_payroll_config->getTaxMaxVal2(); /* Nominal Pajak II */
        $maxTaxVal3 = $this->M_payroll_config->getTaxMaxVal3(); /* Nominal Pajak III */
        $maxTaxVal4 = $this->M_payroll_config->getTaxMaxVal4(); /* Nominal Pajak IV */

        /* START WITHOUT UNFIXED VALUE */
        $taxVal1 = 0;    
        $taxVal2 = 0;    
        $taxVal3 = 0;    
        $taxVal4 = 0;

        $tVal = 0;
        $tSisa = 0;
        if($pembulatanPenghasilan > 0)
        {
            /* TAX 1 */
            if($maxTaxVal1 > 0)
            {
                $tVal = $pembulatanPenghasilan/$maxTaxVal1;  
                if($tVal >= 1){
                    $taxVal1 = $maxTaxVal1 * ($taxPercent1/100);
                }
                else{
                    $taxVal1 = $pembulatanPenghasilan * ($taxPercent1/100);
                }    
            }    
            
            /* TAX 2 */
            if($maxTaxVal2 > 0)
            {
                if($pembulatanPenghasilan > $maxTaxVal1)
                {
                    $tSisa = $pembulatanPenghasilan - $maxTaxVal1; 
                    $tVal = $tSisa/$maxTaxVal2;
                    if($tVal >= 1){
                        $taxVal2 = $maxTaxVal2 * ($taxPercent2/100);
                    }
                    else{
                        $taxVal2 = $tSisa * ($taxPercent2/100);
                    }
                }     
            }
             
            /* TAX 3 */
            if($maxTaxVal3 > 0)
            {
                if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2))
                {
                    $tSisa = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2;
                    $tVal = $tSisa/$maxTaxVal3;
                    if($tVal >= 1){
                        $taxVal3 = $maxTaxVal3 * ($taxPercent3/100);
                    }
                    else{
                        $taxVal3 = $tSisa * ($taxPercent3/100);
                    }
                }    
            }
             
            /* TAX 4 */
            if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2 + $maxTaxVal3))
            {
                $tSisa = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2 - $maxTaxVal3;
                $taxVal4 = $tSisa * ($taxPercent4/100); 
            }               
        }      

        $pajakTerutang = $taxVal1 + $taxVal2 + $taxVal3 + $taxVal4;
        $pajakSebulan = ($pajakTerutang/12);
        $pajakSebulan = floor($pajakSebulan);
        /* END WITHOUT UNFIXED VALUE */



        $spreadsheet->getActiveSheet()
            ->setCellValue('K61', "Tarif Pajak I    ".$taxPercent1."%")
            ->setCellValue('O61',  $taxVal1)
            ->setCellValue('K62', "Tarif Pajak II   ".$taxPercent2."%")
            ->setCellValue('O62', $taxVal2)
            ->setCellValue('K63', "Tarif Pajak III  ".$taxPercent3."%")
            ->setCellValue('O63', $taxVal3)
            ->setCellValue('K64', "Tarif Pajak IV   ".$taxPercent4."%")
            ->setCellValue('O64', $taxVal4)
            ->setCellValue('K65', "Pajak Gaji Setahun")
            ->setCellValue('O65', $pajakTerutang)
            ->setCellValue('K66', "Pajak Gaji Sebulan")
            // ->setCellValue('O66', $pajakSebulan)
            ;





        /* START WITH UNFIXED VALUE */
        $nettoSetahun2 = ( ($netto - $rowData['thr']-$rowData['contract_bonus']) * 12) + $rowData['thr'] + $rowData['contract_bonus'];
        $penghasilanPajak2 = 0;
        if($nettoSetahun2 >= $ptkpTotal)
        {
            $penghasilanPajak2 = $nettoSetahun2 - $ptkpTotal;
        }
        $pembulatanPenghasilan = floor($penghasilanPajak2);

        $taxVal1 = 0;    
        $taxVal2 = 0;    
        $taxVal3 = 0;    
        $taxVal4 = 0;

        $tVal = 0;
        $tSisa = 0;
        if($pembulatanPenghasilan > 0)
        {
            /* TAX 1 */
            if($maxTaxVal1 > 0)
            {
                $tVal = $pembulatanPenghasilan/$maxTaxVal1;  
                if($tVal >= 1){
                    $taxVal1 = $maxTaxVal1 * ($taxPercent1/100);
                }
                else{
                    $taxVal1 = $pembulatanPenghasilan * ($taxPercent1/100);
                }    
            }    
            
            /* TAX 2 */
            if($maxTaxVal2 > 0)
            {
                if($pembulatanPenghasilan > $maxTaxVal1)
                {
                    $tSisa = $pembulatanPenghasilan - $maxTaxVal1; 
                    $tVal = $tSisa/$maxTaxVal2;
                    if($tVal >= 1){
                        $taxVal2 = $maxTaxVal2 * ($taxPercent2/100);
                    }
                    else{
                        $taxVal2 = $tSisa * ($taxPercent2/100);
                    }
                }     
            }
             
            /* TAX 3 */
            if($maxTaxVal3 > 0)
            {
                if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2))
                {
                    $tSisa = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2;
                    $tVal = $tSisa/$maxTaxVal3;
                    if($tVal >= 1){
                        $taxVal3 = $maxTaxVal3 * ($taxPercent3/100);
                    }
                    else{
                        $taxVal3 = $tSisa * ($taxPercent3/100);
                    }
                }    
            }
             
            /* TAX 4 */
            if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2 + $maxTaxVal3))
            {
                $tSisa = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2 - $maxTaxVal3;
                $taxVal4 = $tSisa * ($taxPercent4/100); 
            }               
        }     
        $pajakTerutang2 = $taxVal1 + $taxVal2 + $taxVal3 + $taxVal4;
        $pajakUnFixed = $pajakTerutang2 - $pajakTerutang; 
        // $pajakSebulan2 = ($pajakUnFixed/12);
        // $pajakSebulan2 = floor($pajakSebulan2); 
        /* END WITH UNFIXED VALUE */

        $spreadsheet->getActiveSheet()
            ->setCellValue('K68', "Pajak Non Reguler");
            // ->setCellValue('O68', $pajakUnFixed);


        /* Additional Charge if there is no NPWP */
        $pajakDenda = 0;
        $pajakFixUnDenda = 0;
        if( strlen($npwpNo) < 19 ){
            $pajakSebulan = $pajakSebulan + ($pajakSebulan * ($npwpCharge/100) ); 
            $pajakUnFixed = $pajakUnFixed + ($pajakUnFixed * ($npwpCharge/100) ); 
            $spreadsheet->getActiveSheet()
                // ->setCellValue('O66', $pajakSebulan)
                // ->setCellValue('O68', $pajakUnFixed)
                ->setCellValue('K70', '* Tidak Memiliki NPWP dikenakan pajak plus 20% ');
            $spreadsheet->getActiveSheet()->getStyle("K70")->getFont()
            ->setSize(14)
            ->getColor()
            ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);
        }

        $spreadsheet->getActiveSheet()
                ->setCellValue('O66', $pajakSebulan)
                ->setCellValue('O68', $pajakUnFixed);

        $pajakSebulan+= $pajakUnFixed;
        // $pajakSebulan+= $pajakDenda;
        // $pajakSebulan+= $pajakFixUnDenda;

        /* START UPDATE TAX VALUE TO THE TABLE */            
        $str = "UPDATE trn_salary_slip SET tax_value = ".($pajakSebulan).", non_tax_allowance=".$nonTaxAllowance." WHERE salary_slip_id = '".$slipId."' ";
        $this->db->query($str);
        /* END UPDATE TAX VALUE TO THE TABLE */

        /* START OUT */
        // $outCoordinate = $bruttoCoordinate+2;
        $terima = $brutto-floor($pajakSebulan)-$rowData['jkk_jkm']-$rowData['emp_jht']-$rowData['emp_health_bpjs']-$rowData['emp_jp']-$rowData['health_bpjs']-$rowData['jp'];
        
        $spreadsheet->getActiveSheet()
            ->setCellValue('K37', "Pajak Penghasilan")
            ->setCellValue('K38', "JKK & JKM(2.04%)")
            ->setCellValue('K39', "Iuran JHT BPJS TK(2%)")
            ->setCellValue('K40', "Iuran BPJS Kesehatan TK(1%)")
            ->setCellValue('K41', "Iuran BPJS Kesehatan(4%)")
            ->setCellValue('K42', "Iuran JP TK(1%)")
            ;

        $pajakTotal = floor($pajakSebulan);
        if($pajakTotal > 0) {
            $spreadsheet->getActiveSheet()
                ->setCellValue('O37',  -$pajakTotal);
        } else {
            $spreadsheet->getActiveSheet()
                ->setCellValue('O37',  0);
        }
        
        if($rowData['jkk_jkm'] > 0) {
            $spreadsheet->getActiveSheet()
                ->setCellValue('O38', -$rowData['jkk_jkm']);
        } else {
            $spreadsheet->getActiveSheet()
                ->setCellValue('O38', 0);
        }
        
        if($rowData['emp_jht'] > 0) {
            $spreadsheet->getActiveSheet()
                ->setCellValue('O39', -$rowData['emp_jht']);
        } else {
            $spreadsheet->getActiveSheet()
                ->setCellValue('O39', 0);
        }

        if($rowData['emp_health_bpjs'] > 0) {
            $spreadsheet->getActiveSheet()
                ->setCellValue('O40', -$rowData['emp_health_bpjs']);
        } else {
            $spreadsheet->getActiveSheet()
                ->setCellValue('O40', -$rowData['emp_health_bpjs']);
        }

        if($rowData['health_bpjs'] > 0) {
            $spreadsheet->getActiveSheet()
                ->setCellValue('O41', -$rowData['health_bpjs']);
        } else {
            $spreadsheet->getActiveSheet()
                ->setCellValue('O41', 0);
        }

        if($rowData['emp_jp'] > 0) {
            $spreadsheet->getActiveSheet()
                ->setCellValue('O42', -$rowData['emp_jp']);
        } else {
            $spreadsheet->getActiveSheet()
                ->setCellValue('O42', 0);
        }
        
        $spreadsheet->getActiveSheet()
                ->setCellValue('K43', "TOTAL SEBELUM POTONGAN")
                ->setCellValue('O43', $terima)
                ->setCellValue('K45', "THR")
                ->setCellValue('K46', "Contract Bonus")
                ->setCellValue('K47', "Beban Hutang")
                ;

        /* START POTONGAN */
        if($rowData['thr'] > 0) {
            $spreadsheet->getActiveSheet()
                ->setCellValue('O45', -$rowData['thr']);
        } else {
            $spreadsheet->getActiveSheet()
                ->setCellValue('O45', 0);
        }

        if($rowData['contract_bonus'] > 0) {
            $spreadsheet->getActiveSheet()
                ->setCellValue('O46', -$rowData['contract_bonus']);
        } else {
            $spreadsheet->getActiveSheet()
                ->setCellValue('O46', 0);
        }        

        $debtText = "";
        if($rowData['debt_explanation'] != "") {
            $debtText = "(".$rowData['debt_explanation'].")";
        }

        $tBurden = $rowData['debt_burden'];
        $tBurden = $tBurden * (-1); 
        
        $spreadsheet->getActiveSheet()
                ->setCellValue('M47', $debtText)
                ->setCellValue('O47', $tBurden);
        
        // $cutCoordinate2 = $cutCoordinate+3;
        // $cutCoordinate3 = $cutCoordinate+4;
        
        $thp = $terima - $rowData['thr'] - $rowData['contract_bonus'] - $rowData['debt_burden']; 

        $spreadsheet->getActiveSheet()
                ->setCellValue('K48', 'TOTAL TERIMA')
                ->setCellValue('O48', $thp);
        /* END OUT */
       
        $spreadsheet->getActiveSheet()
                ->setCellValue('A49', 'Dibayarkan Oleh')
                ->setCellValue('A53', 'Payroll/ Accounting')
                ->setCellValue('A57', 'Diterima Oleh Karyawan')
                ->setCellValue('A61', $rowData['name'])
                ->setCellValue('K48', 'TOTAL TERIMA')
                ->setCellValue('O48', $thp);    

        unset($allBorderStyle);
        unset($center);
        unset($right);
        unset($left);
        
        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle($rowData['name']);

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        //Nama File
        $str = $rowData['name'].$rowData['bio_rec_id'];
        $fileName = preg_replace('/\s+/', '', $str);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.Xlsx"');
        // header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        /* BY COMPOSER */
        // $writer = new Xlsx($spreadsheet);
        /* OFFLINE/ BY COPY EXCEL FOLDER  */
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit(0);  
    }
    /* END REDPATH TO EXCEL */

    /* START PONTIL TO EXCEL */
    public function pontilExcel($slipId, $calendarStart, $payrollGroup, $isHealthBPJS, $isJHT, $isJP, $isJKKM)
    {
        /* CALENDAR SETUP */
        $dateCal = $calendarStart;

        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();

        $spreadsheet->getProperties()->setCreator('Maurice - Web - Android')
            ->setLastModifiedBy('Maurice - Web - Android')
            ->setTitle('Office 2007 XLSX Test Document')
            ->setSubject('Office 2007 XLSX Test Document')
            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Test result file');

        $boldFont = [
            'font' => [
                'bold' => true
                // 'color' => ['argb' => '0000FF'],
            ],
        ];

        $totalStyle = [
            'font' => [
                'bold' => true,
                'color' => ['argb' => '0000FF'],
            ],
        ];

        $allBorderStyle = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $outlineBorderStyle = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $topBorderStyle = [
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $bottomBorderStyle = [
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        // $center = [
        //     'alignment' => [
        //         'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
        //         'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
        //     ],
        // ];

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 


        /* START VALIDATE & UPDATE GOVERNMENT REGULATION */     
        $this->M_payroll_config->loadByClient('Pontil_Timika');
        $myConfigId = $this->M_payroll_config->getPayrollConfigId();
        // $this->M_payroll_config->loadById($myConfigId); 
        $healthBpjsConfig = $this->M_payroll_config->getHealthBpjs();
        $maxHealthBpjsConfig = $this->M_payroll_config->getMaxHealthBpjs();
        $jkkJkmConfig = $this->M_payroll_config->getJkkJkm();
        $jhtConfig = $this->M_payroll_config->getJht();
        $empJhtConfig = $this->M_payroll_config->getEmpJht();             
        $empHealthBpjsConfig = $this->M_payroll_config->getEmpHealthBpjs();    
        $jpConfig = $this->M_payroll_config->getJp();
        $empJpConfig = $this->M_payroll_config->getEmpJp();
        // echo $empHealthBpjsConfig; exit(0);
        $npwpCharge = $this->M_payroll_config->getNpwpCharge();
        $this->M_salary_slip->getObjectById($slipId);
        $basicSalary = $this->M_salary_slip->getBasicSalary();
        // echo $healthBpjsConfig; exit(0);
        /* START HEALTH BPJS */
        $healthBpjs = ($healthBpjsConfig/100) * $basicSalary; /* Nilai mst_payroll_config.health_bpjs(%) x Basic Salary (Max 8 Juta) */
        if($healthBpjs > $maxHealthBpjsConfig){
            $healthBpjs = $maxHealthBpjsConfig; 
        }
        /* END HEALTH BPJS */
        /* START JKK-JKM */
        $jkkJkm = ($jkkJkmConfig/100) * $basicSalary; /* Nilai mst_payroll_config.jkk_jkm(%) x Basic Salary */ 
        /* END JKK-JKM */                
        /* START JHT */
        $jht = ($jhtConfig/100) * $basicSalary;  /*Nilai mst_payroll_config.jht(%) x Basic Salary  */       
        /* END JHT */                
        /* START JHT */
        $empJht = ($empJhtConfig/100) * $basicSalary; /* Nilai mst_payroll_config.emp_jht(%) x Basic Salary */      
        /* END JHT */
        /* START JP COMPANY */
        // $jp = ($jpConfig/100) * $basicSalary; /* Nilai mst_payroll_config.jp(%) x Basic Salary */        
        /* END JP COMPANY */
        /* START JP EMPLOYEE */
        $empJp = ($empJpConfig/100) * $basicSalary; /* Nilai mst_payroll_config.emp_jp(%) x Basic Salary */        
        /* END JP EMPLOYEE */               
        
        /* START BPJS KARYAWAN */
        $maxEmpHealthBpjs = $this->M_payroll_config->getMaxEmpBpjs(); /* Nilai Max Gaji Untuk Iuran BPJS Karyawan  */ 
        $empHealthBpjs = ($empHealthBpjsConfig/100) * $basicSalary; /* Nilai mst_payroll_config.emp_health_bpjs(%) x Basic Salary */
        if($empHealthBpjs > $maxEmpHealthBpjs)
        {
            $empHealthBpjs = $maxEmpHealthBpjs; 
        }     
        /* END BPJS KARYAWAN */        

        if($isHealthBPJS == '0')
        {
            $healthBpjs = 0;
            $empHealthBpjs = 0;
        }
        if($isJHT == '0')
        {
            $jht = 0;
            $empJht = 0;
        }
        if($isJP == '0')
        {
            $empJp = 0;
        }
        if($isJKKM == '0')
        {
            $jkkJkm = 0;
        }

        $this->M_salary_slip->setHealthBpjs($healthBpjs);
        $this->M_salary_slip->setEmpHealthBpjs($empHealthBpjs);
        $this->M_salary_slip->setJht($jht);
        $this->M_salary_slip->setEmpJht($empJht);
        $this->M_salary_slip->setEmpJp($empJp);
        $this->M_salary_slip->setJkkJkm($jkkJkm);
        $this->M_salary_slip->update($slipId);
        /* END VALIDATE & UPDATE GOVERNMENT REGULATION */



        $this->db->select('ss.*, mb.bio_rec_id biodata_id, ss.basic_salary salary_basic,ss.bs_prorate, mb.npwp_no, tro.*, mr.*');
        $this->db->from('trn_salary_slip ss, mst_bio_rec mb');
        $this->db->join('mst_roster mr','mb.bio_rec_id = mr.bio_rec_id');
        $this->db->join('trn_overtime tro','mb.bio_rec_id = tro.bio_rec_id');
        $this->db->where('ss.bio_rec_id = mb.bio_rec_id AND ss.client_name = "Pontil_Timika"');
        $this->db->where('salary_slip_id', $slipId);
        $this->db->where('ss.year_period = tro.year_period AND ss.month_period = tro.month_period');        
        $this->db->where('ss.year_period = mr.year_process AND ss.month_period = mr.month_process');        
        $rowData = $query = $this->db->get()->row_array();  // Produces: SELECT * FROM trn_salary_slip (one row only) 
        $ptName = $rowData['client_name'];  
        $monthPeriod = $rowData['month_period'];
        $yearPeriod = $rowData['year_period'];   
        $biodataId = $rowData['biodata_id'];     
        $nie = $rowData['nie'];     
        $dept = $rowData['dept'];  
        $rosterFormat = $rowData['roster_format'];     
        $rosterBase = $rowData['roster_base'];  
        $prorateVal = $rowData['bs_prorate'];
        $npwpNo = $rowData['npwp_no'];

        // echo "<pre>";
            // print_r($row);
            // echo "</pre>";
            // echo $this->db->last_query();
            // exit(0);  

        // echo $this->db->last_query(); exit();

        $tMonth = (int) $monthPeriod;
        $tYear = (int) $yearPeriod;
            
        $dayCountInMonth = cal_days_in_month(CAL_GREGORIAN, $tMonth, $tYear); 

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(6);             
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(9);             
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(7);             
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(8);         
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(5);             
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(5);             
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(5);             
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(5);             
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(5);             
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(3);             
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(8); 

        // Nama Field Baris Pertama
        $spreadsheet->getActiveSheet()
            ->setCellValue('A1', 'PT. SANGATI SOERYA SEJAHTERA')
            ->setCellValue('A2', 'Slip Gaji Karyawan')
            ->setCellValue('A4', 'PT : '.$ptName);

        $tDate1 = $calendarStart.'-'.$monthPeriod.'-'.$yearPeriod;
        $tStr = $yearPeriod.'-'.$monthPeriod.'-'.($calendarStart-1);
        $tDate2 = strtotime($tStr);
        $tDate3 = date("d-m-Y", strtotime("+1 month", $tDate2));

        $spreadsheet->getActiveSheet()
            ->setCellValue('A5', 'Periode : '.$tDate1.' to '.$tDate3)
            ->setCellValue('K10', 'UPAH')
            ->setCellValue('O10', $prorateVal)
            ->setCellValue('N4', 'Id')
            ->setCellValueExplicit('O4', $nie, \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING)
            ->setCellValue('N5', 'Dept')
            ->setCellValue('O5', $dept)
            ->setCellValue('S6', 'Base   : '.$rosterBase)
            ->setCellValue('S7', 'Format : '.$rosterFormat);
       
        $spreadsheet->getActiveSheet()->getStyle("A1:D1")->getFont()->setBold(true)->setSize(16);
        $spreadsheet->getActiveSheet()->getStyle("A2:D2")->getFont()->setBold(true)->setSize(14)->setUnderline(true);
        $spreadsheet->getActiveSheet()->getStyle("A4:G4")->getFont()->setBold(true)->setSize(12); 

        // $objPHPExcel->getActiveSheet()->getStyle('N9:O66')->getNumberFormat()->setFormatCode('#,##0.00');
        $spreadsheet->getActiveSheet()->getStyle('M7:M8')->getNumberFormat()->setFormatCode('#,##0.00');
        $spreadsheet->getActiveSheet()->getStyle('N9:O66')->getNumberFormat()->setFormatCode('#,##0.00');
        $spreadsheet->getActiveSheet()->getStyle("O10:O10")->applyFromArray($totalStyle);

        $spreadsheet->getActiveSheet()
            ->mergeCells("A6:A7")
            ->mergeCells("B6:B7")
            ->mergeCells("C6:C7")
            ->mergeCells("D6:D7")
            ->mergeCells("E6:E7")
            ->mergeCells("F6:I6");

        $spreadsheet->getActiveSheet()->getStyle("A6:A7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("B6:B7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("C6:C7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("D6:D7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("E6:E7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("F6:I6")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("F7:I7")->applyFromArray($allBorderStyle);

        $spreadsheet->getActiveSheet()->getStyle("A6:A7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("B6:B7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("C6:C7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("D6:D7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("E6:E7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("F6:I6")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("F7:I7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle('K48:O48')->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle('A46:I64')->applyFromArray($center);

        $spreadsheet->getActiveSheet()->getStyle('K32:O32')->applyFromArray($totalStyle);
        $spreadsheet->getActiveSheet()->getStyle('K46:O46')->applyFromArray($totalStyle);
        $spreadsheet->getActiveSheet()->getStyle('K33:O33')->applyFromArray($totalStyle);
        $spreadsheet->getActiveSheet()->getStyle('K46:O46')->applyFromArray($totalStyle);
        $spreadsheet->getActiveSheet()->getStyle('K48:O48')->applyFromArray($totalStyle);
        $spreadsheet->getActiveSheet()->getStyle('K63:O66')->applyFromArray($totalStyle);

        $spreadsheet->getActiveSheet()->getStyle('K56:O56')->applyFromArray($topBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle('K63:O63')->applyFromArray($topBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle('K33:O33')->applyFromArray($topBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle('K46:O46')->applyFromArray($topBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle('K53:O53')->applyFromArray($topBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle('K66:O66')->applyFromArray($topBorderStyle);

        $spreadsheet->getActiveSheet()->getStyle('K48:O64')->applyFromArray($outlineBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle('A46:I64')->applyFromArray($outlineBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle('K48:O48')->applyFromArray($bottomBorderStyle);

        $spreadsheet->getActiveSheet()->getStyle("B6:B7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("C6:C7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("D6:D7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("E6:E7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("F6:I6")->getAlignment()->setWrapText(true);        
        $spreadsheet->getActiveSheet()->getStyle("F7:I7")->getAlignment()->setWrapText(true);

        $spreadsheet->getActiveSheet()->getStyle("A6:I7")->getFont()->setBold(true)->setSize(12);

        $spreadsheet->getActiveSheet()
            ->setCellValue('A6', 'DATE')
            ->setCellValue('B6', 'ROSTER DAY')
            ->setCellValue('C6', 'SHIFT DAY')
            ->setCellValue('D6', 'HOURS TOTAL')
            ->setCellValue('E6', 'NT')
            ->setCellValue('F6', 'OVER TIME')
            ->setCellValue('F7', '1')
            ->setCellValue('G7', '2')
            ->setCellValue('H7', '3')
            ->setCellValue('I7', '4');

            $rosterFormat = $rowData['roster_format'];
            $tmpRosterLength = strlen($rosterFormat);
            $strNumRoster = '';
            $tmp = '';
            $dayNo = '';
            $groupCount = 0;
            $strNum = '';
            $dayTotal = 0;
            /* START GET DAYS TOTAL BY ROSTER */
            for ($i=0; $i < $tmpRosterLength; $i++) {
                $strNum = substr($rosterFormat,$i,1);
                /* START MAKE SURE DATA IS NUMBER */ 
                if(is_numeric($strNum))
                {
                    $strNumRoster .= $strNum;
                    $dayTotal += $strNum;
                }
                /* END MAKE SURE DATA IS NUMBER */ 
            }

            $numChar = '';
            $rdData = '';
            $dataIdx = 8;
            $tIdx = 0;
            for ($m=0; $m < strlen($strNumRoster); $m++) { 
                $numChar = substr($strNumRoster,$m,1);

                for ($k=1; $k <= $numChar; $k++) { 
                    # code...
                    $dataIdx++;

                    /* START GET PUBLIC HOLIDAY BY ROSTER MASTER */
                    $tIdx++;
                    $tColIdx = '';
                    if($tIdx < 10){
                        $tColIdx = 'd0'.$tIdx;
                    }else{
                        $tColIdx = 'd'.$tIdx;
                    }
                    $tVal = substr($rowData[$tColIdx],0,2);

                    if($tVal == 'PH'){
                        $rdData = 'PH';
                    }else{
                        $rdData = $k;
                    }
                    /* END GET PUBLIC HOLIDAY BY ROSTER MASTER */


                    if($m % 2 == 0){
                        $tmp .= $k."  ";                 
                        // $rdData = $k;
                    }else{
                        $tmp .= "RO";
                        $rdData = "RO";
                    }
                    /* START ROSTER DAY TITLE */
                    // $dayCountInMonth = cal_days_in_month(CAL_GREGORIAN, $monthPeriod, $yearPeriod);
                    if($dataIdx <= ($dayCountInMonth+8))
                    {
                        $spreadsheet->getActiveSheet()->getStyle("B".$dataIdx.":B".$dataIdx)->applyFromArray($allBorderStyle);
                        $spreadsheet->getActiveSheet()->getStyle("B".$dataIdx.":B".$dataIdx)->applyFromArray($right);
                        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(1, $dataIdx, $rdData);  
                        $spreadsheet->getActiveSheet()
                            ->setCellValue('B'.$dataIdx, $rdData);                      
                    }
                    /* END ROSTER DAY TITLE */
                }

            }

            // echo $tmp; exit(0);

            /* END GET DAYS TOTAL BY ROSTER */ 
            $ot01Count = 0;
            $ot02Count = 0;
            $ot03Count = 0;
            $ot04Count = 0;

            $normalTotal = 0;
            $allTimeTotal = 0;
            /* START SUMMARY ROSTER */
            $rowIdx = 8;
            /* START CALENDAR VALUE */
            // $dayCountInMonth = cal_days_in_month(CAL_GREGORIAN, $monthPeriod, $yearPeriod);  
            $shiftCount = 0; 
            for ($i=1; $i <= $dayCountInMonth; $i++) { 
                /* START NUMBER TITLE */
                $rowIdx++;

                $ot01Column = '';
                $ot02Column = '';
                $ot03Column = '';
                $ot04Column = '';

                if($i < 10){
                    $ot01Column = 'ot1_d0'.$i;
                    $ot02Column = 'ot2_d0'.$i;
                    $ot03Column = 'ot3_d0'.$i;
                    $ot04Column = 'ot4_d0'.$i;
                    $timePerday = 'd0'.$i;
                }else{
                    $ot01Column = 'ot1_d'.$i;
                    $ot02Column = 'ot2_d'.$i;
                    $ot03Column = 'ot3_d'.$i;
                    $ot04Column = 'ot4_d'.$i;
                    $timePerday = 'd'.$i;
                }
                /* OVER TIME */
                $ot01Val = $rowData[$ot01Column];
                $ot02Val = $rowData[$ot02Column];
                $ot03Val = $rowData[$ot03Column];
                $ot04Val = $rowData[$ot04Column];
                
                $timeTotal = 0;
                $strTimePerday = $rowData[$timePerday];
                $tTimePerday = 0;

                /* START PUBLIC HOLIDAY  */
                $phCodeTmp = substr($strTimePerday,0,2);
                $phHoursTmp = substr($strTimePerday,2,strlen($strTimePerday)-2);
                if( (strtoupper($phCodeTmp) == "PH") && (is_numeric($phHoursTmp)) && ($phHoursTmp > 0) )
                {
                    $tTimePerday = $phHoursTmp;
                }
                else
                {
                    $tTimePerday = $rowData[$timePerday];
                }

                if(is_numeric($tTimePerday))
                {
                    $timeTotal = $tTimePerday;
                }

                $normalTime = $timeTotal - $ot01Val - $ot02Val - $ot03Val - $ot04Val; 
                $normalTotal += $normalTime;

                $allTimeTotal += $timeTotal; 

                $ot01Count += $ot01Val;
                $ot02Count += $ot02Val;
                $ot03Count += $ot03Val;
                $ot04Count += $ot04Val;
                             
                if($dateCal > $dayCountInMonth){
                   $dateCal = 1; 
                }

                $spreadsheet->getActiveSheet()->getStyle("A".$rowIdx.":A".$rowIdx)->applyFromArray($allBorderStyle);
                $spreadsheet->getActiveSheet()->getStyle("A".$rowIdx.":A".$rowIdx)->applyFromArray($right);
                // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(0, $rowIdx, $dateCal);
                $spreadsheet->getActiveSheet()
                    ->setCellValue('A'.$rowIdx, $dateCal);

                $dateCal++;
                /* END NUMBER TITLE */
                /* START SHIFT DAY TITLE */
                if( strtoupper($phHoursTmp) == 'PH')
                {
                    $attendStatus = 'PH';
                }
                else if(is_numeric($tTimePerday)){
                    if($tTimePerday > 0 ){
                        $attendStatus = 1;
                        $shiftCount++;
                    }else{
                        $attendStatus = 0;
                    }
                }else if($tTimePerday == ''){
                    $attendStatus = 0;        
                }else {
                    $attendStatus = $tTimePerday;        
                }
                $spreadsheet->getActiveSheet()->getStyle("C".$rowIdx.":C".$rowIdx)->applyFromArray($allBorderStyle);
                $spreadsheet->getActiveSheet()->getStyle("D".$rowIdx.":D".$rowIdx)->applyFromArray($allBorderStyle);
                $spreadsheet->getActiveSheet()->getStyle("E".$rowIdx.":E".$rowIdx)->applyFromArray($allBorderStyle);
                $spreadsheet->getActiveSheet()->getStyle("F".$rowIdx.":F".$rowIdx)->applyFromArray($allBorderStyle);
                $spreadsheet->getActiveSheet()->getStyle("G".$rowIdx.":G".$rowIdx)->applyFromArray($allBorderStyle);
                $spreadsheet->getActiveSheet()->getStyle("H".$rowIdx.":H".$rowIdx)->applyFromArray($allBorderStyle);
                $spreadsheet->getActiveSheet()->getStyle("I".$rowIdx.":I".$rowIdx)->applyFromArray($allBorderStyle);
                $spreadsheet->getActiveSheet()->getStyle("C".$rowIdx.":C".$rowIdx)->applyFromArray($center);
                $spreadsheet->getActiveSheet()->getStyle("D".$rowIdx.":D".$rowIdx)->applyFromArray($right);
                $spreadsheet->getActiveSheet()->getStyle("E".$rowIdx.":E".$rowIdx)->applyFromArray($right);
                $spreadsheet->getActiveSheet()->getStyle("F".$rowIdx.":F".$rowIdx)->applyFromArray($right);
                $spreadsheet->getActiveSheet()->getStyle("G".$rowIdx.":G".$rowIdx)->applyFromArray($right);
                $spreadsheet->getActiveSheet()->getStyle("H".$rowIdx.":H".$rowIdx)->applyFromArray($right);
                $spreadsheet->getActiveSheet()->getStyle("I".$rowIdx.":I".$rowIdx)->applyFromArray($right);

                $spreadsheet->getActiveSheet()
                    ->setCellValue('C'.$rowIdx, $attendStatus)
                    ->setCellValue('D'.$rowIdx, $timeTotal)
                    ->setCellValue('E'.$rowIdx, $normalTime)
                    ->setCellValue('F'.$rowIdx, $ot01Val)
                    ->setCellValue('G'.$rowIdx, $ot02Val)
                    ->setCellValue('H'.$rowIdx, $ot03Val)
                    ->setCellValue('I'.$rowIdx, $ot04Val);
              
            } // for ($i=1; $i <= 31 ; $i++)
            /* END SUMMARY ROSTER */        

            /* START TOTAL */
            /* START TITLE */
            // $totalTitle = $rowIdx + 2;
            $totalTitle = 40;
            $spreadsheet->getActiveSheet()->getStyle('A4')->applyFromArray($center);
            $spreadsheet->getActiveSheet()->getStyle("A".$totalTitle.":A".$totalTitle)->getFont()->setBold(true)->setSize(13);
            // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(0, $totalTitle, "TOTAL");

            $spreadsheet->getActiveSheet()
                    ->setCellValue('A'.$totalTitle, 'TOTAL')
                    ->setCellValue('C'.$totalTitle, $shiftCount)
                    ->setCellValue('D'.$totalTitle, $allTimeTotal)
                    ->setCellValue('E'.$totalTitle, $normalTotal)
                    ->setCellValue('F'.$totalTitle, $ot01Count)
                    ->setCellValue('G'.$totalTitle, $ot02Count)
                    ->setCellValue('H'.$totalTitle, $ot03Count)
                    ->setCellValue('I'.$totalTitle, $ot04Count);
            /* END TITLE */

            /* START TOTAL ROSTER DATA */
            /* Shift Day */
            $spreadsheet->getActiveSheet()->getStyle("C".$totalTitle.":C".$totalTitle)->applyFromArray($right);
            $spreadsheet->getActiveSheet()->getStyle("D".$totalTitle.":D".$totalTitle)->applyFromArray($right);
            $spreadsheet->getActiveSheet()->getStyle("E".$totalTitle.":E".$totalTitle)->applyFromArray($right);
            $spreadsheet->getActiveSheet()->getStyle("F".$totalTitle.":F".$totalTitle)->applyFromArray($right);
            $spreadsheet->getActiveSheet()->getStyle("G".$totalTitle.":G".$totalTitle)->applyFromArray($right);
            $spreadsheet->getActiveSheet()->getStyle("H".$totalTitle.":H".$totalTitle)->applyFromArray($right);
            $spreadsheet->getActiveSheet()->getStyle("I".$totalTitle.":I".$totalTitle)->applyFromArray($right);
            $spreadsheet->getActiveSheet()->getStyle("C".$totalTitle.":C".$totalTitle)->applyFromArray($allBorderStyle);
            $spreadsheet->getActiveSheet()->getStyle("D".$totalTitle.":D".$totalTitle)->applyFromArray($allBorderStyle);
            $spreadsheet->getActiveSheet()->getStyle("E".$totalTitle.":E".$totalTitle)->applyFromArray($allBorderStyle);
            $spreadsheet->getActiveSheet()->getStyle("F".$totalTitle.":F".$totalTitle)->applyFromArray($allBorderStyle);
            $spreadsheet->getActiveSheet()->getStyle("G".$totalTitle.":G".$totalTitle)->applyFromArray($allBorderStyle);
            $spreadsheet->getActiveSheet()->getStyle("H".$totalTitle.":H".$totalTitle)->applyFromArray($allBorderStyle);
            $spreadsheet->getActiveSheet()->getStyle("I".$totalTitle.":I".$totalTitle)->applyFromArray($allBorderStyle);
            $spreadsheet->getActiveSheet()->getStyle("C".$totalTitle.":C".$totalTitle)->getFont()->setBold(true)->setSize(13);
            $spreadsheet->getActiveSheet()->getStyle("D".$totalTitle.":D".$totalTitle)->getFont()->setBold(true)->setSize(13);
            $spreadsheet->getActiveSheet()->getStyle("E".$totalTitle.":E".$totalTitle)->getFont()->setBold(true)->setSize(13);
            $spreadsheet->getActiveSheet()->getStyle("F".$totalTitle.":F".$totalTitle)->getFont()->setBold(true)->setSize(13);
            $spreadsheet->getActiveSheet()->getStyle("G".$totalTitle.":G".$totalTitle)->getFont()->setBold(true)->setSize(13);
            $spreadsheet->getActiveSheet()->getStyle("H".$totalTitle.":H".$totalTitle)->getFont()->setBold(true)->setSize(13);
            $spreadsheet->getActiveSheet()->getStyle("I".$totalTitle.":I".$totalTitle)->getFont()->setBold(true)->setSize(13);
            
            $totalTitle++; 
            /* Title */
            $spreadsheet->getActiveSheet()->getStyle("A".$totalTitle.":A".$totalTitle+1)->applyFromArray($center);
            $spreadsheet->getActiveSheet()->getStyle("A".$totalTitle.":A".$totalTitle)->getFont()->setBold(true)->setSize(13);
            /* Data */
            $otSum = $ot01Count + $ot02Count + $ot03Count + $ot04Count;
            $spreadsheet->getActiveSheet()->getStyle("C41:I41")->applyFromArray($right);
            $spreadsheet->getActiveSheet()->getStyle("C41:I41")->applyFromArray($allBorderStyle);
            $spreadsheet->getActiveSheet()->mergeCells("C41:I41");
            $spreadsheet->getActiveSheet()->getStyle("C41:I41")->getFont()->setBold(true)->setSize(13);

            $spreadsheet->getActiveSheet()
                    ->setCellValue('A'.$totalTitle, 'TOTAL OT')
                    ->setCellValue('C'.$totalTitle, $otSum);

            $this->load->model('M_payroll_config');
            $basicSalary = $rowData['salary_basic'];
            $bsProrate = $rowData['bs_prorate'];
            
            $this->M_payroll_config->loadByClient($ptName);

            $salaryDividerConfig = $this->M_payroll_config->getSalaryDivider();
            $salaryHourly = $basicSalary / $salaryDividerConfig;

            $otMultiplierConfig01 = $this->M_payroll_config->getOt01Multiplier();
            $otMultiplierConfig01 = number_format($otMultiplierConfig01,1);
            $otMultiplierConfig02 = $this->M_payroll_config->getOt02Multiplier();
            $otMultiplierConfig02 = number_format($otMultiplierConfig02,1);
            $otMultiplierConfig03 = $this->M_payroll_config->getOt03Multiplier();
            $otMultiplierConfig03 = number_format($otMultiplierConfig03);
            $otMultiplierConfig04 = $this->M_payroll_config->getOt04Multiplier(); 
            $otMultiplierConfig04 = number_format($otMultiplierConfig04,1); 
            
            $spreadsheet->getActiveSheet()->getStyle("K4:L4")->applyFromArray($left);
            $spreadsheet->getActiveSheet()->getStyle("K4:O66")->getFont()->setSize(13);

            $otTotal1 = $rowData['ot_1']; 
            $otTotal2 = $rowData['ot_2']; 
            $otTotal3 = $rowData['ot_3']; 
            $otTotal4 = $rowData['ot_4']; 

            $allOt = $otTotal1+$otTotal2+$otTotal3+$otTotal4; 

            $spreadsheet->getActiveSheet()
                    ->setCellValue('K4', 'NAMA')
                    ->setCellValue('K5', 'JABATAN')
                    ->setCellValue('K6', 'STATUS')
                    ->setCellValue('K7', 'GAJI POKOK')
                    ->setCellValue('K8', 'OT RATE/HOUR')
                    ->setCellValue('K9', 'NPWP')
                    ->setCellValue('K11', 'OT 1')
                    ->setCellValue('K12', 'OT 2')
                    ->setCellValue('K13', 'OT 3')
                    ->setCellValue('K14', 'OT 4')                    
                    ->setCellValue('M4', $rowData['name'])
                    ->setCellValue('M5', $rowData['position'])
                    ->setCellValue('M6', $rowData['marital_status'])
                    ->setCellValue('M7', $basicSalary)
                    ->setCellValue('M8', $salaryHourly)
                    ->setCellValue('M9', $rowData['npwp_no'])                    
                    ->setCellValue('M11', $rowData['ot_count1']." x ".$otMultiplierConfig01." x ".$salaryHourly." = Rp.")
                    ->setCellValue('N11', $otTotal1)
                    ->setCellValue('M12', $rowData['ot_count2']." x ".$otMultiplierConfig02." x ".$salaryHourly." = Rp.")
                    ->setCellValue('N12', $otTotal2)
                    ->setCellValue('M13', $rowData['ot_count3']." x ".$otMultiplierConfig03." x ".$salaryHourly." = Rp.")
                    ->setCellValue('N13', $otTotal3)
                    ->setCellValue('M14', $rowData['ot_count4']." x ".$otMultiplierConfig04." x ".$salaryHourly." = Rp.")
                    ->setCellValue('N14', $otTotal4);

            $spreadsheet->getActiveSheet()->getStyle('M7:M8')->applyFromArray($totalStyle);

            $overtimeCoordinate = 11;

            /* END OVER TIME DATA */

            /* START OVER TIME TOTAL */
            $spreadsheet->getActiveSheet()->getStyle('K15:O15')->applyFromArray($topBorderStyle);
            $spreadsheet->getActiveSheet()->getStyle('K15:O15')->applyFromArray($totalStyle);
            /* END OVER TIME TOTAL */

            $spreadsheet->getActiveSheet()
                    ->setCellValue('K15', 'OT TOTAL')
                    ->setCellValue('O15', $allOt);

            $this->M_salary->loadByBiodataIdNie($biodataId, $nie);
            $isTravel = $this->M_salary->getIsTravel();
            $isAllowanceEconomy = $this->M_salary->getIsAllowanceEconomy();
            $isIncentiveBonus = $this->M_salary->getIsIncentiveBonus();
            $isShiftBonus = $this->M_salary->getIsShiftBonus();
            $isCCPayment = $this->M_salary->getIsCcPayment();
            $isProductionBonus = $this->M_salary->getIsProduction();

            /* START BONUS TOTAL */
            $bonusTotal = 0;
            
            $spreadsheet->getActiveSheet()
                    ->setCellValue('K17', 'TRV')
                    ->setCellValue('K18', 'Allowance Economy')
                    ->setCellValue('K19', 'Incentive Bonus')
                    ->setCellValue('K20', 'Shift Bonus')
                    ->setCellValue('K21', 'THR')
                    ->setCellValue('K22', 'CC Payment')
                    ->setCellValue('K23', 'Production Bonus')
                    ->setCellValue('K24', 'Workday Adjustment');            
            
            if($isTravel == True) {
                $bonusTotal += $rowData['travel_bonus'];
                $spreadsheet->getActiveSheet()
                    ->setCellValue('N17', $rowData['travel_bonus']);
            } else {
                $spreadsheet->getActiveSheet()
                    ->setCellValue('N17', 0);
            }

            if($isAllowanceEconomy == True) {
                $bonusTotal += $rowData['allowance_economy'];
                // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $bonusCoordinate+1, $rowData['allowance_economy']);
                $spreadsheet->getActiveSheet()
                    ->setCellValue('N18', $rowData['allowance_economy']);
            } else {
                // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $bonusCoordinate+1, 0);
                $spreadsheet->getActiveSheet()
                    ->setCellValue('N18', 0);
            }

            if($isIncentiveBonus == True) {
                $bonusTotal += $rowData['incentive_bonus'];
                // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $bonusCoordinate+2, $rowData['incentive_bonus']);
                $spreadsheet->getActiveSheet()
                    ->setCellValue('N19', $rowData['incentive_bonus']);
            } else {
                // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $bonusCoordinate+2, 0);
                $spreadsheet->getActiveSheet()
                    ->setCellValue('N19', 0);
            }

            if($isShiftBonus == True) {
                $bonusTotal += $rowData['shift_bonus'];
                // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $bonusCoordinate+3, $rowData['shift_bonus']);
                $spreadsheet->getActiveSheet()
                    ->setCellValue('N20', $rowData['shift_bonus']);
            } else {
                // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $bonusCoordinate+3, 0);
                $spreadsheet->getActiveSheet()
                    ->setCellValue('N20', 0);
            }

            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $bonusCoordinate+4, $rowData['thr']);
            $bonusTotal += $rowData['cc_payment'];
            $spreadsheet->getActiveSheet()
                    ->setCellValue('N21', $rowData['thr'])
                    ->setCellValue('N22', $rowData['cc_payment']);

            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $bonusCoordinate+5, $rowData['cc_payment']);
            /*if($isCCPayment == True) {
                $bonusTotal += $rowData['cc_payment'];
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $bonusCoordinate+5, $rowData['cc_payment']);
            } else {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $bonusCoordinate+5, 0);
            }*/

            if($isProductionBonus == True) {
                $bonusTotal += $rowData['production_bonus'];
                // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $bonusCoordinate+6, $rowData['production_bonus']);
                $spreadsheet->getActiveSheet()
                    ->setCellValue('N23', $rowData['production_bonus']);
            } else {
                // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $bonusCoordinate+6, 0);
                $spreadsheet->getActiveSheet()
                    ->setCellValue('N23', 0);
            }
            
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $bonusCoordinate+7, $rowData['workday_adj']);
            $spreadsheet->getActiveSheet()
                    ->setCellValue('N24', $rowData['workday_adj']);
            /* END BONUS, INCENTIVE */

            $bonusTotal += $rowData['thr'];            
            $bonusTotal += $rowData['workday_adj'];               

            // $bonusCoordinate2 = $bonusCoordinate+8;
            $spreadsheet->getActiveSheet()->getStyle('K25:O25')->applyFromArray($topBorderStyle);
            // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(10, $bonusCoordinate2, "BONUS TOTAL");
            
            $spreadsheet->getActiveSheet()
                    ->setCellValue('K25', 'BONUS TOTAL')
                    ->setCellValue('O25', $bonusTotal);

            $spreadsheet->getActiveSheet()->getStyle('K25:O25')->applyFromArray($totalStyle);
            // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(14, $bonusCoordinate2, $bonusTotal);
            /* END BONUS TOTAL */

            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $bruttoCoordinate+4, "Potongan Absensi ".$rowData['unpaid_count']." Hari");
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $bruttoCoordinate+4, $unpaidTotal);
            /* START GOVERNMENT REGULATION */
            $unpaidTotal = $rowData['unpaid_total'];
            $subTotalVal = $rowData['jkk_jkm']+$rowData['health_bpjs']+$rowData['jp']-$unpaidTotal+$rowData['adjust_in']-$rowData['adjust_out'];
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $bruttoCoordinate2, $brutto);
            $brutto = $bsProrate + $allOt + $bonusTotal + $rowData['jkk_jkm'] + $rowData['health_bpjs'] + $rowData['jp'] - $unpaidTotal + $rowData['adjust_in'] - $rowData['adjust_out']; 
            $spreadsheet->getActiveSheet()
                    ->setCellValue('K27', 'JKK & JKM(2.04%)')
                    ->setCellValue('N27', $rowData['jkk_jkm'])
                    ->setCellValue('K28', 'Iuran BPJS Kesehatan(4%)')
                    ->setCellValue('N28', $rowData['health_bpjs'])
                    ->setCellValue('K29', 'Potongan Absensi '.$rowData['unpaid_count'].' Hari')
                    ->setCellValue('N29', $unpaidTotal)
                    ->setCellValue('K30', 'Adjustment In')
                    ->setCellValue('N30', $rowData['adjust_in'])
                    ->setCellValue('K31', 'Adjustment Out')
                    ->setCellValue('N31', $rowData['adjust_out'])
                    ->setCellValue('K32', 'Subtotal')
                    ->setCellValue('O32', $subTotalVal)
                    ->setCellValue('K33', 'TOTAL KOTOR')
                    ->setCellValue('O33', $brutto)

                    ;
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $bruttoCoordinate2, "TOTAL KOTOR");
            
            /* TAX CONFIG */
            // $this->load->model('M_payroll_config');
            $myConfigId = 2;    
            /*switch ($ptName) {
                case 'Redpath_Timika':
                    # code...
                    $myConfigId = 1;
                    break;
                case 'Pontil_Timika':
                    # code...
                    $myConfigId = 2;
                    break;          
                default:
                    # code...
                    break;
            }*/
            $row = $this->M_payroll_config->getObjectById($myConfigId);       
            $maxNonTax = $this->M_payroll_config->getNonTaxAllowance(); /* Max Tunjangan Jabatan */

            $nonTaxAllowance = $brutto * (5/100);
            if($nonTaxAllowance > $maxNonTax){
               $nonTaxAllowance = $maxNonTax; 
            }
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $taxCoordinate+4, $nonTaxAllowance);
            /* END TAX */

            /* START NETTO */
            $nettoCoordinate = 53;
            $netto = $brutto - $rowData['emp_jht'] - $rowData['emp_jp'] - $nonTaxAllowance;
            // $objPHPExcel->getActiveSheet()->getStyle('K'.$nettoCoordinate.':O'.$nettoCoordinate)->applyFromArray($totalStyle);
            // $objPHPExcel->getActiveSheet()->getStyle('K'.$nettoCoordinate.':O'.$nettoCoordinate)->applyFromArray($topBorderStyle);
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $nettoCoordinate, "Netto");
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $nettoCoordinate, $netto);
            /* END NETTO */


            /* START TAX PROCESS */
            $ptkpTotal = $rowData['ptkp_total'];
            /* Perhitungan pajak yang disetahunkan, hanya penghasilan tetap saja. Tunjangan tidak tetap seperti THR dihitung terpisah */
            $nettoSetahun = ( ($netto - $rowData['thr']-$rowData['cc_payment']) * 12 );
            $penghasilanPajak = 0;
            if($nettoSetahun >= $ptkpTotal)
            {
                $penghasilanPajak = $nettoSetahun - $ptkpTotal;
            }

            $yearNettoCoordinate = 54;
            $pembulatanPenghasilan = floor($penghasilanPajak);
            

            $spreadsheet->getActiveSheet()
                ->setCellValue('K48', 'PERHITUNGAN PAJAK PENGHASILAN')
                ->setCellValue('K49', 'Penghasilan Kotor')
                ->setCellValue('O49', $brutto)
                ->setCellValue('K50', 'Iuran JP TK(1%)')
                ->setCellValue('O50', $rowData['emp_jp'])
                ->setCellValue('K51', 'Iuran JHT')
                ->setCellValue('O51', $rowData['emp_jht'])
                ->setCellValue('K52', 'Tunjangan Jabatan')
                ->setCellValue('O52', $nonTaxAllowance)
                ->setCellValue('K53', 'Netto')
                ->setCellValue('O53', $netto)
                ->setCellValue('K54', 'Netto Disetahunkan(Tetap)')
                ->setCellValue('O54', $nettoSetahun)
                ->setCellValue('K55', 'PTKP '.$rowData['marital_status'])
                ->setCellValue('O55', $ptkpTotal)
                ->setCellValue('K56', 'Penghasilan Kena Pajak')
                ->setCellValue('O56', $penghasilanPajak)
                ->setCellValue('K57', 'Pembulatan')
                ->setCellValue('O57', floor($penghasilanPajak))
                ;
            /* TAX CONFIG */
            $taxPercent1 = $this->M_payroll_config->getTaxPercent1(); /* Persentase Progresif Pajak I */
            $taxPercent2 = $this->M_payroll_config->getTaxPercent2(); /* Persentase Progresif Pajak II */
            $taxPercent3 = $this->M_payroll_config->getTaxPercent3(); /* Persentase Progresif Pajak III */
            $taxPercent4 = $this->M_payroll_config->getTaxPercent4(); /* Persentase Progresif Pajak IV */
            $maxTaxVal1 = $this->M_payroll_config->getTaxMaxVal1(); /* Nominal Pajak I */ 
            $maxTaxVal2 = $this->M_payroll_config->getTaxMaxVal2(); /* Nominal Pajak II */
            $maxTaxVal3 = $this->M_payroll_config->getTaxMaxVal3(); /* Nominal Pajak III */
            $maxTaxVal4 = $this->M_payroll_config->getTaxMaxVal4(); /* Nominal Pajak IV */

            /* START WITHOUT UNFIXED  */
            $taxVal1 = 0;    
            $taxVal2 = 0;    
            $taxVal3 = 0;    
            $taxVal4 = 0;

            $tVal = 0;
            $tSisa = 0;
            if($pembulatanPenghasilan > 0)
            {
                /* TAX 1 */
                if($maxTaxVal1 > 0)
                {
                    $tVal = $pembulatanPenghasilan/$maxTaxVal1;  
                    if($tVal >= 1){
                        $taxVal1 = $maxTaxVal1 * ($taxPercent1/100);
                    }
                    else{
                        $taxVal1 = $pembulatanPenghasilan * ($taxPercent1/100);
                    }    
                }    
                
                /* TAX 2 */
                if($maxTaxVal2 > 0)
                {
                    if($pembulatanPenghasilan > $maxTaxVal1)
                    {
                        $tSisa = $pembulatanPenghasilan - $maxTaxVal1; 
                        $tVal = $tSisa/$maxTaxVal2;
                        if($tVal >= 1){
                            $taxVal2 = $maxTaxVal2 * ($taxPercent2/100);
                        }
                        else{
                            $taxVal2 = $tSisa * ($taxPercent2/100);
                        }
                    }     
                }
                 
                /* TAX 3 */
                if($maxTaxVal3 > 0)
                {
                    if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2))
                    {
                        $tSisa = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2;
                        $tVal = $tSisa/$maxTaxVal3;
                        if($tVal >= 1){
                            $taxVal3 = $maxTaxVal3 * ($taxPercent3/100);
                        }
                        else{
                            $taxVal3 = $tSisa * ($taxPercent3/100);
                        }
                    }    
                }
                 
                /* TAX 4 */
                if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2 + $maxTaxVal3))
                {
                    $tSisa = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2 - $maxTaxVal3;
                    $taxVal4 = $tSisa * ($taxPercent4/100); 
                }               
            }

            $taxFeeCoordinate = 59;
           
            $pajakTerutang = $taxVal1 + $taxVal2 + $taxVal3 + $taxVal4;
            $pajakSebulan = $pajakTerutang/12;
            /* END WITHOUT UNFIXED  */



            $spreadsheet->getActiveSheet()
                ->setCellValue('K59', 'Tarif Pajak I    '.$taxPercent1.'%')
                ->setCellValue('O59', $taxVal1)
                ->setCellValue('K60', 'Tarif Pajak II    '.$taxPercent2.'%')
                ->setCellValue('O60', $taxVal2)
                ->setCellValue('K61', 'Tarif Pajak III    '.$taxPercent3.'%')
                ->setCellValue('O61', $taxVal3)
                ->setCellValue('K62', 'Tarif Pajak IV    '.$taxPercent4.'%')
                ->setCellValue('O62', $taxVal4)
                ->setCellValue('K63', 'Pajak Gaji Setahun')
                ->setCellValue('O63', $pajakTerutang)
                ->setCellValue('K64', 'Pajak Gaji Sebulan')
                // ->setCellValue('O64', floor($pajakSebulan))
                ;

            /* END DATA PAYROLL */



             /* START WITH UNFIXED  */
            $nettoSetahun = ( ($netto - $rowData['thr']-$rowData['cc_payment']) * 12 ) + $rowData['thr'] + $rowData['cc_payment'];
            $penghasilanPajak = 0;
            if($nettoSetahun >= $ptkpTotal)
            {
                $penghasilanPajak = $nettoSetahun - $ptkpTotal;
            }

            $pembulatanPenghasilan = floor($penghasilanPajak);
            $taxVal1 = 0;    
            $taxVal2 = 0;    
            $taxVal3 = 0;    
            $taxVal4 = 0;

            $tVal = 0;
            $tSisa = 0;
            if($pembulatanPenghasilan > 0)
            {
                /* TAX 1 */
                if($maxTaxVal1 > 0)
                {
                    $tVal = $pembulatanPenghasilan/$maxTaxVal1;  
                    if($tVal >= 1){
                        $taxVal1 = $maxTaxVal1 * ($taxPercent1/100);
                    }
                    else{
                        $taxVal1 = $pembulatanPenghasilan * ($taxPercent1/100);
                    }    
                }    
                
                /* TAX 2 */
                if($maxTaxVal2 > 0)
                {
                    if($pembulatanPenghasilan > $maxTaxVal1)
                    {
                        $tSisa = $pembulatanPenghasilan - $maxTaxVal1; 
                        $tVal = $tSisa/$maxTaxVal2;
                        if($tVal >= 1){
                            $taxVal2 = $maxTaxVal2 * ($taxPercent2/100);
                        }
                        else{
                            $taxVal2 = $tSisa * ($taxPercent2/100);
                        }
                    }     
                }
                 
                /* TAX 3 */
                if($maxTaxVal3 > 0)
                {
                    if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2))
                    {
                        $tSisa = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2;
                        $tVal = $tSisa/$maxTaxVal3;
                        if($tVal >= 1){
                            $taxVal3 = $maxTaxVal3 * ($taxPercent3/100);
                        }
                        else{
                            $taxVal3 = $tSisa * ($taxPercent3/100);
                        }
                    }    
                }
                 
                /* TAX 4 */
                if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2 + $maxTaxVal3))
                {
                    $tSisa = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2 - $maxTaxVal3;
                    $taxVal4 = $tSisa * ($taxPercent4/100); 
                }               
            }

            $taxFeeCoordinate = 59;
           
            $pajakTerutang2 = ($taxVal1 + $taxVal2 + $taxVal3 + $taxVal4) - $pajakTerutang;
            // $pajakSebulan2 = $pajakTerutang2/12;


            $spreadsheet->getActiveSheet()
                ->setCellValue('K66', 'Pajak Non Reguler');
                // ->setCellValue('O66', floor($pajakTerutang2));

            /* END WITH UNFIXED  */


            /* Additional Charge if there is no NPWP */
            $pajakDenda = 0;
            $pajakFixUnDenda = 0;
            if( strlen($npwpNo) < 19 ){
                $pajakSebulan = $pajakSebulan + ($pajakSebulan * ($npwpCharge/100) ); 
                $pajakTerutang2 = $pajakTerutang2 + ($pajakTerutang2 * ($npwpCharge/100) ); 
                $spreadsheet->getActiveSheet()
                    // ->setCellValue('O64', floor($pajakSebulan))
                    // ->setCellValue('O66', floor($pajakTerutang2))
                    ->setCellValue('K70', '* Tidak Memiliki NPWP dikenakan pajak plus 20% ');
                $spreadsheet->getActiveSheet()->getStyle("K70")->getFont()
                ->setSize(14)
                ->getColor()
                ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);
            }

            $spreadsheet->getActiveSheet()
                    ->setCellValue('O64', floor($pajakSebulan))
                    ->setCellValue('O66', floor($pajakTerutang2));

            $pajakSebulan+= $pajakTerutang2;


            /* START NON MONTHLY TAX -- EDIT @ Aug 15, 2017 BY NEW REGULATION OF FINANCE */
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, 63, "Pajak THR - Bonus");
            // $objPHPExcel->getActiveSheet()->getStyle('K63:O63')->applyFromArray($topBorderStyle);
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, 63, $nonMonthlyBonusTax);
            // $pajakSebulan += $nonMonthlyBonusTax; 
            /* END NON MONTHLY TAX -- EDIT @ Aug 15, 2017 BY NEW REGULATION OF FINANCE */

            /* START UPDATE TAX VALUE TO THE TABLE */
            $str = "UPDATE trn_salary_slip SET tax_value = ".($pajakSebulan).", non_tax_allowance =".$nonTaxAllowance." WHERE salary_slip_id = '".$slipId."' ";
            $this->db->query($str);
            /* END UPDATE TAX VALUE TO THE TABLE */

            /* START OUT */
            $outCoordinate = 35;
            $terima = $brutto-floor($pajakSebulan)-$rowData['jkk_jkm']-$rowData['emp_jht']-$rowData['emp_health_bpjs']-$rowData['emp_jp']-$rowData['health_bpjs']-$rowData['jp'];

            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $outCoordinate, "Pajak Penghasilan");
            
            $spreadsheet->getActiveSheet()
                ->setCellValue('K35', 'Pajak Penghasilan')
                ->setCellValue('K36', 'JKK & JKM(2.04%)')
                ->setCellValue('K37', 'Iuran JHT BPJS TK(2%)')
                ->setCellValue('K38', 'Iuran BPJS Kesehatan TK(1%)')
                ->setCellValue('K39', 'Iuran BPJS Kesehatan(4%)')
                ->setCellValue('K40', 'Iuran JP TK(1%)')
                ->setCellValue('K41', 'TOTAL SEBELUM POTONGAN')
                ->setCellValue('K43', 'THR')
                ->setCellValue('K44', 'CC Payment')
                ->setCellValue('K45', 'Beban Hutang')
                ;

            $spreadsheet->getActiveSheet()->getStyle('K41:O41')->applyFromArray($topBorderStyle);
            $spreadsheet->getActiveSheet()->getStyle('K41:O41')->applyFromArray($totalStyle);
                // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $cutCoordinate+6, "Beban Hutang");
                // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $cutCoordinate+5, "CC Payment");
                // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $cutCoordinate+4, "THR");
                // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $outCoordinate+5, "Iuran JP TK(1%)");
                // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $outCoordinate+4, "Iuran BPJS Kesehatan(4%)");
                // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $outCoordinate+3, "Iuran BPJS Kesehatan TK(1%)");
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $outCoordinate+1, "JKK & JKM(2.04%)");
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $outCoordinate+2, "Iuran JHT BPJS TK(2%)");

            $pajakSebulan = floor($pajakSebulan);
            if($pajakSebulan > 0) {
                // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $outCoordinate, -$pajakSebulan );
                $spreadsheet->getActiveSheet()
                    ->setCellValue('O35', -$pajakSebulan);
            } else {
                // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $outCoordinate, 0 );
                $spreadsheet->getActiveSheet()
                    ->setCellValue('O35', 0);
            }

            if($rowData['jkk_jkm'] > 0) {
                $spreadsheet->getActiveSheet()
                    ->setCellValue('O36', -$rowData['jkk_jkm']);
            } else {
                $spreadsheet->getActiveSheet()
                    ->setCellValue('O36', 0);
            }
            
            if($rowData['emp_jht'] > 0) {
                $spreadsheet->getActiveSheet()
                    ->setCellValue('O37', -$rowData['emp_jht']);
            } else {
                $spreadsheet->getActiveSheet()
                    ->setCellValue('O37', 0);
            }
            
            
            if($rowData['emp_health_bpjs'] > 0) {
                $spreadsheet->getActiveSheet()
                    ->setCellValue('O38', -$rowData['emp_health_bpjs']);
            } else {
                $spreadsheet->getActiveSheet()
                    ->setCellValue('O38', 0);
            }

            
            if($rowData['health_bpjs'] > 0) {
                $spreadsheet->getActiveSheet()
                    ->setCellValue('O39', -$rowData['health_bpjs']);
            } else {
                $spreadsheet->getActiveSheet()
                    ->setCellValue('O39', 0);
            }

            
            if($rowData['emp_jp'] > 0) {
                $spreadsheet->getActiveSheet()
                    ->setCellValue('O40', -$rowData['emp_jp']);
            } else {
                $spreadsheet->getActiveSheet()
                    ->setCellValue('O40', 0);
            }

     

            // $subTotalCoordinate = $outCoordinate + 6;   
            $subTotalVal = floor($pajakSebulan)+$rowData['jkk_jkm']+$rowData['emp_jht']+$rowData['emp_health_bpjs']+$rowData['emp_jp']+$rowData['health_bpjs']+$rowData['jp'];
            // $objPHPExcel->getActiveSheet()->getStyle('K'.$subTotalCoordinate.':O'.$subTotalCoordinate)->applyFromArray($totalStyle);
            // $objPHPExcel->getActiveSheet()->getStyle('K'.$subTotalCoordinate.':O'.$subTotalCoordinate)->applyFromArray($topBorderStyle);

            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $outCoordinate + 5, "Subtotal");
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $outCoordinate + 5, $subTotalVal);
            
            // $outCoordinate2 = $outCoordinate + 6;
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $outCoordinate2, "TOTAL SEBELUM POTONGAN");
            // $objPHPExcel->getActiveSheet()->getStyle('K'.$outCoordinate2.':O'.$outCoordinate2)->applyFromArray($totalStyle);
            // $objPHPExcel->getActiveSheet()->getStyle('K'.$outCoordinate2.':O'.$outCoordinate2)->applyFromArray($topBorderStyle);
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $outCoordinate2, $terima);
            $spreadsheet->getActiveSheet()
                    ->setCellValue('O41', $terima);

            /* START POTONGAN */
            // $cutCoordinate = 39;
            // $objPHPExcel->getActiveSheet()->getStyle('K'.$cutCoordinate.':O'.$cutCoordinate)->applyFromArray($topBorderStyle);
            // $objPHPExcel->getActiveSheet()->getStyle('K'.$cutCoordinate.':O'.$cutCoordinate)->applyFromArray($bottomBorderStyle);
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $cutCoordinate, "(POTONGAN)");

            
            if($rowData['thr'] > 0) {
                // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $cutCoordinate+4, -$rowData['thr']);
                $spreadsheet->getActiveSheet()
                    ->setCellValue('O43', -$rowData['thr']);
            } else {
                // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $cutCoordinate+4, 0);
                $spreadsheet->getActiveSheet()
                    ->setCellValue('O43', 0);
            }
            
            if($rowData['cc_payment'] > 0) {
                // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $cutCoordinate+5, -$rowData['cc_payment']);
                $spreadsheet->getActiveSheet()
                    ->setCellValue('O44', -$rowData['cc_payment']);
            } else {
                // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $cutCoordinate+5, 0);
                $spreadsheet->getActiveSheet()
                    ->setCellValue('O44', 0);
            }

            
            $debtText = "";
            if(trim($rowData['debt_explanation']) != "") {
              $debtText = "(".$rowData['debt_explanation'].")";         
            }


            $tBurden = $rowData['debt_burden'];
            $tBurden = $tBurden * (-1);
            $thp = $terima - $rowData['thr'] - $rowData['cc_payment'] - $rowData['debt_burden']; 
            $spreadsheet->getActiveSheet()
                    ->setCellValue('M45', $debtText)
                    ->setCellValue('O45', $tBurden)
                    ->setCellValue('K46', 'TOTAL TERIMA')
                    ->setCellValue('O46', $thp)
                    ->setCellValue('A48', 'Dibayarkan Oleh')
                    ->setCellValue('A52', 'Payroll/ Accounting')
                    ->setCellValue('A56', 'Diterima Oleh Karyawan')
                    ->setCellValue('A60', $rowData['name'])
                    ;

            

            $spreadsheet->getActiveSheet()
                ->mergeCells('A48:I48')
                ->mergeCells('A52:I52')
                ->mergeCells('A56:I56')
                ->mergeCells('A60:I60')
                ->mergeCells('K48:O48');

            unset($allBorderStyle);
            unset($center);
            unset($right);
            unset($left);
            
            // Rename worksheet
            $spreadsheet->getActiveSheet()->setTitle($rowData['name']);

            // Set active sheet index to the first sheet, so Excel opens this as the first sheet
            $spreadsheet->setActiveSheetIndex(0);

            //Nama File
            $str = $rowData['name'].$rowData['bio_rec_id'];
            $fileName = preg_replace('/\s+/', '', $str);

            // Redirect output to a client’s web browser (Xlsx)
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="'.$fileName.'.Xlsx"');
            // header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0
            /* BY COMPOSER */
            // $writer = new Xlsx($spreadsheet);
            /* OFFLINE/ BY COPY EXCEL FOLDER  */
            $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
            $writer->save('php://output');
            exit(0);  
    }
    /* END PONTIL TO EXCEL */

    public function updateSlipByDept()
    {
        $clientName = "";
        $dept = "";
        $yearPeriod = "";
        $monthPeriod = "";
        $devPercent = 0;

        if(isset($_POST['clientName']))
        {
            $clientName = $this->security->xss_clean($_POST['clientName']);            
        }

        if(isset($_POST['dept']))
        {
            $dept = $this->security->xss_clean($_POST['dept']);            
        }

        if(isset($_POST['yearPeriod']))
        {
            $yearPeriod = $this->security->xss_clean($_POST['yearPeriod']);            
        }

        if(isset($_POST['monthPeriod']))
        {
            $monthPeriod = $this->security->xss_clean($_POST['monthPeriod']);            
        }

        if(isset($_POST['devPercent']))
        {
            $devPercent = $this->security->xss_clean($_POST['devPercent']);            
        }
             
        $this->M_salary_slip->updateSlipByDept($clientName, $dept, $yearPeriod, $monthPeriod, $devPercent);
    }

    /* START PAYMENT EXPORT */
    public function exportPaymentList($ptName, $yearPeriod, $monthPeriod, $dept='All')
    {
        $dept = $this->security->xss_clean($dept);
            // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();  

        $spreadsheet->getProperties()->setCreator('Maurice - Web - Android')
            ->setLastModifiedBy('Maurice - Web - Android')
            ->setTitle('Office 2007 XLSX Test Document')
            ->setSubject('Office 2007 XLSX Test Document')
            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Test result file');

        if (file_exists('assets/images/report_logo.jpg')) {
            $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing->setName('Logo');
            $drawing->setDescription('Logo');
            $drawing->setPath('./assets/images/report_logo.jpg');
            $drawing->setCoordinates('A1');
            $drawing->setHeight(36);
            $drawing->setWorksheet($spreadsheet->getActiveSheet());
        }

        $strFilter = "";
        if($dept != 'All' && $ptName == 'Redpath_Timika')
        {
            $strFilter = " AND ss.dept = '".$dept."' ";    
        }
        else if($dept != 'All' && $ptName == 'Pontil_Timika')
        {
            $strFilter = " AND ss.payroll_group = '".$dept."' ";    
        }

        if($ptName == "Redpath_Timika")
        {
            $strSQL  = "SELECT ss.other_allowance1 AS jumboPercent,(ss.dev_incentive_bonus*(ss.dev_percent/100))*(ss.other_allowance1/100) AS jumboVal,ss.salary_slip_id,mb.bio_rec_id,mb.full_name,ms.nie,mb.position,ms.bank_code,ms.bank_name,ms.account_name,ms.account_no,ss.ptkp_total,ss.thr,ss.contract_bonus,ss.emp_jht,ss.emp_jp,ss.cc_payment,ss.debt_burden,ms.payroll_group,ss.tax_value,mb.npwp_no,";
            $strSQL .= " (";
            $strSQL .= " CAST("; /*START CAST*/
            $strSQL .= " CASE WHEN(ss.basic_salary = ss.bs_prorate) THEN ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 ";
            $strSQL .= " ELSE ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 END + ";
            $strSQL .= " CASE WHEN(ms.is_ot_bonus = 1) THEN ss.ot_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_remote_allowance = 1) THEN ss.remote_allowance ELSE 0 END +";
            // $strSQL .= " ss.thr+";
            $strSQL .= " CASE WHEN(ms.is_dev_incentive_bonus = 1) THEN ss.dev_incentive_bonus*(ss.dev_percent/100) ELSE 0 END +";
            
            $strSQL .= " (ss.dev_incentive_bonus*(ss.dev_percent/100))*(ss.other_allowance1/100) +";

            // $strSQL .= " ss.contract_bonus+";
            $strSQL .= " ss.workday_adj+";
            $strSQL .= " ss.no_accident_bonus+";
            $strSQL .= " ss.jkk_jkm+";
            $strSQL .= " ss.health_bpjs-";
            $strSQL .= " ss.unpaid_total+";
            $strSQL .= " ss.adjust_in-";
            $strSQL .= " ss.adjust_out-";  
            // $strSQL .= " ss.tax_value-";
            $strSQL .= " ss.jkk_jkm-";
            $strSQL .= " ss.emp_jht-";
            $strSQL .= " ss.emp_health_bpjs-";
            $strSQL .= " ss.health_bpjs-";
            $strSQL .= " ss.emp_jp";
            $strSQL .= " AS DECIMAL(11,2))  "; /*END CAST*/
            $strSQL .= " ) AS total, ";


            /* START BRUTTO */
            $strSQL .= " (";
            $strSQL .= " CAST("; /*START CAST*/
            $strSQL .= " CASE WHEN(ss.basic_salary = ss.bs_prorate) THEN ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 ";
            $strSQL .= " ELSE ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 END + ";
            $strSQL .= " CASE WHEN(ms.is_ot_bonus = 1) THEN ss.ot_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_remote_allowance = 1) THEN ss.remote_allowance ELSE 0 END +";
            $strSQL .= " ss.thr+";
            $strSQL .= " CASE WHEN(ms.is_dev_incentive_bonus = 1) THEN ss.dev_incentive_bonus*(ss.dev_percent/100) ELSE 0 END +";

            $strSQL .= " (ss.dev_incentive_bonus*(ss.dev_percent/100))*(ss.other_allowance1/100) +";

            $strSQL .= " ss.contract_bonus+";
            $strSQL .= " ss.workday_adj+";
            $strSQL .= " ss.no_accident_bonus+";
            $strSQL .= " ss.jkk_jkm+";
            $strSQL .= " ss.health_bpjs-";
            $strSQL .= " ss.unpaid_total+";
            $strSQL .= " ss.adjust_in-";
            $strSQL .= " ss.adjust_out "; 
           
            $strSQL .= " AS DECIMAL(11,2))  "; /*END CAST*/
            $strSQL .= " ) AS brutto_tax ";
            /* END BRUTTO */

            $strSQL .= "FROM mst_salary ms,mst_bio_rec mb,trn_salary_slip ss ";
            $strSQL .= "WHERE ms.company_name = '".$ptName."' ";
            $strSQL .= "AND ss.month_period = '".$monthPeriod."' ";
            $strSQL .= "AND ss.year_period = '".$yearPeriod."' ";
            // $strSQL .= "AND ms.payroll_group = '".$payrollGroup."' ";
            $strSQL .= "AND ms.bio_rec_id = mb.bio_rec_id  ";
            $strSQL .= "AND mb.bio_rec_id = ss.bio_rec_id ";
            $strSQL .= "AND mb.is_active = 1 ";      
            $strSQL .= $strFilter;      
            $strSQL .= "ORDER BY mb.full_name ";      
        }
        else if($ptName == "Pontil_Timika")
        {           

            $strSQL  = "SELECT 0 jumboPercent,0 jumboVal,ss.salary_slip_id,mb.bio_rec_id,mb.full_name,ms.nie,mb.position,ms.bank_code,ms.bank_name,ms.account_name,ms.account_no,ss.ptkp_total,ss.thr,ss.contract_bonus,ss.emp_jht,ss.emp_jp,ss.cc_payment,ss.debt_burden,ms.payroll_group,ss.tax_value,mb.npwp_no,";
            $strSQL .= " (";
            $strSQL .= " CAST("; /*START CAST*/
            $strSQL .= " ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 + ";
            $strSQL .= " CASE WHEN(ms.is_travel = 1) THEN ss.travel_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_allowance_economy = 1) THEN ss.allowance_economy ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_incentive_bonus = 1) THEN ss.incentive_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_production = 1) THEN ss.production_bonus ELSE 0 END +";
           
            $strSQL .= " ss.workday_adj+";
            // $strSQL .= " ss.thr+";
            $strSQL .= " ss.jkk_jkm+";
            $strSQL .= " ss.health_bpjs-";
            $strSQL .= " ss.unpaid_total+";
            $strSQL .= " ss.adjust_in-";
            $strSQL .= " ss.adjust_out-";  
            // $strSQL .= " ss.tax_value-";
            $strSQL .= " ss.jkk_jkm-";
            $strSQL .= " ss.emp_jht-";
            $strSQL .= " ss.emp_health_bpjs-";
            $strSQL .= " ss.health_bpjs-";
            $strSQL .= " ss.emp_jp";
            $strSQL .= " AS DECIMAL(11,2))  "; /*END CAST*/
            $strSQL .= " ) AS total, ";

            /* START BRUTTO */
            $strSQL .= " (";
            $strSQL .= " CAST("; /*START CAST*/
            // $strSQL .= " ("; /*START SETAHUNKAN*/
            $strSQL .= " ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 + ";
            $strSQL .= " CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_allowance_economy = 1) THEN ss.allowance_economy ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_incentive_bonus = 1) THEN ss.incentive_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_production = 1) THEN ss.production_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_travel = 1) THEN ss.travel_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_cc_payment = 1) THEN ss.cc_payment ELSE 0 END +"; /*ok*/
            // $strSQL .= " ss.contract_bonus+";
            $strSQL .= " ss.workday_adj+";
            $strSQL .= " ss.thr+";
            $strSQL .= " ss.jkk_jkm+";
            $strSQL .= " ss.health_bpjs-";
            $strSQL .= " ss.unpaid_total+";
            $strSQL .= " ss.adjust_in-";
            $strSQL .= " ss.adjust_out";
            // $strSQL .= " ss.tax_value-";
            // $strSQL .= " ss.jkk_jkm-";
            /*$strSQL .= " ss.emp_jht";*/
            // $strSQL .= " ss.emp_health_bpjs ";
            // $strSQL .= " ss.health_bpjs";
            // $strSQL .= " )*12"; /*END SETAHUNKAN*/
            $strSQL .= " AS DECIMAL(11,2))  "; /*END CAST*/
            $strSQL .= " ) AS brutto_tax ";
            /* END BRUTTO */

            $strSQL .= "FROM mst_salary ms,mst_bio_rec mb,trn_salary_slip ss ";
            $strSQL .= "WHERE ms.company_name = '".$ptName."' ";
            $strSQL .= "AND ss.month_period = '".$monthPeriod."' ";
            $strSQL .= "AND ss.year_period = '".$yearPeriod."' ";
            // $strSQL .= "AND ms.payroll_group = '".$payrollGroup."' ";
            $strSQL .= "AND ms.bio_rec_id = mb.bio_rec_id  ";
            $strSQL .= "AND mb.bio_rec_id = ss.bio_rec_id ";
            $strSQL .= "AND mb.is_active = 1 ";   
            $strSQL .= $strFilter;  
            $strSQL .= "ORDER BY mb.full_name ";      
        }

        // echo $strSQL; exit(0);

        $query = $this->db->query($strSQL)->result_array();   
        /* AUTO WIDTH */     
        foreach(range('B','I') as $columnID)
        {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }

        // Nama Field Baris Pertama
        $spreadsheet->getActiveSheet()
            ->setCellValue('A1', 'PT. SANGATI SOERYA SEJAHTERA')
            ->setCellValue('A2', 'LIST PEMBAYARAN GAJI KARYAWAN PT. '.strtoupper($ptName))
            ->setCellValue('A4', 'Periode : '.$monthPeriod.'-'.$yearPeriod);

        $spreadsheet->getActiveSheet()->getStyle("A1:D1")->getFont()->setBold(true)->setSize(16);
        $spreadsheet->getActiveSheet()->getStyle("A2:D2")->getFont()->setBold(true)->setSize(13);
        $spreadsheet->getActiveSheet()->getStyle("A4:G4")->getFont()->setBold(true)->setSize(12); 

        $spreadsheet->getActiveSheet()
            ->mergeCells("A1:I1")
            ->mergeCells("A2:I2")
            ->mergeCells("A4:I4");

        $boldFont = [
            'font' => [
                'bold' => true
                // 'color' => ['argb' => '0000FF'],
            ],
        ];

        $totalStyle = [
            'font' => [
                'bold' => true,
                'color' => ['argb' => '0000FF'],
            ],
        ];

        $allBorderStyle = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $outlineBorderStyle = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $topBorderStyle = [
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $bottomBorderStyle = [
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        /* COLOURING FOOTER */
        $spreadsheet->getActiveSheet()->getStyle("A6:I7")
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B'); 

        $spreadsheet->getActiveSheet()
            ->setCellValue('A6', 'NO')  
            ->setCellValue('B6', 'NAMA')  
            ->setCellValue('C6', 'ID')  
            ->setCellValue('D6', 'CLASS')  
            ->setCellValue('E6', 'NO REKENING')
            ->setCellValue('F6', 'NAMA REKENING')  
            ->setCellValue('G6', 'JUMLAH')  
            ->setCellValue('H6', 'CODE BANK')  
            ->setCellValue('I6', 'BANK')  
            ;
        $spreadsheet->getActiveSheet()->getStyle("A6:I6")->getFont()->setSize(12);
        $spreadsheet->getActiveSheet()->getStyle("A6:I6")->applyFromArray($outlineBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("A6:A7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("B6:B7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("C6:C7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("D6:D7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("E6:E7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("F6:F7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("G6:G7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("H6:H7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("I6:I7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("A6:A7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("B6:B7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("C6:C7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("D6:D7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("E6:E7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("F6:F7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("G6:G7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("H6:H7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("I6:I7")->applyFromArray($center);

        $spreadsheet->getActiveSheet()->getStyle("A1:I4")->applyFromArray($center);

        $spreadsheet->getActiveSheet()
            ->mergeCells("A6:A7")
            ->mergeCells("B6:B7")
            ->mergeCells("C6:C7")
            ->mergeCells("D6:D7")
            ->mergeCells("E6:E7")
            ->mergeCells("F6:F7")
            ->mergeCells("G6:G7")
            ->mergeCells("H6:H7")
            ->mergeCells("I6:I7")
            ;

        $spreadsheet->getActiveSheet()->getStyle("B6:B7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("C6:C7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("D6:D7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("E6:E7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("F6:F7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("G6:G7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("H6:H7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("I6:I7")->getAlignment()->setWrapText(true);


        $bruttoTax = 0;
        /* TAX CONFIG */
        $myConfigId = 0;    
        switch ($ptName) {
            case 'Redpath_Timika':
                # code...
                $myConfigId = 1;
                break;
            case 'Pontil_Timika':
                # code...
                $myConfigId = 2;
                break;          
            default:
                # code...
                break;
        }
        $row = $this->M_payroll_config->getObjectById($myConfigId); 
        $taxPercent1 = $this->M_payroll_config->getTaxPercent1(); /* Persentase Progresif Pajak I */
        $taxPercent2 = $this->M_payroll_config->getTaxPercent2(); /* Persentase Progresif Pajak II */
        $taxPercent3 = $this->M_payroll_config->getTaxPercent3(); /* Persentase Progresif Pajak III */
        $taxPercent4 = $this->M_payroll_config->getTaxPercent4(); /* Persentase Progresif Pajak IV */
        $maxTaxVal1 = $this->M_payroll_config->getTaxMaxVal1(); /* Nominal Pajak I */ 
        $maxTaxVal2 = $this->M_payroll_config->getTaxMaxVal2(); /* Nominal Pajak II */
        $maxTaxVal3 = $this->M_payroll_config->getTaxMaxVal3(); /* Nominal Pajak III */
        $maxTaxVal4 = $this->M_payroll_config->getTaxMaxVal4(); /* Nominal Pajak IV */
        $maxNonTax = $this->M_payroll_config->getNonTaxAllowance(); /* Max Tunjangan Jabatan */
        

        $rowIdx = 8;
        $rowNo = 0;
        foreach ($query as $row) {
            $rowIdx++;
            $rowNo++;

            /* START UPDATE TAX */
            $bruttoTax = $row['brutto_tax'];
            $pembulatanPenghasilan = $bruttoTax;
            $nonTaxAllowance = ($pembulatanPenghasilan) * (5/100);
            if($nonTaxAllowance > $maxNonTax){
                $nonTaxAllowance = $maxNonTax;
            }

            $netto = $bruttoTax - $row['emp_jht'] - $row['emp_jp'] - $nonTaxAllowance;

            $unFixedIncome = 0;
            if($ptName == "Pontil_Timika"){
                $unFixedIncome = $row['thr'] + $row['cc_payment'];
            }else if($ptName == "Redpath_Timika"){
                $unFixedIncome = $row['thr'] + $row['contract_bonus'];
            }

            $nettoSetahun = ( ($netto - $unFixedIncome)*12 );
            $monthlyTax = 0;

            $taxVal1 = 0;    
            $taxVal2 = 0;    
            $taxVal3 = 0;    
            $taxVal4 = 0;

            $tVal = 0;
            $tSisa = 0;
            $pembulatanPenghasilan = $nettoSetahun - $row['ptkp_total'];
            if($pembulatanPenghasilan > 0)
            {
                /* TAX 1 */
                if($maxTaxVal1 > 0)
                {
                    $tVal = $pembulatanPenghasilan/$maxTaxVal1;  
                    if($tVal >= 1){
                        $taxVal1 = $maxTaxVal1 * ($taxPercent1/100);
                    }
                    else{
                        $taxVal1 = $pembulatanPenghasilan * ($taxPercent1/100);
                    }    
                }    
                
                /* TAX 2 */
                if($maxTaxVal2 > 0)
                {
                    if($pembulatanPenghasilan > $maxTaxVal1)
                    {
                        $tSisa = $pembulatanPenghasilan - $maxTaxVal1; 
                        $tVal = $tSisa/$maxTaxVal2;
                        if($tVal >= 1){
                            $taxVal2 = $maxTaxVal2 * ($taxPercent2/100);
                        }
                        else{
                            $taxVal2 = $tSisa * ($taxPercent2/100);
                        }
                    }     
                }
                 
                /* TAX 3 */
                if($maxTaxVal3 > 0)
                {
                    if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2))
                    {
                        $tSisa = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2;
                        $tVal = $tSisa/$maxTaxVal3;
                        if($tVal >= 1){
                            $taxVal3 = $maxTaxVal3 * ($taxPercent3/100);
                        }
                        else{
                            $taxVal3 = $tSisa * ($taxPercent3/100);
                        }
                    }    
                }
                 
                /* TAX 4 */
                if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2 + $maxTaxVal3))
                {
                    $tSisa = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2 - $maxTaxVal3;
                    $taxVal4 = $tSisa * ($taxPercent4/100); 
                }               
            }

            /* START THR - BONUS TAX -- EDITED @ AUG 15, 2017 BY FINANCE  */
            // $bonusTmp = $row['thr'] + $row['contract_bonus'];
            // $bonusTax = 0;
            // if( $taxVal4 > 0){
            //     $bonusTax = $bonusTmp * ($taxPercent4/100);
            // }else if( $taxVal3 > 0 ){
            //     $bonusTax = $bonusTmp * ($taxPercent3/100);
            // }else if( $taxVal2 > 0 ){
            //     $bonusTax = $bonusTmp * ($taxPercent2/100);                
            // }else{
            //     $bonusTax = $bonusTmp * ($taxPercent1/100);             
            // }

            /* END THR - BONUS TAX -- EDITED @ AUG 15, 2017 BY FINANCE */
            $taxTotalFix = $taxVal1 + $taxVal2 + $taxVal3 + $taxVal4;
            $taxTotal = ( $taxTotalFix/12 );
            if($taxTotal > 0){
                $monthlyTax = $taxTotal; 
            }else{
                $monthlyTax = 0; 
            }


            $nettoSetahunUnFix = ( ($netto - $unFixedIncome)*12 ) + $unFixedIncome;

            $taxTotalUnFix = 0;
            $taxValUnFix1 = 0;    
            $taxValUnFix2 = 0;    
            $taxValUnFix3 = 0;    
            $taxValUnFix4 = 0;

            $tValUnFix = 0;
            $tSisaUnFix = 0;
            $pembulatanPenghasilan = $nettoSetahunUnFix - $row['ptkp_total'];
            if($pembulatanPenghasilan > 0)
            {
                /* TAX 1 */
                if($maxTaxVal1 > 0)
                {
                    $tValUnFix = $pembulatanPenghasilan/$maxTaxVal1;  
                    if($tValUnFix >= 1){
                        $taxValUnFix1 = $maxTaxVal1 * ($taxPercent1/100);
                    }
                    else{
                        $taxValUnFix1 = $pembulatanPenghasilan * ($taxPercent1/100);
                    }    
                }    
                
                /* TAX 2 */
                if($maxTaxVal2 > 0)
                {
                    if($pembulatanPenghasilan > $maxTaxVal1)
                    {
                        $tSisaUnFix = $pembulatanPenghasilan - $maxTaxVal1; 
                        $tValUnFix = $tSisaUnFix/$maxTaxVal2;
                        if($tValUnFix >= 1){
                            $taxValUnFix2 = $maxTaxVal2 * ($taxPercent2/100);
                        }
                        else{
                            $taxValUnFix2 = $tSisaUnFix * ($taxPercent2/100);
                        }
                    }     
                }
                 
                /* TAX 3 */
                if($maxTaxVal3 > 0)
                {
                    if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2))
                    {
                        $tSisaUnFix = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2;
                        $tValUnFix = $tSisaUnFix/$maxTaxVal3;
                        if($tValUnFix >= 1){
                            $taxValUnFix3 = $maxTaxVal3 * ($taxPercent3/100);
                        }
                        else{
                            $taxValUnFix3 = $tSisaUnFix * ($taxPercent3/100);
                        }
                    }    
                }
                 
                /* TAX 4 */
                if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2 + $maxTaxVal3))
                {
                    $tSisaUnFix = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2 - $maxTaxVal3;
                    $taxValUnFix4 = $tSisaUnFix * ($taxPercent4/100); 
                }               
            }

            /* END THR - BONUS TAX -- EDITED @ AUG 15, 2017 BY FINANCE */

            $taxTotalUnFix = ( ($taxValUnFix1 + $taxValUnFix2 + $taxValUnFix3 + $taxValUnFix4) - $taxTotalFix );            
            

            $npwpNo = $row['npwp_no'];
            $npwpCharge = 0;
            if( strlen($npwpNo) < 19 ){
                $npwpCharge = $this->M_payroll_config->getNpwpCharge();
                $monthlyTax = $monthlyTax + ($monthlyTax * ($npwpCharge/100) ); 
                $taxTotalUnFix = $taxTotalUnFix + ($taxTotalUnFix * ($npwpCharge/100) ); 
            }



            $taxTotal = $monthlyTax + $taxTotalUnFix; 


            
            $tmpTotal = $row['total'] - floor($taxTotal) - $row['debt_burden'];
            $totalTerima = 0;
            if ($tmpTotal > 0)
            {
                $totalTerima = $tmpTotal;
            }

            /* START UPDATE TAX VALUE TO THE TABLE */
            if($row['tax_value'] <= 0)
            {
                $slipId = $row['salary_slip_id'];
                $str = "UPDATE trn_salary_slip SET tax_value = ".$taxTotal." WHERE salary_slip_id = '".$slipId."' ";
                $this->db->query($str);                
            }
            /* END UPDATE TAX VALUE TO THE TABLE */

            // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(7, $rowIdx, round($totalTerima) );
            
            $spreadsheet->getActiveSheet()
                ->setCellValue('A'.$rowIdx, $rowNo)
                ->setCellValue('B'.$rowIdx, $row['full_name'])
                ->setCellValueExplicit('C'.$rowIdx, $row['nie'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING)
                ->setCellValue('D'.$rowIdx, $row['position'])
                ->setCellValueExplicit('E'.$rowIdx, $row['account_no'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING)
                ->setCellValue('F'.$rowIdx, $row['account_name'])
                ->setCellValue('G'.$rowIdx, round($totalTerima))
                ->setCellValue('H'.$rowIdx, $row['bank_code'])
                ->setCellValue('I'.$rowIdx, $row['bank_name'])
                // ->setCellValue('I'.$rowIdx, round($taxTotalFix))
                // ->setCellValue('J'.$rowIdx, round($taxTotalUnFix))

                ;


            /* END UPDATE TAX */
            /* SET ROW COLOR */
            if($rowIdx % 2 == 1)
            {
                $spreadsheet->getActiveSheet()->getStyle('A'.$rowIdx.':I'.$rowIdx)
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('EAEBAF');             
            } 

        } /* end foreach ($query as $row) */       
        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(6, $rowIdx+2, "JUMLAH");
        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(7, $rowIdx+2, "=SUM(H9:H".$rowIdx.")");

        $spreadsheet->getActiveSheet()
                ->setCellValue('F'.($rowIdx+2), 'JUMLAH')
                ->setCellValue('G'.($rowIdx+2), '=SUM(G9:G'.$rowIdx.')');

        $spreadsheet->getActiveSheet()->getStyle("I6:I7")->applyFromArray($allBorderStyle);
        // $spreadsheet->getActiveSheet()->getStyle("H6:H7")->applyFromArray($totalStyle);
        $totalBorder = $rowIdx+2;
        $spreadsheet->getActiveSheet()->getStyle("A".$totalBorder.":I".$totalBorder)->applyFromArray($outlineBorderStyle);

        /* SET NUMBERS FORMAT*/
        $spreadsheet->getActiveSheet()->getStyle('G8:G'.($rowIdx+2))->getNumberFormat()->setFormatCode('#,##0.00');
        // $spreadsheet->getActiveSheet()->getStyle('A6:H7')->applyFromArray($totalStyle);
        // $spreadsheet->getActiveSheet()->getStyle('A'.($rowIdx+2).':H'.($rowIdx+2))->applyFromArray($totalStyle);
        /* COLOURING FOOTER */
        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":I".($rowIdx+2))
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B');

        unset($allBorderStyle);
        unset($center);
        unset($right);
        unset($left);
        
        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('Report Excel '.date('d-m-Y H'));

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        //Nama File
        // $str = $rowData['name'].$rowData['bio_rec_id'];
        // test($this->security->xss_clean($_POST($dept),1);
        $str = 'RPTPaymentList';
        $fileName = preg_replace('/\s+/', '', $str);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.Xlsx"');
        // header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        /* BY COMPOSER */
        // $writer = new Xlsx($spreadsheet);
        /* OFFLINE/ BY COPY EXCEL FOLDER  */
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit(0);   
    }
    /* END PAYMENT EXPORT */

    
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    // Load library phpspreadsheet
    require('./vendor/autoload.php');
    use PhpOffice\PhpSpreadsheet\Helper\Sample;
    use PhpOffice\PhpSpreadsheet\IOFactory;
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    // End load library phpspreadsheet

class Invoice extends CI_Controller {
	
	public function index()
	{
		$this->load->view('welcome_message');
	}

    /* START INVOICE LIST EXPORT */
    public function exportInvoiceSummaryRedpath($clientName, $yearPeriod, $monthPeriod, $dept = 'All')
    {
         // Create new Spreadsheet object
         $spreadsheet = new Spreadsheet();  

         $spreadsheet->getProperties()->setCreator('Maurice - Web - Android')
            ->setLastModifiedBy('Maurice - Web - Android')
            ->setTitle('Office 2007 XLSX Test Document')
            ->setSubject('Office 2007 XLSX Test Document')
            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Test result file');

         if (file_exists('assets/images/report_logo.png')) {
            $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing->setName('Logo');
            $drawing->setDescription('Logo');
            $drawing->setPath('./assets/images/report_logo.png');
            $drawing->setCoordinates('A1');
            $drawing->setHeight(36);
            $drawing->setWorksheet($spreadsheet->getActiveSheet());
        }


        $strSQL = "";
        $strFilter = "";
        if($dept != 'All')
        {
            $strFilter = " AND ss.dept = '".$dept."' ";    
        }

        if($clientName == "Redpath_Timika")
        {
            $strSQL  = "SELECT ";
            $strSQL .= "  @no_urut := @no_urut+1 no_urut,"; 
            $strSQL .= "  ms.bio_rec_id,";
            $strSQL .= "  ms.nie,";
            $strSQL .= "  ss.name,";
            $strSQL .= "  ss.dept,";
            $strSQL .= "  ss.dev_percent,";
            $strSQL .= "  ss.position job_desc,";
            $strSQL .= "  ss.basic_salary emp_basic,";
            $strSQL .= "  ss.payroll_level,";
            $strSQL .= "  ((ss.basic_salary/173)) rate,";
            $strSQL .= "  ss.normal_time,";
            $strSQL .= "  ss.ot_count1,";
            $strSQL .= "  ss.ot_count2,";
            $strSQL .= "  ss.ot_count3,";
            $strSQL .= "  ss.ot_count4,";
            $strSQL .= "  (ss.normal_time+ss.ot_count1+ss.ot_count2+ss.ot_count3+ss.ot_count4) worked_hours,";

            /* START OLD UPLIFT */
            // $strSQL .= "  CASE WHEN(ss.normal_time+ss.ot_count1+ss.ot_count2+ss.ot_count3+ss.ot_count4) <= 220 THEN";
            // $strSQL .= "   TRUNCATE((ss.normal_time+ss.ot_count1+ss.ot_count2+ss.ot_count3+ss.ot_count4) * TRUNCATE((ss.basic_salary/173),1) * (86/100),2)";
            // $strSQL .= "  ELSE TRUNCATE(220 * TRUNCATE((ss.basic_salary/173),1) * (86/100),2) END  uplift,";
            /* END OLD UPLIFT */

            /* START NEW UPLIFT */
            $strSQL .= "  CASE WHEN( 7*(ss.in_shift+ss.in_off+ss.in_ph) ) <= 173 THEN";
            $strSQL .= "    ( (40/100)*(ss.basic_salary/173) ) * ( 7*(ss.in_shift+ss.in_off+ss.in_ph) )";
            $strSQL .= "  ELSE ( ( (40/100)*(ss.basic_salary/173) ) * 173 ) END  uplift,";
            /* START OLD UPLIFT */

            $strSQL .= "  ( (ss.normal_time)+(ss.ot_count1*1.5)+(ss.ot_count2*2)+(ss.ot_count3*3)+(ss.ot_count4*4) ) paid_hours,";
            $strSQL .= "  (ot_1+ot_2+ot_3+ot_4) ot_total,";
            $strSQL .= "  ((ot_1+ot_2+ot_3+ot_4)*(35/100)) ot_bonus,"; 
            $strSQL .= "  ss.bs_prorate salary,"; 
            $strSQL .= "  ss.shift_bonus,"; 
            $strSQL .= "  ss.remote_allowance,"; 
            $strSQL .= "  ss.no_accident_bonus,"; 
            $strSQL .= "  (ss.dev_incentive_bonus * (ss.dev_percent/100)) AS dev_incentive_bonus, "; 
            $strSQL .= "  ((ss.dev_incentive_bonus * (ss.dev_percent/100)) * (ss.other_allowance1/100)) AS jumbo_bonus, ";
            $strSQL .= "  tro.attend_total + tro.in_ph_total AS attend_total "; 
            $strSQL .= "FROM "; 
            $strSQL .= "  (SELECT @no_urut := 0) s,"; 
            $strSQL .= "  mst_salary ms,"; 
            $strSQL .= "  trn_salary_slip ss, "; 
            $strSQL .= "  trn_overtime tro "; 
            $strSQL .= "WHERE "; 
            $strSQL .= "  ms.bio_rec_id = ss.bio_rec_id "; 
            $strSQL .= "AND "; 
            $strSQL .= "  ms.bio_rec_id = tro.bio_rec_id "; 
            $strSQL .= "AND "; 
            $strSQL .= "  ss.client_name = '".$clientName."' "; 
            $strSQL .= "AND "; 
            $strSQL .= "  ss.year_period = '".$yearPeriod."' "; 
            $strSQL .= "AND "; 
            $strSQL .= "  ss.month_period = '".$monthPeriod."' ";
            $strSQL .= "AND "; 
            $strSQL .= "  tro.client_name = '".$clientName."' "; 
            $strSQL .= "AND "; 
            $strSQL .= "  tro.year_period = '".$yearPeriod."' "; 
            $strSQL .= "AND "; 
            $strSQL .= "  tro.month_period = '".$monthPeriod."' ";            
            $strSQL .= $strFilter; 
            $strSQL .= "ORDER BY "; 
            $strSQL .= "  ss.name "; 

        }
        
        $query = $this->db->query($strSQL)->result_array();  

        $boldFont = [
            'font' => [
                'bold' => true
                // 'color' => ['argb' => '0000FF'],
            ],
        ];

        $totalStyle = [
            'font' => [
                'bold' => true,
                'color' => ['argb' => '0000FF'],
            ],
        ];

        $allBorderStyle = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $outlineBorderStyle = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $topBorderStyle = [
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $bottomBorderStyle = [
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;        

        // Nama Field Baris Pertama
        $spreadsheet->getActiveSheet()
            ->setCellValue('A1', 'SUMMARY INVOICE')
            ->setCellValue('A2', 'PT. REDPATH')
            ->setCellValue('A3', 'Dept : '.$dept)
            ->setCellValue('A4', 'PERIOD : '.$monthPeriod.'-'.$yearPeriod)
            ->setCellValue('A6', 'NO')
            ->setCellValue('B6', 'NIE')
            ->setCellValue('C6', 'NAME')
            ->setCellValue('D6', 'JOB DESC')
            ->setCellValue('E6', 'LEVEL')
            ->setCellValue('F6', 'DEPT')
            ->setCellValue('G6', '%PVB')
            ->setCellValue('H6', 'ATTEND COUNT')
            ->setCellValue('I6', 'BASIC')
            ->setCellValue('J6', 'RATE')
            ->setCellValue('K6', 'X1.0')
            ->setCellValue('L6', 'X1.5')
            ->setCellValue('M6', 'X2.0')
            ->setCellValue('N6', 'X3.0')
            ->setCellValue('O6', 'X4.0')
            ->setCellValue('P6', 'WORKED HOURS')
            ->setCellValue('Q6', 'UPLIFT')
            ->setCellValue('R6', 'PAID HOURS')
            ->setCellValue('S6', 'OT')
            ->setCellValue('T6', 'OT BONUS')
            ->setCellValue('U6', 'PRORATE')
            ;

        $spreadsheet->getActiveSheet()
            ->mergeCells("A1:AB1")
            ->mergeCells("A2:AB2")
            ->mergeCells("A3:AB3")
            ->mergeCells("A4:AB4")
            ;

        $spreadsheet->getActiveSheet()
            ->mergeCells("A6:A7")
            ->mergeCells("B6:B7")
            ->mergeCells("C6:C7")
            ->mergeCells("D6:D7")
            ->mergeCells("E6:E7")
            ->mergeCells("F6:F7")
            ->mergeCells("G6:G7")
            ->mergeCells("H6:H7")
            ->mergeCells("I6:I7")
            ->mergeCells("J6:J7")
            ->mergeCells("K6:K7")
            ->mergeCells("L6:L7")
            ->mergeCells("M6:M7")
            ->mergeCells("N6:N7")
            ->mergeCells("O6:O7")
            ->mergeCells("P6:P7")
            ->mergeCells("Q6:Q7")
            ->mergeCells("R6:R7")
            ->mergeCells("S6:S7")
            ->mergeCells("T6:T7")
            ->mergeCells("U6:U7")
            ->mergeCells("AA6:AA7")
            ->mergeCells("AB6:AB7");

        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'SUMMARY INVOICE');
        // $objPHPExcel->getActiveSheet()->mergeCells("A1:AB1");
        $spreadsheet->getActiveSheet()->getStyle("A1:AB1")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("A2:AB2")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("A3:AB3")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("A4:AB4")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("A6:A7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("B6:B7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("C6:C7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("D6:D7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("E6:E7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("F6:F7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("G6:G7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("H6:H7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("I6:I7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("J6:J7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("K6:K7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("L6:L7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("M6:M7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("N6:N7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("O6:O7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("P6:P7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("Q6:Q7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("R6:R7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("S6:S7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("T6:T7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("U6:U7")->applyFromArray($center); 
        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(0, 2, 'PT. REDPATH');
        // $spreadsheet->getActiveSheet()->mergeCells("A2:AB2");
        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(0, 4, 'PERIOD : '.$monthPeriod.'-'.$yearPeriod);
        // $spreadsheet->getActiveSheet()->mergeCells("A4:AB4");
        $spreadsheet->getActiveSheet()->getStyle("A1:D1")->getFont()->setBold(true)->setSize(16);
        $spreadsheet->getActiveSheet()->getStyle("A2:D2")->getFont()->setBold(true)->setSize(13);
        $spreadsheet->getActiveSheet()->getStyle("A3:G3")->getFont()->setBold(true)->setSize(12);        
        $spreadsheet->getActiveSheet()->getStyle("A4:G4")->getFont()->setBold(true)->setSize(12);        
        $spreadsheet->getActiveSheet()->getStyle("A6:H6")->getFont()->setSize(12);

        // $spreadsheet->getActiveSheet()->getStyle("A6:H6")->applyFromArray($outlineBorderStyle);

        $spreadsheet->getActiveSheet()->getStyle("A6:AB7")->applyFromArray($allBorderStyle);

        /* SET HEADER BG COLOR*/
        $spreadsheet->getActiveSheet()->getStyle('A6:AB7')
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B'); 

        // $spreadsheet->getActiveSheet()->getStyle("B6:B7")->getAlignment()->setWrapText(true);
        // $spreadsheet->getActiveSheet()->getStyle("C6:C7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("D6:D7")->getAlignment()->setWrapText(true);
        // $spreadsheet->getActiveSheet()->getStyle("E6:E7")->getAlignment()->setWrapText(true);
        // $spreadsheet->getActiveSheet()->getStyle("F6:F7")->getAlignment()->setWrapText(true);
        // $spreadsheet->getActiveSheet()->getStyle("G6:G7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("H6:H7")->getAlignment()->setWrapText(true);
        // $spreadsheet->getActiveSheet()->getStyle("I6:I7")->getAlignment()->setWrapText(true);
        // $spreadsheet->getActiveSheet()->getStyle("J6:J7")->getAlignment()->setWrapText(true);
        // $spreadsheet->getActiveSheet()->getStyle("K6:K7")->getAlignment()->setWrapText(true);
        // $spreadsheet->getActiveSheet()->getStyle("L6:L7")->getAlignment()->setWrapText(true);
        // $spreadsheet->getActiveSheet()->getStyle("M6:M7")->getAlignment()->setWrapText(true);
        // $spreadsheet->getActiveSheet()->getStyle("N6:N7")->getAlignment()->setWrapText(true);
        // $spreadsheet->getActiveSheet()->getStyle("O6:O7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("P6:P7")->getAlignment()->setWrapText(true);
        // $spreadsheet->getActiveSheet()->getStyle("Q6:Q7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("R6:R7")->getAlignment()->setWrapText(true);
        // $spreadsheet->getActiveSheet()->getStyle("S6:S7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("T6:T7")->getAlignment()->setWrapText(true);
        // $spreadsheet->getActiveSheet()->getStyle("U6:U7")->getAlignment()->setWrapText(true);
        // $spreadsheet->getActiveSheet()->getStyle("V6:V7")->getAlignment()->setWrapText(true);
        // $spreadsheet->getActiveSheet()->getStyle("W6:W7")->getAlignment()->setWrapText(true);
        // $spreadsheet->getActiveSheet()->getStyle("X6:X7")->getAlignment()->setWrapText(true);
        // $spreadsheet->getActiveSheet()->getStyle("Y6:Y7")->getAlignment()->setWrapText(true);
        // $spreadsheet->getActiveSheet()->getStyle("Z6:Z7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("AA6:AA7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("AB6:AB7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("V7:Z7")->getAlignment()->setWrapText(true);
        
        $spreadsheet->getActiveSheet()
            ->mergeCells("V6:Z6");

        $spreadsheet->getActiveSheet()
            ->setCellValue('V6', 'BONUS')
            ->setCellValue('V7', 'SHIFT')
            ->setCellValue('W7', 'DEV BONUS')
            ->setCellValue('X7', 'REMOTE LOC')
            ->setCellValue('Y7', 'ACCIDENT FREE')
            ->setCellValue('Z7', 'JUMBO')
            ->setCellValue('AA6', 'AMOUNT TO EMPLOYEE')
            ->setCellValue('AB6', 'TOTAL CHARGE')
            ;

        /* START PAID SHIFT BONUS */
       
        $spreadsheet->getActiveSheet()->getStyle("V6:V7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("W6:W7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("X6:X7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("Y6:Y7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("Z6:Z7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("AA6:AA7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("AB6:AB7")->applyFromArray($center);
        /* END PAID SHIFT BONUS */

        $rowIdx = 8;
        $rowNo = 0;
        foreach ($query as $row) {
            $rowIdx++;
            $rowNo++;

            $spreadsheet->getActiveSheet()
                ->setCellValue('A'.($rowIdx), $rowNo)
                ->setCellValue('B'.($rowIdx), $row['nie'])
                ->setCellValue('C'.($rowIdx), $row['name'])
                ->setCellValue('D'.($rowIdx), $row['job_desc'])
                ->setCellValue('E'.($rowIdx), $row['payroll_level'])
                ->setCellValue('F'.($rowIdx), $row['dept'])
                ->setCellValue('G'.($rowIdx), $row['dev_percent'])
                ->setCellValue('H'.($rowIdx), $row['attend_total'])
                ->setCellValue('I'.($rowIdx), $row['emp_basic'])
                ->setCellValue('J'.($rowIdx), $row['rate'])
                ->setCellValue('K'.($rowIdx), $row['normal_time'])
                ->setCellValue('L'.($rowIdx), $row['ot_count1'])
                ->setCellValue('M'.($rowIdx), $row['ot_count2'])
                ->setCellValue('N'.($rowIdx), $row['ot_count3'])
                ->setCellValue('O'.($rowIdx), $row['ot_count4'])
                ->setCellValue('P'.($rowIdx), $row['worked_hours'])
                /*UPLIFT*/
                ->setCellValue('Q'.($rowIdx), '=(40%)*(I'.$rowIdx.'/173) * IF( (7*H'.$rowIdx.')>=173,173,(7*H'.$rowIdx.'))')
                ->setCellValue('R'.($rowIdx), $row['paid_hours'])
                ->setCellValue('S'.($rowIdx), $row['ot_total'])
                ->setCellValue('T'.($rowIdx), $row['ot_bonus'])
                ->setCellValue('U'.($rowIdx), $row['salary'])
                ->setCellValue('V'.($rowIdx), $row['shift_bonus'])
                ->setCellValue('W'.($rowIdx), $row['dev_incentive_bonus'])
                ->setCellValue('X'.($rowIdx), $row['remote_allowance'])
                ->setCellValue('Y'.($rowIdx), $row['no_accident_bonus'])
                ->setCellValue('Z'.($rowIdx), $row['jumbo_bonus'])
                ->setCellValue('AA'.($rowIdx), "=SUM(S".$rowIdx.":Z".$rowIdx.")")
                ->setCellValue('AB'.($rowIdx), "=SUM(Q".$rowIdx.",AA".$rowIdx.")")
                ;

            /* SET ROW COLOR */
            if($rowIdx % 2 == 1)
            {
                $spreadsheet->getActiveSheet()->getStyle('A'.$rowIdx.':AB'.$rowIdx)
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('EAEBAF');             
            } 
            /* END UPDATE TAX */
        } /* end foreach ($query as $row) */

        
        $spreadsheet->getActiveSheet()
            ->setCellValue('O'.($rowIdx+2), 'TOTAL')
            ->setCellValue('Q'.($rowIdx+2), '=SUM(Q9:Q'.$rowIdx.')')
            ->setCellValue('AA'.($rowIdx+2), "=SUM(AA9:AA".$rowIdx.")")
            ->setCellValue('AB'.($rowIdx+2), "=SUM(AB9:AB".$rowIdx.")");
        
        $spreadsheet->getActiveSheet()->getStyle('A'.($rowIdx+2).':AB'.($rowIdx+2))->applyFromArray($totalStyle);
        $spreadsheet->getActiveSheet()->getStyle('Q'.($rowIdx+2).':AB'.($rowIdx+2))->getNumberFormat()->setFormatCode('#,##0.00'); 
        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(10, ($rowIdx+2), "TOTAL");
        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(16, ($rowIdx+2), "=SUM(Q9:Q".$rowIdx.")");
        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(26, ($rowIdx+2), "=SUM(AA9:AA".$rowIdx.")");
        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(27, ($rowIdx+2), "=SUM(AB9:AB".$rowIdx.")");
        // $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":T".($rowIdx+2))->getFont()->setSize(12);
        // $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":AB".($rowIdx+2))->applyFromArray($outlineBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":AB".($rowIdx+2))
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B');

        /* SET NUMBERS FORMAT*/
        
        unset($allBorderStyle);
        unset($center);
        unset($right);
        unset($left);
        
        
        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('Report Excel '.date('d-m-Y H'));

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        //Nama File
        // $str = $rowData['name'].$rowData['bio_rec_id'];
        $str = 'RPTSumInvoice';
        $fileName = preg_replace('/\s+/', '', $str);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.Xlsx"');
        // header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        /* BY COMPOSER */
        // $writer = new Xlsx($spreadsheet);
        /* OFFLINE/ BY COPY EXCEL FOLDER  */
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit(0);  
    }
    /* END INVOICE LIST EXPORT */

    /* START PONTIL INVOICE LIST EXPORT */
    public function exportSummaryInvoicePontil($clientName, $yearPeriod, $monthPeriod, $payrollGroup='All')
    {
        // Create new Spreadsheet object
         $spreadsheet = new Spreadsheet();  

         $spreadsheet->getProperties()->setCreator('Maurice - Web - Android')
            ->setLastModifiedBy('Maurice - Web - Android')
            ->setTitle('Office 2007 XLSX Test Document')
            ->setSubject('Office 2007 XLSX Test Document')
            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Test result file');

         if (file_exists('assets/images/report_logo.png')) {
            $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing->setName('Logo');
            $drawing->setDescription('Logo');
            $drawing->setPath('./assets/images/report_logo.png');
            $drawing->setCoordinates('A1');
            $drawing->setHeight(36);
            $drawing->setWorksheet($spreadsheet->getActiveSheet());
        }

        $strSQL = "";
        $strFilter = "";
        if($payrollGroup != 'All')
        {
            $strFilter = " AND ss.payroll_group = '".$payrollGroup."' ";    
        }

        $strSQL  = " SELECT ";   
        $strSQL .= "   ms.bio_rec_id, ms.nie, ss.name, ss.position job_desc, ss.basic_salary,ss.bs_prorate, ";
        $strSQL .= "   (ss.bs_prorate - unpaid_total) current_salary, ";
        $strSQL .= "   TRUNCATE((ss.basic_salary/173),1) rate, ";
        $strSQL .= "   ss.normal_time, ss.ot_count1, ss.ot_count2, ss.ot_count3, ss.ot_count4, ";
        $strSQL .= "   ss.ot_1, ss.ot_2, ss.ot_3, ss.ot_4, ";
        $strSQL .= "   (ss.normal_time+ss.ot_count1+ss.ot_count2+ss.ot_count3+ss.ot_count4) worked_hours, ss.travel_bonus, ";
        $strSQL .= "   CASE WHEN(ss.normal_time+ss.ot_count1+ss.ot_count2+ss.ot_count3+ss.ot_count4) <= 312 THEN ";
        $strSQL .= "      TRUNCATE((ss.normal_time+ss.ot_count1+ss.ot_count2+ss.ot_count3+ss.ot_count4) * TRUNCATE((ss.basic_salary/173),1) * (64/100),2) ";
        $strSQL .= "   ELSE TRUNCATE(312 * TRUNCATE((ss.basic_salary/173),1) * (64/100),2) END uplift, ";
        $strSQL .= "    TRUNCATE( (ss.normal_time)+(ss.ot_count1*1.5)+(ss.ot_count2*2)+(ss.ot_count3*3)+(ss.ot_count4*4),1 ) paid_hours, ";
        $strSQL .= "   (ot_1+ot_2+ot_3+ot_4) ot_total, ";
        $strSQL .= "   TRUNCATE((ot_1+ot_2+ot_3+ot_4)*(35/100),2) ot_bonus, ";
        $strSQL .= "   ss.travel_bonus, ss.shift_bonus, ss.incentive_bonus, ss.allowance_economy, ss.workday_adj, ss.production_bonus ";
        $strSQL .= "   FROM mst_salary ms, trn_salary_slip ss ";
        $strSQL .= "   WHERE ms.bio_rec_id = ss.bio_rec_id ";
        $strSQL .= "   AND ss.client_name = 'Pontil_Timika'  ";       
        $strSQL .= "   AND ss.year_period = '".$yearPeriod."' ";       
        $strSQL .= "   AND ss.month_period = '".$monthPeriod."' ";       
        $strSQL .= $strFilter;
        $strSQL .= "    ORDER BY ss.name  ;";    

        $query = $this->db->query($strSQL)->result_array();  

        $boldFont = [
            'font' => [
                'bold' => true
                // 'color' => ['argb' => '0000FF'],
            ],
        ];

        $totalStyle = [
            'font' => [
                'bold' => true,
                'color' => ['argb' => '0000FF'],
            ],
        ];

        $allBorderStyle = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $outlineBorderStyle = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $topBorderStyle = [
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $bottomBorderStyle = [
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        foreach(range('B','Q') as $columnID)
        {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }           

        // Nama Field Baris Pertama
        $spreadsheet->getActiveSheet()
                ->setCellValue('A1', 'SUMMARY INVOICE PT SANGATI SOERYA SEJAHTERA - PT PONTIL INDONESIA (PAPUA/TIMIKA)')
                ->setCellValue('A2', 'PERIOD : '.$monthPeriod.'-'.$yearPeriod)
                ->setCellValue('A3', 'DATE        : ')
                ->setCellValue('A4', 'INVOICE NO  : ')
                ->setCellValue('A5', 'CONTRACT NO : ');

        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'SUMMARY INVOICE PT SANGATI SOERYA SEJAHTERA - PT PONTIL INDONESIA (PAPUA/TIMIKA)');
        $spreadsheet->getActiveSheet()->mergeCells("A1:Q1");
        $spreadsheet->getActiveSheet()->getStyle("A1:Q1")->applyFromArray($center);
        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(0, 2, 'PT. PONTIL');
        // $spreadsheet->getActiveSheet()->mergeCells("A2:U2");
        // $spreadsheet->getActiveSheet()->getStyle("A2:U2")->applyFromArray($center);
        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(0, 2, 'PERIOD : '.$monthPeriod.'-'.$yearPeriod);
        $spreadsheet->getActiveSheet()->mergeCells("A2:Q2");
        $spreadsheet->getActiveSheet()->getStyle("A2:Q2")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("A1:D1")->getFont()->setBold(true)->setSize(16);
        $spreadsheet->getActiveSheet()->getStyle("A2:D2")->getFont()->setBold(true)->setSize(13);
        $spreadsheet->getActiveSheet()->getStyle("A3:G5")->getFont()->setBold(true)->setSize(12); 
        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(0, 3, 'DATE        : ');
        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(0, 4, 'INVOICE NO  : ');
        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(0, 5, 'CONTRACT NO : ');        

        /* SET HEADER BG COLOR*/
        $spreadsheet->getActiveSheet()->getStyle('A6:Q8')
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B'); 

        /* START INVOICE TITLE */
        $spreadsheet->getActiveSheet()->getStyle("A6:H6")->getFont()->setSize(12);

        $spreadsheet->getActiveSheet()->getStyle("A6:A7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("A8:A8")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("B6:B7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("B8:B8")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("C6:C7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("C8:C8")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("D6:E6")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("D7:D7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("D8:D8")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("E7:E7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("E8:E8")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("F6:F7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("F8:F8")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("G6:G6")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("G7:G7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("G8:G8")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("H6:H6")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("H7:H7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("H8:H8")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("I6:I6")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("I7:I7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("I8:I8")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("J6:J6")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("J7:J7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("J8:J8")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("K6:K7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("K8:K8")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("L6:L7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("L8:L8")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("M6:M7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("M8:M8")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("N6:N7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("N8:N8")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("O6:O7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("O8:O8")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("P6:P7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("P8:P8")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("Q6:Q7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("Q8:Q8")->applyFromArray($allBorderStyle); 

        $spreadsheet->getActiveSheet()->getStyle("A6:A7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("A8:A8")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("B6:B7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("B8:B8")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("C6:C7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("C8:C8")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("D6:E6")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("D7:D7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("D8:D8")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("E7:E7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("E8:E8")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("F6:F7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("F8:F8")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("G6:G6")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("G7:G7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("G8:G8")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("H6:H6")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("H7:H7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("H8:H8")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("I6:I6")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("I7:I7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("I8:I8")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("Q8:Q8")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("J6:J6")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("J7:J7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("J8:J8")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("K6:K7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("K8:K8")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("L6:L7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("L8:L8")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("M6:M7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("M8:M8")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("N6:N7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("N8:N8")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("O6:O7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("O8:O8")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("P6:P7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("P8:P8")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("Q6:Q7")->applyFromArray($center);

        $spreadsheet->getActiveSheet()->getStyle("B6:B7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("C6:C7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("D7:D7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("E7:E7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("F6:F7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("G6:G6")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("G7:G7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("H6:H6")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("H7:H7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("I6:I6")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("I7:I7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("J6:J6")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("J7:J7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("K6:K7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("L6:L7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("M6:M7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("N6:N7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("O6:O7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("P6:P7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("Q6:Q7")->getAlignment()->setWrapText(true);

        $spreadsheet->getActiveSheet()->getStyle("A6:H6")->applyFromArray($outlineBorderStyle);

        $spreadsheet->getActiveSheet()->mergeCells("A6:A7");
        $spreadsheet->getActiveSheet()->mergeCells("B6:B7");
        $spreadsheet->getActiveSheet()->mergeCells("C6:C7");
        $spreadsheet->getActiveSheet()->mergeCells("D6:E6");
        $spreadsheet->getActiveSheet()->mergeCells("D7:D7");
        $spreadsheet->getActiveSheet()->mergeCells("D8:D8");
        $spreadsheet->getActiveSheet()->mergeCells("E7:E7");
        $spreadsheet->getActiveSheet()->mergeCells("E8:E8");
        $spreadsheet->getActiveSheet()->mergeCells("F6:F7");
        $spreadsheet->getActiveSheet()->mergeCells("G6:G6");
        $spreadsheet->getActiveSheet()->mergeCells("G7:G7");
        $spreadsheet->getActiveSheet()->mergeCells("H6:H6");
        $spreadsheet->getActiveSheet()->mergeCells("H7:H7");
        $spreadsheet->getActiveSheet()->mergeCells("I6:I6");
        $spreadsheet->getActiveSheet()->mergeCells("I7:I7");
        $spreadsheet->getActiveSheet()->mergeCells("J6:J6");
        $spreadsheet->getActiveSheet()->mergeCells("J7:J7");
        $spreadsheet->getActiveSheet()->mergeCells("K6:K7");
        $spreadsheet->getActiveSheet()->mergeCells("L6:L7");
        $spreadsheet->getActiveSheet()->mergeCells("M6:M7");
        $spreadsheet->getActiveSheet()->mergeCells("N6:N7");
        $spreadsheet->getActiveSheet()->mergeCells("O6:O7");
        $spreadsheet->getActiveSheet()->mergeCells("P6:P7");
        $spreadsheet->getActiveSheet()->mergeCells("Q6:Q7");

        $spreadsheet->getActiveSheet()
                ->setCellValue('A6', 'NO')
                ->setCellValue('A8', 'A')
                ->setCellValue('B6', 'NAME')
                ->setCellValue('B8', 'B')
                ->setCellValue('C6', 'POSITION')
                ->setCellValue('C8', 'C')
                ->setCellValue('D6', 'SALARY')
                ->setCellValue('D7', 'BASE MONTH')
                ->setCellValue('D8', 'D')
                ->setCellValue('E7', 'BASE HOUR')
                ->setCellValue('E8', 'E')
                ->setCellValue('F6', 'NT')
                ->setCellValue('F8', 'F')
                ->setCellValue('G6', 'X 1.5')
                ->setCellValue('G7', 'HOURS')
                ->setCellValue('G8', 'G')
                ->setCellValue('H6', 'X 2.0')
                ->setCellValue('H7', 'HOURS')
                ->setCellValue('H8', 'H')
                ->setCellValue('I6', 'X 3.0')
                ->setCellValue('I7', 'HOURS')
                ->setCellValue('I8', 'I')
                ->setCellValue('J6', 'X 4.0')
                ->setCellValue('J7', 'HOURS')
                ->setCellValue('J8', 'J')
                ->setCellValue('K6', 'WORK HOURS')
                ->setCellValue('K8', 'K')
                ->setCellValue('L6', 'SALARY THIS MONTH')
                ->setCellValue('L8', 'L')
                ->setCellValue('M6', 'OVER TIME')
                ->setCellValue('M8', 'M')
                ->setCellValue('N6', 'TRAVEL ALLOWANCE')
                ->setCellValue('N8', 'N')
                ->setCellValue('O6', 'GROSS SALARY')
                ->setCellValue('O8', 'O')
                ->setCellValue('P6', 'CONTRACTOR FEE')
                ->setCellValue('P8', 'P')
                ->setCellValue('Q6', 'TOTAL INVOICE')
                ->setCellValue('Q8', 'Q');

        /* START TOTAL WORK HOUR */

        $rowIdx = 9;
        $startIdx = $rowIdx; 
        $rowNo = 0;
        foreach ($query as $row) {
            $rowIdx++;
            $rowNo++;

            $gross = $row['current_salary'] + $row['ot_total'] + $row['travel_bonus'];
            $chargeTotal = $gross + $row['uplift'];

            $spreadsheet->getActiveSheet()
                ->setCellValue('A'.$rowIdx, $rowNo)
                ->setCellValue('B'.$rowIdx, $row['name'])
                ->setCellValue('C'.$rowIdx, $row['job_desc'])
                ->setCellValue('D'.$rowIdx, $row['basic_salary'])
                ->setCellValue('E'.$rowIdx, $row['rate'])
                ->setCellValue('F'.$rowIdx, $row['normal_time'])
                ->setCellValue('G'.$rowIdx, $row['ot_count1'])
                ->setCellValue('H'.$rowIdx, $row['ot_count2'])
                ->setCellValue('I'.$rowIdx, $row['ot_count3'])
                ->setCellValue('J'.$rowIdx, $row['ot_count4'])
                ->setCellValue('K'.$rowIdx, $row['worked_hours'])
                ->setCellValue('L'.$rowIdx, $row['current_salary'])
                ->setCellValue('M'.$rowIdx, $row['ot_total'])
                ->setCellValue('N'.$rowIdx, $row['travel_bonus'])
                ->setCellValue('O'.$rowIdx, $gross)
                ->setCellValue('P'.$rowIdx, $row['uplift'])
                ->setCellValue('Q'.$rowIdx, $chargeTotal);

            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $rowIdx, $rowNo);
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $rowIdx, $row['name']);
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $rowIdx, $row['job_desc']);
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $rowIdx, $row['basic_salary']);
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $rowIdx, $row['rate']);
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $rowIdx, $row['normal_time']);
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $rowIdx, $row['ot_count1']);
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $rowIdx, $row['ot_count2']);
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $rowIdx, $row['ot_count3']);
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $rowIdx, $row['ot_count4']);
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $rowIdx, $row['worked_hours']);
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $rowIdx, $row['current_salary']);
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $rowIdx, $row['ot_total']);
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $rowIdx, $row['travel_bonus']);
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $rowIdx, $gross);
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $rowIdx, $row['uplift']);
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15, $rowIdx, '=IF(K'.$rowIdx.'<=312,((E'.$rowIdx.'*K'.$rowIdx.')*64%),((E'.$rowIdx.'*312)*64%))');
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $rowIdx, '1');
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16, $rowIdx, $chargeTotal);
            /* SET ROW COLOR */
            if($rowIdx % 2 == 1)
            {
                $spreadsheet->getActiveSheet()->getStyle('A'.$rowIdx.':Q'.$rowIdx)
                    ->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB('EAEBAF');             
            } 
            /* END UPDATE TAX */
        } /* end foreach ($query as $row) */

        $spreadsheet->getActiveSheet()
            ->setCellValue('F'.($rowIdx+2), 'TOTAL')
            ->setCellValue('Q'.($rowIdx+2), '=SUM(Q'.$startIdx.':Q'.$rowIdx.')')
            ;
        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":Q".($rowIdx+2))->getFont()->setBold(true)->setSize(12);
        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":Q".($rowIdx+2))->applyFromArray($outlineBorderStyle);

        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(2, ($rowIdx+2), "TOTAL");
        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(16, ($rowIdx+2), "=SUM(Q".$startIdx.":Q".$rowIdx.")");
        $spreadsheet->getActiveSheet()->getStyle('D9:Q'.($rowIdx+2))->getNumberFormat()->setFormatCode('#,##0.00');       
        
        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":Q".($rowIdx+2))
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B');

        /* SET NUMBERS FORMAT*/
        
        unset($allBorderStyle);
        unset($center);
        unset($right);
        unset($left);
        
        $spreadsheet->setActiveSheetIndex(0);

        $str = 'PTLSumInvoice';
        $fileName = preg_replace('/\s+/', '', $str);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.Xlsx"');
        // header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        /* BY COMPOSER */
        // $writer = new Xlsx($spreadsheet);
        /* OFFLINE/ BY COPY EXCEL FOLDER  */
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit(0);   
    }
    /* END PONTIL INVOICE LIST EXPORT */


    /* START PONTIL SUMMARY REIMBURSEMENT EXPORT */
    public function exportSummaryReimbursementPontil($clientName, $yearPeriod, $monthPeriod, $payrollGroup)
    {
        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();  

        $spreadsheet->getProperties()->setCreator('Maurice - Web - Android')
            ->setLastModifiedBy('Maurice - Web - Android')
            ->setTitle('Office 2007 XLSX Test Document')
            ->setSubject('Office 2007 XLSX Test Document')
            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Test result file');

        if (file_exists('assets/images/report_logo.png')) {
            $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing->setName('Logo');
            $drawing->setDescription('Logo');
            $drawing->setPath('./assets/images/report_logo.png');
            $drawing->setCoordinates('A1');
            $drawing->setHeight(36);
            $drawing->setWorksheet($spreadsheet->getActiveSheet());
        }

        $strSQL = "";
        $strFilter = "";
        if($payrollGroup != 'All')
        {
            $strFilter = " AND ss.payroll_group = '".$payrollGroup."' ";    
        }

        $strSQL  = " SELECT ";   
        $strSQL .= "   ms.bio_rec_id, ms.nie, ss.name, ss.position job_desc, ss.basic_salary,ss.bs_prorate, ";
        $strSQL .= "   (ss.bs_prorate - unpaid_total) current_salary, ";
        $strSQL .= "   TRUNCATE((ss.basic_salary/173),1) rate, ";
        $strSQL .= "   ss.normal_time, ss.ot_count1, ss.ot_count2, ss.ot_count3, ss.ot_count4, ";
        $strSQL .= "   ss.ot_1, ss.ot_2, ss.ot_3, ss.ot_4, ";
        $strSQL .= "   (ss.normal_time+ss.ot_count1+ss.ot_count2+ss.ot_count3+ss.ot_count4) worked_hours, ss.travel_bonus, ";
        $strSQL .= "   CASE WHEN(ss.normal_time+ss.ot_count1+ss.ot_count2+ss.ot_count3+ss.ot_count4) <= 312 THEN ";
        $strSQL .= "      TRUNCATE((ss.normal_time+ss.ot_count1+ss.ot_count2+ss.ot_count3+ss.ot_count4) * TRUNCATE((ss.basic_salary/173),1) * (64/100),2) ";
        $strSQL .= "   ELSE TRUNCATE(312 * TRUNCATE((ss.basic_salary/173),1) * (64/100),2) END uplift, ";
        $strSQL .= "    TRUNCATE( (ss.normal_time)+(ss.ot_count1*1.5)+(ss.ot_count2*2)+(ss.ot_count3*3)+(ss.ot_count4*4),1 ) paid_hours, ";
        $strSQL .= "   (ot_1+ot_2+ot_3+ot_4) ot_total, ";
        $strSQL .= "   TRUNCATE((ot_1+ot_2+ot_3+ot_4)*(35/100),2) ot_bonus, ";
        $strSQL .= "   ss.travel_bonus, ss.shift_bonus, ss.incentive_bonus, ss.allowance_economy, ss.workday_adj, ss.production_bonus ";
        $strSQL .= "   FROM mst_salary ms, trn_salary_slip ss ";
        $strSQL .= "   WHERE ms.bio_rec_id = ss.bio_rec_id ";
        $strSQL .= "   AND ss.client_name = 'Pontil_Timika'  ";       
        $strSQL .= "   AND ss.year_period = '".$yearPeriod."' ";       
        $strSQL .= "   AND ss.month_period = '".$monthPeriod."' ";       
        $strSQL .= $strFilter;
        $strSQL .= "    ORDER BY ss.name  ;";    

        $query = $this->db->query($strSQL)->result_array();  

        $boldFont = [
            'font' => [
                'bold' => true
                // 'color' => ['argb' => '0000FF'],
            ],
        ];

        $totalStyle = [
            'font' => [
                'bold' => true,
                'color' => ['argb' => '0000FF'],
            ],
        ];

        $allBorderStyle = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $outlineBorderStyle = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $topBorderStyle = [
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $bottomBorderStyle = [
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        /* AUTO WIDTH */     
        foreach(range('B','J') as $columnID)
        {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }          

        // Nama Field Baris Pertama
        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'PT Sangati Soerya Sejahtera');
        // $spreadsheet->getActiveSheet()->mergeCells("A1:B1");
        $spreadsheet->getActiveSheet()->getStyle("A1:B1")->getFont()->setBold(true)->setSize(13);

        $spreadsheet->getActiveSheet()
            ->setCellValue('B1', 'PT Sangati Soerya Sejahtera')     
            ->setCellValue('A2', 'INVOICE TABLE : PT. Pontil Indonesia')     
            ->setCellValue('A4', 'SUMMARY REIMBURSEMENT PT PONTIL - PAPUA/TIMIKA ')     
            ->setCellValue('A5', 'Period      : ')     
            ->setCellValue('A6', 'Contract No : ');   

        $spreadsheet->getActiveSheet()
            ->mergeCells('A1:J1')  
            ->mergeCells('A2:J2')  
            ->mergeCells('A4:J4')  
            ->mergeCells('A5:J5')  
            ->mergeCells('A6:J6');  

        $spreadsheet->getActiveSheet()->getStyle('A1:J6')->applyFromArray($center);

        /* SET HEADER BG COLOR*/
        $spreadsheet->getActiveSheet()->getStyle('A8:J10')
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B'); 

        $spreadsheet->getActiveSheet()
            ->mergeCells("A8:A9")
            ->mergeCells("B8:B9")
            ->mergeCells("C8:C9")
            ->mergeCells("D8:D9")
            ->mergeCells("E8:E9")
            ->mergeCells("F8:F9")
            ->mergeCells("G8:G9")
            ->mergeCells("H8:H9")
            ->mergeCells("I8:I9")
            ->mergeCells("J8:J9");
        
        $spreadsheet->getActiveSheet()->getStyle("A8:A9")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("A10:J10")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("B8:B9")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("C8:C9")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("D8:D9")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("E8:E9")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("F8:F9")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("G8:G9")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("H8:H9")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("I8:I9")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("J8:J9")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("B8:B9")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("D8:D9")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("E8:E9")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("F8:F9")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("G8:G9")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("H8:H9")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("I8:I9")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("J8:J9")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('A8:J10')->applyFromArray($allBorderStyle);

        $spreadsheet->getActiveSheet()
                ->setCellValue('A8', 'NO')
                ->setCellValue('A10', 'A')
                ->setCellValue('B8', 'NAME')
                ->setCellValue('B10', 'B')
                ->setCellValue('C8', 'POSITION')
                ->setCellValue('C10', 'C')
                ->setCellValue('D8', 'SALARY PERIOD')
                ->setCellValue('D10', 'D')
                ->setCellValue('E8', 'ECONOMIC ALLOWANCE')
                ->setCellValue('E10', 'E')
                ->setCellValue('F8', 'INCENTIVE BONUS')
                ->setCellValue('F10', 'F')
                ->setCellValue('G8', 'SHIFT BONUS')
                ->setCellValue('G10', 'G')
                ->setCellValue('H8', 'WORK ADJUSTMENT')
                ->setCellValue('H10', 'J')
                ->setCellValue('I8', 'PRODUCTION BONUS')
                ->setCellValue('I10', 'I')
                ->setCellValue('J8', 'TOTAL REIMBURSEMENT')
                ->setCellValue('J10', 'J')
                ;

        $monthName = '';
        switch ($monthPeriod) {
            case "01":
                $monthName = 'January';
                break;
            case "02":
                $monthName = 'February';
                break;
            case "03":
                $monthName = 'March';
                break;
            case "04":
                $monthName = 'April';
                break;
            case "05":
                $monthName = 'May';
                break;
            case "06":
                $monthName = 'June';
                break;
            case "07":
                $monthName = 'July';
                break;
            case "08":
                $monthName = 'August';
                break;
            case "09":
                $monthName = 'September';
                break;
            case "10":
                $monthName = 'October';
                break;
            case "11":
                $monthName = 'November';
                break;
            case "12":
                $monthName = 'December';
                break;    
            default:
                $monthName = '';
        }

        $periodTmp = $monthName."-".substr($yearPeriod, 2, 2); 
        
        $rowIdx = 11;
        $startIdx = $rowIdx; 
        $rowNo = 0;
        foreach ($query as $row) {
            $rowIdx++;
            $rowNo++;

            $total = $row['allowance_economy'] + $row['incentive_bonus'] + $row['shift_bonus'] + $row['workday_adj'] + $row['production_bonus']; 
            
            $spreadsheet->getActiveSheet()
                ->setCellValue('A'.$rowIdx, $rowNo)
                ->setCellValue('B'.$rowIdx, $row['name'])
                ->setCellValue('C'.$rowIdx, $row['job_desc'])
                ->setCellValue('D'.$rowIdx, $periodTmp)
                ->setCellValue('E'.$rowIdx, $row['allowance_economy'])
                ->setCellValue('F'.$rowIdx, $row['incentive_bonus'])
                ->setCellValue('G'.$rowIdx, $row['shift_bonus'])
                ->setCellValue('H'.$rowIdx, $row['workday_adj'])
                ->setCellValue('I'.$rowIdx, $row['production_bonus'])
                ->setCellValue('J'.$rowIdx, $total);
            
            if($rowIdx % 2 == 1)
            {
                $spreadsheet->getActiveSheet()->getStyle('A'.$rowIdx.':J'.$rowIdx)
                    ->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB('EAEBAF');             
            } 
        } /* end foreach ($query as $row) */

        $spreadsheet->getActiveSheet()
                ->setCellValue('C'.($rowIdx+2), 'TOTAL')
                ->setCellValue('J'.($rowIdx+2), '=SUM(J'.$startIdx.':J'.$rowIdx.')');

        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":J".($rowIdx+2))->getFont()->setBold(true)->setSize(12);
        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":J".($rowIdx+2))->applyFromArray($outlineBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":J".($rowIdx+2))
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B');

        /* SET NUMBERS FORMAT*/
        $spreadsheet->getActiveSheet()->getStyle('E9:J'.($rowIdx+2))->getNumberFormat()->setFormatCode('#,##0.00');       
        /* DIRECTOR SIGN */
        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+5).":A".($rowIdx+5))->getFont()->setBold(true)->setSize(11);
        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+8).":A".($rowIdx+8))->getFont()->setBold(true)->setSize(11)->setUnderline(true);
        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+9).":A".($rowIdx+9))->getFont()->setName('Comic Sans MS')->setSize(10);

        $spreadsheet->getActiveSheet()
            ->setCellValue('A'.($rowIdx+5), 'PT Sangati Soerya Sejahtera')
            ->setCellValue('A'.($rowIdx+8), 'HERI SUSANTO')
            ->setCellValue('A'.($rowIdx+9), 'Direktur');

        /* FRAME */
        unset($allBorderStyle);
        unset($center);
        unset($right);
        unset($left);
        
        $spreadsheet->setActiveSheetIndex(0);

        $str = 'PTLSumReimburse';
        $fileName = preg_replace('/\s+/', '', $str);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.Xlsx"');
        // header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        /* BY COMPOSER */
        // $writer = new Xlsx($spreadsheet);
        /* OFFLINE/ BY COPY EXCEL FOLDER  */
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit(0);      
    }
    /* END PONTIL SUMMARY REIMBURSEMENT EXPORT */

    public function getDataList($pt, $year, $month)
    {

        $this->db->select('*');
        $this->db->from($this->db->database.'.trn_salary_slip');
        $this->db->where('client_name', $pt);
        if( ($pt == 'Machmahon_Sumbawa' || $pt == "Pontil_Sumbawa") )
        {
            if(isset($dept)){
                $dept = $_POST['dept'];
                $this->db->where('dept', $this->security->xss_clean($dept));
            }
        }

        $this->db->where('year_period', $this->security->xss_clean($year));
        $this->db->where('month_period', $this->security->xss_clean($month));
        $this->db->order_by('name',"asc");
        $query = $this->db->get()->result_array();
        // echo $this->db->last_query(); exit(0);
        /*return json_encode($query);*/
        $myData = array();
        foreach ($query as $key => $row) {
            $myData[] = array(
                $row['salary_slip_id'],         
                $pt,         
                $row['name'],         
                // $row['production_bonus'],         
                // $row['workday_adj'],         
                // $row['adjust_in'],         
                // $row['adjust_out'],         
                $row['dept'],         
                $row['position']         
                // $row['attendance_bonus'],         
                // $row['other_allowance1'],         
                // $row['other_allowance2'],         
                // $row['cc_payment'],         
                // $row['thr'],         
                // $row['debt_burden'],
                // $row['debt_explanation']
            );            
        }  
        echo json_encode($myData);   
    }

}

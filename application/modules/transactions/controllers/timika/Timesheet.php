<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    // Load library phpspreadsheet
    require('./vendor/autoload.php');
    use PhpOffice\PhpSpreadsheet\Helper\Sample;
    use PhpOffice\PhpSpreadsheet\IOFactory;
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    // End load library phpspreadsheet

/**
 * 
 */
class Timesheet extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_timesheet'); 
        $this->load->model('M_overtime'); 
        // $this->load->model('masters/M_salary'); 
        $this->load->model('masters/M_allowance'); 
        $this->load->model('masters/M_overtime'); 
        // $this->load->model('M_salary_slip'); 
        $this->load->model('transactions/M_salary_slip');
        $this->load->model('masters/M_mst_salary', 'M_salary'); 
        $this->load->model('masters/M_process_closing'); 
        $this->load->model('masters/M_payroll_config'); 
	}

	public function upload()
    {
        $originalName = $_FILES['file']['name'];  
        $shortName = substr($originalName, 0, 6);
        // echo $shortName; exit();
        /* File's Year */
        $year = substr($originalName, 6, 4);
        $month = substr($originalName, 10, 2);
        $clientName = '';
        switch ($shortName) {
            case 'PTLTMK':
                $clientName = 'Pontil_Timika';
                break;
            case 'RPTTMK':
                $clientName = 'Redpath_Timika';
                break;
            default:
                $clientName = '';
                break;
        }


        if($clientName == '')
        {
            // $this->session->set_flashdata('rosterMsg', 'Client has not registered');
            echo 'Client has not registered';
            exit(0);
            redirect('content/detail/ird');
        }


        $dataCount = $this->M_process_closing->getCountByClientPeriod($clientName, $year, $month);
        if($dataCount >= 1)
        {
            // $this->session->set_flashdata('rosterMsg', 'Data with the same period has been uploaded before');
            echo 'Data with the same period has been uploaded before';
            exit(0);
            redirect('content/detail/ird');
        }
        // echo $dataCount;
        $fileDir = realpath(APPPATH.'../uploads/'); //buat folder dengan nama uploads di root folder 
        // $fileDir = '../uploads/'; //buat folder dengan nama uploads di root folder 
        // $fileDir = $_SERVER['DOCUMENT_ROOT']; //buat folder dengan nama uploads di root folder 
        // echo FCPATH; exit(0);
        $fileDir = FCPATH."/uploads/";

        $fileName = time().$originalName;         
        // $config['upload_path'] = './assets/'; //buat folder dengan nama assets di root folder
        $config['upload_path'] = $fileDir; 
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 10000;         

        $this->load->library('upload');
        $this->upload->initialize($config);         
        // $this->upload->do_upload('file');

        if(! $this->upload->do_upload('file'))
        {
            $this->upload->display_errors();
        }
             
        $media = $this->upload->data('file');
        /* WINDOWS PATH */
        // $inputFileName = $fileDir.'\\'.$fileName;
        $inputFileName = $fileDir.$fileName;
        // $inputFileName = $fileDir.'/'.$fileName;
        // echo $inputFileName; exit(0);
        try {
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                // $reader->setReadFilter( new MyReadFilter() );
                $objPHPExcel = $reader->load($inputFileName);

        } catch(Exception $e) {
            die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
        }

        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
        $dataArr = array();

        $failedNames = array();
        $arrIdx = 0;
          
        $dataCount = 0;
        $noBiodataId = 0;
        /* Start Of TRANSACTION */  
        $this->db->trans_begin();
        if($clientName == 'Redpath_Timika')
        {                                   
            $dataCount = 0;
            $noBiodataId = 0;
            for ($row = 3; $row <= $highestRow; $row++)  //  Read a row of data into an array
            {                     
                 $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                            NULL,
                                            TRUE,
                                            FALSE);


                 $nie = $rowData[0][3]; 
                 $nie = preg_replace('/[\r\n]+/','', $nie);
                 $nie = trim($nie);
                 
                 if($nie != '')
                 {
                     $this->M_timesheet->resetValues();
                     $this->M_timesheet->setRosterId($this->security->xss_clean($this->M_timesheet->GenerateNumber()));                         
                     $salaryRow = $this->M_salary->loadBioIdByNie($nie);
                     $bioId = $salaryRow['bio_rec_id'];

                     /* CLEAR DATA BY ID, CLIENT NAME, YEAR AND MONTH */ 
                     $this->M_salary_slip->deleteByIdPeriod($bioId, $clientName, $year, $month);
                     $str = $this->M_overtime->deleteByIdPeriod($bioId, $clientName, $year, $month);      
                     $this->M_timesheet->deleteByIdPeriod($bioId, $clientName, $year, $month); 

                     

                     // $this->M_MstBioRec->loadByNie($nie);
                     // $bioId = $this->M_MstBioRec->getBioRecId();
                     if($bioId == ""){
                        $noBiodataId++;
                        
                        /* GET ERROR DATA */
                        $failedNames[$arrIdx] = $rowData[0][1];
                        $arrIdx++;

                     }
                     else{
                        $this->M_timesheet->setBioRecId($this->security->xss_clean($bioId));
                     }

                     $this->M_timesheet->setNie($nie);

                     $tData = $rowData[0][37]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     $this->M_timesheet->setRosterBase($this->security->xss_clean($tData));
                     $tData = $rowData[0][38]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     $this->M_timesheet->setRosterFormat($this->security->xss_clean($tData));
                     // $this->M_timesheet->setRosterBase($this->security->xss_clean($rosterBase));
                     // $this->M_timesheet->setRosterFormat($this->security->xss_clean($rosterFormat));
                     $this->M_timesheet->setEmployeeName($this->security->xss_clean($rowData[0][1]));
                     $this->M_timesheet->setClientName($this->security->xss_clean($clientName));

                     $tData = $rowData[0][4]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     $this->M_timesheet->setDept($this->security->xss_clean($tData));
                     
                     $currFullDate = GetCurrentDate();
                     $curMonth = $currFullDate['CurrentYear'];
                     $curYear = $currFullDate['CurrentMonth'];
                     $this->M_timesheet->setMonthProcess($this->security->xss_clean($month));
                     $this->M_timesheet->setYearProcess($this->security->xss_clean($year));
                     
                     $attendCount = 0;
                     
                     $tData = $rowData[0][6]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if($tData == null || trim($tData) == ""){
                        $this->M_timesheet->setD01($this->security->xss_clean("U")); /* Kolom 6 Pada Excel */
                     }else{                            
                        /*$tData = $rowData[0][6]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));*/
                        $this->M_timesheet->setD01($this->security->xss_clean($tData)); /* Kolom 6 Pada Excel */
                        // if( is_numeric($tData) && $tData > 0 ){
                        //     $attendCount++;
                        // }
                     }

                     $tData = $rowData[0][7]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if($tData == null || trim($tData) == ""){
                        $this->M_timesheet->setD02($this->security->xss_clean("U")); /* Kolom 7 Pada Excel */
                     }else{                            
                        $this->M_timesheet->setD02($this->security->xss_clean($tData)); /* Kolom 7 Pada Excel */
                        // if( is_numeric($tData) && $tData > 0 ){
                        //     $attendCount++;
                        // }
                     }

                     $tData = $rowData[0][8]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));   
                     if($tData == null || trim($tData) == ""){
                        $this->M_timesheet->setD03("U");
                     }else{
                        
                        $this->M_timesheet->setD03($this->security->xss_clean($tData));
                        // if( is_numeric($tData) && $tData > 0 ){
                        //     $attendCount++;
                        // }
                     }

                     $tData = $rowData[0][9]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));   
                     if($tData == null || trim($tData) == ""){
                        $this->M_timesheet->setD04($this->security->xss_clean("U"));
                     }else{                            
                        $this->M_timesheet->setD04($this->security->xss_clean($tData));
                        // if( is_numeric($tData) && $tData > 0 ){
                        //     $attendCount++;
                        // }
                     }

                     $tData = $rowData[0][10]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));   
                     if($tData == null || trim($tData) == ""){
                        $this->M_timesheet->setD05($this->security->xss_clean("U"));
                     }else{                            
                        $this->M_timesheet->setD05($this->security->xss_clean($tData));
                        // if( is_numeric($tData) && $tData > 0 ){
                        //     $attendCount++;
                        // }
                     }                        

                     $tData = $rowData[0][11]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if($tData == null || trim($tData) == ""){
                        $this->M_timesheet->setD06($this->security->xss_clean("U"));
                     }else{                            
                        $this->M_timesheet->setD06($this->security->xss_clean($tData));
                        // if( is_numeric($tData) && $tData > 0 ){
                        //     $attendCount++;
                        // }
                     }

                     $tData = $rowData[0][12]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if($tData == null || trim($tData) == ""){
                        $this->M_timesheet->setD07($this->security->xss_clean("U"));
                     }else{                            
                        $this->M_timesheet->setD07($this->security->xss_clean($tData));
                        // if( is_numeric($tData) && $tData > 0 ){
                        //     $attendCount++;
                        // }
                     }

                     $tData = $rowData[0][13]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if($tData == null || trim($tData) == ""){
                        $this->M_timesheet->setD08($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD08($this->security->xss_clean($tData));
                        // if( is_numeric($tData) && $tData > 0 ){
                        //     $attendCount++;
                        // }
                     }

                     $tData = $rowData[0][14]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if($tData == null || trim($tData) == ""){
                        $this->M_timesheet->setD09($this->security->xss_clean("U"));
                     }else{                            
                        $this->M_timesheet->setD09($this->security->xss_clean($rowData[0][14]));
                        // if( is_numeric($tData) && $tData > 0 ){
                        //     $attendCount++;
                        // }
                     }
                     
                     $tData = $rowData[0][15]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if($tData == null || trim($tData) == ""){
                        $this->M_timesheet->setD10($this->security->xss_clean("U"));
                     }else{                            
                        $this->M_timesheet->setD10($this->security->xss_clean($tData));
                        // if( is_numeric($tData) && $tData > 0 ){
                        //     $attendCount++;
                        // }
                     }

                     $tData = $rowData[0][16]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if($tData == null || trim($tData) == ""){
                        $this->M_timesheet->setD11($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD11($this->security->xss_clean($tData));
                        // if( is_numeric($tData) && $tData > 0 ){
                        //     $attendCount++;
                        // }
                     }

                     $tData = $rowData[0][17]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if($tData == null || trim($tData) == ""){
                        $this->M_timesheet->setD12($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD12($this->security->xss_clean($tData));
                        // if( is_numeric($tData) && $tData > 0 ){
                        //     $attendCount++;
                        // }
                     }

                     $tData = $rowData[0][18]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if($tData == null || trim($tData) == ""){
                        $this->M_timesheet->setD13($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD13($this->security->xss_clean($tData));
                        // if( is_numeric($tData) && $tData > 0 ){
                        //     $attendCount++;
                        // }
                     }

                     $tData = $rowData[0][19]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if($tData == null || trim($tData) == ""){
                        $this->M_timesheet->setD14($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD14($this->security->xss_clean($tData));
                        // if( is_numeric($tData) && $tData > 0 ){
                        //     $attendCount++;
                        // }
                     }

                     $tData = $rowData[0][20]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if($tData == null || trim($tData) == ""){
                        $this->M_timesheet->setD15($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD15($this->security->xss_clean($tData));
                        // if( is_numeric($tData) && $tData > 0 ){
                        //     $attendCount++;
                        // }
                     }

                     $tData = $rowData[0][21]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if($tData == null || trim($tData) == ""){
                        $this->M_timesheet->setD16($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD16($this->security->xss_clean($tData));
                        // if( is_numeric($tData) && $tData > 0 ){
                        //     $attendCount++;
                        // }
                     }

                     $tData = $rowData[0][22]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if($tData == null || trim($tData) == ""){
                        $this->M_timesheet->setD17($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD17($this->security->xss_clean($tData));
                        // if( is_numeric($tData) && $tData > 0 ){
                        //     $attendCount++;
                        // }
                     }

                     $tData = $rowData[0][23]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if($tData == null || trim($tData) == ""){
                        $this->M_timesheet->setD18($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD18($this->security->xss_clean($tData));
                        // if( is_numeric($tData) && $tData > 0 ){
                        //     $attendCount++;
                        // }
                     }
                     
                     $tData = $rowData[0][24]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if($tData == null || trim($tData) == ""){
                        $this->M_timesheet->setD19($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD19($this->security->xss_clean($tData));
                        // if( is_numeric($tData) && $tData > 0 ){
                        //     $attendCount++;
                        // }
                     }

                     $tData = $rowData[0][25]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if($tData == null || trim($tData) == ""){
                        $this->M_timesheet->setD20($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD20($this->security->xss_clean($tData));
                        // if( is_numeric($tData) && $tData > 0 ){
                        //     $attendCount++;
                        // }
                     }

                     $tData = $rowData[0][26]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if($tData == null || trim($tData) == ""){
                        $this->M_timesheet->setD21($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD21($this->security->xss_clean($tData));
                        // if( is_numeric($tData) && $tData > 0 ){
                        //     $attendCount++;
                        // }
                     }

                     $tData = $rowData[0][27]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if($tData == null || trim($tData) == ""){
                        $this->M_timesheet->setD22($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD22($this->security->xss_clean($tData));
                        // if( is_numeric($tData) && $tData > 0 ){
                        //     $attendCount++;
                        // }
                     }

                     $tData = $rowData[0][28]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if($tData == null || trim($tData) == ""){
                        $this->M_timesheet->setD23($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD23($this->security->xss_clean($tData));
                        // if( is_numeric($tData) && $tData > 0 ){
                        //     $attendCount++;
                        // }
                     }                         

                     $tData = $rowData[0][29]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if($tData == null || trim($tData) == ""){
                        $this->M_timesheet->setD24($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD24($this->security->xss_clean($tData));
                        // if( is_numeric($tData) && $tData > 0 ){
                        //     $attendCount++;
                        // }
                     }

                     $tData = $rowData[0][30]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if($tData == null || trim($tData) == ""){
                        $this->M_timesheet->setD25($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD25($this->security->xss_clean($tData));
                        // if( is_numeric($tData) && $tData > 0 ){
                        //     $attendCount++;
                        // }
                     }

                     $tData = $rowData[0][31]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if($tData == null || trim($tData) == ""){
                        $this->M_timesheet->setD26($this->security->xss_clean("U"));
                     }else{                            
                        $this->M_timesheet->setD26($this->security->xss_clean($tData));
                        // if( is_numeric($tData) && $tData > 0 ){
                        //     $attendCount++;
                        // }
                     }

                     $tData = $rowData[0][32]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if($tData == null || trim($tData) == ""){
                        $this->M_timesheet->setD27($this->security->xss_clean($rowData[0][32]));
                     }else{
                        $this->M_timesheet->setD27($this->security->xss_clean($tData));
                        // if( is_numeric($tData) && $tData > 0 ){
                        //     $attendCount++;
                        // }
                     }

                     $tData = $rowData[0][33]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if($tData == null || trim($tData) == ""){
                        $this->M_timesheet->setD28($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD28($this->security->xss_clean($tData));
                        // if( is_numeric($tData) && $tData > 0 ){
                        //     $attendCount++;
                        // }
                     }

                     $tData = $rowData[0][34]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if($tData == null || trim($tData) == ""){
                        $this->M_timesheet->setD29($this->security->xss_clean("U"));
                     }else{                            
                        $this->M_timesheet->setD29($this->security->xss_clean($tData));
                        // if( is_numeric($tData) && $tData > 0 ){
                        //     $attendCount++;
                        // }
                     }

                     $tData = $rowData[0][35]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if($tData == null || trim($tData) == ""){
                        $this->M_timesheet->setD30($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD30($this->security->xss_clean($tData));
                        // if( is_numeric($tData) && $tData > 0 ){
                        //     $attendCount++;
                        // }
                     }
                     
                     $tData = $rowData[0][36]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if($tData == null || trim($tData) == ""){
                        $this->M_timesheet->setD31($this->security->xss_clean("U"));
                     }else{                            
                        $this->M_timesheet->setD31($this->security->xss_clean($tData));
                        // if( is_numeric($tData) && $tData > 0 ){
                        //     $attendCount++;
                        // }
                     }

                     $this->M_timesheet->setIsActive($this->security->xss_clean('1'));
                     $this->M_timesheet->setPicProcess($this->security->xss_clean($this->session->userdata('hris_user_id')));
                     $currFullDate = GetCurrentDate();
                     $curDateTime = $currFullDate['CurrentDateTime'];
                     $this->M_timesheet->setProcessTime($this->security->xss_clean($curDateTime));

                     $this->M_timesheet->insert();
                     $dataCount++;
                       
                 }                 
    
                                     
            } // for ($row = 2; $row <= $highestRow; $row++)               
            
        } //if $clientName = 'Redpath_Timika' 
        else if($clientName == 'Pontil_Timika')
        {                                   
            $dataCount = 0;
            $noBiodataId = 0;
            for ($row = 2; $row <= $highestRow; $row++)  //  Read a row of data into an array
            {
                 
                 $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                            NULL,
                                            TRUE,
                                            FALSE);
                 $this->load->model('M_timesheet'); 
                 $nie = $rowData[0][1];
                 $nie = preg_replace('/[\r\n]+/','', $nie);
                 $nie = trim($nie);
                 if($nie != '')
                 {
                     $this->M_timesheet->resetValues();
                     $this->M_timesheet->setRosterId($this->security->xss_clean($this->M_timesheet->GenerateNumber()));
                     $salaryRow = $this->M_salary->loadBioIdByNie($nie);
                     $bioId = $salaryRow['bio_rec_id'];

                     /* CLEAR DATA BY ID, CLIENT NAME, YEAR AND MONTH */   
                     $this->M_salary_slip->deleteByIdPeriod($bioId, $clientName, $year, $month);
                     $this->M_overtime->deleteByIdPeriod($bioId, $clientName, $year, $month);
                     // $this->M_TrnAttendance->deleteByIdPeriod($bioId, $clientName, $year, $month);
                     $this->M_timesheet->deleteByIdPeriod($bioId, $clientName, $year, $month); 
                     
                     if($bioId == "" || $bioId == null){
                        $noBiodataId++;

                        /* GET ERROR DATA */
                        $failedNames[$arrIdx] = $rowData[0][2];
                        $arrIdx++;

                     }else{
                        $this->M_timesheet->setBioRecId($this->security->xss_clean($bioId));
                     }
                     
                     $this->M_timesheet->setNie($rowData[0][1]);
                     // $this->M_timesheet->setRosterBase($this->security->xss_clean($rosterBase));
                     // $this->M_timesheet->setRosterFormat($this->security->xss_clean($rosterFormat));
                     $tData = $rowData[0][34]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     $this->M_timesheet->setRosterBase($this->security->xss_clean($tData));

                     $tData = $rowData[0][35]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     $this->M_timesheet->setRosterFormat($this->security->xss_clean($tData));
                     
                     $this->M_timesheet->setClientName($this->security->xss_clean($clientName));
                     $this->M_timesheet->setEmployeeName($this->security->xss_clean($rowData[0][2]));
                     $myCurrentDate = GetCurrentDate();
                     $curMonth = $myCurrentDate['CurrentMonth'];
                     $curYear = $myCurrentDate['CurrentYear'];
                     $this->M_timesheet->setMonthProcess($this->security->xss_clean($month));
                     $this->M_timesheet->setYearProcess($this->security->xss_clean($year));

                     $attendCount = 0;

                     $tData = $rowData[0][3]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_timesheet->setD01($this->security->xss_clean("U"));   
                     }else{
                        $this->M_timesheet->setD01($this->security->xss_clean($tData));
                        // if( (is_numeric($rowData[0][3]) && $rowData[0][3] > 0) || ($rowData[0][3] == "V") || $rowData[0][3] = "v" ){
                        //     $attendCount++;
                        // }
                     }
                     
                     $tData = $rowData[0][4]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_timesheet->setD02($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD02($this->security->xss_clean($tData));
                        // if( (is_numeric($rowData[0][4]) && $rowData[0][4] > 0) || ($rowData[0][4] == "V") || $rowData[0][4] = "v" ){
                        //     $attendCount++;
                        // }
                     }
                     
                     $tData = $rowData[0][5]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_timesheet->setD03($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD03($this->security->xss_clean($tData));
                        // if( (is_numeric($rowData[0][5]) && $rowData[0][5] > 0) || ($rowData[0][5] == "V") || $rowData[0][5] = "v" ){
                        //     $attendCount++;
                        // }
                     }
                     
                     $tData = $rowData[0][6]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_timesheet->setD04($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD04($this->security->xss_clean($tData));
                        // if( (is_numeric($rowData[0][6]) && $rowData[0][6] > 0) || ($rowData[0][6] == "V") || $rowData[0][6] = "v" ){
                        //     $attendCount++;
                        // }
                     }

                     $tData = $rowData[0][7]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_timesheet->setD05($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD05($this->security->xss_clean($tData));
                        // if( (is_numeric($rowData[0][7]) && $rowData[0][7] > 0) || ($rowData[0][7] == "V") || $rowData[0][7] = "v" ){
                        //     $attendCount++;
                        // }
                     }
                     
                     $tData = $rowData[0][8]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_timesheet->setD06($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD06($this->security->xss_clean($tData));
                        // if( (is_numeric($rowData[0][8]) && $rowData[0][8] > 0) || ($rowData[0][8] == "V") || $rowData[0][8] = "v" ){
                        //     $attendCount++;
                        // }
                     }

                     $tData = $rowData[0][9]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if( ($tData == null) || ($tData == "") ){
                        $this->M_timesheet->setD07($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD07($this->security->xss_clean($tData));
                        // if( (is_numeric($rowData[0][9]) && $rowData[0][9] > 0) || ($rowData[0][9] == "V") || $rowData[0][9] = "v" ){
                        //     $attendCount++;
                        // }
                     }

                     $tData = $rowData[0][10]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_timesheet->setD08($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD08($this->security->xss_clean($tData));
                        // if( is_numeric($rowData[0][10]) && $rowData[0][10] > 0 ){
                        //     $attendCount++;
                        // }
                     }
                     
                     $tData = $rowData[0][11]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if( ($tData == null) || ($tData == "") ){
                        $this->M_timesheet->setD09($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD09($this->security->xss_clean($tData));
                        // if( is_numeric($rowData[0][11]) && $rowData[0][11] > 0 ){
                        //     $attendCount++;
                        // }  
                     }
                     
                     $tData = $rowData[0][12]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_timesheet->setD10($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD10($this->security->xss_clean($tData));
                        // if( is_numeric($rowData[0][12]) && $rowData[0][12] > 0 ){
                        //     $attendCount++;
                        // }
                     }

                     $tData = $rowData[0][13]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_timesheet->setD11($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD11($this->security->xss_clean($tData));
                        // if( is_numeric($rowData[0][14]) && $rowData[0][14] > 0 ){
                        //     $attendCount++;
                        // }
                     }

                     $tData = $rowData[0][14]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_timesheet->setD12($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD12($this->security->xss_clean($rowData[0][14]));
                        // if( is_numeric($rowData[0][14]) && $rowData[0][14] > 0 ){
                        //     $attendCount++;
                        // }
                     }
                     
                     $tData = $rowData[0][15]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_timesheet->setD13($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD13($this->security->xss_clean($tData));
                        // if( is_numeric($rowData[0][15]) && $rowData[0][15] > 0 ){
                        //     $attendCount++;
                        // }
                     }

                     $tData = $rowData[0][16]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_timesheet->setD14($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD14($this->security->xss_clean($tData));
                        // if( is_numeric($rowData[0][16]) && $rowData[0][16] > 0 ){
                        //     $attendCount++;
                        // }
                     }

                     $tData = $rowData[0][17]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_timesheet->setD15($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD15($this->security->xss_clean($tData));
                        // if( is_numeric($rowData[0][17]) && $rowData[0][17] > 0 ){
                        //     $attendCount++;
                        // }
                     }

                     $tData = $rowData[0][18]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_timesheet->setD16($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD16($this->security->xss_clean($tData));
                        // if( is_numeric($rowData[0][18]) && $rowData[0][18] > 0 ){
                        //     $attendCount++;
                        // }
                     }
                     
                     $tData = $rowData[0][19]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_timesheet->setD17($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD17($this->security->xss_clean($tData));
                        // if( is_numeric($rowData[0][19]) && $rowData[0][19] > 0 ){
                        //     $attendCount++;
                        // }
                     }

                     $tData = $rowData[0][20]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_timesheet->setD18($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD18($this->security->xss_clean($tData));
                        // if( is_numeric($rowData[0][20]) && $rowData[0][20] > 0 ){
                        //     $attendCount++;
                        // }
                     }
                     
                     $tData = $rowData[0][21]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_timesheet->setD19($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD19($this->security->xss_clean($tData));
                        // if( is_numeric($rowData[0][21]) && $rowData[0][21] > 0 ){
                        //     $attendCount++;
                        // }
                     }
                     
                     $tData = $rowData[0][22]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_timesheet->setD20($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD20($this->security->xss_clean($tData));
                        // if( is_numeric($rowData[0][22]) && $rowData[0][22] > 0 ){
                        //     $attendCount++;
                        // }
                     }
                     
                     $tData = $rowData[0][23]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if( ($tData == null) || ($tData == "") ){
                        $this->M_timesheet->setD21($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD21($this->security->xss_clean($tData));
                        // if( is_numeric($rowData[0][23]) && $rowData[0][23] > 0 ){
                        //     $attendCount++;
                        // }
                     }
                     
                     $tData = $rowData[0][24]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_timesheet->setD22($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD22($this->security->xss_clean($tData));
                        // if( is_numeric($rowData[0][24]) && $rowData[0][24] > 0 ){
                        //     $attendCount++;
                        // }
                     }
                     
                     $tData = $rowData[0][25]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_timesheet->setD23($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD23($this->security->xss_clean($tData));
                        // if( is_numeric($rowData[0][25]) && $rowData[0][25] > 0 ){
                        //     $attendCount++;
                        // }
                     }
                     
                     $tData = $rowData[0][26]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_timesheet->setD24($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD24($this->security->xss_clean($tData));
                        // if( is_numeric($rowData[0][26]) && $rowData[0][26] > 0 ){
                        //     $attendCount++;
                        // }
                     }
                     
                     $tData = $rowData[0][27]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_timesheet->setD25($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD25($this->security->xss_clean($tData));
                        // if( is_numeric($rowData[0][27]) && $rowData[0][27] > 0 ){
                        //     $attendCount++;
                        // }
                     }

                     $tData = $rowData[0][28]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_timesheet->setD26($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD26($this->security->xss_clean($tData));
                        // if( is_numeric($rowData[0][28]) && $rowData[0][28] > 0 ){
                        //     $attendCount++;
                        // }
                     }

                     $tData = $rowData[0][29]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_timesheet->setD27($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD27($this->security->xss_clean($rowData[0][29]));
                        // if( is_numeric($rowData[0][29]) && $rowData[0][29] > 0 ){
                        //     $attendCount++;
                        // }
                     }
                     
                     $tData = $rowData[0][30]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_timesheet->setD28($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD28($this->security->xss_clean($tData));
                        // if( is_numeric($rowData[0][30]) && $rowData[0][30] > 0 ){
                        //     $attendCount++;
                        // }
                     }
                     
                     $tData = $rowData[0][31]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_timesheet->setD29($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD29($this->security->xss_clean($tData));
                        // if( is_numeric($rowData[0][31]) && $rowData[0][31] > 0 ){
                        //     $attendCount++;
                        // }
                     }
                     
                     $tData = $rowData[0][32]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_timesheet->setD30($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD30($this->security->xss_clean($tData));
                        // if( is_numeric($rowData[0][32]) && $rowData[0][32] > 0 ){
                        //     $attendCount++;
                        // }
                     }
                     
                     $tData = $rowData[0][33]; 
                     $tData = preg_replace('/[\r\n]+/','', $tData);
                     $tData = trim(strtoupper($tData));
                     if( ($tData == null) || (trim($tData) == "") ){
                        $this->M_timesheet->setD31($this->security->xss_clean("U"));
                     }else{
                        $this->M_timesheet->setD31($this->security->xss_clean($tData));
                        // if( is_numeric($rowData[0][33]) && $rowData[0][33] > 0 ){
                        //     $attendCount++;
                        // }
                     }

                     $this->M_timesheet->setIsActive($this->security->xss_clean('1'));
                     $this->M_timesheet->setPicProcess($this->security->xss_clean($this->session->userdata('hris_user_id')));
                     $myCurrentDate = GetCurrentDate();
                     $curDateTime = $myCurrentDate['CurrentDateTime'];
                     $this->M_timesheet->setProcessTime($this->security->xss_clean($curDateTime));
                     // if($attendCount > 0)
                     // {
                    $this->M_timesheet->insert();
                    $dataCount++;
                     // }

                     /* START OVERTIME PROCESS */

                     /* END OVERTIME PROCESS */

                       
                 }                  
                
            } // for ($row = 2; $row <= $highestRow; $row++)               
            
        } //if $clientName = 'Pontil_Timika'

        /* End Of TRANSACTION */  
        // echo $shortName; exit();
        if ($this->db->trans_status() === FALSE)
        {
                $this->db->trans_rollback();
           // $this->session->set_flashdata('rosterMsg', 'Gagal Import Data, Cek Kembali Format File');
           $this->load->M_process_closing->resetValues();
           $this->load->M_timesheet->resetValues();     
           echo 'Import Data Failed, Please Check File Format';
        }
        else
        {
                $this->db->trans_commit();
           // $this->session->set_flashdata('rosterMsg', 'Import Data Berhasil : '.$dataCount.', Tanpa Biodata Id : '.$noBiodataId);
           echo 'Import Data Succeed : '.$dataCount.', No Biodata Id : '.$noBiodataId;
        }        
            
            // $this->session->set_flashdata('failedDatas', $failedNames);
            // redirect('content/detail/ird');
    }


    public function rosterProcess()
    {
        ini_set('max_execution_time', 400); //300 seconds = 5 minutes
        $biodataId = '';
        $clientName = '';
        $month = '0';
        $year = '0';
        $rosterMaster = '';
        $rosterColumn = '';
        $ot01Column = '';
        $ot02Column = '';
        $ot03Column = '';
        $ot04Column = '';
        $isAlpaColumn = '';
        $isPermitColumn = '';
        $isSickColumn = '';
        $isEmergencyColumn = '';
        $offColumn = '';
        $failMessage = "Process Failed";


        if(isset($_POST['biodataId']) && $this->security->xss_clean($_POST['biodataId']) == 'BySlipId')
        {
            /* SET BIODATA ID */
            $slipId = $this->security->xss_clean($_POST['slipId']);
            $this->M_salary_slip->getObjectById($slipId);            
            $biodataId = $this->M_salary_slip->getBioRecId();
            if($biodataId == ''){
                exit();
            }
        }        

        // if(isset($_POST['biodataId']))
        // {
        //     $biodataId = $this->security->xss_clean($_POST['biodataId']); 
        // }

        if(isset($_POST['clientName']))
        {
            $clientName = $this->security->xss_clean($_POST['clientName']); 
        }

        if(isset($_POST['monthPeriod']))
        {
            $month = $this->security->xss_clean($_POST['monthPeriod']); 
        }

        if(isset($_POST['yearPeriod']))
        {
            $year = $this->security->xss_clean($_POST['yearPeriod']); 
        }  
        /* DATA CHECKING IS HERE */
        $dataCount = $this->M_process_closing->getCountByClientPeriod($clientName, $year, $month);
        if($dataCount >= 1)
        {
            $failMessage = "Data with the same period has been processed before";
            echo $failMessage;
            exit(0);
        }

        $myDate = $year."-".$month."-"."01"; 
        $daysCountMonth = GetDbLastDayOfMonth($myDate);

        if($biodataId == "") {
            $data = $this->M_timesheet->loadByClientPeriod($clientName, $year, $month);
        } else {
            $data = $this->M_timesheet->loadByIdClientPeriod($biodataId, $clientName, $year, $month); 
        // echo $this->db->last_query(); exit();
        }
        /* NILAI LEMBUR PERJAM (NLP) = GAJI POKOK/173 */
        $tmp51 = 0;
        $tmp52 = 0;
        $tmp61 = 0;
        $tmp62 = 0;
        $tmp63 = 0;
        $startDay = 0;

        /* START TRANSACTION */
        // $this->db->trans_start();
        $this->db->trans_begin();
        /* Start Roster Redpath - Pontil */
        if($clientName == 'Redpath_Timika' || $clientName == 'Pontil_Timika') 
        {            
            $tmp = '';
            /* START FOREACH */
            foreach ($data as $row) 
            {
                $st = '';
                $workDay = '';
                $offDay = '';           
                $bioId = $row['bio_rec_id'];

                /* CLEAR DATA OVER TIME AND SALARY SLIP */
                $this->M_salary_slip->deleteByIdPeriod($bioId, $clientName, $year, $month);
        // echo "Hello"; exit(0);
                $this->M_overtime->deleteByIdPeriod($bioId, $clientName, $year, $month);
                $tmpRoster = $row['roster_format'];
                $tmpRosterBase = substr($row['roster_base'],0,1);
                $strNumRoster = '';
                $totalRoster = 0;
                $tmpRosterLength = strlen($tmpRoster);
                /* START GET DAYS TOTAL BY ROSTER */
                for ($i=0; $i < $tmpRosterLength; $i++) {
                    $tNumChar = substr($tmpRoster,$i,1);
                    /* START MAKE SURE DATA IS NUMBER */ 
                    if(is_numeric($tNumChar))
                    {
                        $strNumRoster .= $tNumChar;
                        $totalRoster += $tNumChar;
                    }
                    /* END MAKE SURE DATA IS NUMBER */ 
                }
                /* END GET DAYS TOTAL BY ROSTER */       

                if($daysCountMonth != $totalRoster)
                {
                    $failMessage = "NIK ".$bioId." : Jumlah Hari Tidak Cocok Antara Format Roster & File";
                }

                $rosterFormat = '';
                $z = 0;
                $rosterMasterCount = strlen($strNumRoster);
                $tNumberRoster = $strNumRoster;
                $dayIndex = 0;

                $offAttendCount = 0;

                $otCount1 = 0;
                $otCount2 = 0;
                $otCount3 = 0;
                $otCount4 = 0;
                
                $offCount = 0;
                $emergencyCount = 0;
                $sickCount = 0;
                $unpaidPermitCount = 0;
                $unpaidCount = 0;
                $attendCount = 0;
                $attendPHCount = 0; /* Attend in Public Holiday */
                $phCountInMonth = 0; /* Public Holiday Count */
                $vacationDate = '';
                $vacationCount = 0;
                
                $standByCount = 0;
                $paidPermitCount = 0;
                $paidVacation = 0;
                
                $unpaidDate = '';
                $otTmp = 0; 
                $wdCode = '';

                $sixthDayTmp = 0;                
                $this->M_overtime->resetValues();
                /* START SHIFT BREAKDOWN */
                for ($x=1; $x <= $rosterMasterCount; $x++) 
                {         
                    /* SHIFT GROUPING */           
                    if(($x % 2) == 1)
                    {
                        $rosterFormat = substr($tNumberRoster, $x-1, 2);
                    }
                    $x++;

                    $workDay = substr($rosterFormat, 0, 1);
                    $offDay = substr($rosterFormat, 1, 1);

                    /* OVERTIME VARIABLE  */
                    $otInOffCount1 = $_POST['otInOffCount'];
                    $otInOffCount2 = $otInOffCount1 + 1;
                    $otInOffCount3 = $otInOffCount2 + 1;

                    /* START GET WORK DAYS BY SHIFT */
                    for ($wd=1; $wd <= $workDay; $wd++) { 
                        $dayIndex++; 
                        if($dayIndex < 10){
                            $rosterColumn = 'd0'.$dayIndex; 
                            $ot01Column = 'setOt1D0'.$dayIndex;    
                            $ot02Column = 'setOt2D0'.$dayIndex;    
                            $ot03Column = 'setOt3D0'.$dayIndex;    
                            $ot04Column = 'setOt4D0'.$dayIndex;  
                            $isAlpaColumn = 'setIsAlpa0'.$dayIndex;  
                            $isPermitColumn = 'setIsPermit0'.$dayIndex;  
                            $isSickColumn = 'setIsSick0'.$dayIndex;  
                            $isEmergencyColumn = 'setIsEmergency0'.$dayIndex;  
                            $offColumn = 'setIsOff0'.$dayIndex;  
                        }
                        else {
                            $rosterColumn = 'd'.$dayIndex;       
                            $ot01Column = 'setOt1D'.$dayIndex;    
                            $ot02Column = 'setOt2D'.$dayIndex;    
                            $ot03Column = 'setOt3D'.$dayIndex;    
                            $ot04Column = 'setOt4D'.$dayIndex;    
                            $isAlpaColumn = 'setIsAlpa'.$dayIndex;  
                            $isPermitColumn = 'setIsPermit'.$dayIndex;  
                            $isSickColumn = 'setIsSick'.$dayIndex;  
                            $isEmergencyColumn = 'setIsEmergency'.$dayIndex;  
                            $offColumn = 'setIsOff'.$dayIndex;  
                        }      

                        $otTmp = trim($row[$rosterColumn]);                            

                        /* PUBLIC HOLIDAY */
                        $phCodeTmp = substr($otTmp,0,2);
                        $phHoursTmp = substr($otTmp,2,strlen($otTmp)-2);
                        $st .= $phCodeTmp; 
                        if( (strtoupper($phCodeTmp) == "PH") )
                        {
                            if( (is_numeric($phHoursTmp)) && ($phHoursTmp > 0) )
                            {
                                $attendPHCount++;
                            } 
                            $phCountInMonth++;
                        }

                        /* EXCLUDE OF DATA IN ARRAY WILL BE TREATED AS  */
                        $arrTmpCode = array('PH','SB','PV','PP','RO');
                        $arrOtTmp = array('A','U','I','S','V', 'E');
                        
                        if($clientName == 'Redpath_Timika'){
                            $arrOtTmp = array('A','U','I','S','V', 'E', 'D');
                        }

                        if( (!in_array( strtoupper($otTmp), $arrOtTmp)) && (!in_array( strtoupper($phCodeTmp), $arrTmpCode)) && !is_numeric($otTmp))
                        {
                            $tmp .= $otTmp;                             
                            $unpaidCount++;
                            if( ($workDay == '5') || (($workDay != '5') && ($workDay != '6') && ($tmpRosterBase = '5')) ){
                                $unpaidDate .= '5';
                            }
                            else if( ($workDay == '6') || (($workDay != '5') && ($workDay != '6') && ($tmpRosterBase = '6')) ){
                                $unpaidDate .= '6';                                
                            }
                        }


                        /* RESET OVERTIME PROPERTIES IN WORKDAY */
                        $this->M_overtime->$ot01Column(0);
                        $this->M_overtime->$ot02Column(0);
                        $this->M_overtime->$ot03Column(0);
                        $this->M_overtime->$ot04Column(0);                        
                                                
                        /* GET ATTEND COUNT SHIFT DAY */
                        if(is_numeric($otTmp) && $otTmp > 0)
                        {
                            $attendCount++;
                        }

                        /* START PUBLIC HOLIDAY  */                    
                        if( (strtoupper($phCodeTmp) == "PH") && (is_numeric($phHoursTmp)) && ($phHoursTmp > 0) )
                        {
                            if($phHoursTmp >= $otInOffCount1){
                                $this->M_overtime->$ot02Column($otInOffCount1);
                                $otCount2 += $otInOffCount1;
                            }else{
                                $this->M_overtime->$ot02Column($phHoursTmp);
                                $otCount2 += $phHoursTmp;
                            }

                            if($phHoursTmp > $otInOffCount1){
                                if($phHoursTmp < $otInOffCount2){
                                    $tVal = $otInOffCount2 - $phHoursTmp; 
                                } else {
                                    $tVal = 1;
                                }

                                $this->M_overtime->$ot03Column($tVal);
                                $otCount3 += $tVal;
                            }

                            if($phHoursTmp > $otInOffCount2){
                                $tVal = $phHoursTmp - $otInOffCount2;
                                $this->M_overtime->$ot04Column($tVal);
                                $otCount4 += $tVal;
                            }
                        } 

                        /* GET STAND BY COUNT */
                        else if( strtoupper($phCodeTmp) == "SB")
                        {
                            $standByCount++;
                        }

                        /* GET PAID PERMIT COUNT */
                        else if( strtoupper($phCodeTmp) == "PP")
                        {
                            $paidPermitCount++;
                        }

                        /* GET PAID VACATION COUNT */
                        else if( strtoupper($phCodeTmp) == "PV")
                        {
                            $paidVacation++;
                        }                        

                        /* GET ALPA COUNT OFF DAY */
                        else if($otTmp == "A" || $otTmp == "a" || $otTmp == "U" || $otTmp == "u")
                        {
                            // if($rosterColumn == 'd01') {
                            //     echo $otTmp; exit(0);
                            // }

                            $unpaidCount++;
                            if( ($workDay == '5') || (($workDay != '5') && ($workDay != '6') && ($tmpRosterBase = '5')) ){
                                $unpaidDate .= '5';
                            }
                            else if( ($workDay == '6') || (($workDay != '5') && ($workDay != '6') && ($tmpRosterBase = '6')) ){
                                $unpaidDate .= '6';                                
                            }
                        }

                        /* GET PERMIT COUNT OFF DAY */
                        else if($otTmp == "I" || $otTmp == "i")
                        {
                            $unpaidPermitCount++;
                            $unpaidCount++;
                            if( ($workDay == '5') || (($workDay != '5') && ($workDay != '6') && ($tmpRosterBase = '5')) ){
                                $unpaidDate .= '5';
                            }
                            else if( ($workDay == '6') || (($workDay != '5') && ($workDay != '6') && ($tmpRosterBase = '6')) ){
                                $unpaidDate .= '6';                                
                            }
                        }

                        /* GET SICK COUNT OFF DAY */
                        else if($otTmp == "S" || $otTmp == "s")
                        {
                            $sickCount++;
                        }

                        /* GET VACATION COUNT OFF DAY */
                        else if($otTmp == "V" || $otTmp == "v")
                        {
                            $vacationDate .= $dayIndex.'.';
                            $vacationCount++;
                            /*if($clientName == 'Pontil_Timika')
                            {
                                $attendCount++;
                            }*/                           
                            
                        } 

                        /* GET ALPA COUNT */
                        else if($otTmp == '0' || $otTmp == '')
                        {
                            $unpaidCount++;
                            if( ($workDay == '5') || (($workDay != '5') && ($workDay != '6') && ($tmpRosterBase = '5')) ){
                                $unpaidDate .= '5';
                            }
                            else if( ($workDay == '6') || (($workDay != '5') && ($workDay != '6') && ($tmpRosterBase = '6')) ){
                                $unpaidDate .= '6';                                
                            }
                        }

                        else if($otTmp == "E" || $otTmp == "e")
                        {
                            $emergencyCount++;
                        }

                        else if( (strtoupper($otTmp) == "D") && ($clientName == "Redpath_Timika") )
                        {
                            $emergencyCount++;
                        }                                                

                        else if( ($workDay == '5')  || (($workDay != '5') && ($workDay != '6') && ($tmpRosterBase = '5')) )
                        {
                            /* HARI KERJA */
                            # 8 JAM PERTAMA NORMAL    
                            # JAM KE 9 (LEMBUR 1 x 1.5)     
                            # JAM KE >= 10 (LEMBUR 1 x 2)

                            // echo $otTmp; exit(0);    

                            if(is_numeric($otTmp)){
                                if($otTmp > 8){
                                    if($otTmp < 9){
                                        $tVal = 9 - $otTmp;
                                    } else {
                                        $tVal = 1;
                                    }
                                    $this->M_overtime->$ot01Column($tVal);
                                    $otCount1 += $tVal;
                                }
                                if($otTmp > 9){
                                    $tVal = $otTmp - 9;
                                    $otCount2 += $tVal;
                                    $this->M_overtime->$ot02Column($tVal);
                                }     
                            } 
                        }

                        else if( ($workDay == '6') || (($workDay != '5') && ($workDay != '6') && ($tmpRosterBase = '6')) )
                        {
                            /* HARI KERJA */    
                            # 7 JAM PERTAMA NORMAL    
                            # JAM KE 8 (LEMBUR 1 x 1.5)     
                            # JAM KE >= 9 (LEMBUR 1 x 2)  
                            
                            if($sixthDayTmp == 6)
                            {
                                $sixthDayTmp = 0;       
                            }

                            $sixthDayTmp++;                       
                            // $tmp .= $sixthDayTmp."  "; 
                            if(is_numeric($otTmp)){
                                if($sixthDayTmp < 6)
                                {
                                    if($otTmp > 7){
                                        if($otTmp < 8){
                                            $tVal = 8 - $otTmp;
                                        } else {
                                            $tVal = 1;
                                        }
                                        $otCount1 += $tVal;
                                        $this->M_overtime->$ot01Column($tVal);
                                    }
                                    if($otTmp > 8){
                                        $tVal = $otTmp - 8;
                                        $otCount2 += $tVal;
                                        $this->M_overtime->$ot02Column($tVal);
                                    }       
                                }
                                else
                                {
                                    /* HARI KERJA */    
                                    # 5 JAM PERTAMA NORMAL    
                                    # JAM KE 6 (LEMBUR 1 x 1.5)      
                                    # JAM KE >= 7 (LEMBUR 1 x 2)
                                    if($otTmp > 5){
                                        if($otTmp < 6) {
                                            $tVal = 6 - $otTmp;
                                        } else {
                                            $tVal = 1;
                                        }
                                        $this->M_overtime->$ot01Column($tVal);
                                        $otCount1 += $tVal;
                                    }
                                    if($otTmp > 6){
                                        $tVal = $otTmp - 6;
                                        $otCount2 += $tVal;
                                        $this->M_overtime->$ot02Column($tVal);
                                    }                                    
                                }
                            } // if(is_numeric($otTmp))
                        }
                        /* GET ALPA COUNT OFF DAY */                                                                   
                    }
                    /* END GET WORK DAYS BY SHIFT */                 
                    /* START GET OFF DAYS BY SHIFT */
                    for ($od=1; $od <= $offDay; $od++) 
                    {
                        $dayIndex++;
                        $otCount1 = 0;
                        if($dayIndex < 10){
                            $rosterColumn = 'd0'.$dayIndex;
                            $ot01Column = 'setOt1D0'.$dayIndex;    
                            $ot02Column = 'setOt2D0'.$dayIndex;    
                            $ot03Column = 'setOt3D0'.$dayIndex;    
                            $ot04Column = 'setOt4D0'.$dayIndex;  
                        }
                        else {
                            $rosterColumn = 'd'.$dayIndex;
                            $ot01Column = 'setOt1D'.$dayIndex;    
                            $ot02Column = 'setOt2D'.$dayIndex;    
                            $ot03Column = 'setOt3D'.$dayIndex;    
                            $ot04Column = 'setOt4D'.$dayIndex;    
                        }
                        $otTmp = $row[$rosterColumn];
                        
                        /* RESET OVERTIME PROPERTIES IN WORKDAY */
                        $this->M_overtime->$ot01Column(0);
                        $this->M_overtime->$ot02Column(0);
                        $this->M_overtime->$ot03Column(0);
                        $this->M_overtime->$ot04Column(0);
                        /* GET ATTEND COUNT OFF DAY */

                        $phCodeTmp = substr($otTmp,0,2);
                        $phHoursTmp = substr($otTmp,2,strlen($otTmp)-2);
                        $st .= $phCodeTmp; 
                        if( (strtoupper($phCodeTmp) == "PH") && (is_numeric($phHoursTmp)) && ($phHoursTmp > 0) )
                        {
                            $otTmp = $phHoursTmp; 
                        }

                        if(is_numeric($otTmp) && $otTmp > 0)
                        {
                            $attendCount++;
                            $offAttendCount++;                            
                        }                        

                        /* HARI LIBUR */
                        # LEMBUR II (7 JAM PERTAMA X 2)
                        # LEMBUR III (JAM KE 8 X 3)
                        # LEMBUR IV (JAM >= 9 X 4)
                        if (is_numeric($otTmp)) {
                            if($otTmp <= 0){
                                $offCount++;
                            }
                        }else{
                            $offCount++;
                        }
                        // $otInOffCount1 = $_POST['otInOffCount'];
                        // $otInOffCount2 = $otInOffCount1 + 1;
                        // $otInOffCount3 = $otInOffCount2 + 1;
                        if(is_numeric($otTmp)){
                            if($otTmp >= $otInOffCount1){
                                $this->M_overtime->$ot02Column($otInOffCount1);
                                $otCount2 += $otInOffCount1;
                            }else{
                                $this->M_overtime->$ot02Column($otTmp);
                                $otCount2 += $otTmp;
                            }

                            if($otTmp > $otInOffCount1){
                                if($otTmp < $otInOffCount2){
                                    $tVal = $otInOffCount2 - $otTmp;
                                } else{
                                    $tVal = 1;
                                }

                                $otCount3 += $tVal;
                                $this->M_overtime->$ot03Column($tVal);
                            }

                            if($otTmp > $otInOffCount2){
                                $tVal = $otTmp - $otInOffCount2;
                                $this->M_overtime->$ot04Column($tVal);
                                $otCount4 += $tVal;
                            }                            
                        }
                    }
                    /* END GET OFF DAYS BY SHIFT */ 
                } 
                /* for ($x=1; $x <= $rosterMasterCount; $x++) */
                /* END SHIFT BREAKDOWN */
                /* START SET DATA INSERT TRN_OVERTIME */
                $otId = $this->M_overtime->GenerateNumber();                 
                $this->M_overtime->setOvertimeId($otId);
                $this->M_overtime->setBioRecId($bioId);
                $this->M_overtime->setClientName($clientName);
                $this->M_overtime->setRosterId($row['roster_id']);

                $yearPeriod = $row['year_process'];
                $monthPeriod = $row['month_process'];

                $this->M_overtime->setYearPeriod($yearPeriod);
                $this->M_overtime->setMonthPeriod($monthPeriod);                
                /* Start Dynamic Data*/
                $this->M_overtime->setOffTotal($offCount);                
                $this->M_overtime->setEmergencyTotal($emergencyCount);                
                $this->M_overtime->setSickTotal($sickCount);                
                $this->M_overtime->setPermitTotal($unpaidPermitCount);                
                $this->M_overtime->setAlpaTotal($unpaidCount);                
                $this->M_overtime->setAttendTotal($attendCount);                
                $this->M_overtime->setUnpaidDays($unpaidDate);                
                $this->M_overtime->setVacationDays($vacationDate);  
                $this->M_overtime->setVacationTotal($vacationCount);  
                $this->M_overtime->setAttendInOff($offAttendCount);                
                $this->M_overtime->setPhTotal($phCountInMonth);                
                $this->M_overtime->setInPhTotal($attendPHCount);                
                
                $this->M_overtime->setStandbyTotal($standByCount);                
                $this->M_overtime->setPaidVacationTotal($paidVacation);                
                $this->M_overtime->setPaidPermitTotal($paidPermitCount);                
                /* End Dynamic Data*/                
                $this->M_overtime->setIsActive($row['is_active']);
                $this->M_overtime->setPicProcess($this->session->userdata('hris_user_id'));
                $myCurrentDate = GetCurrentDate();
                $this->M_overtime->setProcessTime($myCurrentDate['CurrentDateTime']);
                
                $this->M_overtime->insert();   
                $this->payrollProcess($bioId, $clientName, $yearPeriod, $monthPeriod);
                $this->allowanceProcess($bioId, $clientName, $yearPeriod, $monthPeriod);    
                
                /* END SET DATA INSERT TRN_OVERTIME */    
            } /* END FOREACH */
            $this->taxProcess($clientName, $year, $month);
            ini_set('max_execution_time', 60); //60 seconds = 1 minutes       

            /* COMMIT TRANSACTION */
            // $this->db->trans_complete();
            if($this->db->trans_status() === FALSE)
            {
              // $this->session->set_flashdata('rosterMsg', 'Update Data Gagal');
                 $this->db->trans_rollback();
                echo $failMessage;
            }
            else
            {
              // $this->session->set_flashdata('rosterMsg', 'Update Data Berhasil');
                 $this->db->trans_commit();
                // echo "Process Succeed...";
                echo $this->getPayrollList($clientName, $year, $month, 'All');
            } 
        }
        /* End Roster Redpath - Pontil_Timika */ 
    }

    // public function payrollProcess()
    public function payrollProcess($biodataId, $clientName, $yearPeriod, $monthPeriod)
    {
        // $biodataId = '';
        // $clientName = '';
        // $year = '';
        // $month = '';
        // if(isset($_POST['biodataId']))
        // {
        //     $biodataId = $_POST['biodataId']; 
        // }
        // if(isset($_POST['clientName']))
        // {
        //     $clientName = $_POST['clientName'];
        // }
        // if(isset($_POST['yearPeriod']))
        // {
        //     $yearPeriod = $_POST['yearPeriod'];
        // }
        // if(isset($_POST['monthPeriod']))
        // {
        //     $monthPeriod = $_POST['monthPeriod'];
        // }

        if($clientName == 'Redpath_Timika' || $clientName == 'Pontil_Timika')
        {
            $queryFilter = '';
            if($biodataId != '')
            {
                $queryFilter = 'AND mr.bio_rec_id = "'.$biodataId.'" ';    
            }            

            $this->db->select('mr.*');
            $this->db->select('tro.*');
            $this->db->select('mbr.bio_rec_id, mbr.full_name, mbr.marital_status, mbr.position,mr.roster_id, mr.roster_base, mr.dept');
            $this->db->select('tro.attend_total, tro.attend_in_off, tro.ph_total, tro.in_ph_total, tro.vacation_days, tro.permit_total');
            $this->db->select('ms.basic_salary,ms.positional_allowance,ms.transport_housing,ms.is_remote_allowance,ms.is_shift_bonus,ms.payroll_group');
            $this->db->select('ms.is_allowance_economy,ms.is_ot_bonus,ms.is_incentive_bonus,ms.is_dev_incentive_bonus, ms.is_overtime, ms.is_travel,ms.salary_level');
            $this->db->select('d01,d02,d03,d04,d05,d06,d07,d08,d09,d10,d11,d12,d13,d14,d15,d16,d17,d18,d19,d20,d21,d22,d23,d24,d25,d26,d27,d28,d29,d30,d31');
            $this->db->from('mst_salary ms, mst_bio_rec mbr');
            $this->db->join('mst_roster mr',' mbr.bio_rec_id = mr.bio_rec_id', 'left');
            $this->db->join('trn_overtime tro','mbr.bio_rec_id = tro.bio_rec_id', 'left');
            $this->db->where('ms.bio_rec_id = mbr.bio_rec_id');  
            if($biodataId != '')
            {
                $this->db->where('mr.bio_rec_id = "'.$biodataId.'" ');
            }       
            $this->db->where('mr.year_process = "'.$yearPeriod.'" ');
            $this->db->where('mr.month_process = "'.$monthPeriod.'" ');
            $this->db->where('tro.year_period = "'.$yearPeriod.'" ');
            $this->db->where('tro.month_period = "'.$monthPeriod.'" ');
            $this->db->where('ms.company_name = "'.$clientName.'" ');
            $this->db->where('mbr.is_active = 1');
            $query = $this->db->get()->result_array();

            /* START GET CONFIG FROM mst_payroll_config TABLE */
            // $this->load->model('M_payroll_config');
            $myConfigId = 0;    
            switch ($clientName) {
                case 'Redpath_Timika':
                    # code...
                    $myConfigId = 1;
                    break;
                case 'Pontil_Timika':
                    # code...
                    $myConfigId = 2;
                    break;
                default:
                    # code...
                    break;
            }
            $row = $this->M_payroll_config->getObjectById($myConfigId);        
            /* END GET CONFIG FROM mst_payroll_config TABLE */
            

            /* START LOOP DATABASE */
            $myData = array();
            foreach ($query as $key => $row) {
                /* Set Default Variable Values */
                $salaryHourly = 0;
                $otTotal01 = 0;
                $otTotal02 = 0;
                $otTotal03 = 0;
                $otTotal04 = 0;

                $trvValue = 0;
                $allowanceEconomy = 0;
                $incentiveBonus = 0;
                $shiftBonus = 0;
                $remoteAllowance = 0;
                $positionalAllowance = 0;
                $allOverTimeTotal = 0;
                $otBonus = 0;
                $transHousingAllowance = 0;
                
                $nonTaxAllowance = 0;
                $attendTotal = 0;
                $attendInPH = 0;
                $phTotal = 0;
                $attendInOff = 0;
                $sickTotal = 0;
                $emergencyTotal = 0;
                $offTotal = 0;
                $alpaTotal = 0;
                $vacationTotal = 0;

                $this->M_salary_slip->resetValues();

                /* Start Data Values */
                $bioRecId = $row['bio_rec_id'];
                $rosterId = $row['roster_id'];
                $payrollGroup = $row['payroll_group'];
                $payrollLevel = $row['salary_level'];
                $rosterBase = $row['roster_base'];
                /* BASIC SALARY */
                $basicSalary = $row['basic_salary'];
                $fullName = $row['full_name'];
                $dept = $row['dept'];

                $maritalStatus = $row['marital_status'];
                $positionalAllowance = $row['positional_allowance'];
                $transHousingAllowance = $row['transport_housing'];
                $phTotal = $row['ph_total'];
                $attendInPH = $row['in_ph_total'];
                $attendInOff = $row['attend_in_off'];
                $attendTotal = $row['attend_total'] + $phTotal;
                $permitTotal = $row['permit_total']; 
                $offTotal = $row['off_total'];
                $sickTotal = $row['sick_total'];
                $alpaTotal = $row['alpa_total'];
                $vacationTotal = $row['vacation_total'];
                $emergencyTotal = $row['emergency_total'];
                $unpaidDays = $row['unpaid_days'];
                $position = $row['position'];               
                $isTravel = $row['is_travel'];
                $isRemoteAllowance = $row['is_remote_allowance'];
                $isShiftBonus = $row['is_shift_bonus'];
                $isAllowanceEconomy = $row['is_allowance_economy'];
                $isOtBonus = $row['is_ot_bonus'];
                $isIncentiveBonus = $row['is_incentive_bonus'];
                $isDevIncentiveBonus = $row['is_dev_incentive_bonus'];
                $isOvertime = $row['is_overtime'];
                /* Add By Maurice @28-12-2017 */
                $standByCount = $row['standby_total'];
                $paidPermitCount = $row['paid_permit_total'];
                $paidVacation = $row['paid_vacation_total'];
                /* End Data Values */

                $shiftAttend = $row['attend_total'] - $row['attend_in_off'];
                $offAttend = $row['attend_in_off'];
                $phAttend = $row['in_ph_total'];
                
                /* Start Get Config Values */
                $remotePercentConfig = $this->M_payroll_config->getRemotePercent();
                $shiftBonusConfig = $this->M_payroll_config->getShiftBonus();
                
                $otBonusConfig = $this->M_payroll_config->getOtBonus();
                $incentiveBonusConfig = $this->M_payroll_config->getIncentiveBonus();  
                $healthBpjsConfig = $this->M_payroll_config->getHealthBpjs();
                $maxHealthBpjsConfig = $this->M_payroll_config->getMaxHealthBpjs();
                $jkkJkmConfig = $this->M_payroll_config->getJkkJkm();
                $jhtConfig = $this->M_payroll_config->getJht();
                $empJhtConfig = $this->M_payroll_config->getEmpJht();             
                $empHealthBpjsConfig = $this->M_payroll_config->getEmpHealthBpjs();    
                $jpConfig = $this->M_payroll_config->getJp();
                $empJpConfig = $this->M_payroll_config->getEmpJp();           
                $salaryDividerConfig = $this->M_payroll_config->getSalaryDivider();

                
                
                $otMultiplierConfig01 = $this->M_payroll_config->getOt01Multiplier();
                $otMultiplierConfig02 = $this->M_payroll_config->getOt02Multiplier();
                $otMultiplierConfig03 = $this->M_payroll_config->getOt03Multiplier();
                $otMultiplierConfig04 = $this->M_payroll_config->getOt04Multiplier();                     
                $nonTaxAllowanceConfig = $this->M_payroll_config->getNonTaxAllowance();                                   
                $isProrateConfig = $this->M_payroll_config->getIsProrate();

                $noAccidentPercent = $this->M_payroll_config->getNoAccidentPercent(); 

                // echo $jpConfig; exit(0);                                  

                /* START OVER TIME TOTAL */
                $otCount1 = 0;
                $otCount2 = 0;
                $otCount3 = 0;
                $otCount4 = 0;
                $timeTotal = 0;
                $ntTotal = 0;

                for ($i=1; $i <= 31; $i++) { 
                    # code...
                    $idx = '';
                    if($i < 10){
                        $idx = '0'.$i; 
                    }else{
                        $idx = $i; 
                    }
                    $otCount1 += $row['ot1_d'.$idx];
                    $otCount2 += $row['ot2_d'.$idx];
                    $otCount3 += $row['ot3_d'.$idx];
                    $otCount4 += $row['ot4_d'.$idx];

                    /* COUNTING OF WORKED HOUR */    
                    $tmpTime = trim($row['d'.$idx]);
                    if(is_numeric($tmpTime)) {
                        $timeTotal += $row['d'.$idx];
                    } else {
                        $tmpCode = substr($tmpTime, 0, 2);
                        if($tmpCode == "PH" || $tmpCode == "RO") {
                            $tmpHour = substr($tmpTime, 2, strlen($tmpTime)-2);
                            $timeTotal += floatval($tmpHour);
                        }
                    } 
                }

                $ntTotal = $timeTotal - $otCount1 - $otCount2 - $otCount3 - $otCount4;
                /* OVER TIME HOURLY */
                $salaryHourly = $basicSalary / $salaryDividerConfig; 
                
                $otTotal01 = $salaryHourly * $otMultiplierConfig01 * $otCount1; /* Rate Per Jam x Nilai OT 1 x Total Jam Lembur */
                $salaryHourly = $basicSalary / $salaryDividerConfig; 
                $otTotal02 = $salaryHourly * $otMultiplierConfig02 * $otCount2; /* Rate Per Jam x Nilai OT 2 x Total Jam Lembur */
                $otTotal03 = $salaryHourly * $otMultiplierConfig03 * $otCount3; /* Rate Per Jam x Nilai OT 3 x Total Jam Lembur */
        	
                $otTotal04 = $salaryHourly * $otMultiplierConfig04 * $otCount4; /* Rate Per Jam x Nilai OT 4 x Total Jam Lembur */

                /* CHANGED TO COUNT ONLY */
                // $otTotal01 = $otCount1; /* Rate Per Jam x Nilai OT 1 x Total Jam Lembur */
                // $otTotal02 = $otCount2; /* Rate Per Jam x Nilai OT 2 x Total Jam Lembur */
                /* CHANGED TO COUNT ONLY */
                // $otTotal03 = $otCount3; /* Rate Per Jam x Nilai OT 3 x Total Jam Lembur */
                // $otTotal04 = $otCount4; /* Rate Per Jam x Nilai OT 4 x Total Jam Lembur */

                /* END OVER TIME TOTAL */
                  
                /* START TRAVEL */
                $trvMultiplierConfig = $this->M_payroll_config->getTrvMultiplier();
                
                // $shiftAttend = $row['attend_total'] - $row['attend_in_off'];
                // $offAttend = $row['attend_in_off'];
                // $phAttend = $row['in_ph_total'];

                $trvValue = ( ($shiftAttend + $offAttend + $phAttend) * $trvMultiplierConfig ) * $salaryHourly;
                /* END TRAVEL */

                // echo  $attendTotal." * ".$trvMultiplierConfig." * ".$salaryHourly;
                
                /* START ALLOWANCE ECONOMY */
                $allowanceEconomy = $this->M_payroll_config->getAllowanceEconomy(); /* Nilai Konstana mst_payroll_config.allowance_economy */
                /* END ALLOWANCE ECONOMY */

                /* START INCENTIVE BONUS */
                // $shiftAttend = $row['attend_total'] - $row['attend_in_off'];
                // $offAttend = $row['attend_in_off'];
                // $phAttend = $row['in_ph_total'];

                $incentiveBonus = $incentiveBonusConfig * ($shiftAttend + $offAttend + $phAttend); /* Nilai mst_payroll_config.incentive_bonus x Jumlah Masuk Hari Kerja  */
                /* END INCENTIVE BONUS */

                /* START SHIFT BONUS */ 
                if($clientName == 'Pontil_Timika')
                {
                    $shiftBonus = $shiftBonusConfig * ($shiftAttend + $offAttend + $phAttend); /* Nilai mst_payroll_config.shift_bonus x Jumlah Hari Masuk Kerja  */
                }
                else if($clientName == 'Redpath_Timika')
                {
                    /* Perubahan Formula base on  */
                    $tAttend = $shiftAttend + $offAttend + $phAttend;
                    if($tAttend > 26){
                       $tAttend = 26; 
                    } 
                    if( trim($dept)!= 'ADMIN'){
                        /* IF DEPT != ADMIN */
                        $shiftBonus = $shiftBonusConfig * $tAttend; /* Nilai mst_payroll_config.shift_bonus x Jumlah Hari Masuk Kerja (max = 26 days) */                        
                    }else{
                        /* IF DEPT == ADMIN */
                        $shiftBonus = 20000 * $tAttend; /* 20000 x Jumlah Hari Masuk Kerja (max = 26 days) */                        
                    }
                }
                /* END SHIFT BONUS */                   

                /* START PRODUCTION BONUS */
                /* MANUALLY INPUT PER PERSON */
                /* END PRODUCTION BONUS */

                /* START WORK DAY ADJUSTMENT */
                /* MANUALLY INPUT PER PERSON  */
                /* END WORK DAY ADJUSTMENT */

                /* START WORK CONTRACT BONUS */
                /* MANUALLY INPUT PER PERSON */
                /* END WORK CONTRACT BONUS */

                /* START THR */
                /* MANUALLY INPUT PER PERSON  */
                /* END THR */

                /* START CC PAYMENT */
                /* MANUALLY INPUT PER PERSON  */
                /* END CC PAYMENT */

                /* START ALPA TOTAL */             
                $totalAlpa = 0;
                $unpaidLength = strlen($unpaidDays);
                for ($ud=0; $ud < $unpaidLength; $ud++) { 
                    $unpaidChar = substr($unpaidDays, $ud, 1);                  

                    if($unpaidChar == '5')
                    {
                        $totalAlpa += (1/21) * $basicSalary;
                    }
                    else if($unpaidChar == '6')
                    {
                        $totalAlpa += (1/25) * $basicSalary;
                    }
                }   
                /* END ALPA TOTAL */

                /* START OVER TIME BONUS */
                $allOverTimeTotal = $otTotal01 + $otTotal02 + $otTotal03 + $otTotal04; 

                $otBonus = ($otBonusConfig/100) * $allOverTimeTotal; /* Nilai mst_payroll_config.ot_bonus(%) x Total Nilai Over Time */
                /* START OVER TIME BONUS */
                /* END OVER TIME BONUS */

                // if($fullName == "Yopianus Sondegau"){
                //     echo $allOverTimeTotal.' - '.$otBonusConfig.' - '.$otBonus; exit(0);
                // }    

                /* START DEVELOPMENT INCENTIVE */

                /* EXCLUDE If Work Hours Less Than 8 Hours */
                $excludeCountDay = 0;
                $colTmp = '';
                for($z=1; $z <= 31; $z++){
                    if($z < 10){
                        $colTmp = 'd0'.$z;
                    }else{
                        $colTmp = 'd'.$z;
                    }
                    if( is_numeric($row[$colTmp]) && ($row[$colTmp] > 0) && ($row[$colTmp] < 8) ){
                        $excludeCountDay++;
                    } 

                    if( substr($row[$colTmp],0,2) == 'PH' ){
                        $phVal = substr($row[$colTmp],2, strlen($row[$colTmp]-2));
                        if( is_numeric($phVal) && ($phVal > 0) && ($phVal < 8) ){
                            $excludeCountDay++; 
                        }
                    }                    
                }


                $alpaSickPercent = 0;
                for ($m=0; $m < ($alpaTotal-$permitTotal); $m++) { 
                    $alpaSickPercent += 50;
                }
                for ($m=0; $m < $sickTotal; $m++) { 
                    $alpaSickPercent += 15;
                }
                $devIncentiveBonus = 0;
                if($alpaSickPercent < 100)
                {
                    /* FORMULA CHANGE (35/100)/27) to (35/100)/26) BASE ON  */
                    /* FORMULA CHANGE (Max Day Count is 26) BASE ON Ntus Email @ 26-11-2018 */
                    $tdayCount = $row['attend_total'] + $row['in_ph_total'] - $excludeCountDay;
                    if($tdayCount > 26){
                       $tdayCount = 26; 
                    }
                    /* FORMULA CHANGE (35/100)/26) to (50/100)/26) BASE ON  */
                    $tmp = $basicSalary * ((50/100)/26) * $tdayCount;
                    /* (35%)/27 x Jumlah Masuk Kerja Yang Lebih Dari 7 Jam (Jika ada alpa, potong 50% perhari, Sakit 15% perhari ) */
                    $devIncentiveBonus = $tmp - ($tmp * ($alpaSickPercent/100));    
                }  

                if($devIncentiveBonus <= 0) {
                   $devIncentiveBonus = 0; 
                }           
                /* END DEVELOPMENT INCENTIVE */             
                // echo $excludeCountDay; exit(0);

                /* START GOVERNMENT REGULATION */
                $nonTaxAllowance = $this->M_payroll_config->getNonTaxAllowance(); /* Nilai Tunjangan Non Pajak */
                $taxPercent1 = $this->M_payroll_config->getTaxPercent1(); /* Persentase Progresif Pajak I */
                $taxPercent2 = $this->M_payroll_config->getTaxPercent2(); /* Persentase Progresif Pajak II */
                $taxPercent3 = $this->M_payroll_config->getTaxPercent3(); /* Persentase Progresif Pajak III */
                $taxPercent4 = $this->M_payroll_config->getTaxPercent4(); /* Persentase Progresif Pajak IV */
                $taxVal1 = $this->M_payroll_config->getTaxMaxVal1(); /* Nominal Pajak I */ 
                $taxVal2 = $this->M_payroll_config->getTaxMaxVal2(); /* Nominal Pajak II */
                $taxVal3 = $this->M_payroll_config->getTaxMaxVal3(); /* Nominal Pajak III */
                $taxVal4 = $this->M_payroll_config->getTaxMaxVal4(); /* Nominal Pajak IV */
                $ptkpBasic = $this->M_payroll_config->getPtkp(); /* Nominal PTKP */
                $ptkpDependent = $this->M_payroll_config->getPtkpDependent(); /* Nominal PTKP Tanggungan Per Orang */              
                /* START GET PTKP TOTAL  */
                $ptkpMultiplier = 0;
                switch ($maritalStatus) {
                    case 'TK0':
                        $ptkpMultiplier = 0;
                        break;
                    case 'TK1':
                        $ptkpMultiplier = 1;
                        break;
                    case 'TK2':
                        $ptkpMultiplier = 2;
                        break;
                    case 'TK3':
                        $ptkpMultiplier = 3;
                        break;
                    case 'K0':
                        $ptkpMultiplier = 1;
                        break;
                    case 'K1':
                        $ptkpMultiplier = 2;
                        break;
                    case 'K2':
                        $ptkpMultiplier = 3;
                        break;
                    case 'K3':
                        $ptkpMultiplier = 4;
                        break;  
                    
                    default:
                        $ptkpMultiplier = 0;
                        break;
                }

                $totalPTKP = $ptkpBasic + ($ptkpDependent * $ptkpMultiplier);

                /* END GET PTKP TOTAL */
                
                $tmpSalary = $basicSalary; 
                $vacDays = $row['vacation_days'];
                $wdAttendCount = 0;
                $bsProrate = 0;
                /* START GET PRORATE */
                if( ($isProrateConfig == true) ){  


                    if( ($clientName == 'Redpath_Timika') && (strlen($vacDays) > 0) )
                    {
                        $wdAttendCount += $row['attend_total']; // 5
                        $wdAttendCount += $row['in_ph_total']; //2
                        // $wdAttendCount -= $row['ph_total']; //2
                        $wdAttendCount -= $row['attend_in_off'];//2
                        $wdAttendCount += $row['sick_total']; //0
                        $wdAttendCount += $row['emergency_total']; //1
                        $wdAttendCount += $row['paid_permit_total']; //0
                        $wdAttendCount += $row['standby_total'];//0
                        $wdAttendCount += $row['paid_vacation_total']; //0
                        $wdAttendCount += $row['alpa_total']; /* Include $row['permit_total'] */ //5
                        $bfCode = substr($row['roster_base'], 0, 1);
                        if( $bfCode == '5' ){
                            if($wdAttendCount > 21) 
                            {
                                $wdAttendCount = 21;   
                            }
                            $bsProrate = ($wdAttendCount/21) * $basicSalary;
                        }else if( $bfCode == '6' ){
                            if($wdAttendCount > 25) 
                            {
                                $wdAttendCount = 25;   
                            }
                            $bsProrate = ($wdAttendCount/25) * $basicSalary;
                        } 
                        /* Request By Ilham & Bambang @15-12-2017 */
                        $tmpSalary = $bsProrate;         
                    }

                    else if( ($clientName == 'Pontil_Timika')  && ($standByCount > 0) )
                    {
                        $wdAttendCount += $row['attend_total']; // 5
                        $wdAttendCount += $row['in_ph_total']; //2
                        $wdAttendCount -= $row['attend_in_off'];//2
                        $wdAttendCount += $row['sick_total']; //0
                        $wdAttendCount += $row['emergency_total']; //1
                        $wdAttendCount += $row['paid_permit_total']; //0
                        $wdAttendCount += $row['vacation_total']; //0
                        $wdAttendCount += $row['paid_vacation_total']; //0
                        $wdAttendCount += $row['alpa_total']; /* Include $row['permit_total'] */ //5
                        
                        $bfCode = substr($row['roster_base'], 0, 1);
                        if( $bfCode == '5' ){
                            if($wdAttendCount > 21) 
                            {
                                $wdAttendCount = 21;   
                            }
                            $bsProrate = ($wdAttendCount/21) * $basicSalary;
                        }else if( $bfCode == '6' ){
                            if($wdAttendCount > 25) 
                            {
                                $wdAttendCount = 25;   
                            }
                            $bsProrate = ($wdAttendCount/25) * $basicSalary;
                        } 
                        /* Request By Ilham & Bambang @15-12-2017 */
                        $tmpSalary = $bsProrate;         
                    }
                    
                }
                // echo $row['attend_total']; exit(0);
                $this->M_salary_slip->setBsProrate($tmpSalary);
                
                /* START REMOTE LOCATION BONUS */
                /* Aturan Baru 26 Jan 2018 (20% Dari Basic), Dari mst_payroll_config.remote_allowance */
                /* By Addesant, terjadi perubahan 02 Feb 2018 (20% Dari Prorate), Dari mst_payroll_config.remote_allowance */
                /* START IN SHIFT, IN OFF, IN PH  */
                $shiftAttend = $row['attend_total'] - $row['attend_in_off'];
                $offAttend = $row['attend_in_off'];
                $phAttend = $row['in_ph_total'];
                $tAttendCount = $shiftAttend + $offAttend + $phAttend;
                if($tAttendCount > 26){
                   $tAttendCount = 26; 
                }
                /* New Rules (Basic*20%)/26)*Kehadiran (Maks. 26 Hari) Based On Ntus Email @ 11/26/2018 */
                $remoteAllowance = (( ($remotePercentConfig/100) * $basicSalary )/26)*$tAttendCount; 
                if( ($shiftAttend + $offAttend + $phAttend) <= 0 ) {
                    $remoteAllowance = 0;
                }
                /* END REMOTE LOCATION BONUS */

                /* START POSITIONAL ALLOWANCE/ NON TAX */
                $nonTaxTmp = $basicSalary * (5/100);
                if($nonTaxTmp < $nonTaxAllowance){
                    $nonTaxAllowance = $nonTaxTmp;                    
                }                
                /* END POSITIONAL ALLOWANCE/ NON TAX */

                /* START HEALTH BPJS */
                $healthBpjs = ($healthBpjsConfig/100) * $basicSalary; /* Nilai mst_payroll_config.health_bpjs(%) x Basic Salary (Max 8 Juta) */
                if($healthBpjs > $maxHealthBpjsConfig){
                    $healthBpjs = $maxHealthBpjsConfig; 
                }
                /* END HEALTH BPJS */

                /* START JKK-JKM */
                $jkkJkm = ($jkkJkmConfig/100) * $basicSalary; /* Nilai mst_payroll_config.jkk_jkm(%) x Basic Salary */
                /* END JKK-JKM */

                /* START JHT */
                $jht = ($jhtConfig/100) * $basicSalary; /* Nilai mst_payroll_config.jht(%) x Basic Salary */        
                /* END JHT */
                
                /* START JHT */
                $empJht = ($empJhtConfig/100) * $basicSalary; /* Nilai mst_payroll_config.emp_jht(%) x Basic Salary */      
                /* END JHT */

                /* START JP COMPANY */
                // $jp = ($jpConfig/100) * $basicSalary; /* Nilai mst_payroll_config.jp(%) x Basic Salary */        
                /* END JP COMPANY */

                /* START JP EMPLOYEE */
                $maxEmpJp = $this->M_payroll_config->getMaxEmpJp(); /* Nilai Max Untuk Iuran JP Karyawan  */
                $empJp = ($empJpConfig/100) * $basicSalary; /* Nilai mst_payroll_config.emp_jp(%) x Basic Salary */        
                if($empJp > $maxEmpJp) {
                    $empJp = $maxEmpJp; 
                }
                /* END JP EMPLOYEE */

                /* START BPJS KARYAWAN */
                $maxEmpHealthBpjs = $this->M_payroll_config->getMaxEmpBpjs(); /* Nilai Max Gaji Untuk Iuran BPJS Karyawan  */ 
                $empHealthBpjs = ($empHealthBpjsConfig/100) * $basicSalary; /* Nilai mst_payroll_config.emp_health_bpjs(%) x Basic Salary */
                if($empHealthBpjs > $maxEmpHealthBpjs)
                {
                    $empHealthBpjs = $maxEmpHealthBpjs; 
                }     
                /* END BPJS KARYAWAN */

                /* START ALL ALLOWANCE */
                $allAllowance = 0;
                if($clientName == 'Pontil_Timika')
                {
                    $allAllowance += $trvValue; 
                    $allAllowance += $allowanceEconomy; 
                    $allAllowance += $incentiveBonus; 
                    $allAllowance += $shiftBonus; 
                    $allAllowance += $jkkJkm;               
                    $allAllowance += $healthBpjs;               
                    // $allAllowance += $jp;               
                    $allAllowance -= $totalAlpa;                
                } else if($clientName == 'Redpath_Timika')
                {
                    $allAllowance += $otBonus; 
                    $allAllowance += $shiftBonus; 
                    $allAllowance += $remoteAllowance;                 
                    $allAllowance += $devIncentiveBonus; 
                    $allAllowance += $healthBpjs;
                    // $allAllowance += $jp;
                    $allAllowance += $jkkJkm;               
                    $allAllowance -= $totalAlpa;                
                }
                /* END ALL ALLOWANCE */

                /* START BRUTO INCOME */
                $bruto = $tmpSalary + $allOverTimeTotal + $allAllowance;  
                /* END BRUTO INCOME */          

                /* START NETTO INCOME */
                $nettoTax = $bruto - $empJht - $nonTaxAllowance;  
                /* END NETTO INCOME */
                
                /* END GOVERNMENT REGULATION */

                /* START INSERT TABEL DATA SLIP */
                
                $this->M_salary_slip->setSalarySlipId($this->M_salary_slip->GenerateNumber());
                $this->M_salary_slip->setBioRecId($bioRecId);
                $this->M_salary_slip->setRosterId($rosterId);

                $this->M_salary_slip->setYearPeriod($yearPeriod);
                $this->M_salary_slip->setMonthPeriod($monthPeriod);
                
                $fullName = preg_replace('~[\r\n]+~', '', $fullName);
                $this->M_salary_slip->setName($fullName);
                $this->M_salary_slip->setDept($dept);
                $this->M_salary_slip->setPosition($position);
                $this->M_salary_slip->setMaritalStatus($maritalStatus);
                $this->M_salary_slip->setClientName($clientName);
                $this->M_salary_slip->setBasicSalary($basicSalary); 

                $this->M_salary_slip->setNormalTime($ntTotal);
                if($isOvertime == '1'){
                    $this->M_salary_slip->setOt1($otTotal01);
                    $this->M_salary_slip->setOt2($otTotal02);
                    $this->M_salary_slip->setOt3($otTotal03);
                    $this->M_salary_slip->setOt4($otTotal04);
                    
                    $this->M_salary_slip->setOtCount1($otCount1);
                    $this->M_salary_slip->setOtCount2($otCount2);
                    $this->M_salary_slip->setOtCount3($otCount3);
                    $this->M_salary_slip->setOtCount4($otCount4);
                }
                if($isTravel == '1'){
                    $this->M_salary_slip->setTravelBonus($trvValue);
                }
                if($isRemoteAllowance == '1'){
                    $this->M_salary_slip->setRemoteAllowance($remoteAllowance);
                }
                if($isShiftBonus == '1'){
                    $this->M_salary_slip->setShiftBonus($shiftBonus);
                }
                if($isAllowanceEconomy == '1'){
                    $this->M_salary_slip->setAllowanceEconomy($allowanceEconomy);
                }
                if($isOtBonus == '1'){
                    $this->M_salary_slip->setOtBonus($otBonus);
                }
                if($isIncentiveBonus == '1'){
                    $this->M_salary_slip->setIncentiveBonus($incentiveBonus);
                }
                if($isDevIncentiveBonus == '1'){
                    $this->M_salary_slip->setDevIncentiveBonus($devIncentiveBonus);
                    $this->M_salary_slip->setDevPercent(100);
                }

                $this->M_salary_slip->setHealthBpjs($healthBpjs);
                $this->M_salary_slip->setJkkJkm($jkkJkm);
                $this->M_salary_slip->setJht($jht);
                // $this->M_salary_slip->setJp($jp);
                $this->M_salary_slip->setEmpHealthBpjs($empHealthBpjs);
                $this->M_salary_slip->setEmpJht($empJht);
                $this->M_salary_slip->setEmpJp($empJp);
                                
                $this->M_salary_slip->setInShift($shiftAttend);
                $this->M_salary_slip->setInOff($offAttend);
                $this->M_salary_slip->setInPh($phAttend);
                /* END IN SHIFT, IN OFF, IN PH  */
                $this->M_salary_slip->setUnpaidCount($unpaidLength);
                $this->M_salary_slip->setUnpaidTotal($totalAlpa);
                $this->M_salary_slip->setNonTaxAllowance($nonTaxAllowance);
                $this->M_salary_slip->setPtkpTotal($totalPTKP);          
                $this->M_salary_slip->setPayrollGroup($payrollGroup);
                $this->M_salary_slip->setPayrollLevel($payrollLevel);
                $this->M_salary_slip->setPicInput($this->session->userdata('hris_user_id'));
                $myCurrentDate = GetCurrentDate();
                $this->M_salary_slip->setInputTime($myCurrentDate['CurrentDateTime']);
                $tst = $this->M_salary_slip->insert();
                /* END INSERT TABEL DATA SLIP */      
            } 
            /* END LOOP DATABASE */
            // $this->loadSlipByPtYearMonth($clientName, $yearPeriod, $monthPeriod);        
            
        }      
        
        // $this->load->model('m_payroll');
        // $rs = $this->m_payroll->payrollprocess($clientName, $year, $month);
        // echo json_encode($rs);
    }

    /* START ALLOWANCE PROCESS */
    public function allowanceProcess($bioId, $ptName, $yearPeriod, $monthPeriod)
    {
        // $rows = $this->M_allowance->getByClientPeriod($ptName, $yearPeriod, $monthPeriod);
        $rows = $this->M_allowance->getByBioClientPeriod($bioId, $ptName, $yearPeriod, $monthPeriod);
        foreach ($rows as $row) 
        {            
            $biodataId = $row['biodata_id'];
            $clientName = $row['client_name'];
            $year = $row['year_period'];
            $month = $row['month_period'];

            $thr = $row['thr'];
            $contractBonus = $row['contract_bonus'];
            $kpiBonus = $row['kpi_bonus'];
            $outCamp = $row['out_camp'];
            $contrctRemaining = $row['contract_remaining'];
            $productionBonus = $row['production_bonus'];
            $this->M_salary_slip->getObjectByClientPeriod($biodataId, $clientName, $year, $month);
            $slipId = $this->M_salary_slip->getSalarySlipId();
            $this->M_salary_slip->setThr($thr);
            if($clientName == 'Redpath_Timika'){
                $this->M_salary_slip->setWorkdayAdj($contrctRemaining);
                $this->M_salary_slip->setContractBonus($contractBonus);
            }
            else if($clientName == 'Pontil_Timika'){
                $this->M_salary_slip->setCcPayment($contractBonus);
                $this->M_salary_slip->setProductionBonus($productionBonus);
            }
            $this->M_salary_slip->update($slipId);            
        }
            // echo $thr; exit();
    }
    /* END ALLOWANCE PROCESS */

    /* START PAYMENT EXPORT */
    public function taxProcess($ptName, $yearPeriod, $monthPeriod)
    {
        //membuat objek

        if($ptName == "Redpath_Timika")
        {
            $strSQL  = "SELECT ss.other_allowance1 AS jumboPercent,(ss.dev_incentive_bonus*(ss.dev_percent/100))*(ss.other_allowance1/100) AS jumboVal,ss.salary_slip_id,mb.bio_rec_id,mb.full_name,mb.npwp_no,ms.nie,mb.position,ms.bank_name,ms.account_name,ms.account_no,ss.ptkp_total,ss.thr,ss.contract_bonus,ss.emp_jht,ss.emp_jp,ss.cc_payment,ss.debt_burden,ms.payroll_group,ss.tax_value,";
            $strSQL .= " (";
            $strSQL .= " CAST("; /*START CAST*/
            $strSQL .= " CASE WHEN(ss.basic_salary = ss.bs_prorate) THEN ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 ";
            $strSQL .= " ELSE ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 END + ";
            $strSQL .= " CASE WHEN(ms.is_ot_bonus = 1) THEN ss.ot_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_remote_allowance = 1) THEN ss.remote_allowance ELSE 0 END +";
            // $strSQL .= " ss.thr+";
            $strSQL .= " CASE WHEN(ms.is_dev_incentive_bonus = 1) THEN ss.dev_incentive_bonus*(ss.dev_percent/100) ELSE 0 END +";
            
            $strSQL .= " (ss.dev_incentive_bonus*(ss.dev_percent/100))*(ss.other_allowance1/100) +";

            // $strSQL .= " ss.contract_bonus+";
            $strSQL .= " ss.workday_adj+";
            $strSQL .= " ss.no_accident_bonus+";
            $strSQL .= " ss.jkk_jkm+";
            $strSQL .= " ss.health_bpjs-";
            $strSQL .= " ss.unpaid_total+";
            $strSQL .= " ss.adjust_in-";
            $strSQL .= " ss.adjust_out-";  
            // $strSQL .= " ss.tax_value-";
            $strSQL .= " ss.jkk_jkm-";
            $strSQL .= " ss.emp_jht-";
            $strSQL .= " ss.emp_health_bpjs-";
            $strSQL .= " ss.health_bpjs-";
            $strSQL .= " ss.emp_jp";
            $strSQL .= " AS DECIMAL(11,2))  "; /*END CAST*/
            $strSQL .= " ) AS total, ";

            /* START BRUTTO */
            $strSQL .= " (";
            $strSQL .= " CAST("; /*START CAST*/
            $strSQL .= " CASE WHEN(ss.basic_salary = ss.bs_prorate) THEN ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 ";
            $strSQL .= " ELSE ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 END + ";
            $strSQL .= " CASE WHEN(ms.is_ot_bonus = 1) THEN ss.ot_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_remote_allowance = 1) THEN ss.remote_allowance ELSE 0 END +";
            $strSQL .= " ss.thr+";
            $strSQL .= " CASE WHEN(ms.is_dev_incentive_bonus = 1) THEN ss.dev_incentive_bonus*(ss.dev_percent/100) ELSE 0 END +";

            $strSQL .= " (ss.dev_incentive_bonus*(ss.dev_percent/100))*(ss.other_allowance1/100) +";

            $strSQL .= " ss.contract_bonus+";
            $strSQL .= " ss.workday_adj+";
            $strSQL .= " ss.no_accident_bonus+";
            $strSQL .= " ss.jkk_jkm+";
            $strSQL .= " ss.health_bpjs-";
            $strSQL .= " ss.unpaid_total+";
            $strSQL .= " ss.adjust_in-";
            $strSQL .= " ss.adjust_out "; 
            // $strSQL .= " )*12"; /*END SETAHUNKAN*/
            $strSQL .= " AS DECIMAL(11,2))  "; /*END CAST*/
            $strSQL .= " ) AS brutto_tax ";
            /* END BRUTTO */

            $strSQL .= "FROM mst_salary ms,mst_bio_rec mb,trn_salary_slip ss ";
            $strSQL .= "WHERE ms.company_name = '".$ptName."' ";
            $strSQL .= "AND ss.month_period = '".$monthPeriod."' ";
            $strSQL .= "AND ss.year_period = '".$yearPeriod."' ";

            // $strSQL .= "AND mb.bio_rec_id = ".'"2017070253"'." ";    

            // $strSQL .= "AND ms.payroll_group = '".$payrollGroup."' ";
            $strSQL .= "AND ms.bio_rec_id = mb.bio_rec_id  ";
            $strSQL .= "AND mb.bio_rec_id = ss.bio_rec_id ";
            $strSQL .= "AND mb.is_active = 1 ";      

              
            
            $strSQL .= "ORDER BY mb.full_name ";      
        }
        else if($ptName == "Pontil_Timika")
        {           

            $strSQL  = "SELECT 0 jumboPercent,0 jumboVal,ss.salary_slip_id,mb.bio_rec_id,mb.full_name,mb.npwp_no,ms.nie,mb.position,ms.bank_name,ms.account_name,ms.account_no,ss.ptkp_total,ss.thr,ss.contract_bonus,ss.emp_jht,ss.emp_jp,ss.cc_payment,ss.debt_burden,ms.payroll_group,ss.tax_value,";
            $strSQL .= " (";
            $strSQL .= " CAST("; /*START CAST*/
            $strSQL .= " ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 + ";
            $strSQL .= " CASE WHEN(ms.is_travel = 1) THEN ss.travel_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_allowance_economy = 1) THEN ss.allowance_economy ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_incentive_bonus = 1) THEN ss.incentive_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_production = 1) THEN ss.production_bonus ELSE 0 END +";

            $strSQL .= " ss.workday_adj+";
            // $strSQL .= " ss.thr+";
            $strSQL .= " ss.jkk_jkm+";
            $strSQL .= " ss.health_bpjs-";
            $strSQL .= " ss.unpaid_total+";
            $strSQL .= " ss.adjust_in-";
            $strSQL .= " ss.adjust_out-";  
            // $strSQL .= " ss.tax_value-";
            $strSQL .= " ss.jkk_jkm-";
            $strSQL .= " ss.emp_jht-";
            $strSQL .= " ss.emp_health_bpjs-";
            $strSQL .= " ss.health_bpjs-";
            $strSQL .= " ss.emp_jp";
            $strSQL .= " AS DECIMAL(11,2))  "; /*END CAST*/
            $strSQL .= " ) AS total, ";

            /* START BRUTTO */
            $strSQL .= " (";
            $strSQL .= " CAST("; /*START CAST*/
            // $strSQL .= " ("; /*START SETAHUNKAN*/
            $strSQL .= " ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 + ";
            $strSQL .= " CASE WHEN(ms.is_shift_bonus = 1) THEN ss.shift_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_allowance_economy = 1) THEN ss.allowance_economy ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_incentive_bonus = 1) THEN ss.incentive_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_production = 1) THEN ss.production_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_travel = 1) THEN ss.travel_bonus ELSE 0 END +";
            $strSQL .= " CASE WHEN(ms.is_cc_payment = 1) THEN ss.cc_payment ELSE 0 END +"; /*ok*/
            // $strSQL .= " ss.contract_bonus+";
            $strSQL .= " ss.workday_adj+";
            $strSQL .= " ss.thr+";
            $strSQL .= " ss.jkk_jkm+";
            $strSQL .= " ss.health_bpjs-";
            $strSQL .= " ss.unpaid_total+";
            $strSQL .= " ss.adjust_in-";
            $strSQL .= " ss.adjust_out";
            // $strSQL .= " ss.tax_value-";
            // $strSQL .= " ss.jkk_jkm-";
            /*$strSQL .= " ss.emp_jht";*/
            // $strSQL .= " ss.emp_health_bpjs ";
            // $strSQL .= " ss.health_bpjs";
            // $strSQL .= " )*12"; /*END SETAHUNKAN*/
            $strSQL .= " AS DECIMAL(11,2))  "; /*END CAST*/
            $strSQL .= " ) AS brutto_tax ";
            /* END BRUTTO */

            $strSQL .= "FROM mst_salary ms,mst_bio_rec mb,trn_salary_slip ss ";
            $strSQL .= "WHERE ms.company_name = '".$ptName."' ";
            $strSQL .= "AND ss.month_period = '".$monthPeriod."' ";
            $strSQL .= "AND ss.year_period = '".$yearPeriod."' ";
            // $strSQL .= "AND ms.payroll_group = '".$payrollGroup."' ";
            $strSQL .= "AND ms.bio_rec_id = mb.bio_rec_id  ";
            $strSQL .= "AND mb.bio_rec_id = ss.bio_rec_id ";
            $strSQL .= "AND mb.is_active = 1 ";   
            $strSQL .= "ORDER BY mb.full_name ";      
        }

        // echo $strSQL; exit(0); 

        $query = $this->db->query($strSQL)->result_array();   
        $bruttoTax = 0;
        /* TAX CONFIG */
        // $this->load->model('M_payroll_config');
        $myConfigId = 0;    
        switch ($ptName) {
            case 'Redpath_Timika':
                # code...
                $myConfigId = 1;
                break;
            case 'Pontil_Timika':
                # code...
                $myConfigId = 2;
                break;          
            default:
                # code...
                break;
        }
        $row = $this->M_payroll_config->getObjectById($myConfigId); 
        $taxPercent1 = $this->M_payroll_config->getTaxPercent1(); /* Persentase Progresif Pajak I */
        $taxPercent2 = $this->M_payroll_config->getTaxPercent2(); /* Persentase Progresif Pajak II */
        $taxPercent3 = $this->M_payroll_config->getTaxPercent3(); /* Persentase Progresif Pajak III */
        $taxPercent4 = $this->M_payroll_config->getTaxPercent4(); /* Persentase Progresif Pajak IV */
        $maxTaxVal1 = $this->M_payroll_config->getTaxMaxVal1(); /* Nominal Pajak I */ 
        $maxTaxVal2 = $this->M_payroll_config->getTaxMaxVal2(); /* Nominal Pajak II */
        $maxTaxVal3 = $this->M_payroll_config->getTaxMaxVal3(); /* Nominal Pajak III */
        $maxTaxVal4 = $this->M_payroll_config->getTaxMaxVal4(); /* Nominal Pajak IV */
        $maxNonTax = $this->M_payroll_config->getNonTaxAllowance(); /* Max Tunjangan Jabatan */

        $rowIdx = 8;
        $rowNo = 0;
        foreach ($query as $row) {
            $rowIdx++;
            $rowNo++;
            /* START UPDATE TAX */
            $bruttoTax = $row['brutto_tax'];
            
            $pembulatanPenghasilan = $bruttoTax;
            $nonTaxAllowance = ($pembulatanPenghasilan) * (5/100);
            if($nonTaxAllowance > $maxNonTax){
                $nonTaxAllowance = $maxNonTax;
            }

            $netto = $bruttoTax - $row['emp_jht'] - $row['emp_jp'] - $nonTaxAllowance;

            $unFixedIncome = 0;
            if($ptName == "Pontil_Timika"){
                $unFixedIncome = $row['thr'] + $row['cc_payment'];
            }else if($ptName == "Redpath_Timika"){
                $unFixedIncome = $row['thr'] + $row['contract_bonus'];
            }

            // $nettoSetahun = ( ($netto - $unFixedIncome)*12 ) + $unFixedIncome;
            $nettoSetahun = ( ($netto - $unFixedIncome)*12 );
            $monthlyTax = 0;

            $taxVal1 = 0;    
            $taxVal2 = 0;    
            $taxVal3 = 0;    
            $taxVal4 = 0;

            $tVal = 0;
            $tSisa = 0;
            $pembulatanPenghasilan = $nettoSetahun - $row['ptkp_total'];
            if($pembulatanPenghasilan > 0)
            {
                /* TAX 1 */
                if($maxTaxVal1 > 0)
                {
                    $tVal = $pembulatanPenghasilan/$maxTaxVal1;  
                    if($tVal >= 1){
                        $taxVal1 = $maxTaxVal1 * ($taxPercent1/100);
                    }
                    else{
                        $taxVal1 = $pembulatanPenghasilan * ($taxPercent1/100);
                    }    
                }    
                
                /* TAX 2 */
                if($maxTaxVal2 > 0)
                {
                    if($pembulatanPenghasilan > $maxTaxVal1)
                    {
                        $tSisa = $pembulatanPenghasilan - $maxTaxVal1; 
                        $tVal = $tSisa/$maxTaxVal2;
                        if($tVal >= 1){
                            $taxVal2 = $maxTaxVal2 * ($taxPercent2/100);
                        }
                        else{
                            $taxVal2 = $tSisa * ($taxPercent2/100);
                        }
                    }     
                }
                 
                /* TAX 3 */
                if($maxTaxVal3 > 0)
                {
                    if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2))
                    {
                        $tSisa = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2;
                        $tVal = $tSisa/$maxTaxVal3;
                        if($tVal >= 1){
                            $taxVal3 = $maxTaxVal3 * ($taxPercent3/100);
                        }
                        else{
                            $taxVal3 = $tSisa * ($taxPercent3/100);
                        }
                    }    
                }
                 
                /* TAX 4 */
                if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2 + $maxTaxVal3))
                {
                    $tSisa = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2 - $maxTaxVal3;
                    $taxVal4 = $tSisa * ($taxPercent4/100); 
                }               
            }

            /* END THR - BONUS TAX -- EDITED @ AUG 15, 2017 BY FINANCE */
            $taxTotalFix = $taxVal1 + $taxVal2 + $taxVal3 + $taxVal4;
            $taxTotal = ( $taxTotalFix/12 );
            if($taxTotal > 0){
                $monthlyTax = $taxTotal; 
            }else{
                $monthlyTax = 0; 
            }
            
           



            





            $nettoSetahunUnFix = ( ($netto - $unFixedIncome)*12 ) + $unFixedIncome;

            $taxTotalUnFix = 0;
            $taxValUnFix1 = 0;    
            $taxValUnFix2 = 0;    
            $taxValUnFix3 = 0;    
            $taxValUnFix4 = 0;

            $tValUnFix = 0;
            $tSisaUnFix = 0;
            $pembulatanPenghasilan = $nettoSetahunUnFix - $row['ptkp_total'];
            if($pembulatanPenghasilan > 0)
            {
                /* TAX 1 */
                if($maxTaxVal1 > 0)
                {
                    $tValUnFix = $pembulatanPenghasilan/$maxTaxVal1;  
                    if($tValUnFix >= 1){
                        $taxValUnFix1 = $maxTaxVal1 * ($taxPercent1/100);
                    }
                    else{
                        $taxValUnFix1 = $pembulatanPenghasilan * ($taxPercent1/100);
                    }    
                }    
                
                /* TAX 2 */
                if($maxTaxVal2 > 0)
                {
                    if($pembulatanPenghasilan > $maxTaxVal1)
                    {
                        $tSisaUnFix = $pembulatanPenghasilan - $maxTaxVal1; 
                        $tValUnFix = $tSisaUnFix/$maxTaxVal2;
                        if($tValUnFix >= 1){
                            $taxValUnFix2 = $maxTaxVal2 * ($taxPercent2/100);
                        }
                        else{
                            $taxValUnFix2 = $tSisaUnFix * ($taxPercent2/100);
                        }
                    }     
                }
                 
                /* TAX 3 */
                if($maxTaxVal3 > 0)
                {
                    if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2))
                    {
                        $tSisaUnFix = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2;
                        $tValUnFix = $tSisaUnFix/$maxTaxVal3;
                        if($tValUnFix >= 1){
                            $taxValUnFix3 = $maxTaxVal3 * ($taxPercent3/100);
                        }
                        else{
                            $taxValUnFix3 = $tSisaUnFix * ($taxPercent3/100);
                        }
                    }    
                }
                 
                /* TAX 4 */
                if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2 + $maxTaxVal3))
                {
                    $tSisaUnFix = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2 - $maxTaxVal3;
                    $taxValUnFix4 = $tSisaUnFix * ($taxPercent4/100); 
                }               
            }

            /* END THR - BONUS TAX -- EDITED @ AUG 15, 2017 BY FINANCE */

            $taxTotalUnFix = ( ($taxValUnFix1 + $taxValUnFix2 + $taxValUnFix3 + $taxValUnFix4) - $taxTotalFix );

            
            $npwpNo = $row['npwp_no'];
            $npwpCharge = 0;
            if( strlen($npwpNo) < 19 ){
                $npwpCharge = $this->M_payroll_config->getNpwpCharge();
                $monthlyTax = $monthlyTax + ($monthlyTax * ($npwpCharge/100) ); 
                $taxTotalUnFix = $taxTotalUnFix + ($taxTotalUnFix * ($npwpCharge/100) ); 
            }
            $monthlyTax += $taxTotalUnFix; 
            // echo $monthlyTax; exit(0);

            
            $tmpTotal = $row['total'] - floor($monthlyTax) - $row['debt_burden'];
            $totalTerima = 0;
            if ($tmpTotal > 0)
            {
                $totalTerima = $tmpTotal;
            }

            /* START UPDATE TAX VALUE TO THE TABLE */
            if($row['tax_value'] <= 0)
            {
                $slipId = $row['salary_slip_id'];
                $str = "UPDATE trn_salary_slip SET tax_value = ".$monthlyTax." WHERE salary_slip_id = '".$slipId."' ";
                $this->db->query($str);                
            }
            /* END UPDATE TAX VALUE TO THE TABLE */

            /* END UPDATE TAX */
            /* SET ROW COLOR */

        } /* end foreach ($query as $row) */       
    }
    /* END PAYMENT EXPORT */

    public function getPayrollList($pt, $year, $month, $deptGroup='All')
    {
        $this->db->select('*');
        $this->db->from($this->db->database.'.trn_salary_slip');
        $this->db->where('client_name', $pt);

        $tDept = $this->db->escape_str($deptGroup);
        
        if($pt == 'Redpath_Timika'){
            if($deptGroup != 'All'){
                $this->db->where('dept', $this->security->xss_clean($tDept));
            }            
        }

        else if($pt == 'Pontil_Timika'){
            if($deptGroup != 'All'){
                $this->db->where('payroll_group', $this->security->xss_clean($tDept));
            }            
        }
        

        // if( ($pt == "AMNT_Sumbawa" || $pt == 'Machmahon_Sumbawa' || $pt == "Trakindo_Sumbawa") && $dept != "")
        // {
        //     $this->db->where('dept', $this->security->xss_clean($dept));
        // }
        $this->db->where('year_period', $this->security->xss_clean($year));
        $this->db->where('month_period', $this->security->xss_clean($month));
        $this->db->order_by('name',"asc");
        $query = $this->db->get()->result_array();
        // echo $this->db->last_query(); exit(0);
        // echo $this->db->last_query(); exit(0);
        /*return json_encode($query);*/
        $myData = array();
        foreach ($query as $key => $row) {
            $myData[] = array(
                $row['salary_slip_id'],         
                $pt,         
                $row['name'],         
                // $row['production_bonus'],         
                // $row['workday_adj'],         
                // $row['adjust_in'],         
                // $row['adjust_out'],         
                $row['dept'],         
                $row['position']         
                // $row['attendance_bonus'],         
                // $row['other_allowance1'],         
                // $row['other_allowance2'],         
                // $row['cc_payment'],         
                // $row['thr'],         
                // $row['debt_burden'],
                // $row['debt_explanation']
            );            
        }  
        echo json_encode($myData);  
        // echo $this->db->last_query(); 
    }



}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	// Load library phpspreadsheet
    require('./vendor/autoload.php');
    use PhpOffice\PhpSpreadsheet\Helper\Sample;
    use PhpOffice\PhpSpreadsheet\IOFactory;
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    // End load library phpspreadsheet

class SptUpload extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct(); 
		 
		$this->load->model('masters/M_mst_pajak');
        $this->load->model('masters/M_mst_salary', 'M_salary'); 
        $this->load->model('masters/M_mst_bio_rec'); 
		$this->load->model('M_salary_slip');
	}

	public function upload()
    {
    	/* SPTRPTSMB201904 */
        $originalName = $_FILES['file']['name'];  
        $shortName = substr($originalName, 0, 9);
        $year = substr($originalName, 9, 4);
        // $month = substr($originalName, 13, 2);
        /* Database's Year */
        // $dbYear = right($this->db->database,4);
        // $shortName = substr($originalName, 0, 6);
        /* File's Year */
        // $year = substr($originalName, 6, 4);
        // $month = substr($originalName, 10, 2);
        $clientName = '';
        switch ($shortName) {            
          
            case 'SPTPTLSMB':
                $clientName = 'Pontil_Sumbawa';
                break;
            case 'SPTPTLBWG':
                $clientName = 'Pontil_Banyuwangi';
                break;
            case 'SPTAMTSMB':
                $clientName = 'AMNT_Sumbawa';
                break; 
            case 'SPTRPTTMK':
                $clientName = 'Redpath_Timika';
                break;      
                      
            default:
                $clientName = '';
                break;
        } 
        
        if($clientName == '')
        {
            echo 'Import Data Failed, Please Check File Format';
            exit(0);
        }

        // $dataCount = $this->M_process_closing->getCountByClientPeriod($clientName, $year, $month);
        // if($dataCount >= 1)
        // {
        //     echo 'Data with the same period has been uploaded before';
        //     exit(0);
        //     // redirect('content/detail/ird');
        // }
        $fileDir = realpath(APPPATH.'../uploads/'); //buat folder dengan nama uploads di root folder 
        // $fileDir = '../uploads/'; //buat folder dengan nama uploads di root folder 
        // $fileDir = $_SERVER['DOCUMENT_ROOT']; //buat folder dengan nama uploads di root folder 
        // echo FCPATH; exit(0);
        $fileDir = FCPATH."/uploads/";

        $fileName = time().$originalName;         
        // $config['upload_path'] = './assets/'; //buat folder dengan nama assets di root folder
        $config['upload_path'] = $fileDir; 
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 10000;         

        $this->load->library('upload');
        $this->upload->initialize($config);         
        // $this->upload->do_upload('file');

        if(! $this->upload->do_upload('file'))
        {
            $this->upload->display_errors();
        }
             
        $media = $this->upload->data('file');
        /* WINDOWS PATH */
        // $inputFileName = $fileDir.'\\'.$fileName;
        $inputFileName = $fileDir.$fileName;
        // $inputFileName = $fileDir.'/'.$fileName;
        // echo $inputFileName; exit(0);
        try {
                // $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                // $reader->setReadFilter( new MyReadFilter() );
                $objPHPExcel = $reader->load($inputFileName);
                // $inputFileType = IOFactory::identify($inputFileName);                
                // $objReader = IOFactory::createReader($inputFileType);
                // $objPHPExcel = $objReader->load($inputFileName);                

        } catch(Exception $e) {
            die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
        }
 
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
        $dataArr = array();

        $failedNames = array();
        $arrIdx = 0;
          
        $dataCount = 0;
        $noBiodataId = 0;
        // $sptId =0;

        $tData = '';
        $tmpData = '';
        // $masaPajak = '';
        $nie = '';
        /* Start Of TRANSACTION */  
        $this->db->trans_begin();
        /* START UPLOAD AMNT Sumbawa Staff, Trakindo Sumbawa */
        if($clientName == 'Pontil_Sumbawa' || 'AMNT_Sumbawa' || 'Redpath_Timika' || 'Pontil_Timika')
        { 

            $dataCount = 0;
            $noBiodataId = 0;
            $sptId = 0;
            for ($row = 8; $row <= $highestRow; $row++)  //  Read a row of data into an array
            {
                 $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                            NULL,
                                            TRUE,
                                            FALSE);
                //badge no
                $nie = $rowData[0][1];
                $nie = preg_replace('/[\r\n]+/','', $nie);
                $nie = trim($nie);
                if($nie != '')
                {
                    $this->M_mst_pajak->resetValues();                    
                    $this->M_mst_pajak->setSptId($this->security->xss_clean($this->M_mst_pajak->GenerateNumber()));   
                    $salaryRow = $this->M_salary->loadBioIdByNie($nie);

                    
                    $bioId = $salaryRow['bio_rec_id'];      

                    $this->M_mst_pajak->deleteByIdPeriod($bioId, $nie, $clientName,$year); //delete bio id, nie, clientname pd saat upload agr tidak double data

                    if($bioId == "" || $bioId == null){
                        $noBiodataId++;
                        /* GET ERROR DATA */
                        $failedNames[$arrIdx] = $rowData[0][9];
                        $arrIdx++;
                    }else{
                        $this->M_mst_pajak->setBioRecId($this->security->xss_clean($bioId));
                    }                     
                    $this->M_mst_pajak->setBadgeNo($nie);

                    $this->M_mst_pajak->setClientName($this->security->xss_clean($clientName));

                        //masa pajak
                        $tData = $rowData[0][2]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_mst_pajak->setMasaPajak($this->security->xss_clean("0"));   
                        }else{
                            $this->M_mst_pajak->setMasaPajak($this->security->xss_clean($tData));
                        }

                        //tahun pajak
                        $tData = $rowData[0][3]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_mst_pajak->setTahunPajak($this->security->xss_clean("0"));   
                        }else{
                            $this->M_mst_pajak->setTahunPajak($this->security->xss_clean($tData));
                        }

                        //pembetulan
                        $tData = $rowData[0][4]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_mst_pajak->setPembetulan($this->security->xss_clean("0"));   
                        }else{
                            $this->M_mst_pajak->setPembetulan($this->security->xss_clean($tData));
                        }

                        //no bukti potong
                        $tData = $rowData[0][5]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_mst_pajak->setNoBuktiPotong($this->security->xss_clean("0"));   
                        }else{
                            $this->M_mst_pajak->setNoBuktiPotong($this->security->xss_clean($tData));
                        }

                        // masa awal
                        $tData = $rowData[0][6]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_mst_pajak->setMasaAwal($this->security->xss_clean("0"));   
                        }else{
                            $this->M_mst_pajak->setMasaAwal($this->security->xss_clean($tData));
                        }

                        // masa akhir
                        $tData = $rowData[0][7]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_mst_pajak->setMasaAkhir($this->security->xss_clean("0"));   
                        }else{
                            $this->M_mst_pajak->setMasaAkhir($this->security->xss_clean($tData));
                        }                    

                        // no npwp
                        $tData = (int)$rowData[0][8]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_mst_pajak->setNpwpNo($this->security->xss_clean("0"));   
                        }else{
                            $this->M_mst_pajak->setNpwpNo($this->security->xss_clean($tData));
                        }

                        // nik
                        $tData = (int)$rowData[0][9]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_mst_pajak->setNik($this->security->xss_clean("0"));   
                        }else{
                            $this->M_mst_pajak->setNik($this->security->xss_clean($tData));
                        }  

                        // full name
                        $tData = $rowData[0][10]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_mst_pajak->setFullName($this->security->xss_clean("0"));   
                        }else{
                            $this->M_mst_pajak->setFullName($this->security->xss_clean($tData));
                        }

                        // address
                        $tData = $rowData[0][11]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_mst_pajak->setAddress($this->security->xss_clean("0"));   
                        }else{
                            $this->M_mst_pajak->setAddress($this->security->xss_clean($tData));
                        }

                        // gender
                        $tData = $rowData[0][12]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_mst_pajak->setGender($this->security->xss_clean("0"));   
                        }else{
                            $this->M_mst_pajak->setGender($this->security->xss_clean($tData));
                        }

                        // status ptkp
                        $tData = $rowData[0][13]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_mst_pajak->setStatusPtkp($this->security->xss_clean("0"));   
                        }else{
                            $this->M_mst_pajak->setStatusPtkp($this->security->xss_clean($tData));
                        }

                        // jumlah tanggungan
                        $tData = $rowData[0][14]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_mst_pajak->setJumlahTanggungan($this->security->xss_clean("0"));   
                        }else{
                            $this->M_mst_pajak->setJumlahTanggungan($this->security->xss_clean($tData));
                        }

                        // position
                        $tData = $rowData[0][15]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_mst_pajak->setPosition($this->security->xss_clean("0"));   
                        }else{
                            $this->M_mst_pajak->setPosition($this->security->xss_clean($tData));
                        }

                        // wp luar negri
                        $tData = $rowData[0][16]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_mst_pajak->setWpLuarNegri($this->security->xss_clean("0"));   
                        }else{
                            $this->M_mst_pajak->setWpLuarNegri($this->security->xss_clean($tData));
                        }

                        // country code
                        $tData = $rowData[0][17]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_mst_pajak->setCountryCode($this->security->xss_clean("0"));   
                        }else{
                            $this->M_mst_pajak->setCountryCode($this->security->xss_clean($tData));
                        }

                        // tax code
                        $tData = $rowData[0][18]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_mst_pajak->setTaxCode($this->security->xss_clean("0"));   
                        }else{
                            $this->M_mst_pajak->setTaxCode($this->security->xss_clean($tData));
                        }

                        // total 1
                        $tData = $rowData[0][19]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_mst_pajak->setTotal1($this->security->xss_clean("0"));   
                        }else{
                            $this->M_mst_pajak->setTotal1($this->security->xss_clean($tData));
                        }

                        // total 2
                        $tData = $rowData[0][20]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_mst_pajak->setTotal2($this->security->xss_clean("0"));   
                        }else{
                            $this->M_mst_pajak->setTotal2($this->security->xss_clean($tData));
                        }

                        // total 3
                        $tData = $rowData[0][21]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_mst_pajak->setTotal3($this->security->xss_clean("0"));   
                        }else{
                            $this->M_mst_pajak->setTotal3($this->security->xss_clean($tData));
                        }

                        // total 4
                        $tData = $rowData[0][22]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_mst_pajak->setTotal4($this->security->xss_clean("0"));   
                        }else{
                            $this->M_mst_pajak->setTotal4($this->security->xss_clean($tData));
                        }

                        // total 5
                        $tData = $rowData[0][23]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_mst_pajak->setTotal5($this->security->xss_clean("0"));   
                        }else{
                            $this->M_mst_pajak->setTotal5($this->security->xss_clean($tData));
                        }

                        // total 6
                        $tData = $rowData[0][24]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_mst_pajak->setTotal6($this->security->xss_clean("0"));   
                        }else{
                            $this->M_mst_pajak->setTotal6($this->security->xss_clean($tData));
                        }

                        // total 7
                        $tData = $rowData[0][25]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_mst_pajak->setTotal7($this->security->xss_clean("0"));   
                        }else{
                            $this->M_mst_pajak->setTotal7($this->security->xss_clean($tData));
                        }

                        // total 8
                        $tData = $rowData[0][26]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_mst_pajak->setTotal8($this->security->xss_clean("0"));   
                        }else{
                            $this->M_mst_pajak->setTotal8($this->security->xss_clean($tData));
                        }

                        // total 9
                        $tData = $rowData[0][27]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_mst_pajak->setTotal9($this->security->xss_clean("0"));   
                        }else{
                            $this->M_mst_pajak->setTotal9($this->security->xss_clean($tData));
                        }

                        // total 10
                        $tData = $rowData[0][28]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_mst_pajak->setTotal10($this->security->xss_clean("0"));   
                        }else{
                            $this->M_mst_pajak->setTotal10($this->security->xss_clean($tData));
                        }

                        // Total 11
                        $tData = $rowData[0][29]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_mst_pajak->setTotal11($this->security->xss_clean("0"));   
                        }else{
                            $this->M_mst_pajak->setTotal11($this->security->xss_clean($tData));
                        }

                        // Total 12
                        $tData = $rowData[0][30]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_mst_pajak->setTotal12($this->security->xss_clean("0"));   
                        }else{
                            $this->M_mst_pajak->setTotal12($this->security->xss_clean($tData));
                        }

                        // Total 13
                        $tData = $rowData[0][31]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_mst_pajak->setTotal13($this->security->xss_clean("0"));   
                        }else{
                            $this->M_mst_pajak->setTotal13($this->security->xss_clean($tData));
                        }

                        // Total 14
                        $tData = $rowData[0][32]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_mst_pajak->setTotal14($this->security->xss_clean("0"));   
                        }else{
                            $this->M_mst_pajak->setTotal14($this->security->xss_clean($tData));
                        }

                        // Total 15
                        $tData = $rowData[0][33]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_mst_pajak->setTotal15($this->security->xss_clean("0"));   
                        }else{
                            $this->M_mst_pajak->setTotal15($this->security->xss_clean($tData));
                        }

                        // Total 16
                        $tData = $rowData[0][34]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_mst_pajak->setTotal16($this->security->xss_clean("0"));   
                        }else{
                            $this->M_mst_pajak->setTotal16($this->security->xss_clean($tData));
                        }

                        //Total 17
                        $tData = $rowData[0][35]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_mst_pajak->setTotal17($this->security->xss_clean("0"));   
                        }else{
                            $this->M_mst_pajak->setTotal17($this->security->xss_clean($tData));
                        }

                        // Total 18
                        $tData = $rowData[0][36]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_mst_pajak->setTotal18($this->security->xss_clean("0"));   
                        }else{
                            $this->M_mst_pajak->setTotal18($this->security->xss_clean($tData));
                        }

                        // Total 19
                        $tData = $rowData[0][37]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_mst_pajak->setTotal19($this->security->xss_clean("0"));   
                        }else{
                            $this->M_mst_pajak->setTotal19($this->security->xss_clean($tData));
                        }

                        // Total 20
                        $tData = $rowData[0][38]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_mst_pajak->setTotal20($this->security->xss_clean("0"));   
                        }else{
                            $this->M_mst_pajak->setTotal20($this->security->xss_clean($tData));
                        }

                        // status pindah
                        $tData = $rowData[0][39]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_mst_pajak->setStatusPindah($this->security->xss_clean("0"));   
                        }else{
                            $this->M_mst_pajak->setStatusPindah($this->security->xss_clean($tData));
                        }

                        // npwp pemotong
                        $tData = $rowData[0][40]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_mst_pajak->setNpwpPemotong($this->security->xss_clean("0"));   
                        }else{
                            $this->M_mst_pajak->setNpwpPemotong($this->security->xss_clean($tData));
                        }

                        // nama pemotong
                        $tData = $rowData[0][41]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_mst_pajak->setNamaPemotong($this->security->xss_clean("0"));   
                        }else{
                            $this->M_mst_pajak->setNamaPemotong($this->security->xss_clean($tData));
                        }

                        // tanggal bukti pemotong
                        $tData = $rowData[0][42]; 
                        $tData = preg_replace('/[\r\n]+/','', $tData);
                        $tData = trim(strtoupper($tData));
                        if( ($tData == null) || (trim($tData) == "") ){
                            $this->M_mst_pajak->setTanggalBuktiPemotong($this->security->xss_clean("0"));   
                        }else{
                            $this->M_mst_pajak->setTanggalBuktiPemotong($this->security->xss_clean($tData));
                        }

                       
                        // test($tData,1);

                    $this->M_mst_pajak->setPicProcess($this->security->xss_clean($this->session->userdata('hris_user_id')));
                    $myCurrentDate = GetCurrentDate();
                    $curDateTime = $myCurrentDate['CurrentDateTime'];

                    $this->M_mst_pajak->setProcessTime($this->security->xss_clean($curDateTime));
                    $this->M_mst_pajak->insert();
        			// echo $year.'- '.$month.' '.$dataCount; exit(0);
                    $dataCount++; 
                    // test($dataCount,1)
                }                                
                
            } // for ($row = 2; $row <= $highestRow; $row++)   
            // echo $tmpData;                        
            
        } //if $clientName = 'AMNT_Sumbawa_Staf'

        /* End Of TRANSACTION */  
        if ($this->db->trans_status() === FALSE || $noBiodataId > 0)
        {
           $this->db->trans_rollback();
           // $this->session->set_flashdata('rosterMsg', 'Gagal Import Data, Cek Kembali Format File');
          echo 'Unknown Data : '.$noBiodataId.', Import Data Failed, Please Check File Format';
        }
        else
        {
           $this->db->trans_commit();
           // $this->session->set_flashdata('rosterMsg', 'Import Data Berhasil : '.$dataCount.', Tanpa Biodata Id : '.$noBiodataId);
           echo 'Import Data Succeed : '.$dataCount;
        }  

            
    }
}
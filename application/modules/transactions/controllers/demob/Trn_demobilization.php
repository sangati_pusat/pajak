<?php
/*********************************************************************
 *  Created By       : Generator Version 1.0.23                      *
 *  Created Date     : Feb 27, 2020                                 *
 *  Description      : All code generated by controller generator    *
 *  Generator Author : Tommy Maurice(tommy_maurice@yahoo.com)        *
 *********************************************************************/
/*********************************************************************
 * To use XSS Filter, Altering                                       *
 * your application/config/autoload.php file in the following way:   *
 * $autoload['helper']=array('security');                            *
 * Example : $data = $this->security->xss_clean($data);              *
 *********************************************************************/
defined('BASEPATH') OR exit('No direct script access allowed');
class Trn_demobilization extends CI_Controller
{
     /* START CONSTRUCTOR */
     public function __construct()
     {
     	parent::__construct();
     	  $this->load->helper('security');
     	  $this->load->model('M_trn_demobilization');
          $this->load->model('masters/M_mst_bio_rec');
          // $this->load->model("masters/M_mst_salary");
          // $this->load->model('masters/M_mst_client');
          // $this->load->model('masters/M_mst_contract');
     }
     /* END CONSTRUCTOR */

     public function index()
      {
        $rows = $this->M_mst_bio_rec->loadAllDemob();
        $myData = array();
        foreach ($rows as $row) {
            $myData[] = array(
                $row['bio_rec_id'], 
                $row['full_name'],    
                $row['position'],
                $row['company_name']

            );            
        }
        echo json_encode($myData);    
      }
     
    public function All()
      {
        $rows = $this->M_mst_bio_rec->loadAllDemob();
        $myData = array();
        foreach ($rows as $row) {
            $myData[] = array(
                $row['bio_rec_id'], 
                // $row['nie'],        
                $row['full_name']    
            );            
        }
        echo json_encode($myData);    
      }


    public function loadData()
    {        
        $rows = $this->M_trn_demobilization->getAll();
        $myData = array();

        foreach ($rows as $row) {
            $myData[] = array(
                // $row['salary_id'], kek man
                $row['bio_rec_id'],       
                $row['full_name'],
                $row['position'],    
                $row['client_name'],
                $row['demob_date'],
                $row['demob_site'],
                $row['demob_status'],
                $row['remarks']            
            );            
        }
        // $this->test($myData,1);
        echo json_encode($myData);    
    }

     /* START INSERT */
     public function ins()
     {
        $userInput      = $this->session->userdata('hris_user_id'); 
        $myCurrentDate  = GetCurrentDate();
        // test($_POST['demobStatus'],1);
     	$this->M_trn_demobilization->setBioRecId($this->security->xss_clean($_POST['bioRecId']));
     	$this->M_trn_demobilization->setFullName($this->security->xss_clean($_POST['fullName']));
        $this->M_trn_demobilization->setPosition($this->security->xss_clean($_POST['position']));
     	$this->M_trn_demobilization->setClientName($this->security->xss_clean($_POST['clientName']));
        $this->M_trn_demobilization->setDemobDate($this->security->xss_clean($_POST['demobDate']));
     	$this->M_trn_demobilization->setDemobSite($this->security->xss_clean($_POST['demobSite']));
     	$this->M_trn_demobilization->setDemobStatus($this->security->xss_clean($_POST['demobStatus']));
     	$this->M_trn_demobilization->setRemarks($this->security->xss_clean($_POST['remarks']));
     	$this->M_trn_demobilization->setIsActive($this->security->xss_clean('1'));
     	$this->M_trn_demobilization->setPicInput($this->security->xss_clean($this->session->userdata('hris_user_id')));
        $curDateTime = $myCurrentDate['CurrentDateTime'];
     	$this->M_trn_demobilization->setInputTime($this->security->xss_clean($curDateTime));
  
     	$this->M_trn_demobilization->insert();
     }
     /* END INSERT */

     /* START UPDATE */
     public function upd()
     {    
        $currFullDate   = GetCurrentDate();
        $curDateTime    = $currFullDate['CurrentDateTime'];
        $bioRecId       = ($this->security->xss_clean($_POST['bioRecId']));
        $demobDate      = ($this->security->xss_clean($_POST['demobDate']));
        $demobStatus    = ($this->security->xss_clean($_POST['demobStatus']));
        $remarks        = ($this->security->xss_clean($_POST['remarks']));
        $sqldept = '';
        // test($demobStatus,1);
        $sql            = "  UPDATE trn_demob SET demob_date = '".$demobDate."', demob_status = '".$demobStatus."', remarks = '".$remarks."', edit_time = '".$curDateTime."', pic_edit = '".$this->session->userdata('hris_user_id')."' WHERE bio_rec_id='".$bioRecId."' ";

        if($demobStatus == 'Active' || $demobStatus == 'Transfer To' || $demobStatus == 'Lay Off' || $demobStatus == 'Standby' || $demobStatus == 'Contract Completion' ){
            $sqldept .= 'is_active = 1, is_blacklist = 0 ';
        }
        elseif ($demobStatus == 'Blacklist') {
            $sqldept .= 'is_active = 0, is_blacklist = 1 ';
        }
        elseif ($demobStatus == 'Resignation' || $demobStatus == 'Termination' || $demobStatus == 'Unfit Location') {
            $sqldept .= 'is_active = 0, is_blacklist = 0 ';
        }
        else{
            $sqldept ='';
        }
        $sql1            = "UPDATE mst_bio_rec SET ".$sqldept.", edit_time = '".$curDateTime."', pic_edit = '".$this->session->userdata('hris_user_id')."' WHERE bio_rec_id='".$bioRecId."' ";
          // test($sql,1);
        $query1 = $this->db->query($sql1);
        $query = $this->db->query($sql);
        // return $query1;
      
      }
     /* END UPDATE */

    public function del()
    {        
        if(isset($_POST['idDelete']))
        {
            $this->M_trn_demobilization->delete($this->security->xss_clean($_POST['idDelete']));
            echo "Data  ".$_POST['idDelete']."  has been deleted";
        }
    }

    /* START GET ALL */
    public function loadAll()
    {
     	$rs = $this->M_trn_demobilization->getAll();
     	echo json_encode($rs); 
    }
    /* END GET ALL */
}


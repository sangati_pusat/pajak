<style type="text/css">
tr.selected {
  background-color: #B0BED9!important;
}
</style>

<div id="loader"></div>

<!--START FORM DATA -->
<div class="col-md-12" id="is_transaction">
  <div class="tile bg-info">
    <!-- <h3 class="tile-title">Barang Masuk</h3> -->
    <div class="tile-body">
      <form class="row is_header">
      <!-- <form class="row is_header" action="<?php #echo base_url() ?>transactions/sumbawa/Timesheet/payrollProcess"> -->
        
        <div class="form-group col-sm-12 col-md-2">
          <label class="control-label">CLIENT</label>
          <code id="docKindErr" class="errMsg"><span> : Required</span></code>
          <select class="form-control" id="clientName" name="clientName" required="">
    				<option value="" disabled="" selected="">Pilih</option>
    				<!-- <option value="Pontil_Timika">Pontil Timika</option> -->
    				<!-- <option value="Redpath_Timika">Redpath Timika</option> -->
    				<!-- <option value="Agincourt_Martabe">Agincourt Martabe</option> -->
    				<option value="Pontil_Timika">Pontil Timika</option>
            <option value="Redpath_Timika">Redpath Timika</option>
    		  </select>
        </div>

        <div class="form-group col-sm-12 col-md-2">
          <label class="control-label">YEAR</label>
          <code id="docKindErr" class="errMsg"><span> : Required</span></code>
          <select class="form-control" id="yearPeriod" name="yearPeriod" required="">
      			<option value="" disabled="" selected="">Pilih</option>
      			<script type="text/javascript">
      				var dt = new Date();
      				var currYear = dt.getFullYear();
      				var currMonth = dt.getMonth();
                      var currDay = dt.getDate();
                      var tmpDate = new Date(currYear + 1, currMonth, currDay);
                      var startYear = tmpDate.getFullYear();
      				var endYear = startYear - 80;							
      				for (var i = startYear; i >= endYear; i--) 
      				{
      					document.write("<option value='"+i+"'>"+i+"</option>");						
      				}
      			</script>
      		</select>
        </div>

        <div class="form-group col-sm-12 col-md-2">
          <label class="control-label">MONTH</label>
          <code id="docKindErr" class="errMsg"><span> : Required</span></code>
          <select class="form-control" id="monthPeriod" name="monthPeriod" required="">
    				<option value="" disabled="" selected="">Pilih</option>
    				<script type="text/javascript">
    					var tMonth = 1;
    					for (var i = tMonth; i <= 12; i++) 
    					{
    						if(i < 10)
    						{
    							document.write("<option value='0"+i+"'>0"+i+"</option>");							
    						}
    						else
    						{
    							document.write("<option value='"+i+"'>"+i+"</option>");								
    						}
    						
    					}

    				</script>
    			</select>
        </div>

        <div class="form-group col-sm-12 col-md-2">
          <label class="control-label">OT OFF </label>
          <code id="docKindErr" class="errMsg"><span> : Required</span></code>
          <select class="form-control" id="otInOffCount" name="otInOffCount" required="">
      			<option value="7">7 Jam Pertama</option>
      			<option value="8">8 Jam Pertama</option>
      			<option value="9">9 Jam Pertama</option>
      			<option value="10">10 Jam Pertama</option>
      		</select>
        </div>

        <div class="form-group col-sm-12 col-md-2">
          <label class="control-label" for="startDate">START</label>
          <select class="form-control" id="startDate" name="startDate" required="">
            <!-- <option value="" disabled="" selected="">Pilih</option> -->
            <script type="text/javascript">
              var tDay = 1;
              for (var i = tDay; i <= 31; i++) 
              {
                document.write("<option value='"+i+"'>"+i+"</option>");               
              }
            </script>
          </select> 
        </div>


        <div class="form-group col-sm-12 col-md-2 payrollGroup">
          <label class="control-label" for="payrollGroup">GROUP</label>
          <select class="form-control" id="payrollGroup" name="payrollGroup" required="">
            <option value="" disabled="" selected="">Pilih</option>
            <option value="All">All</option>
           <!--  <option value="A">A</option>
            <option value="B">B</option>
            <option value="C">C</option>
            <option value="D">D</option>
            <option value="E">E</option>
            <option value="F">F</option>
            <option value="G">G</option>
            <option value="H">H</option>
            <option value="I">I</option>
            <option value="J">J</option>
            <option value="K">K</option>
            <option value="L">L</option>
            <option value="M">M</option>
            <option value="N">N</option>
            <option value="O">O</option>
            <option value="P">P</option> -->
          </select>
        </div>


        <!-- START CUSTOMIZE GOVERNMENT REGULATION/   -->   
        <div class="form-group col-sm-12 col-md-3 govReg">     
            <input type="checkbox" id="cbHealthBPJS" name="cbHealthBPJS" checked="">
            <label class="control-label" for="healthBPJS">Health BPJS</label>&nbsp;&nbsp;&nbsp;&nbsp;

            <!-- <input type="checkbox" id="cbEmpBPJS" name="cbEmpBPJS" checked="">
            <label class="control-label" for="empBPJS">Employment BPJS</label> -->

            <input type="checkbox" id="cbJHT" name="cbJHT" checked="">
            <label class="control-label" for="JHT">JHT</label>&nbsp;&nbsp;&nbsp;&nbsp;

            <input type="checkbox" id="cbJP" name="cbJP" checked="">
            <label class="control-label" for="JP">JP</label>&nbsp;&nbsp;&nbsp;&nbsp;

            <input type="checkbox" id="cbJKKM" name="cbJKKM" checked="">
            <label class="control-label" for="JKK-JKM">JKK-JKM</label>&nbsp;&nbsp;&nbsp;&nbsp;

            <!-- <input type="checkbox" id="isById" name="isById">
            <label class="control-label" for="isById">Biodata</label>
            <input type="text" id="Biodata" name="Biodata"> -->
      

        </div>
        <!-- START CUSTOMIZE GOVERNMENT REGULATION/   -->
        

        <!-- <div class="form-group col-sm-12 col-md-6">
          <label class="control-label">Keterangan</label>
          <textarea class="form-control" id="headerInfo"></textarea>
        </div>  -->    
               
        <div class="form-group col-md-12 align-self-end">
          <button class="btn btn-warning" id="btnProsesRoster" type="button"><i class="fa fa-refresh"></i>Roster Process</button>
          <button class="btn btn-warning" id="btnDisplay" type="button"><i class="fa fa-refresh"></i>Display Data</button>
		      <br>
		      <!-- <br> -->
    	  <h3><code id="dataProses" class="backTransparent"><span></span></code></h3>	
        </div>
      </form>
    </div>
    <div class="tile bg-primary row devData">
        <div class="form-group col-sm-12 col-md-3">
          <label class="control-label" for="dept">DEPT</label>
          <select class="form-control" id="dept" name="dept" required="">
              <option value="" disabled="" selected="">Pilih</option>
              <option value="Admin">Admin</option> 
              <option value="Alimak">Alimak</option>
              <option value="Amole Rehab">Amole Rehab</option>
              <option value="Engineering">Engineering</option>
              <option value="Electric">Electric</option>
              <option value="DMLZ">DMLZ</option>
              <option value="Extrasion">Extrasion</option>
              <option value="GBC Area I">GBC Area I</option>
              <option value="GBC Area II">GBC Area II</option>
              <option value="GBC Area III">GBC Area III</option>
              <option value="MCM">MCM</option>
              <option value="Raisebore">Raisebore</option>
              <option value="Safety And Training">Safety And Training</option>
              <option value="Service Crew">Service Crew</option>
              <option value="STCP">STCP</option>
            </select>          
        </div>
        <div class="form-group col-sm-12 col-md-3">
            <label class="control-label" for="devPercent">PERSENTASE</label><br>
            <input type="number" id="devPercent" name="devPercent" placeholder="Persentase">
            &nbsp; &nbsp; &nbsp;
            <input id="btnUpdateDevPercent" class="btn btn-warning" type="button" value="Update"/>
        </div>
    </div>
  </div>


</div>
<!-- END FORM DATA -->

<!-- START DETAIL MODAL -->
<div class="modal fade" id="editPayrollModal" tabindex="-1" role="dialog" aria-labelledby="isModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-notify modal-warning" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-center" id="isModalLabel">Data Payroll</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">     

          <!-- START DATA TABLE -->
          <div class="tile-body">

                  <!-- Start Form EDIT PAYROLL -->
                  <form method="post" class="my_detail">
                      <div class="form-group">
                          <label class="control-label" for="payrollId">Slip Id</label>
                          <input class="form-control" type="text" id="payrollId" name="payrollId" placeholder="Id" readonly="">
                      </div>                     

                      <div class="form-group">
                          <label class="control-label" for="payrollName">Name</label>
                          <input class="form-control" type="text" id="payrollName" name="payrollName" placeholder="Nama Karyawan" readonly="">
                      </div>    
          
                      <div class="form-group productionBonus">
                            <label class="control-label" for="productionBonus">Production Bonus</label>
                            <input class="form-control" type="text" id="productionBonus" name="productionBonus" placeholder="Bonus Produksi">
                        </div>

                        <div class="form-group workAdjustment">
                            <label class="control-label" for="workAdjustment">Remaining Contract</label>
                            <input class="form-control" type="text" id="workAdjustment" name="workAdjustment" placeholder="Penyesuaian Hari Kerja">
                        </div>

                        <div class="form-group adjustmentIn">
                            <label class="control-label" for="adjustmentIn">Adjustment In</label>
                            <input class="form-control" type="text" id="adjustmentIn" name="adjustmentIn" placeholder="Penyesuaian Masuk">
                        </div>

                        <div class="form-group adjustmentOut">
                            <label class="control-label" for="adjustmentOut">Adjustment Out</label>
                            <input class="form-control" type="text" id="adjustmentOut" name="adjustmentOut" placeholder="Penyesuaian Keluar">
                        </div>

                        <div class="form-group contractBonus">
                            <label class="control-label" for="contractBonus">Contract Bonus</label>
                            <input class="form-control" type="text" id="contractBonus" name="contractBonus" placeholder="Bonus Kontrak">
                        </div>

                        <div class="form-group ccPayment">
                            <label class="control-label" for="ccPayment">CC Payment</label>
                            <input class="form-control" type="text" id="ccPayment" name="ccPayment" placeholder="CC Payment">
                        </div>

                        <div class="form-group outCamp">
                            <label class="control-label" for="outCamp">Out Camp</label>
                            <input class="form-control" type="text" id="outCamp" name="outCamp" placeholder="Out Camp">
                        </div>

                        <div class="form-group safetyBonus">
                            <label class="control-label" for="safetyBonus">Safety Bonus</label>
                            <input class="form-control" type="text" id="safetyBonus" name="safetyBonus" placeholder="Safety Bonus">
                        </div>

                        <div class="form-group attendanceBonus">
                            <label class="control-label" for="attendanceBonus">Attendance Bonus</label>
                            <input class="form-control" type="text" id="attendanceBonus" name="attendanceBonus" placeholder="Attendance Bonus">
                        </div>

                        <div class="form-group otherAllowance1">
                            <label class="control-label" for="otherAllowance1">Other Allowance 1</label>
                            <input class="form-control" type="text" id="otherAllowance1" name="otherAllowance1" placeholder="Other Allowance 1">
                        </div>

                        <div class="form-group otherAllowance2">
                            <label class="control-label" for="otherAllowance2">Other Allowance 2</label>
                            <input class="form-control" type="text" id="otherAllowance2" name="otherAllowance2" placeholder="Other Allowance 2">
                        </div>

                        <div class="form-group jumboPercent">
                            <label class="control-label" for="jumboPercent">Jumbo Percent</label>
                            <input class="form-control" type="number" id="jumboPercent" name="jumboPercent" placeholder="Jumbo Percent">
                        </div>

                        <div class="form-group noAccidentFee">
                            <label class="control-label" for="noAccidentFee">No Accident Fee</label>
                            <input type="checkbox" id="cbAccidentFee" name="cbAccidentFee">
                            <input class="form-control" type="text" id="noAccidentFee" name="noAccidentFee" placeholder="No Accident Fee">
                        </div>
                        
                        <div class="form-group thr">
                            <label class="control-label" for="thr">THR</label>
                            <input class="form-control" type="text" id="thr" name="thr" placeholder="THR">
                        </div>

                        <div class="form-group debtBurden">
                            <label class="control-label" for="debtBurden">Debt Value</label>
                            <input class="form-control" type="text" id="debtBurden" name="debtBurden" placeholder="Beban Hutang">
                        </div>

                        <div class="form-group debtExplanation">
                            <label class="control-label" for="debtExplanation">Debt Info</label>
                            <input class="form-control" type="text" id="debtExplanation" name="debtExplanation" placeholder="Keterangan Beban Hutang">
                        </div>

                        <div class="form-group salaryStatus">
                            <label class="control-label" for="salaryStatus">Salary Status</label>
                            <select class="form-control" id="salaryStatus" name="salaryStatus" required="">
                              <option value="" disabled="" selected="">Pilih</option>
                              <option value="Warning">Warning</option>              
                              <option value="">None</option>              
                            </select>
                        </div>   

                        <div class="form-group statusInfo">
                            <label class="control-label" for="statusInfo">Info Status</label>
                            <input class="form-control" type="text" id="statusInfo" name="statusInfo" placeholder="Keterangan Status">
                        </div>                      

                        <div class="form-group">
                            <!-- <label class="control-label" for="rowIdx">Index</label> -->
                            <input class="form-control" type="hidden" id="rowIdx" name="rowIdx" value="" readonly="">
                        </div>                                                                                                                                  
                  </form>
                  <!-- End Form EDIT PAYROLL -->

            
          </div>
          <!-- END DATA TABLE -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" id="savePayroll" data-dismiss="modal" class="btn btn-primary">Save</button>
      </div>
    </div>
  </div>
</div>
<!-- END DETAIL MODAL -->


<div class="col-md-12">
  <div class="tile bg-info">
    <!-- <h3 class="tile-title">Barang Masuk</h3> -->
    <div class="tile-body">
       <!-- START DATA TABLE -->
        <div class="tile-body">
          <table class="table table-hover table-bordered" id="slipTable">
            <thead class="thead-dark">
              <tr>
                <th>Slip Id</th>
                <th>Client Name</th>
                <!-- <th>Badge No</th> -->
                <th>Emp Name</th>
                <th>Dept</th>
                <th>Position</th>
                <th>Update</th>
                <th>Print</th>
                <th>Process</th>
              </tr>
            </thead>
            <tbody>
              <!-- <tr>
                <td>Tiger Nixon</td>
                <td>System Architect</td>
                <td>Edinburgh</td>
                <td>61</td>
                <td>2011/04/25</td>
                <td>$320,800</td>
              </tr>  -->       
            </tbody>
          </table>
        </div>
        <!-- END DATA TABLE -->
    </div>
  </div>
</div>
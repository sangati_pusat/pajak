<script type="text/javascript">
    $(document).ready(function(){
        var rowIdx = null;
        var invoiceList = $('#invoiceList').DataTable({
                "paging":   true,
                "ordering": false,
                "info":     false,
                "filter":   true
                // "columnDefs": [{
                //     "targets": -2,
                //     "data": null,
                //     "defaultContent": "<button class='btn btn-primary btn-xs btn_update' data-toggle='modal' data-target='#editPayrollModal'><span class='glyphicon glyphicon-edit'></span></button>"
                // },
                // {
                //     "targets": -1,
                //     "data": null,
                //     "defaultContent": "<button class='btn btn-primary btn-xs btn_print'><span class='glyphicon glyphicon-print'></span></button>"   
                // }]
            });
        $("#loader").hide();
        var cn = '';
        $('.dept').hide();

        var dept = '';
        var dataPrint = '';
        $('#dept').on('change', function(){
            dept = $('#dept').val();
        });

        $("#printReimbursement").hide();   
        $("#fileToPrintGroup").hide();
        $("#printInvoice").hide();

        $(".errMsg").hide();

        /* START CLICK MAIN BUTTON */
        $("#viewInvoiceList").on("click", function(){            
            var clientName = $("#clientName").val();            
            var yearPeriod = $("#yearPeriod").val();
            var monthPeriod = $("#monthPeriod").val();
            var payrollGroup = $("#payrollGroup").val();
            // var dataPrint = $("#dataPrint").val();
            dept = $('#dept').val();
            var isValid = true;
            if($('#clientName option:selected').text() == "Pilih")
            {
                $("#clientName").focus();
                $("#clientNameErr").show();
                // alert('PT Harus Dipilih ');
                isValid = false;
            } 
            else if($('#yearPeriod option:selected').text() == "Pilih")
            {
                $("#yearPeriod").focus();
                $("#yearPeriodErr").show();
                // alert('Tahun Harus Dipilih ');
                isValid = false;
            }
            else if($('#monthPeriod option:selected').text() == "Pilih")
            {
                $("#monthPeriod").focus();
                $("#monthPeriodErr").show();
                // alert('Bulan Harus Dipilih ');
                isValid = false;
            }

            if(clientName == 'Redpath_Timika'){
                if($('#dept option:selected').text() == "Pilih")
                {
                    $("#dept").focus();
                    $("#deptErr").show();
                    // alert('Bulan Harus Dipilih ');
                    isValid = false;
                }  

            }
            
            /*else if($('#payrollGroup option:selected').text() == "Pilih")
            {
                $("#payrollGroup").focus();
                alert('Group Harus Dipilih ');
                isValid = false;
            }*/

            if(isValid == false)
            {return false} 

            if(clientName == 'Pontil_Timika'){
                dept = payrollGroup;
            }

            var myUrl = '<?php echo base_url() ?>'+'transactions/timika/Timesheet/getPayrollList/'+clientName+'/'+yearPeriod+'/'+monthPeriod+'/'+dept;


            // }
            // alert(myUrl); 

            // /* PONTIL INVOICE */
            // else if (dataPrint == 'PSI') {
            //     myUrl = "<?php #echo base_url() ?>"+"timika/Invoice/exportSummaryInvoicePontil/"+clientName+"/"+yearPeriod+"/"+monthPeriod+"/"+payrollGroup;
            // }
            
            // /* PONTIL REIMBURSMENT */
            // else if (dataPrint == 'PRS') {
            //     myUrl = "<?php #echo base_url() ?>"+"timika/Invoice/exportSummaryReimbursementPontil/"+clientName+"/"+yearPeriod+"/"+monthPeriod+"/"+payrollGroup;
            // }

            $("#loader").show();
            $.ajax({
                method  : "POST",
                url : myUrl,
                data    : {
                    pt   : clientName,
                    year : yearPeriod,
                    month: monthPeriod,
                    // payrollGroup : payrollGroup,
                    dept : dept 
                },
                success : function(data){
                    // alert(data);
                    invoiceList.clear().draw();
                    var dataSrc = JSON.parse(data);                 
                    invoiceList.rows.add(dataSrc).draw(false);
                    $("#loader").hide();
                },
                error   : function(data){
                    alert(data);
                }   
            });
        });
        /* END CLICK MAIN BUTTON */

        /* START SELECT DATA */
        $("#invoiceList tbody").on("click", "tr", function(){
            var rowData = invoiceList.row( this ).data();
            if ( $(this).hasClass('selected') ) 
            {
                $(this).removeClass('selected');
            }
            else 
            {
                invoiceList.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');                    
            }
            rowIdx = invoiceList.row( this ).index();
            $("#rowIdx").val(rowIdx); 
        });
        /* END SELECT DATA */
        
        $('.payrollGroup').hide();
        $('#dataPrint').find('option:not(:first)').remove();
        $('#clientName').on('change', function(){
            var clientName = $('#clientName').val();
            $('#dataPrint').find('option:not(:first)').remove();
            $('.payrollGroup').hide();
            if(clientName == 'Redpath_Timika')
            {   
                $('#dataPrint').find('option:not(:first)').remove();            
                $('#dataPrint').append(new Option("Redpath Summary Invoice", "RSI"));
                $('#dataPrint').append(new Option("Redpath Payment List", "RPL"));

                $('.dept').show();
                $('#dept').append(new Option("All", "All"));
                $('#dept').append(new Option("Admin", "Admin"));
                $('#dept').append(new Option("Alimak", "Alimak"));
                $('#dept').append(new Option("Amole Rehab", "Amole Rehab"));
                $('#dept').append(new Option("Engineering", "Engineering"));
                $('#dept').append(new Option("Electric", "Electric"));
                $('#dept').append(new Option("DMLZ", "DMLZ"));
                $('#dept').append(new Option("Extrasion", "Extrasion"));
                $('#dept').append(new Option("GBC Area I", "GBC Area I"));
                $('#dept').append(new Option("GBC Area II", "GBC Area II"));
                $('#dept').append(new Option("GBC Area III", "GBC Area III"));
                $('#dept').append(new Option("MCM", "MCM"));
                $('#dept').append(new Option("Raisebore", "Raisebore"));
                $('#dept').append(new Option("Safety And Training", "Safety And Training"));
                $('#dept').append(new Option("Service Crew", "Service Crew"));
                $('#dept').append(new Option("STCP", "STCP"));                
                // $('#dataPrint').append(new Option("Redpath Invoice", "RIV"));
                // $('#dataPrint').append(new Option("Redpath Receipt Note", "RRN"));
            }
            else if(clientName == 'Pontil_Timika')
            {
                $('#dataPrint').find('option:not(:first)').remove();
                $('#dataPrint').append(new Option("Pontil Summary Invoice", "PSI"));   
                $('#dataPrint').append(new Option("Pontil Reimbursement Summary", "PRS"));   
                $('#dataPrint').append(new Option("Pontil Payment List", "PPL"));
                $('.dept').hide();
                $('.payrollGroup').show();
            }

        });

        $('#dataPrint').on('change', function(){
            // alert($('#dataPrint').val());
            dataPrint = $("#dataPrint").val();
        });

        /* START PRINT DATA */
        $('#printToFile').on('click', function(){
            var clientName = $("#clientName").val();
            var myDept = '';
            
            var yearPeriod = $("#yearPeriod").val();
            var monthPeriod = $("#monthPeriod").val();
            var payrollGroup = $("#payrollGroup").val();
            var dataPrint = $("#dataPrint").val();

            var isValid = true;            
            $("#loader").show();
            if($('#clientName option:selected').text() == "Pilih")
            {
                $("#clientName").focus();
                $('#clientNameErr').show();
                // alert('Client Name cannot be empty ');
                isValid = false;
            } 
            else if($('#yearPeriod option:selected').text() == "Pilih")
            {
                $("#yearPeriod").focus();
                $('#yearPeriodErr').show();
                // alert('Year cannot be empty ');
                isValid = false;
            } 
            else if($('#monthPeriod option:selected').text() == "Pilih")
            {
                $("#monthPeriod").focus();
                $('#monthPeriodErr').show();
                // alert('Month cannot be empty ');
                isValid = false;
            } 
            
             
            else if($('#clientName').val() == "Redpath_Timika")
            {
                myDept = $("#dept").val();              
            }
            else if($('#clientName').val() == "Pontil_Timika")
            {
                myDept = $("#payrollGroup").val();                
                if($('#fileToPrint option:selected').text() == "Pilih")
                {
                    $("#fileToPrint").focus();
                    $('#fileToPrintErr').show();
                    // alert('File cannot be empty');
                    isValid = false;
                } 
                if($('#payrollGroup option:selected').text() == "Pilih")
                {
                    $("#payrollGroup").focus();
                    $('#payrollGroupErr').show();
                    // alert('Group cannot be empty ');
                    isValid = false;
                } 
            }
            else if($('#dataPrint option:selected').text() == "Pilih")
            {
                $("#dataPrint").focus();
                $('#dataPrintErr').show();
                // alert('Data Print cannot be empty ');
                isValid = false;
            }
            var myUrl = '';
            
            if (dataPrint == 'RSI') {
                myUrl = "<?php echo base_url() ?>"+"transactions/timika/Invoice/exportInvoiceSummaryRedpath/"+clientName+"/"+yearPeriod+"/"+monthPeriod+"/"+myDept;
            }
            
            else if (dataPrint == 'RPL' || dataPrint == 'PPL') {
                myUrl = "<?php echo base_url() ?>"+"transactions/timika/Payroll/exportPaymentList/"+clientName+"/"+yearPeriod+"/"+monthPeriod+"/"+myDept;
            }

            else if (dataPrint == 'PSI') {
                myUrl = "<?php echo base_url() ?>"+"transactions/timika/Invoice/exportSummaryInvoicePontil/"+clientName+"/"+yearPeriod+"/"+monthPeriod+"/"+payrollGroup;
            }
            
            else if (dataPrint == 'PRS') {
                myUrl = "<?php echo base_url() ?>"+"transactions/timika/Invoice/exportSummaryReimbursementPontil/"+clientName+"/"+yearPeriod+"/"+monthPeriod+"/"+payrollGroup;
            }
            

                     
            if(isValid == false)
            {return false} 
            
            $.ajax({
                method : "POST",
                url : myUrl,
                data : {
                    clientName : clientName,
                    yearPeriod : yearPeriod,
                    monthPeriod : monthPeriod,
                    payrollGroup : payrollGroup,
                    dept : myDept 
                },
                // success : function(data){
                //     alert(data)
                // },
                success : function(response){
                    $("#loader").hide();
                    console.log(response);
                    window.open(myUrl,'_blank');
                },                
                error : function(data){
                    alert("Failed");
                }   
            });
        });
        /* END PRINT DATA */

        
        
    });
</script>
<style type="text/css">
tr.selected {
  background-color: #B0BED9!important;
}
</style>

<div id="loader"></div>

<!--START FORM DATA -->
<div class="col-md-12" id="is_transaction">
  <div class="tile bg-info">
    <!-- <h3 class="tile-title">Barang Masuk</h3> -->
    <div class="tile-body">
      <form class="row is_header">
      <!-- <form class="row is_header" action="<?php #echo base_url() ?>transactions/sumbawa/Timesheet/payrollProcess"> -->
        
        <div class="form-group col-sm-12 col-md-2">
          <label class="control-label">CLIENT</label>
          <code id="docKindErr" class="errMsg"><span> : Required</span></code>
          <select class="form-control" id="clientName" name="clientName" required="">
    				<option value="" disabled="" selected="">Pilih</option>
    				<!-- <option value="Pontil_Timika">Pontil Timika</option> -->
    				<!-- <option value="Redpath_Timika">Redpath Timika</option> -->
    				<!-- <option value="Agincourt_Martabe">Agincourt Martabe</option> -->
    				<option value="AMNT_Sumbawa">AMNT Sumbawa </option>
    				<option value="AMNT_Sumbawa_Staff">Staff AMNT Sumbawa </option>
    				<option value="Trakindo_Sumbawa">Trakindo Sumbawa</option>
    				<option value="Machmahon_Sumbawa">Machmahon Sumbawa</option>
    				<option value="LCP_Sumbawa">Lamurung Cipta Persada</option>
            <option value="Pontil_Sumbawa">Pontil Sumbawa</option>
            <option value="Pontil_Banyuwangi">Pontil Banyuwangi</option>
    		  </select>
        </div>

        <div class="form-group col-sm-12 col-md-2">
          <label class="control-label">YEAR</label>
          <code id="docKindErr" class="errMsg"><span> : Required</span></code>
          <select class="form-control" id="yearPeriod" name="yearPeriod" required="">
      			<option value="" disabled="" selected="">Pilih</option>
      			<script type="text/javascript">
      				var dt = new Date();
      				var currYear = dt.getFullYear();
      				var currMonth = dt.getMonth();
                      var currDay = dt.getDate();
                      var tmpDate = new Date(currYear + 1, currMonth, currDay);
                      var startYear = tmpDate.getFullYear();
      				var endYear = startYear - 80;							
      				for (var i = startYear; i >= endYear; i--) 
      				{
      					document.write("<option value='"+i+"'>"+i+"</option>");						
      				}
      			</script>
      		</select>
        </div>

        <div class="form-group col-sm-12 col-md-2">
          <label class="control-label">MONTH</label>
          <code id="docKindErr" class="errMsg"><span> : Required</span></code>
          <select class="form-control" id="monthPeriod" name="monthPeriod" required="">
    				<option value="" disabled="" selected="">Pilih</option>
    				<script type="text/javascript">
    					var tMonth = 1;
    					for (var i = tMonth; i <= 12; i++) 
    					{
    						if(i < 10)
    						{
    							document.write("<option value='0"+i+"'>0"+i+"</option>");							
    						}
    						else
    						{
    							document.write("<option value='"+i+"'>"+i+"</option>");								
    						}
    						
    					}

    				</script>
    			</select>
        </div>

        <div class="form-group col-sm-12 col-md-2">
          <label class="control-label">OT OFF </label>
          <code id="docKindErr" class="errMsg"><span> : Required</span></code>
          <select class="form-control" id="otInOffCount" name="otInOffCount" required="">
      			<option value="7">7 Jam Pertama</option>
      			<option value="8">8 Jam Pertama</option>
      			<option value="9">9 Jam Pertama</option>
      			<option value="10">10 Jam Pertama</option>
      		</select>
        </div>

        <div class="form-group col-sm-12 col-md-2 dept">
          <label class="control-label">DEPT </label>
          <code id="docKindErr" class="errMsg"><span> : Required</span></code>
          <select class="form-control" id="dept" name="dept" required="">
            <option value="" disabled="" selected="">Pilih</option>
            <!-- <option value="FIXED PLANT">FIXED PLANT</option>
            <option value="LINESMAN">LINESMAN</option>
            <option value="POWER PLANT">POWER PLANT</option>
            <option value="WELDER PLANT">WELDER PLANT</option>
            <option value="CAT RENTAL">CAT RENTAL</option>
            <option value="FACILITY MMA">FACILITY MMA</option>
            <option value="MAN HAUL">MAN HAUL</option>
            <option value="ENGINEERING">ENGINEERING</option> -->
          </select>
        </div>

        <div class="form-group col-sm-12 col-md-2">
          <label class="control-label" for="startDate">START</label>
          <select class="form-control" id="startDate" name="startDate" required="">
            <!-- <option value="" disabled="" selected="">Pilih</option> -->
            <script type="text/javascript">
              var tDay = 1;
              for (var i = tDay; i <= 31; i++) 
              {
                document.write("<option value='"+i+"'>"+i+"</option>");               
              }
            </script>
          </select> 
        </div>


         <div class="form-group col-sm-12 col-md-2 dataGroup">
          <label class="control-label">GROUP </label>
          <code id="docKindErr" class="errMsg"><span> : Required</span></code>
          <select class="form-control" id="dataGroup" name="dataGroup" required="">
            <!-- <option value="A" selected="">All</option>
            <option value="L">Local</option>
            <option value="N">Non Local</option> -->
          </select>
        </div>


        <!-- START CUSTOMIZE GOVERNMENT REGULATION/   -->   
        <div class="form-group col-sm-12 col-md-3 govReg">     
            <input type="checkbox" id="cbHealthBPJS" name="cbHealthBPJS" checked="">
            <label class="control-label" for="healthBPJS">Health BPJS</label>&nbsp;&nbsp;&nbsp;&nbsp;

            <!-- <input type="checkbox" id="cbEmpBPJS" name="cbEmpBPJS" checked="">
            <label class="control-label" for="empBPJS">Employment BPJS</label> -->

            <input type="checkbox" id="cbJHT" name="cbJHT" checked="">
            <label class="control-label" for="JHT">JHT</label>&nbsp;&nbsp;&nbsp;&nbsp;

            <input type="checkbox" id="cbJP" name="cbJP" checked="">
            <label class="control-label" for="JP">JP</label>&nbsp;&nbsp;&nbsp;&nbsp;

            <input type="checkbox" id="cbJKKM" name="cbJKKM" checked="">
            <label class="control-label" for="JKK-JKM">JKK-JKM</label>
        </div>
        <!-- START CUSTOMIZE GOVERNMENT REGULATION/   -->
        

        <!-- <div class="form-group col-sm-12 col-md-6">
          <label class="control-label">Keterangan</label>
          <textarea class="form-control" id="headerInfo"></textarea>
        </div>  -->    
               
        <div class="form-group col-md-12 align-self-end">
          <button class="btn btn-warning" id="btnProsesRoster" type="button"><i class="fa fa-refresh"></i>Roster Process</button>

          
          <button class="btn btn-warning" id="btnDisplay" type="button"><i class="fa fa-refresh"></i>Display Data</button>
		      <br>
		      <!-- <br> -->
    	     <h3><code id="dataProses" class="backTransparent"><span></span></code></h3>	
        </div>
        
        <div class="form-group col-md-12 align-self-end">
          <button class="btn btn-warning" id="btnGetNie" type="button"><i class="fa fa-refresh"></i>Process Id</button>
          <button class="btn btn-warning" id="btnCancelNie" type="button"><i class="fa fa-refresh"></i>Cancel Id</button>
        </div>

        <div class="form-group col-md-2 align-self-end">
          <input class="form-control" type="text" id="nie" name="nie" placeholder="Badge No">
        </div>

      </form>
    </div>
  </div>
</div>
<!-- END FORM DATA -->

<!-- START DETAIL MODAL -->
<div class="modal fade" id="editPayrollModal" tabindex="-1" role="dialog" aria-labelledby="isModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-notify modal-warning" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-center" id="isModalLabel">Data Payroll</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">     

          <!-- START DATA TABLE -->
          <div class="tile-body">

                  <!-- Start Form EDIT PAYROLL -->
                  <form method="post" class="my_detail">
                      <div class="form-group">
                          <label class="control-label" for="payrollId">Slip Id</label>
                          <input class="form-control" type="text" id="payrollId" name="payrollId" placeholder="Id" readonly="">
                      </div>                     

                      <div class="form-group">
                          <label class="control-label" for="payrollName">Name</label>
                          <input class="form-control" type="text" id="payrollName" name="payrollName" placeholder="Nama Karyawan" readonly="">
                      </div>    
          
                      <div class="form-group productionBonus">
                          <label class="control-label" for="productionBonus">Production Bonus</label>
                          <input class="form-control" type="text" id="productionBonus" name="productionBonus" placeholder="Bonus Produksi">
                      </div>

                      

                      <div class="form-group adjustmentIn">
                          <label class="control-label" for="adjustmentIn">Adjustment In</label>
                          <input class="form-control" type="text" id="adjustmentIn" name="adjustmentIn" placeholder="Penyesuaian Masuk">
                      </div>

                      <div class="form-group adjustmentOut">
                          <label class="control-label" for="adjustmentOut">Adjustment Out</label>
                          <input class="form-control" type="text" id="adjustmentOut" name="adjustmentOut" placeholder="Penyesuaian Keluar">
                      </div>

                      <div class="form-group contractBonus">
                          <label class="control-label" for="contractBonus">Contract Bonus</label>
                          <input class="form-control" type="text" id="contractBonus" name="contractBonus" placeholder="Bonus Kontrak">
                      </div>

                      <!-- <div class="form-group ccPayment">
                          <label class="control-label" for="ccPayment">CC Payment</label>
                          <input class="form-control" type="text" id="ccPayment" name="ccPayment" placeholder="CC Payment">
                      </div> -->

                      <div class="form-group outCamp">
                          <label class="control-label" for="outCamp">Out Camp</label>
                          <input class="form-control" type="text" id="outCamp" name="outCamp" placeholder="Out Camp">
                      </div>

                      <div class="form-group safetyBonus">
                          <label class="control-label" for="safetyBonus">Safety Bonus</label>
                          <input class="form-control" type="text" id="safetyBonus" name="safetyBonus" placeholder="Safety Bonus">
                      </div>

                      <div class="form-group travelBonus">
                          <label class="control-label" for="travelBonus">Travel Bonus</label>
                          <input class="form-control" type="text" id="travelBonus" name="travelBonus" placeholder="Travel Bonus">
                      </div>


                      <div class="form-group attendanceBonus">
                          <label class="control-label" for="attendanceBonus">Attendance Bonus</label>
                          <input class="form-control" type="text" id="attendanceBonus" name="attendanceBonus" placeholder="Attendance Bonus">
                      </div>

                      <div class="form-group drillingBonus">
                          <label class="control-label" for="drillingBonus">Drilling Bonus</label>
                          <input class="form-control" type="text" id="drillingBonus" name="drillingBonus" placeholder="Drilling Bonus">
                      </div>

                      <div class="form-group actManagerBonus">
                          <label class="control-label" for="actManagerBonus">Acting Manager Bonus</label>
                          <input class="form-control" type="text" id="actManagerBonus" name="actManagerBonus" placeholder="Acting Manager Bonus">
                      </div>

                      <!-- START INVOICE ONLY --> 
                      <div class="form-group general1 invoiceOnly">
                          <label class="control-label" for="general1">Mobilization</label>
                          <input class="form-control" type="text" id="general1" name="general1" placeholder="Mobilization">
                      </div>
                      <div class="form-group general2 invoiceOnly">
                          <label class="control-label" for="general2">Uniform</label>
                          <input class="form-control" type="text" id="general2" name="general2" placeholder="Uniform">
                      </div>
                      <div class="form-group general3 invoiceOnly">
                          <label class="control-label" for="general3">MCU</label>
                          <input class="form-control" type="text" id="general3" name="general3" placeholder="MCU">
                      </div>
                      <div class="form-group general4 invoiceOnly">
                          <label class="control-label" for="general4">Ticket</label>
                          <input class="form-control" type="text" id="general4" name="general4" placeholder="Ticket">
                      </div>
                      <div class="form-group general5 invoiceOnly">
                          <label class="control-label" for="general5">APD</label>
                          <input class="form-control" type="text" id="general5" name="general5" placeholder="APD">
                      </div>
                      <div class="form-group general6 invoiceOnly">
                          <label class="control-label" for="general6">Meals</label>
                          <input class="form-control" type="text" id="general6" name="general6" placeholder="Meals">
                      </div>
                      <!-- END INVOICE ONLY -->                        

                      <div class="form-group otherAllowance1">
                          <label class="control-label" for="otherAllowance1">Health Benefits</label>
                          <input class="form-control" type="text" id="otherAllowance1" name="otherAllowance1" placeholder="Other Allowance 1">
                      </div>

                      <div class="form-group othrAllowanceTrakindo">
                          <label class="control-label" for="othrAllowanceTrakindo">Other Allowance Trakindo</label>
                          <input class="form-control" type="text" id="othrAllowanceTrakindo" name="othrAllowanceTrakindo" placeholder="Other Allowance ">
                      </div>

                      <div class="form-group kpiBenefits">
                          <label class="control-label" for="kpiBenefits">KPI Benefits</label>
                          <input class="form-control" type="text" id="kpiBenefits" name="kpiBenefits" placeholder="KPI">
                      </div>

                      <div class="form-group otherAllowance2">
                          <label class="control-label" for="otherAllowance2">Others Allowance Trakindo</label>
                          <input class="form-control" type="text" id="otherAllowance2" name="otherAllowance2" placeholder="Other Allowance Trakindo">
                      </div>

                      <div class="form-group otherAllowance2">
                          <label class="control-label" for="otherAllowance2">Others</label>
                          <input class="form-control" type="text" id="otherAllowance2" name="otherAllowance2" placeholder="Other Allowance 2">
                      </div>

                      <div class="form-group oAllowanceTra1">
                          <label class="control-label" for="oAllowanceTra1">Other Allowance 1</label>
                          <input class="form-control" type="text" id="oAllowanceTra1" name="oAllowanceTra1" placeholder="Other Allowance 1">
                      </div>

                      <div class="form-group oAllowanceTra2">
                          <label class="control-label" for="oAllowanceTra2">Other Allowance 2</label>
                          <input class="form-control" type="text" id="oAllowanceTra2" name="oAllowanceTra2" placeholder="Other Allowance 2">
                      </div>            

                      <div class="form-group noAccidentFee">
                          <label class="control-label" for="noAccidentFee">No Accident Fee</label>
                          <input type="checkbox" id="cbAccidentFee" name="cbAccidentFee">
                          <input class="form-control" type="text" id="noAccidentFee" name="noAccidentFee" placeholder="No Accident Fee">
                      </div>
                      
                      <div class="form-group thr">
                          <label class="control-label" for="thr">THR</label>
                          <input class="form-control" type="text" id="thr" name="thr" placeholder="THR">
                      </div>

                      <div class="form-group workAdjustment">
                          <label class="control-label" for="workAdjustment">Adjustment</label>
                          <input class="form-control" type="text" id="workAdjustment" name="workAdjustment" placeholder="Penyesuaian">
                      </div>
                      
                      <div class="form-group debtBurden">
                          <label class="control-label" for="debtBurden">Debt Value</label>
                          <input class="form-control" type="text" id="debtBurden" name="debtBurden" placeholder="Beban Hutang">
                      </div>

                      <div class="form-group debtExplanation">
                          <label class="control-label" for="debtExplanation">Debt Info</label>
                          <input class="form-control" type="text" id="debtExplanation" name="debtExplanation" placeholder="Keterangan Beban Hutang">
                      </div>

                      <div class="form-group">
                          <!-- <label class="control-label" for="rowIdx">Index</label> -->
                          <input class="form-control" type="hidden" id="rowIdx" name="rowIdx" value="" readonly="">
                      </div>                                                                                                                                     
                  </form>
                  <!-- End Form EDIT PAYROLL -->

            
          </div>
          <!-- END DATA TABLE -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" id="savePayroll" data-dismiss="modal" class="btn btn-primary">Save</button>
      </div>
    </div>
  </div>
</div>
<!-- END DETAIL MODAL -->


<div class="col-md-12">
  <div class="tile bg-info">
    <!-- <h3 class="tile-title">Barang Masuk</h3> -->
    <div class="tile-body">
       <!-- START DATA TABLE -->
        <div class="tile-body">
          <table class="table table-hover table-bordered" id="slipTable">
            <thead class="thead-dark">
              <tr>
                <th>Slip Id</th>
                <th>Client Name</th>
                <!-- <th>Badge No</th> -->
                <th>Emp Name</th>
                <th>Dept</th>
                <th>Position</th>
                <th>Update</th>
                <th>Print</th>
                <th>Process</th>
              </tr>
            </thead>
            <tbody>
              <!-- <tr>
                <td>Tiger Nixon</td>
                <td>System Architect</td>
                <td>Edinburgh</td>
                <td>61</td>
                <td>2011/04/25</td>
                <td>$320,800</td>
              </tr>  -->       
            </tbody>
          </table>
        </div>
        <!-- END DATA TABLE -->
    </div>
  </div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
  		$("#loader").hide();
  		$("#myDiv").hide();

		$('#uploadRoster').on("click", function(){
			$("#loader").show();
  			$("#myDiv").hide();
			$('#uploadRoster').prop('disabled', true);
			var formData = new FormData($("#rosterUploadForm")[0]);
			var myUrl = "";

			var fileName = $('#file').val();
			var isPTLSMB = fileName.includes("PTLSMB"); 
			// var isPTLBWG = fileName.includes("PTLBWG"); 
			var isLCPSMB = fileName.includes("LCPSMB"); 


			if (isPTLSMB == true){
			   myUrl = "<?php echo base_url() ?>"+"transactions/sumbawa/Timesheet_ptlsmb/upload/";
			}
			// else if (isPTLBWG == true){
			//    myUrl = "<?php #echo base_url() ?>"+"transactions/sumbawa/Timesheet_ptlbwg/upload/";
			// } 
			else if (isLCPSMB == true){
			   myUrl = "<?php echo base_url() ?>"+"transactions/sumbawa/Timesheet_lcpsmb/upload/";
			}
			else{
			   return false;
			}

			// alert(myUrl); return false;
			
			$.ajax({
			    url: myUrl,
			    type: "POST",
			    data : formData,
			    processData: false,
			    contentType: false,
			    beforeSend: function() {
			    },
			    success: function(data){
					$("#myDiv").show();
					$("#loader").hide();
					$('#uploadRoster').prop('disabled', false);
                    // alert(data);
                    $.notify({
		                title: "<h5>Informasi : </h5>",
		                message: "<strong>"+data+"</strong> </br></br> ",
		                icon: '' 
		            },
		            {
		                type: "success",
		                delay: 3000
		            });	
			    },
			    error: function(data){
					// alert(data);
					$.notify({
		                title: "<h5>Informasi : </h5>",
		                message: "<strong>"+data+"</strong> </br></br> ",
		                icon: '' 
		            },
		            {
		                type: "warning",
		                delay: 3000
		            });
			    }
			    /*error: function(xhr, ajaxOptions, thrownError) {
			       console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			    }*/
			});

		});


		$('#uploadAllowance').on("click", function(){
			$("#loader").show();
  			$("#myDiv").hide();
			$('#uploadAllowance').prop('disabled', true);
			var formData = new FormData($("#allowanceUploadForm")[0]);
			var myUrl = "<?php echo base_url() ?>"+"transactions/sumbawa/Allowance/upload/";
			// alert(myUrl);
			$.ajax({
			    url: myUrl,
			    type: "POST",
			    data : formData,
			    processData: false,
			    contentType: false,
			    beforeSend: function() {
			    },
			    success: function(data){
					$("#myDiv").show();
					$("#loader").hide();
					$('#uploadAllowance').prop('disabled', false);
                    // alert(data);
                    $.notify({
		                title: "<h5>Informasi : </h5>",
		                message: "<strong>"+data+"</strong> </br></br> ",
		                icon: '' 
		            },
		            {
		                type: "success",
		                delay: 3000
		            });	
			    },
			    error: function(data){
					// alert(data);
					$.notify({
		                title: "<h5>Informasi : </h5>",
		                message: "<strong>"+data+"</strong> </br></br> ",
		                icon: '' 
		            },
		            {
		                type: "success",
		                delay: 3000
		            });
			    }
			    /*error: function(xhr, ajaxOptions, thrownError) {
			       console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			    }*/
			});

		});


	});
</script>
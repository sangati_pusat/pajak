<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    // Load library phpspreadsheet
    require('./vendor/autoload.php');
    use PhpOffice\PhpSpreadsheet\Helper\Sample;
    use PhpOffice\PhpSpreadsheet\IOFactory;
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    // End load library phpspreadsheet

class Payroll_ptlbwg extends CI_Controller {

    public function __construct(){
        parent::__construct();
        // $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
        // $this->load->model('M_MstProcessClosing');
        // $this->load->model('masters/M_salary_slip');
        $this->load->model('masters/M_roster_hist');
        $this->load->model('M_slip_ptlbwg','M_salary_slip'); 
        $this->load->model('masters/M_payroll_config'); 
        // $this->load->model('masters/M_salary');
        $this->load->model('masters/M_mst_salary', 'M_salary'); 
    }

    public function toExcel($slipId, $clientName, $calendarStart, $payrollGroup, $isHealthBPJS, $isJHT, $isJP, $isJKKM)
    {             
        
        if ($clientName == 'Pontil_Banyuwangi') {            
            $this->pontilBwgExcel($slipId, $calendarStart, $payrollGroup, $isHealthBPJS, $isJHT, $isJP, $isJKKM);
        }   
                
    }


    public function pontilBwgExcel($slipId, $calendarStart, $payrollGroup, $isHealthBPJS, $isJHT, $isJP, $isJKKM)
    {
        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();

        $spreadsheet->getProperties()->setCreator('Maurice - Web - Android')
            ->setLastModifiedBy('Maurice - Web - Android')
            ->setTitle('Office 2007 XLSX Test Document')
            ->setSubject('Office 2007 XLSX Test Document')
            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Test result file');

        if (file_exists('assets/images/report_logo.png')) {
            $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing->setName('Logo');
            $drawing->setDescription('Logo');
            $drawing->setPath('./assets/images/report_logo.png');
            $drawing->setCoordinates('M1');
            $drawing->setHeight(38);
            $drawing->setWorksheet($spreadsheet->getActiveSheet());
        }

        $boldFont = [
            'font' => [
                'bold' => true
                // 'color' => ['argb' => '0000FF'],
            ],
        ];

        $totalStyle = [
            'font' => [
                'bold' => true,
                'color' => ['argb' => '0000FF'],
            ],
        ];

        $allBorderStyle = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $outlineBorderStyle = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $topBorderStyle = [
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $bottomBorderStyle = [
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        // $center = [
        //     'alignment' => [
        //         'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
        //         'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
        //     ],
        // ];

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        // http://192.168.10.17/os-hris-2019/sumbawa/Payroll/toexcel/2019031175/Machmahon_Sumbawa/1/undefined/1/1/1/1
        // $slipId = "2019040001";
        // $calendarStart = "1";
        // $payrollGroup = "";
        // $isHealthBPJS = "1";
        // $isJHT = "1";
        // $isJP = "1";
        // $isJKKM = "1";
        
        /* CALENDAR SETUP */
        $dateCal = $calendarStart;

        /* START VALIDATE & UPDATE GOVERNMENT REGULATION */     
        $this->M_payroll_config->loadByClient('Pontil_Banyuwangi');
        $myConfigId             = $this->M_payroll_config->getPayrollConfigId();
        // $this->M_payroll_config->loadById($myConfigId); 
        $healthBpjsConfig       = $this->M_payroll_config->getHealthBpjs();
        $maxHealthBpjsConfig    = $this->M_payroll_config->getMaxHealthBpjs();
        $jkkJkmConfig           = $this->M_payroll_config->getJkkJkm();
        $jhtConfig              = $this->M_payroll_config->getJht();
        $empJhtConfig           = $this->M_payroll_config->getEmpJht();             
        $empHealthBpjsConfig    = $this->M_payroll_config->getEmpHealthBpjs();    
        $jpConfig               = $this->M_payroll_config->getJp();
        $empJpConfig            = $this->M_payroll_config->getEmpJp();
        // echo $empHealthBpjsConfig; exit(0);
        $npwpCharge             = $this->M_payroll_config->getNpwpCharge();
        $this->M_salary_slip->getObjectById($slipId);
        $basicSalary            = $this->M_salary_slip->getBasicSalary();
        // echo $healthBpjsConfig; exit(0);
        /* START HEALTH BPJS */
        $healthBpjs = ($healthBpjsConfig/100) * $basicSalary; /* Nilai mst_payroll_config.health_bpjs(%) x Basic Salary (Max 8 Juta) */
        if($healthBpjs > $maxHealthBpjsConfig){
            $healthBpjs = $maxHealthBpjsConfig; 
        }
        /* END HEALTH BPJS */
        /* START JKK-JKM */
        $jkkJkm = ($jkkJkmConfig/100) * $basicSalary; /* Nilai mst_payroll_config.jkk_jkm(%) x Basic Salary */ 
        /* END JKK-JKM */                
        /* START JHT */
        $jht = ($jhtConfig/100) * $basicSalary;  /*Nilai mst_payroll_config.jht(%) x Basic Salary  */       
        /* END JHT */                
        /* START JHT */
        $empJht = ($empJhtConfig/100) * $basicSalary; /* Nilai mst_payroll_config.emp_jht(%) x Basic Salary */      
        /* END JHT */
        /* START JP COMPANY */
        // $jp = ($jpConfig/100) * $basicSalary; /* Nilai mst_payroll_config.jp(%) x Basic Salary */        
        /* END JP COMPANY */
        /* START JP EMPLOYEE */
        $empJp = ($empJpConfig/100) * $basicSalary; /* Nilai mst_payroll_config.emp_jp(%) x Basic Salary */        
        /* END JP EMPLOYEE */               
        
        /* START BPJS KARYAWAN */
        $maxEmpHealthBpjs = $this->M_payroll_config->getMaxEmpBpjs(); /* Nilai Max Gaji Untuk Iuran BPJS Karyawan  */ 
        $empHealthBpjs = ($empHealthBpjsConfig/100) * $basicSalary; /* Nilai mst_payroll_config.emp_health_bpjs(%) x Basic Salary */
        if($empHealthBpjs > $maxEmpHealthBpjs)
        {
            $empHealthBpjs = $maxEmpHealthBpjs; 
        }     
        /* END BPJS KARYAWAN */        

        if($isHealthBPJS == '0')
        {
            $healthBpjs = 0;
            $empHealthBpjs = 0;
        }
        if($isJHT == '0')
        {
            $jht = 0;
            $empJht = 0;
        }
        if($isJP == '0')
        {
            $empJp = 0;
        }
        if($isJKKM == '0')
        {
            $jkkJkm = 0;
        }

        $this->M_salary_slip->setHealthBpjs($healthBpjs);
        $this->M_salary_slip->setEmpHealthBpjs($empHealthBpjs);
        $this->M_salary_slip->setJht($jht);
        $this->M_salary_slip->setEmpJht($empJht);
        $this->M_salary_slip->setEmpJp($empJp);
        $this->M_salary_slip->setJkkJkm($jkkJkm);
        $this->M_salary_slip->update($slipId);
        /* END VALIDATE & UPDATE GOVERNMENT REGULATION */

        $this->db->select('ss.*, mb.bio_rec_id biodata_id, ss.basic_salary salary_basic,ss.bs_prorate, mb.npwp_no, tro.*, mr.*');
        $this->db->from('trn_slip_ptlbwg ss, mst_bio_rec mb');
        $this->db->join('mst_roster mr','mb.bio_rec_id = mr.bio_rec_id');
        $this->db->join('trn_overtime tro','mb.bio_rec_id = tro.bio_rec_id');
        $this->db->where('ss.bio_rec_id = mb.bio_rec_id AND ss.client_name = "Pontil_Banyuwangi"');
        $this->db->where('salary_slip_id', $slipId);
        $this->db->where('ss.year_period = tro.year_period AND ss.month_period = tro.month_period');        
        $this->db->where('ss.year_period = mr.year_process AND ss.month_period = mr.month_process');        
        $rowData = $query = $this->db->get()->row_array();  // Produces: SELECT * FROM trn_slip_ptlbwg (one row only) 
        $ptName         = $rowData['client_name'];  
        $monthPeriod    = $rowData['month_period'];
        $yearPeriod     = $rowData['year_period'];     
        $biodataId      = $rowData['biodata_id'];     
        $nie            = $rowData['nie'];     
        $dept           = $rowData['dept'];     
        $rosterFormat   = $rowData['roster_format'];     
        $rosterBase     = $rowData['roster_base']; 
        $payrollLevel   = $rowData['payroll_level']; 
        $npwpNo         = $rowData['npwp_no']; 
        $tMonth         = (int) $monthPeriod;
        $tYear          = (int) $yearPeriod;

        // echo $this->db->last_query(); exit();

        $wdAttendCount = 0;
        $wdAttendCount = $rowData['in_shift'] + $rowData['in_off'] + $rowData['in_ph'];
        // $wdAttendCount += $rowData['attend_total']; // 5
        //                 $wdAttendCount += $rowData['in_ph_total']; //2
        //                 $wdAttendCount -= $rowData['attend_in_off'];//2
        //                 $wdAttendCount += $rowData['sick_total']; //0
        //                 $wdAttendCount += $rowData['emergency_total']; //1
        //                 $wdAttendCount += $rowData['paid_permit_total']; //0
        //                 $wdAttendCount += $rowData['paid_vacation_total']; //0
        //                 $wdAttendCount += $rowData['alpa_total']; 
        //                 $wdAttendCount += $rowData['off_total'];

        $dayCountInMonth = cal_days_in_month(CAL_GREGORIAN, $tMonth, $tYear); 

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(6);             
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(9);             
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(7);             
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(8);         
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(5);             
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(5);             
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(5);             
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(5);             
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(5);             
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(3);             
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(8);

        $tDate1 = $calendarStart.'-'.$monthPeriod.'-'.$yearPeriod;
        // $tStr = $yearPeriod.'-'.$monthPeriod.'-'.($calendarStart);
        // $tDate2 = strtotime($tStr);
        // $tDate3 = date("d-m-Y", strtotime("-1 day", $tDate2));
        $tDate3 = date("t-m-Y", strtotime($tDate1));
        // Add some data
        $spreadsheet->getActiveSheet()
            ->setCellValue('A1', 'PT. SANGATI SOERYA SEJAHTERA')
            ->setCellValue('A2', 'Slip Gaji Karyawan')
            ->setCellValue('A4', 'PT : '.$ptName)
            ->setCellValue('A5', 'Periode : '.$tDate1.' to '.$tDate3);

        $spreadsheet->getActiveSheet()->getStyle("A1:D1")->getFont()->setBold(true)->setSize(16);
        $spreadsheet->getActiveSheet()->getStyle("A2:D2")->getFont()->setBold(true)->setSize(14)->setUnderline(true);
        $spreadsheet->getActiveSheet()->getStyle("A4:G4")->getFont()->setBold(true)->setSize(12);

        $spreadsheet->getActiveSheet()->mergeCells("A6:A7");
        $spreadsheet->getActiveSheet()->mergeCells("B6:B7");
        $spreadsheet->getActiveSheet()->mergeCells("C6:C7");
        $spreadsheet->getActiveSheet()->mergeCells("D6:D7");
        $spreadsheet->getActiveSheet()->mergeCells("E6:E7");
        $spreadsheet->getActiveSheet()->mergeCells("F6:I6");
        
        $spreadsheet->getActiveSheet()->getStyle("A6:E7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("F6:I7")->applyFromArray($allBorderStyle);

        // $spreadsheet->getActiveSheet()
        //     ->getStyle('A10:H10')
        //     ->getBorders()
        //     ->getBottom()
        //     ->setBorderStyle(Border::BORDER_THIN)
        //     ->setColor(new Color('FFFF0000'));
        
        $spreadsheet->getActiveSheet()->getStyle("A6:I7")->applyFromArray($center);
        // $spreadsheet->getActiveSheet()->getStyle('A6:I7')->getAlignment()->setHorizontal('center');
        // $spreadsheet->getActiveSheet()->getStyle('A6:I7')->getAlignment()->setVertical('center');

        $spreadsheet->getActiveSheet()->getStyle("A6:I7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()
            ->setCellValue('A6', 'DATE')
            ->setCellValue('B6', 'ROSTER DAY')
            ->setCellValue('C6', 'SHIFT DAY')
            ->setCellValue('D6', 'HOURS TOTAL')
            ->setCellValue('E6', 'NT')
            ->setCellValue('F6', 'OVERTIME')
            ->setCellValue('F7', '1')
            ->setCellValue('G7', '2')
            ->setCellValue('H7', '3')
            ->setCellValue('I7', '4');

        $spreadsheet->getActiveSheet()->getStyle("A6:I7")->applyFromArray($boldFont);
        // $spreadsheet->getActiveSheet()->getStyle("A6:I7")->getFont()->setBold();
        // $spreadsheet->getDefaultStyle()->getFont()->setBold();  
        // $spreadsheet->getActiveSheet()->getStyle("A6:I7")->getFont()->setBold();  
        $rosterFormat = $rowData['roster_format'];
        $tmpRosterLength = strlen($rosterFormat);
        $strNumRoster = '';
        $tmp = '';
        $dayNo = '';
        $groupCount = 0;
        $strNum = '';
        $dayTotal = 0;
        /* START GET DAYS TOTAL BY ROSTER */
        for ($i=0; $i < $tmpRosterLength; $i++) {
            $strNum = substr($rosterFormat,$i,1);
            /* START MAKE SURE DATA IS NUMBER */ 
            if(is_numeric($strNum))
            {
                $strNumRoster .= $strNum;
                $dayTotal += $strNum;
            }
            /* END MAKE SURE DATA IS NUMBER */ 
        }
        $numChar = '';
        $rdData = '';
        $dataIdx = 8;
        $tIdx = 0;  

        $ot01Count = 0;
        $ot02Count = 0;
        $ot03Count = 0;
        $ot04Count = 0;

        $normalTotal = 0;
        $allTimeTotal = 0;
        /* START SUMMARY ROSTER */
        $rowIdx = 8;
        /* START CALENDAR VALUE */
        // $dayCount = cal_days_in_month(CAL_GREGORIAN, $monthPeriod, $yearPeriod);   

        /* Get Last Day Number */
        $strDate = $yearPeriod.'-'.$monthPeriod.'-01';
        $tQuery  = 'SELECT * FROM mst_roster_hist ';
        $tQuery .= 'WHERE bio_rec_id = "'.$biodataId.'" ';
        $tQuery .= 'AND DATE_ADD("'.$strDate.'", INTERVAL -1 MONTH) = DATE_FORMAT(CONCAT(year_period,"-",month_period,"-01"), "%Y-%m-%d") ';
        $tQuery .= 'LIMIT 1 ';
        $tData = $this->db->query($tQuery)->row_array();
        $tLastDay = 0;
        if(isset($tData)){
            $tLastDay = $tData['last_day']; 
        }

        $rosterIdx = $tLastDay;
        for ($i=1; $i <= $dayCountInMonth; $i++) { 
            /* START NUMBER TITLE */
            $rowIdx++;

            $ot01Column = '';
            $ot02Column = '';
            $ot03Column = '';
            $ot04Column = '';

            if($i < 10){
                $ot01Column = 'ot1_d0'.$i;
                $ot02Column = 'ot2_d0'.$i;
                $ot03Column = 'ot3_d0'.$i;
                $ot04Column = 'ot4_d0'.$i;
                $timePerday = 'd0'.$i;
            }else{
                $ot01Column = 'ot1_d'.$i;
                $ot02Column = 'ot2_d'.$i;
                $ot03Column = 'ot3_d'.$i;
                $ot04Column = 'ot4_d'.$i;
                $timePerday = 'd'.$i;
            }
            /* OVER TIME */
            $ot01Val = $rowData[$ot01Column];
            $ot02Val = $rowData[$ot02Column];
            $ot03Val = $rowData[$ot03Column];
            $ot04Val = $rowData[$ot04Column];
            
            $timeTotal = 0;
            $strTimePerday = $rowData[$timePerday];
            $tTimePerday = 0;            

            /* START PUBLIC HOLIDAY  */
            $phCodeTmp = substr($strTimePerday,0,2);
            $phHoursTmp = substr($strTimePerday,2,strlen($strTimePerday)-2);
            if( (strtoupper($phCodeTmp) == "RO" || strtoupper($phCodeTmp) == "PH" || strtoupper($phCodeTmp) == "SP" || strtoupper($phCodeTmp) == "NS" || strtoupper($phCodeTmp) == "SD") && (is_numeric($phHoursTmp)) && ($phHoursTmp > 0) )
            {
                $tTimePerday = $phHoursTmp;
            }
            else
            {
                $tTimePerday = $rowData[$timePerday];
            }

            if(is_numeric($tTimePerday))
            {
                $timeTotal = $tTimePerday;
                if($tTimePerday > 1) {
                    $attendStatus = 1;
                }
            }

            $normalTime = $timeTotal - $ot01Val - $ot02Val - $ot03Val - $ot04Val; 
            $normalTotal += $normalTime;

            $allTimeTotal += $timeTotal; 

            $ot01Count += $ot01Val;
            $ot02Count += $ot02Val;
            $ot03Count += $ot03Val;
            $ot04Count += $ot04Val;
                         
            if($dateCal > $dayCountInMonth){
               $dateCal = 1; 
            }

            $spreadsheet->getActiveSheet()->getStyle('A'.$rowIdx.':A'.$rowIdx)->applyFromArray($allBorderStyle);
            $spreadsheet->getActiveSheet()->getStyle('A'.$rowIdx.':A'.$rowIdx)->applyFromArray($right);
            $spreadsheet->getActiveSheet()->setCellValue('A'.$rowIdx, $dateCal);  

            $dateCal++;
            /* END NUMBER TITLE */

                    
            $attendStatus = 0;        
            $rosterIdx++;
            if($rosterIdx > 7){
               $rosterIdx = 1;                
            }
            $rdData = $rosterIdx;
            if( strtoupper($strTimePerday) == 'STR' || strtoupper($strTimePerday) == 'END' )
            {
                $rdData = $strTimePerday; 
                $rosterIdx = 0;
            }
            if( strtoupper($strTimePerday) == 'ID' || strtoupper($strTimePerday) == 'A' || strtoupper($strTimePerday) == 'S' || strtoupper($strTimePerday) == 'I') {
                $rdData = $strTimePerday; 
            }
            
            if( strtoupper($phCodeTmp) == 'RO' ) {
                $rdData = $phCodeTmp; 
                $rosterIdx = 0;
            }

            if( strtoupper($phCodeTmp) == 'SP' ) {
                $rdData = $phCodeTmp; 
                $rosterIdx = 0;
            }

            if( strtoupper($phCodeTmp) == 'SB' ) {
                $rdData = $phCodeTmp; 
            }

            if( strtoupper($phCodeTmp) == 'PH' || strtoupper($phCodeTmp) == 'NS') {
                $rdData = $phCodeTmp; 
            }
            
            $hourVal = right($strTimePerday,1); 
            if( is_numeric($hourVal) ) {
                $attendStatus = 1;        
            }

            $arrTmpCode = array('PH','SB','PV','PP','RO','NS','SD','ID');
            $arrOtTmp = array('A','U','I','S','V', 'E', 'STR', 'END');
            if( (!in_array( strtoupper($phCodeTmp), $arrTmpCode)) && (!in_array( strtoupper($strTimePerday), $arrOtTmp)) && !is_numeric($strTimePerday))
            {
                $attendStatus = 0;      
            }
            
            $spreadsheet->getActiveSheet()
                ->setCellValue('B'.$rowIdx, $rdData)
                ->setCellValue('C'.$rowIdx, $attendStatus)
                ->setCellValue('D'.$rowIdx, $timeTotal)
                ->setCellValue('E'.$rowIdx, $normalTime)
                ->setCellValue('F'.$rowIdx, $ot01Val)
                ->setCellValue('G'.$rowIdx, $ot02Val)
                ->setCellValue('H'.$rowIdx, $ot03Val)
                ->setCellValue('I'.$rowIdx, $ot04Val);

            $spreadsheet->getActiveSheet()->getStyle("B".$rowIdx.":I".$rowIdx)->applyFromArray($allBorderStyle);
            $spreadsheet->getActiveSheet()->getStyle("B".$rowIdx.":I".$rowIdx)->applyFromArray($center);  
            // $spreadsheet->getActiveSheet()->getStyle("B".$rowIdx.":I".$rowIdx)->getAlignment()->setHorizontal('center');
            // $spreadsheet->getActiveSheet()->getStyle("B".$rowIdx.":I".$rowIdx)->getAlignment()->setVertical('center');

        } // for ($i=1; $i <= 31 ; $i++)


        $this->M_roster_hist->resetValues();
        $this->M_roster_hist->delete($slipId);    
        $this->M_roster_hist->setRhId($slipId);    
        $this->M_roster_hist->setBioRecId($biodataId);    
        $this->M_roster_hist->setClientName($ptName);    
        $this->M_roster_hist->setYearPeriod($yearPeriod);    
        $this->M_roster_hist->setMonthPeriod($monthPeriod);    
        $this->M_roster_hist->setLastDay($rosterIdx); 
        $this->M_roster_hist->insert(); 

        $totalTitle = 40;
        $spreadsheet->getActiveSheet()
                ->setCellValue('A'.$totalTitle, 'TOTAL')
                ->setCellValue('C'.$totalTitle, $wdAttendCount)
                ->setCellValue('D'.$totalTitle, $allTimeTotal)
                ->setCellValue('E'.$totalTitle, $normalTotal)
                ->setCellValue('F'.$totalTitle, $ot01Count)
                ->setCellValue('G'.$totalTitle, $ot02Count)
                ->setCellValue('H'.$totalTitle, $ot03Count)
                ->setCellValue('I'.$totalTitle, $ot04Count);

        $spreadsheet->getActiveSheet()->getStyle("A".$totalTitle.":A".$totalTitle+1)->applyFromArray($center);
        // $spreadsheet->getActiveSheet()->getStyle("A".$totalTitle.":A".$totalTitle+1)->getAlignment()->setHorizontal('center');
        // $spreadsheet->getActiveSheet()->getStyle("A".$totalTitle.":A".$totalTitle+1)->getAlignment()->setVertical('center');

        $spreadsheet->getActiveSheet()->getStyle("C".$totalTitle.":I".$totalTitle)->applyFromArray($right);
        $spreadsheet->getActiveSheet()->getStyle("A".$totalTitle.":I".$totalTitle)->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("A".$totalTitle.":I".$totalTitle)->getFont()->setBold(true)->setSize(13);
        
        /* END TITLE */       

        /* START TOTAL OVER TIME  */
        $totalTitle++; 
        /* Title */
        $spreadsheet->getActiveSheet()->getStyle("A".$totalTitle.":A".$totalTitle+1)->applyFromArray($center);
        // $spreadsheet->getActiveSheet()->getStyle("A".$totalTitle.":A".$totalTitle+1)->getAlignment()->setHorizontal('center');
        // $spreadsheet->getActiveSheet()->getStyle("A".$totalTitle.":A".$totalTitle+1)->getAlignment()->setVertical('center');

        $spreadsheet->getActiveSheet()->getStyle("A".$totalTitle.":A".$totalTitle)->getFont()->setBold(true)->setSize(13);
        $spreadsheet->getActiveSheet()->setCellValue('A'.$totalTitle, 'TOTAL OT');
        /* Data */
       
        $spreadsheet->getActiveSheet()->getStyle("C41:I41")->applyFromArray($right);
        $spreadsheet->getActiveSheet()->getStyle("A41:I41")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->mergeCells("C41:I41");
        $spreadsheet->getActiveSheet()->getStyle("C41:I41")->getFont()->setBold(true)->setSize(13);
        $spreadsheet->getActiveSheet()->setCellValue('C41', '=SUM(F40:I40)');

        $spreadsheet->getActiveSheet()->getStyle("A43:I64")->applyFromArray($outlineBorderStyle);
        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(1, 47, "Dibayarkan Oleh");
        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(1, 51, "Payroll/ Accounting");
        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(1, 55, "Diterima Oleh Karyawan");
        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(1, 59, $rowData['name']); 

        $this->M_salary->loadByBiodataIdNie($biodataId,$nie);
        $bankName = $this->M_salary->getBankName();
        $bankNo = $this->M_salary->getAccountNo();
        $spreadsheet->getActiveSheet()
                ->setCellValue('B47', 'Dibayarkan Oleh')    
                ->setCellValue('B51', 'Payroll/ Accounting')    
                ->setCellValue('B55', 'Diterima Oleh Karyawan')    
                ->setCellValue('B59', $rowData['name'])
                ->setCellValue('B62', $bankName." : ".$bankNo);     

        $basicSalary = $rowData['salary_basic'];
        $bsProrate = $rowData['bs_prorate'];
        if($bsProrate > $basicSalary)
        {
            $bsProrate = $basicSalary;   
        }

        $this->M_payroll_config->loadByClient($ptName);
        $salaryDividerConfig = $this->M_payroll_config->getSalaryDivider();
        $salaryHourly = $basicSalary / $salaryDividerConfig;
        // $salaryHourly = number_format($salaryHourly,2);
        $otMultiplierConfig01 = $this->M_payroll_config->getOt01Multiplier();
        // $otMultiplierConfig01 = number_format($otMultiplierConfig01,1);
        $otMultiplierConfig02 = $this->M_payroll_config->getOt02Multiplier();
        // $otMultiplierConfig02 = number_format($otMultiplierConfig02,1);
        $otMultiplierConfig03 = $this->M_payroll_config->getOt03Multiplier();
        // $otMultiplierConfig03 = number_format($otMultiplierConfig03,1);
        $otMultiplierConfig04 = $this->M_payroll_config->getOt04Multiplier(); 
        // $otMultiplierConfig04 = number_format($otMultiplierConfig04,1);

        $spreadsheet->getActiveSheet()->getStyle("K4:L4")->applyFromArray($left);
        $spreadsheet->getActiveSheet()->getStyle("K4:Q43")->getFont()->setSize(13);

        $spreadsheet->getActiveSheet()
            ->setCellValue('K4', 'NAMA')
            ->setCellValue('M4', $rowData['name'])
            ->setCellValue('N4', 'Id')
            ->setCellValue('O4', $nie)
            ->setCellValue('K5', 'JABATAN')
            ->setCellValue('M5', $rowData['position'])
            ->setCellValue('N5', 'DEPT ')
            ->setCellValue('O5', $dept)
            ->setCellValue('K6', 'STATUS')
            ->setCellValue('M6', $rowData['marital_status'])
            ->setCellValue('K7', 'GAJI POKOK')
            ->setCellValue('M7', $basicSalary)
            ->setCellValue('K8', 'RATE/HOUR')
            ->setCellValue('M8', $salaryHourly)
            ->setCellValue('K9', 'NPWP')
            ->setCellValue('M9', $rowData['npwp_no'])
            ->setCellValue('K11', 'UPAH')
            ->setCellValue('O11', $bsProrate)
            ->setCellValue('K12', 'OT 1')
            ->setCellValue('K13', 'OT 2')
            ->setCellValue('K14', 'OT 3')
            ->setCellValue('K15', 'OT 4')
            ;  

        $spreadsheet->getActiveSheet()->getStyle('M7:M8')->applyFromArray($totalStyle);
        $otTotal1 = $rowData['ot_1']; 
        $otTotal2 = $rowData['ot_2']; 
        $otTotal3 = $rowData['ot_3']; 
        $otTotal4 = $rowData['ot_4']; 

        $allOt = $otTotal1+$otTotal2+$otTotal3+$otTotal4; 

        $strOT1 = number_format($rowData['ot_count1'],1)." x ".number_format($otMultiplierConfig01,1)." x ".number_format($salaryHourly,2)." = Rp.";
        $strOT2 = number_format($rowData['ot_count2'],1)." x ".number_format($otMultiplierConfig02,1)." x ".number_format($salaryHourly,2)." = Rp.";
        $strOT3 = number_format($rowData['ot_count3'],1)." x ".number_format($otMultiplierConfig03,1)." x ".number_format($salaryHourly,2)." = Rp.";
        $strOT4 = number_format($rowData['ot_count4'],1)." x ".number_format($otMultiplierConfig04,1)." x ".number_format($salaryHourly,2)." = Rp.";
        
        $spreadsheet->getActiveSheet()
            ->setCellValue('M12', $strOT1)
            ->setCellValue('N12', $otTotal1)
            ->setCellValue('M13', $strOT2)
            ->setCellValue('N13', $otTotal2)
            ->setCellValue('M14', $strOT3)
            ->setCellValue('N14', $otTotal3)
            ->setCellValue('M15', $strOT4)
            ->setCellValue('N15', $otTotal4)
            ->setCellValue('K16', 'OT TOTAL')
            ->setCellValue('O16', $allOt);

        $spreadsheet->getActiveSheet()->getStyle('K16:O16')->applyFromArray($topBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle('K16:O16')->applyFromArray($totalStyle);

        $this->M_salary->loadByBiodataIdNie($biodataId, $nie);
        $isOtBonus = $this->M_salary->getIsOtBonus(); 
        $isShiftBonus = $this->M_salary->getIsShiftBonus();
        $isRemoteAllowance = $this->M_salary->getIsRemoteAllowance();
        // $isDevIncentiveBonus = $this->M_salary->getIsDevIncentiveBonus();

        /* START BONUS TOTAL */
        $bonusTotal = 0; 

        $outCampBonus = $rowData['flying_camp'];
        $trvBonus = $rowData['travel_bonus'];
        $attendanceBonus = $rowData['attendance_bonus'];
        $otherAllowance1 = $rowData['other_allowance1'];
        $otherAllowance2 = $rowData['other_allowance2'];
        $bonusTotal = $outCampBonus + $trvBonus + $attendanceBonus + $otherAllowance1 + $otherAllowance2;   
        /* END BONUS, INCENTIVE */

        $bonusTotal += $rowData['thr'];
        $bonusTotal += $rowData['contract_bonus'];
        $bonusTotal += $rowData['drilling_bonus'];
        $bonusTotal += $rowData['act_manager_bonus'];
        // $spreadsheet->getActiveSheet()->getStyle('K'.($bonusIdx+7).':O'.($bonusIdx+7))->applyFromArray($topBorderStyle);
        // $spreadsheet->getActiveSheet()->getStyle('K'.($bonusIdx+7).':O'.($bonusIdx+7))->applyFromArray($totalStyle);

        $bonusIdx = 18; 
        $spreadsheet->getActiveSheet()
            ->setCellValue('K'.$bonusIdx, 'TVR Extra Time')
            ->setCellValue('N'.$bonusIdx, $trvBonus)
            ->setCellValue('K'.($bonusIdx+1), 'Out Camp Bonus / BBM')
            ->setCellValue('N'.($bonusIdx+1), $outCampBonus)
            // ->setCellValue('K'.($bonusIdx+2), 'Attendance Bonus')
            // ->setCellValue('N'.($bonusIdx+2), $attendanceBonus)
            // ->setCellValue('K'.($bonusIdx+3), 'KPI Benefits')
            // ->setCellValue('N'.($bonusIdx+3), $otherAllowance1)
            // ->setCellValue('K'.($bonusIdx+4), 'Other benefits')
            // ->setCellValue('N'.($bonusIdx+4),  $otherAllowance2)
            ->setCellValue('K'.($bonusIdx+2), 'THR')
            ->setCellValue('N'.($bonusIdx+2), $rowData['thr'])
            ->setCellValue('K'.($bonusIdx+3), 'Contract Bonus')
            ->setCellValue('N'.($bonusIdx+3), $rowData['contract_bonus'])
            
            ->setCellValue('K'.($bonusIdx+4), 'Drilling Bonus')
            ->setCellValue('N'.($bonusIdx+4), $rowData['drilling_bonus'])
            ->setCellValue('K'.($bonusIdx+5), 'Act SPV Bonus')
            ->setCellValue('N'.($bonusIdx+5), $rowData['act_manager_bonus'])            

            ->setCellValue('K'.($bonusIdx+6), 'BONUS TOTAL')
            ->setCellValue('O'.($bonusIdx+6), $bonusTotal);

        $spreadsheet->getActiveSheet()->getStyle('K'.($bonusIdx+6).':O'.($bonusIdx+6))->applyFromArray($topBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle('K'.($bonusIdx+6).':O'.($bonusIdx+6))->applyFromArray($totalStyle);

        /* START GOVERNMENT REGULATION */
        $unpaidTotal = $rowData['unpaid_total'];
        $tUnpaidCount = $rowData['unpaid_count'];
        $subTotalVal = $rowData['jkk_jkm'] + $rowData['health_bpjs'] /*+ $rowData['jp']*/ - $unpaidTotal + $rowData['adjust_in'] - $rowData['adjust_out'];
        $govRegIdx = 27;
        $spreadsheet->getActiveSheet()
            ->setCellValue('K'.$govRegIdx, 'JKK & JKM(2.04%)')
            ->setCellValue('N'.$govRegIdx, $rowData['jkk_jkm'])
            ->setCellValue('K'.($govRegIdx+1), 'Iuran BPJS Kesehatan(4%)')
            ->setCellValue('N'.($govRegIdx+1), $rowData['health_bpjs'])
            ->setCellValue('K'.($govRegIdx+2), 'Potongan Absensi '.$tUnpaidCount.' Hari')
            ->setCellValue('N'.($govRegIdx+2), $unpaidTotal)
            ->setCellValue('K'.($govRegIdx+3), 'Adjustment In')
            ->setCellValue('N'.($govRegIdx+3), $rowData['adjust_in'])
            ->setCellValue('K'.($govRegIdx+4), 'Adjustment Out')
            ->setCellValue('N'.($govRegIdx+4), $rowData['adjust_out'])
            ->setCellValue('K'.($govRegIdx+5), 'Subtotal')
            ->setCellValue('O'.($govRegIdx+5), $subTotalVal);

        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(10, $govRegIdx+5, "Subtotal");
        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(14, $govRegIdx+5, $subTotalVal);
        $spreadsheet->getActiveSheet()->getStyle('K'.($govRegIdx+5).':O'.($govRegIdx+5))->applyFromArray($totalStyle);

        $brutto = $bsProrate + $allOt + $bonusTotal + $rowData['jkk_jkm'] + $rowData['health_bpjs'] /*+ $rowData['jp']*/ - $unpaidTotal + $rowData['adjust_in'] - $rowData['adjust_out'];
        $bruttoCoordinate = $govRegIdx+5;
        $spreadsheet->getActiveSheet()
            ->setCellValue('K'.$bruttoCoordinate, 'TOTAL KOTOR')
            ->setCellValue('O'.$bruttoCoordinate, $brutto);

        $spreadsheet->getActiveSheet()->getStyle('K'.$bruttoCoordinate.':O'.$bruttoCoordinate)->applyFromArray($topBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle('K'.$bruttoCoordinate.':O'.$bruttoCoordinate)->applyFromArray($totalStyle);

        $taxCoordinate = 49; /* TAX ROWS COORDINATE  */
        $tmpCoordinate1 = $taxCoordinate+17;
        $tmpCoordinate2 = 0;
        $spreadsheet->getActiveSheet()->getStyle("K".$taxCoordinate.":O".($tmpCoordinate1-2))->applyFromArray($outlineBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle('K'.$taxCoordinate.':O'.($taxCoordinate))->applyFromArray($totalStyle);
        $tmpCoordinate1 = $taxCoordinate + 19;
        $spreadsheet->getActiveSheet()->getStyle("K".$taxCoordinate.":O".$tmpCoordinate1)->getFont()->setSize(13);

        $myConfigId = 1;  
        $row = $this->M_payroll_config->getObjectById($myConfigId);       
        $maxNonTax = $this->M_payroll_config->getNonTaxAllowance(); /* Max Tunjangan Jabatan */

        $nonTaxAllowance = $brutto * (5/100);
        if($nonTaxAllowance > $maxNonTax){
           $nonTaxAllowance = $maxNonTax; 
        }
        
        $spreadsheet->getActiveSheet()
            ->setCellValue('K'.$taxCoordinate, 'PERHITUNGAN PAJAK PENGHASILAN')
            ->setCellValue('K'.($taxCoordinate+1), 'Penghasilan Kotor')
            ->setCellValue('O'.($taxCoordinate+1), $brutto)
            ->setCellValue('K'.($taxCoordinate+2), 'Iuran JP TK(1%)')
            ->setCellValue('O'.($taxCoordinate+2), $rowData['emp_jp'])
            ->setCellValue('K'.($taxCoordinate+3), 'Iuran JHT')
            ->setCellValue('O'.($taxCoordinate+3), $rowData['emp_jht'])            
            ->setCellValue('K'.($taxCoordinate+4), 'Tunjangan Jabatan')
            ->setCellValue('O'.($taxCoordinate+4), $nonTaxAllowance);

        /* START NETTO */
        $netto = $brutto - $rowData['emp_jht'] - $rowData['emp_jp'] - $nonTaxAllowance;


        /* START TAX PROCESS */
        $ptkpTotal = $rowData['ptkp_total'];
        $unFixedIncome = $rowData['thr']+$rowData['contract_bonus']+$rowData['drilling_bonus']+$rowData['act_manager_bonus'];

        /* Perhitungan pajak yang disetahunkan, hanya penghasilan tetap saja. Tunjangan tidak tetap seperti THR dihitung terpisah */
        // $nettoSetahun = ( ($netto - $unFixedIncome) * 12) + $unFixedIncome;
        $nettoSetahun = ($netto - $unFixedIncome) * 12;
        $nettoSetahunFixUn = ( ($netto - $unFixedIncome) * 12) + $unFixedIncome;


        $penghasilanPajak = 0;
        if($nettoSetahun >= $ptkpTotal)
        {
            $penghasilanPajak = $nettoSetahun - $ptkpTotal;
        }
        $pembulatanPenghasilan = floor($penghasilanPajak);

        $penghasilanPajakFixUn = 0;
        if($nettoSetahunFixUn >= $ptkpTotal)
        {
            $penghasilanPajakFixUn = $nettoSetahunFixUn - $ptkpTotal;
        }
        $pembulatanPenghasilanFixUn = floor($penghasilanPajakFixUn);
        
        $nettoCoordinate = 54;
        $spreadsheet->getActiveSheet()->getStyle('K'.$nettoCoordinate.':O'.($nettoCoordinate))->applyFromArray($totalStyle);
        $spreadsheet->getActiveSheet()->getStyle('K'.$nettoCoordinate.':O'.($nettoCoordinate))->applyFromArray($topBorderStyle);
        $spreadsheet->getActiveSheet()
            ->setCellValue('K'.$nettoCoordinate, 'Netto')
            ->setCellValue('O'.$nettoCoordinate, $netto);
        /* END NETTO */
        /*if($brutto <= 0)
        {
            $brutto = 0;    
        }*/ 
        $yearNettoCoordinate = 55;
        $spreadsheet->getActiveSheet()->getStyle('K'.($yearNettoCoordinate+2).':O'.($yearNettoCoordinate+2))->applyFromArray($topBorderStyle);
        $spreadsheet->getActiveSheet()
            ->setCellValue('K'.$yearNettoCoordinate, 'Netto Disetahunkan(Tetap)')
            ->setCellValue('O'.$yearNettoCoordinate, $nettoSetahun)
            ->setCellValue('K'.($yearNettoCoordinate+1), 'PTKP '.$rowData['marital_status'])
            ->setCellValue('O'.($yearNettoCoordinate+1), $ptkpTotal)
            ->setCellValue('K'.($yearNettoCoordinate+2), 'PENGHASILAN KENA PAJAK')
            ->setCellValue('O'.($yearNettoCoordinate+2), $penghasilanPajak)
            ->setCellValue('K'.($yearNettoCoordinate+3), 'Pembulatan')
            ->setCellValue('O'.($yearNettoCoordinate+3), $pembulatanPenghasilan);

        $taxPercent1 = $this->M_payroll_config->getTaxPercent1(); /* Persentase Progresif Pajak I */
        $taxPercent2 = $this->M_payroll_config->getTaxPercent2(); /* Persentase Progresif Pajak II */
        $taxPercent3 = $this->M_payroll_config->getTaxPercent3(); /* Persentase Progresif Pajak III */
        $taxPercent4 = $this->M_payroll_config->getTaxPercent4(); /* Persentase Progresif Pajak IV */
        $maxTaxVal1 = $this->M_payroll_config->getTaxMaxVal1(); /* Nominal Pajak I */ 
        $maxTaxVal2 = $this->M_payroll_config->getTaxMaxVal2(); /* Nominal Pajak II */
        $maxTaxVal3 = $this->M_payroll_config->getTaxMaxVal3(); /* Nominal Pajak III */
        $maxTaxVal4 = $this->M_payroll_config->getTaxMaxVal4(); /* Nominal Pajak IV */
        $taxVal1 = 0;    
        $taxVal2 = 0;    
        $taxVal3 = 0;    
        $taxVal4 = 0;

        $tVal = 0;
        $tSisa = 0;
        if($pembulatanPenghasilan > 0)
        {
            /* TAX 1 */
            if($maxTaxVal1 > 0)
            {
                $tVal = $pembulatanPenghasilan/$maxTaxVal1;  
                if($tVal >= 1){
                    $taxVal1 = $maxTaxVal1 * ($taxPercent1/100);
                }
                else{
                    $taxVal1 = $pembulatanPenghasilan * ($taxPercent1/100);
                }    
            }    
            
            /* TAX 2 */
            if($maxTaxVal2 > 0)
            {
                if($pembulatanPenghasilan > $maxTaxVal1)
                {
                    $tSisa = $pembulatanPenghasilan - $maxTaxVal1; 
                    $tVal = $tSisa/$maxTaxVal2;
                    if($tVal >= 1){
                        $taxVal2 = $maxTaxVal2 * ($taxPercent2/100);
                    }
                    else{
                        $taxVal2 = $tSisa * ($taxPercent2/100);
                    }
                }     
            }
             
            /* TAX 3 */
            if($maxTaxVal3 > 0)
            {
                if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2))
                {
                    $tSisa = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2;
                    $tVal = $tSisa/$maxTaxVal3;
                    if($tVal >= 1){
                        $taxVal3 = $maxTaxVal3 * ($taxPercent3/100);
                    }
                    else{
                        $taxVal3 = $tSisa * ($taxPercent3/100);
                    }
                }    
            }
             
            /* TAX 4 */
            if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2 + $maxTaxVal3))
            {
                $tSisa = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2 - $maxTaxVal3;
                $taxVal4 = $tSisa * ($taxPercent4/100); 
            }               
        }
        
        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, 63, $line);
        $pajakTerutang = $taxVal1 + $taxVal2 + $taxVal3 + $taxVal4;



        /* NON REGULAR TAX */
        $taxValFixUn1 = 0;    
        $taxValFixUn2 = 0;    
        $taxValFixUn3 = 0;    
        $taxValFixUn4 = 0;

        $tValFixUn = 0;
        $tSisaFixUn = 0;
        if($pembulatanPenghasilanFixUn > 0)
        {
            /* TAX 1 */
            if($maxTaxVal1 > 0)
            {
                $tValFixUn = $pembulatanPenghasilanFixUn/$maxTaxVal1;  
                if($tValFixUn >= 1){
                    $taxValFixUn1 = $maxTaxVal1 * ($taxPercent1/100);
                }
                else{
                    $taxValFixUn1 = $pembulatanPenghasilanFixUn * ($taxPercent1/100);
                }    
            }    
            
            /* TAX 2 */
            if($maxTaxVal2 > 0)
            {
                if($pembulatanPenghasilanFixUn > $maxTaxVal1)
                {
                    $tSisaFixUn = $pembulatanPenghasilanFixUn - $maxTaxVal1; 
                    $tValFixUn = $tSisaFixUn/$maxTaxVal2;
                    if($tValFixUn >= 1){
                        $taxValFixUn2 = $maxTaxVal2 * ($taxPercent2/100);
                    }
                    else{
                        $taxValFixUn2 = $tSisaFixUn * ($taxPercent2/100);
                    }
                }     
            }
             
            /* TAX 3 */
            if($maxTaxVal3 > 0)
            {
                if($pembulatanPenghasilanFixUn > ($maxTaxVal1 + $maxTaxVal2))
                {
                    $tSisaFixUn = $pembulatanPenghasilanFixUn - $maxTaxVal1 - $maxTaxVal2;
                    $tValFixUn = $tSisaFixUn/$maxTaxVal3;
                    if($tValFixUn >= 1){
                        $taxValFixUn3 = $maxTaxVal3 * ($taxPercent3/100);
                    }
                    else{
                        $taxValFixUn3 = $tSisaFixUn * ($taxPercent3/100);
                    }
                }    
            }
             
            /* TAX 4 */
            if($pembulatanPenghasilanFixUn > ($maxTaxVal1 + $maxTaxVal2 + $maxTaxVal3))
            {
                $tSisaFixUn = $pembulatanPenghasilanFixUn - $maxTaxVal1 - $maxTaxVal2 - $maxTaxVal3;
                $taxValFixUn4 = $tSisaFixUn * ($taxPercent4/100); 
            }               
        }
        
        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, 63, $line);
        $pajakTerutangFixUn = $taxValFixUn1 + $taxValFixUn2 + $taxValFixUn3 + $taxValFixUn4;
        $pajakTerutangFixUn = $pajakTerutangFixUn - $pajakTerutang;

        // echo $pajakTerutangFixUn.' -- '.$pajakTerutang; exit(0);  
        // echo $nettoSetahunFixUn.' -- '.$nettoSetahun; exit(0);  


        /* Additional Charge if there is no NPWP */
        if( strlen($npwpNo) < 19 ){
            $pajakTerutang = $pajakTerutang + ($pajakTerutang * ($npwpCharge/100) ); 
            $pajakTerutangFixUn = $pajakTerutangFixUn + ($pajakTerutangFixUn * ($npwpCharge/100) ); 
            $spreadsheet->getActiveSheet()
                ->setCellValue('K65', '* Tidak Memiliki NPWP dikenakan pajak plus 20% ');
            $spreadsheet->getActiveSheet()->getStyle("K65")->getFont()
            ->setSize(14)
            ->getColor()
            ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);
        }

        $tarifTaxCoordinate = $taxCoordinate + 9; /* TAX ROWS COORDINATE  */        
        $pajakSebulan = ($pajakTerutang/12);
        $pajakSebulan = floor($pajakSebulan);

        // $pajakSebulanFixUn = ($pajakTerutangFixUn/12);
        // $pajakSebulanFixUn = floor($pajakSebulanFixUn);

         

        $spreadsheet->getActiveSheet()->getStyle('K'.($tarifTaxCoordinate+5).':O'.($tarifTaxCoordinate+6))->applyFromArray($totalStyle);
        $spreadsheet->getActiveSheet()->getStyle('K'.($tarifTaxCoordinate+5).':O'.($tarifTaxCoordinate+5))->applyFromArray($topBorderStyle);
        // $tarifTaxCoordinate = $taxCoordinate + 12; /* TAX ROWS COORDINATE  */
        $spreadsheet->getActiveSheet()
            ->setCellValue('K'.($tarifTaxCoordinate+1), 'Tarif Pajak I    '.$taxPercent1.'%')
            ->setCellValue('O'.($tarifTaxCoordinate+1), $taxVal1)
            ->setCellValue('K'.($tarifTaxCoordinate+2), 'Tarif Pajak II    '.$taxPercent2.'%')
            ->setCellValue('O'.($tarifTaxCoordinate+2), $taxVal2)
            ->setCellValue('K'.($tarifTaxCoordinate+3), 'Tarif Pajak III    '.$taxPercent3.'%')
            ->setCellValue('O'.($tarifTaxCoordinate+3), $taxVal3)
            ->setCellValue('K'.($tarifTaxCoordinate+4), 'Tarif Pajak IV    '.$taxPercent4.'%')
            ->setCellValue('O'.($tarifTaxCoordinate+4), $taxVal4)
            ->setCellValue('K'.($tarifTaxCoordinate+5), 'Pajak Gaji Setahun')
            ->setCellValue('O'.($tarifTaxCoordinate+5), $pajakTerutang)
            ->setCellValue('K'.($tarifTaxCoordinate+6), 'Pajak Gaji Sebulan')
            ->setCellValue('O'.($tarifTaxCoordinate+6), $pajakSebulan);

        if( $pajakTerutangFixUn > 0) {
            
            $spreadsheet->getActiveSheet()
                ->setCellValue('K66', 'Pajak Non Reguler')
                // ->setCellValue('K67', $pajakTerutang)
                ->setCellValue('O66', $pajakTerutangFixUn);

            $spreadsheet->getActiveSheet()->getStyle('K66:O66')->applyFromArray($totalStyle);
            $spreadsheet->getActiveSheet()->getStyle('K66:O66')->applyFromArray($topBorderStyle);

        }

        /* START UPDATE TAX VALUE TO THE TABLE */            
        $str = "UPDATE trn_slip_ptlbwg SET tax_value = ".($pajakSebulan+$pajakTerutangFixUn).", non_tax_allowance=".$nonTaxAllowance." WHERE salary_slip_id = '".$slipId."' ";
        $this->db->query($str);
        /* END UPDATE TAX VALUE TO THE TABLE */

        /* START OUT */
        $terima = $brutto-floor($pajakSebulan+$pajakTerutangFixUn)-$rowData['jkk_jkm']-$rowData['emp_jht']-$rowData['emp_health_bpjs']-$rowData['emp_jp']-$rowData['health_bpjs'];
        $outCoordinate = 34;
        $spreadsheet->getActiveSheet()
            ->setCellValue('K'.$outCoordinate, 'Pajak Penghasilan')
            ->setCellValue('O'.$outCoordinate, 0)
            ->setCellValue('K'.($outCoordinate+1), 'JKK & JKM(2.04%)')
            ->setCellValue('O'.($outCoordinate+1), 0)
            ->setCellValue('K'.($outCoordinate+2), 'Iuran JHT BPJS TK(2%)')
            ->setCellValue('O'.($outCoordinate+2), 0)
            ->setCellValue('K'.($outCoordinate+3), 'Iuran BPJS Kesehatan TK(1%)')
            ->setCellValue('O'.($outCoordinate+3), 0)
            ->setCellValue('K'.($outCoordinate+4), 'Iuran BPJS Kesehatan(4%)')
            ->setCellValue('O'.($outCoordinate+4), 0)
            ->setCellValue('K'.($outCoordinate+5), 'Iuran JP TK(1%)')
            ->setCellValue('O'.($outCoordinate+5), 0);

        $pajakSebulan = floor($pajakSebulan);
        $pajakTerutangFixUn = floor($pajakTerutangFixUn);

        if($pajakSebulan > 0) {
            $spreadsheet->getActiveSheet()
             ->setCellValue('O'.$outCoordinate, -($pajakSebulan+$pajakTerutangFixUn));
        }

        if($rowData['jkk_jkm'] > 0) {
            $spreadsheet->getActiveSheet()
             ->setCellValue('O'.($outCoordinate+1), -$rowData['jkk_jkm']);
        } 

        if($rowData['emp_jht'] > 0) {
            $spreadsheet->getActiveSheet()
             ->setCellValue('O'.($outCoordinate+2), -$rowData['emp_jht']);
        } 

        if($rowData['emp_health_bpjs'] > 0) {
            $spreadsheet->getActiveSheet()
             ->setCellValue('O'.($outCoordinate+3), -$rowData['emp_health_bpjs']);
        } 

        if($rowData['health_bpjs'] > 0) {
            $spreadsheet->getActiveSheet()
             ->setCellValue('O'.($outCoordinate+4), -$rowData['health_bpjs']);
        } 

        if($rowData['emp_jp'] > 0) {
            $spreadsheet->getActiveSheet()
             ->setCellValue('O'.($outCoordinate+5), -$rowData['emp_jp']);
        } 
        
        $outCoordinate2 = $outCoordinate + 6;
        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $outCoordinate2, "TOTAL SEBELUM POTONGAN");
        $spreadsheet->getActiveSheet()->getStyle('K'.$outCoordinate2.':O'.$outCoordinate2)->applyFromArray($topBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle('K'.$outCoordinate2.':O'.$outCoordinate2)->applyFromArray($totalStyle);
        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(14, $outCoordinate2, $terima);

        $debtText = "";
        if($rowData['debt_explanation'] != "") {
            $debtText = "(".$rowData['debt_explanation'].")";
        }
        $tBurden = $rowData['debt_burden'];
        $tBurden = $tBurden * (-1);

        $cutCoordinate = 41;
        $spreadsheet->getActiveSheet()
            ->setCellValue('K'.$outCoordinate2, 'Potong Pajak')
            ->setCellValue('O'.$outCoordinate2, $terima)
            ->setCellValue('K'.($cutCoordinate+1), 'THR')
            ->setCellValue('O'.($cutCoordinate+1), 0)
            ->setCellValue('K'.($cutCoordinate+2), 'Contract Bonus')
            ->setCellValue('O'.($cutCoordinate+2), 0)
            ->setCellValue('K'.($cutCoordinate+3), 'Drilling Bonus')
            // ->setCellValue('K'.($cutCoordinate+4), 'Act Manager Bonus')
            
            ->setCellValue('K'.($cutCoordinate+4), 'Adj')
            ->setCellValue('O'.($cutCoordinate+4), '0')

            ->setCellValue('K'.($cutCoordinate+5), 'Beban Hutang')
            ->setCellValue('M'.($cutCoordinate+5), $debtText)
            ->setCellValue('O'.($cutCoordinate+5), $tBurden);

        /* START POTONGAN */
        if($rowData['thr'] > 0) {
            $spreadsheet->getActiveSheet()
            ->setCellValue('O'.($cutCoordinate+1), -$rowData['thr']);
        } 

        if($rowData['contract_bonus'] > 0) {
            $spreadsheet->getActiveSheet()
              ->setCellValue('O'.($cutCoordinate+2), -$rowData['contract_bonus']);
        }

        if($rowData['drilling_bonus'] > 0) {
            $spreadsheet->getActiveSheet()
              ->setCellValue('O'.($cutCoordinate+3), -$rowData['drilling_bonus']);
        }

        // if($rowData['act_manager_bonus'] > 0) {
        //     $spreadsheet->getActiveSheet()
        //       ->setCellValue('O'.($cutCoordinate+4), $rowData['act_manager_bonus']);
        // }

        if($rowData['workday_adj'] > 0) {
            $spreadsheet->getActiveSheet()
              ->setCellValue('O'.($cutCoordinate+4), $rowData['workday_adj']);
        }         
        /* END POTONGAN */
        
        $cutCoordinate2 = $cutCoordinate+4;
        $cutCoordinate3 = $cutCoordinate+6;
        $spreadsheet->getActiveSheet()->getStyle("K".$cutCoordinate.":O".$cutCoordinate3)->getFont()->setSize(13);
        $spreadsheet->getActiveSheet()->getStyle('K'.$cutCoordinate3.':O'.$cutCoordinate3)->applyFromArray($totalStyle);
        $spreadsheet->getActiveSheet()->getStyle('K'.$cutCoordinate3.':O'.$cutCoordinate3)->applyFromArray($topBorderStyle);

        $thp = $terima - 
               $rowData['thr'] - 
               $rowData['contract_bonus'] - 
               $rowData['debt_burden'] - 
               $rowData['drilling_bonus'] + 
               // $rowData['act_manager_bonus'] + 
               $rowData['workday_adj'];

        $spreadsheet->getActiveSheet()
              ->setCellValue('K'.($cutCoordinate2+2), 'TOTAL TERIMA')
              ->setCellValue('O'.($cutCoordinate2+2), $thp);
        /* END OUT */

        $spreadsheet->getActiveSheet()
          ->getStyle('K7:O66')->getNumberFormat()
          ->setFormatCode('#,##0.00');

        // Rename worksheet
        $nama_payroll       = substr($rowData['name'],0,30);
        $spreadsheet->getActiveSheet()->setTitle($nama_payroll);

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        //Nama File
        $str = $rowData['name'].$rowData['bio_rec_id'];
        $fileName = preg_replace('/\s+/', '', $str);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.Xlsx"');
        // header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        /* BY COMPOSER */
        // $writer = new Xlsx($spreadsheet);
        /* OFFLINE/ BY COPY EXCEL FOLDER  */
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit(0);

    }

    public function getSlipByIid(){
        $slipId = "";
        if(isset($_POST['slipId']))
        {
            $slipId = $_POST['slipId'];
        }

        $this->db->select('*');
        $this->db->from($this->db->database.'.trn_slip_ptlbwg');
        $this->db->where('salary_slip_id', $slipId);
        $query = $this->db->get()->row_array();
        $myData = array();
        $myData[0] = $query['salary_slip_id']; 
        $myData[1] = $query['name'];
        $myData[2] = $query['production_bonus'];
        $myData[3] = $query['workday_adj'];
        $myData[4] = $query['adjust_in'];
        $myData[5] = $query['adjust_out'];
        $myData[6] = $query['contract_bonus'];
        $myData[7] = $query['flying_camp'];
        $myData[8] = $query['safety_bonus'];
        $myData[9] = $query['attendance_bonus'];
        $myData[10] = $query['other_allowance1'];
        $myData[11] = $query['other_allowance2'];
        $myData[12] = $query['no_accident_bonus'];
        $myData[13] = $query['thr'];
        $myData[14] = $query['debt_burden'];
        $myData[15] = $query['debt_explanation'];         
        $myData[16] = $query['dept'];         
        $myData[17] = $query['position'];         
        $myData[18] = $query['travel_bonus'];         
        $myData[19] = $query['drilling_bonus'];         
        $myData[20] = $query['act_manager_bonus'];         
        echo json_encode($myData);   
    }  

    public function updatePayroll()
    {
        $clientName = "";
        $payrollId = "";
        $productionBonus = 0;
        $workAdjustment = 0;
        $adjustmentIn = 0;
        $adjustmentOut = 0;
        $contractBonus = 0;
        $outCamp = 0;
        $safetyBonus = 0;
        $attendanceBonus = 0;
        $otherAllowance1 = 0;
        $otherAllowance2 = 0;
        $oAllowanceTra1 = 0;
        $oAllowanceTra2 = 0;
        $travelBonus = 0;
        $drillingBonus = 0;
        $actManagerBonus = 0;
        // $general1 = 0;
        // $general2 = 0;
        // $general3 = 0;
        // $general4 = 0;
        // $general5 = 0;
        // $general6 = 0;
        $noAccidentFee = 0;
        // $ccPayment = 0;
        $thr = 0;
        $debtBurden = 0;
        $debtExplanation = '';
        
        if(isset($_POST['clientName']))
        {
            $clientName = $this->security->xss_clean($_POST['clientName']);            
        }
        if(isset($_POST['payrollId']))
        {
            $payrollId = $this->security->xss_clean($_POST['payrollId']);            
        }
        // if(isset($_POST['productionBonus']))
        // {
        //     $productionBonus = $this->security->xss_clean($_POST['productionBonus']);            
        // }
        if(isset($_POST['workAdjustment']))
        {
            $workAdjustment = $this->security->xss_clean($_POST['workAdjustment']);            
        }

        // if(isset($_POST['adjustmentIn']))
        // {
        //     $adjustmentIn = $this->security->xss_clean($_POST['adjustmentIn']);            
        // }

        // if(isset($_POST['adjustmentOut']))
        // {
        //     $adjustmentOut = $this->security->xss_clean($_POST['adjustmentOut']);            
        // }

        if(isset($_POST['contractBonus']))
        {
            $contractBonus = $this->security->xss_clean($_POST['contractBonus']);            
        }

        if(isset($_POST['outCamp']))
        {
            $outCamp = $this->security->xss_clean($_POST['outCamp']);            
        }

        // if(isset($_POST['safetyBonus']))
        // {
        //     $safetyBonus = $this->security->xss_clean($_POST['safetyBonus']);            
        // }

        if(isset($_POST['attendanceBonus']))
        {
            $attendanceBonus = $this->security->xss_clean($_POST['attendanceBonus']);            
        }

        
        // if(isset($_POST['noAccidentFee']))
        // {
        //     $noAccidentFee = $this->security->xss_clean($_POST['noAccidentFee']);            
        // }
        if(isset($_POST['drillingBonus']))
        {
            $drillingBonus = $this->security->xss_clean($_POST['drillingBonus']);            
        }
        
        if(isset($_POST['actManagerBonus']))
        {
            $actManagerBonus = $this->security->xss_clean($_POST['actManagerBonus']);            
        }

        if(isset($_POST['thr']))
        {
            $thr = $this->security->xss_clean($_POST['thr']);            
        }

        if(isset($_POST['debtBurden']))
        {
            $debtBurden = $this->security->xss_clean($_POST['debtBurden']);            
        }

        if(isset($_POST['debtExplanation']))
        {
            $debtExplanation = $this->security->xss_clean($_POST['debtExplanation']);            
        }

        // $this->load->model('M_salary_slip');
        $rs = $this->M_salary_slip->getObjectById($payrollId);
        if(isset($_POST['travelBonus'])) {
            $travelBonus = $this->security->xss_clean($_POST['travelBonus']);
        }
        
        $this->M_salary_slip->setTravelBonus($travelBonus);
        
        $this->M_salary_slip->setDrillingBonus($drillingBonus);
        $this->M_salary_slip->setActManagerBonus($actManagerBonus);

        
        $this->M_salary_slip->setProductionBonus($productionBonus);
        $this->M_salary_slip->setWorkdayAdj($workAdjustment);
        $this->M_salary_slip->setAdjustIn($adjustmentIn);
        $this->M_salary_slip->setAdjustOut($adjustmentOut);
        $this->M_salary_slip->setContractBonus($contractBonus);
        $this->M_salary_slip->setFlyingCamp($outCamp);
        $this->M_salary_slip->setSafetyBonus($safetyBonus);
        $this->M_salary_slip->setAttendanceBonus($attendanceBonus);
        $this->M_salary_slip->setOtherAllowance1($otherAllowance1);
        $this->M_salary_slip->setOtherAllowance2($otherAllowance2);

        // $this->M_salary_slip->setGeneral1($general1);/*Mobilization*/
        // $this->M_salary_slip->setGeneral2($general2);/*Uniform*/
        // $this->M_salary_slip->setGeneral3($general3);/*MCU*/
        // $this->M_salary_slip->setGeneral4($general4);/*Ticket*/
        // $this->M_salary_slip->setGeneral5($general5);/*APD*/
        // $this->M_salary_slip->setGeneral6($general6);/*Travel*/
        
        $this->M_salary_slip->setNoAccidentBonus($noAccidentFee);
        // $this->M_salary_slip->setCcPayment($ccPayment);
        $this->M_salary_slip->setThr($thr);
        $this->M_salary_slip->setDebtBurden($debtBurden);
        $this->M_salary_slip->setDebtExplanation($debtExplanation);
        $this->M_salary_slip->update($payrollId);
        // echo $payrollId;
    } 

    /* START PAYMENT EXPORT */
    public function exportPaymentList($ptName, $yearPeriod, $monthPeriod, $group)
    {
        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();  

        $spreadsheet->getProperties()->setCreator('Maurice - Web - Android')
            ->setLastModifiedBy('Maurice - Web - Android')
            ->setTitle('Office 2007 XLSX Test Document')
            ->setSubject('Office 2007 XLSX Test Document')
            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Test result file');

        if (file_exists('assets/images/report_logo.png')) {
            $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing->setName('Logo');
            $drawing->setDescription('Logo');
            $drawing->setPath('./assets/images/report_logo.png');
            $drawing->setCoordinates('A1');
            $drawing->setHeight(38);
            $drawing->setWorksheet($spreadsheet->getActiveSheet());
        }

        $strFilter = "";                          
        if($group !='ALL'){
            $strFilter .= "AND ms.payroll_group = '".$group."' ";
        }

        $strSQL  = "SELECT 0 jumboPercent,0 jumboVal,ss.salary_slip_id,mb.bio_rec_id,mb.full_name,ms.nie,mb.position,ms.bank_code,ms.bank_name,ms.account_name,ms.account_no,mb.npwp_no,ss.ptkp_total,ss.thr,ss.contract_bonus,ss.emp_jht,ss.emp_jp,ss.drilling_bonus,ss.act_manager_bonus,ss.debt_burden,ms.payroll_group,ss.tax_value,ss.attendance_bonus,ss.workday_adj,ms.payroll_group,";
        $strSQL .= " (";
        $strSQL .= " CAST("; /*START CAST*/
        $strSQL .= " ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 + ";
        $strSQL .= " ss.flying_camp +";
        $strSQL .= " ss.travel_bonus +";
        $strSQL .= " ss.attendance_bonus +";
        $strSQL .= " ss.thr+";
        $strSQL .= " ss.contract_bonus+";
        $strSQL .= " ss.drilling_bonus+";
        $strSQL .= " ss.act_manager_bonus+";
        // $strSQL .= " CASE WHEN(ms.is_cc_payment = 1) THEN ss.cc_payment ELSE 0 END +"; /*ok*/
        // $strSQL .= " ss.workday_adj+";
        $strSQL .= " ss.jkk_jkm+";
        $strSQL .= " ss.health_bpjs-";
        $strSQL .= " ss.unpaid_total+";
        $strSQL .= " ss.adjust_in-";
        $strSQL .= " ss.adjust_out-";

        $strSQL .= " ss.jkk_jkm-";
        $strSQL .= " ss.emp_jht-";
        $strSQL .= " ss.emp_health_bpjs-";
        $strSQL .= " ss.health_bpjs-";
        $strSQL .= " ss.emp_jp-";

        $strSQL .= " ss.thr-";
        $strSQL .= " ss.contract_bonus-";
        $strSQL .= " ss.drilling_bonus+";
        $strSQL .= " ss.workday_adj-";
        $strSQL .= " ss.debt_burden";

        $strSQL .= " AS DECIMAL(11,2))  "; /*END CAST*/
        $strSQL .= " ) AS total, ";

        /* START BRUTTO */
        $strSQL .= " (";
        $strSQL .= " CAST("; /*START CAST*/
        // $strSQL .= " ("; /*START SETAHUNKAN*/
        $strSQL .= " ss.bs_prorate + ss.ot_1 + ss.ot_2 + ss.ot_3 + ss.ot_4 + ";
        $strSQL .= " ss.flying_camp +";
        $strSQL .= " ss.travel_bonus +";
        $strSQL .= " ss.attendance_bonus +";
        $strSQL .= " ss.thr+";
        $strSQL .= " ss.contract_bonus+";
        $strSQL .= " ss.drilling_bonus+";
        $strSQL .= " ss.act_manager_bonus+";
        // $strSQL .= " CASE WHEN(ms.is_cc_payment = 1) THEN ss.cc_payment ELSE 0 END +"; /*ok*/
        // $strSQL .= " ss.workday_adj+";
        $strSQL .= " ss.jkk_jkm+";
        $strSQL .= " ss.health_bpjs-";
        $strSQL .= " ss.unpaid_total+";
        $strSQL .= " ss.adjust_in-";
        $strSQL .= " ss.adjust_out";
        // $strSQL .= " ss.tax_value-";
        // $strSQL .= " ss.jkk_jkm-";
        /*$strSQL .= " ss.emp_jht";*/
        // $strSQL .= " ss.emp_health_bpjs ";
        // $strSQL .= " ss.health_bpjs";
        // $strSQL .= " )*12"; /*END SETAHUNKAN*/
        $strSQL .= " AS DECIMAL(11,2))  "; /*END CAST*/
        $strSQL .= " ) AS brutto_tax ";
        /* END BRUTTO */

        $strSQL .= "FROM mst_salary ms,mst_bio_rec mb,trn_slip_ptlbwg ss ";
        $strSQL .= "WHERE ms.company_name = '".$ptName."' ";
        $strSQL .= "AND ss.month_period = '".$monthPeriod."' ";
        $strSQL .= "AND ss.year_period = '".$yearPeriod."' ";
        // $strSQL .= "AND ms.payroll_group = '".$group."' ";
        $strSQL .= "AND ms.bio_rec_id = mb.bio_rec_id  ";
        $strSQL .= "AND mb.bio_rec_id = ss.bio_rec_id ";
        $strSQL .= "AND mb.is_active = 1 ";   
        $strSQL .= $strFilter;  
        $strSQL .= "ORDER BY mb.full_name ";    
        
        // echo $strSQL; exit(0);coba

        $query = $this->db->query($strSQL)->result_array();   
        /* AUTO WIDTH */     
        foreach(range('B','H') as $columnID)
        {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }

        // Nama Field Baris Pertama
        $spreadsheet->getActiveSheet()
            ->setCellValue('A1', 'PT. SANGATI SOERYA SEJAHTERA')
            ->setCellValue('A2', 'LIST PEMBAYARAN GAJI KARYAWAN PT. '.strtoupper($ptName).' ('.$group.')')
            ->setCellValue('A4', 'Periode : '.$monthPeriod.'-'.$yearPeriod);

        $spreadsheet->getActiveSheet()->getStyle("A1:D1")->getFont()->setBold(true)->setSize(16);
        $spreadsheet->getActiveSheet()->getStyle("A2:D2")->getFont()->setBold(true)->setSize(13);
        $spreadsheet->getActiveSheet()->getStyle("A4:G4")->getFont()->setBold(true)->setSize(12); 

        $spreadsheet->getActiveSheet()
            ->mergeCells("A1:H1")
            ->mergeCells("A2:H2")
            ->mergeCells("A4:H4");

        $boldFont = [
            'font' => [
                'bold' => true
                // 'color' => ['argb' => '0000FF'],
            ],
        ];

        $totalStyle = [
            'font' => [
                'bold' => true,
                'color' => ['argb' => '0000FF'],
            ],
        ];

        $allBorderStyle = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $outlineBorderStyle = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $topBorderStyle = [
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $bottomBorderStyle = [
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        /* COLOURING FOOTER */
        $spreadsheet->getActiveSheet()->getStyle("A6:H7")
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B'); 

        $spreadsheet->getActiveSheet()
            ->setCellValue('A6', 'NO')  
            ->setCellValue('B6', 'NAMA')  
            ->setCellValue('C6', 'ID')  
            // ->setCellValue('D6', 'CLASS')  
            ->setCellValue('D6', 'BANK CODE')  
            ->setCellValue('E6', 'BANK')  
            ->setCellValue('F6', 'NAMA REKENING')  
            ->setCellValue('G6', 'NO REKENING')  
            ->setCellValue('H6', 'JUMLAH');  

        $spreadsheet->getActiveSheet()->getStyle("A6:H6")->getFont()->setSize(12);
        $spreadsheet->getActiveSheet()->getStyle("A6:H6")->applyFromArray($outlineBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("A6:A7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("B6:B7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("C6:C7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("D6:D7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("E6:E7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("F6:F7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("G6:G7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("H6:H7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("A6:A7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("B6:B7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("C6:C7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("D6:D7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("E6:E7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("F6:F7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("G6:G7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("H6:H7")->applyFromArray($center);

        $spreadsheet->getActiveSheet()->getStyle("A1:H4")->applyFromArray($center);

        $spreadsheet->getActiveSheet()
            ->mergeCells("A6:A7")
            ->mergeCells("B6:B7")
            ->mergeCells("C6:C7")
            ->mergeCells("D6:D7")
            ->mergeCells("E6:E7")
            ->mergeCells("F6:F7")
            ->mergeCells("G6:G7")
            ->mergeCells("H6:H7");
            // ->mergeCells("I6:I7");

        $spreadsheet->getActiveSheet()->getStyle("B6:B7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("C6:C7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("D6:D7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("E6:E7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("F6:F7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("G6:G7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle("H6:H7")->getAlignment()->setWrapText(true);
        // $spreadsheet->getActiveSheet()->getStyle("I6:I7")->getAlignment()->setWrapText(true);


        $bruttoTax = 0;
        /* TAX CONFIG */
        $myConfigId = 0;    
        switch ($ptName) {
            case 'Pontil_Banyuwangi':
                # code...
                $myConfigId = 9;
                break;          
            default:
                # code...
                break;
        }
        $row = $this->M_payroll_config->getObjectById($myConfigId); 
        $taxPercent1 = $this->M_payroll_config->getTaxPercent1(); /* Persentase Progresif Pajak I */
        $taxPercent2 = $this->M_payroll_config->getTaxPercent2(); /* Persentase Progresif Pajak II */
        $taxPercent3 = $this->M_payroll_config->getTaxPercent3(); /* Persentase Progresif Pajak III */
        $taxPercent4 = $this->M_payroll_config->getTaxPercent4(); /* Persentase Progresif Pajak IV */
        $maxTaxVal1 = $this->M_payroll_config->getTaxMaxVal1(); /* Nominal Pajak I */ 
        $maxTaxVal2 = $this->M_payroll_config->getTaxMaxVal2(); /* Nominal Pajak II */
        $maxTaxVal3 = $this->M_payroll_config->getTaxMaxVal3(); /* Nominal Pajak III */
        $maxTaxVal4 = $this->M_payroll_config->getTaxMaxVal4(); /* Nominal Pajak IV */
        $maxNonTax = $this->M_payroll_config->getNonTaxAllowance(); /* Max Tunjangan Jabatan */
        $npwpCharge = $this->M_payroll_config->getNpwpCharge();

        $rowIdx = 8;
        $rowNo = 0;
        foreach ($query as $row) {
            $rowIdx++;
            $rowNo++;

            /* START UPDATE TAX */
            $bruttoTax = $row['brutto_tax'];
            $pembulatanPenghasilan = $bruttoTax;
            $nonTaxAllowance = ($pembulatanPenghasilan) * (5/100);
            if($nonTaxAllowance > $maxNonTax){
                $nonTaxAllowance = $maxNonTax;
            }

            $netto = $bruttoTax - $row['emp_jht'] - $row['emp_jp'] - $nonTaxAllowance;
            $unFixedIncome = $row['thr'] + $row['contract_bonus'] + $row['drilling_bonus'] + $row['act_manager_bonus'];
            // $nettoSetahun = ( ($netto - $unFixedIncome)*12 ) + $unFixedIncome;
            $nettoSetahun = ( ($netto - $unFixedIncome)*12 );
            $monthlyTax = 0;

            $taxVal1 = 0;    
            $taxVal2 = 0;    
            $taxVal3 = 0;    
            $taxVal4 = 0;

            $tVal = 0;
            $tSisa = 0;
            $pembulatanPenghasilan = $nettoSetahun - $row['ptkp_total'];
            if($pembulatanPenghasilan > 0)
            {
                /* TAX 1 */
                if($maxTaxVal1 > 0)
                {
                    $tVal = $pembulatanPenghasilan/$maxTaxVal1;  
                    if($tVal >= 1){
                        $taxVal1 = $maxTaxVal1 * ($taxPercent1/100);
                    }
                    else{
                        $taxVal1 = $pembulatanPenghasilan * ($taxPercent1/100);
                    }    
                }    
                
                /* TAX 2 */
                if($maxTaxVal2 > 0)
                {
                    if($pembulatanPenghasilan > $maxTaxVal1)
                    {
                        $tSisa = $pembulatanPenghasilan - $maxTaxVal1; 
                        $tVal = $tSisa/$maxTaxVal2;
                        if($tVal >= 1){
                            $taxVal2 = $maxTaxVal2 * ($taxPercent2/100);
                        }
                        else{
                            $taxVal2 = $tSisa * ($taxPercent2/100);
                        }
                    }     
                }
                 
                /* TAX 3 */
                if($maxTaxVal3 > 0)
                {
                    if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2))
                    {
                        $tSisa = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2;
                        $tVal = $tSisa/$maxTaxVal3;
                        if($tVal >= 1){
                            $taxVal3 = $maxTaxVal3 * ($taxPercent3/100);
                        }
                        else{
                            $taxVal3 = $tSisa * ($taxPercent3/100);
                        }
                    }    
                }
                 
                /* TAX 4 */
                if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2 + $maxTaxVal3))
                {
                    $tSisa = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2 - $maxTaxVal3;
                    $taxVal4 = $tSisa * ($taxPercent4/100); 
                }               
            }

            /* START THR - BONUS TAX -- EDITED @ AUG 15, 2017 BY FINANCE  */
            // $bonusTmp = $row['thr'] + $row['contract_bonus'];
            // $bonusTax = 0;
            // if( $taxVal4 > 0){
            //     $bonusTax = $bonusTmp * ($taxPercent4/100);
            // }else if( $taxVal3 > 0 ){
            //     $bonusTax = $bonusTmp * ($taxPercent3/100);
            // }else if( $taxVal2 > 0 ){
            //     $bonusTax = $bonusTmp * ($taxPercent2/100);                
            // }else{
            //     $bonusTax = $bonusTmp * ($taxPercent1/100);             
            // }


            $nettoSetahunFixUn = ( ($netto - $unFixedIncome) * 12) + $unFixedIncome;
            $ptkpTotal = $row['ptkp_total'];
            $penghasilanPajakFixUn = 0;
            if($nettoSetahunFixUn >= $ptkpTotal)
            {
                $penghasilanPajakFixUn = $nettoSetahunFixUn - $ptkpTotal;
            }
            $pembulatanPenghasilanFixUn = floor($penghasilanPajakFixUn);

            /* NON REGULAR TAX */
            $taxValFixUn1 = 0;    
            $taxValFixUn2 = 0;    
            $taxValFixUn3 = 0;    
            $taxValFixUn4 = 0;

            $tValFixUn = 0;
            $tSisaFixUn = 0;
            if($pembulatanPenghasilanFixUn > 0)
            {
                /* TAX 1 */
                if($maxTaxVal1 > 0)
                {
                    $tValFixUn = $pembulatanPenghasilanFixUn/$maxTaxVal1;  
                    if($tValFixUn >= 1){
                        $taxValFixUn1 = $maxTaxVal1 * ($taxPercent1/100);
                    }
                    else{
                        $taxValFixUn1 = $pembulatanPenghasilanFixUn * ($taxPercent1/100);
                    }    
                }    
                
                /* TAX 2 */
                if($maxTaxVal2 > 0)
                {
                    if($pembulatanPenghasilanFixUn > $maxTaxVal1)
                    {
                        $tSisaFixUn = $pembulatanPenghasilanFixUn - $maxTaxVal1; 
                        $tValFixUn = $tSisaFixUn/$maxTaxVal2;
                        if($tValFixUn >= 1){
                            $taxValFixUn2 = $maxTaxVal2 * ($taxPercent2/100);
                        }
                        else{
                            $taxValFixUn2 = $tSisaFixUn * ($taxPercent2/100);
                        }
                    }     
                }
                 
                /* TAX 3 */
                if($maxTaxVal3 > 0)
                {
                    if($pembulatanPenghasilanFixUn > ($maxTaxVal1 + $maxTaxVal2))
                    {
                        $tSisaFixUn = $pembulatanPenghasilanFixUn - $maxTaxVal1 - $maxTaxVal2;
                        $tValFixUn = $tSisaFixUn/$maxTaxVal3;
                        if($tValFixUn >= 1){
                            $taxValFixUn3 = $maxTaxVal3 * ($taxPercent3/100);
                        }
                        else{
                            $taxValFixUn3 = $tSisaFixUn * ($taxPercent3/100);
                        }
                    }    
                }
                 
                /* TAX 4 */
                if($pembulatanPenghasilanFixUn > ($maxTaxVal1 + $maxTaxVal2 + $maxTaxVal3))
                {
                    $tSisaFixUn = $pembulatanPenghasilanFixUn - $maxTaxVal1 - $maxTaxVal2 - $maxTaxVal3;
                    $taxValFixUn4 = $tSisaFixUn * ($taxPercent4/100); 
                }               
            }


            /* END THR - BONUS TAX -- EDITED @ AUG 15, 2017 BY FINANCE */

            $npwpNo = $row['npwp_no'];
            $taxTotal = ( ($taxVal1 + $taxVal2 + $taxVal3 + $taxVal4)/12 );


            $pajakTerutangFixUn = $taxValFixUn1 + $taxValFixUn2 + $taxValFixUn3 + $taxValFixUn4;
            $pajakTerutangFixUn = $pajakTerutangFixUn - ($taxVal1 + $taxVal2 + $taxVal3 + $taxVal4);

            if($pajakTerutangFixUn <= 0){
                $pajakTerutangFixUn = 0;
            }

            if( strlen($npwpNo) < 19 ){
                $taxTotal = $taxTotal + ($taxTotal * ($npwpCharge/100) ); 
                $pajakTerutangFixUn = $pajakTerutangFixUn + ($pajakTerutangFixUn * ($npwpCharge/100) ); 
                // $spreadsheet->getActiveSheet()
                //     ->setCellValue('K65', '* Tidak Memiliki NPWP dikenakan pajak plus 20% ');
                // $spreadsheet->getActiveSheet()->getStyle("K65")->getFont()
                //     ->setSize(14)
                //     ->getColor()
                //     ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);
            }

            if($taxTotal > 0){
                $monthlyTax = $taxTotal; 
            }else{
                $monthlyTax = 0; 
            }
            
            $monthlyTax = $monthlyTax + $pajakTerutangFixUn;
            // $tmpTotal = $row['total'] - floor($monthlyTax) - $row['debt_burden'] - $unFixedIncome + $row['workday_adj'];
            $tmpTotal = $row['total'] - floor($monthlyTax);
            $totalTerima = 0;
            if ($tmpTotal > 0)
            {
                $totalTerima = $tmpTotal;
            }

            /* START UPDATE TAX VALUE TO THE TABLE */
            if($row['tax_value'] <= 0)
            {
                $slipId = $row['salary_slip_id'];
                $str = "UPDATE trn_slip_ptlbwg SET tax_value = ".($monthlyTax)." WHERE salary_slip_id = '".$slipId."' ";
                $this->db->query($str);                
            }
            /* END UPDATE TAX VALUE TO THE TABLE */

            // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(7, $rowIdx, round($totalTerima) );

            // $strSQL .= " ss.jkk_jkm-";
            // $strSQL .= " ss.emp_jht-";
            // $strSQL .= " ss.emp_health_bpjs-";
            // $strSQL .= " ss.health_bpjs-";
            // $strSQL .= " ss.emp_jp";
            $spreadsheet->getActiveSheet()
                ->setCellValue('A'.$rowIdx, $rowNo)
                ->setCellValue('B'.$rowIdx, $row['full_name'])
                ->setCellValueExplicit('C'.$rowIdx, $row['nie'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING)
                // ->setCellValue('D'.$rowIdx, $row['position'])
                ->setCellValue('D'.$rowIdx, $row['bank_code'])
                ->setCellValue('E'.$rowIdx, $row['bank_name'])
                ->setCellValue('F'.$rowIdx, $row['account_name'])
                ->setCellValueExplicit('G'.$rowIdx, $row['account_no'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING)
                ->setCellValue('H'.$rowIdx, round($totalTerima))
                // ->setCellValue('I'.$rowIdx, round($bruttoTax))
                // ->setCellValue('J'.$rowIdx, $taxTotal)
                // ->setCellValue('K'.$rowIdx, $pajakTerutangFixUn)
                // ->setCellValue('L'.$rowIdx, $monthlyTax)
                // ->setCellValue('M'.$rowIdx, $tmpTotal)
                ;

            /* END UPDATE TAX */
            /* SET ROW COLOR */
            if($rowIdx % 2 == 1)
            {
                $spreadsheet->getActiveSheet()->getStyle('A'.$rowIdx.':H'.$rowIdx)
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('EAEBAF');             
            } 

        } /* end foreach ($query as $row) */       
        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(6, $rowIdx+2, "JUMLAH");
        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(7, $rowIdx+2, "=SUM(H9:H".$rowIdx.")");

        $spreadsheet->getActiveSheet()
                ->setCellValue('G'.($rowIdx+2), 'JUMLAH')
                ->setCellValue('H'.($rowIdx+2), '=SUM(H9:H'.$rowIdx.')');

        $spreadsheet->getActiveSheet()->getStyle("H6:H7")->applyFromArray($allBorderStyle);
        // $spreadsheet->getActiveSheet()->getStyle("H6:H7")->applyFromArray($totalStyle);
        $totalBorder = $rowIdx+2;
        $spreadsheet->getActiveSheet()->getStyle("A".$totalBorder.":H".$totalBorder)->applyFromArray($outlineBorderStyle);

        /* SET NUMBERS FORMAT*/
        $spreadsheet->getActiveSheet()->getStyle('H8:H'.($rowIdx+2))->getNumberFormat()->setFormatCode('#,##0.00');
        // $spreadsheet->getActiveSheet()->getStyle('A6:H7')->applyFromArray($totalStyle);
        // $spreadsheet->getActiveSheet()->getStyle('A'.($rowIdx+2).':H'.($rowIdx+2))->applyFromArray($totalStyle);
        /* COLOURING FOOTER */
        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":H".($rowIdx+2))
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B');

        unset($allBorderStyle);
        unset($center);
        unset($right);
        unset($left);
        
        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('Report Excel '.date('d-m-Y H'));

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        //Nama File
        // $str = $rowData['name'].$rowData['bio_rec_id'];
        $str = 'RPTPaymentList';
        $fileName = preg_replace('/\s+/', '', $str);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.Xlsx"');
        // header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        /* BY COMPOSER */
        // $writer = new Xlsx($spreadsheet);
        /* OFFLINE/ BY COPY EXCEL FOLDER  */
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit(0);   
    }
    /* END PAYMENT EXPORT */

    
}

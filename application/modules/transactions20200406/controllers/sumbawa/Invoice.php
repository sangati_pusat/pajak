<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    // Load library phpspreadsheet
    require('./vendor/autoload.php');
    use PhpOffice\PhpSpreadsheet\Helper\Sample;
    use PhpOffice\PhpSpreadsheet\IOFactory;
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    // End load library phpspreadsheet

class Invoice extends CI_Controller {
	
	public function index()
	{
		$this->load->view('welcome_message');
	}

    /* START INVOICE LIST EXPORT */
    public function exportInvoiceSummaryMachmahon($yearPeriod, $monthPeriod, $dept)
    {
        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();  

         $spreadsheet->getProperties()->setCreator('Maurice - Web - Android')
            ->setLastModifiedBy('Maurice - Web - Android')
            ->setTitle('Office 2007 XLSX Test Document')
            ->setSubject('Office 2007 XLSX Test Document')
            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Test result file');

         if (file_exists('assets/images/report_logo.png')) {
            $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing->setName('Logo');
            $drawing->setDescription('Logo');
            $drawing->setPath('./assets/images/report_logo.png');
            $drawing->setCoordinates('A1');
            $drawing->setHeight(36);
            $drawing->setWorksheet($spreadsheet->getActiveSheet());
        }

        $boldFont = [
            'font' => [
                'bold' => true
                // 'color' => ['argb' => '0000FF'],
            ],
        ];

        $totalStyle = [
            'font' => [
                'bold' => true,
                'color' => ['argb' => '0000FF'],
            ],
        ];

        $allBorderStyle = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $outlineBorderStyle = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $topBorderStyle = [
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $bottomBorderStyle = [
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $strSQL = "";
        $strFilter = "";
        
        $monthPeriod = $this->security->xss_clean($monthPeriod);
        $yearPeriod = $this->security->xss_clean($yearPeriod);
        $dept = $this->security->xss_clean($dept);

        $strSQL  = "SELECT ";
        $strSQL .= "  @no_urut := @no_urut+1 no_urut,"; 
        $strSQL .= "  ms.bio_rec_id,";
        $strSQL .= "  ms.nie,";
        $strSQL .= "  ss.name,";
        $strSQL .= "  ss.position job_desc,";
        $strSQL .= "  ss.basic_salary,";
        $strSQL .= "  ss.bs_prorate,";
        // $strSQL .= "  TRUNCATE((ss.basic_salary/173),1) rate,";
        $strSQL .= "  (ss.basic_salary/173) rate,";
        $strSQL .= "  ss.normal_time,";
        $strSQL .= "  ss.ot_count1,";
        $strSQL .= "  ss.ot_count2,";
        $strSQL .= "  ss.ot_count3,";
        $strSQL .= "  ss.ot_count4,";
        $strSQL .= "  ss.ot_1, ss.ot_2, ss.ot_3, ss.ot_4, ";
        $strSQL .= "  (ss.normal_time+ss.ot_count1+ss.ot_count2+ss.ot_count3+ss.ot_count4) worked_hours,";
        $strSQL .= "  ss.thr,";
        $strSQL .= "  ss.other_allowance1,"; /* KPI Benefits  */
        $strSQL .= "  (ss.jkk_jkm + ss.jht) AS bpjstk,"; /* BPJS Tenaga Kerja */
        $strSQL .= "  ss.jp,"; /* BPJS Pensiun */
        $strSQL .= "  ss.health_bpjs,"; /* BPJS Kesehatan */
        $strSQL .= "  ss.flying_camp,";
        $strSQL .= "  ss.safety_bonus,";
        $strSQL .= "  ss.contract_bonus,";
        $strSQL .= "  ss.general_1,"; /* Akomodasi */
        $strSQL .= "  ss.general_3,"; /* MCU */
        $strSQL .= "  ss.general_6,"; /* MEALS */
        $strSQL .= "  ss.other_allowance2,";
        $strSQL .= "  (ss.ot_1+ss.ot_2+ss.ot_3+ss.ot_4) ot_total ";
        $strSQL .= "FROM "; 
        $strSQL .= "  (SELECT @no_urut := 0) s,"; 
        $strSQL .= "  mst_salary ms,"; 
        $strSQL .= "  trn_salary_slip ss "; 
        $strSQL .= "WHERE "; 
        $strSQL .= "  ms.bio_rec_id = ss.bio_rec_id "; 
        $strSQL .= "AND "; 
        // $strSQL .= "  ss.client_name = '".$clientName."' "; 
        $strSQL .= "  ss.client_name = '"."Machmahon_Sumbawa"."' "; 
        $strSQL .= "AND "; 
        $strSQL .= "  ss.dept = '".$dept."' "; 
        $strSQL .= "AND "; 
        $strSQL .= "  ss.year_period = '".$yearPeriod."' "; 
        $strSQL .= "AND "; 
        $strSQL .= "  ss.month_period = '".$monthPeriod."' "; 
        // $strSQL .= $strFilter;
        $strSQL .= "ORDER BY "; 
        $strSQL .= "  ss.name "; 
        // echo $strSQL; exit(0);
        $query = $this->db->query($strSQL)->result_array(); 

        /* START DATA IS HERE */
        $spreadsheet->getActiveSheet()
            ->setCellValue('A1', 'SUMMARY INVOICE')
            ->setCellValue('A2', 'PT. MACHMAHON')
            ->setCellValue('A4', 'PERIOD : '.$monthPeriod.'-'.$yearPeriod.'    Dept : '.$dept);
        /* END DATA IS HERE */

        $spreadsheet->getActiveSheet()->mergeCells("A1:AC1");
        $spreadsheet->getActiveSheet()->getStyle("A1:M1")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->mergeCells("A2:AC2");
        $spreadsheet->getActiveSheet()->getStyle("A2:M2")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->mergeCells("A4:AC4");
        $spreadsheet->getActiveSheet()->getStyle("A4:M4")->applyFromArray($boldFont);
        $spreadsheet->getActiveSheet()->getStyle("A4:M4")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("A1:D1")->getFont()->setBold(true)->setSize(16);
        $spreadsheet->getActiveSheet()->getStyle("A2:D2")->getFont()->setBold(true)->setSize(13);
        $spreadsheet->getActiveSheet()->getStyle("A4:G4")->getFont()->setBold(true)->setSize(12);  

        $spreadsheet->getActiveSheet()->getStyle('A6:AC7')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('F2BE6B');
        $spreadsheet->getActiveSheet()->getStyle('A6:AC7')
                ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLUE); 

        $spreadsheet->getActiveSheet()->getStyle("A6:H6")->getFont()->setSize(12);
        $spreadsheet->getActiveSheet()->getStyle("A6:H6")->applyFromArray($outlineBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("A6:AC9")->applyFromArray($center);
        /* START TITLE NO */
        $spreadsheet->getActiveSheet()->getStyle("A6:AC8")->applyFromArray($allBorderStyle);
        // $objPHPExcel->getActiveSheet()->getStyle("A6:A7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()
            ->mergeCells("A6:A7")
            ->mergeCells("B6:B7")
            ->mergeCells("C6:C7")
            ->mergeCells("D6:D7")
            ->mergeCells("E6:E7")
            ->mergeCells("F6:F7");

        $spreadsheet->getActiveSheet()
            ->mergeCells("G6:H6")
            ->mergeCells("I6:J6")
            ->mergeCells("K6:L6")
            ->mergeCells("M6:N6")
            ->mergeCells("O6:P6")
            ->mergeCells("S6:U6");

        $spreadsheet->getActiveSheet()
            ->setCellValue('A6', 'No')
            ->setCellValue('A8', 'A')
            ->setCellValue('B6', 'Badge')
            ->setCellValue('B8', 'B')
            ->setCellValue('C6', 'Name')
            ->setCellValue('C8', 'C')
            ->setCellValue('D6', 'Position')
            ->setCellValue('D8', 'D')
            ->setCellValue('E6', 'Basic Salary')
            ->setCellValue('E8', 'E')
            ->setCellValue('F6', 'Gross Salary')
            ->setCellValue('F8', 'F')
            ->setCellValue('G6', 'OT 1.5')
            ->setCellValue('G7', 'Jam')
            ->setCellValue('H7', '(Rp)')
            ->setCellValue('G8', 'G')
            ->setCellValue('H8', 'H')
            ->setCellValue('I6', 'OT 2.0')
            ->setCellValue('I7', 'Jam')
            ->setCellValue('J7', '(Rp)')
            ->setCellValue('I8', 'I')
            ->setCellValue('J8', 'J')
            ->setCellValue('K6', 'OT 3.0')
            ->setCellValue('K7', 'Jam')
            ->setCellValue('L7', '(Rp)')
            ->setCellValue('K8', 'K')
            ->setCellValue('L8', 'L')
            ->setCellValue('M6', 'OT 4.0')
            ->setCellValue('M7', 'Jam')
            ->setCellValue('N7', '(Rp)')
            ->setCellValue('M8', 'M')
            ->setCellValue('N8', 'N')
            ->setCellValue('O6', 'Total Over Time')
            ->setCellValue('O7', 'Jam')
            ->setCellValue('P7', '(Rp)')
            ->setCellValue('O8', 'O')
            ->setCellValue('P8', 'P')
            ->setCellValue('Q6', 'THR')
            ->setCellValue('Q7', '(Rp)')
            ->setCellValue('Q8', 'Q')
            ->setCellValue('R6', 'Bonus KPI')
            ->setCellValue('R7', '(Rp)')
            ->setCellValue('R8', 'R')
            ->setCellValue('S6', 'BPJS')
            ->setCellValue('S7', 'Tenaga Kerja (Rp)')
            ->setCellValue('T7', 'Pensiun (Rp)')
            ->setCellValue('U7', 'Kesehatan (Rp)')
            ->setCellValue('S8', 'S')
            ->setCellValue('T8', 'T')
            ->setCellValue('U8', 'U')
            ->setCellValue('V6', 'Salary,OT,THR,Bonus,BPJS')
            ->setCellValue('V7', '(Rp)')
            ->setCellValue('V8', 'V')
            ->setCellValue('W6', 'Management Fee')
            ->setCellValue('W7', '(Rp)')
            ->setCellValue('W8', 'W')
            ->setCellValue('X6', 'Medical MCU')
            ->setCellValue('X7', '(Rp)')
            ->setCellValue('X8', 'X')
            ->setCellValue('Y6', 'Meals')
            ->setCellValue('Y7', '(Rp)')
            ->setCellValue('Y8', 'X')
            ->setCellValue('Z6', 'Akomodasi (Hotel,Tiket)')
            ->setCellValue('Z7', '(Rp)')
            ->setCellValue('Z8', 'Z')
            ->setCellValue('AA6', 'Total Allowance')
            ->setCellValue('AA7', '(Rp)')
            ->setCellValue('AA8', 'AA')
            ->setCellValue('AB6', 'Fiskal, Tax, Admin Cost (2%)')
            ->setCellValue('AB7', '(Rp)')
            ->setCellValue('AB8', 'AB')
            ->setCellValue('AC6', 'Total Invoice')
            ->setCellValue('AC7', '(Rp)')
            ->setCellValue('AC8', 'AC')
            ->setCellValue('S9', '(E*5.74%)')
            ->setCellValue('T9', '(E*2%)')
            ->setCellValue('V9', '(F+P+Q+R+S+T+U)')
            ->setCellValue('W9', '(V*10%)')
            ->setCellValue('AA9', '(S+T+U)')
            ->setCellValue('AB9', '(AA*2%)')
            ->setCellValue('AC9', '(V+W+AA+AB)');

        $spreadsheet->getActiveSheet()->getStyle("A9:AC9")->applyFromArray($outlineBorderStyle);

        $rowIdx = 9;
        $rowNo = 0;
        foreach ($query as $row) {
            $rowIdx++;
            $rowNo++;
            $spreadsheet->getActiveSheet()
                ->setCellValue('A'.($rowIdx), $rowNo)
                ->setCellValue('B'.($rowIdx), $row['nie'])
                ->setCellValue('C'.($rowIdx), $row['name'])
                ->setCellValue('D'.($rowIdx), $row['job_desc'])
                ->setCellValue('E'.($rowIdx), $row['basic_salary'])
                ->setCellValue('F'.($rowIdx), $row['bs_prorate'])
                ->setCellValue('G'.($rowIdx), $row['ot_count1'])
                ->setCellValue('H'.($rowIdx), $row['ot_1'])
                ->setCellValue('I'.($rowIdx), $row['ot_count2'])
                ->setCellValue('J'.($rowIdx), $row['ot_2'])
                ->setCellValue('K'.($rowIdx), $row['ot_count3'])
                ->setCellValue('L'.($rowIdx), $row['ot_3'])
                ->setCellValue('M'.($rowIdx), $row['ot_count4'])
                ->setCellValue('N'.($rowIdx), $row['ot_4'])
                ->setCellValue('O'.($rowIdx), '=SUM(G'.$rowIdx.',I'.$rowIdx.',K'.$rowIdx.',M'.$rowIdx.')')
                ->setCellValue('P'.($rowIdx), '=SUM(H'.$rowIdx.',J'.$rowIdx.',L'.$rowIdx.',N'.$rowIdx.')')
                ->setCellValue('Q'.($rowIdx), $row['thr'])
                ->setCellValue('R'.($rowIdx), $row['other_allowance1'])
                ->setCellValue('S'.($rowIdx), $row['bpjstk'])
                ->setCellValue('T'.($rowIdx), $row['jp'])
                ->setCellValue('U'.($rowIdx), $row['health_bpjs'])
                ->setCellValue('V'.($rowIdx), '=SUM(F'.$rowIdx.',P'.$rowIdx.',Q'.$rowIdx.',R'.$rowIdx.',S'.$rowIdx.',T'.$rowIdx.',U'.$rowIdx.')')
                ->setCellValue('W'.($rowIdx), '=V'.$rowIdx.'*10%')
                ->setCellValue('X'.($rowIdx), $row['general_3'])/*MCU*/
                ->setCellValue('Y'.($rowIdx), $row['general_6'])/*MEALS*/
                ->setCellValue('Z'.($rowIdx), $row['general_1'])/*Akomodasi*/
                ->setCellValue('AA'.($rowIdx), '=SUM(S'.$rowIdx.',T'.$rowIdx.',U'.$rowIdx.')')
                ->setCellValue('AB'.($rowIdx), '=AA'.$rowIdx.'*2%')
                ->setCellValue('AC'.($rowIdx), '=SUM(V'.$rowIdx.',W'.$rowIdx.',AA'.$rowIdx.',AB'.$rowIdx.')');                          

            /* SET ROW COLOR */
            if($rowIdx % 2 == 1)
            {
                $spreadsheet->getActiveSheet()->getStyle('A'.$rowIdx.':AC'.$rowIdx)
                    ->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB('EAEBAF');
            } 
            /* END UPDATE TAX */
        } /* end foreach ($query as $row) */

        // $spreadsheet->getActiveSheet()->getStyle("A6:AC8")->getAlignment()->setWrapText(true);

        // Nama Field Baris Pertama
        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'SUMMARY INVOICE');

        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, 'PT. MACHMAHON');
        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 4, 'PERIOD : '.$monthPeriod.'-'.$yearPeriod.'    Dept : '.$dept);

        /* SET HEADER BG COLOR*/   

        $spreadsheet->getActiveSheet()
            ->setCellValue('S'.($rowIdx+2), "TOTAL")
            ->setCellValue('AC'.($rowIdx+2), "=SUM(AC10:AC".$rowIdx.")");

        $spreadsheet->getActiveSheet()->getStyle('A'.($rowIdx+2).':AC'.($rowIdx+2))
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('F2BE6B');
                
        $spreadsheet->getActiveSheet()->getStyle('A'.($rowIdx+2).':AC'.($rowIdx+2))
                ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLUE); 

        // $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":AC".($rowIdx+2))->applyFromArray($outlineBorderStyle);
        // $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":AC".($rowIdx+2))
        //     ->getFill()
        //     ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        //     ->getStartColor()
        //     ->setRGB('F2BE6B');

        /* SET NUMBERS FORMAT*/
        $spreadsheet->getActiveSheet()->getStyle('E9:AC'.($rowIdx+2))->getNumberFormat()->setFormatCode('#,##0.00');       
        
        unset($allBorderStyle);
        unset($center);
        unset($right);
        unset($left);    


        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('Report Excel '.date('d-m-Y H'));

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        //Nama File
        // $str = $rowData['name'].$rowData['bio_rec_id'];
        $str = 'MCMSumInvoice';
        $fileName = preg_replace('/\s+/', '', $str);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.Xlsx"');
        // header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        /* BY COMPOSER */
        // $writer = new Xlsx($spreadsheet);
        /* OFFLINE/ BY COPY EXCEL FOLDER  */
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit(0);
    }	


    /* START PONTIL INVOICE LIST EXPORT */
    public function exportSummaryInvoicePontil($clientName, $yearPeriod, $monthPeriod)
    {
        //membuat objek
        // $objPHPExcel = new PHPExcel();

        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();  

        $spreadsheet->getProperties()->setCreator('Maurice - Web - Android')
            ->setLastModifiedBy('Maurice - Web - Android')
            ->setTitle('Office 2007 XLSX Test Document')
            ->setSubject('Office 2007 XLSX Test Document')
            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Test result file');

        if (file_exists('assets/images/report_logo.png')) {
            $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing->setName('Logo');
            $drawing->setDescription('Logo');
            $drawing->setPath('./assets/images/report_logo.png');
            $drawing->setCoordinates('A1');
            $drawing->setHeight(36);
            $drawing->setWorksheet($spreadsheet->getActiveSheet());
        }

        $strSQL = "";
        $strFilter = "";
        // if($payrollGroup != 'All')
        // {
        //     $strFilter = " AND ss.payroll_group = '".$payrollGroup."' ";    
        // }
        $strSQL  = " SELECT ";   
        $strSQL .= "   ms.bio_rec_id, ms.nie, ss.name, ss.position job_desc, ss.basic_salary,ss.bs_prorate, ";
        $strSQL .= "   (ss.bs_prorate - unpaid_total) current_salary, ";
        $strSQL .= "   TRUNCATE((ss.basic_salary/173),1) rate, ";
        $strSQL .= "   ss.normal_time, ss.ot_count1, ss.ot_count2, ss.ot_count3, ss.ot_count4, ";
        $strSQL .= "   ss.ot_1, ss.ot_2, ss.ot_3, ss.ot_4, ";
        $strSQL .= "   (ss.normal_time+ss.ot_count1+ss.ot_count2+ss.ot_count3+ss.ot_count4) worked_hours, ss.travel_bonus, ss.attendance_bonus,ss.flying_camp,";
        $strSQL .= "   CASE WHEN(ss.normal_time+ss.ot_count1+ss.ot_count2+ss.ot_count3+ss.ot_count4) <= 312 THEN ";
        $strSQL .= "      TRUNCATE((ss.normal_time+ss.ot_count1+ss.ot_count2+ss.ot_count3+ss.ot_count4) * TRUNCATE((ss.basic_salary/173),1) * (64/100),2) ";
        $strSQL .= "   ELSE TRUNCATE(312 * TRUNCATE((ss.basic_salary/173),1) * (64/100),2) END uplift, ";
        $strSQL .= "    TRUNCATE( (ss.normal_time)+(ss.ot_count1*1.5)+(ss.ot_count2*2)+(ss.ot_count3*3)+(ss.ot_count4*4),1 ) paid_hours, ";
        $strSQL .= "   (ot_1+ot_2+ot_3+ot_4) ot_total, ";
        $strSQL .= "   TRUNCATE((ot_1+ot_2+ot_3+ot_4)*(35/100),2) ot_bonus, ";
        $strSQL .= "   ss.travel_bonus, ss.shift_bonus, ss.incentive_bonus, ss.allowance_economy, ss.workday_adj, ss.production_bonus ";
        $strSQL .= "   FROM mst_salary ms, trn_salary_slip ss ";
        $strSQL .= "   WHERE ms.bio_rec_id = ss.bio_rec_id ";
        $strSQL .= "   AND ss.client_name = 'Pontil_Sumbawa'  ";       
        $strSQL .= "   AND ss.year_period = '".$yearPeriod."' ";       
        $strSQL .= "   AND ss.month_period = '".$monthPeriod."' ";       
        // $strSQL .= $strFilter;
        $strSQL .= "    ORDER BY ss.name  ;";    
        // echo $strSQL; exit(0);
        $query = $this->db->query($strSQL)->result_array();  

        
        $boldFont = [
            'font' => [
                'bold' => true
                // 'color' => ['argb' => '0000FF'],
            ],
        ];

        $totalStyle = [
            'font' => [
                'bold' => true,
                'color' => ['argb' => '0000FF'],
            ],
        ];

        $allBorderStyle = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $outlineBorderStyle = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $topBorderStyle = [
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $bottomBorderStyle = [
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        // foreach(range('B','Q') as $columnID)
        // {
        //     $spreadsheet->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        // }           

        // Nama Field Baris Pertama
        $spreadsheet->getActiveSheet()
                ->setCellValue('A1', 'SUMMARY INVOICE PT SANGATI SOERYA SEJAHTERA - PT PONTIL INDONESIA (PAPUA/TIMIKA)')
                ->setCellValue('A2', 'PERIOD : '.$monthPeriod.'-'.$yearPeriod)
                ->setCellValue('A3', 'DATE        : ')
                ->setCellValue('A4', 'INVOICE NO  : ')
                ->setCellValue('A5', 'CONTRACT NO : ');

        $spreadsheet->getActiveSheet()->mergeCells("A1:S1");

        $spreadsheet->getActiveSheet()->getStyle("A1:S1")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->mergeCells("A2:S2");
        $spreadsheet->getActiveSheet()->getStyle("A2:S2")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("A1:D1")->getFont()->setBold(true)->setSize(16);
        $spreadsheet->getActiveSheet()->getStyle("A2:D2")->getFont()->setBold(true)->setSize(13);
        $spreadsheet->getActiveSheet()->getStyle("A3:G5")->getFont()->setBold(true)->setSize(12); 

        /* SET HEADER BG COLOR*/
        $spreadsheet->getActiveSheet()->getStyle('A6:S8')
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B'); 

        // $spreadsheet->getActiveSheet()->getStyle('A6:AC7')
        //         ->getFill()
        //         ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        //         ->getStartColor()
        //         ->setRGB('F2BE6B');
        // $spreadsheet->getActiveSheet()->getStyle('A6:AC7')
        //         ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLUE); 

        /* START INVOICE TITLE */
        $spreadsheet->getActiveSheet()->getStyle("A6:H6")->getFont()->setSize(12);
        $spreadsheet->getActiveSheet()->getStyle("A6:H6")->applyFromArray($outlineBorderStyle);
        /* START TITLE NO */
        $titleRowIdx = 6;
        $titleColIdx = 0;

        $spreadsheet->getActiveSheet()
                ->setCellValue('A6', 'NO')
                ->setCellValue('B6', 'NAME')
                ->setCellValue('C6', 'POSITION')
                ->setCellValue('D6', 'SALARY')
                ->setCellValue('F6', 'NT')
                ->setCellValue('G6', 'X 1.5')
                ->setCellValue('H6', 'X 2.0')
                ->setCellValue('I6', 'X 3.0')
                ->setCellValue('J6', 'X 4.0')
                ->setCellValue('K6', 'WORK HOURS')
                ->setCellValue('L6', 'SALARY THIS MONTH')
                ->setCellValue('M6', 'OVER TIME')
                ->setCellValue('N6', 'ALLOWANCE')
                ->setCellValue('Q6', 'GROSS SALARY')
                ->setCellValue('R6', 'CONTRACTOR FEE')
                ->setCellValue('S6', 'TOTAL INVOICE');

        $spreadsheet->getActiveSheet()
                ->setCellValue('D7', 'BASE MONTH')
                ->setCellValue('E7', 'BASE HOUR')
                ->setCellValue('G7', 'HOURS')
                ->setCellValue('H7', 'HOURS')
                ->setCellValue('I7', 'HOURS')
                ->setCellValue('J7', 'HOURS')
                ->setCellValue('N7', 'TRAVEL')
                ->setCellValue('O7', 'ATTENDANCE')
                ->setCellValue('P7', 'OUT CAMP')
                ;

        $spreadsheet->getActiveSheet()
                ->setCellValue('A8', 'A')
                ->setCellValue('B8', 'B')
                ->setCellValue('C8', 'C')
                ->setCellValue('D8', 'D')
                ->setCellValue('E8', 'E')
                ->setCellValue('F8', 'F')
                ->setCellValue('G8', 'G')
                ->setCellValue('H8', 'H')
                ->setCellValue('I8', 'I')
                ->setCellValue('J8', 'J')
                ->setCellValue('K8', 'K')
                ->setCellValue('L8', 'L')
                ->setCellValue('M8', 'M')
                ->setCellValue('N8', 'N')
                ->setCellValue('O8', 'O')
                ->setCellValue('P8', 'P')
                ->setCellValue('Q8', 'Q')
                ->setCellValue('R8', 'R')
                ->setCellValue('S8', 'S');

        $spreadsheet->getActiveSheet()
            ->mergeCells("A6:A7")
            ->mergeCells("B6:B7")
            ->mergeCells("C6:C7")
            ->mergeCells("F6:F7")
            ->mergeCells("K6:K7")
            ->mergeCells("L6:L7")
            ->mergeCells("M6:M7")           
            ->mergeCells("Q6:Q7")
            ->mergeCells("R6:R7")
            ->mergeCells("S6:S7")
        ;

        $spreadsheet->getActiveSheet()
            ->mergeCells("D6:E6")
            ->mergeCells("N6:P6");

        $spreadsheet->getActiveSheet()->getStyle("A6:S8")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("A6:S8")->applyFromArray($center);
        // /* END TITLE NO */

        $rowIdx = 10;
        $startIdx = $rowIdx; 
        $rowNo = 0;
        foreach ($query as $row) {
            $rowIdx++;
            $rowNo++;

            $gross = $row['current_salary'] + $row['ot_total'] + $row['travel_bonus'] + $row['attendance_bonus']+$row['flying_camp'];
            $chargeTotal = $gross + $row['uplift'];
            $spreadsheet->getActiveSheet()
                ->setCellValue('A'.($rowIdx), $rowNo)
                ->setCellValue('B'.($rowIdx), $row['name'])
                ->setCellValue('C'.($rowIdx), $row['job_desc'])
                ->setCellValue('D'.($rowIdx), $row['basic_salary'])
                ->setCellValue('E'.($rowIdx), $row['rate'])
                ->setCellValue('F'.($rowIdx), $row['normal_time'])
                ->setCellValue('G'.($rowIdx), $row['ot_count1'])
                ->setCellValue('H'.($rowIdx), $row['ot_count2'])
                ->setCellValue('I'.($rowIdx), $row['ot_count3'])
                ->setCellValue('J'.($rowIdx), $row['ot_count4'])
                ->setCellValue('K'.($rowIdx), $row['worked_hours'])
                ->setCellValue('L'.($rowIdx), $row['current_salary'])
                ->setCellValue('M'.($rowIdx), $row['ot_total'])
                ->setCellValue('N'.($rowIdx), $row['travel_bonus'])
                ->setCellValue('O'.($rowIdx), $row['attendance_bonus'])
                ->setCellValue('P'.($rowIdx), $row['flying_camp'])
                ->setCellValue('Q'.($rowIdx), $gross)
                ->setCellValue('R'.($rowIdx), '=IF(K'.$rowIdx.'<=312,((E'.$rowIdx.'*K'.$rowIdx.')*64%),((E'.$rowIdx.'*312)*64%))')
                ->setCellValue('S'.($rowIdx), $chargeTotal);

            /* SET ROW COLOR */
            if($rowIdx % 2 == 1)
            {
                $spreadsheet->getActiveSheet()->getStyle('A'.$rowIdx.':S'.$rowIdx)
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('EAEBAF');             
            } 
            /* END UPDATE TAX */
        } /* end foreach ($query as $row) */

        $spreadsheet->getActiveSheet()
                ->setCellValue('K'.($rowIdx+2), 'TOTAL')
                ->setCellValue('S'.($rowIdx+2), '=SUM(S'.$startIdx.':S'.$rowIdx.')');

        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":S".($rowIdx+2))->getFont()->setBold(true)->setSize(12); 
        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":S".($rowIdx+2))->applyFromArray($outlineBorderStyle);

        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":S".($rowIdx+2))
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B');

        /* SET NUMBERS FORMAT*/
        $spreadsheet->getActiveSheet()->getStyle('D9:S'.($rowIdx+2))->getNumberFormat()->setFormatCode('#,##0.00');
        $spreadsheet->getActiveSheet()->getStyle('A1')->getAlignment()->setWrapText(true);     
        
        unset($allBorderStyle);
        unset($center);
        unset($right);
        unset($left);
        
        $spreadsheet->setActiveSheetIndex(0);

        $str = 'PTLSumInvoice';
        $fileName = preg_replace('/\s+/', '', $str);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.Xlsx"');
        // header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        /* BY COMPOSER */
        // $writer = new Xlsx($spreadsheet);
        /* OFFLINE/ BY COPY EXCEL FOLDER  */
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit(0);   
    }
    /* END PONTIL INVOICE LIST EXPORT */

    public function getDataList($pt, $year, $month)
    {

        $this->db->select('*');
        $this->db->from($this->db->database.'.trn_salary_slip');
        $this->db->where('client_name', $pt);
        if( ($pt == 'Machmahon_Sumbawa' || $pt == "Pontil_Sumbawa") )
        {
            if(isset($dept)){
                $dept = $_POST['dept'];
                $this->db->where('dept', $this->security->xss_clean($dept));
            }
        }

        $this->db->where('year_period', $this->security->xss_clean($year));
        $this->db->where('month_period', $this->security->xss_clean($month));
        $this->db->order_by('name',"asc");
        $query = $this->db->get()->result_array();
        // echo $this->db->last_query(); exit(0);
        /*return json_encode($query);*/
        $myData = array();
        foreach ($query as $key => $row) {
            $myData[] = array(
                $row['salary_slip_id'],         
                $pt,         
                $row['name'],         
                // $row['production_bonus'],         
                // $row['workday_adj'],         
                // $row['adjust_in'],         
                // $row['adjust_out'],         
                $row['dept'],         
                $row['position']         
                // $row['attendance_bonus'],         
                // $row['other_allowance1'],         
                // $row['other_allowance2'],         
                // $row['cc_payment'],         
                // $row['thr'],         
                // $row['debt_burden'],
                // $row['debt_explanation']
            );            
        }  
        echo json_encode($myData);   
    }

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    // Load library phpspreadsheet
    require('./vendor/autoload.php');
    use PhpOffice\PhpSpreadsheet\Helper\Sample;
    use PhpOffice\PhpSpreadsheet\IOFactory;
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    // End load library phpspreadsheet

class Payroll extends CI_Controller {

    public function __construct(){
        parent::__construct();
        // $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
        // $this->load->model('M_MstProcessClosing');
        // $this->load->model('masters/M_salary_slip');
        $this->load->model('masters/M_roster_hist');
        $this->load->model('M_salary_slip'); 
        $this->load->model('masters/M_payroll_config'); 
        // $this->load->model('masters/M_salary');
        $this->load->model('masters/M_mst_salary', 'M_salary'); 
    }

    public function toExcel($slipId, $clientName, $calendarStart, $payrollGroup, $isHealthBPJS, $isJHT, $isJP, $isJKKM)
    {             
        /* CUSTOMIZE GOVERNMENT REGULATION */
        if ($clientName == "AMNT_Sumbawa") {            
            $this->amntSmbExcel($slipId, $calendarStart, $payrollGroup, $isHealthBPJS, $isJHT, $isJP, $isJKKM);
        }
        else if ($clientName == 'Machmahon_Sumbawa') {            
            $this->machMahonSmbExcel($slipId, $calendarStart, $payrollGroup, $isHealthBPJS, $isJHT, $isJP, $isJKKM);
        }
        else if ($clientName == 'Pontil_Sumbawa') {            
            $this->pontilSmbExcel($slipId, $calendarStart, $payrollGroup, $isHealthBPJS, $isJHT, $isJP, $isJKKM);
        }   
        elseif ($clientName == "Trakindo_Sumbawa") {
            $this->trakindoSmbExcel($slipId, $calendarStart, $payrollGroup, $isHealthBPJS, $isJHT, $isJP, $isJKKM);
        }        
    }

  
    // public function my_report()
    public function machMahonSmbExcel($slipId, $calendarStart, $payrollGroup, $isHealthBPJS, $isJHT, $isJP, $isJKKM)
    {
        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();

        $spreadsheet->getProperties()->setCreator('Maurice - Web - Android')
            ->setLastModifiedBy('Maurice - Web - Android')
            ->setTitle('Office 2007 XLSX Test Document')
            ->setSubject('Office 2007 XLSX Test Document')
            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Test result file');

        $boldFont = [
            'font' => [
                'bold' => true
                // 'color' => ['argb' => '0000FF'],
            ],
        ];

        $totalStyle = [
            'font' => [
                'bold' => true,
                'color' => ['argb' => '0000FF'],
            ],
        ];

        $allBorderStyle = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $outlineBorderStyle = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $topBorderStyle = [
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $bottomBorderStyle = [
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        // $center = [
        //     'alignment' => [
        //         'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
        //         'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
        //     ],
        // ];

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        // http://192.168.10.17/os-hris-2019/sumbawa/Payroll/toexcel/2019031175/Machmahon_Sumbawa/1/undefined/1/1/1/1
        // $slipId = "2019040001";
        // $calendarStart = "1";
        // $payrollGroup = "";
        // $isHealthBPJS = "1";
        // $isJHT = "1";
        // $isJP = "1";
        // $isJKKM = "1";
        
        /* CALENDAR SETUP */
        $dateCal = $calendarStart;

        /* START VALIDATE & UPDATE GOVERNMENT REGULATION */     
        $this->M_payroll_config->loadByClient('Machmahon_Sumbawa');
        $myConfigId = $this->M_payroll_config->getPayrollConfigId();
        // $this->M_payroll_config->loadById($myConfigId); 
        $healthBpjsConfig = $this->M_payroll_config->getHealthBpjs();
        $maxHealthBpjsConfig = $this->M_payroll_config->getMaxHealthBpjs();
        $jkkJkmConfig = $this->M_payroll_config->getJkkJkm();
        $jhtConfig = $this->M_payroll_config->getJht();
        $empJhtConfig = $this->M_payroll_config->getEmpJht();             
        $empHealthBpjsConfig = $this->M_payroll_config->getEmpHealthBpjs();    
        $jpConfig = $this->M_payroll_config->getJp();
        $empJpConfig = $this->M_payroll_config->getEmpJp();
        // echo $empHealthBpjsConfig; exit(0);
        $npwpCharge = $this->M_payroll_config->getNpwpCharge();
        $this->M_salary_slip->getObjectById($slipId);
        $basicSalary = $this->M_salary_slip->getBasicSalary();
        // echo $healthBpjsConfig; exit(0);
        /* START HEALTH BPJS */
        $healthBpjs = ($healthBpjsConfig/100) * $basicSalary; /* Nilai mst_payroll_config.health_bpjs(%) x Basic Salary (Max 8 Juta) */
        if($healthBpjs > $maxHealthBpjsConfig){
            $healthBpjs = $maxHealthBpjsConfig; 
        }
        /* END HEALTH BPJS */
        /* START JKK-JKM */
        $jkkJkm = ($jkkJkmConfig/100) * $basicSalary; /* Nilai mst_payroll_config.jkk_jkm(%) x Basic Salary */ 
        /* END JKK-JKM */                
        /* START JHT */
        $jht = ($jhtConfig/100) * $basicSalary;  /*Nilai mst_payroll_config.jht(%) x Basic Salary  */       
        /* END JHT */                
        /* START JHT */
        $empJht = ($empJhtConfig/100) * $basicSalary; /* Nilai mst_payroll_config.emp_jht(%) x Basic Salary */      
        /* END JHT */
        /* START JP COMPANY */
        // $jp = ($jpConfig/100) * $basicSalary; /* Nilai mst_payroll_config.jp(%) x Basic Salary */        
        /* END JP COMPANY */
        /* START JP EMPLOYEE */
        $empJp = ($empJpConfig/100) * $basicSalary; /* Nilai mst_payroll_config.emp_jp(%) x Basic Salary */        
        /* END JP EMPLOYEE */               
        
        /* START BPJS KARYAWAN */
        $maxEmpHealthBpjs = $this->M_payroll_config->getMaxEmpBpjs(); /* Nilai Max Gaji Untuk Iuran BPJS Karyawan  */ 
        $empHealthBpjs = ($empHealthBpjsConfig/100) * $basicSalary; /* Nilai mst_payroll_config.emp_health_bpjs(%) x Basic Salary */
        if($empHealthBpjs > $maxEmpHealthBpjs)
        {
            $empHealthBpjs = $maxEmpHealthBpjs; 
        }     
        /* END BPJS KARYAWAN */        

        if($isHealthBPJS == '0')
        {
            $healthBpjs = 0;
            $empHealthBpjs = 0;
        }
        if($isJHT == '0')
        {
            $jht = 0;
            $empJht = 0;
        }
        if($isJP == '0')
        {
            $empJp = 0;
        }
        if($isJKKM == '0')
        {
            $jkkJkm = 0;
        }

        $this->M_salary_slip->setHealthBpjs($healthBpjs);
        $this->M_salary_slip->setEmpHealthBpjs($empHealthBpjs);
        $this->M_salary_slip->setJht($jht);
        $this->M_salary_slip->setEmpJht($empJht);
        $this->M_salary_slip->setEmpJp($empJp);
        $this->M_salary_slip->setJkkJkm($jkkJkm);
        $this->M_salary_slip->update($slipId);
        /* END VALIDATE & UPDATE GOVERNMENT REGULATION */


        $this->db->select('ss.*, mb.bio_rec_id biodata_id, ss.basic_salary salary_basic,ss.bs_prorate, mb.npwp_no, tro.*, mr.*');
        $this->db->from('trn_salary_slip ss, mst_bio_rec mb');
        $this->db->join('mst_roster mr','mb.bio_rec_id = mr.bio_rec_id');
        $this->db->join('trn_overtime tro','mb.bio_rec_id = tro.bio_rec_id');
        $this->db->where('ss.bio_rec_id = mb.bio_rec_id AND ss.client_name = "Machmahon_Sumbawa"');
        $this->db->where('salary_slip_id', $slipId);
        $this->db->where('ss.year_period = tro.year_period AND ss.month_period = tro.month_period');        
        $this->db->where('ss.year_period = mr.year_process AND ss.month_period = mr.month_process');        
        $rowData = $query = $this->db->get()->row_array();  // Produces: SELECT * FROM trn_salary_slip (one row only) 
        $ptName = $rowData['client_name'];  
        $monthPeriod = $rowData['month_period'];
        $yearPeriod = $rowData['year_period'];     
        $biodataId = $rowData['biodata_id'];     
        $nie = $rowData['nie'];     
        $dept = $rowData['dept'];     
        $rosterFormat = $rowData['roster_format'];     
        $rosterBase = $rowData['roster_base']; 
        $payrollLevel = $rowData['payroll_level']; 
        $npwpNo = $rowData['npwp_no']; 
        $tMonth = (int) $monthPeriod;
        $tYear = (int) $yearPeriod;

        $wdAttendCount = 0;
        $wdAttendCount += $rowData['attend_total']; // 5
                        $wdAttendCount += $rowData['in_ph_total']; //2
                        $wdAttendCount -= $rowData['attend_in_off'];//2
                        $wdAttendCount += $rowData['sick_total']; //0
                        $wdAttendCount += $rowData['emergency_total']; //1
                        $wdAttendCount += $rowData['paid_permit_total']; //0
                        $wdAttendCount += $rowData['paid_vacation_total']; //0
                        $wdAttendCount += $rowData['alpa_total']; 
                        $wdAttendCount += $rowData['off_total'];

        $dayCountInMonth = cal_days_in_month(CAL_GREGORIAN, $tMonth, $tYear);  
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(6);             
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(9);             
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(7);             
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(8);         
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(5);             
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(5);             
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(5);             
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(5);             
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(5);             
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(3);             
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(8);

        $tDate1 = $calendarStart.'-'.$monthPeriod.'-'.$yearPeriod;
        $tStr = $yearPeriod.'-'.$monthPeriod.'-'.($calendarStart-1);
        $tDate2 = strtotime($tStr);
        $tDate3 = date("d-m-Y", strtotime("+1 month", $tDate2));
        // Add some data
        $spreadsheet->getActiveSheet()
            ->setCellValue('A1', 'PT. SANGATI SOERYA SEJAHTERA')
            ->setCellValue('A2', 'Slip Gaji Karyawan')
            ->setCellValue('A4', 'PT : '.$ptName)
            ->setCellValue('A5', 'Periode : '.$tDate1.' to '.$tDate3);

        $spreadsheet->getActiveSheet()->getStyle("A1:D1")->getFont()->setBold(true)->setSize(16);
        $spreadsheet->getActiveSheet()->getStyle("A2:D2")->getFont()->setBold(true)->setSize(14)->setUnderline(true);
        $spreadsheet->getActiveSheet()->getStyle("A4:G4")->getFont()->setBold(true)->setSize(12);

        $spreadsheet->getActiveSheet()->mergeCells("A6:A7");
        $spreadsheet->getActiveSheet()->mergeCells("B6:B7");
        $spreadsheet->getActiveSheet()->mergeCells("C6:C7");
        $spreadsheet->getActiveSheet()->mergeCells("D6:D7");
        $spreadsheet->getActiveSheet()->mergeCells("E6:E7");
        $spreadsheet->getActiveSheet()->mergeCells("F6:I6");
        
        $spreadsheet->getActiveSheet()->getStyle("A6:E7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("F6:I7")->applyFromArray($allBorderStyle);

        // $spreadsheet->getActiveSheet()
        //     ->getStyle('A10:H10')
        //     ->getBorders()
        //     ->getBottom()
        //     ->setBorderStyle(Border::BORDER_THIN)
        //     ->setColor(new Color('FFFF0000'));
        
        $spreadsheet->getActiveSheet()->getStyle("A6:I7")->applyFromArray($center);
        // $spreadsheet->getActiveSheet()->getStyle('A6:I7')->getAlignment()->setHorizontal('center');
        // $spreadsheet->getActiveSheet()->getStyle('A6:I7')->getAlignment()->setVertical('center');

        $spreadsheet->getActiveSheet()->getStyle("A6:I7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()
            ->setCellValue('A6', 'DATE')
            ->setCellValue('B6', 'ROSTER DAY')
            ->setCellValue('C6', 'SHIFT DAY')
            ->setCellValue('D6', 'HOURS TOTAL')
            ->setCellValue('E6', 'NT')
            ->setCellValue('F6', 'OVERTIME')
            ->setCellValue('F7', '1')
            ->setCellValue('G7', '2')
            ->setCellValue('H7', '3')
            ->setCellValue('I7', '4');

        $spreadsheet->getActiveSheet()->getStyle("A6:I7")->applyFromArray($boldFont);
        // $spreadsheet->getActiveSheet()->getStyle("A6:I7")->getFont()->setBold();
        // $spreadsheet->getDefaultStyle()->getFont()->setBold();  
        // $spreadsheet->getActiveSheet()->getStyle("A6:I7")->getFont()->setBold();  
        $rosterFormat = $rowData['roster_format'];
        $tmpRosterLength = strlen($rosterFormat);
        $strNumRoster = '';
        $tmp = '';
        $dayNo = '';
        $groupCount = 0;
        $strNum = '';
        $dayTotal = 0;
        /* START GET DAYS TOTAL BY ROSTER */
        for ($i=0; $i < $tmpRosterLength; $i++) {
            $strNum = substr($rosterFormat,$i,1);
            /* START MAKE SURE DATA IS NUMBER */ 
            if(is_numeric($strNum))
            {
                $strNumRoster .= $strNum;
                $dayTotal += $strNum;
            }
            /* END MAKE SURE DATA IS NUMBER */ 
        }
        $numChar = '';
        $rdData = '';
        $dataIdx = 8;
        $tIdx = 0;  

        $ot01Count = 0;
        $ot02Count = 0;
        $ot03Count = 0;
        $ot04Count = 0;

        $normalTotal = 0;
        $allTimeTotal = 0;
        /* START SUMMARY ROSTER */
        $rowIdx = 8;
        /* START CALENDAR VALUE */
        // $dayCount = cal_days_in_month(CAL_GREGORIAN, $monthPeriod, $yearPeriod);   

        /* Get Last Day Number */
        $strDate = $yearPeriod.'-'.$monthPeriod.'-01';
        $tQuery  = 'SELECT * FROM mst_roster_hist ';
        $tQuery .= 'WHERE bio_rec_id = "'.$biodataId.'" ';
        $tQuery .= 'AND DATE_ADD("'.$strDate.'", INTERVAL -1 MONTH) = DATE_FORMAT(CONCAT(year_period,"-",month_period,"-01"), "%Y-%m-%d") ';
        $tQuery .= 'LIMIT 1 ';
        $tData = $this->db->query($tQuery)->row_array();
        $tLastDay = 0;
        if(isset($tData)){
            $tLastDay = $tData['last_day']; 
        }

        $rosterIdx = $tLastDay;
        for ($i=1; $i <= $dayCountInMonth; $i++) { 
            /* START NUMBER TITLE */
            $rowIdx++;

            $ot01Column = '';
            $ot02Column = '';
            $ot03Column = '';
            $ot04Column = '';

            if($i < 10){
                $ot01Column = 'ot1_d0'.$i;
                $ot02Column = 'ot2_d0'.$i;
                $ot03Column = 'ot3_d0'.$i;
                $ot04Column = 'ot4_d0'.$i;
                $timePerday = 'd0'.$i;
            }else{
                $ot01Column = 'ot1_d'.$i;
                $ot02Column = 'ot2_d'.$i;
                $ot03Column = 'ot3_d'.$i;
                $ot04Column = 'ot4_d'.$i;
                $timePerday = 'd'.$i;
            }
            /* OVER TIME */
            $ot01Val = $rowData[$ot01Column];
            $ot02Val = $rowData[$ot02Column];
            $ot03Val = $rowData[$ot03Column];
            $ot04Val = $rowData[$ot04Column];
            
            $timeTotal = 0;
            $strTimePerday = $rowData[$timePerday];
            $tTimePerday = 0;            

            /* START PUBLIC HOLIDAY  */
            $phCodeTmp = substr($strTimePerday,0,2);
            $phHoursTmp = substr($strTimePerday,2,strlen($strTimePerday)-2);
            if( (strtoupper($phCodeTmp) == "RO" || strtoupper($phCodeTmp) == "PH" || strtoupper($phCodeTmp) == "SP" || strtoupper($phCodeTmp) == "NS" || strtoupper($phCodeTmp) == "SD") && (is_numeric($phHoursTmp)) && ($phHoursTmp > 0) )
            {
                $tTimePerday = $phHoursTmp;
            }
            else
            {
                $tTimePerday = $rowData[$timePerday];
            }

            if(is_numeric($tTimePerday))
            {
                $timeTotal = $tTimePerday;
                if($tTimePerday > 1) {
                    $attendStatus = 1;
                }
            }

            $normalTime = $timeTotal - $ot01Val - $ot02Val - $ot03Val - $ot04Val; 
            $normalTotal += $normalTime;

            $allTimeTotal += $timeTotal; 

            $ot01Count += $ot01Val;
            $ot02Count += $ot02Val;
            $ot03Count += $ot03Val;
            $ot04Count += $ot04Val;
                         
            if($dateCal > $dayCountInMonth){
               $dateCal = 1; 
            }

            $spreadsheet->getActiveSheet()->getStyle('A'.$rowIdx.':A'.$rowIdx)->applyFromArray($allBorderStyle);
            $spreadsheet->getActiveSheet()->getStyle('A'.$rowIdx.':A'.$rowIdx)->applyFromArray($right);
            $spreadsheet->getActiveSheet()->setCellValue('A'.$rowIdx, $dateCal);  

            $dateCal++;
            /* END NUMBER TITLE */

                    
            $attendStatus = 0;        
            $rosterIdx++;
            if($rosterIdx > 7){
               $rosterIdx = 1;                
            }
            $rdData = $rosterIdx;
            if( strtoupper($strTimePerday) == 'STR' || strtoupper($strTimePerday) == 'END' )
            {
                $rdData = $strTimePerday; 
                $rosterIdx = 0;
            }
            if( strtoupper($strTimePerday) == 'ID' || strtoupper($strTimePerday) == 'A' || strtoupper($strTimePerday) == 'S' || strtoupper($strTimePerday) == 'I') {
                $rdData = $strTimePerday; 
            }
            
            if( strtoupper($phCodeTmp) == 'RO' ) {
                $rdData = $phCodeTmp; 
                $rosterIdx = 0;
            }

            if( strtoupper($phCodeTmp) == 'SP' ) {
                $rdData = $phCodeTmp; 
                $rosterIdx = 0;
            }

            if( strtoupper($phCodeTmp) == 'PH' || strtoupper($phCodeTmp) == 'NS') {
                $rdData = $phCodeTmp; 
            }
            
            $hourVal = right($strTimePerday,1); 
            if( is_numeric($hourVal) ) {
                $attendStatus = 1;        
            }

            $arrTmpCode = array('PH','SB','PV','PP','RO','NS','SD','ID');
            $arrOtTmp = array('A','U','I','S','V', 'E', 'STR', 'END');
            if( (!in_array( strtoupper($phCodeTmp), $arrTmpCode)) && (!in_array( strtoupper($strTimePerday), $arrOtTmp)) && !is_numeric($strTimePerday))
            {
                $attendStatus = 0;      
            }
            
            $spreadsheet->getActiveSheet()
                ->setCellValue('B'.$rowIdx, $rdData)
                ->setCellValue('C'.$rowIdx, $attendStatus)
                ->setCellValue('D'.$rowIdx, $timeTotal)
                ->setCellValue('E'.$rowIdx, $normalTime)
                ->setCellValue('F'.$rowIdx, $ot01Val)
                ->setCellValue('G'.$rowIdx, $ot02Val)
                ->setCellValue('H'.$rowIdx, $ot03Val)
                ->setCellValue('I'.$rowIdx, $ot04Val);

            $spreadsheet->getActiveSheet()->getStyle("B".$rowIdx.":I".$rowIdx)->applyFromArray($allBorderStyle);
            $spreadsheet->getActiveSheet()->getStyle("B".$rowIdx.":I".$rowIdx)->applyFromArray($center);  
            // $spreadsheet->getActiveSheet()->getStyle("B".$rowIdx.":I".$rowIdx)->getAlignment()->setHorizontal('center');
            // $spreadsheet->getActiveSheet()->getStyle("B".$rowIdx.":I".$rowIdx)->getAlignment()->setVertical('center');

        } // for ($i=1; $i <= 31 ; $i++)


        $this->M_roster_hist->resetValues();
        $this->M_roster_hist->delete($slipId);    
        $this->M_roster_hist->setRhId($slipId);    
        $this->M_roster_hist->setBioRecId($biodataId);    
        $this->M_roster_hist->setClientName($ptName);    
        $this->M_roster_hist->setYearPeriod($yearPeriod);    
        $this->M_roster_hist->setMonthPeriod($monthPeriod);    
        $this->M_roster_hist->setLastDay($rosterIdx); 
        $this->M_roster_hist->insert(); 

        $totalTitle = 40;
        $spreadsheet->getActiveSheet()
                ->setCellValue('A'.$totalTitle, 'TOTAL')
                ->setCellValue('C'.$totalTitle, $wdAttendCount)
                ->setCellValue('D'.$totalTitle, $allTimeTotal)
                ->setCellValue('E'.$totalTitle, $normalTotal)
                ->setCellValue('F'.$totalTitle, $ot01Count)
                ->setCellValue('G'.$totalTitle, $ot02Count)
                ->setCellValue('H'.$totalTitle, $ot03Count)
                ->setCellValue('I'.$totalTitle, $ot04Count);

        $spreadsheet->getActiveSheet()->getStyle("A".$totalTitle.":A".$totalTitle+1)->applyFromArray($center);
        // $spreadsheet->getActiveSheet()->getStyle("A".$totalTitle.":A".$totalTitle+1)->getAlignment()->setHorizontal('center');
        // $spreadsheet->getActiveSheet()->getStyle("A".$totalTitle.":A".$totalTitle+1)->getAlignment()->setVertical('center');

        $spreadsheet->getActiveSheet()->getStyle("C".$totalTitle.":I".$totalTitle)->applyFromArray($right);
        $spreadsheet->getActiveSheet()->getStyle("A".$totalTitle.":I".$totalTitle)->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("A".$totalTitle.":I".$totalTitle)->getFont()->setBold(true)->setSize(13);
        
        /* END TITLE */       

        /* START TOTAL OVER TIME  */
        $totalTitle++; 
        /* Title */
        $spreadsheet->getActiveSheet()->getStyle("A".$totalTitle.":A".$totalTitle+1)->applyFromArray($center);
        // $spreadsheet->getActiveSheet()->getStyle("A".$totalTitle.":A".$totalTitle+1)->getAlignment()->setHorizontal('center');
        // $spreadsheet->getActiveSheet()->getStyle("A".$totalTitle.":A".$totalTitle+1)->getAlignment()->setVertical('center');

        $spreadsheet->getActiveSheet()->getStyle("A".$totalTitle.":A".$totalTitle)->getFont()->setBold(true)->setSize(13);
        $spreadsheet->getActiveSheet()->setCellValue('A'.$totalTitle, 'TOTAL OT');
        /* Data */
       
        $spreadsheet->getActiveSheet()->getStyle("C41:I41")->applyFromArray($right);
        $spreadsheet->getActiveSheet()->getStyle("A41:I41")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->mergeCells("C41:I41");
        $spreadsheet->getActiveSheet()->getStyle("C41:I41")->getFont()->setBold(true)->setSize(13);
        $spreadsheet->getActiveSheet()->setCellValue('C41', '=SUM(F40:I40)');

        $spreadsheet->getActiveSheet()->getStyle("A46:I63")->applyFromArray($outlineBorderStyle);
        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(1, 47, "Dibayarkan Oleh");
        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(1, 51, "Payroll/ Accounting");
        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(1, 55, "Diterima Oleh Karyawan");
        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(1, 59, $rowData['name']); 

        $spreadsheet->getActiveSheet()
                ->setCellValue('B47', 'Dibayarkan Oleh')    
                ->setCellValue('B51', 'Payroll/ Accounting')    
                ->setCellValue('B55', 'Diterima Oleh Karyawan')    
                ->setCellValue('B59', $rowData['name']);    

        $basicSalary = $rowData['salary_basic'];
        $bsProrate = $rowData['bs_prorate'];
        if($bsProrate > $basicSalary)
        {
            $bsProrate = $basicSalary;   
        }

        $this->M_payroll_config->loadByClient($ptName);
        $salaryDividerConfig = $this->M_payroll_config->getSalaryDivider();
        $salaryHourly = $basicSalary / $salaryDividerConfig;
        // $salaryHourly = number_format($salaryHourly,2);
        $otMultiplierConfig01 = $this->M_payroll_config->getOt01Multiplier();
        // $otMultiplierConfig01 = number_format($otMultiplierConfig01,1);
        $otMultiplierConfig02 = $this->M_payroll_config->getOt02Multiplier();
        // $otMultiplierConfig02 = number_format($otMultiplierConfig02,1);
        $otMultiplierConfig03 = $this->M_payroll_config->getOt03Multiplier();
        // $otMultiplierConfig03 = number_format($otMultiplierConfig03,1);
        $otMultiplierConfig04 = $this->M_payroll_config->getOt04Multiplier(); 
        // $otMultiplierConfig04 = number_format($otMultiplierConfig04,1);

        $spreadsheet->getActiveSheet()->getStyle("K4:L4")->applyFromArray($left);
        $spreadsheet->getActiveSheet()->getStyle("K4:Q43")->getFont()->setSize(13);

        $spreadsheet->getActiveSheet()
            ->setCellValue('K4', 'NAMA')
            ->setCellValue('M4', $rowData['name'])
            ->setCellValue('N4', 'Id')
            ->setCellValue('O4', $nie)
            ->setCellValue('K5', 'JABATAN')
            ->setCellValue('M5', $rowData['position'])
            ->setCellValue('N5', 'DEPT ')
            ->setCellValue('O5', $dept)
            ->setCellValue('K6', 'STATUS')
            ->setCellValue('M6', $rowData['marital_status'])
            ->setCellValue('K7', 'GAJI POKOK')
            ->setCellValue('M7', $basicSalary)
            ->setCellValue('K8', 'RATE/HOUR')
            ->setCellValue('M8', $salaryHourly)
            ->setCellValue('K9', 'NPWP')
            ->setCellValue('M9', $rowData['npwp_no'])
            ->setCellValue('K11', 'UPAH')
            ->setCellValue('O11', $bsProrate)
            ->setCellValue('K12', 'OT 1')
            ->setCellValue('K13', 'OT 2')
            ->setCellValue('K14', 'OT 3')
            ->setCellValue('K15', 'OT 4')
            ;  

        $spreadsheet->getActiveSheet()->getStyle('M7:M8')->applyFromArray($totalStyle);
        $otTotal1 = $rowData['ot_1']; 
        $otTotal2 = $rowData['ot_2']; 
        $otTotal3 = $rowData['ot_3']; 
        $otTotal4 = $rowData['ot_4']; 

        $allOt = $otTotal1+$otTotal2+$otTotal3+$otTotal4; 

        $strOT1 = number_format($rowData['ot_count1'],1)." x ".number_format($otMultiplierConfig01,1)." x ".number_format($salaryHourly,2)." = Rp.";
        $strOT2 = number_format($rowData['ot_count2'],1)." x ".number_format($otMultiplierConfig02,1)." x ".number_format($salaryHourly,2)." = Rp.";
        $strOT3 = number_format($rowData['ot_count2'],1)." x ".number_format($otMultiplierConfig03,1)." x ".number_format($salaryHourly,2)." = Rp.";
        $strOT4 = number_format($rowData['ot_count2'],1)." x ".number_format($otMultiplierConfig04,1)." x ".number_format($salaryHourly,2)." = Rp.";
        
        $spreadsheet->getActiveSheet()
            ->setCellValue('M12', $strOT1)
            ->setCellValue('N12', $otTotal1)
            ->setCellValue('M13', $strOT2)
            ->setCellValue('N13', $otTotal2)
            ->setCellValue('M14', $strOT3)
            ->setCellValue('N14', $otTotal3)
            ->setCellValue('M15', $strOT4)
            ->setCellValue('N15', $otTotal4)
            ->setCellValue('K16', 'OT TOTAL')
            ->setCellValue('O16', $allOt);

        $spreadsheet->getActiveSheet()->getStyle('K16:O16')->applyFromArray($topBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle('K16:O16')->applyFromArray($totalStyle);

        $this->M_salary->loadByBiodataIdNie($biodataId, $nie);
        $isOtBonus = $this->M_salary->getIsOtBonus(); 
        $isShiftBonus = $this->M_salary->getIsShiftBonus();
        $isRemoteAllowance = $this->M_salary->getIsRemoteAllowance();
        $isDevIncentiveBonus = $this->M_salary->getIsDevIncentiveBonus();

        /* START BONUS TOTAL */
        $bonusTotal = 0; 

        $outCampBonus = $rowData['flying_camp'];
        $safetyBonus = $rowData['safety_bonus'];
        $attendanceBonus = $rowData['attendance_bonus'];
        $otherAllowance1 = $rowData['other_allowance1'];
        $otherAllowance2 = $rowData['other_allowance2'];
        $bonusTotal = $outCampBonus + $safetyBonus + $attendanceBonus + $otherAllowance1 + $otherAllowance2;   
        /* END BONUS, INCENTIVE */

        $bonusTotal += $rowData['thr'];
        $bonusTotal += $rowData['contract_bonus'];
        // $spreadsheet->getActiveSheet()->getStyle('K'.($bonusIdx+7).':O'.($bonusIdx+7))->applyFromArray($topBorderStyle);
        // $spreadsheet->getActiveSheet()->getStyle('K'.($bonusIdx+7).':O'.($bonusIdx+7))->applyFromArray($totalStyle);

        $bonusIdx = 18; 
        $spreadsheet->getActiveSheet()
            ->setCellValue('K'.$bonusIdx, 'Out Camp Bonus')
            ->setCellValue('N'.$bonusIdx, $outCampBonus)
            ->setCellValue('K'.($bonusIdx+1), 'Safety Bonus')
            ->setCellValue('N'.($bonusIdx+1), $safetyBonus)
            ->setCellValue('K'.($bonusIdx+2), 'Attendance Bonus')
            ->setCellValue('N'.($bonusIdx+2), $attendanceBonus)
            ->setCellValue('K'.($bonusIdx+3), 'KPI Benefits')
            ->setCellValue('N'.($bonusIdx+3), $otherAllowance1)
            ->setCellValue('K'.($bonusIdx+4), 'Other benefits')
            ->setCellValue('N'.($bonusIdx+4),  $otherAllowance2)
            ->setCellValue('K'.($bonusIdx+5), 'THR')
            ->setCellValue('N'.($bonusIdx+5), $rowData['thr'])
            ->setCellValue('K'.($bonusIdx+6), 'Contract Bonus')
            ->setCellValue('N'.($bonusIdx+6), $rowData['contract_bonus'])
            ->setCellValue('K'.($bonusIdx+7), 'BONUS TOTAL')
            ->setCellValue('O'.($bonusIdx+7), $bonusTotal);

        $spreadsheet->getActiveSheet()->getStyle('K'.($bonusIdx+7).':O'.($bonusIdx+7))->applyFromArray($topBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle('K'.($bonusIdx+7).':O'.($bonusIdx+7))->applyFromArray($totalStyle);

        /* START GOVERNMENT REGULATION */
        $unpaidTotal = $rowData['unpaid_total'];
        $tUnpaidCount = $rowData['unpaid_count'];
        $subTotalVal = $rowData['jkk_jkm'] + $rowData['health_bpjs'] /*+ $rowData['jp']*/ - $unpaidTotal + $rowData['adjust_in'] - $rowData['adjust_out'];
        $govRegIdx = 27;
        $spreadsheet->getActiveSheet()
            ->setCellValue('K'.$govRegIdx, 'JKK & JKM(2.04%)')
            ->setCellValue('N'.$govRegIdx, $rowData['jkk_jkm'])
            ->setCellValue('K'.($govRegIdx+1), 'Iuran BPJS Kesehatan(4%)')
            ->setCellValue('N'.($govRegIdx+1), $rowData['health_bpjs'])
            ->setCellValue('K'.($govRegIdx+2), 'Potongan Absensi '.$tUnpaidCount.' Hari')
            ->setCellValue('N'.($govRegIdx+2), $unpaidTotal)
            ->setCellValue('K'.($govRegIdx+3), 'Adjustment In')
            ->setCellValue('N'.($govRegIdx+3), $rowData['adjust_in'])
            ->setCellValue('K'.($govRegIdx+4), 'Adjustment Out')
            ->setCellValue('N'.($govRegIdx+4), $rowData['adjust_out'])
            ->setCellValue('K'.($govRegIdx+5), 'Subtotal')
            ->setCellValue('O'.($govRegIdx+5), $subTotalVal);

        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(10, $govRegIdx+5, "Subtotal");
        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(14, $govRegIdx+5, $subTotalVal);
        $spreadsheet->getActiveSheet()->getStyle('K'.($govRegIdx+5).':O'.($govRegIdx+5))->applyFromArray($totalStyle);

        $brutto = $bsProrate + $allOt + $bonusTotal + $rowData['jkk_jkm'] + $rowData['health_bpjs'] /*+ $rowData['jp']*/ - $unpaidTotal + $rowData['adjust_in'] - $rowData['adjust_out'];
        $bruttoCoordinate = $govRegIdx+5;
        $spreadsheet->getActiveSheet()
            ->setCellValue('K'.$bruttoCoordinate, 'TOTAL KOTOR')
            ->setCellValue('O'.$bruttoCoordinate, $brutto);

        $spreadsheet->getActiveSheet()->getStyle('K'.$bruttoCoordinate.':O'.$bruttoCoordinate)->applyFromArray($topBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle('K'.$bruttoCoordinate.':O'.$bruttoCoordinate)->applyFromArray($totalStyle);

        $taxCoordinate = 47; /* TAX ROWS COORDINATE  */
        $tmpCoordinate1 = $taxCoordinate+17;
        $tmpCoordinate2 = 0;
        $spreadsheet->getActiveSheet()->getStyle("K".$taxCoordinate.":O".($tmpCoordinate1-1))->applyFromArray($outlineBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle('K'.$taxCoordinate.':O'.($taxCoordinate))->applyFromArray($totalStyle);
        $tmpCoordinate1 = $taxCoordinate + 19;
        $spreadsheet->getActiveSheet()->getStyle("K".$taxCoordinate.":O".$tmpCoordinate1)->getFont()->setSize(13);

        $myConfigId = 1;  
        $row = $this->M_payroll_config->getObjectById($myConfigId);       
        $maxNonTax = $this->M_payroll_config->getNonTaxAllowance(); /* Max Tunjangan Jabatan */

        $nonTaxAllowance = $brutto * (5/100);
        if($nonTaxAllowance > $maxNonTax){
           $nonTaxAllowance = $maxNonTax; 
        }
        
        $spreadsheet->getActiveSheet()
            ->setCellValue('K'.$taxCoordinate, 'PERHITUNGAN PAJAK PENGHASILAN')
            ->setCellValue('K'.($taxCoordinate+1), 'Penghasilan Kotor')
            ->setCellValue('O'.($taxCoordinate+1), $brutto)
            ->setCellValue('K'.($taxCoordinate+2), 'Iuran JP TK(1%)')
            ->setCellValue('O'.($taxCoordinate+2), $rowData['emp_jp'])
            ->setCellValue('K'.($taxCoordinate+3), 'Iuran JHT')
            ->setCellValue('O'.($taxCoordinate+3), $rowData['emp_jht'])            
            ->setCellValue('K'.($taxCoordinate+4), 'Tunjangan Jabatan')
            ->setCellValue('O'.($taxCoordinate+4), $nonTaxAllowance);

        /* START NETTO */
        $netto = $brutto - $rowData['emp_jht'] - $rowData['emp_jp'] - $nonTaxAllowance;


        /* START TAX PROCESS */
        $ptkpTotal = $rowData['ptkp_total'];
        /* Perhitungan pajak yang disetahunkan, hanya penghasilan tetap saja. Tunjangan tidak tetap seperti THR dihitung terpisah */
        $nettoSetahun = ( ($netto - $rowData['thr']-$rowData['contract_bonus']) * 12) + $rowData['thr'] + $rowData['contract_bonus'];
        $penghasilanPajak = 0;
        if($nettoSetahun >= $ptkpTotal)
        {
            $penghasilanPajak = $nettoSetahun - $ptkpTotal;
        }
        $pembulatanPenghasilan = floor($penghasilanPajak);
        
        $nettoCoordinate = 53;
        $spreadsheet->getActiveSheet()->getStyle('K'.$nettoCoordinate.':O'.($nettoCoordinate))->applyFromArray($totalStyle);
        $spreadsheet->getActiveSheet()->getStyle('K'.$nettoCoordinate.':O'.($nettoCoordinate))->applyFromArray($topBorderStyle);
        $spreadsheet->getActiveSheet()
            ->setCellValue('K'.$nettoCoordinate, 'Netto')
            ->setCellValue('O'.$nettoCoordinate, $netto);
        /* END NETTO */
        /*if($brutto <= 0)
        {
            $brutto = 0;    
        }*/ 
        $yearNettoCoordinate = 54;
        $spreadsheet->getActiveSheet()->getStyle('K'.($yearNettoCoordinate+2).':O'.($yearNettoCoordinate+2))->applyFromArray($topBorderStyle);
        $spreadsheet->getActiveSheet()
            ->setCellValue('K'.$yearNettoCoordinate, 'Netto Disetahunkan(Tetap)')
            ->setCellValue('O'.$yearNettoCoordinate, $nettoSetahun)
            ->setCellValue('K'.($yearNettoCoordinate+1), 'PTKP '.$rowData['marital_status'])
            ->setCellValue('O'.($yearNettoCoordinate+1), $ptkpTotal)
            ->setCellValue('K'.($yearNettoCoordinate+2), 'PTKP '.$rowData['marital_status'])
            ->setCellValue('O'.($yearNettoCoordinate+2), $penghasilanPajak)
            ->setCellValue('K'.($yearNettoCoordinate+3), 'Pembulatan')
            ->setCellValue('O'.($yearNettoCoordinate+3), $pembulatanPenghasilan);


        $taxPercent1 = $this->M_payroll_config->getTaxPercent1(); /* Persentase Progresif Pajak I */
        $taxPercent2 = $this->M_payroll_config->getTaxPercent2(); /* Persentase Progresif Pajak II */
        $taxPercent3 = $this->M_payroll_config->getTaxPercent3(); /* Persentase Progresif Pajak III */
        $taxPercent4 = $this->M_payroll_config->getTaxPercent4(); /* Persentase Progresif Pajak IV */
        $maxTaxVal1 = $this->M_payroll_config->getTaxMaxVal1(); /* Nominal Pajak I */ 
        $maxTaxVal2 = $this->M_payroll_config->getTaxMaxVal2(); /* Nominal Pajak II */
        $maxTaxVal3 = $this->M_payroll_config->getTaxMaxVal3(); /* Nominal Pajak III */
        $maxTaxVal4 = $this->M_payroll_config->getTaxMaxVal4(); /* Nominal Pajak IV */
        $taxVal1 = 0;    
        $taxVal2 = 0;    
        $taxVal3 = 0;    
        $taxVal4 = 0;

        $tVal = 0;
        $tSisa = 0;
        if($pembulatanPenghasilan > 0)
        {
            /* TAX 1 */
            if($maxTaxVal1 > 0)
            {
                $tVal = $pembulatanPenghasilan/$maxTaxVal1;  
                if($tVal >= 1){
                    $taxVal1 = $maxTaxVal1 * ($taxPercent1/100);
                }
                else{
                    $taxVal1 = $pembulatanPenghasilan * ($taxPercent1/100);
                }    
            }    
            
            /* TAX 2 */
            if($maxTaxVal2 > 0)
            {
                if($pembulatanPenghasilan > $maxTaxVal1)
                {
                    $tSisa = $pembulatanPenghasilan - $maxTaxVal1; 
                    $tVal = $tSisa/$maxTaxVal2;
                    if($tVal >= 1){
                        $taxVal2 = $maxTaxVal2 * ($taxPercent2/100);
                    }
                    else{
                        $taxVal2 = $tSisa * ($taxPercent2/100);
                    }
                }     
            }
             
            /* TAX 3 */
            if($maxTaxVal3 > 0)
            {
                if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2))
                {
                    $tSisa = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2;
                    $tVal = $tSisa/$maxTaxVal3;
                    if($tVal >= 1){
                        $taxVal3 = $maxTaxVal3 * ($taxPercent3/100);
                    }
                    else{
                        $taxVal3 = $tSisa * ($taxPercent3/100);
                    }
                }    
            }
             
            /* TAX 4 */
            if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2 + $maxTaxVal3))
            {
                $tSisa = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2 - $maxTaxVal3;
                $taxVal4 = $tSisa * ($taxPercent4/100); 
            }               
        }
        
        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, 63, $line);
        $pajakTerutang = $taxVal1 + $taxVal2 + $taxVal3 + $taxVal4;

        /* Additional Charge if there is no NPWP */
        if( strlen($npwpNo) < 19 ){
            $pajakTerutang = $pajakTerutang + ($pajakTerutang * ($npwpCharge/100) ); 
        }

        $tarifTaxCoordinate = $taxCoordinate + 10; /* TAX ROWS COORDINATE  */        
        $pajakSebulan = ($pajakTerutang/12);
        $pajakSebulan = floor($pajakSebulan);
        $spreadsheet->getActiveSheet()->getStyle('K'.($tarifTaxCoordinate+5).':O'.($tarifTaxCoordinate+6))->applyFromArray($totalStyle);
        $spreadsheet->getActiveSheet()->getStyle('K'.($tarifTaxCoordinate+5).':O'.($tarifTaxCoordinate+5))->applyFromArray($topBorderStyle);
        // $tarifTaxCoordinate = $taxCoordinate + 12; /* TAX ROWS COORDINATE  */
        $spreadsheet->getActiveSheet()
            ->setCellValue('K'.($tarifTaxCoordinate+1), 'Tarif Pajak I    '.$taxPercent1.'%')
            ->setCellValue('O'.($tarifTaxCoordinate+1), $taxVal1)
            ->setCellValue('K'.($tarifTaxCoordinate+2), 'Tarif Pajak II    '.$taxPercent2.'%')
            ->setCellValue('O'.($tarifTaxCoordinate+2), $taxVal2)
            ->setCellValue('K'.($tarifTaxCoordinate+3), 'Tarif Pajak III    '.$taxPercent3.'%')
            ->setCellValue('O'.($tarifTaxCoordinate+3), $taxVal3)
            ->setCellValue('K'.($tarifTaxCoordinate+4), 'Tarif Pajak IV    '.$taxPercent4.'%')
            ->setCellValue('O'.($tarifTaxCoordinate+4), $taxVal4)
            ->setCellValue('K'.($tarifTaxCoordinate+5), 'Pajak Gaji Setahun')
            ->setCellValue('O'.($tarifTaxCoordinate+5), $pajakTerutang)
            ->setCellValue('K'.($tarifTaxCoordinate+6), 'Pajak Gaji Sebulan')
            ->setCellValue('O'.($tarifTaxCoordinate+6), $pajakSebulan);

        /* START UPDATE TAX VALUE TO THE TABLE */            
        $str = "UPDATE trn_salary_slip SET tax_value = ".$pajakSebulan.", non_tax_allowance=".$nonTaxAllowance." WHERE salary_slip_id = '".$slipId."' ";
        $this->db->query($str);
        /* END UPDATE TAX VALUE TO THE TABLE */

        /* START OUT */
        $terima = $brutto-floor($pajakSebulan)-$rowData['jkk_jkm']-$rowData['emp_jht']-$rowData['emp_health_bpjs']-$rowData['emp_jp']-$rowData['health_bpjs'];
        $outCoordinate = 35;
        $spreadsheet->getActiveSheet()
            ->setCellValue('K'.$outCoordinate, 'Pajak Penghasilan')
            ->setCellValue('O'.$outCoordinate, 0)
            ->setCellValue('K'.($outCoordinate+1), 'JKK & JKM(2.04%)')
            ->setCellValue('O'.($outCoordinate+1), 0)
            ->setCellValue('K'.($outCoordinate+2), 'Iuran JHT BPJS TK(2%)')
            ->setCellValue('O'.($outCoordinate+2), 0)
            ->setCellValue('K'.($outCoordinate+3), 'Iuran BPJS Kesehatan TK(1%)')
            ->setCellValue('O'.($outCoordinate+3), 0)
            ->setCellValue('K'.($outCoordinate+4), 'Iuran BPJS Kesehatan(4%)')
            ->setCellValue('O'.($outCoordinate+4), 0)
            ->setCellValue('K'.($outCoordinate+5), 'Iuran JP TK(1%)')
            ->setCellValue('O'.($outCoordinate+5), 0);

        $pajakSebulan = floor($pajakSebulan);

        if($pajakSebulan > 0) {
            $spreadsheet->getActiveSheet()
             ->setCellValue('O'.$outCoordinate, -$pajakSebulan);
        }

        if($rowData['jkk_jkm'] > 0) {
            $spreadsheet->getActiveSheet()
             ->setCellValue('O'.($outCoordinate+1), -$rowData['jkk_jkm']);
        } 

        if($rowData['emp_jht'] > 0) {
            $spreadsheet->getActiveSheet()
             ->setCellValue('O'.($outCoordinate+2), -$rowData['emp_jht']);
        } 

        if($rowData['emp_health_bpjs'] > 0) {
            $spreadsheet->getActiveSheet()
             ->setCellValue('O'.($outCoordinate+3), -$rowData['emp_health_bpjs']);
        } 

        if($rowData['health_bpjs'] > 0) {
            $spreadsheet->getActiveSheet()
             ->setCellValue('O'.($outCoordinate+4), -$rowData['health_bpjs']);
        } 

        if($rowData['emp_jp'] > 0) {
            $spreadsheet->getActiveSheet()
             ->setCellValue('O'.($outCoordinate+5), -$rowData['emp_jp']);
        } 
        
        $outCoordinate2 = $outCoordinate + 6;
        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $outCoordinate2, "TOTAL SEBELUM POTONGAN");
        $spreadsheet->getActiveSheet()->getStyle('K'.$outCoordinate2.':O'.$outCoordinate2)->applyFromArray($topBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle('K'.$outCoordinate2.':O'.$outCoordinate2)->applyFromArray($totalStyle);
        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(14, $outCoordinate2, $terima);

        $debtText = "";
        if($rowData['debt_explanation'] != "") {
            $debtText = "(".$rowData['debt_explanation'].")";
        }
        $tBurden = $rowData['debt_burden'];
        $tBurden = $tBurden * (-1);

        $cutCoordinate = 42;
        $spreadsheet->getActiveSheet()
            ->setCellValue('K'.$outCoordinate2, 'Pajak Penghasilan')
            ->setCellValue('O'.$outCoordinate2, $terima)
            ->setCellValue('K'.($cutCoordinate+1), 'THR')
            ->setCellValue('O'.($cutCoordinate+1), 0)
            ->setCellValue('K'.($cutCoordinate+2), 'Contract Bonus')
            ->setCellValue('O'.($cutCoordinate+2), 0)
            ->setCellValue('K'.($cutCoordinate+3), 'Beban Hutang')
            ->setCellValue('M'.($cutCoordinate+3), $debtText)
            ->setCellValue('O'.($cutCoordinate+3), $tBurden);

        /* START POTONGAN */
        if($rowData['thr'] > 0) {
            $spreadsheet->getActiveSheet()
            ->setCellValue('O'.($cutCoordinate+1), -$rowData['thr']);
        } 

        if($rowData['contract_bonus'] > 0) {
            $spreadsheet->getActiveSheet()
              ->setCellValue('O'.($cutCoordinate+2), -$rowData['contract_bonus']);
        }        
        /* END POTONGAN */
        
        $cutCoordinate2 = $cutCoordinate+3;
        $cutCoordinate3 = $cutCoordinate+4;
        $spreadsheet->getActiveSheet()->getStyle("K".$cutCoordinate.":O".$cutCoordinate3)->getFont()->setSize(13);
        $spreadsheet->getActiveSheet()->getStyle('K'.$cutCoordinate3.':O'.$cutCoordinate3)->applyFromArray($totalStyle);
        $spreadsheet->getActiveSheet()->getStyle('K'.$cutCoordinate3.':O'.$cutCoordinate3)->applyFromArray($topBorderStyle);

        $thp = $terima - $rowData['thr'] - $rowData['contract_bonus'] - $rowData['debt_burden']; 
        $spreadsheet->getActiveSheet()
              ->setCellValue('K'.($cutCoordinate2+1), 'TOTAL TERIMA')
              ->setCellValue('O'.($cutCoordinate2+1), $thp);
        /* END OUT */

        $spreadsheet->getActiveSheet()
          ->getStyle('K7:O63')->getNumberFormat()
          ->setFormatCode('#,##0.00');

        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('Report Excel '.date('d-m-Y H'));

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        //Nama File
        $str = $rowData['name'].$rowData['bio_rec_id'];
        $fileName = preg_replace('/\s+/', '', $str);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.Xlsx"');
        // header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        /* BY COMPOSER */
        // $writer = new Xlsx($spreadsheet);
        /* OFFLINE/ BY COPY EXCEL FOLDER  */
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit(0);

    }

    public function pontilSmbExcel($slipId, $calendarStart, $payrollGroup, $isHealthBPJS, $isJHT, $isJP, $isJKKM)
    {
        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();

        $spreadsheet->getProperties()->setCreator('Maurice - Web - Android')
            ->setLastModifiedBy('Maurice - Web - Android')
            ->setTitle('Office 2007 XLSX Test Document')
            ->setSubject('Office 2007 XLSX Test Document')
            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Test result file');

        $boldFont = [
            'font' => [
                'bold' => true
                // 'color' => ['argb' => '0000FF'],
            ],
        ];

        $totalStyle = [
            'font' => [
                'bold' => true,
                'color' => ['argb' => '0000FF'],
            ],
        ];

        $allBorderStyle = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $outlineBorderStyle = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $topBorderStyle = [
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $bottomBorderStyle = [
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        // $center = [
        //     'alignment' => [
        //         'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
        //         'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
        //     ],
        // ];

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        // http://192.168.10.17/os-hris-2019/sumbawa/Payroll/toexcel/2019031175/Machmahon_Sumbawa/1/undefined/1/1/1/1
        // $slipId = "2019040001";
        // $calendarStart = "1";
        // $payrollGroup = "";
        // $isHealthBPJS = "1";
        // $isJHT = "1";
        // $isJP = "1";
        // $isJKKM = "1";
        
        /* CALENDAR SETUP */
        $dateCal = $calendarStart;

        /* START VALIDATE & UPDATE GOVERNMENT REGULATION */     
        $this->M_payroll_config->loadByClient('Pontil_Sumbawa');
        $myConfigId = $this->M_payroll_config->getPayrollConfigId();
        // $this->M_payroll_config->loadById($myConfigId); 
        $healthBpjsConfig = $this->M_payroll_config->getHealthBpjs();
        $maxHealthBpjsConfig = $this->M_payroll_config->getMaxHealthBpjs();
        $jkkJkmConfig = $this->M_payroll_config->getJkkJkm();
        $jhtConfig = $this->M_payroll_config->getJht();
        $empJhtConfig = $this->M_payroll_config->getEmpJht();             
        $empHealthBpjsConfig = $this->M_payroll_config->getEmpHealthBpjs();    
        $jpConfig = $this->M_payroll_config->getJp();
        $empJpConfig = $this->M_payroll_config->getEmpJp();
        // echo $empHealthBpjsConfig; exit(0);
        $npwpCharge = $this->M_payroll_config->getNpwpCharge();
        $this->M_salary_slip->getObjectById($slipId);
        $basicSalary = $this->M_salary_slip->getBasicSalary();
        // echo $healthBpjsConfig; exit(0);
        /* START HEALTH BPJS */
        $healthBpjs = ($healthBpjsConfig/100) * $basicSalary; /* Nilai mst_payroll_config.health_bpjs(%) x Basic Salary (Max 8 Juta) */
        if($healthBpjs > $maxHealthBpjsConfig){
            $healthBpjs = $maxHealthBpjsConfig; 
        }
        /* END HEALTH BPJS */
        /* START JKK-JKM */
        $jkkJkm = ($jkkJkmConfig/100) * $basicSalary; /* Nilai mst_payroll_config.jkk_jkm(%) x Basic Salary */ 
        /* END JKK-JKM */                
        /* START JHT */
        $jht = ($jhtConfig/100) * $basicSalary;  /*Nilai mst_payroll_config.jht(%) x Basic Salary  */       
        /* END JHT */                
        /* START JHT */
        $empJht = ($empJhtConfig/100) * $basicSalary; /* Nilai mst_payroll_config.emp_jht(%) x Basic Salary */      
        /* END JHT */
        /* START JP COMPANY */
        // $jp = ($jpConfig/100) * $basicSalary; /* Nilai mst_payroll_config.jp(%) x Basic Salary */        
        /* END JP COMPANY */
        /* START JP EMPLOYEE */
        $empJp = ($empJpConfig/100) * $basicSalary; /* Nilai mst_payroll_config.emp_jp(%) x Basic Salary */        
        /* END JP EMPLOYEE */               
        
        /* START BPJS KARYAWAN */
        $maxEmpHealthBpjs = $this->M_payroll_config->getMaxEmpBpjs(); /* Nilai Max Gaji Untuk Iuran BPJS Karyawan  */ 
        $empHealthBpjs = ($empHealthBpjsConfig/100) * $basicSalary; /* Nilai mst_payroll_config.emp_health_bpjs(%) x Basic Salary */
        if($empHealthBpjs > $maxEmpHealthBpjs)
        {
            $empHealthBpjs = $maxEmpHealthBpjs; 
        }     
        /* END BPJS KARYAWAN */        

        if($isHealthBPJS == '0')
        {
            $healthBpjs = 0;
            $empHealthBpjs = 0;
        }
        if($isJHT == '0')
        {
            $jht = 0;
            $empJht = 0;
        }
        if($isJP == '0')
        {
            $empJp = 0;
        }
        if($isJKKM == '0')
        {
            $jkkJkm = 0;
        }

        $this->M_salary_slip->setHealthBpjs($healthBpjs);
        $this->M_salary_slip->setEmpHealthBpjs($empHealthBpjs);
        $this->M_salary_slip->setJht($jht);
        $this->M_salary_slip->setEmpJht($empJht);
        $this->M_salary_slip->setEmpJp($empJp);
        $this->M_salary_slip->setJkkJkm($jkkJkm);
        $this->M_salary_slip->update($slipId);
        /* END VALIDATE & UPDATE GOVERNMENT REGULATION */

        $this->db->select('ss.*, mb.bio_rec_id biodata_id, ss.basic_salary salary_basic,ss.bs_prorate, mb.npwp_no, tro.*, mr.*');
        $this->db->from('trn_salary_slip ss, mst_bio_rec mb');
        $this->db->join('mst_roster mr','mb.bio_rec_id = mr.bio_rec_id');
        $this->db->join('trn_overtime tro','mb.bio_rec_id = tro.bio_rec_id');
        $this->db->where('ss.bio_rec_id = mb.bio_rec_id AND ss.client_name = "Pontil_Sumbawa"');
        $this->db->where('salary_slip_id', $slipId);
        $this->db->where('ss.year_period = tro.year_period AND ss.month_period = tro.month_period');        
        $this->db->where('ss.year_period = mr.year_process AND ss.month_period = mr.month_process');        
        $rowData = $query = $this->db->get()->row_array();  // Produces: SELECT * FROM trn_salary_slip (one row only) 
        $ptName = $rowData['client_name'];  
        $monthPeriod = $rowData['month_period'];
        $yearPeriod = $rowData['year_period'];     
        $biodataId = $rowData['biodata_id'];     
        $nie = $rowData['nie'];     
        $dept = $rowData['dept'];     
        $rosterFormat = $rowData['roster_format'];     
        $rosterBase = $rowData['roster_base']; 
        $payrollLevel = $rowData['payroll_level']; 
        $npwpNo = $rowData['npwp_no']; 
        $tMonth = (int) $monthPeriod;
        $tYear = (int) $yearPeriod;

        // echo $this->db->last_query(); exit();

        $wdAttendCount = 0;
        $wdAttendCount = $rowData['in_shift'] + $rowData['in_off'] + $rowData['in_ph'];
        // $wdAttendCount += $rowData['attend_total']; // 5
        //                 $wdAttendCount += $rowData['in_ph_total']; //2
        //                 $wdAttendCount -= $rowData['attend_in_off'];//2
        //                 $wdAttendCount += $rowData['sick_total']; //0
        //                 $wdAttendCount += $rowData['emergency_total']; //1
        //                 $wdAttendCount += $rowData['paid_permit_total']; //0
        //                 $wdAttendCount += $rowData['paid_vacation_total']; //0
        //                 $wdAttendCount += $rowData['alpa_total']; 
        //                 $wdAttendCount += $rowData['off_total'];

        $dayCountInMonth = cal_days_in_month(CAL_GREGORIAN, $tMonth, $tYear); 

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(6);             
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(9);             
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(7);             
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(8);         
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(5);             
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(5);             
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(5);             
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(5);             
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(5);             
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(3);             
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(8);

        $tDate1 = $calendarStart.'-'.$monthPeriod.'-'.$yearPeriod;
        $tStr = $yearPeriod.'-'.$monthPeriod.'-'.($calendarStart-1);
        $tDate2 = strtotime($tStr);
        $tDate3 = date("d-m-Y", strtotime("+1 month", $tDate2));
        // Add some data
        $spreadsheet->getActiveSheet()
            ->setCellValue('A1', 'PT. SANGATI SOERYA SEJAHTERA')
            ->setCellValue('A2', 'Slip Gaji Karyawan')
            ->setCellValue('A4', 'PT : '.$ptName)
            ->setCellValue('A5', 'Periode : '.$tDate1.' to '.$tDate3);

        $spreadsheet->getActiveSheet()->getStyle("A1:D1")->getFont()->setBold(true)->setSize(16);
        $spreadsheet->getActiveSheet()->getStyle("A2:D2")->getFont()->setBold(true)->setSize(14)->setUnderline(true);
        $spreadsheet->getActiveSheet()->getStyle("A4:G4")->getFont()->setBold(true)->setSize(12);

        $spreadsheet->getActiveSheet()->mergeCells("A6:A7");
        $spreadsheet->getActiveSheet()->mergeCells("B6:B7");
        $spreadsheet->getActiveSheet()->mergeCells("C6:C7");
        $spreadsheet->getActiveSheet()->mergeCells("D6:D7");
        $spreadsheet->getActiveSheet()->mergeCells("E6:E7");
        $spreadsheet->getActiveSheet()->mergeCells("F6:I6");
        
        $spreadsheet->getActiveSheet()->getStyle("A6:E7")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("F6:I7")->applyFromArray($allBorderStyle);

        // $spreadsheet->getActiveSheet()
        //     ->getStyle('A10:H10')
        //     ->getBorders()
        //     ->getBottom()
        //     ->setBorderStyle(Border::BORDER_THIN)
        //     ->setColor(new Color('FFFF0000'));
        
        $spreadsheet->getActiveSheet()->getStyle("A6:I7")->applyFromArray($center);
        // $spreadsheet->getActiveSheet()->getStyle('A6:I7')->getAlignment()->setHorizontal('center');
        // $spreadsheet->getActiveSheet()->getStyle('A6:I7')->getAlignment()->setVertical('center');

        $spreadsheet->getActiveSheet()->getStyle("A6:I7")->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()
            ->setCellValue('A6', 'DATE')
            ->setCellValue('B6', 'ROSTER DAY')
            ->setCellValue('C6', 'SHIFT DAY')
            ->setCellValue('D6', 'HOURS TOTAL')
            ->setCellValue('E6', 'NT')
            ->setCellValue('F6', 'OVERTIME')
            ->setCellValue('F7', '1')
            ->setCellValue('G7', '2')
            ->setCellValue('H7', '3')
            ->setCellValue('I7', '4');

        $spreadsheet->getActiveSheet()->getStyle("A6:I7")->applyFromArray($boldFont);
        // $spreadsheet->getActiveSheet()->getStyle("A6:I7")->getFont()->setBold();
        // $spreadsheet->getDefaultStyle()->getFont()->setBold();  
        // $spreadsheet->getActiveSheet()->getStyle("A6:I7")->getFont()->setBold();  
        $rosterFormat = $rowData['roster_format'];
        $tmpRosterLength = strlen($rosterFormat);
        $strNumRoster = '';
        $tmp = '';
        $dayNo = '';
        $groupCount = 0;
        $strNum = '';
        $dayTotal = 0;
        /* START GET DAYS TOTAL BY ROSTER */
        for ($i=0; $i < $tmpRosterLength; $i++) {
            $strNum = substr($rosterFormat,$i,1);
            /* START MAKE SURE DATA IS NUMBER */ 
            if(is_numeric($strNum))
            {
                $strNumRoster .= $strNum;
                $dayTotal += $strNum;
            }
            /* END MAKE SURE DATA IS NUMBER */ 
        }
        $numChar = '';
        $rdData = '';
        $dataIdx = 8;
        $tIdx = 0;  

        $ot01Count = 0;
        $ot02Count = 0;
        $ot03Count = 0;
        $ot04Count = 0;

        $normalTotal = 0;
        $allTimeTotal = 0;
        /* START SUMMARY ROSTER */
        $rowIdx = 8;
        /* START CALENDAR VALUE */
        // $dayCount = cal_days_in_month(CAL_GREGORIAN, $monthPeriod, $yearPeriod);   

        /* Get Last Day Number */
        $strDate = $yearPeriod.'-'.$monthPeriod.'-01';
        $tQuery  = 'SELECT * FROM mst_roster_hist ';
        $tQuery .= 'WHERE bio_rec_id = "'.$biodataId.'" ';
        $tQuery .= 'AND DATE_ADD("'.$strDate.'", INTERVAL -1 MONTH) = DATE_FORMAT(CONCAT(year_period,"-",month_period,"-01"), "%Y-%m-%d") ';
        $tQuery .= 'LIMIT 1 ';
        $tData = $this->db->query($tQuery)->row_array();
        $tLastDay = 0;
        if(isset($tData)){
            $tLastDay = $tData['last_day']; 
        }

        $rosterIdx = $tLastDay;
        for ($i=1; $i <= $dayCountInMonth; $i++) { 
            /* START NUMBER TITLE */
            $rowIdx++;

            $ot01Column = '';
            $ot02Column = '';
            $ot03Column = '';
            $ot04Column = '';

            if($i < 10){
                $ot01Column = 'ot1_d0'.$i;
                $ot02Column = 'ot2_d0'.$i;
                $ot03Column = 'ot3_d0'.$i;
                $ot04Column = 'ot4_d0'.$i;
                $timePerday = 'd0'.$i;
            }else{
                $ot01Column = 'ot1_d'.$i;
                $ot02Column = 'ot2_d'.$i;
                $ot03Column = 'ot3_d'.$i;
                $ot04Column = 'ot4_d'.$i;
                $timePerday = 'd'.$i;
            }
            /* OVER TIME */
            $ot01Val = $rowData[$ot01Column];
            $ot02Val = $rowData[$ot02Column];
            $ot03Val = $rowData[$ot03Column];
            $ot04Val = $rowData[$ot04Column];
            
            $timeTotal = 0;
            $strTimePerday = $rowData[$timePerday];
            $tTimePerday = 0;            

            /* START PUBLIC HOLIDAY  */
            $phCodeTmp = substr($strTimePerday,0,2);
            $phHoursTmp = substr($strTimePerday,2,strlen($strTimePerday)-2);
            if( (strtoupper($phCodeTmp) == "RO" || strtoupper($phCodeTmp) == "PH" || strtoupper($phCodeTmp) == "SP" || strtoupper($phCodeTmp) == "NS" || strtoupper($phCodeTmp) == "SD") && (is_numeric($phHoursTmp)) && ($phHoursTmp > 0) )
            {
                $tTimePerday = $phHoursTmp;
            }
            else
            {
                $tTimePerday = $rowData[$timePerday];
            }

            if(is_numeric($tTimePerday))
            {
                $timeTotal = $tTimePerday;
                if($tTimePerday > 1) {
                    $attendStatus = 1;
                }
            }

            $normalTime = $timeTotal - $ot01Val - $ot02Val - $ot03Val - $ot04Val; 
            $normalTotal += $normalTime;

            $allTimeTotal += $timeTotal; 

            $ot01Count += $ot01Val;
            $ot02Count += $ot02Val;
            $ot03Count += $ot03Val;
            $ot04Count += $ot04Val;
                         
            if($dateCal > $dayCountInMonth){
               $dateCal = 1; 
            }

            $spreadsheet->getActiveSheet()->getStyle('A'.$rowIdx.':A'.$rowIdx)->applyFromArray($allBorderStyle);
            $spreadsheet->getActiveSheet()->getStyle('A'.$rowIdx.':A'.$rowIdx)->applyFromArray($right);
            $spreadsheet->getActiveSheet()->setCellValue('A'.$rowIdx, $dateCal);  

            $dateCal++;
            /* END NUMBER TITLE */

                    
            $attendStatus = 0;        
            $rosterIdx++;
            if($rosterIdx > 7){
               $rosterIdx = 1;                
            }
            $rdData = $rosterIdx;
            if( strtoupper($strTimePerday) == 'STR' || strtoupper($strTimePerday) == 'END' )
            {
                $rdData = $strTimePerday; 
                $rosterIdx = 0;
            }
            if( strtoupper($strTimePerday) == 'ID' || strtoupper($strTimePerday) == 'A' || strtoupper($strTimePerday) == 'S' || strtoupper($strTimePerday) == 'I') {
                $rdData = $strTimePerday; 
            }
            
            if( strtoupper($phCodeTmp) == 'RO' ) {
                $rdData = $phCodeTmp; 
                $rosterIdx = 0;
            }

            if( strtoupper($phCodeTmp) == 'SP' ) {
                $rdData = $phCodeTmp; 
                $rosterIdx = 0;
            }

            if( strtoupper($phCodeTmp) == 'PH' || strtoupper($phCodeTmp) == 'NS') {
                $rdData = $phCodeTmp; 
            }
            
            $hourVal = right($strTimePerday,1); 
            if( is_numeric($hourVal) ) {
                $attendStatus = 1;        
            }

            $arrTmpCode = array('PH','SB','PV','PP','RO','NS','SD','ID');
            $arrOtTmp = array('A','U','I','S','V', 'E', 'STR', 'END');
            if( (!in_array( strtoupper($phCodeTmp), $arrTmpCode)) && (!in_array( strtoupper($strTimePerday), $arrOtTmp)) && !is_numeric($strTimePerday))
            {
                $attendStatus = 0;      
            }
            
            $spreadsheet->getActiveSheet()
                ->setCellValue('B'.$rowIdx, $rdData)
                ->setCellValue('C'.$rowIdx, $attendStatus)
                ->setCellValue('D'.$rowIdx, $timeTotal)
                ->setCellValue('E'.$rowIdx, $normalTime)
                ->setCellValue('F'.$rowIdx, $ot01Val)
                ->setCellValue('G'.$rowIdx, $ot02Val)
                ->setCellValue('H'.$rowIdx, $ot03Val)
                ->setCellValue('I'.$rowIdx, $ot04Val);

            $spreadsheet->getActiveSheet()->getStyle("B".$rowIdx.":I".$rowIdx)->applyFromArray($allBorderStyle);
            $spreadsheet->getActiveSheet()->getStyle("B".$rowIdx.":I".$rowIdx)->applyFromArray($center);  
            // $spreadsheet->getActiveSheet()->getStyle("B".$rowIdx.":I".$rowIdx)->getAlignment()->setHorizontal('center');
            // $spreadsheet->getActiveSheet()->getStyle("B".$rowIdx.":I".$rowIdx)->getAlignment()->setVertical('center');

        } // for ($i=1; $i <= 31 ; $i++)


        $this->M_roster_hist->resetValues();
        $this->M_roster_hist->delete($slipId);    
        $this->M_roster_hist->setRhId($slipId);    
        $this->M_roster_hist->setBioRecId($biodataId);    
        $this->M_roster_hist->setClientName($ptName);    
        $this->M_roster_hist->setYearPeriod($yearPeriod);    
        $this->M_roster_hist->setMonthPeriod($monthPeriod);    
        $this->M_roster_hist->setLastDay($rosterIdx); 
        $this->M_roster_hist->insert(); 

        $totalTitle = 40;
        $spreadsheet->getActiveSheet()
                ->setCellValue('A'.$totalTitle, 'TOTAL')
                ->setCellValue('C'.$totalTitle, $wdAttendCount)
                ->setCellValue('D'.$totalTitle, $allTimeTotal)
                ->setCellValue('E'.$totalTitle, $normalTotal)
                ->setCellValue('F'.$totalTitle, $ot01Count)
                ->setCellValue('G'.$totalTitle, $ot02Count)
                ->setCellValue('H'.$totalTitle, $ot03Count)
                ->setCellValue('I'.$totalTitle, $ot04Count);

        $spreadsheet->getActiveSheet()->getStyle("A".$totalTitle.":A".$totalTitle+1)->applyFromArray($center);
        // $spreadsheet->getActiveSheet()->getStyle("A".$totalTitle.":A".$totalTitle+1)->getAlignment()->setHorizontal('center');
        // $spreadsheet->getActiveSheet()->getStyle("A".$totalTitle.":A".$totalTitle+1)->getAlignment()->setVertical('center');

        $spreadsheet->getActiveSheet()->getStyle("C".$totalTitle.":I".$totalTitle)->applyFromArray($right);
        $spreadsheet->getActiveSheet()->getStyle("A".$totalTitle.":I".$totalTitle)->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("A".$totalTitle.":I".$totalTitle)->getFont()->setBold(true)->setSize(13);
        
        /* END TITLE */       

        /* START TOTAL OVER TIME  */
        $totalTitle++; 
        /* Title */
        $spreadsheet->getActiveSheet()->getStyle("A".$totalTitle.":A".$totalTitle+1)->applyFromArray($center);
        // $spreadsheet->getActiveSheet()->getStyle("A".$totalTitle.":A".$totalTitle+1)->getAlignment()->setHorizontal('center');
        // $spreadsheet->getActiveSheet()->getStyle("A".$totalTitle.":A".$totalTitle+1)->getAlignment()->setVertical('center');

        $spreadsheet->getActiveSheet()->getStyle("A".$totalTitle.":A".$totalTitle)->getFont()->setBold(true)->setSize(13);
        $spreadsheet->getActiveSheet()->setCellValue('A'.$totalTitle, 'TOTAL OT');
        /* Data */
       
        $spreadsheet->getActiveSheet()->getStyle("C41:I41")->applyFromArray($right);
        $spreadsheet->getActiveSheet()->getStyle("A41:I41")->applyFromArray($allBorderStyle);
        $spreadsheet->getActiveSheet()->mergeCells("C41:I41");
        $spreadsheet->getActiveSheet()->getStyle("C41:I41")->getFont()->setBold(true)->setSize(13);
        $spreadsheet->getActiveSheet()->setCellValue('C41', '=SUM(F40:I40)');

        $spreadsheet->getActiveSheet()->getStyle("A46:I63")->applyFromArray($outlineBorderStyle);
        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(1, 47, "Dibayarkan Oleh");
        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(1, 51, "Payroll/ Accounting");
        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(1, 55, "Diterima Oleh Karyawan");
        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(1, 59, $rowData['name']); 

        $spreadsheet->getActiveSheet()
                ->setCellValue('B47', 'Dibayarkan Oleh')    
                ->setCellValue('B51', 'Payroll/ Accounting')    
                ->setCellValue('B55', 'Diterima Oleh Karyawan')    
                ->setCellValue('B59', $rowData['name']);    

        $basicSalary = $rowData['salary_basic'];
        $bsProrate = $rowData['bs_prorate'];
        if($bsProrate > $basicSalary)
        {
            $bsProrate = $basicSalary;   
        }

        $this->M_payroll_config->loadByClient($ptName);
        $salaryDividerConfig = $this->M_payroll_config->getSalaryDivider();
        $salaryHourly = $basicSalary / $salaryDividerConfig;
        // $salaryHourly = number_format($salaryHourly,2);
        $otMultiplierConfig01 = $this->M_payroll_config->getOt01Multiplier();
        // $otMultiplierConfig01 = number_format($otMultiplierConfig01,1);
        $otMultiplierConfig02 = $this->M_payroll_config->getOt02Multiplier();
        // $otMultiplierConfig02 = number_format($otMultiplierConfig02,1);
        $otMultiplierConfig03 = $this->M_payroll_config->getOt03Multiplier();
        // $otMultiplierConfig03 = number_format($otMultiplierConfig03,1);
        $otMultiplierConfig04 = $this->M_payroll_config->getOt04Multiplier(); 
        // $otMultiplierConfig04 = number_format($otMultiplierConfig04,1);

        $spreadsheet->getActiveSheet()->getStyle("K4:L4")->applyFromArray($left);
        $spreadsheet->getActiveSheet()->getStyle("K4:Q43")->getFont()->setSize(13);

        $spreadsheet->getActiveSheet()
            ->setCellValue('K4', 'NAMA')
            ->setCellValue('M4', $rowData['name'])
            ->setCellValue('N4', 'Id')
            ->setCellValue('O4', $nie)
            ->setCellValue('K5', 'JABATAN')
            ->setCellValue('M5', $rowData['position'])
            ->setCellValue('N5', 'DEPT ')
            ->setCellValue('O5', $dept)
            ->setCellValue('K6', 'STATUS')
            ->setCellValue('M6', $rowData['marital_status'])
            ->setCellValue('K7', 'GAJI POKOK')
            ->setCellValue('M7', $basicSalary)
            ->setCellValue('K8', 'RATE/HOUR')
            ->setCellValue('M8', $salaryHourly)
            ->setCellValue('K9', 'NPWP')
            ->setCellValue('M9', $rowData['npwp_no'])
            ->setCellValue('K11', 'UPAH')
            ->setCellValue('O11', $bsProrate)
            ->setCellValue('K12', 'OT 1')
            ->setCellValue('K13', 'OT 2')
            ->setCellValue('K14', 'OT 3')
            ->setCellValue('K15', 'OT 4')
            ;  

        $spreadsheet->getActiveSheet()->getStyle('M7:M8')->applyFromArray($totalStyle);
        $otTotal1 = $rowData['ot_1']; 
        $otTotal2 = $rowData['ot_2']; 
        $otTotal3 = $rowData['ot_3']; 
        $otTotal4 = $rowData['ot_4']; 

        $allOt = $otTotal1+$otTotal2+$otTotal3+$otTotal4; 

        $strOT1 = number_format($rowData['ot_count1'],1)." x ".number_format($otMultiplierConfig01,1)." x ".number_format($salaryHourly,2)." = Rp.";
        $strOT2 = number_format($rowData['ot_count2'],1)." x ".number_format($otMultiplierConfig02,1)." x ".number_format($salaryHourly,2)." = Rp.";
        $strOT3 = number_format($rowData['ot_count3'],1)." x ".number_format($otMultiplierConfig03,1)." x ".number_format($salaryHourly,2)." = Rp.";
        $strOT4 = number_format($rowData['ot_count4'],1)." x ".number_format($otMultiplierConfig04,1)." x ".number_format($salaryHourly,2)." = Rp.";
        
        $spreadsheet->getActiveSheet()
            ->setCellValue('M12', $strOT1)
            ->setCellValue('N12', $otTotal1)
            ->setCellValue('M13', $strOT2)
            ->setCellValue('N13', $otTotal2)
            ->setCellValue('M14', $strOT3)
            ->setCellValue('N14', $otTotal3)
            ->setCellValue('M15', $strOT4)
            ->setCellValue('N15', $otTotal4)
            ->setCellValue('K16', 'OT TOTAL')
            ->setCellValue('O16', $allOt);

        $spreadsheet->getActiveSheet()->getStyle('K16:O16')->applyFromArray($topBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle('K16:O16')->applyFromArray($totalStyle);

        $this->M_salary->loadByBiodataIdNie($biodataId, $nie);
        $isOtBonus = $this->M_salary->getIsOtBonus(); 
        $isShiftBonus = $this->M_salary->getIsShiftBonus();
        $isRemoteAllowance = $this->M_salary->getIsRemoteAllowance();
        $isDevIncentiveBonus = $this->M_salary->getIsDevIncentiveBonus();

        /* START BONUS TOTAL */
        $bonusTotal = 0; 

        $outCampBonus = $rowData['flying_camp'];
        $trvBonus = $rowData['travel_bonus'];
        $attendanceBonus = $rowData['attendance_bonus'];
        $otherAllowance1 = $rowData['other_allowance1'];
        $otherAllowance2 = $rowData['other_allowance2'];
        $bonusTotal = $outCampBonus + $trvBonus + $attendanceBonus + $otherAllowance1 + $otherAllowance2;   
        /* END BONUS, INCENTIVE */

        $bonusTotal += $rowData['thr'];
        $bonusTotal += $rowData['contract_bonus'];
        // $spreadsheet->getActiveSheet()->getStyle('K'.($bonusIdx+7).':O'.($bonusIdx+7))->applyFromArray($topBorderStyle);
        // $spreadsheet->getActiveSheet()->getStyle('K'.($bonusIdx+7).':O'.($bonusIdx+7))->applyFromArray($totalStyle);

        $bonusIdx = 18; 
        $spreadsheet->getActiveSheet()
            ->setCellValue('K'.$bonusIdx, 'Out Camp Bonus')
            ->setCellValue('N'.$bonusIdx, $outCampBonus)
            ->setCellValue('K'.($bonusIdx+1), 'Travel Bonus')
            ->setCellValue('N'.($bonusIdx+1), $trvBonus)
            ->setCellValue('K'.($bonusIdx+2), 'Attendance Bonus')
            ->setCellValue('N'.($bonusIdx+2), $attendanceBonus)
            // ->setCellValue('K'.($bonusIdx+3), 'KPI Benefits')
            // ->setCellValue('N'.($bonusIdx+3), $otherAllowance1)
            // ->setCellValue('K'.($bonusIdx+4), 'Other benefits')
            // ->setCellValue('N'.($bonusIdx+4),  $otherAllowance2)
            ->setCellValue('K'.($bonusIdx+3), 'THR')
            ->setCellValue('N'.($bonusIdx+3), $rowData['thr'])
            ->setCellValue('K'.($bonusIdx+4), 'Contract Bonus')
            ->setCellValue('N'.($bonusIdx+4), $rowData['contract_bonus'])
            ->setCellValue('K'.($bonusIdx+5), 'BONUS TOTAL')
            ->setCellValue('O'.($bonusIdx+5), $bonusTotal);

        $spreadsheet->getActiveSheet()->getStyle('K'.($bonusIdx+5).':O'.($bonusIdx+5))->applyFromArray($topBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle('K'.($bonusIdx+5).':O'.($bonusIdx+5))->applyFromArray($totalStyle);

        /* START GOVERNMENT REGULATION */
        $unpaidTotal = $rowData['unpaid_total'];
        $tUnpaidCount = $rowData['unpaid_count'];
        $subTotalVal = $rowData['jkk_jkm'] + $rowData['health_bpjs'] /*+ $rowData['jp']*/ - $unpaidTotal + $rowData['adjust_in'] - $rowData['adjust_out'];
        $govRegIdx = 27;
        $spreadsheet->getActiveSheet()
            ->setCellValue('K'.$govRegIdx, 'JKK & JKM(2.04%)')
            ->setCellValue('N'.$govRegIdx, $rowData['jkk_jkm'])
            ->setCellValue('K'.($govRegIdx+1), 'Iuran BPJS Kesehatan(4%)')
            ->setCellValue('N'.($govRegIdx+1), $rowData['health_bpjs'])
            ->setCellValue('K'.($govRegIdx+2), 'Potongan Absensi '.$tUnpaidCount.' Hari')
            ->setCellValue('N'.($govRegIdx+2), $unpaidTotal)
            ->setCellValue('K'.($govRegIdx+3), 'Adjustment In')
            ->setCellValue('N'.($govRegIdx+3), $rowData['adjust_in'])
            ->setCellValue('K'.($govRegIdx+4), 'Adjustment Out')
            ->setCellValue('N'.($govRegIdx+4), $rowData['adjust_out'])
            ->setCellValue('K'.($govRegIdx+5), 'Subtotal')
            ->setCellValue('O'.($govRegIdx+5), $subTotalVal);

        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(10, $govRegIdx+5, "Subtotal");
        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(14, $govRegIdx+5, $subTotalVal);
        $spreadsheet->getActiveSheet()->getStyle('K'.($govRegIdx+5).':O'.($govRegIdx+5))->applyFromArray($totalStyle);

        $brutto = $bsProrate + $allOt + $bonusTotal + $rowData['jkk_jkm'] + $rowData['health_bpjs'] /*+ $rowData['jp']*/ - $unpaidTotal + $rowData['adjust_in'] - $rowData['adjust_out'];
        $bruttoCoordinate = $govRegIdx+5;
        $spreadsheet->getActiveSheet()
            ->setCellValue('K'.$bruttoCoordinate, 'TOTAL KOTOR')
            ->setCellValue('O'.$bruttoCoordinate, $brutto);

        $spreadsheet->getActiveSheet()->getStyle('K'.$bruttoCoordinate.':O'.$bruttoCoordinate)->applyFromArray($topBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle('K'.$bruttoCoordinate.':O'.$bruttoCoordinate)->applyFromArray($totalStyle);

        $taxCoordinate = 47; /* TAX ROWS COORDINATE  */
        $tmpCoordinate1 = $taxCoordinate+17;
        $tmpCoordinate2 = 0;
        $spreadsheet->getActiveSheet()->getStyle("K".$taxCoordinate.":O".($tmpCoordinate1-1))->applyFromArray($outlineBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle('K'.$taxCoordinate.':O'.($taxCoordinate))->applyFromArray($totalStyle);
        $tmpCoordinate1 = $taxCoordinate + 19;
        $spreadsheet->getActiveSheet()->getStyle("K".$taxCoordinate.":O".$tmpCoordinate1)->getFont()->setSize(13);

        $myConfigId = 1;  
        $row = $this->M_payroll_config->getObjectById($myConfigId);       
        $maxNonTax = $this->M_payroll_config->getNonTaxAllowance(); /* Max Tunjangan Jabatan */

        $nonTaxAllowance = $brutto * (5/100);
        if($nonTaxAllowance > $maxNonTax){
           $nonTaxAllowance = $maxNonTax; 
        }
        
        $spreadsheet->getActiveSheet()
            ->setCellValue('K'.$taxCoordinate, 'PERHITUNGAN PAJAK PENGHASILAN')
            ->setCellValue('K'.($taxCoordinate+1), 'Penghasilan Kotor')
            ->setCellValue('O'.($taxCoordinate+1), $brutto)
            ->setCellValue('K'.($taxCoordinate+2), 'Iuran JP TK(1%)')
            ->setCellValue('O'.($taxCoordinate+2), $rowData['emp_jp'])
            ->setCellValue('K'.($taxCoordinate+3), 'Iuran JHT')
            ->setCellValue('O'.($taxCoordinate+3), $rowData['emp_jht'])            
            ->setCellValue('K'.($taxCoordinate+4), 'Tunjangan Jabatan')
            ->setCellValue('O'.($taxCoordinate+4), $nonTaxAllowance);

        /* START NETTO */
        $netto = $brutto - $rowData['emp_jht'] - $rowData['emp_jp'] - $nonTaxAllowance;


        /* START TAX PROCESS */
        $ptkpTotal = $rowData['ptkp_total'];
        /* Perhitungan pajak yang disetahunkan, hanya penghasilan tetap saja. Tunjangan tidak tetap seperti THR dihitung terpisah */
        $nettoSetahun = ( ($netto - $rowData['thr']-$rowData['contract_bonus']) * 12) + $rowData['thr'] + $rowData['contract_bonus'];
        $penghasilanPajak = 0;
        if($nettoSetahun >= $ptkpTotal)
        {
            $penghasilanPajak = $nettoSetahun - $ptkpTotal;
        }
        $pembulatanPenghasilan = floor($penghasilanPajak);
        
        $nettoCoordinate = 53;
        $spreadsheet->getActiveSheet()->getStyle('K'.$nettoCoordinate.':O'.($nettoCoordinate))->applyFromArray($totalStyle);
        $spreadsheet->getActiveSheet()->getStyle('K'.$nettoCoordinate.':O'.($nettoCoordinate))->applyFromArray($topBorderStyle);
        $spreadsheet->getActiveSheet()
            ->setCellValue('K'.$nettoCoordinate, 'Netto')
            ->setCellValue('O'.$nettoCoordinate, $netto);
        /* END NETTO */
        /*if($brutto <= 0)
        {
            $brutto = 0;    
        }*/ 
        $yearNettoCoordinate = 54;
        $spreadsheet->getActiveSheet()->getStyle('K'.($yearNettoCoordinate+2).':O'.($yearNettoCoordinate+2))->applyFromArray($topBorderStyle);
        $spreadsheet->getActiveSheet()
            ->setCellValue('K'.$yearNettoCoordinate, 'Netto Disetahunkan(Tetap)')
            ->setCellValue('O'.$yearNettoCoordinate, $nettoSetahun)
            ->setCellValue('K'.($yearNettoCoordinate+1), 'PTKP '.$rowData['marital_status'])
            ->setCellValue('O'.($yearNettoCoordinate+1), $ptkpTotal)
            ->setCellValue('K'.($yearNettoCoordinate+2), 'PTKP '.$rowData['marital_status'])
            ->setCellValue('O'.($yearNettoCoordinate+2), $penghasilanPajak)
            ->setCellValue('K'.($yearNettoCoordinate+3), 'Pembulatan')
            ->setCellValue('O'.($yearNettoCoordinate+3), $pembulatanPenghasilan);


        $taxPercent1 = $this->M_payroll_config->getTaxPercent1(); /* Persentase Progresif Pajak I */
        $taxPercent2 = $this->M_payroll_config->getTaxPercent2(); /* Persentase Progresif Pajak II */
        $taxPercent3 = $this->M_payroll_config->getTaxPercent3(); /* Persentase Progresif Pajak III */
        $taxPercent4 = $this->M_payroll_config->getTaxPercent4(); /* Persentase Progresif Pajak IV */
        $maxTaxVal1 = $this->M_payroll_config->getTaxMaxVal1(); /* Nominal Pajak I */ 
        $maxTaxVal2 = $this->M_payroll_config->getTaxMaxVal2(); /* Nominal Pajak II */
        $maxTaxVal3 = $this->M_payroll_config->getTaxMaxVal3(); /* Nominal Pajak III */
        $maxTaxVal4 = $this->M_payroll_config->getTaxMaxVal4(); /* Nominal Pajak IV */
        $taxVal1 = 0;    
        $taxVal2 = 0;    
        $taxVal3 = 0;    
        $taxVal4 = 0;

        $tVal = 0;
        $tSisa = 0;
        if($pembulatanPenghasilan > 0)
        {
            /* TAX 1 */
            if($maxTaxVal1 > 0)
            {
                $tVal = $pembulatanPenghasilan/$maxTaxVal1;  
                if($tVal >= 1){
                    $taxVal1 = $maxTaxVal1 * ($taxPercent1/100);
                }
                else{
                    $taxVal1 = $pembulatanPenghasilan * ($taxPercent1/100);
                }    
            }    
            
            /* TAX 2 */
            if($maxTaxVal2 > 0)
            {
                if($pembulatanPenghasilan > $maxTaxVal1)
                {
                    $tSisa = $pembulatanPenghasilan - $maxTaxVal1; 
                    $tVal = $tSisa/$maxTaxVal2;
                    if($tVal >= 1){
                        $taxVal2 = $maxTaxVal2 * ($taxPercent2/100);
                    }
                    else{
                        $taxVal2 = $tSisa * ($taxPercent2/100);
                    }
                }     
            }
             
            /* TAX 3 */
            if($maxTaxVal3 > 0)
            {
                if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2))
                {
                    $tSisa = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2;
                    $tVal = $tSisa/$maxTaxVal3;
                    if($tVal >= 1){
                        $taxVal3 = $maxTaxVal3 * ($taxPercent3/100);
                    }
                    else{
                        $taxVal3 = $tSisa * ($taxPercent3/100);
                    }
                }    
            }
             
            /* TAX 4 */
            if($pembulatanPenghasilan > ($maxTaxVal1 + $maxTaxVal2 + $maxTaxVal3))
            {
                $tSisa = $pembulatanPenghasilan - $maxTaxVal1 - $maxTaxVal2 - $maxTaxVal3;
                $taxVal4 = $tSisa * ($taxPercent4/100); 
            }               
        }
        
        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, 63, $line);
        $pajakTerutang = $taxVal1 + $taxVal2 + $taxVal3 + $taxVal4;

        /* Additional Charge if there is no NPWP */
        if( strlen($npwpNo) < 19 ){
            $pajakTerutang = $pajakTerutang + ($pajakTerutang * ($npwpCharge/100) ); 
        }

        $tarifTaxCoordinate = $taxCoordinate + 10; /* TAX ROWS COORDINATE  */        
        $pajakSebulan = ($pajakTerutang/12);
        $pajakSebulan = floor($pajakSebulan);
        $spreadsheet->getActiveSheet()->getStyle('K'.($tarifTaxCoordinate+5).':O'.($tarifTaxCoordinate+6))->applyFromArray($totalStyle);
        $spreadsheet->getActiveSheet()->getStyle('K'.($tarifTaxCoordinate+5).':O'.($tarifTaxCoordinate+5))->applyFromArray($topBorderStyle);
        // $tarifTaxCoordinate = $taxCoordinate + 12; /* TAX ROWS COORDINATE  */
        $spreadsheet->getActiveSheet()
            ->setCellValue('K'.($tarifTaxCoordinate+1), 'Tarif Pajak I    '.$taxPercent1.'%')
            ->setCellValue('O'.($tarifTaxCoordinate+1), $taxVal1)
            ->setCellValue('K'.($tarifTaxCoordinate+2), 'Tarif Pajak II    '.$taxPercent2.'%')
            ->setCellValue('O'.($tarifTaxCoordinate+2), $taxVal2)
            ->setCellValue('K'.($tarifTaxCoordinate+3), 'Tarif Pajak III    '.$taxPercent3.'%')
            ->setCellValue('O'.($tarifTaxCoordinate+3), $taxVal3)
            ->setCellValue('K'.($tarifTaxCoordinate+4), 'Tarif Pajak IV    '.$taxPercent4.'%')
            ->setCellValue('O'.($tarifTaxCoordinate+4), $taxVal4)
            ->setCellValue('K'.($tarifTaxCoordinate+5), 'Pajak Gaji Setahun')
            ->setCellValue('O'.($tarifTaxCoordinate+5), $pajakTerutang)
            ->setCellValue('K'.($tarifTaxCoordinate+6), 'Pajak Gaji Sebulan')
            ->setCellValue('O'.($tarifTaxCoordinate+6), $pajakSebulan);

        /* START UPDATE TAX VALUE TO THE TABLE */            
        $str = "UPDATE trn_salary_slip SET tax_value = ".$pajakSebulan.", non_tax_allowance=".$nonTaxAllowance." WHERE salary_slip_id = '".$slipId."' ";
        $this->db->query($str);
        /* END UPDATE TAX VALUE TO THE TABLE */

        /* START OUT */
        $terima = $brutto-floor($pajakSebulan)-$rowData['jkk_jkm']-$rowData['emp_jht']-$rowData['emp_health_bpjs']-$rowData['emp_jp']-$rowData['health_bpjs'];
        $outCoordinate = 35;
        $spreadsheet->getActiveSheet()
            ->setCellValue('K'.$outCoordinate, 'Pajak Penghasilan')
            ->setCellValue('O'.$outCoordinate, 0)
            ->setCellValue('K'.($outCoordinate+1), 'JKK & JKM(2.04%)')
            ->setCellValue('O'.($outCoordinate+1), 0)
            ->setCellValue('K'.($outCoordinate+2), 'Iuran JHT BPJS TK(2%)')
            ->setCellValue('O'.($outCoordinate+2), 0)
            ->setCellValue('K'.($outCoordinate+3), 'Iuran BPJS Kesehatan TK(1%)')
            ->setCellValue('O'.($outCoordinate+3), 0)
            ->setCellValue('K'.($outCoordinate+4), 'Iuran BPJS Kesehatan(4%)')
            ->setCellValue('O'.($outCoordinate+4), 0)
            ->setCellValue('K'.($outCoordinate+5), 'Iuran JP TK(1%)')
            ->setCellValue('O'.($outCoordinate+5), 0);

        $pajakSebulan = floor($pajakSebulan);

        if($pajakSebulan > 0) {
            $spreadsheet->getActiveSheet()
             ->setCellValue('O'.$outCoordinate, -$pajakSebulan);
        }

        if($rowData['jkk_jkm'] > 0) {
            $spreadsheet->getActiveSheet()
             ->setCellValue('O'.($outCoordinate+1), -$rowData['jkk_jkm']);
        } 

        if($rowData['emp_jht'] > 0) {
            $spreadsheet->getActiveSheet()
             ->setCellValue('O'.($outCoordinate+2), -$rowData['emp_jht']);
        } 

        if($rowData['emp_health_bpjs'] > 0) {
            $spreadsheet->getActiveSheet()
             ->setCellValue('O'.($outCoordinate+3), -$rowData['emp_health_bpjs']);
        } 

        if($rowData['health_bpjs'] > 0) {
            $spreadsheet->getActiveSheet()
             ->setCellValue('O'.($outCoordinate+4), -$rowData['health_bpjs']);
        } 

        if($rowData['emp_jp'] > 0) {
            $spreadsheet->getActiveSheet()
             ->setCellValue('O'.($outCoordinate+5), -$rowData['emp_jp']);
        } 
        
        $outCoordinate2 = $outCoordinate + 6;
        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $outCoordinate2, "TOTAL SEBELUM POTONGAN");
        $spreadsheet->getActiveSheet()->getStyle('K'.$outCoordinate2.':O'.$outCoordinate2)->applyFromArray($topBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle('K'.$outCoordinate2.':O'.$outCoordinate2)->applyFromArray($totalStyle);
        // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(14, $outCoordinate2, $terima);

        $debtText = "";
        if($rowData['debt_explanation'] != "") {
            $debtText = "(".$rowData['debt_explanation'].")";
        }
        $tBurden = $rowData['debt_burden'];
        $tBurden = $tBurden * (-1);

        $cutCoordinate = 42;
        $spreadsheet->getActiveSheet()
            ->setCellValue('K'.$outCoordinate2, 'Pajak Penghasilan')
            ->setCellValue('O'.$outCoordinate2, $terima)
            ->setCellValue('K'.($cutCoordinate+1), 'THR')
            ->setCellValue('O'.($cutCoordinate+1), 0)
            ->setCellValue('K'.($cutCoordinate+2), 'Contract Bonus')
            ->setCellValue('O'.($cutCoordinate+2), 0)
            ->setCellValue('K'.($cutCoordinate+3), 'Beban Hutang')
            ->setCellValue('M'.($cutCoordinate+3), $debtText)
            ->setCellValue('O'.($cutCoordinate+3), $tBurden);

        /* START POTONGAN */
        if($rowData['thr'] > 0) {
            $spreadsheet->getActiveSheet()
            ->setCellValue('O'.($cutCoordinate+1), -$rowData['thr']);
        } 

        if($rowData['contract_bonus'] > 0) {
            $spreadsheet->getActiveSheet()
              ->setCellValue('O'.($cutCoordinate+2), -$rowData['contract_bonus']);
        }        
        /* END POTONGAN */
        
        $cutCoordinate2 = $cutCoordinate+3;
        $cutCoordinate3 = $cutCoordinate+4;
        $spreadsheet->getActiveSheet()->getStyle("K".$cutCoordinate.":O".$cutCoordinate3)->getFont()->setSize(13);
        $spreadsheet->getActiveSheet()->getStyle('K'.$cutCoordinate3.':O'.$cutCoordinate3)->applyFromArray($totalStyle);
        $spreadsheet->getActiveSheet()->getStyle('K'.$cutCoordinate3.':O'.$cutCoordinate3)->applyFromArray($topBorderStyle);

        $thp = $terima - $rowData['thr'] - $rowData['contract_bonus'] - $rowData['debt_burden']; 
        $spreadsheet->getActiveSheet()
              ->setCellValue('K'.($cutCoordinate2+1), 'TOTAL TERIMA')
              ->setCellValue('O'.($cutCoordinate2+1), $thp);
        /* END OUT */

        $spreadsheet->getActiveSheet()
          ->getStyle('K7:O63')->getNumberFormat()
          ->setFormatCode('#,##0.00');

        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('Report Excel '.date('d-m-Y H'));

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        //Nama File
        $str = $rowData['name'].$rowData['bio_rec_id'];
        $fileName = preg_replace('/\s+/', '', $str);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.Xlsx"');
        // header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        /* BY COMPOSER */
        // $writer = new Xlsx($spreadsheet);
        /* OFFLINE/ BY COPY EXCEL FOLDER  */
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit(0);

    }

    public function getSlipByIid(){
        $slipId = "";
        if(isset($_POST['slipId']))
        {
            $slipId = $_POST['slipId'];
        }

        $this->db->select('*');
        $this->db->from($this->db->database.'.trn_salary_slip');
        $this->db->where('salary_slip_id', $slipId);
        $query = $this->db->get()->row_array();
        $myData = array();
        $myData[0] = $query['salary_slip_id']; 
        $myData[1] = $query['name'];
        $myData[2] = $query['production_bonus'];
        $myData[3] = $query['workday_adj'];
        $myData[4] = $query['adjust_in'];
        $myData[5] = $query['adjust_out'];
        $myData[6] = $query['contract_bonus'];
        $myData[7] = $query['flying_camp'];
        $myData[8] = $query['safety_bonus'];
        $myData[9] = $query['attendance_bonus'];
        $myData[10] = $query['other_allowance1'];
        $myData[11] = $query['other_allowance2'];
        $myData[12] = $query['no_accident_bonus'];
        $myData[13] = $query['cc_payment'];
        $myData[14] = $query['thr'];
        $myData[15] = $query['debt_burden'];
        $myData[16] = $query['debt_explanation'];         
        $myData[17] = $query['general_1']; /*MOBILIZATION*/         
        $myData[18] = $query['general_2']; /*SERAGAM*/         
        $myData[19] = $query['general_3']; /*MCU*/        
        $myData[20] = $query['general_4']; /*Ticket*/        
        $myData[21] = $query['general_5']; /*APD*/        
        $myData[22] = $query['general_6']; /*Meals*/        
        $myData[23] = $query['dept'];         
        $myData[24] = $query['position'];         
        $myData[25] = $query['travel_bonus'];         
        echo json_encode($myData);   
    }  

    public function updatePayroll()
    {
        $clientName = "";
        $payrollId = "";
        $productionBonus = 0;
        $workAdjustment = 0;
        $adjustmentIn = 0;
        $adjustmentOut = 0;
        $contractBonus = 0;
        $outCamp = 0;
        $safetyBonus = 0;
        $attendanceBonus = 0;
        $otherAllowance1 = 0;
        $otherAllowance2 = 0;
        $oAllowanceTra1 = 0;
        $oAllowanceTra2 = 0;
        $travelBonus = 0;
        $general1 = 0;
        $general2 = 0;
        $general3 = 0;
        $general4 = 0;
        $general5 = 0;
        $general6 = 0;
        $noAccidentFee = 0;
        $ccPayment = 0;
        $thr = 0;
        $debtBurden = 0;
        $debtExplanation = '';
        
        if(isset($_POST['clientName']))
        {
            $clientName = $this->security->xss_clean($_POST['clientName']);            
        }
        if(isset($_POST['payrollId']))
        {
            $payrollId = $this->security->xss_clean($_POST['payrollId']);            
        }
        if(isset($_POST['productionBonus']))
        {
            $productionBonus = $this->security->xss_clean($_POST['productionBonus']);            
        }
        if(isset($_POST['workAdjustment']))
        {
            $workAdjustment = $this->security->xss_clean($_POST['workAdjustment']);            
        }

        if(isset($_POST['adjustmentIn']))
        {
            $adjustmentIn = $this->security->xss_clean($_POST['adjustmentIn']);            
        }

        if(isset($_POST['adjustmentOut']))
        {
            $adjustmentOut = $this->security->xss_clean($_POST['adjustmentOut']);            
        }

        if(isset($_POST['contractBonus']))
        {
            $contractBonus = $this->security->xss_clean($_POST['contractBonus']);            
        }

        if(isset($_POST['outCamp']))
        {
            $outCamp = $this->security->xss_clean($_POST['outCamp']);            
        }

        if(isset($_POST['safetyBonus']))
        {
            $safetyBonus = $this->security->xss_clean($_POST['safetyBonus']);            
        }

        if(isset($_POST['attendanceBonus']))
        {
            $attendanceBonus = $this->security->xss_clean($_POST['attendanceBonus']);            
        }


        if($clientName == "AMNT_Sumbawa") {

            if(isset($_POST['otherAllowance1']))
            {
                $otherAllowance1 = $this->security->xss_clean($_POST['otherAllowance1']);            
            }

            if(isset($_POST['otherAllowance2']))
            {
                $otherAllowance2 = $this->security->xss_clean($_POST['otherAllowance2']);            
            }

        }
        if($clientName == 'Machmahon_Sumbawa') {

            if(isset($_POST['kpiBenefits']))
            {
                $otherAllowance1 = $this->security->xss_clean($_POST['kpiBenefits']);            
            }

            if(isset($_POST['otherAllowance2']))
            {
                $otherAllowance2 = $this->security->xss_clean($_POST['otherAllowance2']);            
            }

        }
        else if($clientName == "Trakindo_Sumbawa") {
            if(isset($_POST['oAllowanceTra1']))
            {
                $otherAllowance1 = $this->security->xss_clean($_POST['oAllowanceTra1']);            
            }

            if(isset($_POST['oAllowanceTra2']))
            {
                $otherAllowance2 = $this->security->xss_clean($_POST['oAllowanceTra2']);            
            }
        }

        


        if(isset($_POST['general1']))
        {
            $general1 = $this->security->xss_clean($_POST['general1']);            
        }
        if(isset($_POST['general2']))
        {
            $general2 = $this->security->xss_clean($_POST['general2']);            
        }
        if(isset($_POST['general3']))
        {
            $general3 = $this->security->xss_clean($_POST['general3']);            
        }
        if(isset($_POST['general4']))
        {
            $general4 = $this->security->xss_clean($_POST['general4']);            
        }
        if(isset($_POST['general5']))
        {
            $general5 = $this->security->xss_clean($_POST['general5']);            
        }
        if(isset($_POST['general6']))
        {
            $general6 = $this->security->xss_clean($_POST['general6']);            
        }

        
        if(isset($_POST['noAccidentFee']))
        {
            $noAccidentFee = $this->security->xss_clean($_POST['noAccidentFee']);            
        }
        if(isset($_POST['ccPayment']))
        {
            $ccPayment = $this->security->xss_clean($_POST['ccPayment']);            
        }

        if(isset($_POST['thr']))
        {
            $thr = $this->security->xss_clean($_POST['thr']);            
        }

        if(isset($_POST['debtBurden']))
        {
            $debtBurden = $this->security->xss_clean($_POST['debtBurden']);            
        }

        if(isset($_POST['debtExplanation']))
        {
            $debtExplanation = $this->security->xss_clean($_POST['debtExplanation']);            
        }

        // $this->load->model('M_salary_slip');
        $rs = $this->M_salary_slip->getObjectById($payrollId);
        if($clientName == 'Pontil_Sumbawa') {
            if(isset($_POST['travelBonus'])) {
                $travelBonus = $this->security->xss_clean($_POST['travelBonus']);
                $this->M_salary_slip->setTravelBonus($travelBonus);
            }
        }
        $this->M_salary_slip->setProductionBonus($productionBonus);
        $this->M_salary_slip->setWorkdayAdj($workAdjustment);
        $this->M_salary_slip->setAdjustIn($adjustmentIn);
        $this->M_salary_slip->setAdjustOut($adjustmentOut);
        $this->M_salary_slip->setContractBonus($contractBonus);
        $this->M_salary_slip->setFlyingCamp($outCamp);
        $this->M_salary_slip->setSafetyBonus($safetyBonus);
        $this->M_salary_slip->setAttendanceBonus($attendanceBonus);
        $this->M_salary_slip->setOtherAllowance1($otherAllowance1);
        $this->M_salary_slip->setOtherAllowance2($otherAllowance2);

        $this->M_salary_slip->setGeneral1($general1);/*Mobilization*/
        $this->M_salary_slip->setGeneral2($general2);/*Uniform*/
        $this->M_salary_slip->setGeneral3($general3);/*MCU*/
        $this->M_salary_slip->setGeneral4($general4);/*Ticket*/
        $this->M_salary_slip->setGeneral5($general5);/*APD*/
        $this->M_salary_slip->setGeneral6($general6);/*Travel*/
        
        $this->M_salary_slip->setNoAccidentBonus($noAccidentFee);
        $this->M_salary_slip->setCcPayment($ccPayment);
        $this->M_salary_slip->setThr($thr);
        $this->M_salary_slip->setDebtBurden($debtBurden);
        $this->M_salary_slip->setDebtExplanation($debtExplanation);
        $this->M_salary_slip->update($payrollId);
        // echo $payrollId;
    } 

    
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

      private $my_db = 'db_admin';

    	public function __construct(){
    		parent::__construct();
        $this->load->model('M_user');
    		$this->load->model('M_mstusermenu');
    	}

    	public function index()
    	{
    		
    	}	

    	public function getUsers()
    	{
    		$strQuery = 'SELECT * FROM '.$this->my_db.'.mst_user ';
    		$rs = $this->db->query($strQuery)->result_array();
    		$myData = array();
    		foreach ($rs as $row) {
    			$myData[] = array(
                  $row['user_id'], 
                  $row['user_password'], 
                  $row['user_group'],
                  $row['is_active']
                ); 
    		}
    		echo json_encode($myData);
    	}

      public function getUserRow()
      {
        $strQuery = 'SELECT * FROM '.$this->my_db.'.mst_user ';
        $rs = $this->db->query($strQuery)->result_array();
        return $rs;
        /*$myData = array();
        foreach ($rs as $row) {
          $myData[] = array(
                  $row['user_id'], 
                  $row['user_password'], 
                  $row['user_group'],
                  $row['is_active']
                ); 
        }
        echo json_encode($myData);*/
      } 	

	    /* Start of insert execute */
      public function Ins() /* Execute Insert To Database  */
      {
         
         if(isset($_POST['dstate']) && $_POST['dstate'] == 'Add')
         {

           $this->M_user->setUserId($this->security->xss_clean($_POST['userId']));
           $this->M_user->setUserPassword($this->security->xss_clean($_POST['password']));
           // $this->M_user->setUserGroup($this->security->xss_clean($_POST['storeId']));
           // $this->M_user->setStoreCode($this->security->xss_clean($_POST['store_code']));
           $this->M_user->setIsActive($this->security->xss_clean($_POST['isActive']));
           // echo "Helo"; exit(0); 
           $this->M_user->insert();
         }
      }
      /* End of insert execute */

      /* Start of update execute */
      public function Upd() /* Execute Update To Database  */
      {
      	 if(isset($_POST['dstate']) && $_POST['dstate'] == 'Edit')
         {
         	$idUpdate = $this->security->xss_clean($_POST['idUpdate']);
            // $this->M_user->setUserId($this->security->xss_clean($_POST['userId']));
            $this->M_user->setUserPassword($this->security->xss_clean($_POST['password']));
            $this->M_user->setUserGroup($this->security->xss_clean($_POST['userGroup']));
            // $this->M_user->setStoreCode($this->security->xss_clean($_POST['store_code']));
            $this->M_user->setIsActive($this->security->xss_clean($_POST['isActive']));
            $this->M_user->update($idUpdate);
         }
      }
      /* End of update execute */

      /* Start of delete execute */
      public function Del() /* Execute Delete To Database */
      {
      	 $id = $this->security->xss_clean($_POST['idDelete']);
         $this->M_user->delete($id);
      }
      /* End of delete execute */

      public function getMenusByUser(){
          $userId = '';
          if(isset($_POST['userId'])){
            $userId = $this->security->xss_clean($_POST['userId']);
          } 
          $strSql = 'SELECT mm.menu_id,mm.app_code,mm.menu_code,mm.menu_name,';
          $strSql .= 'CASE WHEN ISNULL(um.user_menu_id) THEN "No" ELSE "Yes" END AS is_choosen ';
          $strSql .= 'FROM '.$this->my_db.'.mst_menu mm ';
          $strSql .= 'LEFT JOIN '.$this->my_db.'.mst_user_menu um ';
          $strSql .= 'ON mm.menu_id = um.menu_id AND um.user_id="'.$userId.'" ';
          $strSql .= 'WHERE mm.app_code = "NEW HRIS" ';
          $rs = $this->db->query($strSql)->result_array();
          // return $rs;
          $myArray = array();
          foreach ($rs as $row) {
            $myArray[] = array(
              $row['menu_id'],
              $row['app_code'],
              $row['menu_code'],
              $row['menu_name'],
              $row['is_choosen']
            );
          }
          echo json_encode($myArray);
      } 

	   public function userMenuInsert(){
        $muId = $this->M_mstusermenu->GenerateNumber();
        $userId = null;
        $menuId = null;
        if(isset($_POST['userId'])){
           $userId = $this->security->xss_clean($_POST['userId']);
        } 
        if(isset($_POST['menuId'])){
           $menuId = $this->security->xss_clean($_POST['menuId']);
        }
        $this->M_mstusermenu->resetValues();
        $this->M_mstusermenu->setUserMenuId($muId);
        $this->M_mstusermenu->setUserId($userId);
        $this->M_mstusermenu->setMenuId($menuId);
        $this->M_mstusermenu->insert();
     }

     public function userMenuDelete(){
        $userId = null;
        $menuId = null;
        if(isset($_POST['userId'])){
           $userId = $this->security->xss_clean($_POST['userId']);
        } 
        if(isset($_POST['menuId'])){
           $menuId = $this->security->xss_clean($_POST['menuId']);
        }
        $this->M_mstusermenu->deleteByUserMenu($userId, $menuId);
     }

     public function getUserById()
     {
        $userId = '';
        $oldPassword = '';
        if(isset($_POST['userId'])){
          $userId = $this->security->xss_clean($_POST['userId']);
        }
        if(isset($_POST['oldPassword'])){
          $oldPassword = $this->security->xss_clean($_POST['oldPassword']);
        }
        $strQuery  = 'SELECT * FROM '.$this->my_db.'.mst_user ';
        $strQuery .= 'WHERE user_id = "'.$userId.'" ';
        $strQuery .= 'AND user_password = "'.$oldPassword.'"';
        $row = $this->db->query($strQuery)->row_array();
        echo $row['user_id'];        
     } 

     public function updatePassword()
     {
        $userId = '';
        $oldPassword = '';
        $newPassword = ''; 
        if(isset($_POST['userId'])){
          $userId = $this->security->xss_clean($_POST['userId']);
        }
        if(isset($_POST['oldPassword'])){
          $oldPassword = $this->security->xss_clean($_POST['oldPassword']);
        }

        if(isset($_POST['newPassword'])){
          $newPassword = $this->security->xss_clean($_POST['newPassword']);
        }

        $strQuery  = 'UPDATE '.$this->my_db.'.mst_user SET user_password = "'.$newPassword.'" ';
        $strQuery .= 'WHERE user_id = "'.$userId.'" ';
        $strQuery .= 'AND user_password = "'.$oldPassword.'"';
        // echo $strQuery;
        $this->db->query($strQuery);
     } 
}

<!-- START FORM DATA -->
<div class="col-md-12">
  <div class="tile bg-info">
    <h3 class="tile-title">User Menu</h3>
    <div class="tile-body">
      <form class="row bg-info">       


        <div class="form-group col-sm-12 col-md-6">
          <label class="control-label">User Id</label>
          <!-- <code id="storeIdErr" class="errMsg"><span> : Required</span></code> -->
          <select class="form-control" id="userId">
            <option disabled="" selected="" value="">Pilih</option>
        <?php 
          $userList = $user_list; 

          foreach ($userList as $row) {
            $selected = '';
            echo '<option value="'.$row['user_id'].'" '.$selected.'>'.$row['user_id'].'</option>'; 
          }
        ?>               
          </select>
        </div>
       

        <!-- <div class="form-group col-sm-12">
          <input class="myUsersInput" type="checkbox" id="isActive" checked=""> &nbsp; Aktif
        </div> -->
       
        <div class="form-group col-md-4 align-self-end">
          <!-- <button class="btn btn-warning" type="button" id="btnAdd"><i class="fa fa-fw fa-lg fa fa-edit"></i>Tambah</button> -->
          <!-- <button class="btn btn-warning myUsersInput" type="button" id="btnSave"><i class="fa fa-fw fa-lg fa-check-circle"></i>Simpan</button> -->
          <!-- <input class="form-control" type="hidden" id="storesId" name="storesId" placeholder="Id Toko"> -->
          <input class="form-control" type="hidden" id="dstate" name="dstate" value="" placeholder="Status">
        </div>

      </form>
    </div>
  </div>
</div>
<!-- END FORM DATA -->

<!-- START DATA TABLE -->
<div class="col-md-12">
  <div class="tile">
    <h3 class="tile-title">Menu List</h3>
    <div class="tile-body">
      <table class="table table-hover table-bordered" id="usermenuTable">
        <thead class="thead-dark">
          <tr>
            <th>Id Menu</th>
            <th>App Code</th>
            <th>Menu Code</th>
            <th>Menu Name</th>
            <th>Access</th>
            <th>Choose</th>
          </tr>
        </thead>
        <tbody>
          
        </tbody>
      </table>
    </div>
  </div>
</div>
<!-- END DATA TABLE
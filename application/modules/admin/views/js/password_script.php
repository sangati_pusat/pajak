<script type="text/javascript">
	$(document).ready(function() {
		var isValid = false; 
		var userIdRs = ''; 
		$('.errMsg').hide();
		$('#btnSave').attr('disabled', true);

		// $("#password").blur(function(){
		// 	alert('Blur');
		// });

		$('#password').on('change', function(){
			var myUrl = "<?php echo base_url() ?>admin/Users/getUserById"; 
			var userId = $('#userId').val();
			var oldPassword = $('#password').val();
			var newPassword = $('#newPassword').val();
			var confirmPassword = $('#confirmPassword').val();
			// alert(myUrl);
			$.ajax({
				method : "POST",
				url : myUrl,
				data : {
					userId : userId,
					oldPassword : oldPassword
				},
				success : function(data){
					userIdRs = data;
					// alert(userIdRs);
					if(userIdRs == ''){
						$('#password').focus();
						$('#password').select();
						alert('Password lama salah');
						// return false;
					}
					
					if( (userIdRs != '') && (newPassword != '') && (newPassword == confirmPassword) ){
						isValid = true;
					  	$('#btnSave').attr('disabled', false);
					}else{
						isValid = false;
					  	$('#btnSave').attr('disabled', true);
					}
				},
				error : function(data){
					alert('Failed');
				}
			});
		});

		$('#newPassword').on('change', function(){
			var newPassword = $('#newPassword').val();
			var confirmPassword = $('#confirmPassword').val();	
			if( (userIdRs != '') && (newPassword != '') && (newPassword == confirmPassword) ){
			  isValid = true;	
			  $('#btnSave').attr('disabled', false);	
			}else{
			  isValid = false;
			  $('#btnSave').attr('disabled', true);	
			}

		});

		$('#confirmPassword').on('change', function(){
			var newPassword = $('#newPassword').val();
			var confirmPassword = $('#confirmPassword').val();	
			if( (userIdRs != '') && (confirmPassword != '') && (newPassword == confirmPassword) ){
			  // isConfPassValid = true;	
			  isValid = true;
			  $('#btnSave').attr('disabled', false);	
			}else{
			  isValid = false;	
			  $('#btnSave').attr('disabled', true);	
			}

		});

		$('#btnSave').on('click', function(){
			var myUrl = "<?php echo base_url() ?>admin/Users/updatePassword";
			var userId = $('#userId').val();
			var oldPassword = $('#password').val();
			var newPassword = $('#newPassword').val();
			$.ajax({
				method : "POST",
				url : myUrl,
				data : {
					userId : userId,
					oldPassword : oldPassword,
					newPassword : newPassword
				},
				success : function(data){
					alert('Password sudah diupdate');
				},
				error : function(data){
					alert('Failed');
				}
			});
		});

	});
</script>
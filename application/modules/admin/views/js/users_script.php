<script type="text/javascript">
	$(document).ready(function() {
		/* START INCOMING ITEMS TABLE */	
		var usersTable = $("#usersTable").DataTable({
	            "paging":   true,
	            "ordering": false,
	            "info":     false,
	            "filter":   true,	

	            "columnDefs": [
		            {
		                "targets": -2,
		                "data": null,
		                "defaultContent": "<button class='btn btn-warning btn-sm btnEdit' type='button'><i class='fa fa-fw fa-sm fa fa-edit'></i></button>"
		            },
		            {
		                "targets": -1,
		                "data": null,
		                "defaultContent": "<button class='btn btn-warning btn-sm btnDelete' type='button'><i class='fa fa-fw fa-sm fa fa-trash'></i></button>"
		            }
	            ]
	        });
		/* END INCOMING ITEMS TABLE */
		$(".errMsg").hide();
		$(".myUsersInput").attr("disabled", true);
		var myUrl = "<?php echo base_url() ?>admin/Users/getUsers"; 
		// alert(myUrl);
		$.ajax({
			method : "POST",
			url : myUrl,
			data : {
				id : ""
			},
			success : function(data){
				// alert(data);
				usersTable.clear().draw();
				var srcData = JSON.parse(data);
				usersTable.rows.add(srcData).draw(false);
			},
			error : function(data){
				alert("Gagal load data");
			}
		});

		var rowIdx = null;
		var idUpdate = '';
		$("#usersTable tbody").on("click", ".btnEdit", function(){
			var userRow = usersTable.row( $(this).parents("tr") );
			rowIdx = usersTable.row( $(this).parents("tr") ).index();
			var userId = userRow.data()[0];
			var password = userRow.data()[1];
			var userGroup = userRow.data()[2];
			var isActive = userRow.data()[3];
			idUpdate = userId; 			
			$(".myUsersInput").attr("disabled", false);
			$("#userId").attr("disabled", true);
			$("#password").focus();
			$("#dstate").val("Edit");

			$("#userId").val(userId);
			$("#password").val(password);
			$("#userGroup").val(userGroup);
			$("#isActive").val(isActive);
			if(isActive == '1'){
				$('#isActive').prop('checked', true)
			}else{
				$('#isActive').prop('checked', false)
			}

			$("#userGroup option:contains("+userGroup+")").attr('selected', 'selected');
			
		});

		$("#btnAdd").on("click", function() {
			$(".myUsersInput").attr("disabled", false);
			$("#userId").attr("disabled", false);
			$("#dstate").val("Add");
			$("#userId").focus();
			$("#userId").val("");
			$("#password").val("");
			$("#isActive").prop("checked", true);
		});

		/* START SAVE DATA */
		$("#btnSave").on("click", function() {
			var dstate = $("#dstate").val();
			var userId = $("#userId").val();
			var password = $("#password").val();
			var userGroup = $("#userGroup").val();
			var isActive = $("#isActive").val();
			var isActive = "0";

			if ($('#isActive').is(":checked"))
			{
			  isActive = "1";
			}

			if(userId == ''){
				$('#userIdErr').show();
				$('#userId').focus();
				return false;
			}else if(password == ''){
				$('#passwordErr').show();
				$('#password').focus();
				return false;
			}else if( $("#userGroup option:selected").text() == "Pilih" ){
				$('#userGroupErr').show();
				$('#userGroup').focus();
				return false;
			}

			// alert(isActive);
			if(dstate == ""){
				return false;
			}

			// alert(isActive);
			// alert(dstate); 			
			var myUrl = "";
			if(dstate == "Add"){
				myUrl = "<?php echo base_url() ?>admin/Users/Ins";				
			}else if(dstate == "Edit") {
				myUrl = "<?php echo base_url() ?>admin/Users/Upd";				
			}
			$.ajax({
				url : myUrl,
				method : "POST",
				data : {
					dstate : dstate,
					idUpdate : idUpdate,
					userId : userId,
					password : password,
					userGroup : userGroup,
					isActive : isActive

				},
				success : function(data){
					alert("Data has been saved");						
					// alert(data);						
					if(dstate == 'Edit'){
						usersTable.row(rowIdx).data([
	                        userId,password,userGroup,isActive
	                    ]).draw(false);
					}
					if(dstate == 'Add'){
	                     usersTable.row.add([
				            userId,password,userGroup,isActive
				        ]).draw(false);
					}
					$("#dstate").val("");
					$("#userId").val("");
				},
				error : function(data){
					alert(data);
					// alert("Failed");
					// console.log(response);
	                // window.open(myUrl,'_blank');
				}	
			});

			$(".myUsersInput").attr("disabled", true);
		});
		/* END SAVE DATA */

		$("#usersTable tbody").on("click", ".btnDelete", function(){
			var userRow = usersTable.row( $(this).parents("tr") );
			rowIdx = usersTable.row( $(this).parents("tr") ).index();
			var userId = userRow.data()[0];
			// alert(userId);
			var is_valid = confirm(userId+" will deleted?");
            if (is_valid == true){

            		var myUrl = "<?php echo base_url() ?>admin/Users/Del";
					$.ajax({
						url : myUrl,
						method : "POST",
						data : {
							idDelete : userId
						},
						success : function(data){
							alert(data);
            				userRow.remove().draw();
							alert('Data has been deleted');
						},
						error : function(data){
							alert('Failed to delete');
						}
					});
			
            	
            }

		
		});



		
	})
</script>
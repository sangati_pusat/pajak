<!-- START FORM DATA -->
<div class="col-md-12">
  <div class="tile bg-info">
    <h3 class="tile-title">Master User</h3>
    <div class="tile-body">
      <form class="row bg-info">

        <div class="form-group col-sm-12 col-md-6">
          <label class="control-label">User Name</label>
          <code id="userIdErr" class="errMsg"><span> : Required</span></code>
          <input class="form-control myUsersInput" type="text" id="userId" disabled="" placeholder="User Id">
        </div>

        <div class="form-group col-sm-12 col-md-6">
          <label class="control-label">Password</label>
          <code id="passwordErr" class="errMsg"><span> : Required</span></code>
          <input class="form-control myUsersInput" type="text" id="password" placeholder="Password">
        </div>


        <div class="form-group col-sm-12 col-md-6">
          <label class="control-label">User Group</label>
          <code id="userGroupErr" class="errMsg"><span> : Required</span></code>
        <?php 
          $userStore = $this->session->userdata('hris_user_group');  


          if( $userStore == 'Pusat' ){
            echo '<select class="form-control" id="userGroup">';
            echo     '<option disabled="" selected="" value="">Pilih</option>';
            echo     '<option value="Pusat">Pusat</option>';
            echo     '<option value="Agincourt_Martabe">Agincourt Martabe</option>';
            echo     '<option value="Pontil_Timika">Pontil Timika</option>';
          }else{
            echo '<select class="form-control" id="userGroup" disabled="">';
          }

          $dataStore = $stores;

          foreach ($dataStore as $row) {
            $selected = '';
            if($userStore == $row['store_id']){
              $selected = 'selected=""';

            }
            echo '<option value="'.$row['store_id'].'" '.$selected.'>'.$row['store_name'].'</option>'; 
          }
        ?>               
          </select>
        </div>
       

        <div class="form-group col-sm-12">
          <input class="myUsersInput" type="checkbox" id="isActive" checked=""> &nbsp; Aktif
        </div>
       
        <div class="form-group col-md-4 align-self-end">
          <button class="btn btn-warning" type="button" id="btnAdd"><i class="fa fa-fw fa-lg fa fa-edit"></i>Tambah</button>
          <button class="btn btn-warning myUsersInput" type="button" id="btnSave"><i class="fa fa-fw fa-lg fa-check-circle"></i>Simpan</button>
          <!-- <input class="form-control" type="hidden" id="storesId" name="storesId" placeholder="Id Toko"> -->
          <input class="form-control" type="hidden" id="dstate" name="dstate" value="" placeholder="Status">
        </div>

      </form>
    </div>
  </div>
</div>
<!-- END FORM DATA -->

<!-- START DATA TABLE -->
<div class="col-md-12">
  <div class="tile">
    <h3 class="tile-title">List User</h3>
    <div class="tile-body">
      <table class="table table-hover table-bordered" id="usersTable">
        <thead class="thead-dark">
          <tr>
            <th>Id User</th>
            <th>Password</th>
            <th>Group</th>
            <th>Aktif</th>
            <th>Edit</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          
        </tbody>
      </table>
    </div>
  </div>
</div>
<!-- END DATA TABLE
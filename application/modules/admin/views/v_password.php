<!-- START FORM DATA -->
<div class="col-md-12">
  <div class="tile bg-info">
    <h3 class="tile-title">Master User</h3>
    <div class="tile-body">
      <form class="row bg-info">

        <div class="form-group col-sm-12 col-md-6">
          <label class="control-label">User Name</label>
          <!-- <code id="userIdErr" class="errMsg"><span> : Required</span></code> -->
          <input class="form-control" type="text" id="userId" value="<?php echo $this->session->userdata('pos_user_id') ?>" disabled="" placeholder="User Id">
        </div>

        <div class="form-group col-sm-12 col-md-6">
          <label class="control-label">Password</label>
          <code id="passwordErr" class="errMsg"><span> : Required</span></code>
          <input class="form-control myPasswordInput" type="text" id="password" placeholder="Password">
        </div>

        <div class="form-group col-sm-12 col-md-6">
          <label class="control-label">New Password</label>
          <code id="newPasswordErr" class="errMsg"><span> : Required</span></code>
          <input class="form-control myPasswordInput" type="text" id="newPassword" placeholder="Password">
        </div>

        <div class="form-group col-sm-12 col-md-6">
          <label class="control-label">Confirm Password</label>
          <code id="confirmPasswordErr" class="errMsg"><span> : Required</span></code>
          <input class="form-control myPasswordInput" type="text" id="confirmPassword" placeholder="Password">
        </div>                

        <!-- <div class="form-group col-sm-12">
          <input class="myPasswordInput" type="checkbox" id="isActive" checked=""> &nbsp; Aktif
        </div> -->
       
        <div class="form-group col-md-4 align-self-end">
          <!-- <button class="btn btn-warning" type="button" id="btnAdd"><i class="fa fa-fw fa-lg fa fa-edit"></i>Tambah</button> -->
          <button class="btn btn-warning myPasswordInput" type="button" id="btnSave"><i class="fa fa-fw fa-lg fa-check-circle"></i>Simpan</button>
          <!-- <input class="form-control" type="hidden" id="storesId" name="storesId" placeholder="Id Toko"> -->
          <!-- <input class="form-control" type="hidden" id="dstate" name="dstate" value="" placeholder="Status"> -->
        </div>

      </form>
    </div>
  </div>
</div>
<!-- END FORM DATA -->


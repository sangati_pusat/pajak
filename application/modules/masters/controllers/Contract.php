<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contract extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model("masters/M_mst_contract");
        $this->load->model("masters/M_mst_bio_rec");
        $this->load->model("masters/M_mst_salary");
        $this->load->model("masters/M_loan");
    }

    public function index()
    {
        /*$rows = $this->M_mst_contract->loadData();
        $myData = array();
        foreach ($rows as $row) {
            $myData[] = array(
                $row['contract_id'],         
                $row['bio_rec_id'],    
                $row['employee_name'],    
                $row['contract_no'],    
                $row['client_name'],    
                $row['contract_start'],    
                $row['contract_end']   
            );            
        }
        echo json_encode($myData);*/
        $this->loadContractByClientEntry();
    }
    
    public function loadContractByClient()
    {
        $clientName = $this->session->userdata('hris_user_group');
        $rows = $this->M_mst_contract->loadContractByClientName($clientName);
        // echo $rows;
        $myData = array();
        foreach ($rows as $row) {
            $myData[] = array(
                $row['contract_id'],         
                $row['nie'],    
                $row['full_name'],    
                $row['client_name'],    
                $row['dept'],    
                $row['position'],    
                $row['contract_no'],    
                $row['date_of_hire'],    
                $row['local_foreign'],    
                $row['contract_start'],    
                $row['contract_end'],  
                $row['remarks']   
            );            
        }
        echo json_encode($myData);
    }

    public function loadContractByClientEntry()
    {
        $clientName = $this->session->userdata('hris_user_group');
        $rows = $this->M_mst_contract->loadContractByClientName($clientName);
        // echo $rows;
        $myData = array();
        foreach ($rows as $row) {
            $myData[] = array(
                $row['contract_id'],         
                $row['nie'],    
                $row['full_name'],    
                $row['client_name'],    
                $row['dept'],    
                $row['position'],    
                $row['contract_no'],    
                // $row['date_of_hire'],    
                // $row['local_foreign'],    
                $row['contract_start'],    
                $row['contract_end'],   
                $row['remarks']   
            );            
        }
        echo json_encode($myData);
    }

    public function ins()
    {
        $contractId = $this->M_mst_contract->GenerateNumber();
        $biodataId = $_POST['bioId'];
        $biodataId = preg_replace('/[\r\n]+/','', $biodataId);
        $biodataId = trim($biodataId);

        $myCounter = $this->M_mst_contract->getLastCounterByBiodataId($biodataId);
        $this->M_mst_contract->setContractId($this->security->xss_clean($contractId));
        $this->M_mst_contract->setBioRecId($this->security->xss_clean($biodataId));

        $empName = preg_replace('/[\r\n]+/','', $_POST['empName']);
        $empName = trim($empName);
        $this->M_mst_contract->setEmployeeName($this->security->xss_clean($empName));

        $contractNo = preg_replace('/[\r\n]+/','', $_POST['contractNo']);
        $contractNo = trim(strtoupper($contractNo));
        $this->M_mst_contract->setContractNo($this->security->xss_clean($contractNo));

        $dept = preg_replace('/[\r\n]+/','', $_POST['deptName']);
        $dept = trim($dept);
        $this->M_mst_contract->setDept($this->security->xss_clean($dept));

        $position = preg_replace('/[\r\n]+/','', $_POST['position']);
        $position = trim($position);
        $this->M_mst_contract->setPosition($this->security->xss_clean($position));

        $this->M_mst_contract->setContractStart($this->security->xss_clean($_POST['contractStart']));
        $this->M_mst_contract->setContractEnd($this->security->xss_clean($_POST['contractEnd']));

        $ptName = preg_replace('/[\r\n]+/','', $_POST['ptName']);
        $ptName = trim($ptName);
        $this->M_mst_contract->setClientName($this->security->xss_clean($ptName));
        
        $this->M_mst_contract->setIsActive($this->security->xss_clean('1'));
        $this->M_mst_contract->setContractCounter($myCounter);
        $currFullDate = GetCurrentDate();
        $curDateTime = $currFullDate['CurrentDateTime'];
        $this->M_mst_contract->setInputTime($this->security->xss_clean($curDateTime));
        $this->M_mst_contract->setPicInput($this->security->xss_clean($this->session->userdata('hris_user_id')));

        /* START TRANSACTION */
        $this->db->trans_begin();
        $this->M_mst_contract->insert();
        $this->M_mst_contract->updateOldContractByBioId($biodataId, $myCounter);

        if ($this->db->trans_status() === FALSE)
        {
                /* ROLLBACK TRANSACTION IF ANY ERROR */
                $this->db->trans_rollback();
                echo 0;
        }
        else
        {
                /* COMMIT TRANSACTION */
                $this->db->trans_commit();
        }

        echo $contractId;
    }

    public function del()
    {
        // echo $_POST['idDelete'];
        // exit();
        if(isset($_POST['idDelete']))
        {
            $this->M_mst_contract->delete($this->security->xss_clean($_POST['idDelete']));
            echo "Data  ".$_POST['idDelete']."  has been deleted";
        }
    }

    /* START UPDATE */
      public function Upd() /* Execute Update To Database  */
      {
        $currFullDate = GetCurrentDate();
        $curDateTime = $currFullDate['CurrentDateTime'];
        $id = ($this->security->xss_clean($_POST['bioId']));
        $endContract = ($this->security->xss_clean($_POST['contractEnd']));
        $contractStart = ($this->security->xss_clean($_POST['contractStart']));
        $remarks = ($this->security->xss_clean($_POST['remarks']));
        $sql   = "UPDATE mst_contract SET contract_end = '".$endContract."', contract_start = '".$contractStart."', edit_time = '".$curDateTime."', pic_edit = '".$this->session->userdata('hris_user_id')."', remarks = '".$remarks."'  WHERE contract_id='".$id."' ";
        $query1 = $this->db->query($sql);
        // return $query1;
      }
     /* END UPDATE */


    public function contractHighchart(){
        $clientName = $this->session->userdata('hris_user_group');
        $data  = $this->M_mst_contract->myContractChart($clientName);

        $myData = array();

        foreach ($data as $row) {
            $myData[] = array(
                "name"  => $row['label'], 
                "y"     => (int)$row['y']         
            );            
        }        
        print_r(json_encode($myData, true));
    }

    public function contractChart(){
        $clientName = $this->session->userdata('hris_user_group');
        $data  = $this->M_mst_contract->contractChart($clientName);
        print_r(json_encode($data, true));
    }

    public function myContractChart(){
        $clientName = $this->session->userdata('hris_user_group');
        $data = $this->M_mst_contract->myContractChart($clientName);

        $myData = array();
        foreach ($data as $row) {
            $myData[] = array(
                $row['y'], 
                $row['label']        
            );            
        }        
        echo json_encode($myData); 
    }

    public function myContractChartCount(){
        $clientName = $this->session->userdata('hris_user_group');
        $sql  = 'SELECT COUNT(*) dataTotal ';
        $sql .= ' FROM `mst_contract`';
        $sql .= ' WHERE is_active = 1 ';
         
        if($clientName != 'Pusat')
        {
           $sql .= ' AND client_name = "'.$clientName.'"';
        }

        $row = $this->db->query($sql)->row_array();
        $myCounter = 0;
        if(isset($row['dataTotal']))
        {
            $myCounter = $row['dataTotal'];            
        }     
        echo $myCounter;
    }


    public function contractHistDisplay(){
        $clientName = '';
        if(isset($_POST['clientName']))
        {
            $clientName = $_POST['clientName'];
        }

        $stQuery  = 'SELECT bio_rec_id, employee_name, client_name, COUNT(bio_rec_id) contract_count ';
        $stQuery .= 'FROM mst_contract ';
        $stQuery .= 'WHERE client_name = "'.$clientName.'" ';
        $stQuery .= 'AND is_close = 0 ';
        $stQuery .= 'GROUP BY bio_rec_id ';
        $stQuery .= 'HAVING COUNT(bio_rec_id) >= 9 ';
        $stQuery .= 'ORDER BY employee_name, contract_counter ';

        $data = $this->db->query($stQuery)->result_array();

        $myData = array();
        foreach ($data as $row) {
            $myData[] = array(
                $row['bio_rec_id'], 
                $row['employee_name'], 
                $row['client_name'], 
                $row['contract_count']        
            );            
        }        
        echo json_encode($myData); 
    }

    public function contractDetailDisplay(){
        $biodataId = '';
        $clientName = '';
        if(isset($_POST['biodataId']))
        {
            $biodataId = $_POST['biodataId'];
        }
        if(isset($_POST['clientName']))
        {
            $clientName = $_POST['clientName'];
        }

        $stQuery  = 'SELECT bio_rec_id, employee_name, client_name, contract_start, contract_end, contract_counter ';
        $stQuery .= 'FROM mst_contract ';
        $stQuery .= 'WHERE bio_rec_id = "'.$biodataId.'" ';
        $stQuery .= 'AND client_name = "'.$clientName.'" ';
        $stQuery .= 'AND is_close = 0 ';
        $stQuery .= 'ORDER BY employee_name, contract_counter ';  
        $data = $this->db->query($stQuery)->result_array();
        $myData = array();
        foreach ($data as $row) {
            $myData[] = array(
                $row['contract_counter'], 
                $row['contract_start'], 
                $row['contract_end']        
            );            
        }        
        echo json_encode($myData); 
    }

    /* START CONTRACT LIST EXPORT */
    public function exportContractHist($clientName)
    {
        //membuat objek
        $objPHPExcel = new PHPExcel();      
        /* AUTO WIDTH */     
        foreach(range('A','G') as $columnID)
        {
            $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }

        $totalStyle = array(
            'font'  => array(
                'bold'  => true,
                'color' => array('rgb' => '0000FF'),
                // 'size'  => 15,
                // 'name'  => 'Verdana'
            )
        );
        
        $allBorderStyle = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );

        $outlineBorderStyle = array(
          'borders' => array(
            'outline' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN
            )
          )
        );

        $topBorderStyle = array(
          'borders' => array(
            'top' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN
            )
          )
        );

        $bottomBorderStyle = array(
          'borders' => array(
            'bottom' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN
            )
          )
        );

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = PHPExcel_Style_Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = PHPExcel_Style_Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = PHPExcel_Style_Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = PHPExcel_Style_Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = PHPExcel_Style_Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = PHPExcel_Style_Alignment::VERTICAL_CENTER;  

        // Nama Field Baris Pertama
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'EMPLOYEE ID');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 1, 'NAME');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 1, 'CLIENT');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 1, 'CONTRACT COUNT');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 1, 'SEQENCES');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 1, 'CONTRACT START');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 1, 'CONTRACT END');
        $objPHPExcel->getActiveSheet()->getStyle("A1:G1")->getFont()->setBold(true)->setSize(12);
        /* CENTER TEXT */
        $objPHPExcel->getActiveSheet()->getStyle("A1:G1")->applyFromArray($center);

        /* COLOURING HEADER */
        $objPHPExcel->getActiveSheet()->getStyle("A1:G2")
        ->getFill()
        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
        ->getStartColor()
        ->setRGB('F2BE6B');

        $stQuery  = 'SELECT bio_rec_id, employee_name, client_name, COUNT(bio_rec_id) contract_count ';
        $stQuery .= 'FROM mst_contract ';
        $stQuery .= 'WHERE client_name = "'.$clientName.'" ';
        $stQuery .= 'AND is_close = 0 ';
        $stQuery .= 'GROUP BY bio_rec_id ';
        $stQuery .= 'HAVING COUNT(bio_rec_id) >= 9 ';
        $stQuery .= 'ORDER BY employee_name, contract_counter ';  
        $query = $this->db->query($stQuery)->result_array();  
        $rowIdx = 2;
        foreach ($query as $row) {          
            $bioId = $row['bio_rec_id'];
            $employeeName = $row['employee_name'];
            $clientName = $row['client_name'];
            $contractCount = $row['contract_count'];
            
            $rowIdx++;            

            $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$rowIdx, $bioId, PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $rowIdx, $employeeName);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $rowIdx, $clientName);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $rowIdx, $contractCount);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$rowIdx.':'.'G'.$rowIdx)->getFont()->setBold(true)->getColor()->setRGB('001188');;
            /* COLOURING DATA */
            $objPHPExcel->getActiveSheet()->getStyle('A'.$rowIdx.':'.'G'.$rowIdx)
            ->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('FCF7B6');

            $stQuery  = 'SELECT bio_rec_id, employee_name, client_name, contract_start, contract_end, contract_counter ';
            $stQuery .= 'FROM mst_contract ';
            $stQuery .= 'WHERE bio_rec_id = "'.$bioId.'" ';
            $stQuery .= 'AND client_name = "'.$clientName.'" ';
            $stQuery .= 'AND is_close = 0 ';
            $stQuery .= 'ORDER BY employee_name, contract_counter ';  
            $detailData = $this->db->query($stQuery)->result_array();

            foreach ($detailData as $dataRow) {
                $rowIdx++;
                // $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$rowIdx, $dataRow['work_order'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $rowIdx, $dataRow['contract_counter']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $rowIdx, $dataRow['contract_start']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $rowIdx, $dataRow['contract_end']);
                
            } // foreach ($detailData as $dataRow)

        } // foreach ($query as $row) 

        $footerRow = $rowIdx + 1;   
        /* COLOURING FOOTER */
        $objPHPExcel->getActiveSheet()->getStyle("A".$footerRow.":G".($footerRow))
        ->getFill()
        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
        ->getStartColor()
        ->setRGB('F2BE6B');    
        // $objPHPExcel->getActiveSheet()->getStyle('E3:E'.$rowIdx)->getNumberFormat()->setFormatCode('#,##0.00');
        unset($allBorderStyle);
        unset($center);
        unset($right);
        unset($left);
        
        $objPHPExcel->setActiveSheetIndex(0);

        //Set Title
        $objPHPExcel->getActiveSheet()->setTitle('ExportContractList');

        //Save ke .xlsx, kalau ingin .xls, ubah 'Excel2007' menjadi 'Excel5'
        $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->setPreCalculateFormulas(true);

        //Header
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

        //Nama File
        $fileName = "ContractHist".$clientName;
        header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx"');
        // header('Content-Disposition: attachment;filename="'.$ptName.$yearPeriod.$monthPeriod'.xlsx"');

        //Download
        $objWriter->save("php://output");             
           
    }
    /* END CONTRACT LIST EXPORT */

    public function getBioCompany()
    {
      $sql   = 'SELECT a.bio_rec_id, a.full_name, b.company_name FROM mst_bio_rec a '; 
      $sql  .='JOIN mst_salary b ON a.bio_rec_id=b.bio_rec_id ';
      $sql  .='ORDER BY a.full_name ASC ';
      $query = $this->db->query($sql)->result_array();
      // return $query;
      
      $myData = array();
      foreach ($query as $row) {
          $myData[] = array(
              $row['bio_rec_id'], 
              $row['full_name'],        
              $row['company_name']    
          );            
      }
      echo json_encode($myData);    
    }
    
}

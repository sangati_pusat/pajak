<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Process_Closing extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model("masters/M_process_closing");
    }

    public function checkClosing($clientName, $yearPeriod, $monthPeriod)
    {
    	$dataCount = $this->M_process_closing->getCountByClientPeriod($clientName, $yearPeriod, $monthPeriod);
        if($dataCount >= 1)
        {
            echo '1';
        }
        else
        {
        	echo '0';
        }
    }
}
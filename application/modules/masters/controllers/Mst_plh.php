<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// Load library phpspreadsheet
    require('./vendor/autoload.php');
    use PhpOffice\PhpSpreadsheet\Helper\Sample;
    use PhpOffice\PhpSpreadsheet\IOFactory;
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    // End load library phpspreadsheet
class Mst_plh extends CI_Controller
{
     /* START CONSTRUCTOR */
     public function __construct()
     {
     	parent::__construct();
     	$this->load->helper('security');
     	$this->load->model('masters/M_trn_plh');
        $this->load->model('masters/M_mst_rte');
     }
     /* END CONSTRUCTOR */

     public function index()
      {
        $rows = $this->M_mst_rte->getAll();
        $myData = array();
        foreach ($rows as $row) {
            $myData[] = array(
                $row['rte_number']
            );            
        }
        echo json_encode($myData);    
      }

     /* START INSERT */
     public function ins()
     {    
        $startDate = $_POST['startDate'];
        $bulan     = substr($startDate, 0,2);
        $tanggal   = substr($startDate, 3,2);
        $tahun     = substr($startDate, 6,4);
        $sDate     = $tahun.'-'.$bulan.'-'.$tanggal;

        $endDate   = $_POST['endDate'];
        $bulan     = substr($endDate, 0,2);
        $tanggal   = substr($endDate, 3,2);
        $tahun     = substr($endDate, 6,4);
        $eDate     = $tahun.'-'.$bulan.'-'.$tanggal;
        // test($semua,1);
        // $this->M_trn_plh->setBioRecIdPlh($this->security->xss_clean($bioRecIdPlh));tangal yang manas
        $userInput      = $this->session->userdata('hris_user_id'); 
        $headerId       = $this->M_trn_plh->GenerateNumber($_POST['clientName']);
        $myCurrentDate  = GetCurrentDate();
        $periode        = date('Y').date('m');
        $get_pc_code    = $this->M_trn_plh->get_no_doc($periode);

          $this->M_trn_plh->setBioRecIdPlh($this->security->xss_clean($headerId));
     	  $this->M_trn_plh->setBadgeNo($this->security->xss_clean($_POST['badgeNo']));
          $this->M_trn_plh->setRteNumber($this->security->xss_clean($_POST['rteNumber']));
     	  $this->M_trn_plh->setClientName($this->security->xss_clean($_POST['clientName']));
          $this->M_trn_plh->setIdCardNo($this->security->xss_clean($_POST['idCardNo']));
          $this->M_trn_plh->setPosition($this->security->xss_clean($_POST['position']));
          $this->M_trn_plh->setBpjskes($this->security->xss_clean($_POST['bpjskes']));
          $this->M_trn_plh->setBpjstk($this->security->xss_clean($_POST['bpjstk']));
     	  $this->M_trn_plh->setFullName($this->security->xss_clean($_POST['fullName']));
     	  $this->M_trn_plh->setStartDate($this->security->xss_clean($sDate));
          $this->M_trn_plh->setEndDate($this->security->xss_clean($eDate));
          $this->M_trn_plh->setMonthPeriod($this->security->xss_clean($_POST['monthPeriod']));
     	  $this->M_trn_plh->setYearPeriod($this->security->xss_clean($_POST['yearPeriod']));
     	  $this->M_trn_plh->setWorkDayAmmount($this->security->xss_clean($_POST['workDayAmmount']));
     	  $this->M_trn_plh->setPlhSalary($this->security->xss_clean($_POST['plhSalary']));
     	  $this->M_trn_plh->setIsActive($this->security->xss_clean('1'));
     	  $this->M_trn_plh->setPicInput($this->security->xss_clean($this->session->userdata('hris_user_id')));
          $curDateTime = $myCurrentDate['CurrentDateTime'];
     	  $this->M_trn_plh->setInputTime($this->security->xss_clean($curDateTime));
     	  $this->M_trn_plh->insert();
     	// $this->M_trn_plh->setPicEdit($this->security->xss_clean($_POST['picEdit'])); 
     	// $this->M_trn_plh->setEditTime($this->security->xss_clean($_POST['editTime']));
     }
     /* END INSERT */

    /* START UPDATE */
      public function Upd($bioRecIdPlh) /* Execute Update To Database  */
      {
        $startDate  = $_POST['startDate'];
        $endDate    = $_POST['endDate'];
        $pemisahs    = substr($startDate, 2,1);
        $pemisahe    = substr($endDate, 2,1);
        $bulan      = '';
        $tanggal    = '';
        $tahun      = '';
        $sDate      = '';
        $eDate      = '';

        if($pemisahs == '/'){
            $bulan      = substr($startDate, 0,2);
            $tanggal    = substr($startDate, 3,2);
            $tahun      = substr($startDate, 6,4);
            $sDate      = $tahun.'-'.$bulan.'-'.$tanggal;
        }
        else{
            $sDate      = $startDate;
        }

        if ($pemisahe == '/') {
            $bulan      = substr($endDate, 0,2);
            $tanggal    = substr($endDate, 3,2);
            $tahun      = substr($endDate, 6,4);
            $eDate      = $tahun.'-'.$bulan.'-'.$tanggal;
        }
        else{
            $eDate      = $endDate;
        }

    
        $currFullDate   = GetCurrentDate();
        $curDateTime    = $currFullDate['CurrentDateTime'];
        $id             = ($this->security->xss_clean($bioRecIdPlh));
        $startDate      = ($this->security->xss_clean($sDate));
        $endDate        = ($this->security->xss_clean($eDate));
        $monthPeriod    = ($this->security->xss_clean($_POST['monthPeriod']));
        $yearPeriod     = ($this->security->xss_clean($_POST['yearPeriod']));
        $workDayAmmount = ($this->security->xss_clean($_POST['workDayAmmount']));
        $plhSalary      = ($this->security->xss_clean($_POST['plhSalary']));
        
        $sql            = " UPDATE daily_worker SET start_date = '".$sDate."', end_date = '".$eDate."', month_period = '".$monthPeriod."', year_period = '".$yearPeriod."', work_day_ammount = '".$workDayAmmount."', plh_salary = '".$plhSalary."', edit_time = '".$curDateTime."', pic_edit = '".$this->session->userdata('hris_user_id')."' WHERE bio_rec_id_plh = '".$id."' ";
        // test($sql,1);
        $query1 = $this->db->query($sql);
        // test ($query1);
      }
     /* END UPDATE */

     /* START DELETE */
     public function del()
     {
     	$id = $this->security->xss_clean($_POST['bioRecIdPlh']);
     	$this->M_trn_plh->delete($id);
     }
     /* END DELETE */
     /* START GET ALL */
     public function loadAll()
     {
     	$rs = $this->M_trn_plh->getAll();
     	echo json_encode($rs); 
     }
     /* END GET ALL */

    public function loadPlhData()
    {        
        $rows = $this->M_trn_plh->getAll();
        $myData = array();

        foreach ($rows as $row) {
            $myData[] = array(
                // $row['salary_id'], 
                $row['bio_rec_id_plh'],       
                $row['rte_number'],
                $row['id_card_no'],
                $row['position'],
                $row['bpjskes'],
                $row['bpjstk'],
                $row['badge_no'],
                $row['full_name'],    
                $row['client_name'],
                $row['start_date'],
                $row['end_date'],
                $row['work_day_ammount'],   
                $row['plh_salary'],
                $row['year_period'],
                $row['month_period']
             

            );            
        }
        // $this->test($myData,1);
        echo json_encode($myData);    
    }

    public function getListPlh ($ptName, $startDate, $endDate)
    {

        $ptName      = $this->security->xss_clean($ptName);
        $startDate   = $startDate;
        $endDate     = $endDate;

        $query       = " SELECT dw.bio_rec_id_plh,dw.rte_number,dw.badge_no,dw.start_date,dw.end_date,dw.client_name,dw.id_card_no,dw.position,dw.bpjskes,dw.bpjstk,dw.full_name,dw.work_day_ammount,dw.month_period,dw.year_period,dw.plh_salary FROM db_recruitment.daily_worker dw WHERE dw.start_date >= '".$startDate."' AND dw.end_date <= '".$endDate."' ";       

        if($ptName !='ALL DATA'){
            $query .= "AND dw.client_name = '".$ptName."' ";
        }
        
        $sql         = $this->db->query($query)->result_array();

        $myData = array();
        foreach ($sql as $key => $row) {
            $myData[] = array(
                $row['bio_rec_id_plh'],         
                $row['rte_number'],         
                $row['full_name'],         
                $row['client_name'],                  
                $row['id_card_no'],                  
                $row['position'],                  
                $row['bpjskes'],                  
                $row['bpjstk'],                  
                $row['badge_no'],         
                $row['work_day_ammount'],         
                $row['start_date'],                  
                $row['end_date'],                  
                $row['plh_salary'],         
                $row['year_period'],
                $row['month_period']                  
                
            );            
        }  
        echo json_encode($myData);   
    }

    /* START INVOICE LIST EXPORT */
    public function exportPlh($ptName, $startDate, $endDate)
    {

        //membuat objek
        $spreadsheet = new Spreadsheet();  

         $spreadsheet->getProperties()->setCreator('Maurice - Web - Android')
            ->setLastModifiedBy('Maurice - Web - Android')
            ->setTitle('Office 2007 XLSX Test Document')
            ->setSubject('Office 2007 XLSX Test Document')
            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Test result file');

         if (file_exists('assets/images/report_logo.png')) {
            $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing->setName('Logo');
            $drawing->setDescription('Logo');
            $drawing->setPath('./assets/images/report_logo.png');
            $drawing->setCoordinates('A1');
            $drawing->setHeight(36);
            $drawing->setWorksheet($spreadsheet->getActiveSheet());
        }

        $ptName      = $this->security->xss_clean($ptName);
        $startDate   = $this->security->xss_clean($startDate);
        $endDate     = $this->security->xss_clean($endDate);

        $query       = " SELECT dw.bio_rec_id_plh,dw.rte_number,dw.badge_no,dw.start_date,dw.end_date,dw.client_name,dw.id_card_no,dw.position,dw.bpjskes,dw.bpjstk,dw.full_name,dw.work_day_ammount,dw.month_period,dw.year_period,dw.plh_salary FROM db_recruitment.daily_worker dw WHERE dw.start_date >= '".$startDate."' AND dw.end_date <= '".$endDate."' ";
        
        if($ptName !='ALL DATA'){
            $query .= "AND dw.client_name = '".$ptName."' ";
        }

        // test($query,1);
        $sql         = $this->db->query($query)->result_array();

         $boldFont = [
            'font' => [
                'bold' => true
                // 'color' => ['argb' => '0000FF'],
            ],
        ];

        $totalStyle = [
            'font' => [
                'bold' => true,
                'color' => ['argb' => '0000FF'],
            ],
        ];

        $allBorderStyle = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $outlineBorderStyle = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $topBorderStyle = [
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $bottomBorderStyle = [
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;        
         


        // Nama Field Baris Pertama  //
        $spreadsheet->getActiveSheet()
            ->setCellValue('C1', 'PT. SANGATI SOERYA SEJAHTERA')
            ->setCellValue('C2', 'PLH - '.$ptName)
            ->setCellValue('C3', 'Periode : ' .$startDate. ' s/d ' .$endDate)
           
            ->setCellValue('A6', 'NO')
            ->setCellValue('B6', 'BIODATA ID')
            ->setCellValue('C6', 'RTE NUMBER')
            ->setCellValue('D6', 'NAME')
            ->setCellValue('E6', 'CLIENT NAME')
            ->setCellValue('F6', 'ID CARD NO')
            ->setCellValue('G6', 'POSITION')
            ->setCellValue('H6', 'BPJS KESEHATAN')
            ->setCellValue('I6', 'BPJS KETENAGAKERJAAN')
            ->setCellValue('J6', 'BADGE NO')
            ->setCellValue('K6', 'WORK DAY AMOUNT')
            ->setCellValue('L6', 'START DATE')
            ->setCellValue('M6', 'END DATE')
            ->setCellValue('N6', 'YEAR PERIOD')
            ->setCellValue('O6', 'MONTH PERIOD')
            ->setCellValue('P6', 'SALARY');
                       

        $spreadsheet->getActiveSheet()
            ->mergeCells("C1:P1")
            ->mergeCells("C2:P2")
            ->mergeCells("C3:P3");

        $spreadsheet->getActiveSheet()
            ->mergeCells("A6:A7")
            ->mergeCells("B6:B7")
            ->mergeCells("C6:C7")
            ->mergeCells("D6:D7")
            ->mergeCells("E6:E7")
            ->mergeCells("F6:F7")
            ->mergeCells("G6:G7")
            ->mergeCells("H6:H7")
            ->mergeCells("I6:I7")
            ->mergeCells("J6:J7")
            ->mergeCells("K6:K7")
            ->mergeCells("L6:L7")
            ->mergeCells("M6:M7")
            ->mergeCells("N6:N7")
            ->mergeCells("O6:O7")
            ->mergeCells("P6:P7");
            


        //START HEADER PT SANGATI SOERYA SEJAHTERA //    
        $spreadsheet->getActiveSheet()->getStyle("A1:G1")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("A2:G2")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("A3:G3")->applyFromArray($center);
        //END HEADER PT SANGATI SOERYA SEJAHTERA //   

        // START TABLE LIST ACTIVE //
        $spreadsheet->getActiveSheet()->getStyle("A6:A7")->applyFromArray($center);

        $spreadsheet->getActiveSheet()->getStyle("B6:B7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth("15");

        $spreadsheet->getActiveSheet()->getStyle("C6:C7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth("15");

        $spreadsheet->getActiveSheet()->getStyle("D6:D7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth("15");

        $spreadsheet->getActiveSheet()->getStyle("E6:E7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth("15");

        $spreadsheet->getActiveSheet()->getStyle("F6:F7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth("15");

        $spreadsheet->getActiveSheet()->getStyle("G6:G7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth("15");

        $spreadsheet->getActiveSheet()->getStyle("H6:H7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth("15");

        $spreadsheet->getActiveSheet()->getStyle("I6:I7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth("15");

        $spreadsheet->getActiveSheet()->getStyle("J6:J7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth("15");

        $spreadsheet->getActiveSheet()->getStyle("K6:K7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth("15");

        $spreadsheet->getActiveSheet()->getStyle("L6:L7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth("15");

        $spreadsheet->getActiveSheet()->getStyle("M6:M7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getColumnDimension('M')->setWidth("15");

        $spreadsheet->getActiveSheet()->getStyle("N6:N7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getColumnDimension('N')->setWidth("15");

        $spreadsheet->getActiveSheet()->getStyle("O6:O7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getColumnDimension('O')->setWidth("15");

        $spreadsheet->getActiveSheet()->getStyle("P6:P7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getColumnDimension('P')->setWidth("15");
         
        // END TABLE LIST ACTIVE //

        
        $spreadsheet->getActiveSheet()->getStyle("A1:P1")->getFont()->setBold(true)->setSize(13);
        $spreadsheet->getActiveSheet()->getStyle("A2:P2")->getFont()->setBold(true)->setSize(13);
        $spreadsheet->getActiveSheet()->getStyle("A3:P3")->getFont()->setBold(true)->setSize(13);
              
        $spreadsheet->getActiveSheet()->getStyle("A6:P6")->getFont()->setBold(true)->setSize(13);
        $spreadsheet->getActiveSheet()->getStyle("A6:P7")->applyFromArray($allBorderStyle);

       
        /* SET HEADER BG COLOR*/
        $spreadsheet->getActiveSheet()->getStyle('A6:P7')
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B'); 

        $rowIdx = 8;
        $rowNo = 0;
        foreach ($sql as $row) {
            $rowIdx++;
            $rowNo++;

            $spreadsheet->getActiveSheet()
                ->setCellValue('A'.($rowIdx), $rowNo)
                ->setCellValue('B'.($rowIdx), $row['bio_rec_id_plh'])
                ->setCellValue('C'.($rowIdx), $row['rte_number'])
                ->setCellValue('D'.($rowIdx), $row['full_name'])
                ->setCellValue('E'.($rowIdx), $row['client_name'])
                ->setCellValue('F'.($rowIdx), $row['id_card_no'])
                ->setCellValue('G'.($rowIdx), $row['position'])
                ->setCellValue('H'.($rowIdx), $row['bpjskes'])
                ->setCellValue('I'.($rowIdx), $row['bpjstk'])
                ->setCellValue('J'.($rowIdx), $row['badge_no'])
                ->setCellValue('K'.($rowIdx), $row['work_day_ammount'])
                ->setCellValue('L'.($rowIdx), $row['start_date'])
                ->setCellValue('M'.($rowIdx), $row['end_date'])
                ->setCellValue('N'.($rowIdx), $row['year_period'])
                ->setCellValue('O'.($rowIdx), $row['month_period'])
                ->setCellValue('P'.($rowIdx), $row['plh_salary']);

            /* SET ROW COLOR */
            if($rowIdx % 2 == 1)
            {
                $spreadsheet->getActiveSheet()->getStyle('A'.$rowIdx.':P'.$rowIdx)
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('EAEBAF');     

                $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(12, ($rowIdx+2), 'TOTAL');
                $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(15, ($rowIdx+2), '=SUM(P8:P'.($rowIdx+2).')');
                $spreadsheet->getActiveSheet()->getStyle('P8:P'.($rowIdx+2))->getNumberFormat()->setFormatCode('#,##0.00');

                $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":P".($rowIdx+2))
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('F2BE6B');        
            } 
    //         /* END UPDATE TAX */
               
      
        } /* end foreach ($query as $row) */


        /* SET NUMBERS FORMAT*/
        
        unset($allBorderStyle);
        unset($center);
        unset($right);
        unset($left);
        
        
        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('PLH_Reports');

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        //Nama File
        // $str = $rowData['name'].$rowData['bio_rec_id'];
        $str = 'PLH_'.$ptName;
        $fileName = preg_replace('/\s+/', '', $str);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.Xlsx"');
        // header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        /* BY COMPOSER */
        // $writer = new Xlsx($spreadsheet);
        /* OFFLINE/ BY COPY EXCEL FOLDER  */
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit(0);  
    }

}

?>
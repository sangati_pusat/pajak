<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Mst_user extends CI_Controller
{
     /* START CONSTRUCTOR */
     public function __construct()
     {
     	parent::__construct();
     	$this->load->helper('security');
     	$this->load->model('M_mst_client');
     }
     /* END CONSTRUCTOR */
     
     public function loadAll()
     {
     	$rs = $this->db->query("SELECT user_id FROM db_admin.mst_user WHERE is_active='1' 
AND user_id IN ('muhammad.rafiuddin@sangati.co','putri','satria.ardika@sangati.co','virda.mariaagustin@lmsi.co')")->result_array();
     	echo json_encode($rs); 
     }

}


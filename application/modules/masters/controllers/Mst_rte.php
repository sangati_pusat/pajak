<?php

defined('BASEPATH') OR exit('No direct script access allowed');
// Load library phpspreadsheet
    require('./vendor/autoload.php');
    use PhpOffice\PhpSpreadsheet\Helper\Sample;
    use PhpOffice\PhpSpreadsheet\IOFactory;
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    // End load library phpspreadsheet
class Mst_rte extends CI_Controller
{
     /* START CONSTRUCTOR */
     public function __construct()
     {
     	parent::__construct();
     	$this->load->helper('security');
     	$this->load->model('masters/M_mst_rte');
        $this->load->model('masters/M_trn_rte_detail');
     }
     /* END CONSTRUCTOR */
     /* START INSERT */
     public function ins()
     {
      $myCurrentDate = GetCurrentDate();
      $inputTime     = $myCurrentDate['CurrentDateTime'];
      $userInput     = $this->session->userdata('hris_user_id'); 
      if(isset($_POST['dataHeader']))
        {
            $header = $_POST['dataHeader'];  
        }
      // $header        = $this->security->xss_clean($_POST['dataHeader']);
      // test($header,1);

        $tName = $this->security->xss_clean($header['rteNumber']);
     	$this->M_mst_rte->setRteNumber($this->security->xss_clean($header['rteNumber']));
     	$this->M_mst_rte->setDept($this->security->xss_clean($header['dept']));
     	$this->M_mst_rte->setDateRequired($this->security->xss_clean($header['dateRequired']));
     	$this->M_mst_rte->setCompany($this->security->xss_clean($header['company']));
     	$this->M_mst_rte->setLabourSource($this->security->xss_clean($header['labourSource']));
        $this->M_mst_rte->setPicInput($userInput);
        $this->M_mst_rte->setInputTime($inputTime);
 	    $this->M_mst_rte->insert();

      if(isset($_POST['dataManpowerPlan']))
        {
          $this->load->model('M_trn_rte_detail'); 
          $detail = $_POST['dataManpowerPlan'];                
          foreach ($detail as $key => $data) {
            $startDate = $data['startDate'];
            $bulan     = substr($startDate, 0,2);
            $tanggal   = substr($startDate, 3,2);
            $tahun     = substr($startDate, 6,4);
            $sDate     = $tahun.'-'.$bulan.'-'.$tanggal;

            $endDate   = $data['endDate'];
            $bulan     = substr($endDate, 0,2);
            $tanggal   = substr($endDate, 3,2);
            $tahun     = substr($endDate, 6,4);
            $eDate     = $tahun.'-'.$bulan.'-'.$tanggal;
            $this->M_trn_rte_detail->setRteNumber($tName);
            $this->M_trn_rte_detail->setPosition($this->security->xss_clean($data['position']));
            $this->M_trn_rte_detail->setRatePerDay($this->security->xss_clean($data['ratePerDay']));
            $this->M_trn_rte_detail->setTotalLabour($this->security->xss_clean($data['totalLabour']));
            $this->M_trn_rte_detail->setStartDate($this->security->xss_clean($sDate));
            $this->M_trn_rte_detail->setEndDate($this->security->xss_clean($eDate));
            $this->M_trn_rte_detail->setTotalDate($this->security->xss_clean($data['totalDate']));
            $this->M_trn_rte_detail->setWorkLocation($this->security->xss_clean($data['workLocation']));
            $this->M_trn_rte_detail->setRemarks($this->security->xss_clean($data['remarks']));
            $this->M_trn_rte_detail->setPicInput($userInput);
            $this->M_trn_rte_detail->setInputTime($inputTime);
            $this->M_trn_rte_detail->insert();
          } /* foreach */
        }           
       /* End Insert Education */ //tes coba
      $this->db->trans_complete();
       if($this->db->trans_status() === FALSE)
       {
          echo 'Error Simpan Data';
       }
       else
       {
          echo $tName;
       }
     }
     /* END INSERT */

     /* START UPDATE */
     public function upd()
     {
     	$id = $this->security->xss_clean($_POST['rteNumber']);
     	$this->M_mst_rte->getObjectById($id);
     	$this->M_mst_rte->setRteNumber($this->security->xss_clean($_POST['rteNumber']));
     	$this->M_mst_rte->setDept($this->security->xss_clean($_POST['dept']));
     	$this->M_mst_rte->setDateRequired($this->security->xss_clean($_POST['dateRequired']));
     	$this->M_mst_rte->setCompany($this->security->xss_clean($_POST['company']));
     	$this->M_mst_rte->setLabourSource($this->security->xss_clean($_POST['labourSource']));
     	$this->M_mst_rte->setPosition($this->security->xss_clean($_POST['position']));
     	$this->M_mst_rte->setRatePerDay($this->security->xss_clean($_POST['ratePerDay']));
     	$this->M_mst_rte->setTotalLabour($this->security->xss_clean($_POST['totalLabour']));
     	$this->M_mst_rte->setStartDate($this->security->xss_clean($_POST['startDate']));
     	$this->M_mst_rte->setEndDate($this->security->xss_clean($_POST['endDate']));
     	$this->M_mst_rte->setTotalDate($this->security->xss_clean($_POST['totalDate']));
     	$this->M_mst_rte->setWorkLocation($this->security->xss_clean($_POST['workLocation']));
     	$this->M_mst_rte->setRemarks($this->security->xss_clean($_POST['remarks']));
     	$this->M_mst_rte->setPicInput($this->security->xss_clean($_POST['picInput']));
     	$this->M_mst_rte->setInputTime($this->security->xss_clean($_POST['inputTime']));
     	$this->M_mst_rte->setPicEdit($this->security->xss_clean($_POST['picEdit']));
     	$this->M_mst_rte->setEditTime($this->security->xss_clean($_POST['editTime']));
     	$this->M_mst_rte->update($id);
     }
     /* END UPDATE */
     /* START DELETE */
     public function del()
     {
     	$id = $this->security->xss_clean($_POST['rteNumber']);
     	$this->M_mst_rte->delete($id);
     }
     /* END DELETE */
     /* START GET ALL */
     public function loadAll()
     {
     	$rs = $this->M_mst_rte->getAll();
     	echo json_encode($rs); 
     }
     /* END GET ALL */

     public function getRteNumber(){
      $stQuery = 'SELECT rte_number ';
      $stQuery.= 'FROM mst_rte';
      $rs = $this->db->query($stQuery)->result_array();
      // return json_encode($rs);
      print_r(json_encode($rs)); 
     }

     public function getListRte ($rteNumber, $dateRequired)
     { 

        $rteNumber      = str_replace('%5E', '/', $rteNumber); //pengambilan RTE NO.
        $rteNumber      = $this->security->xss_clean($rteNumber); //pengambilan RTE NO.
        $dateRequired   = $this->security->xss_clean($dateRequired);

        $query          = " SELECT a.rte_number,a.dept,a.date_required,a.company,a.labour_source,
                                   b.position,b.rate_per_day,b.total_labour,b.start_date,b.end_date,
                                   b.total_date,b.work_location,b.remarks  
                            FROM db_recruitment.mst_rte a
                            JOIN db_recruitment.trn_rte_detail b
                            ON a.rte_number = b.rte_number ";

                            if($rteNumber !='ALL DATA'){
                               $query .= "WHERE a.rte_number = '".$rteNumber."' ";
                            }
                            if($dateRequired !='ALL DATA'){
                                $query .= "AND  MONTH(a.date_required) = '".$dateRequired."' ";
                            }
        // test($query,1);
                                          
        $sql            = $this->db->query($query)->result_array();

        $myData = array();
        foreach ($sql as $key => $row) {
            $myData[] = array(                  
                $row['rte_number'],         
                $row['dept'],         
                $row['date_required'],                  
                $row['company'],                  
                $row['labour_source'],         
                $row['position'],                  
                $row['rate_per_day'],                  
                $row['total_labour'],         
                $row['start_date'],                  
                $row['end_date'],
                $row['total_date'],         
                $row['work_location'],         
                $row['remarks']                        
            );            
        }  
        echo json_encode($myData);   
     }

    /* START INVOICE LIST EXPORT */
     public function exportRte($rteNumber, $dateRequired)
     {

        //membuat objek
        $spreadsheet = new Spreadsheet();  

         $spreadsheet->getProperties()->setCreator('Maurice - Web - Android')
            ->setLastModifiedBy('Maurice - Web - Android')
            ->setTitle('Office 2007 XLSX Test Document')
            ->setSubject('Office 2007 XLSX Test Document')
            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Test result file');

         if (file_exists('assets/images/report_logo.png')) {
            $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing->setName('Logo');
            $drawing->setDescription('Logo');
            $drawing->setPath('./assets/images/report_logo.png');
            $drawing->setCoordinates('A1');
            $drawing->setHeight(36);
            $drawing->setWorksheet($spreadsheet->getActiveSheet());
        }

        $rteNumber      = str_replace('%5E', '/', $rteNumber); //pengambilan RTE NO.
        $rteNumber      = $this->security->xss_clean($rteNumber); //pengambilan RTE NO.
        $dateRequired   = $this->security->xss_clean($dateRequired);

        $query          = " SELECT a.rte_number,a.dept,a.date_required,a.company,a.labour_source,
                                   b.position,b.rate_per_day,b.total_labour,b.start_date,b.end_date,
                                   b.total_date,b.work_location,b.remarks  
                            FROM db_recruitment.mst_rte a
                            JOIN db_recruitment.trn_rte_detail b
                            ON a.rte_number = b.rte_number ";

                            if($rteNumber !='ALL DATA'){
                               $query .= "WHERE a.rte_number = '".$rteNumber."' ";
                            }
                            if($dateRequired !='ALL DATA'){
                                $query .= "AND  MONTH(a.date_required) = '".$dateRequired."' ";
                            }
        
        // test($query,1);
        $sql            = $this->db->query($query)->result_array();

         $boldFont = [
            'font' => [
                'bold' => true
                // 'color' => ['argb' => '0000FF'],
            ],
        ];

        $totalStyle = [
            'font' => [
                'bold' => true,
                'color' => ['argb' => '0000FF'],
            ],
        ];

        $allBorderStyle = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $outlineBorderStyle = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $topBorderStyle = [
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $bottomBorderStyle = [
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;        
         


        // Nama Field Baris Pertama  //
        $spreadsheet->getActiveSheet()
            ->setCellValue('C1', 'PT. SANGATI SOERYA SEJAHTERA')
            ->setCellValue('C2', 'REQUISITION TO EMPLOYEE (RTE) OUTSOURCING LABOUR : '.$rteNumber)
            ->setCellValue('C3', 'PERIODE : '.$dateRequired)
           
            ->setCellValue('A6', 'NO')
            ->setCellValue('B6', 'RTE NUMBER')
            ->setCellValue('C6', 'DEPT')
            ->setCellValue('D6', 'DATE REQUIRED')
            ->setCellValue('E6', 'COMPANY EMPLOYING THE LABOUR')
            ->setCellValue('F6', 'LABOUR SOURCE')
            ->setCellValue('G6', 'POSITION')
            ->setCellValue('H6', 'RATE PER DAY')
            ->setCellValue('I6', 'TOTAL LABOUR')
            ->setCellValue('J6', 'START DATE')
            ->setCellValue('K6', 'END DATE')
            ->setCellValue('L6', 'TOTAL DATE')
            ->setCellValue('M6', 'WORK LOCATION')
            ->setCellValue('N6', 'REMARKS');
                       

        $spreadsheet->getActiveSheet()
            ->mergeCells("C1:N1")
            ->mergeCells("C2:N2")
            ->mergeCells("C3:N3");

        $spreadsheet->getActiveSheet()
            ->mergeCells("A6:A7")
            ->mergeCells("B6:B7")
            ->mergeCells("C6:C7")
            ->mergeCells("D6:D7")
            ->mergeCells("E6:E7")
            ->mergeCells("F6:F7")
            ->mergeCells("G6:G7")
            ->mergeCells("H6:H7")
            ->mergeCells("I6:I7")
            ->mergeCells("J6:J7")
            ->mergeCells("K6:K7")
            ->mergeCells("L6:L7")
            ->mergeCells("M6:M7")
            ->mergeCells("N6:N7");
            // ->mergeCells("O6:O7");
            


        //START HEADER PT SANGATI SOERYA SEJAHTERA //    
        $spreadsheet->getActiveSheet()->getStyle("A1:G1")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("A2:G2")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("A3:G3")->applyFromArray($center);
        //END HEADER PT SANGATI SOERYA SEJAHTERA //   

        // START TABLE LIST ACTIVE //
        $spreadsheet->getActiveSheet()->getStyle("A6:A7")->applyFromArray($center);

        $spreadsheet->getActiveSheet()->getStyle("B6:B7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth("15");

        $spreadsheet->getActiveSheet()->getStyle("C6:C7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth("15");

        $spreadsheet->getActiveSheet()->getStyle("D6:D7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth("15");

        $spreadsheet->getActiveSheet()->getStyle("E6:E7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth("15");

        $spreadsheet->getActiveSheet()->getStyle("F6:F7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth("15");

        $spreadsheet->getActiveSheet()->getStyle("G6:G7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth("15");

        $spreadsheet->getActiveSheet()->getStyle("H6:H7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth("15");

        $spreadsheet->getActiveSheet()->getStyle("I6:I7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth("15");

        $spreadsheet->getActiveSheet()->getStyle("J6:J7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth("15");

        $spreadsheet->getActiveSheet()->getStyle("K6:K7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth("15");

        $spreadsheet->getActiveSheet()->getStyle("L6:L7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth("15");

        $spreadsheet->getActiveSheet()->getStyle("M6:M7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getColumnDimension('M')->setWidth("15");

        $spreadsheet->getActiveSheet()->getStyle("N6:N7")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getColumnDimension('N')->setWidth("15");         
        // END TABLE LIST ACTIVE //

        
        $spreadsheet->getActiveSheet()->getStyle("A1:N1")->getFont()->setBold(true)->setSize(13);
        $spreadsheet->getActiveSheet()->getStyle("A2:N2")->getFont()->setBold(true)->setSize(13);
        $spreadsheet->getActiveSheet()->getStyle("A3:N3")->getFont()->setBold(true)->setSize(13);
              
        $spreadsheet->getActiveSheet()->getStyle("A6:N6")->getFont()->setBold(true)->setSize(13);
        $spreadsheet->getActiveSheet()->getStyle("A6:N7")->applyFromArray($allBorderStyle);

       
        /* SET HEADER BG COLOR*/
        $spreadsheet->getActiveSheet()->getStyle('A6:N7')
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('F2BE6B'); 

        $rowIdx = 8;
        $rowNo = 0;
        foreach ($sql as $row) {
            $rowIdx++;
            $rowNo++;

            $spreadsheet->getActiveSheet()
                ->setCellValue('A'.($rowIdx), $rowNo)
                ->setCellValue('B'.($rowIdx), $row['rte_number'])
                ->setCellValue('C'.($rowIdx), $row['dept'])
                ->setCellValue('D'.($rowIdx), $row['date_required'])
                ->setCellValue('E'.($rowIdx), $row['company'])
                ->setCellValue('F'.($rowIdx), $row['labour_source'])
                ->setCellValue('G'.($rowIdx), $row['position'])
                ->setCellValue('H'.($rowIdx), $row['rate_per_day'])
                ->setCellValue('I'.($rowIdx), $row['total_labour'])
                ->setCellValue('J'.($rowIdx), $row['start_date'])
                ->setCellValue('K'.($rowIdx), $row['end_date'])
                ->setCellValue('L'.($rowIdx), $row['total_date'])
                ->setCellValue('M'.($rowIdx), $row['work_location'])
                ->setCellValue('N'.($rowIdx), $row['remarks']);

            /* SET ROW COLOR */
            if($rowIdx % 2 == 1)
            {
                $spreadsheet->getActiveSheet()->getStyle('A'.$rowIdx.':N'.$rowIdx)
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('EAEBAF');     

                // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(11, ($rowIdx+2), 'TOTAL');
                // $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(13, ($rowIdx+2), '=SUM(M8:M'.($rowIdx+2).')');
                // $spreadsheet->getActiveSheet()->getStyle('M8:M'.($rowIdx+2))->getNumberFormat()->setFormatCode('#,##0.00');

                // $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":M".($rowIdx+2))
                // ->getFill()
                // ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                // ->getStartColor()
                // ->setRGB('F2BE6B');        
            } 
    //         /* END UPDATE TAX */
               
      
        } /* end foreach ($query as $row) */


        /* SET NUMBERS FORMAT*/
        
        unset($allBorderStyle);
        unset($center);
        unset($right);
        unset($left);
        
        
        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('PLH_Reports');

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        //Nama File
        // $str = $rowData['name'].$rowData['bio_rec_id'];
        $str = 'PLH_Agincourt'.$rteNumber;
        $fileName = preg_replace('/\s+/', '', $str);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.Xlsx"');
        // header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        /* BY COMPOSER */
        // $writer = new Xlsx($spreadsheet);
        /* OFFLINE/ BY COPY EXCEL FOLDER  */
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit(0);  
     }
}
?>

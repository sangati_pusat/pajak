<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require('./vendor/autoload.php');
use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Mst_kpi extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('masters/M_trn_kpi');
        $this->load->model('masters/M_mst_bio_rec');
    } 

    public function index()
    {
        $rows = $this->M_trn_kpi->loadAllActive();
        $myData = array();
        foreach ($rows as $row) {
            $myData[] = array(
                $row['bio_rec_id'],         
                $row['company_name'],         
                $row['dept'],         
                $row['position'],         
                $row['full_name'],
            );            
        }
        echo json_encode($myData);    
    }

    
    public function loadKpiData()
    {        
        $rows = $this->M_trn_kpi->loadData();
        $myData = array();
        foreach ($rows as $row) {
            $myData[] = array(
                // $row['salary_id'], 
                $row['bio_rec_id'],       
                $row['full_name'],    
                $row['client_name'],
                $row['emp_dept'],
                $row['emp_position'],
                $row['year_period'],
                $row['month_period'],   
                $row['attendance'],    
                $row['performance'],
                $row['dicipline'],
                $row['communication'],
                $row['result_val'],
                $row['result_grade'],
                $row['remarks'],
                $row['pic_input'],
                $row['input_time'],
                $row['pic_edit'],
                $row['edit_time']

            );            
        }
        // $this->test($myData,1);
        echo json_encode($myData);    
    }

    public function ins()
    {

        // $this->test($hasil,1);
        $kpiId = $this->M_trn_kpi->GenerateNumber();
        $this->M_trn_kpi->setKpiId($kpiId);

        $this->M_trn_kpi->setBioRecId($this->security->xss_clean($_POST['biorecId']));
        $this->M_trn_kpi->setFullName($this->security->xss_clean($_POST['name']));
        $this->M_trn_kpi->setAttendance($this->security->xss_clean($_POST['attendance']));
        $this->M_trn_kpi->setDicipline($this->security->xss_clean($_POST['dicipline']));
        $this->M_trn_kpi->setPerformance($this->security->xss_clean($_POST['performance']));
        $this->M_trn_kpi->setCommunication($this->security->xss_clean($_POST['communication']));
        $this->M_trn_kpi->setYearPeriod($this->security->xss_clean($_POST['yearPeriod']));
        $this->M_trn_kpi->setMonthPeriod($this->security->xss_clean($_POST['monthPeriod']));

        $this->M_trn_kpi->setEmpDept($this->security->xss_clean($_POST['dept']));
        $this->M_trn_kpi->setEmpPosition($this->security->xss_clean($_POST['position']));

        $this->M_trn_kpi->setClientName($this->security->xss_clean($_POST['clientName']));
        $this->M_trn_kpi->setResultVal($this->security->xss_clean($_POST['nilai']));
        $this->M_trn_kpi->setResultGrade($this->security->xss_clean($_POST['hasil']));
        $this->M_trn_kpi->setRemarks($this->security->xss_clean($_POST['remarks']));
        $this->M_trn_kpi->setPicInput($this->security->xss_clean($this->session->userdata('hris_user_id')));
        $this->M_trn_kpi->setInputTime($this->security->xss_clean(date('Y-m-d H:i:s')));

        $this->M_trn_kpi->insert();
        $this->loadKpiData();
    }

    public function get_kpiall()
    {
        $data   = array(); 
        $param  = array();
        $total  = 0;
        $start  = $this->input->get('start');
        $length = $this->input->get('length');
        $search = $this->input->get('search');
        $order  = $this->input->get('order');
        $columns= $this->input->get('columns');
        $client = $this->input->get('client');
        $month  = $this->input->get('month');
        $year   = $this->input->get('year');

        if(is_array($columns) && isset($order[0]['column'])){
            $param['order'] = array(
                'by' => $columns[$order[0]['column']]['data'],
                'type' => $order[0]['dir']
            );
        }

        if(isset($search['value']) && $search['value']) $param['where']['full_name'] = $search['value'];
        $data   = $this->M_trn_kpi->get_all_kpi($start,$length,$param,$client,$month,$year);
        $total  = $this->M_trn_kpi->get_count_kpi($param,$client,$month,$year);

        jsout(
            array(
                'success'=>1,
                'data'=>$data,
                'iTotalDisplayRecords'=>$total,
                'iTotalRecords'=>count($data)
            )
        );
    }

    public function printKpi($client,$month,$year)
    {
        // $client     = $this->input->post('client');
        // $month      = $this->input->post('month');
        // $year       = $this->input->post('year');

        $spreadsheet = new Spreadsheet();

        $boldFont = [
            'font' => [
                'bold' => true
            ],
        ];

        $totalStyle = [
            'font' => [
                'bold' => true,
                'color' => ['argb' => '0000FF'],
            ],
        ];

        $allBorderStyle = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $outlineBorderStyle = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $topBorderStyle = [
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $bottomBorderStyle = [  
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ];

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        /* COLOURING FOOTER */
        $spreadsheet->getActiveSheet()->getStyle("A4:O5")
        ->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()
        ->setRGB('F2BE6B'); 

        $strSql  = 'SELECT * FROM `db_recruitment`.`trn_kpi` WHERE client_name="'.$client.'" AND year_period="'.$year.'" AND month_period="'.$month.'" ORDER BY kpi_id DESC ';
        $query = $this->db->query($strSql)->result_array(); 

        $spreadsheet->getActiveSheet()
            ->setCellValue('A1', 'PT. SANGATI SOERYA SEJAHTERA')
            ->setCellValue('A2', 'KEY PERFORMANCE INDICATOR '.$client);

        $spreadsheet->getActiveSheet()->mergeCells("A1:O1");
        $spreadsheet->getActiveSheet()->getStyle("A1:O1")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->mergeCells("A2:O2");
        $spreadsheet->getActiveSheet()->getStyle("A2:O2")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("A4:O4")->applyFromArray($boldFont);
        $spreadsheet->getActiveSheet()->getStyle("A4:O4")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("A1:O1")->getFont()->setBold(true)->setSize(16);
        $spreadsheet->getActiveSheet()->getStyle("A2:O2")->getFont()->setBold(true)->setSize(13);
        $spreadsheet->getActiveSheet()->getStyle("A4:O4")->getFont()->setBold(true)->setSize(12);

        $spreadsheet->getActiveSheet()->getStyle('A4:O5')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('F2BE6B');

        $spreadsheet->getActiveSheet()
            ->mergeCells("A4:A5")
            ->mergeCells("B4:B5")
            ->mergeCells("C4:C5")
            ->mergeCells("D4:D5")
            ->mergeCells("E4:E5")
            ->mergeCells("F4:F5")
            ->mergeCells("G4:G5")
            ->mergeCells("H4:H5")
            ->mergeCells("I4:I5")
            ->mergeCells("J4:J5")
            ->mergeCells("K4:K5")
            ->mergeCells("L4:L5")
            ->mergeCells("M4:M5")
            ->mergeCells("N4:N5")
            ->mergeCells("O4:O5");

        $titleRowIdx = 4;

        $spreadsheet->getActiveSheet()
            ->setCellValue('A4', 'No')
            ->setCellValue('B4', 'Biodata ID')
            ->setCellValue('C4', 'Name')
            ->setCellValue('D4', 'Client')
            ->setCellValue('E4', 'Department')
            ->setCellValue('F4', 'Position')
            ->setCellValue('G4', 'Year')
            ->setCellValue('H4', 'Month')
            ->setCellValue('I4', 'Attendance')
            ->setCellValue('J4', 'Performance')
            ->setCellValue('K4', 'Dicipline')
            ->setCellValue('L4', 'Communication')
            ->setCellValue('M4', 'Value')
            ->setCellValue('N4', 'Grade')
            ->setCellValue('O4', 'Remarks');

        $spreadsheet->getActiveSheet()->getStyle("A4:O4")->applyFromArray($outlineBorderStyle);

        $rowIdx = 5;
        $rowNo = 0;

        foreach ($query as $row) {
            $rowIdx++;
            $rowNo++;
            $spreadsheet->getActiveSheet()
                ->setCellValue('A'.($rowIdx), $rowNo)
                ->setCellValue('B'.($rowIdx), $row['bio_rec_id'])
                ->setCellValue('C'.($rowIdx), $row['full_name'])
                ->setCellValue('D'.($rowIdx), $row['client_name'])
                ->setCellValue('E'.($rowIdx), $row['emp_dept'])
                ->setCellValue('F'.($rowIdx), $row['emp_position'])
                ->setCellValue('G'.($rowIdx), $row['year_period'])
                ->setCellValue('H'.($rowIdx), $row['month_period'])
                ->setCellValue('I'.($rowIdx), $row['attendance'])
                ->setCellValue('J'.($rowIdx), $row['performance'])
                ->setCellValue('K'.($rowIdx), $row['dicipline'])
                ->setCellValue('L'.($rowIdx), $row['communication'])
                ->setCellValue('M'.($rowIdx), $row['result_val'])
                ->setCellValue('N'.($rowIdx), $row['result_grade'])
                ->setCellValue('O'.($rowIdx), $row['remarks']);
        
            if($rowIdx % 2 == 1)
            {
                $spreadsheet->getActiveSheet()->getStyle('A'.$rowIdx.':O'.$rowIdx)
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('EAEBAF');             
            } 
        }

        $spreadsheet->getActiveSheet()->getStyle("A".($rowIdx+2).":O".($rowIdx+2))
        ->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()
        ->setRGB('F2BE6B');

        unset($allBorderStyle);
        unset($center);
        unset($right);
        unset($left);

        $spreadsheet->setActiveSheetIndex(0);

        $str = 'KPI'.$client.$year.$month;
        $fileName = preg_replace('/\s+/', '', $str);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.Xlsx"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit(0);
    }



   

//         function test($x,$exit=0, $hide=false)
// {
//   echo ($hide) ? '<div style="display:none;">' : '';
//   echo "<pre>";
//   if(is_array($x) || is_object($x)){
//     echo print_r($x);
//   }elseif(is_string($x)){
//     echo $x;
//   }else{
//     echo var_dump($x);
//   }
//   echo "</pre><hr />";
//   echo ($hide) ? '</div>' : '';
//   if($exit==1){ die(); }
// }

        // public function printKpi(){

        // $this->load->model('M_TrnKpi');
        // $query      = $this->M_TrnKpi->print_kpi();
        // $data       = $this->M_TrnKpi->count_print_kpi();

        // $jml_data   = $data->jml;
        // $start_at   = 3;
        // $jumlah_row = $jml_data+$start_at;
        // $start_border= 'A'.$start_at.':N'.$jumlah_row;
        

        // $this->excel_generator->getActiveSheet()->setCellValue('A1', 'Key Performance Indicator');
        // $this->excel_generator->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
        // $this->excel_generator->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        // $this->excel_generator->getActiveSheet()->mergeCells('A1:N1');
        // $this->excel_generator->getActiveSheet()->getStyle('A1:N100')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        // $styleArray = array(
        //     'borders' => array(
        //         'allborders' => array(
        //             'style' => PHPExcel_Style_Border::BORDER_THIN
        //             )
        //     )
        // );

        // $this->excel_generator->getActiveSheet()->getStyle($start_border)->applyFromArray($styleArray);
        // unset($styleArray);
        
        // $this->excel_generator->start_at($start_at);
        // $this->excel_generator->set_header(array('Biodata ID', 'Name', 'Client Name', 'Dept', 'Position', 'Year Period', 'Month Period','Attendance','Performance','Discipline','Communication','Result Value','Result Grade','Remarks'));
        // $this->excel_generator->set_column(array('bio_rec_id', 'name', 'client_name', 'emp_dept', 'emp_position', 'year_period', 'month_period','attendance','performance','dicipline','communication','result_val','result_grade','remarks'));
        // $this->excel_generator->set_width(array(15, 20, 20, 20, 15, 20,20, 20, 20,20,20,20,20,20));
        // $this->excel_generator->exportTo2007('KeyPerformanceIndicator');
        
        // }
        // END UNTUK EXPORT EXCEL//



        /* START PAYMENT EXPORT */
    public function exportKpi()
    {
        //membuat objek
        $objPHPExcel = new PHPExcel();

        // else if($ptName == "Redpath_Timika")

            $strSQL  = "SELECT mb.bio_rec_id,ss.name,ss.client_name,tk.emp_dept,tk.emp_position,ss.year_period,ss.month_period,tk.attendance,tk.performance,tk.dicipline,tk.communication,tk.result_val,tk.result_grade,tk.remarks ";
            $strSQL .= "FROM db_recruitment2018.mst_bio_rec mb,db_recruitment2018.trn_salary_slip ss,db_recruitment2018.trn_kpi tk ";
            $strSQL .= "WHERE mb.bio_rec_id = ss.bio_rec_id  ";
            $strSQL .= "AND mb.bio_rec_id = tk.bio_rec_id ";
                 

        // echo $strSQL; exit(0);


        $query = $this->db->query($strSQL)->result_array();   
        /* AUTO WIDTH */     
        foreach(range('A','N') as $columnID)
        {
            $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }

        // Nama Field Baris Pertama
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'PT. SANGATI SOERYA SEJAHTERA');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, 'KeyPerformanceIndicator PT. '.strtoupper('RED PATH') );
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 4, 'Periode : Januari-2018');
        $objPHPExcel->getActiveSheet()->getStyle("A1:D1")->getFont()->setBold(true)->setSize(16);
        $objPHPExcel->getActiveSheet()->getStyle("A2:D2")->getFont()->setBold(true)->setSize(13);
        // $objPHPExcel->getActiveSheet()->getStyle("A2:D2")->getFont()->setBold(true)->setSize(14)->setUnderline(true);
        $objPHPExcel->getActiveSheet()->getStyle("A4:N4")->getFont()->setBold(true)->setSize(12); 

        $totalStyle = array(
            'font'  => array(
                'bold'  => true,
                'color' => array('rgb' => '0000FF'),
                // 'size'  => 15,
                // 'name'  => 'Verdana'
            )
        );
        
        $allBorderStyle = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );

        $outlineBorderStyle = array(
          'borders' => array(
            'outline' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN
            )
          )
        );

        $topBorderStyle = array(
          'borders' => array(
            'top' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN
            )
          )
        );

        $bottomBorderStyle = array(
          'borders' => array(
            'bottom' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN
            )
          )
        );

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = PHPExcel_Style_Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = PHPExcel_Style_Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = PHPExcel_Style_Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = PHPExcel_Style_Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = PHPExcel_Style_Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = PHPExcel_Style_Alignment::VERTICAL_CENTER;  

        /* COLOURING FOOTER */
        $objPHPExcel->getActiveSheet()->getStyle("A6:I7")
        ->getFill()
        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
        ->getStartColor()
        ->setRGB('F2BE6B');    

        /* START PAYMENT TITLE */
        $objPHPExcel->getActiveSheet()->getStyle("A6:N6")->getFont()->setBold(true)->setSize(12);
        $objPHPExcel->getActiveSheet()->getStyle("A6:N6")->applyFromArray($outlineBorderStyle);
        /* START TITLE ID */
        $titleRowIdx = 6;
        $titleColIdx = 0;
        $objPHPExcel->getActiveSheet()->getStyle("A6:A7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("A6:A7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("A6:A7");
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'ID');
        /* END TITLE ID */

        /* START NAMA   */
        $titleColIdx++; // 1
        $objPHPExcel->getActiveSheet()->getStyle("B6:B7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("B6:B7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("B6:B7");
        $objPHPExcel->getActiveSheet()->getStyle("B6:B7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'NAMA');
        /* END NAMA   */

        /* START NIE  */
        $titleColIdx++; // 2
        $objPHPExcel->getActiveSheet()->getStyle("C6:C7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("C6:C7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("C6:C7");
        $objPHPExcel->getActiveSheet()->getStyle("C6:C7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $titleRowIdx, 'NIE');
        /* END NIE  */

        /* START CLIENT NAME  */
        $titleColIdx++; // 3
        $objPHPExcel->getActiveSheet()->getStyle("D6:D7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("D6:D7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("D6:D7");
        $objPHPExcel->getActiveSheet()->getStyle("D6:D7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'CLIENT NAME');
        /* END CLIENT NAME  */

        /* START POSITION  */
        $titleColIdx++; // 4
        $objPHPExcel->getActiveSheet()->getStyle("E6:E7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("E6:E7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("E6:E7");
        $objPHPExcel->getActiveSheet()->getStyle("E6:E7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'POSISI');
        /* END POSITION  */

        /* START STATUS  */
        $titleColIdx++; // 5
        $objPHPExcel->getActiveSheet()->getStyle("F6:F7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("F6:F7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("F6:F7");
        // $objPHPExcel->getActiveSheet()->getStyle("F6:F7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'STATUS');
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth("9");
        /* END STATUS  */

        /* START LOCAL/ NON LOCAL  */
        $titleColIdx++; // 6
        $objPHPExcel->getActiveSheet()->getStyle("G6:G7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("G6:G7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("G6:G7");
        $objPHPExcel->getActiveSheet()->getStyle("G6:G7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'LOKAL/ NON LOKAL');
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth("15");
        /* END LOCAL/ NON LOCAL */

        /* START IS ACTIVE  */
        $titleColIdx++; // 7
        $objPHPExcel->getActiveSheet()->getStyle("H6:H7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("H6:H7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("H6:H7");
        // $objPHPExcel->getActiveSheet()->getStyle("H6:H7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'IS ACTIVE');
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth("10");
        /* END IS ACTIVE  */

         /* START NPWP  */
        $titleColIdx++; // 8
        $objPHPExcel->getActiveSheet()->getStyle("I6:I7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("I6:I7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("I6:I7");
        $objPHPExcel->getActiveSheet()->getStyle("I6:I7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'NPWP');
        /* END NPWP  */ 

         /* START NPWP  */
        $titleColIdx++; // 8
        $objPHPExcel->getActiveSheet()->getStyle("J6:J7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("J6:J7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("J6:J7");
        $objPHPExcel->getActiveSheet()->getStyle("J6:J7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'NPWP');
        /* END NPWP  */

         /* START NPWP  */
        $titleColIdx++; // 8
        $objPHPExcel->getActiveSheet()->getStyle("K6:K7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("K6:K7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("K6:K7");
        $objPHPExcel->getActiveSheet()->getStyle("K6:K7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'NPWP');
        /* END NPWP  */

         /* START NPWP  */
        $titleColIdx++; // 8
        $objPHPExcel->getActiveSheet()->getStyle("L6:L7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("L6:L7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("L6:L7");
        $objPHPExcel->getActiveSheet()->getStyle("L6:L7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'NPWP');
        /* END NPWP  */

         /* START NPWP  */
        $titleColIdx++; // 8
        $objPHPExcel->getActiveSheet()->getStyle("M6:M7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("M6:M7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("M6:M7");
        $objPHPExcel->getActiveSheet()->getStyle("M6:M7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'NPWP');
        /* END NPWP  */

         /* START NPWP  */
        $titleColIdx++; // 8
        $objPHPExcel->getActiveSheet()->getStyle("N6:N7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("N6:N7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("N6:N7");
        $objPHPExcel->getActiveSheet()->getStyle("N6:N7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'NPWP');
        /* END NPWP  */    

        /* END PAYMENT TITLE */ 

 
        $bruttoTax = 0;

        $rowIdx = 6;
        $rowNo = 0;
        foreach ($query as $row) {
            $rowIdx++;
            $rowNo++;
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $rowIdx, $rowNo);
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $rowIdx, $row['bio_rec_id']);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$rowIdx, $row['bio_rec_id'], PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $rowIdx, $row['name']);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$rowIdx, $row['client_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $rowIdx, $row['emp_dept']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $rowIdx, $row['emp_position']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $rowIdx, $row['year_period']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $rowIdx, $row['month_period']);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$rowIdx, $row['attendance'], PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('I'.$rowIdx, $row['performance'], PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$rowIdx, $row['dicipline'], PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$rowIdx, $row['communication'], PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('L'.$rowIdx, $row['result_val'], PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('M'.$rowIdx, $row['result_grade'], PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('N'.$rowIdx, $row['remarks'], PHPExcel_Cell_DataType::TYPE_STRING);          
            /* END UPDATE TAX */
            /* SET ROW COLOR */
            if($rowIdx % 2 == 1)
            {
                $objPHPExcel->getActiveSheet()->getStyle('A'.$rowIdx.':N'.$rowIdx)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('EAEBAF');             
            } 

        } /* end foreach ($query as $row) */       
        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $rowIdx+2, "JUMLAH");
        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $rowIdx+2, "=SUM(H9:H".$rowIdx.")");
        // $objPHPExcel->getActiveSheet()->getStyle("H6:H7")->applyFromArray($allBorderStyle);
        // $totalBorder = $rowIdx+2;
        // $objPHPExcel->getActiveSheet()->getStyle("A".$totalBorder.":H".$totalBorder)->applyFromArray($outlineBorderStyle);

        /* SET NUMBERS FORMAT*/
        // $objPHPExcel->getActiveSheet()->getStyle('H8:H'.($rowIdx+2))->getNumberFormat()->setFormatCode('#,##0.00');
        /* COLOURING FOOTER */
        $objPHPExcel->getActiveSheet()->getStyle("A".($rowIdx+2).":N".($rowIdx+2))
        ->getFill()
        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
        ->getStartColor()
        ->setRGB('F2BE6B');

        unset($allBorderStyle);
        unset($center);
        unset($right);
        unset($left);
        
        $objPHPExcel->setActiveSheetIndex(0);

        //Set Title
        $objPHPExcel->getActiveSheet()->setTitle('KeyPerformanceIndicator');

        //Save ke .xlsx, kalau ingin .xls, ubah 'Excel2007' menjadi 'Excel5'
        $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->setPreCalculateFormulas(true);

        //Header
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

        //Nama File
        $fileName = 'Active'.$ptName.$yearPeriod.$monthPeriod;
        header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx"');
        // header('Content-Disposition: attachment;filename="'.$ptName.$yearPeriod.$monthPeriod'.xlsx"');

        //Download
        $objWriter->save("php://output");    
    }
    /* END PAYMENT EXPORT */
      
}

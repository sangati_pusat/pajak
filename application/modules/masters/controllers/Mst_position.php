<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Mst_position extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
	}

	public function getPositionGroup(){
		$stQuery = 'SELECT pg_id,pg_code,pg_name,is_active ';
		$stQuery.= 'FROM mst_position_group WHERE is_active = 1 ORDER BY pg_name';
		$rs = $this->db->query($stQuery)->result_array();
		// return json_encode($rs);
		print_r(json_encode($rs)); 
	}

	public function getPosition(){
		$stQuery = 'SELECT position_id,gp_id,position_code,position_name,is_active ';
		$stQuery.= 'FROM mst_position WHERE is_active = 1 ORDER BY position_name';
		$rs = $this->db->query($stQuery)->result_array();
		// return json_encode($rs);
		print_r(json_encode($rs)); 
	}

	public function getPositionByPG($pgId){
		$stQuery = 'SELECT position_id,gp_id,position_code,position_name,is_active ';
		$stQuery.= 'FROM mst_position ';
		$stQuery.= 'WHERE is_active = 1 ';
		$stQuery.= 'AND gp_id = "'.$pgId.'" ';
		$stQuery.= 'ORDER BY position_name';
		// echo $stQuery; exit(0);
		$rs = $this->db->query($stQuery)->result_array();
		print_r(json_encode($rs));
		// return $rs;
		// return json_encode($rs);
		// print_r($rs); 
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    require('./vendor/autoload.php');
    use PhpOffice\PhpSpreadsheet\Helper\Sample;
    use PhpOffice\PhpSpreadsheet\IOFactory;
    use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Mst_salary extends CI_Controller
{
      public function __construct()
      {
        parent::__construct();
        $this->load->model("masters/M_mst_bio_rec");
        $this->load->model("masters/M_mst_salary");
        $this->load->model("masters/M_mst_bank");
      } 

      public function index()
      {
        $rows = $this->M_mst_bio_rec->loadAllActive();
        $myData = array();
        foreach ($rows as $row) {
            $myData[] = array(
                $row['bio_rec_id'], 
                $row['nie'],        
                $row['full_name']    
            );            
        }
        echo json_encode($myData);    
      }

      public function loadSalaryData()
      {
        $rows = $this->M_mst_salary->loadData();
        $myData = array();
        foreach ($rows as $row) {
            $myData[] = array(
                $row['salary_id'],         
                $row['nie'],    
                $row['full_name'],    
                $row['payroll_group'],    
                $row['company_name'],    
                $row['salary_level'],    
                $row['basic_salary'],    
                $row['bank_name'],    
                $row['account_name'],    
                $row['account_no']    
            );            
        }
        echo json_encode($myData);    
      }

      public function loadSalaryDataReport()
      {
        $rows = $this->M_mst_salary->loadDataReport($this->input->post('pt'));
        $myData = array();
        foreach ($rows as $row) {
            if($row['edit_time']=='0000-00-00 00:00:00' || $row['edit_time']==''){
                $last_update    = $row['input_time'];
            }else{
                $last_update    = $row['edit_time'];
            }
            $myData[] = array(
                $row['salary_id'],         
                $row['nie'],    
                $row['full_name'],    
                $row['payroll_group'],    
                $row['company_name'],    
                $row['salary_level'],    
                $row['basic_salary'],    
                $row['bank_name'],    
                $row['account_name'],    
                $row['account_no'],
                $last_update
            );            
        }
        echo json_encode($myData);    
      }

    public function get_salaryall()
    {
        $data   = array(); 
        $param  = array();
        $total  = 0;
        $start  = $this->input->get('start');
        $length = $this->input->get('length');
        $search = $this->input->get('search');
        $order  = $this->input->get('order');
        $columns= $this->input->get('columns');
        $client = $this->input->get('client');

        if(is_array($columns) && isset($order[0]['column'])){
            $param['order'] = array(
                'by' => $columns[$order[0]['column']]['data'],
                'type' => $order[0]['dir']
            );
        }

        if(isset($search['value']) && $search['value']) $param['where']['full_name'] = $search['value'];
        $data   = $this->M_mst_salary->get_all_salary($start, $length, $param,$client);
        $total  = $this->M_mst_salary->get_count_display($param,$client);

        jsout(
            array(
                'success'=>1,
                'data'=>$data,
                'iTotalDisplayRecords'=>$total,
                'iTotalRecords'=>count($data)
            )
        );
    }

      public function loadSalaryAccount()
      {
        $rows = $this->M_MstSalary->loadData();
        $myData = array();
        foreach ($rows as $row) {
            $myData[] = array(
                $row['salary_id'],         
                $row['full_name'],    
                $row['payroll_group'],    
                $row['company_name'],    
                $row['bank_name'],    
                $row['account_name'],    
                $row['account_no']    
            );            
        }
        echo json_encode($myData);    
      }

      public function ins()
      {
        $salaryId = $this->M_mst_salary->GenerateNumber();
        // echo $salaryId;
        // exit();
        $this->M_mst_salary->setSalaryId($salaryId);
        $this->M_mst_salary->setBioRecId($this->security->xss_clean($_POST['bioId']));

        $tNie = $_POST['idEksternalNo'];
        $tNie = preg_replace('/[\r\n]+/','', $tNie);
        $tNie = trim(strtoupper($tNie)); 

        $get_name   = $this->M_mst_bank->getById($_POST['bankId']);
        $bankName   = $get_name['bank_name'];
               
        $this->M_mst_salary->setNie($this->security->xss_clean($tNie));
        $this->M_mst_salary->setCompanyName($this->security->xss_clean($_POST['ptName']));
        $this->M_mst_salary->setPayrollGroup($this->security->xss_clean($_POST['payrollGroup']));
        $this->M_mst_salary->setAreaPenempatan($this->security->xss_clean($_POST['areaPenempatan']));
        $this->M_mst_salary->setSalaryLevel($this->security->xss_clean($_POST['salaryLevel']));
        $this->M_mst_salary->setBasicSalary($this->security->xss_clean($_POST['basicSalary']));
        $this->M_mst_salary->setPositionalAllowance($this->security->xss_clean($_POST['positionAllowance']));
        $this->M_mst_salary->setTransportHousing($this->security->xss_clean($_POST['housingAllowance']));
        $this->M_mst_salary->setIsRemoteAllowance($this->security->xss_clean($_POST['isRemoteLocation']));
        $this->M_mst_salary->setIsShiftBonus($this->security->xss_clean($_POST['isShiftBonus']));
        $this->M_mst_salary->setIsAllowanceEconomy($this->security->xss_clean($_POST['isAllowanceEconomy']));
        $this->M_mst_salary->setIsOtBonus($this->security->xss_clean($_POST['isOTBonus']));
        $this->M_mst_salary->setIsIncentiveBonus($this->security->xss_clean($_POST['isIncentiveBonus']));
        $this->M_mst_salary->setIsDevIncentiveBonus($this->security->xss_clean($_POST['isIncentiveDevBonus']));
        $this->M_mst_salary->setIsProduction($this->security->xss_clean($_POST['isProductionBonus']));        
        $this->M_mst_salary->setIsCcPayment($this->security->xss_clean($_POST['isCcPayment']));        
        $this->M_mst_salary->setIsOvertime($this->security->xss_clean($_POST['isOverTime']));
        $this->M_mst_salary->setIsTravel($this->security->xss_clean($_POST['isTravelBonus']));
        $this->M_mst_salary->setBankName($this->security->xss_clean($bankName));
        $this->M_mst_salary->setBankId($this->security->xss_clean($_POST['bankId']));
        $this->M_mst_salary->setBankCode($this->security->xss_clean($_POST['bankCode']));
        $this->M_mst_salary->setAccountName($this->security->xss_clean($_POST['accountName']));

        $accountNo = $_POST['accountNo'];
        $accountNo = preg_replace('/[\r\n]+/','', $accountNo);
        $accountNo = trim(strtoupper($accountNo));        
        $this->M_mst_salary->setAccountNo($this->security->xss_clean($accountNo));
        $this->M_mst_salary->setPicInput($this->security->xss_clean($this->session->userdata('hris_user_id')));
        $currFullDate = GetCurrentDate();
        $curDateTime = $currFullDate['CurrentDateTime'];
        $this->M_mst_salary->setInputTime($this->security->xss_clean($curDateTime));
        // $this->M_MstSalary->setPicEdit($this->security->xss_clean($_POST['pic_edit']));
        // $this->M_MstSalary->setEditTime($this->security->xss_clean($_POST['edit_time']));

        $this->M_mst_salary->insert();
        // echo "Check"; exit(0);
        // echo $this->db->queries();
        // echo $salaryId;
      }

      public function del()
      {
        if(isset($_POST['idDelete']))
        {
            $this->M_mst_salary->delete($this->security->xss_clean($_POST['idDelete']));
            echo "Hapus Data  ".$_POST['idDelete']."  Berhasil";
        }
      }

      /*START LOAD SALARY LEVEL  BY @DIRAAYUW */
      public function loadSalaryLevel()
      {
        $rows = $this->M_MstSalary->loadUpdateLevel();
        $myData = array();
        foreach ($rows as $row) {
            $myData[] = array(
                $row['nie'],         
                $row['full_name'],        
                $row['company_name'],    
                $row['salary_level'],    
                $row['basic_salary']       
            );            
        }
        echo json_encode($myData);    
      }
      /*END LOAD SALARY LEVEL BY @DIRAAYUW */

      /*START UPDATE SALARY LEVEL BY @DIRAAYUW  */
      public function updateSalaryLevel() 
      {
        $nie = '';
        $salaryLevel = '';
        $basicSalary = '';
        
        if(isset($_POST['id_eksternal'])) 
        {
            $id_eksternal = $_POST['id_eksternal'];
        }
        
        if(isset($_POST['nie'])) 
        {
            $nie = $_POST['nie'];
        }
        if(isset($_POST['salaryLevel'])) 
        {
            $salaryLevel = $_POST['salaryLevel'];
        }
        if(isset($_POST['basicSalary'])) 
        {
            $basicSalary = $_POST['basicSalary'];
        }
        

        $nie = preg_replace('/[\r\n]+/','', $nie);
        $nie = trim(strtoupper($nie));

        $salaryLevel = preg_replace('/[\r\n]+/','', $salaryLevel);
        $salaryLevel = trim(strtoupper($salaryLevel));

        $basicSalary = preg_replace('/[\r\n]+/','', $basicSalary);
        $basicSalary = trim(strtoupper($basicSalary));


        $text = $this->M_mst_salary->updateSalaryLevelByIdNew($id_eksternal, $salaryLevel, $basicSalary, $nie);
      }

      function test($x,$exit=0, $hide=false)
{
    echo ($hide) ? '<div style="display:none;">' : '';
    echo "<pre>";
    if(is_array($x) || is_object($x)){
        echo print_r($x);
    }elseif(is_string($x)){
        echo $x;
    }else{
        echo var_dump($x);
    }
    echo "</pre><hr />";
    echo ($hide) ? '</div>' : '';
    if($exit==1){ die(); }
}

      public function updateAccount() 
      {

        $this->load->model("masters/M_mst_bank_hist");
        $this->load->model("master/M_mst_bio_rec");

        $salaryId = '';
        $bankName = '';
        $bankCode = '';
        $bankId = '';
        $accountName = '';
        $accountNo = '';
        
        if(isset($_POST['salaryId'])){
            $salaryId = $this->security->xss_clean($this->db->escape_str($_POST['salaryId']));
        }
        if(isset($_POST['bankName'])){
            $bankName = $this->security->xss_clean($this->db->escape_str($_POST['bankName']));
        }
        if(isset($_POST['bankCode'])){
            $bankCode = $this->security->xss_clean($this->db->escape_str($_POST['bankCode']));
        }
        if(isset($_POST['bankId'])){
            $bankId = $this->security->xss_clean($this->db->escape_str($_POST['bankId']));
        }
        if(isset($_POST['accountName'])){
            $accountName = $this->security->xss_clean($this->db->escape_str($_POST['accountName']));
        }
        if(isset($_POST['accountNo'])){
            $accountNo = $this->security->xss_clean($this->db->escape_str($_POST['accountNo']));
        }

        $salaryId = preg_replace('/[\r\n]+/','', $salaryId);
        $salaryId = trim(strtoupper($salaryId));

        $bankName = preg_replace('/[\r\n]+/','', $bankName);
        $bankName = trim(strtoupper($bankName));

        $bankCode = preg_replace('/[\r\n]+/','', $bankCode);
        $bankCode = trim(strtoupper($bankCode));

        $bankId   = preg_replace('/[\r\n]+/','', $bankId);
        $bankId   = trim(strtoupper($bankId));

        $accountName = preg_replace('/[\r\n]+/','', $accountName);
        $accountName = trim(strtoupper($accountName));

        $accountNo = preg_replace('/[\r\n]+/','', $accountNo);
        $accountNo = trim(strtoupper($accountNo));

        $get_name   = $this->M_mst_bank->getById($bankId);
        $bankName   = $get_name['bank_name'];

        $periode        = date('Y').date('m');
        $id_history     = $periode.$this->db->query("SELECT IFNULL(LPAD(MAX(SUBSTRING(history_id,7,3))+1,3,'0'),'001') nomor_id FROM mst_bank_hist WHERE SUBSTRING(history_id,1,6)='".$periode."'")->row()->nomor_id;

        $data_salary    = $this->M_mst_salary->getById($salaryId);
        $bio_rec_id     = $data_salary['bio_rec_id'];
        $salary_id      = $salaryId;
        $nie            = $data_salary['nie'];        
        $data_bio       = $this->M_mst_bio_rec->getById($bio_rec_id);
        $full_name      = $data_bio['full_name'];
        $old_acc_name   = $data_salary['account_name'];
        $old_acc_bank   = $data_salary['bank_name'];
        $old_acc_no     = $data_salary['account_no'];
        $new_acc_name   = $accountName;
        $new_acc_bank   = $bankName;
        $new_acc_no     = $accountNo;
        $is_sent        = '0';

        $text = $this->M_mst_bank_hist->insertHistory($id_history,$bio_rec_id,$salary_id,$nie,$full_name,$old_acc_name,$old_acc_bank,$old_acc_no,$new_acc_name,$new_acc_bank,$new_acc_no,$is_sent);
        $text = $this->M_mst_salary->updateAccoountById($salaryId, $bankName, $accountName, $accountNo, $bankCode, $bankId);
      }

   /* START  getSalaryByClientLevelUpdate BY DIRAAYUW*/

   public function getSalaryByClientLevelUpdate($clientName, $payrollLevel)
    {
        $strQuery = "SELECT * FROM mst_payroll_level WHERE client_name = '".$clientName."' AND payroll_level = '".$payrollLevel."'"; 
        $row = $this->db->query($strQuery)->row_array();
        $bs = 0;
        if(isset($row['basic_salary']))
        {
            $bs = $row['basic_salary'];            
        }        
        echo $bs; 
    } 
    /* END getSalaryByClientLevelUpdate BY DIRAAYUW*/

    public function getSalaryByClientLevel($clientName, $level)
    {
        $strQuery = "SELECT * FROM mst_payroll_level WHERE client_name = '".$clientName."' AND payroll_level = '".$level."'"; 
        $row = $this->db->query($strQuery)->row_array();
        $bs = 0;
        if(isset($row['basic_salary']))
        {
            $bs = $row['basic_salary'];            
        }        
        echo $bs; 
    }

    function exportdataSalary($pt){
        $objPHPExcel = new Spreadsheet();

        $query = $this->M_mst_salary->loadDataReport($pt);

        // Nama Field Baris Pertama
        $objPHPExcel->getActiveSheet()
            ->setCellValue('A1', 'PT. SANGATI SOERYA SEJAHTERA')
            ->setCellValue('A2', 'Data Salary Master');
            // ->setCellValue('A4', 'Periode : '.$monthPeriod.'-'.$yearPeriod);

        $objPHPExcel->getActiveSheet()->getStyle("A1:S1")->getFont()->setBold(true)->setSize(16);
        $objPHPExcel->getActiveSheet()->getStyle("A2:S2")->getFont()->setBold(true)->setSize(13);
        $objPHPExcel->getActiveSheet()->getStyle("A4:S4")->getFont()->setBold(true)->setSize(12); 

        $totalStyle = array(
            'font'  => array(
                'bold'  => true,
                'color' => array('rgb' => '0000FF'),
                // 'size'  => 15,
                // 'name'  => 'Verdana'
            )
        );
        
        $allBorderStyle = array(
            'borders' => array(
                'allborders' => array(
                    'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                )
            )
        );

        $outlineBorderStyle = array(
          'borders' => array(
            'outline' => array(
              'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
            )
          )
        );

        $topBorderStyle = array(
          'borders' => array(
            'top' => array(
              'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
            )
          )
        );

        $bottomBorderStyle = array(
          'borders' => array(
            'bottom' => array(
              'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
            )
          )
        );

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $objPHPExcel->getActiveSheet()->getStyle("A6:K7")
        ->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()
        ->setRGB('F2BE6B');      

        /* START PAYMENT TITLE */
        $objPHPExcel->getActiveSheet()->getStyle("A6:S6")->getFont()->setBold(true)->setSize(12);
        $objPHPExcel->getActiveSheet()->getStyle("A6:S6")->applyFromArray($outlineBorderStyle);
        /* START TITLE NO */
        $titleRowIdx = 6;
        $titleColIdx = 1;
        $objPHPExcel->getActiveSheet()->getStyle("A6:A7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("A6:A7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("A6:A7");
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'TABLE ID');
        /* END TITLE NO */

        /* START NAMA KARYAWAN  */
        $titleColIdx++; // 1
        $objPHPExcel->getActiveSheet()->getStyle("B6:B7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("B6:B7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("B6:B7");
        $objPHPExcel->getActiveSheet()->getStyle("B6:B7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'EXTERNAL ID');
        /* END NAMA KARYAWAN  */

        $titleColIdx++; // 1
        $objPHPExcel->getActiveSheet()->getStyle("C6:C7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("C6:C7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("C6:C7");
        $objPHPExcel->getActiveSheet()->getStyle("C6:C7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'NAME');

        /* START NO BPJS  */
        $titleColIdx++; // 2
        $objPHPExcel->getActiveSheet()->getStyle("D6:D7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("D6:D7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("D6:D7");
        $objPHPExcel->getActiveSheet()->getStyle("D6:D7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'PAYROLL GROUP');
        /* END NO BPJS  */

          /* START GROUP  */
        $titleColIdx++; // 2
        $objPHPExcel->getActiveSheet()->getStyle("E6:E7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("E6:E7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("E6:E7");
        $objPHPExcel->getActiveSheet()->getStyle("E6:E7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'CLIENT');
        /* END GROUP  */

        /* START ETHNIC */
        $titleColIdx++; // 3
        $objPHPExcel->getActiveSheet()->getStyle("F6:F7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("F6:F7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("F6:F7");
        $objPHPExcel->getActiveSheet()->getStyle("F6:F7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'LEVEL');
        /* END ETHNIC */

        /* START MARITAL STATUS  */
        $titleColIdx++; // 4
        $objPHPExcel->getActiveSheet()->getStyle("G6:G7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("G6:G7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("G6:G7");
        $objPHPExcel->getActiveSheet()->getStyle("G6:G7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'BASIC SALARY');
        /* END MARITAL STATUS  */

        /* START NPWP */
        $titleColIdx++; // 5
        $objPHPExcel->getActiveSheet()->getStyle("H6:H7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("H6:H7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("H6:H7");
        $objPHPExcel->getActiveSheet()->getStyle("H6:H7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'BANK NAME');
        /* END NPWP */

        /* START SALARY LEVEL */
        $titleColIdx++; // 6
        $objPHPExcel->getActiveSheet()->getStyle("I6:I7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("I6:I7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("I6:I7");
        $objPHPExcel->getActiveSheet()->getStyle("I6:I7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'ACCOUNT NAME');
        /* END SALARY LEVEL */

        /* START BASIC SALARY */
        $titleColIdx++; // 7
        $objPHPExcel->getActiveSheet()->getStyle("J6:J7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("J6:J7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("J6:J7");
        $objPHPExcel->getActiveSheet()->getStyle("J6:J7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'ACCOUNT NO');
        /* END BASIC SALARY */  

        /* START JKK-JKM(2.04%) */
        $titleColIdx++; // 7
        $objPHPExcel->getActiveSheet()->getStyle("K6:K7")->applyFromArray($allBorderStyle);
        $objPHPExcel->getActiveSheet()->getStyle("K6:K7")->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->mergeCells("K6:K7");
        $objPHPExcel->getActiveSheet()->getStyle("K6:K7")->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'LAST UPDATE');
        /* END JKK-JKM(2.04%) */

        // /* START BPJS KESEHATAN(4%) */
        // $titleColIdx++; // 7
        // $objPHPExcel->getActiveSheet()->getStyle("L6:L7")->applyFromArray($allBorderStyle);
        // $objPHPExcel->getActiveSheet()->getStyle("L6:L7")->applyFromArray($center);
        // $objPHPExcel->getActiveSheet()->mergeCells("L6:L7");
        // $objPHPExcel->getActiveSheet()->getStyle("L6:L7")->getAlignment()->setWrapText(true);
        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'ACCOUNT NAME');
        // /* END BPJS KESEHATAN(4%) */  

        // $titleColIdx++; // 7
        // $objPHPExcel->getActiveSheet()->getStyle("M6:M7")->applyFromArray($allBorderStyle);
        // $objPHPExcel->getActiveSheet()->getStyle("M6:M7")->applyFromArray($center);
        // $objPHPExcel->getActiveSheet()->mergeCells("M6:M7");
        // $objPHPExcel->getActiveSheet()->getStyle("M6:M7")->getAlignment()->setWrapText(true);
        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'ACCOUNT NUMBER');

        // $titleColIdx++; // 7
        // $objPHPExcel->getActiveSheet()->getStyle("N6:N7")->applyFromArray($allBorderStyle);
        // $objPHPExcel->getActiveSheet()->getStyle("N6:N7")->applyFromArray($center);
        // $objPHPExcel->getActiveSheet()->mergeCells("N6:N7");
        // $objPHPExcel->getActiveSheet()->getStyle("N6:N7")->getAlignment()->setWrapText(true);
        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'BPJS KESEHATAN(4%)');

        //  $titleColIdx++; // 7
        // $objPHPExcel->getActiveSheet()->getStyle("O6:O7")->applyFromArray($allBorderStyle);
        // $objPHPExcel->getActiveSheet()->getStyle("O6:O7")->applyFromArray($center);
        // $objPHPExcel->getActiveSheet()->mergeCells("O6:O7");
        // $objPHPExcel->getActiveSheet()->getStyle("O6:O7")->getAlignment()->setWrapText(true);
        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'JHT EMPLOYEE(2%)');
 
        // $titleColIdx++; // 7
        // $objPHPExcel->getActiveSheet()->getStyle("P6:P7")->applyFromArray($allBorderStyle);
        // $objPHPExcel->getActiveSheet()->getStyle("P6:P7")->applyFromArray($center);
        // $objPHPExcel->getActiveSheet()->mergeCells("P6:P7");
        // $objPHPExcel->getActiveSheet()->getStyle("P6:P7")->getAlignment()->setWrapText(true);
        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'JHT COMPANY(3.7%)');

        // $titleColIdx++; // 7
        // $objPHPExcel->getActiveSheet()->getStyle("Q6:Q7")->applyFromArray($allBorderStyle);
        // $objPHPExcel->getActiveSheet()->getStyle("Q6:Q7")->applyFromArray($center);
        // $objPHPExcel->getActiveSheet()->mergeCells("Q6:Q7");
        // $objPHPExcel->getActiveSheet()->getStyle("Q6:Q7")->getAlignment()->setWrapText(true);
        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'JAMINAN PENSIUN (1%)');

        // $titleColIdx++; // 7
        // $objPHPExcel->getActiveSheet()->getStyle("R6:R7")->applyFromArray($allBorderStyle);
        // $objPHPExcel->getActiveSheet()->getStyle("R6:R7")->applyFromArray($center);
        // $objPHPExcel->getActiveSheet()->mergeCells("R6:R7");
        // $objPHPExcel->getActiveSheet()->getStyle("R6:R7")->getAlignment()->setWrapText(true);
        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'JAMINAN PENSIUN (2%)');

        // $titleColIdx++; // 7
        // $objPHPExcel->getActiveSheet()->getStyle("S6:S7")->applyFromArray($allBorderStyle);
        // $objPHPExcel->getActiveSheet()->getStyle("S6:S7")->applyFromArray($center);
        // $objPHPExcel->getActiveSheet()->mergeCells("S6:S7");
        // $objPHPExcel->getActiveSheet()->getStyle("S6:S7")->getAlignment()->setWrapText(true);
        // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($titleColIdx, $titleRowIdx, 'NET PAYMENT');

        $rowIdx = 7;
        $rowNo = 0;
        foreach ($query as $row) {

            if($row['edit_time']=='0000-00-00 00:00:00' || $row['edit_time']==''){
                $last_update    = $row['input_time'];
            }else{
                $last_update    = $row['edit_time'];
            }

            $rowIdx++;
            $rowNo++;
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$rowIdx, $row['salary_id'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$rowIdx, $row['nie']);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$rowIdx, $row['full_name']);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$rowIdx, $row['payroll_group']);
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$rowIdx, $row['company_name']);
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$rowIdx, $row['salary_level']);
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$rowIdx, $row['basic_salary']);
            $objPHPExcel->getActiveSheet()->setCellValue('H'.$rowIdx, $row['bank_name']);
            $objPHPExcel->getActiveSheet()->setCellValue('I'.$rowIdx, $row['account_name']);
            $objPHPExcel->getActiveSheet()->setCellValue('J'.$rowIdx, $row['account_no'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValue('K'.$rowIdx, $last_update);
            // $objPHPExcel->getActiveSheet()->setCellValue('L'.$rowIdx, $row['account_name']);
            // $objPHPExcel->getActiveSheet()->setCellValue('M'.$rowIdx, $row['account_no']);
            // $objPHPExcel->getActiveSheet()->setCellValue('N'.$rowIdx, $row['health_bpjs']);
            // $objPHPExcel->getActiveSheet()->setCellValue('O'.$rowIdx, $row['emp_jht']);
            // $objPHPExcel->getActiveSheet()->setCellValue('P'.$rowIdx, $row['company_jht']);
            // $objPHPExcel->getActiveSheet()->setCellValue('Q'.$rowIdx, $row['emp_jp']);
            // $objPHPExcel->getActiveSheet()->setCellValue('R'.$rowIdx, $setbasic_salary);
            // $objPHPExcel->getActiveSheet()->setCellValue('S'.$rowIdx, $row['total']);

            /* SET ROW COLOR */
            if($rowIdx % 2 == 1)
            {
                $objPHPExcel->getActiveSheet()->getStyle('A'.$rowIdx.':K'.$rowIdx)
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('EAEBAF');             
            } 

        } /* end foreach ($query as $row) */       
        
        // $objPHPExcel->getActiveSheet()->getStyle("A".($rowIdx+2).":M".($rowIdx+2))
        // ->getFill()
        // ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        // ->getStartColor()
        // ->setRGB('F2BE6B');

        /* SET NUMBERS FORMAT*/

        unset($allBorderStyle);
        unset($center);
        unset($right);
        unset($left);
        
        $objPHPExcel->setActiveSheetIndex(0);

        //Set Title
        $objPHPExcel->getActiveSheet()->setTitle('Data Salary Master');

        //Save ke .xlsx, kalau ingin .xls, ubah 'Excel2007' menjadi 'Excel5'
        $writer = IOFactory::createWriter($objPHPExcel, 'Xlsx');
        $writer->setPreCalculateFormulas(true);

        //Header
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

        //Nama File
        $fileName = "Data Salary Master";
        header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx"');
        // header('Content-Disposition: attachment;filename="'.$client.$yearPeriod.$monthPeriod'.xlsx"');

        //Download
        $writer->save('php://output');
        exit(0); 
    }
}

<style type="text/css">
tr.selected {
  background-color: #B0BED9!important;
}
</style>

<div class="col-md-12">
  <div class="tile">
    <div class="tile-body">
      <form class="form-horizontal">
        <div class="form-group row">
          <label class="control-label col-md-2">Internal Id</label>
          <div class="col-md-4">
            <input class="errMsg form-control col-md-12" type="text" placeholder="Id Internal" id="bioId" data-toggle="modal" data-target="#salaryModal">
          </div>
          <label class="control-label col-md-2">Name</label>
          <div class="col-md-4">
            <input class="errMsg form-control col-md-12" type="text" placeholder="Nama Karyawan" id="empName" disabled="">
          </div>
        </div>
        <div class="form-group row">
          <label class="control-label col-md-2">Eksternal Id</label>
          <div class="col-md-4">
            <input class="errMsg form-control col-md-12" type="text" placeholder="Id Eksternal" id="idEksternalNo">
          </div>
          <label class="control-label col-md-2">Client Name</label>
          <div class="col-md-4">
            <select class="errMsg form-control" id="ptName" name="ptName" required="">
              <option value="" disabled="" selected="">Pilih</option>
            </select> 
          </div>
        </div>
        <div class="form-group row">
          <label class="control-label col-md-2">Grup Payroll</label>
          <div class="col-md-4">
            <select class="errMsg form-control" id="payrollGroup" name="payrollGroup" required="">
              <option value="" disabled="" selected="">Pilih</option>
              <option value="A">A</option>
              <option value="B">B</option>
              <option value="C">C</option>
              <option value="D">D</option>
              <option value="E">E</option>
              <option value="F">F</option>
              <option value="G">G</option>
              <option value="H">H</option>
              <option value="I">I</option>
              <option value="J">J</option>
              <option value="K">K</option>
              <option value="L">L</option>
              <option value="M">M</option>
              <option value="N">N</option>
              <option value="O">O</option>
              <option value="P">P</option>
              <option value="Staff">Staff</option>
              <option value="Pontil Jakarta">Pontil Jakarta</option>
            </select>
          </div>
          <label class="control-label col-md-2">Placement</label>
          <div class="col-md-4">
            <select class="errMsg form-control" id="areaPenempatan" name="areaPenempatan" required="">
              <option value="" disabled="" selected="">Pilih</option>
              <option value="Jakarta">Jakarta</option>
              <option value="Papua">Papua</option>
              <option value="Sumbawa">Sumbawa</option>
              <option value="Sibolga">Sibolga</option>
              <option value="Menado">Menado</option>
              <option value="Banyuwangi">Banyuwangi</option>
              <option value="lain-lain">Lain-Lain</option>
            </select>
          </div>
        </div>

        <div class="form-group row">
          <label class="control-label col-md-2">Level</label>
          <div class="col-md-4">
            <!-- <select class="errMsg form-control" id="salaryLevel" name="salaryLevel" required="">
              <option value="" disabled="" selected="">Pilih</option>
              <option value="NP">NP</option>
              <option value="L1">L1</option>
              <option value="L2">L2</option>
              <option value="L3">L3</option>
              <option value="L4">L4</option>
            </select>  -->
            <select class="errMsg form-control" id="salaryLevel" name="salaryLevel" required="">
              <option value="" disabled="" selected="">Pilih</option>
              <?php 
              $data   = $this->db->query('SELECT payroll_level, basic_salary FROM mst_payroll_level')->result();
              foreach ($data as $key => $value) {
              ?>
              <option value="<?php echo $value->payroll_level; ?>" data-basic='<?php echo $value->basic_salary; ?>'><?php echo $value->payroll_level; ?></option>
              <?php 
              }
              ?>
            </select> 
          </div>
          <label class="control-label col-md-2">Basic Salary</label>
          <div class="col-md-4">
            <input type="text" class="errMsg form-control currencyField" id="basicSalary" name="basicSalary" value="0" placeholder="Gaji Pokok" required="">
          </div>
        </div>

        <div class="form-group row">
          <label class="control-label col-md-2">Position Allowance</label>
          <div class="col-md-4">
            <input type="text" class="errMsg form-control currencyField" id="positionAllowance" name="positionAllowance" value="0" placeholder="Tunjangan Jabatan" required="">
          </div>
          <label class="control-label col-md-2">Housing & Transport Allowance</label>
          <div class="col-md-4">
            <input type="text" class="errMsg form-control currencyField" id="housingAllowance" name="housingAllowance" value="0" placeholder="Tunjangan Transport & Tinggal" required="">
          </div>
        </div>

        <div class="form-group row">
          <label class="control-label col-md-2">Is Remote Allowance</label>
          <div class="col-md-4">
            <select class="errMsg form-control" id="isRemoteLocation" name="isRemoteLocation" required="">
              <option value="" disabled="" selected="">Pilih</option>
              <option value="1">Ya</option>
              <option value="0">Tidak</option>
            </select> 
          </div>
          <label class="control-label col-md-2">Is Shift Bonus</label>
          <div class="col-md-4">
            <select class="errMsg form-control" id="isShiftBonus" name="isShiftBonus" required="">
              <option value="" disabled="" selected="">Pilih</option>
              <option value="1">Ya</option>
              <option value="0">Tidak</option>
            </select> 
          </div>
        </div>

        <div class="form-group row">
          <label class="control-label col-md-2">Is Allowance Economy</label>
          <div class="col-md-4">
            <select class="errMsg form-control" id="isAllowanceEconomy" name="isAllowanceEconomy" required="">
              <option value="" disabled="" selected="">Pilih</option>
              <option value="1">Ya</option>
              <option value="0">Tidak</option>
            </select> 
          </div>
          <label class="control-label col-md-2">Is Overtime Bonus</label>
          <div class="col-md-4">
            <select class="errMsg form-control" id="isOTBonus" name="isOTBonus" required="">
              <option value="" disabled="" selected="">Pilih</option>
              <option value="1">Ya</option>
              <option value="0">Tidak</option>
            </select> 
          </div>
        </div>

        <div class="form-group row">
          <label class="control-label col-md-2">Is Bonus Incentive</label>
          <div class="col-md-4">
            <select class="errMsg form-control" id="isIncentiveBonus" name="isIncentiveBonus" required="">
              <option value="" disabled="" selected="">Pilih</option>
              <option value="1">Ya</option>
              <option value="0">Tidak</option>
            </select> 
          </div>
          <label class="control-label col-md-2">Is Dev Incentive</label>
          <div class="col-md-4">
            <select class="errMsg form-control" id="isIncentiveDevBonus" name="isIncentiveDevBonus" required="">
              <option value="" disabled="" selected="">Pilih</option>
              <option value="1">Ya</option>
              <option value="0">Tidak</option>
            </select>  
          </div>
        </div>

        <div class="form-group row">
          <label class="control-label col-md-2">Is Production</label>
          <div class="col-md-4">
            <select class="errMsg form-control" id="isProductionBonus" name="isProductionBonus" required="">
              <option value="" disabled="" selected="">Pilih</option>
              <option value="1">Ya</option>
              <option value="0">Tidak</option>
            </select> 
          </div>
          <label class="control-label col-md-2">Is CC Payment</label>
          <div class="col-md-4">
            <select class="errMsg form-control" id="isCcPayment" name="isCcPayment" required="">
              <option value="" disabled="" selected="">Pilih</option>
              <option value="1">Ya</option>
              <option value="0">Tidak</option>
            </select> 
          </div>
        </div>

        <div class="form-group row">
          <label class="control-label col-md-2">Is Travel</label>
          <div class="col-md-4">
            <select class="errMsg form-control" id="isTravelBonus" name="isTravelBonus" required="">
              <option value="" disabled="" selected="">Pilih</option>
              <option value="1">Ya</option>
              <option value="0">Tidak</option>
            </select>  
          </div>
          <label class="control-label col-md-2">Is Overtime</label>
          <div class="col-md-4">
            <select class="errMsg form-control" id="isOverTime" name="isOverTime" required="">
              <option value="" disabled="" selected="">Pilih</option>
              <option value="1">Ya</option>
              <option value="0">Tidak</option>
            </select> 
          </div>
        </div>

        <div class="form-group row">
          <label class="control-label col-md-2">Bank Name</label>
          <div class="col-md-4">
            <select class="errMsg form-control" id="bankName" name="bankName" required="">
              <!-- <option value="" disabled="" selected="">Pilih</option>
              <option value="BCA">BCA</option>
              <option value="BNI">BNI</option>
              <option value="BNI SYARIAH">BNI SYARIAH</option>
              <option value="BRI">BRI</option>
              <option value="BRI SYARIAH">BRI SYARIAH</option>
              <option value="BTN">BTN</option>
              <option value="CIMB NIAGA">CIMB NIAGA</option>
              <option value="DANAMON">DANAMON</option>
              <option value="MANDIRI">MANDIRI</option>
              <option value="MEGA">MEGA</option>
              <option value="MAYBANK">MAYBANK</option>
              <option value="PAPUA">PAPUA</option>
              <option value="None">None</option> -->
            </select> 
          </div>
          <label class="control-label col-md-2">Account Name</label>
          <div class="col-md-4">
            <input type="text" class="errMsg form-control" id="accountName" name="accountName" placeholder="Nama Pemilik Rekening" required="">
          </div>
        </div>

        <div class="form-group row">
          <label class="control-label col-md-2">Account No</label>
          <div class="col-md-4">
            <input type="text" class="errMsg form-control" id="accountNo" name="accountNo" placeholder="Nomor Rekening" required="">
          </div>
          <label class="control-label col-md-2"></label>
          <div class="col-md-4">
            <!-- <input type="text" class="form-control" id="accountName" name="accountName" placeholder="Nama Pemilik Rekening" required=""> -->
          </div>
        </div>
        <div class="tile-footer">
          <!-- <input type="button" class="btn btn-primary" id="addBiodata" name="addBiodata" data-toggle="modal" data-target="#salaryModal" value="Add"> -->
          <input type="button" class="btn btn-primary" id="saveSalary" name="saveSalary" value="Save">
        </div>
      </form>
        <div class="tile-footer"></div>
        <div class="table-responsive">
          <table id="salaryTable" class="table table-hover table-bordered">
            <thead class="thead-dark">
              <tr role="row">
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 120px;">Table Id</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 120px;">External Id</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 160px;">Name</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Payroll Group</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Client</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Level</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Basic Salary</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Bank Name</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 170px;">Account Name</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 170px;">Account No</th>
              </tr>
            </thead>                    
            <tbody>                      
            </tbody>
          </table>  
          <input type="button" class="btn btn-primary" id="viewData" name="viewData" value="Display Data">
          <input type="button" class="btn btn-primary" id="deleteData" name="deleteData" value="Delete Data">
        </div>
    </div>
  </div>
</div>

    
<!-- Start salaryModal UPDATE DATA  -->
<div class="modal fade" id="salaryModal" tabindex="-1" role="dialog" aria-labelledby="salaryModalLabel"> 
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
        <h4 class="modal-title" id="salaryModalLabel">Salary</h4>
      </div>
      <div>
        <div id="detail" class="panel panel-body panelButtonDetail">
          <div class="table-responsive">
            <table id="biodataTable" class="table table-hover table-bordered">
              <thead class="thead-dark">
                <tr role="row">
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 139px;">Id Internal</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 139px;">Id Eksternal</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 150px;">Nama</th>
                </tr>
              </thead>                    
              <tbody>       
              </tbody>
            </table>  
          </div>
        </div>
      </div> 
      <div class="modal-footer">
        <input type="button" class="btn btn-primary btn-sm" data-dismiss="modal" value="Close"> 
      </div>
    </div>
  </div> 
</div> 
<!-- End salaryModal UPDATE DATA -->
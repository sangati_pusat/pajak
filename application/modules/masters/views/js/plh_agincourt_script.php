<script type="text/javascript">
    
    $(document).ready(function(){    
		/* START BIODATA TABLE */	
		var plhTable = $('#plhTable').DataTable({
	            "paging":   true,
	            "ordering": false,
	            "info":     false,
	            "filter":   true
                
	        });
		/* END BIODATA TABLE */	  
        var biodataTable = $('#biodataTable').DataTable({
                "paging":   true,
                "ordering": false,
                "info":     false,
                "filter":   true
        });
        /* END BIODATA TABLE */
        var namektpTable = $('#namektpTable').DataTable({
                "paging":   true,
                "ordering": false,
                "info":     false,
                "filter":   true
        });

        $("#editPlh").attr('disabled',true);
        $("#addPlh").attr('disabled',false);

        /* START LOAD DATA FROM DATABASE TO TABLE */
       	$.ajax({
    		method : "POST",
    		 url : "<?php echo base_url() ?>"+"masters/Mst_plh/loadPlhData", 
    		data : {
    			bioRecIdPlh :""
    		},
    		success : function(data){
				plhTable.clear().draw();
				var dataSrc = JSON.parse(data);	
				plhTable.rows.add(dataSrc).draw(false);
				// alert("Succeed");	
    		},
    		error : function(){
    			alert("Failed Load Data");
    		}
    	});
        /* END LOAD DATA FROM DATABASE TO TABLE */


        /* END DETAIL GLOBAL */ 
        var isDuplicateId = false;
        var confirmUpdate = false;
        $('#idCardNoDuplicate').hide();
        $('#idCardNoDigit').hide();
        $('#idCardNo').on('focusout', function(){        
          var idCardNo = $('#idCardNo').val();
          var myUrl = "<?php echo base_url() ?>"+"masters/Mst_bio_rec/getIdNo/"+idCardNo;
          var idLength = idCardNo.length;     

              $.ajax({
                  method : "POST",
                  url : myUrl,
                  data : {
                      idCardNo : idCardNo
                  },
                  success : function(data){
                      // debugger
                      $('#idCardNoDigit').hide(); 
                      if(idLength != 16){
                        $('#idCardNo').focus();
                        $('#idCardNoDigit').show(); 
                      } else {
                        isDuplicateId = false;
                        $('#idCardNoDuplicate').hide(); 
                        if(data > 0){                          
                          /* Confirmation Data Update */
                          confirmUpdate = confirm('Duplicate data, update?');
                          if(confirmUpdate == true) {
                            window.location.href="<?php echo base_url() ?>"+"home/detail/clr/"+idCardNo;                     
                          } else {
                            $('#idCardNo').focus();  
                          }

                          $('#idCardNoDuplicate').show();
                          isDuplicateId = true;
                        }
                      } 
                  }, 
                  error : function(data){
                      alert('Failed');
                  }
              });

        });


        /* START LOAD BIODATA  */
        $("#addPlh").on("click", function(){
            $("#bioRecIdPlh").val("");
            // $("#loanDate").val("<?php #echo date("Y-m-d") ?>");
            $.ajax({
                method : "POST",
                url : "<?php echo base_url() ?>"+"masters/Mst_plh",
                // url : "<?php #echo base_url() ?>"+"MstSalary/bio_rec_company", 
                data : {
                    id :""
                },
                success : function(data){
                    biodataTable.clear().draw();
                    var dataSrc = JSON.parse(data); 
                    biodataTable.rows.add(dataSrc).draw(false); 
                },
                error : function(){
                    alert("Failed");
                }
            });
        });
        /* END LOAD BIODATA  */      

        /* START SELECT BROWSE DATA */
        // var bioRecIdPlh = "";
        var rteNumber = "";
        var rowData = null;
        $('#biodataTable tbody').on( 'click', 'tr', function () {
            var rowData = biodataTable.row( this ).data();
            if ( $(this).hasClass('selected') ) 
            {
                $(this).removeClass('selected');
            }
            else 
            {
                biodataTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');                    
            }
            // bioRecIdPlh = rowData[0];
            rteNumber = rowData[0];
        }); 
        /* END SELECT BROWSE DATA */

        $("#chooseBiodata").on("click", function(){
            // $("#bioRecIdPlh").val(bioRecIdPlh);
            $("#rteNumber").val(rteNumber);
        });

        $(function(){
          $("#startDate").datepicker();
          $("#endDate").datepicker().on("change",function(){
            CalcDiff();
          });
        });

        function CalcDiff(){
          // debugger
          var date1 = new Date($('#startDate').val());
          var date2 = new Date($('#endDate').val());
          var timeDiff = Math.abs(date2.getTime() - date1.getTime());
          var diffDays = Math.ceil(timeDiff / (999 * 3600 * 24));
          if(date1 > date2){
              var newdate = $('#'+string).datepicker('getDate');
              newdate.setDate( (new Date(newdate)).getDate());
              if(string == 'first')
              {
                this.scnd = newdate;
                $('#second').datepicker('setDate', newdate);
                $("#workDayAmmount").html(0);
              }
          else
          {
            this.first = newdate;
            $('#first').datepicker('setDate', newdate);
            $("#workDayAmmount").html(0);
          }
                } else {
                  $("#workDayAmmount").val(diffDays);
                }
        }

        /* START LOAD BIODATA  */
        $("#fullName").on("click", function(){
            $("#fullName").val("");
            $.ajax({
                method : "POST",
                url : "<?php echo base_url() ?>"+"masters/Mst_bio_rec", 
                data : {
                    id :""
                },
                success : function(data){
                    namektpTable.clear().draw();
                    var dataSrc = JSON.parse(data); 
                    namektpTable.rows.add(dataSrc).draw(false); 
                },
                error : function(){
                    alert("Failed");
                }
            });
        });
        /* END LOAD BIODATA  */

        /* START SELECT BROWSE DATA */
        var fullName = "";
        var idCardNo = "";
        var position = "";
        var rowData = null;
        $('#namektpTable tbody').on( 'click', 'tr', function () {
            var rowData = namektpTable.row( this ).data();
            if ($(this).hasClass('selected')){
                $(this).removeClass('selected');
            }else{
                namektpTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');                    
            }
      
            fullName = rowData[0];
            idCardNo = rowData[1];
            position = rowData[2];
            $("#fullName").val(fullName);
            $("#idCardNo").val(idCardNo);
            $("#position").val(position);
            $('#salaryModal').modal('toggle');
        }); 
        /* END SELECT BROWSE DATA */


		/* START SAVE DATA */
        $("#savePlh").on("click", function(){
        	$(".errMsg").css({"border": "2px solid #ced4da"});     
        	// debugger
            var rteNumber        = $("#rteNumber").val();
            var idCardNo         = $("#idCardNo").val();
            var position         = $("#position").val();
            var bpjskes          = $("#bpjskes").val();
            var bpjstk           = $("#bpjstk").val();
            var badgeNo          = $("#badgeNo").val();
            var fullName         = $("#fullName").val();           
            var clientName       = $("#clientName").val();
            var startDate        = $('#startDate').val();
            var endDate          = $('#endDate').val();
            var workDayAmmount   = $('#workDayAmmount').val();
            var plhSalary        = $('#plhSalary').val();                    
            var yearPeriod       = $('#yearPeriod').val();
            var monthPeriod      = $('#monthPeriod').val();
        	
			var isValid = true;
			
            if($("#rteNumber").val() == "")
            {
                isValid = false;
                $("#rteNumber").css({"border": "2px solid red"});    
                $("#rteNumber").focus(); 
            }
            else if($('#idCardNo').val() == "")
            {
                isValid = false;
                $("#idCardNo").css({"border": "2px solid red"}); 
                $('#idCardNo').focus();
            }
            else if($('#position').val() == "")
            {
                isValid = false;
                $("#position").css({"border": "2px solid red"}); 
                $('#position').focus();
            }
            else if($('#bpjskes').val() == "")
            {
                isValid = false;
                $("#bpjskes").css({"border": "2px solid red"}); 
                $('#bpjskes').focus();
            }
            else if($('#bpjstk').val() == "")
            {
                isValid = false;
                $("#bpjstk").css({"border": "2px solid red"}); 
                $('#bpjstk').focus();
            }
            else if($('#badgeNo').val() == "")
            {
                isValid = false;
                $("#badgeNo").css({"border": "2px solid red"}); 
                $('#badgeNo').focus();
            }
            else if($("#fullName").val() == "")
			{
				isValid = false;
				$("#fullName").css({"border": "2px solid red"});	
				$("#fullName").focus();	
			}
            else if($('#clientName option:selected').text() == "Select")
            {
                isValid = false;
                $("#clientName").css({"border": "2px solid red"}); 
                $('#clientName').focus();
            } 
			
            else if($('#startDate').val() == "")
            {
                isValid = false;
                $("#startDate").css({"border": "2px solid red"}); 
                $('#startDate').focus();
            }
            else if($('#endDate').val() == "")
            {
                isValid = false;
                $("#endDate").css({"border": "2px solid red"}); 
                $('#endDate').focus();
            }
			else if($('#workDayAmmount').val() == "")
			{
				isValid = false;
				$("#workDayAmmount").css({"border": "2px solid red"}); 
                $('#workDayAmmount').focus();
			}
            else if($('#plhSalary').val() == "")
            {
                isValid = false;
                $("#plhSalary").css({"border": "2px solid red"}); 
                $('#plhSalary').focus();
            }
            else if($('#yearPeriod option:selected').text() == "Select")
            {
                isValid = false;
                $("#yearPeriod").css({"border": "2px solid red"}); 
                $('#yearPeriod').focus();
            }
            else if($('#monthPeriod option:selected').text() == "Select")
            {
                isValid = false;
                $("#monthPeriod").css({"border": "2px solid red"}); 
                $('#monthPeriod').focus();
            }
            
			if(isValid == false)
            {
                return false;
            }

			/* START LOAD DATA FROM DATABASE TO TABLE */
            var myUrl = "<?php echo base_url() ?>"+"masters/Mst_plh/ins";
	       	$.ajax({
        		method : "POST",
        		url : myUrl, 
        		data : {
                    // bioRecIdPlh      : bioRecIdPlh,
                    rteNumber        : rteNumber,       
                    idCardNo         : idCardNo,
                    position         : position,
                    bpjskes          : bpjskes,
                    bpjstk           : bpjstk,
                    badgeNo          : badgeNo,
                    fullName         : fullName,       
                    clientName       : clientName,
                    startDate        : startDate,
                    endDate          : endDate,
                    workDayAmmount   : workDayAmmount,
                    plhSalary        : plhSalary,
                    yearPeriod       : yearPeriod,
                    monthPeriod      : monthPeriod
                    
        		},
        		success : function(data){
                    alert("Data has been saved");
                    location.reload();
                },
                error : function(data){
                    isValid = false;
                    alert("Failed save data");
                }
        	});
	        /* END LOAD DATA FROM DATABASE TO TABLE */


			/* START ADD DATA TO DATABASE */
			/* END ADD DATA TO DATABASE */
        });
        /* END SAVE DATA */

        /* START SELECT CONTRACT DATA */
        var rowIdx = 0;
        var rowContractt = null;
        $('#plhTable tbody').on( 'click', 'tr', function () {
            var rowContractt = plhTable.row( this ).data();
            if ( $(this).hasClass('selected') ) 
            {
                $(this).removeClass('selected');
            }
            else 
            {
                plhTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');                    
            }
            bioRecIdPlh = rowContractt[0];
            
            $("#bioRecIdPlh").attr('disabled',false);
            $("#rteNumber").attr('disabled',true);
            $("#idCardNo").attr('disabled',true);
            $("#position").attr('disabled',true);
            $("#bpjskes").attr('disabled',true);
            $("#bpjstk").attr('disabled',true);
            $("#badgeNo").attr('disabled',true);
            $("#fullName").attr('disabled',true);
            $("#clientName").attr('disabled',true);
            $("#startDate").attr('disabled',false);
            $("#endDate").attr('disabled',false);
            $("#workDayAmmount").attr('disabled',true);
            $("#plhSalary").attr('disabled',false);
            $("#yearPeriod").attr('disabled',false);
            $("#monthPeriod").attr('disabled',false);
            $("#savePlh").attr('disabled',true);
            $("#editPlh").attr('disabled',false);
            $("#addPlh").attr('disabled',true);

            rowIdx = plhTable.row( this ).index();
            bioRecIdPlh      = rowContractt['0'];
            rteNumber        = rowContractt['1'];
            idCardNo         = rowContractt['2'];
            position         = rowContractt['3'];
            bpjskes          = rowContractt['4'];
            bpjstk           = rowContractt['5'];
            badgeNo          = rowContractt['6'];
            fullName         = rowContractt['7'];
            clientName       = rowContractt['8'];
            startDate        = rowContractt['9'];
            endDate          = rowContractt['10'];
            workDayAmmount   = rowContractt['11'];
            plhSalary        = rowContractt['12'];
            yearPeriod       = rowContractt['13'];
            monthPeriod      = rowContractt['14'];
            
            $('#bioRecIdPlh').val(bioRecIdPlh);
            $('#rteNumber').val(rteNumber);
            $('#idCardNo').val(idCardNo);
            $('#position').val(position);
            $('#bpjskes').val(bpjskes);
            $('#bpjstk').val(bpjstk);
            $('#badgeNo').val(badgeNo);
            $('#fullName').val(fullName);
            $('#clientName').val(clientName);
            $('#startDate').val(startDate);
            $('#endDate').val(endDate);
            $('#workDayAmmount').val(workDayAmmount);
            $('#plhSalary').val(plhSalary);
            $('#yearPeriod').val(yearPeriod);
            $('#monthPeriod').val(monthPeriod);

            // alert(contractId);
        }); 

        /* END SELECT CONTRACT DATA */

        /* START EDIT CONTRACT */
        $("#editPlh").on("click", function(){
        $(".errMsg").css({"border": "2px solid #ced4da"});
        // debugger
    
          
            var bioRecIdPlh    = $("#bioRecIdPlh").val(); 
            var startDate      = $("#startDate").val(); 
            var endDate        = $("#endDate").val(); 
            var workDayAmmount = $("#workDayAmmount").val(); 
            var plhSalary      = $("#plhSalary").val(); 
            var yearPeriod     = $("#yearPeriod").val(); 
            var monthPeriod    = $("#monthPeriod").val(); 

            var isValid    = true;
            
            if($('#startDate').val() == "")
            {
                isValid = false;
                $("#startDate").css({"border": "2px solid red"}); 
                $('#startDate').focus();
            }
            else if($('#endDate').val() == "")
            {
                isValid = false;
                $("#endDate").css({"border": "2px solid red"}); 
                $('#endDate').focus();
            }
            else if($('#workDayAmmount').val() == "")
            {
                isValid = false;
                $("#workDayAmmount").css({"border": "2px solid red"}); 
                $('#workDayAmmount').focus();
            }
            else if($('#plhSalary').val() == "")
            {
                isValid = false;
                $("#plhSalary").css({"border": "2px solid red"}); 
                $('#plhSalary').focus();
            }
            else if($('#yearPeriod option:selected').text() == "Select")
            {
                isValid = false;
                $("#yearPeriod").css({"border": "2px solid red"}); 
                $('#yearPeriod').focus();
            }
            else if($('#monthPeriod option:selected').text() == "Select")
            {
                isValid = false;
                $("#monthPeriod").css({"border": "2px solid red"}); 
                $('#monthPeriod').focus();
            }


            if(isValid == false)
            {
                return false;
            }
           
            /* START LOAD DATA FROM DATABASE TO TABLE */
            var myUrl = "<?php echo base_url() ?>"+"masters/Mst_plh/Upd/"+bioRecIdPlh; 
            $.ajax({
                method : "POST",
                url : myUrl, 
                data : {
                        
                    startDate      : startDate,
                    endDate        : endDate,
                    workDayAmmount : workDayAmmount,
                    plhSalary      : plhSalary,                   
                    yearPeriod     : yearPeriod,
                    monthPeriod    : monthPeriod
                },
                success : function(data){
                    /* START ADD DATA TO TABLE */
                    // clearInputs();
                    location.reload();               
                    alert("Data has been edited");
                    /* END ADD DATA TO TABLE */
                },
                error : function(data){
                    isValid = false;
                    alert("Failed Edit data");
                }
            });
            /* END LOAD DATA FROM DATABASE TO TABLE */
          

        });
        // END EDIT PLH

	});
</script>
<script type="text/javascript">
    $(document).ready(function(){
        var rowIdx = null;
        var nonBiodataIdList = $('#nonBiodataIdList').DataTable({
                "paging":   true,
                "ordering": false,
                "info":     false,
                "filter":   true
                // "columnDefs": [{
                //     "targets": -2,
                //     "data": null,
                //     "defaultContent": "<button class='btn btn-primary btn-xs btn_update' data-toggle='modal' data-target='#editPayrollModal'><span class='glyphicon glyphicon-edit'></span></button>"
                // },
                // {
                //     "targets": -1,
                //     "data": null,
                //     "defaultContent": "<button class='btn btn-primary btn-xs btn_print'><span class='glyphicon glyphicon-print'></span></button>"   
                // }]
            });

        /* START CLICK MAIN BUTTON */
        $("#viewNonBio").on("click", function(){
            // debugger
            var clientName = $("#clientName").val();
            var monthPeriod = $("#monthPeriod").val();
            var yearPeriod = $("#yearPeriod").val();
           

            var isValid = true;
            if($('#clientName option:selected').text() == "Pilih")
            {
                $("#clientName").focus();
                alert('Client Name cannot be empty  ');
                isValid = false;
            } 
            
            else if($('#monthPeriod option:selected').text() == "Pilih")
            {
                $("#monthPeriod").focus();
                alert('Month Period cannot be empty ');
                isValid = false;
            }            
            else if($('#yearPeriod option:selected').text() == "Pilih")
            {
                $("#yearPeriod").focus();
                alert('Year Period cannot be empty ');
                isValid = false;
            }

            if(isValid == false)
            {return false} 

            var myUrl = "<?php echo base_url() ?>"+"masters/Mst_roster_check/getNonBiodataIdList/"+clientName+"/"+monthPeriod+"/"+yearPeriod;
            // alert(myUrl); 

            $.ajax({
                method  : "POST",
                url : myUrl,
                data    : {
                    clientName      : clientName,
                    monthPeriod     : monthPeriod,
                    yearPeriod      : yearPeriod
                    
                },
                 success : function(data){
                    nonBiodataIdList.clear().draw();
                    var dataSrc = JSON.parse(data);                 
                    nonBiodataIdList.rows.add(dataSrc).draw(false);                  
                },
                error   : function(data){
                    // alert("Failed");
                    alert(data);
                
                }

            });
        });
        /* END CLICK MAIN BUTTON */

        /* START SELECT DATA */
        $("#nonBiodataIdList tbody").on("click", "tr", function(){
            var rowData = nonBiodataIdList.row( this ).data();
            if ( $(this).hasClass('selected') ) 
            {
                $(this).removeClass('selected');
            }
            else 
            {
                nonBiodataIdList.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');                    
            }
            rowIdx = nonBiodataIdList.row( this ).index();
            $("#rowIdx").val(rowIdx); 
        });
        /* END SELECT DATA */       
        
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        var contractId = "";
        /* START BIODATA TABLE */   
        var biodataTable = $('#biodataTable').DataTable({
                "paging":   true,
                "ordering": false,
                "info":     false,
                "filter":   true
        });
        /* END BIODATA TABLE */

        var contractTable = $('#contractTable').DataTable({
                "paging"         : true,
                "ordering"       : false,
                "info"           : false,
                "filter"         : true,
                "columnDefs"     : [{
                // "targets": -2,
                    // "data": null,
                    // "defaultContent": "<button class='btn btn-primary btn-xs btn_update' data-toggle='modal' data-target='#editPayrollModal'><span class='glyphicon glyphicon-edit'></span></button>"
                },

                {

                    "targets": -1,
                    "data": null,
                    "defaultContent": "<button class='btn btn-warning btn-xs btn_delete'><i class='fa fa-close'></i></button>"   
                }]
            });

       

        /* START DETAIL GLOBAL */   
        function clearInputs()
        {
            $('.contract_input :input').each(function(){
                $(this).val('');
            });            
        }
        /* END DETAIL GLOBAL */ 

        /* START LOAD DATA FROM DATABASE TO TABLE */
        var myUrl = "<?php echo base_url() ?>"+"masters/Contract/loadContractByClientEntry";
        // alert(myUrl);
        $.ajax({
            method : "POST",
             url : myUrl, 
            data : {
                id :""
            },
            success : function(data){
                contractTable.clear().draw();
                var dataSrc = JSON.parse(data); 
                contractTable.rows.add(dataSrc).draw(false);
            },
            error : function(){
                alert("Failed Load Data");
            }
        });
        /* END LOAD DATA FROM DATABASE TO TABLE */

        /* START LOAD BIODATA  */
        // myUrl = "<?php echo base_url() ?>"+"masters/Contract/getBioCompany";
        // $("#addBiodata").on("click", function(){
        //     $("#bioRecId").val("");
        //     $.ajax({
        //         method : "POST",
        //         url : myUrl, 
        //         data : {
        //             id :""
        //         },
        //         success : function(data){
        //             biodataTable.clear().draw();
        //             var dataSrc = JSON.parse(data); 
        //             biodataTable.rows.add(dataSrc).draw(false); 
        //         },
        //         error : function(){
        //             alert("Failed");
        //         }
        //     });
        // });
        /* END LOAD BIODATA  */
        $("#remarks").attr('disabled',true);    
        $("#editContract").attr('disabled',true);

        /* START SELECT BROWSE DATA */
        var internalId = "";
        var name = "";
        var rowData = null;
        $('#biodataTable tbody').on( 'click', 'tr', function () {
            var rowData = biodataTable.row( this ).data();
            if ( $(this).hasClass('selected') ) 
            {
                $(this).removeClass('selected');
            }
            else 
            {
                biodataTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');                    
            }
            internalId = rowData[0];
            name = rowData[2];
        }); 
        /* END SELECT BROWSE DATA */

        $("#chooseBiodata").on("click", function(){
            $("#bioId").val(internalId);
            $("#empName").val(name);
        });

        // debugger
        var client  = "<?php echo $this->session->userdata('hris_user_group'); ?>";
        $.ajax({
            url : "<?php echo base_url() ?>"+"masters/Mst_client/loadAll",
            method : "POST",
            async : false,
            dataType : 'json',
            success: function(data){
            var html = '';
            var i;
                if(client=='Pusat'){
                    html += '<option value="" disabled="" selected="">Select</option>';
                    for(i=0; i<data.length; i++){
                      html += '<option value="'+data[i].client_value+'" >'+data[i].client_name+'</option>';
                    }
                }else{
                    html += '<option value="'+client+'" >'+client+'</option>';
                }
                    
            $('#ptName').html(html);
            }
        });

        /* START SELECT CONTRACT DATA */
        var rowIdx = 0;
        var rowContractt = null;
        $('#contractTable tbody').on( 'click', 'tr', function () {
            var rowContractt = contractTable.row( this ).data();
            if ( $(this).hasClass('selected') ) 
            {
                $(this).removeClass('selected');
            }
            // else 
            // {
            //     contractTable.$('tr.selected').removeClass('selected');
            //     $(this).addClass('selected');                    
            // }
            $("#bioId").attr('disabled',true);
            $("#empName").attr('disabled',true);
            $("#contractNo").attr('disabled',true);
            $("#deptName").attr('disabled',true);
            // $("#basicSalary").attr('disabled',true);
            $("#contractEnd").attr('disabled',false);
            $("#contractStart").attr('disabled',false);
            $("#position").attr('disabled',true);
            $("#ptName").attr('disabled',true);
            $("#remarks").attr('disabled',false);
            $("#addBiodata").attr('disabled',true);
            $("#saveContract").attr('disabled',true);
            $("#editContract").attr('disabled',false);

            rowIdx = contractTable.row( this ).index();
            bioId           = rowContractt['0'];
            nie             = rowContractt['1'];
            empName         = rowContractt['2'];
            ptName          = rowContractt['3'];
            deptName        = rowContractt['4'];
            position        = rowContractt['5'];
            contractNo      = rowContractt['6'];
            contractStart   = rowContractt['7'];
            contractEnd     = rowContractt['8'];
            
            $('#bioId').val(bioId);
            $('#empName').val(empName);
            $('#contractNo').val(contractNo);
            $('#deptName').val(deptName);
            $('#position').val(position);
            $('#contractStart').val(contractStart);
            $('#contractEnd').val(contractEnd);
            $('#ptName').val(ptName);

            // alert(contractId);
        }); 
        /* END SELECT CONTRACT DATA */

        /* START SAVE DATA */
        $("#saveContract").on("click", function(){
            $(".errMsg").css({"border": "2px solid #ced4da"}); 
            // var contractId = $("#contractId").val();
            var bioId = $("#bioId").val();
            var empName = $("#empName").val();
            var contractNo = $('#contractNo').val();
            var deptName = $('#deptName').val();
            var position = $('#position').val();
            var contractStart = $('#contractStart').val();     
            var contractEnd = $('#contractEnd').val();
            var ptName = $("#ptName").val();              
                              
            var isValid = true;
            if($("#bioId").val() == "")
            {
                isValid = false;
                $("#bioId").css({"border": "2px solid red"});
                $("#bioId").focus();    
            }

            else if($("#empName").val() == "")
            {
                isValid = false;
                $("#empName").css({"border": "2px solid red"});
                $("#empName").focus();  
            }

            else if($("#contractNo").val() == "")
            {
                isValid = false;
                $("#contractNo").css({"border": "2px solid red"});   
                $("#contractNo").focus();  
            }

            else if($('#deptName option:selected').text() == "Select")
            {
                isValid = false;
                $("#deptName").css({"border": "2px solid red"});    
                $("#deptName").focus(); 
            }

            else if($("#position").val() == "")
            {
                isValid = false;
                $("#position").css({"border": "2px solid red"});     
                $("#position").focus();  
            }

            else if ($('#contractStart').val() == "") 
            {
                isValid = false;
                $('#contractStart').css({"border": "2px solid red"}); 
                $('#contractStart').focus();
                
            }

            else if ($('#contractEnd').val() == "") 
            {
                isValid = false;
                $('#contractEnd').css({"border": "2px solid red"});  
                $('#contractEnd').focus();
                // isHeaderValid = false;
            }

            else if($('#ptName option:selected').text() == "Select")
            {
                isValid = false;
                $("#ptName").css({"border": "2px solid red"});  
                $("#ptName").focus();   
            }
            
            

            if(isValid == false)
            {
                return false;
            }

            /* START LOAD DATA FROM DATABASE TO TABLE */

            var myUrl = "<?php echo base_url() ?>"+"masters/Contract/ins"; 
            // alert(myUrl);
            $.ajax({
                method : "POST",
                url : myUrl, 
                data : {
                    bioId : bioId,
                    empName : empName,
                    contractNo : contractNo,
                    deptName : deptName,
                    position : position,
                    contractStart : contractStart,
                    contractEnd : contractEnd,   
                    ptName : ptName
                                    
                },
                success : function(data){
                    alert("Data has been saved");
                    location.reload();
                },
                error : function(data){
                    isValid = false;
                    alert("Failed save data");
                }
            });
            /* END LOAD DATA FROM DATABASE TO TABLE */

            if(isValid == false)
            {
                return false;
            }


            /* START ADD DATA TO DATABASE */
            /* END ADD DATA TO DATABASE */
        });
        /* END SAVE DATA */

        /* START VIEW DATA */
        $("#viewData").on("click", function(){
            /* START LOAD DATA FROM DATABASE TO TABLE */
            $.ajax({
                method : "POST",
                 url : "<?php echo base_url() ?>"+"Contract", 
                data : {
                    id :""
                },
                success : function(data){
                    contractTable.clear().draw();
                    var dataSrc = JSON.parse(data); 
                    contractTable.rows.add(dataSrc).draw(false);
                },
                error : function(){
                    alert("Failed Load Data");
                }
            });
            /* END LOAD DATA FROM DATABASE TO TABLE */
        });
        /* END VIEW DATA */

        /* START SELECT CONTRACT DATA */       
        var rowContract = null;
        $('#contractTable tbody').on( 'click', 'tr', function () {
            var rowContract = contractTable.row( this ).data();
            if ( $(this).hasClass('selected') ) 
            {
                $(this).removeClass('selected');
            }
            else 
            {
                contractTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');                    
            }
            contractId = rowContract[0];
            // alert(contractId);
        }); 
        /* END SELECT CONTRACT DATA */


         /* START DELETE DATA CONTRACT  */
        var is_valid = false;   
        $("#contractTable").on('click', '.btn_delete', function(){
            var idDelete = contractId; 
            if(idDelete == "")
            {
                alert("Please select data first");
                return false;
            }

            is_valid = confirm('Data : '+idDelete+' will be deleted?');
            if (is_valid) 
            {
                idDelete = "";
                contractTable.row('.selected').remove().draw( false );

                /* START AJAX DELETE */
                $.ajax({
                    method : "POST",
                    url : "<?php echo base_url() ?>"+"masters/Contract/del", 
                    data : {
                        idDelete : contractId
                    },
                    success : function(data){
                        alert('Data has been deleted');
                        location.reload();
                    },
                    error : function(){
                        alert("Delete data failed");
                    }

                });
                /* END AJAX DELETE */
            }
        });
              
        /* END DELETE DATA CONTRACT  */ 

        /* START LOAD BIODATA  */
        $("#addBiodata").on("click", function(){
            $("#bioId").val("");
            // $("#loanDate").val("<?php #echo date("Y-m-d") ?>");
            $.ajax({
                method : "POST",
                url : "<?php echo base_url() ?>"+"masters/Mst_salary",
                // url : "<?php #echo base_url() ?>"+"MstSalary/bio_rec_company", 
                data : {
                    id :""
                },
                success : function(data){
                    biodataTable.clear().draw();
                    var dataSrc = JSON.parse(data); 
                    biodataTable.rows.add(dataSrc).draw(false); 
                },
                error : function(){
                    alert("Failed");
                }
            });
        });
        /* END LOAD BIODATA  */

        
        /* START SELECT BROWSE DATA */
        var internalId = "";
        var name = "";
        var rowData = null;
        $('#contractTable tbody').on( 'click', 'tr', function () {
            var rowData = biodataTable.row( this ).data();
            if ( $(this).hasClass('selected') ) 
            {
                $(this).removeClass('selected');
            }
            else 
            {
                biodataTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');                    
            }
            internalId = rowData[0];
            name = rowData[1];
            // company_name = rowData[2];
            // company_name = rowData[2];
        }); 
        /* END SELECT BROWSE DATA */

        $("#chooseBiodata").on("click", function(){
            $("#bioId").val(internalId);
            $("#empName").val(name);
            // $("#clientName").val(company_name);
        });

        /* START EDIT CONTRACT */
        $("#editContract").on("click", function(){
            $(".errMsg").show();
        // debugger
            // var contractId = $("#contractId").val();
            var bioId           = $("#bioId").val();
            var contractNo      = $('#contractNo').val();
            var contractEnd     = $('#contractEnd').val();
            var contractStart   = $('#contractStart').val();
            var remarks         = $("#remarks").val(); 

            var isValid    = true;
            if($("#bioId").val() == "")
            {
                isValid = false;
                $("#bioIdErr").show();  
                $("#bioId").focus();    
            }

            else if ($('#contractEnd').val() == "") 
            {
                $('#contractEnd').focus(); 
                $('#contractEndErr').show();
                isValid = false;
            }

            else if ($('#contractStart').val() == "") 
            {
                $('#contractStart').focus(); 
                $('#contractStartErr').show();
                isValid = false;
            }

            else if($('#remarks option:selected').text() == "Pilih")
            {
                isValid = false;
                $("#remarksErr").show(); 
                $("#remarks").focus();   
            }

            
            // alert(contractStart);
            // if(isValid == false)
            // {
            //     return false;
            // }
            /* START LOAD DATA FROM DATABASE TO TABLE */
            var myUrl = "<?php echo base_url() ?>"+"masters/Contract/Upd"; 
            $.ajax({
                method : "POST",
                url : myUrl, 
                data : {
                    bioId        : bioId,
                    contractEnd  : contractEnd,                 
                    contractStart  : contractStart,                 
                    remarks      : remarks                   
                },
                success : function(data){
                    /* START ADD DATA TO TABLE */
                    clearInputs();
                     location.reload();
                    // var dataSrc = JSON.parse(data);
                    // contractTable.row.add([bioId, contractEnd, contractStart, remarks]).draw(false);                 
                    alert("Data has been edited");
                    /* END ADD DATA TO TABLE */
                },
                error : function(data){
                    isValid = false;
                    alert("Failed Edit data");
                }
            });
            /* END LOAD DATA FROM DATABASE TO TABLE */
          

        });
        // END EDIT CONTRACT


    })
</script>
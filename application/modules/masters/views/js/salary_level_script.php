<script type="text/javascript">
    
    $(document).ready(function(){

        table = $('#table-salary').DataTable({
            serverSide : true,
            processing: true,
            // columnDefs: [
            //     {
            //         "targets": [0,1,2,3,4,5,6],
            //         "searchable": false
            //     }],
            "ordering": false,
            ajax: {
                url: '<?php echo base_url() ?>'+'masters/Mst_salary/get_salaryall',
                type: "GET", 
                dataSrc: "data",
                data: function(d) { 
                    d.client = $("#ptName").val();
                }
            },
            columns : [
                {"data"   : "salary_id"},
                {"data"   : "nie"},
                {"data"   : "full_name"},
                {"data"   : "payroll_group"},
                {"data"   : "company_name"},
                {"data"   : "salary_level"},
                {"data"   : "basic_salary"},
                {"data"   : "pic_edit"},
                {"data"   : "edit_time"},
                {
                    "data"      : null,
                    className: 'action',
                    render: function(idx, typ, dt) {
                        var button = '';
                        return button +
                            '<button class="btn btn-outline-info" type="button" id="updateSalary" data-nie="'+dt.nie+'" data-full_name="'+dt.full_name+'" data-salary_level="'+dt.salary_level+'" data-basic_salary="'+dt.basic_salary+'" data-company_name="'+dt.company_name+'" data-toggle="modal" data-target="#levelSalaryModal">Update</button>'
                            // '<button class="btn btn-outline-info" type="button" id="updateSalary" data-nie="'+dt.nie+'" data-fullName="'+dt.full_name+'" data-salaryLevel="'+dt.salary_level+'" data-basicSalary="'+dt.basic_salary+'" data-toggle="modal" data-target="#levelSalaryModal">Update</button>'
                    }
                }
            ]
        });

        $.ajax({
            url : "<?php echo base_url() ?>"+"masters/Mst_client/loadAll",
            method : "POST",
            async : false,
            dataType : 'json',
            success: function(data){
            var html = '';
            var i;
                // html += '<option value="" disabled="" selected="">Select</option>';
                for(i=0; i<data.length; i++){
                    if(data[i].client_value=='Redpath_Timika'){
                        html += '<option value="'+data[i].client_value+'" selected="">'+data[i].client_name+'</option>';
                    }else{
                        html += '<option value="'+data[i].client_value+'" >'+data[i].client_name+'</option>';
                    }
                }   
            $('#ptName').html(html);
            }
        });

        $('#searchClient').click(function(ev){ 
            ev.preventDefault();
            var ptName  = $('#ptName').val();
            table.ajax.reload();
            
        });

        $('#salaryLevel').click(function(){
            var salary = $('#salaryLevel option:selected').attr('data-basic');
            $('#basicSalary').val(salary);
        });


        $('#table-salary').on('click','#updateSalary', function (e) {
            var idNie       = $(this).data('nie');
            var fullName    = $(this).data('full_name');
            var salaryLevel = $(this).data('salary_level');
            var basicSalary = $(this).data('basic_salary');
            var companyName= $(this).data('company_name');

            $('#bioId').val(idNie);
            $('#nie').val(idNie);
            $('#clientName').val(companyName);
            $('#fullName').val(fullName);
            $('#basicSalary').val(basicSalary);
            $('#salaryLevel').val(salaryLevel);

            if(companyName=='Redpath_Timika'){
                $('#salaryLevel').removeAttr('disabled');
                $('#basicSalary').attr('disabled','disabled');
            }else{
                $('#basicSalary').removeAttr('disabled');
                $('#salaryLevel').attr('disabled','disabled');

            }
        });

		/* START SAVE DATA */
        $("#saveSalary").on("click", function(){
        	$(".errMsg").css({"border": "2px solid #ced4da"});

            var id_eksternal    = $('#bioId').val();
            var client_name     = $('#clientName').val();
            var full_name       = $('#fullName').val();
            var basic_salary    = $('#basicSalary').val();
            var salary_level    = $('#salaryLevel').val();
            var nie             = $('#nie').val();
        	
			var isValid = true;
			if($("#basicSalary").val() == "0")
			{
				isValid = false;
				$("#basicSalary").css({"border": "2px solid red"}); 	
				$("#basicSalary").focus();	
			}

			if(isValid == false)
			{
				return false;
			}

			/* START LOAD DATA FROM DATABASE TO TABLE */
            var myUrl = "<?php echo base_url() ?>"+"masters/Mst_salary/updateSalaryLevel";
	       	$.ajax({
        		method : "POST",
        		url : myUrl, 
        		data : {
        			id_eksternal : id_eksternal,
		        	client_name  : client_name,
		        	full_name    : full_name,
		        	basicSalary  : basic_salary,
		        	salaryLevel  : salary_level,
                    nie          : nie
        		},
        		success : function(data){
					/* START ADD DATA TO TABLE */
    				alert("Data has been saved");
                    // location.reload();
        			// alert(data);
        		},
        		error : function(data){
        			isValid = false;
        			alert("Save failed");
        		}
        	});
	        /* END LOAD DATA FROM DATABASE TO TABLE */

			if(isValid == false)
			{
				return false;
			}


			/* START ADD DATA TO DATABASE */
			/* END ADD DATA TO DATABASE */
        });
        /* END SAVE DATA */

        function clearitem(){
            $(".errMsg").val('');
        }

        /* START VIEW DATA */
        $("#viewData").on("click", function(){
        	/* START LOAD DATA FROM DATABASE TO TABLE */
	       	$.ajax({
	    		method : "POST",
	    		 url : "<?php echo base_url() ?>"+"masters/Mst_salary/loadSalaryData", 
	    		data : {
	    			id :""
	    		},
	    		success : function(data){
					salaryTable.clear().draw();
					var dataSrc = JSON.parse(data);	
					salaryTable.rows.add(dataSrc).draw(false);
					// alert("Succeed");	
	    		},
	    		error : function(){
	    			alert("Failed Load Data");
	    		}
	    	});
	        /* END LOAD DATA FROM DATABASE TO TABLE */
        });
        /* END VIEW DATA */

        /* START SELECT SALARY DATA */
        var salaryId = "";
        var rowSalary = null;
        $('#salaryTable tbody').on( 'click', 'tr', function () {
            var rowSalary = salaryTable.row( this ).data();
            if ( $(this).hasClass('selected') ) 
            {
                $(this).removeClass('selected');
            }
            else 
            {
                salaryTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');                    
            }
            salaryId = rowSalary[0];
        }); 
        /* END SELECT SALARY DATA */


	});
</script>
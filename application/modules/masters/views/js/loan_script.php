<script type="text/javascript">
    $(document).ready(function(){
        var loanId = "";
        /* START BIODATA TABLE */   
        var biodataTable = $('#biodataTable').DataTable({
                "paging":   true,
                "ordering": false,
                "info":     false,
                "filter":   true
            });
        /* END BIODATA TABLE */

        var debtBrdTable = $('#debtBrdTable').DataTable({
                "paging":   true,
                "ordering": false,
                "info":     false,
                "filter":   true,
                "columnDefs": [{
                    // "targets": -2,
                    // "data": null,
                    // "defaultContent": "<button class='btn btn-primary btn-xs btn_update' data-toggle='modal' data-target='#editPayrollModal'><span class='glyphicon glyphicon-edit'></span></button>"
                },
                {
                    "targets": -1,
                    "data": null,
                    "defaultContent": "<button class='btn btn-warning btn-xs btn_print'><i class='fa fa-print'></i></button>"   
                }]
            });

        /* START DETAIL GLOBAL */   
        function clearInputs()
        {
            $('.contract_input :input').each(function(){
                $(this).val('');
            });            
        }
        /* END DETAIL GLOBAL */ 

        /* START LOAD DATA FROM DATABASE TO TABLE */
        var myUrl = "<?php echo base_url() ?>"+"masters/Loan/loadDebtBrd";
        //alert(myUrl); 
        $.ajax({
            method : "POST",
             url : myUrl, 
            data : {
                id :""
            },
            success : function(data){
                debtBrdTable.clear().draw();
                var dataSrc = JSON.parse(data); 
                debtBrdTable.rows.add(dataSrc).draw(false);
            },
            error : function(){
                alert("Failed Load Data");
            }
        });
        /* END LOAD DATA FROM DATABASE TO TABLE */

        /* START LOAD BIODATA  */
        $("#addBiodata").on("click", function(){
            $("#idEksternalNo").val("");
            $("#loanDate").val("<?php echo date("Y-m-d") ?>");
            $.ajax({
                method : "POST",
                url : "<?php echo base_url() ?>"+"masters/Loan/getBioCompany",
                // url : "<?php #echo base_url() ?>"+"MstSalary/bio_rec_company", 
                data : {
                    id :""
                },
                success : function(data){
                    biodataTable.clear().draw();
                    var dataSrc = JSON.parse(data); 
                    biodataTable.rows.add(dataSrc).draw(false); 
                },
                error : function(){
                    alert("Failed");
                }
            });
        });
        /* END LOAD BIODATA  */

        /* START SELECT BROWSE DATA */
        var internalId = "";
        var externalId = "";
        var name = "";
        var rowData = null;
        $('#biodataTable tbody').on( 'click', 'tr', function () {
            var rowData = biodataTable.row( this ).data();
            if ( $(this).hasClass('selected') ) 
            {
                $(this).removeClass('selected');
            }
            else 
            {
                biodataTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');                    
            }
            internalId = rowData[0];
            name = rowData[1];
            company_name = rowData[2];
        }); 
        /* END SELECT BROWSE DATA */

        $("#chooseBiodata").on("click", function(){
            $("#bioRecId").val(internalId);
            $("#fullName").val(name);
            $("#clientName").val(company_name);
        });

        /*START JUMLAH PINJAMAN DIBAGI CICILAN = SISA */
        $("#numberOfMonths").on("change", function(){     
            var loanAmount = $("#loanAmount").val();
            var numberOfMonths = $("#numberOfMonths").val();
            var sisa = loanAmount/numberOfMonths;
            $("#amountPerMonth").val(sisa);
            // alert(loanAmount);
        });

        $("#loanAmount").on("change", function(){           
            var loanAmount = $("#loanAmount").val();
            var numberOfMonths = $("#numberOfMonths").val();
            var sisa = loanAmount/numberOfMonths;
            $("#amountPerMonth").val(sisa);
            // alert(loanAmount);
        });        
        /*END JUMLAH PINJAMAN DIBAGI CICILAN = SISA */

        /* START SELECT CONTRACT DATA */       
        var rowLoan = null;
        $('#debtBrdTable tbody').on( 'click', 'tr', function () {
            var rowLoan = debtBrdTable.row( this ).data();
            if ( $(this).hasClass('selected') ) 
            {
                $(this).removeClass('selected');
            }
            else 
            {
                debtBrdTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');                    
            }
            loanId = rowLoan[0];
            // alert(loanId);
        }); 
        /* END SELECT CONTRACT DATA */


        // $(".errMsg").hide();

        /* START SAVE DATA */
        $("#saveBrd").on("click", function(){

            $(".errMsg").css({"border": "2px solid #ced4da"}); 
            var bioRecId = $("#bioRecId").val();
            var fullName = $("#fullName").val();
            var clientName = $("#clientName").val();        
            var loanDate = $("#loanDate").val(); 
            var monthBegin = $("#monthBegin").val();
            var yearBegin = $("#yearBegin").val();
            var loanAmount = $("#loanAmount").val();
            var numberOfMonths = $("#numberOfMonths").val();
            var amountPerMonth = $("#amountPerMonth").val();
            var remaining = $("#remaining").val();
            var remarks = $("#remarks").val();

            var isValid = true;
           
            if($("#bioRecId").val() == "")
            {
                isValid = false;
                $("#bioRecId").css({"border": "2px solid red"});
                $("#bioRecId").focus();    
            }

            else if($("#fullName").val() == "")
            {
                isValid = false;
                $("#fullName").css({"border": "2px solid red"});  
                $("#fullName").focus();  
            }

            else if($("#clientName").val() == "")
            {
                isValid = false;
                $("#clientName").css({"border": "2px solid red"});    
                $("#clientName").focus();  
            }

            else if ($("#loanDate").val() == "") 
            {
                isValid = false;
                $("#loanDate").css({"border": "2px solid red"});
                $("#loanDate").focus();
                
            }

            else if ($('#monthBegin option:selected').text() == "Select")
            {
                isValid = false;
                $("#monthBegin").css({"border": "2px solid red"});
                $("#monthBegin").focus();
                
            }

            else if($('#yearBegin option:selected').text() == "Select")
            {
                isValid = false;
                $("#yearBegin").css({"border": "2px solid red"});   
                $("#yearBegin").focus();  
            }
            
            else if ($("#loanAmount").val() == "") 
            {
                isValid = false;
                $("#loanAmount").css({"border": "2px solid red"});
                $("#loanAmount").focus();               
            }

            else if($('#numberOfMonths option:selected').text() == "Select")
            {
                isValid = false;
                $("#numberOfMonths").css({"border": "2px solid red"}); 
                $("#numberOfMonths").focus();   
            }

            else if($("#amountPerMonth" ).val() == "") 
            {
                isValid = false;
                $("#amountPerMonth").css({"border": "2px solid red"}); 
                $("#amountPerMonth").focus();   
            }

            else if($("#remaining" ).val() == "") 
            {
                isValid = false;
                $("#remaining").css({"border": "2px solid red"});
                $("#remaining").focus();   
            }

            else if($("#remarks" ).val() == "") 
            {
                isValid = false;
                $("#remarks").css({"border": "2px solid red"}); 
                $("#remarks").focus();   
            }

            if(isValid == false)
            {
                return false;
            }
          

            /* START LOAD DATA FROM DATABASE TO TABLE */

            var myUrl = "<?php echo base_url() ?>"+"masters/Loan/ins";          
            $.ajax({
                method : "POST",
                url : myUrl, 
                data : {
                    loanId : loanId,
                    bioRecId : bioRecId,
                    fullName : fullName,
                    clientName : clientName,
                    loanDate : loanDate,
                    monthBegin : monthBegin,
                    yearBegin : yearBegin,
                    loanAmount : loanAmount,
                    numberOfMonths : numberOfMonths,
                    amountPerMonth : amountPerMonth,
                    remaining : remaining,
                    remarks : remarks

                },
                success : function(data){
                    alert("Data has been saved");
                    location.reload();
                },
                error : function(data){
                    isValid = false;
                    alert("Failed save data");
                }
            });
            /* END LOAD DATA FROM DATABASE TO TABLE */

            if(isValid == false)
            {
                return false;
            }

            /* START ADD DATA TO DATABASE */
            /* END ADD DATA TO DATABASE */
        });
        /* END SAVE DATA */

         /* START PRINT BUTTON CLICK */
        $('#debtBrdTable tbody').on( 'click', '.btn_print', function () {
            var data = debtBrdTable.row( $(this).parents('tr') ).data();
            var loanId = data[0]; 
            var bioRecId = data[1]; 
            var fullName = data[2];
            var clientName = data[3]; 
            var monthPeriod = data[4];
            var yearPeriod = data[5];
            var amount = data[6];
            

            var myUrl = "<?php echo base_url() ?>"+"masters/Loan/toDebtBrd/"+loanId+"/"+fullName+"/"+clientName;
            // alert(myUrl);
            $.ajax({
                method : "POST",
                url : myUrl,
                data : {
                    loanId : loanId,
                    bioRecId : bioRecId,
                    fullName : fullName,
                    clientName : clientName,
                    monthPeriod : monthPeriod,
                    yearPeriod : yearPeriod,
                    amount : amount 
                },
                success : function(response){
                    console.log(response);
                    window.open(myUrl,'_blank');
                },
                error : function(data){
                    alert("Failed");
                }   
            });
        });
        /* END PRINT BUTTON CLICK */


    })
</script>
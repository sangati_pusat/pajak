<script type="text/javascript">
  $(document).ready(function () {

    // $("#etnics").select2();
    // $("#groupPosition").select2();

    function clearDetailInputs()
    {
      $('.my_detail :input').each(function(){
          $(this).val('');
      });            
    }

  // fungsi acordion
      var $tabs = $('#horizontalTab');
      $tabs.responsiveTabs({
      active: 0,
      rotate: false,
      startCollapsed: 'accordion',
      collapsible: 'accordion',
      setHash: true,
        // disabled: [3,4],
      });

      $(function(){
          $("#startDate").datepicker();
          $("#endDate").datepicker().on("change",function(){
            CalcDiff();
          });
        });

        function CalcDiff(){
          // debugger
          var date1 = new Date($('#startDate').val());
          var date2 = new Date($('#endDate').val());
          var timeDiff = Math.abs(date2.getTime() - date1.getTime());
          var diffDays = Math.ceil(timeDiff / (999 * 3600 * 24));
          if(date1 > date2){
              var newdate = $('#'+string).datepicker('getDate');
              newdate.setDate( (new Date(newdate)).getDate());
              if(string == 'first')
              {
                this.scnd = newdate;
                $('#second').datepicker('setDate', newdate);
                $("#totalDate").html(0);
              }
          else
          {
            this.first = newdate;
            $('#first').datepicker('setDate', newdate);
            $("#totalDate").html(0);
          }
                } else {
                  $("#totalDate").val(diffDays);
                }
        }

  
        $.ajax({
            url : "<?php echo base_url() ?>"+"masters/Mst_position/getPosition",
            method : "POST",
            data : {
                id : ""
            },
            success : function(data){
                var dataSrc = JSON.parse(data); 
                var dataLength = dataSrc.length;
                $('#position').find('option:not(:first)').remove();
                for(var i = 0; i < dataLength; i++){
                    $('#position').append(new Option(dataSrc[i]['position_name'], dataSrc[i]['position_name']));
                }
            },
            error : function(){
                alert('Failed');
            }
        });

        /* Start Save EDUCATION Table */
        var manpowerPlanTable = $('#manpowerPlanTable').DataTable({
          "paging"        : false,
          "responsive"    : true,
          "ordering"      : false,
          "info"          : false,
          "filter"        : false,
          "columnDefs": [{
              "targets": -1,
              "data": null,
              "defaultContent": "<button class='btn btn-danger btn-sm btnDelete'><i class='fa fa-times' aria-hidden='true'></i></button>"
          }]
        });

        
        /* End Delete EDUCATION  */  
        /* END EDUCATION TABLE */  

        

        function clearHeaderInputs() {
          $('.my_header [type = text]').each(function(){
            $(this).val('');
          }); 
          $("#rteNumber").focus();  
          manpowerPlanTable.clear().draw();
        }

        /* Start Header Focus */        
        

        $('#saveBiodata').on('click', function(){
            $(".errMsg").css({"border": "2px solid #ced4da"});   
          //debugger
            /* Start Header Form Validation */
            $('#rteNumber').show();
            $('#dept').show();
            $('#dateRequired').show();
            $('#company').show();
            $('#labourSource').show();

            var isHeaderValid = true;

              if ($('#rteNumber').val() == "") {
                isHeaderValid = false;
                $("#rteNumber").css({"border": "2px solid red"});    
                $('#rteNumber').focus();
              }
              else if ($('#dept option:selected').text() == "Select") {
                isHeaderValid = false;
                $('#dept').css({"border": "2px solid red"}); 
                $('#dept').focus();
              }
              else if ($('#dateRequired').val() == "") {
                isHeaderValid = false;
                $('#dateRequired').css({"border": "2px solid red"});  
                $('#dateRequired').focus();
              }
              else if ($('#company option:selected').text() == "Select") {
                isHeaderValid = false;
                $('#company').css({"border": "2px solid red"});  
                $('#company').focus();
              }
              else if ($('#labourSource option:selected').text() == "Select") {
                isHeaderValid = false;
                $('#labourSource').css({"border": "2px solid red"}); 
                $('#labourSource').focus();
              }
             

              else if ($("#manpowerPlanTable").DataTable().row().count() == 0) {
                // alert'Empty table';
                alert( 'Data Table Harus Di Isi' );
                isHeaderValid = false;
              }
             

              if(isHeaderValid == false) {
                return false;
              }
             

              var headerDataArr = {
                rteNumber        : $('#rteNumber').val(),
                dept             : $('#dept').val(),
                dateRequired     : $('#dateRequired').val(),
                company          : $('#company').val(),
                labourSource     : $('#labourSource').val()  
                
              }
                /* Start POST EDUCATION Table tes*/

              var manpowerPlanDataArr = [];
              function getManpowerPlanDetails(){
                var dataCount = manpowerPlanTable.rows().data().length;
                for (var i = 0; i < dataCount; i++) {
                  var dataRow = manpowerPlanTable.row(i).data();
                    manpowerPlanDataArr.push ({
                        position        : dataRow[0],   
                        ratePerDay      : dataRow[1],   
                        totalLabour     : dataRow[2],   
                        startDate       : dataRow[3],   
                        endDate         : dataRow[4],  
                        totalDate       : dataRow[5],  
                        workLocation    : dataRow[6], 
                        remarks         : dataRow[7] 
                  });         
                } 
                return manpowerPlanDataArr;
              } 

              manpowerPlanArrData = getManpowerPlanDetails();
              /* End POST EDUCATION Table */


              // debugger

              $.ajax({                
                method : "POST", 
                url : "<?php echo base_url() ?>"+"masters/Mst_rte/Ins", 
                data : {
                  saveBiodata      : 1,
                  dataHeader       : headerDataArr,
                  dataManpowerPlan : manpowerPlanArrData
                                             
                }, 
                error   : function(data) {
                    alert(data);
                    // $("#saveMsg").html("Failed to save, please check for data duplication");       
                },
                success : function(data) {
                    clearHeaderInputs();
                    $("#saveMsg").html("<h5>RTE ID : </h5><div id='succeedId'><h3>"+data+"</h3></div><h5> Data Saved</h5>");       
                }
              });
              // table.clear().draw();  
              /* End Header Form Validation */
            });        
        /* End HEADER */

        /* Start Detail Validation */
        $("#addManpowerPlan").attr("disabled", true);
       
        $("#rteNumber").on("change", function(){
          var rteNumber = $("#rteNumber").val();    
          if(rteNumber.trim() != ""){
            $("#addManpowerPlan").attr("disabled", false);
            
          }   
          else{
            $("#addManpowerPlan").attr("disabled", true);
            
          }
        });
        /* End Detail Validation */
        $('#position').show();
        $('#ratePerDay').show();
        $('#totalLabour').show();
        $('#startDate').show();
        $('#endDate').show();
        $('#totalDate').show();
        $('#workLocation').show();
        $('#remarks').show();

        $('#saveManpowerPlan').click(function(){
       
          var isDataValid = true;
          $('#position').show();
          $('#ratePerDay').show();
          $('#totalLabour').show();
          $('#startDate').show();
          $('#endDate').show();
          $('#totalDate').show();
          $('#workLocation').show();
          $('#remarks').show();
          
          if($('#position').val() == "") 
          {
            $('#position').focus();
            $('#position').show();
            isDataValid = false;
          }
          else if($('#ratePerDay').val() == "") 
          {
            $('#ratePerDay').focus();
            $('#ratePerDay').show();
            isDataValid = false;
          }
          else if($('#totalLabour option:selected').text() == "Select") 
          {
            $('#totalLabour').focus();
            $('#totalLabour').show();
            isDataValid = false;
          }
          else if($('#startDate').val() == "") 
          {
            $('#startDate').focus();
            $('#startDate').show();
            isDataValid = false;
          }
          else if($('#endDate').val() == "") 
          {
            $('#endDate').focus();
            $('#endDate').show();
            isDataValid = false;
          }
          else if($('#totalDate').val() == "") 
          {
            $('#totalDate').focus();
            $('#totalDate').show();
            isDataValid = false;
          }
          else if($('#workLocation').val() == "") 
          {
            $('#workLocation').focus();
            $('#workLocation').show();
            isDataValid = false;
          }
          else if($('#remarks').val() == "") 
          {
            $('#remarks').focus();
            $('#remarks').show();
            isDataValid = false;
          }
          if(isDataValid == false)
          {
            return false;
          }
          /* End EDUCATION Form Validation */

          manpowerPlanTable.row.add([
            $('#position').val(),
            $('#ratePerDay').val(),
            $('#totalLabour').val(),
            $('#startDate').val(),
            $('#endDate').val(),
            $('#totalDate').val(),
            $('#workLocation').val(),
            $('#remarks').val()

          ]).draw(false);
          clearDetailInputs();  
          $('#position').focus();                              
        });
        /* End Save EDUCATION Table */

        /* Start Select EDUCATION */
        $('#manpowerPlanTable tbody').on( 'click', 'tr', function () {
          var rowData = manpowerPlanTable.row( this ).data();
          if ( $(this).hasClass('selected') ) 
          {
            $(this).removeClass('selected');
          }
          else 
          {
            manpowerPlanTable.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');                    
          }
        }); 
        /* End Select EDUCATION */

        /* Start Delete EDUCATION  */  
        var is_valid = false;
        $('#manpowerPlanTable tbody').on( 'click', '.btnDelete', function (){   
          is_valid = confirm('Are you sure to delete?');
          if (is_valid) 
          {
            manpowerPlanTable.row( $(this).parents('tr') ).remove().draw();
          }
        });

    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        var rowIdx = null;
        var plhReportsTable = $('#plhReportsTable').DataTable({
                "paging":   true,
                "ordering": false,
                "info":     false,
                "filter":   true
                // "columnDefs": [{
                //     "targets": -2,
                //     "data": null,
                //     "defaultContent": "<button class='btn btn-primary btn-xs btn_update' data-toggle='modal' data-target='#editPayrollModal'><span class='glyphicon glyphicon-edit'></span></button>"
                // },
                // {
                //     "targets": -1,
                //     "data": null,
                //     "defaultContent": "<button class='btn btn-primary btn-xs btn_print'><span class='glyphicon glyphicon-print'></span></button>"   
                // }]
            });
        // $('#dataProses').html('');
        $( function() {
            var dateFormat = "mm/dd/yy",
            from = $( "#startDate" )
                   .datepicker({
                    defaultDate: "+1w",
                    changeMonth: true,
                    numberOfMonths: 2
            })
            .on( "change", function() {
            to.datepicker( "option", "minDate", getDate( this ) );
            }),

            to   = $( "#endDate" )
                   .datepicker({
                    defaultDate: "+1w",
                    changeMonth: true,
                    numberOfMonths: 2
            })
            .on( "change", function() {
            from.datepicker( "option", "maxDate", getDate( this ) );
        });
     
        function getDate( element ) {
            var date;
                try {
                    date = $.datepicker.parseDate( dateFormat, element.value );
                } catch( error ) {
                    date = null;
                }
     
            return date;
        }

        $( "#awal" )
            .datepicker({
              defaultDate: "+1w",
              changeMonth: true,
              numberOfMonths: 2
            })
            .on( "change", function() {
              to.datepicker( "option", "minDate", getDate( this ) );
            })
        });

        /* START CLICK MAIN BUTTON */
        $("#viewPlhReports").on("click", function(){            
            $("#loader").show();
            // $('.errMsg').hide();
            $('#btnDisplay').prop('disabled', true);

            isValid = true;    
            if ($('#ptName option:selected').text() == "ALL DATA")
            {
                alert('Client Name cannot be empty');
                $('#ptName').focus();
                isValid = false;
            }
            if ($('#startDate option:selected').val() == "")
            {
                // alert('Month period cannot be empty');
                $('#startDate').focus();
                isValid = false;
            }
            if ($('#endDate option:selected').val() == "")
            {
                // alert('Year Period cannot be empty');
                $('#endDate').focus();
                isValid = false;
            } 

            var ptName      = $('#ptName').val();
            var startDate   = $('#startDate').val().substr(6, 4)+'-'+$('#startDate').val().substr(0, 2)+'-'+$('#startDate').val().substr(3, 2);
            var endDate     = $('#endDate').val().substr(6, 4)+'-'+$('#endDate').val().substr(0, 2)+'-'+$('#endDate').val().substr(3, 2);
            // debugger
            var myUrl = "<?php echo base_url() ?>"+"masters/Mst_plh/getListPlh/"+ptName+"/"+startDate+"/"+endDate;
            // alert(myUrl); 

            $.ajax({
                method  : "POST",
                url : myUrl,
                data    : {
                    ptName      : ptName,
                    startDate   : startDate,
                    endDate     : endDate
                    
                },
                 success : function(data){

                    $("#loader").hide();
                    $('#viewPlhReports').prop('disabled', false);
                    plhReportsTable.clear().draw();
                    var dataSrc = JSON.parse(data);                 
                    plhReportsTable.rows.add(dataSrc).draw(false);
                    
                },
                error   : function(data){
                    
                    $("#loader").hide();
                    $('#viewPlhReports').prop('disabled', false);
                    $.notify({
                        title: "<h5>Informasi : </h5>",
                        message: "<strong>"+data+"</strong> </br></br> ",
                        icon: '' 
                    },
                    {
                        type: "warning",
                        delay: 3000
                    }); 
                }

            });
        });/* $('#btnProsesPayroll').on('click', function(){ */
        /* END CLICK MAIN BUTTON */

        /* START SELECT DATA */
        $("#plhReportsTable tbody").on("click", "tr", function(){
            var rowData = plhReportsTable.row( this ).data();
            if ( $(this).hasClass('selected') ) 
            {
                $(this).removeClass('selected');
            }
            else 
            {
                plhReportsTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');                    
            }
            rowIdx = plhReportsTable.row( this ).index();
            $("#rowIdx").val(rowIdx); 
        });
        /* END SELECT DATA */
    
        /* START PRINT DATA */
        $('#printPlhReports').on('click', function(){
            isValid = true;    
            if ($('#ptName option:selected').text() == "ALL DATA")
            {
                alert('Client Name cannot be empty');
                $('#ptName').focus();
                isValid = false;
            }
            if ($('#startDate option:selected').val() == "")
            {
                alert('Month Period cannot be empty');
                $('#startDate').focus();
                isValid = false;
            }
            if ($('#endDate option:selected').val() == "")
            {
                alert('Year Period cannot be empty');
                $('#endDate').focus();
                isValid = false;
            }  

            var data        = plhReportsTable.row( $(this).parents('tr') ).data(); 

            var ptName      = $('#ptName').val();
            var startDate   = $('#startDate').val().substr(6, 4)+'-'+$('#startDate').val().substr(0, 2)+'-'+$('#startDate').val().substr(3, 2);
            var endDate     = $('#endDate').val().substr(6, 4)+'-'+$('#endDate').val().substr(0, 2)+'-'+$('#endDate').val().substr(3, 2);

            var myUrl       = "<?php echo base_url() ?>"+"masters/Mst_plh/exportPlh/"+ptName+"/"+startDate+"/"+endDate;
           
           // alert(myUrl);
            
            $.ajax({
                method : "POST",
                url : myUrl,
                data : {
                    ptName : ptName,
                    startDate : startDate,
                    endDate : endDate
                },
                success : function(response){
                    console.log(response);
                    window.open(myUrl,'_blank');
                },
                error : function(data){
                    $.notify({
                        title: "<h5>Informasi : </h5>",
                        message: "<strong>"+data+"</strong> </br></br> ",
                        icon: '' 
                    },
                    {
                        type: "warning",
                        delay: 3000
                    }); 
                }   
            });
        });
        
    });
</script>
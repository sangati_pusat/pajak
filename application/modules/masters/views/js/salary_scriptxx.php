<script type="text/javascript">
    
    $(document).ready(function(){

		/* START BIODATA TABLE */	
		var biodataTable = $('#biodataTable').DataTable({
	            "paging":   true,
	            "ordering": false,
	            "info":     false,
	            "filter":   true
	        });
		/* END BIODATA TABLE */

        // Start Master Bank
        $.ajax({
            url : "<?php echo site_url('masters/Mst_bank/selectBank');?>",
            method : "POST",
            // data : {id: id},
            async : true,
            dataType : 'json',
            success: function(data){
                // debugger
                var html = '';
                var i;
                html += '<option disabled="" value="" data-code="" data-nama="" selected="">Pilih</option>';
                for(i=0; i<data.length; i++){
                    html += '<option value='+data[i].bank_name+' data-code='+data[i].bank_code+' data-id='+data[i].bank_id+'>'+data[i].bank_name+'</option>';
                }
                html += '<option value="None" data-code="None" data-nama="None">None</option>';
                $('#bankName').html(html);

            }
        });
        // End Master bank

		/* START BIODATA TABLE */	
		var salaryTable = $('#salaryTable').DataTable({
	            "paging":   true,
	            "ordering": false,
	            "info":     false,
	            "filter":   true
	        });
		/* END BIODATA TABLE */	

        /* START CURRENCY DISPLAY */
        $('#basicSalary').on('blur', function(){
            var myVal = $('#basicSalary').val();
            var rs = myVal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            $('#basicSalary').val(rs);
        });

        $('#basicSalary').on('focus', function(){
            var myVal = $('#basicSalary').val();
            var rs = myVal ? parseFloat(myVal.replace(/,/g, '')) : '';
            $('#basicSalary').val(rs);
            $('#basicSalary').select();
        });

        $('#positionAllowance').on('blur', function(){
            var myVal = $('#positionAllowance').val();
            var rs = myVal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            $('#positionAllowance').val(rs);
        });

        $('#positionAllowance').on('focus', function(){
            var myVal = $('#positionAllowance').val();
            var rs = myVal ? parseFloat(myVal.replace(/,/g, '')) : '';
            $('#positionAllowance').val(rs);
            $('#positionAllowance').select();
        });

        $('#housingAllowance').on('blur', function(){
            var myVal = $('#housingAllowance').val();
            var rs = myVal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            $('#housingAllowance').val(rs);
        });

        $('#housingAllowance').on('focus', function(){
            var myVal = $('#housingAllowance').val();
            var rs = myVal ? parseFloat(myVal.replace(/,/g, '')) : '';
            $('#housingAllowance').val(rs);
            $('#housingAllowance').select();
        });
        /* END CURRENCY DISPLAY */    

        /* START LOAD DATA FROM DATABASE TO TABLE */
       	$.ajax({
    		method : "POST",
    		 url : "<?php echo base_url() ?>"+"masters/Mst_salary/loadSalaryData", 
    		data : {
    			id :""
    		},
    		success : function(data){
				salaryTable.clear().draw();
				var dataSrc = JSON.parse(data);	
				salaryTable.rows.add(dataSrc).draw(false);
				// alert("Succeed");	
    		},
    		error : function(){
    			alert("Failed Load Data");
    		}
    	});
        /* END LOAD DATA FROM DATABASE TO TABLE */
		
        /* START LOAD BIODATA  */
        $("#bioId").on("click", function(){
        	$("#idEksternalNo").val("");
        	$.ajax({
        		method : "POST",
        		url : "<?php echo base_url() ?>"+"masters/Mst_salary", 
        		data : {
        			id :""
        		},
        		success : function(data){
    				biodataTable.clear().draw();
					var dataSrc = JSON.parse(data);	
					biodataTable.rows.add(dataSrc).draw(false);	
        		},
        		error : function(){
        			alert("Failed");
        		}
        	});
        });
        /* END LOAD BIODATA  */

        /* START SELECT BROWSE DATA */
        var internalId = "";
        var externalId = "";
        var name = "";
        var rowData = null;
        $('#biodataTable tbody').on( 'click', 'tr', function () {
            var rowData = biodataTable.row( this ).data();
            if ($(this).hasClass('selected')){
                $(this).removeClass('selected');
            }else{
                biodataTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');                    
            }
            internalId = rowData[0];
            externalId = rowData[1];
            name = rowData[2];
            $("#bioId").val(internalId);
            $("#idEksternalNo").val(externalId);
            $("#empName").val(name);
            $('#salaryModal').modal('toggle');
        }); 
        /* END SELECT BROWSE DATA */

        // $("#chooseBiodata").on("click", function(){
        // 	$("#bioId").val(internalId);
        // 	$("#idEksternalNo").val(externalId);
        // 	$("#empName").val(name);
        // });

		// $(".errMsg").css({"border": "2px solid #ced4da"});
		/* START SAVE DATA */
        $("#saveSalary").on("click", function(){
            // debugger
        	$(".errMsg").css({"border": "2px solid #ced4da"});     
        	var bioId = $("#bioId").val();
        	var empName = $("#empName").val();
        	var idEksternalNo = $("#idEksternalNo").val();
        	var ptName = $("#ptName").val();
        	var payrollGroup = $("#payrollGroup").val();
        	var areaPenempatan = $("#areaPenempatan").val();
        	var salaryLevel = $("#salaryLevel").val();
        	var basicSalary = $("#basicSalary").val();
        	var positionAllowance = $("#positionAllowance").val();
        	var housingAllowance = $("#housingAllowance").val();
        	var isRemoteLocation = $("#isRemoteLocation").val();
        	var isShiftBonus = $("#isShiftBonus").val();
        	var isAllowanceEconomy = $("#isAllowanceEconomy").val();
        	var isOTBonus = $("#isOTBonus").val();
        	var isIncentiveBonus = $("#isIncentiveBonus").val();
            var isIncentiveDevBonus = $("#isIncentiveDevBonus").val();
            var isCcPayment = $("#isCcPayment").val();
        	var isProductionBonus = $("#isProductionBonus").val();
        	var isTravelBonus = $("#isTravelBonus").val();
        	var isOverTime = $("#isOverTime").val();        	
        	var bankName = $("#bankName").val();
            var bankCode = $('#bankName option:selected').attr('data-code');
            var bankId = $('#bankName option:selected').attr('data-id');
        	var accountName = $("#accountName").val();
        	var accountNo = $("#accountNo").val();

            basicSalary = basicSalary ? parseFloat(basicSalary.replace(/,/g, '')) : '';
            positionAllowance = positionAllowance ? parseFloat(positionAllowance.replace(/,/g, '')) : '';
            housingAllowance = housingAllowance ? parseFloat(housingAllowance.replace(/,/g, '')) : '';            
        	
			var isValid = true;
			if($("#bioId").val() == "")
			{
                isValid = false;
				$("#bioId").css({"border": "2px solid red"});
				$("#bioId").focus();	
			}

			else if($("#empName").val() == "")
			{
				isValid = false;
				$("#empName").css({"border": "2px solid red"});	
				$("#empName").focus();	
			}
			else if($("#idEksternalNo").val() == "")
			{
				isValid = false;
				$("#idEksternalNo").css({"border": "2px solid red"}); 
                $("#idEksternalNo").focus();  	
			}
			else if($('#ptName option:selected').text() == "Pilih")
			{
				isValid = false;
				$("#ptName").css({"border": "2px solid red"}); 
                $('#ptName').focus();
			}
			else if($('#payrollGroup option:selected').text() == "Pilih")
			{
				isValid = false;
				$("#payrollGroup").css({"border": "2px solid red"}); 
                $('#payrollGroup').focus();
			}
            else if($('#areaPenempatan option:selected').text() == "Pilih")
            {
                isValid = false;
                $("#areaPenempatan").css({"border": "2px solid red"}); 
                $('#areaPenempatan').focus();
            }
			else if($("#basicSalary").val() == "0")
			{
				isValid = false;
				$("#basicSalary").css({"border": "2px solid red"}); 	
				$("#basicSalary").focus();	
			}
			else if($("#positionAllowance").val() == "")
			{
				isValid = false;
				$("#positionAllowance").css({"border": "2px solid red"}); 
				$("#positionAllowance").focus();	
			}
			else if($("#housingAllowance").val() == "")
			{
				isValid = false;
				$("#housingAllowance").css({"border": "2px solid red"}); 
				$("#housingAllowance").focus();	
			}
			else if($('#isRemoteLocation option:selected').text() == "Pilih")
			{
				isValid = false;
				$("#isRemoteLocation").css({"border": "2px solid red"});	
				$("#isRemoteLocation").focus();	
			}
			else if($('#isShiftBonus option:selected').text() == "Pilih")
			{
				isValid = false;
				$("#isShiftBonus").css({"border": "2px solid red"});	
				$("#isShiftBonus").focus();	
			}
			else if($('#isAllowanceEconomy option:selected').text() == "Pilih")
			{
				isValid = false;
				$("#isAllowanceEconomy").css({"border": "2px solid red"});
				$("#isAllowanceEconomy").focus();	
			}
			else if($('#isOTBonus option:selected').text() == "Pilih")
			{
				isValid = false;
				$("#isOTBonus").css({"border": "2px solid red"});	
				$("#isOTBonus").focus();	
			}
			else if($('#isIncentiveBonus option:selected').text() == "Pilih")
			{
				isValid = false;
				$("#isIncentiveBonus").css({"border": "2px solid red"});	
				$("#isIncentiveBonus").focus();	
			}
			else if($('#isIncentiveDevBonus option:selected').text() == "Pilih")
			{
				isValid = false;
				$("#isIncentiveDevBonus").css({"border": "2px solid red"});
				$("#isIncentiveDevBonus").focus();	
			}
            else if($('#isProductionBonus option:selected').text() == "Pilih")
            {
                isValid = false;
                $("#isProductionBonus").css({"border": "2px solid red"});  
                $("#isProductionBonus").focus();  
            }
			else if($('#isTravelBonus option:selected').text() == "Pilih")
            {
                isValid = false;
                $("#isTravelBonus").css({"border": "2px solid red"}); 
                $("#isTravelBonus").focus();    
            }
            else if($('#isCcPayment option:selected').text() == "Pilih")
			{
				isValid = false;
				$("#isCcPayment").css({"border": "2px solid red"});
				$("#isCcPayment").focus();	
			}
			else if($('#isOverTime option:selected').text() == "Pilih")
			{
				isValid = false;
				$("#isOverTime").css({"border": "2px solid red"});	
				$("#isOverTime").focus();	
			}
			else if($('#bankName option:selected').text() == "Pilih")
			{
				isValid = false;
				$("#bankName").css({"border": "2px solid red"});	
				$("#bankName").focus();	
			}			
			else if($("#accountName").val() == "")
			{
				isValid = false;
				$("#accountName").css({"border": "2px solid red"});	
				$("#accountName").focus();	
			}
			else if($("#accountNo").val() == "")
			{
				isValid = false;
				$("#accountNo").css({"border": "2px solid red"});	
				$("#accountNo").focus();	
			}


			if(isValid == false)
			{
				return false;
			}

			/* START LOAD DATA FROM DATABASE TO TABLE */
            var myUrl = "<?php echo base_url() ?>"+"masters/Mst_salary/ins";
	       	$.ajax({
        		method : "POST",
        		url : myUrl, 
        		data : {
        			bioId : bioId,
		        	empName : empName,
		        	idEksternalNo : idEksternalNo,
		        	ptName : ptName,
		        	payrollGroup : payrollGroup,
		        	areaPenempatan : areaPenempatan,
		        	salaryLevel : salaryLevel,
		        	basicSalary : basicSalary,
		        	positionAllowance : positionAllowance,
		        	housingAllowance : housingAllowance,
		        	isRemoteLocation : isRemoteLocation,
		        	isShiftBonus : isShiftBonus,
		        	isAllowanceEconomy : isAllowanceEconomy,
		        	isOTBonus : isOTBonus,
                    isIncentiveBonus : isIncentiveBonus,
		        	isCcPayment : isCcPayment,
                    isIncentiveDevBonus : isIncentiveDevBonus,
		        	isProductionBonus : isProductionBonus,
		        	isTravelBonus : isTravelBonus,
		        	isOverTime : isOverTime,
		        	bankName : bankName,
                    bankCode : bankCode,
                    bankId : bankId,
		        	accountName : accountName,
		        	accountNo : accountNo
                    
        		},
        		success : function(data){
					/* START ADD DATA TO TABLE */
					salaryId = data;
					salaryTable.row.add([
		                salaryId,
		                idEksternalNo,
		                empName,
		                payrollGroup,
		                ptName,
		                salaryLevel,
		                basicSalary,
		                bankName,
		                accountName,
		                accountNo
		            ]).draw(false);
					/* END ADD DATA TO TABLE */
                    clearitem();
    				alert("Data has been saved");
        			// alert(data);
        		},
        		error : function(data){
        			isValid = false;
        			alert("Save failed, please check data duplication");
        		}
        	});
	        /* END LOAD DATA FROM DATABASE TO TABLE */

			if(isValid == false)
			{
				return false;
			}


			/* START ADD DATA TO DATABASE */
			/* END ADD DATA TO DATABASE */
        });
        /* END SAVE DATA */

        function clearitem(){
            $(".errMsg").val('');
        }

        /* START VIEW DATA */
        $("#viewData").on("click", function(){
        	/* START LOAD DATA FROM DATABASE TO TABLE */
	       	$.ajax({
	    		method : "POST",
	    		 url : "<?php echo base_url() ?>"+"masters/Mst_salary/loadSalaryData", 
	    		data : {
	    			id :""
	    		},
	    		success : function(data){
					salaryTable.clear().draw();
					var dataSrc = JSON.parse(data);	
					salaryTable.rows.add(dataSrc).draw(false);
					// alert("Succeed");	
	    		},
	    		error : function(){
	    			alert("Failed Load Data");
	    		}
	    	});
	        /* END LOAD DATA FROM DATABASE TO TABLE */
        });
        /* END VIEW DATA */

        /* START SELECT SALARY DATA */
        var salaryId = "";
        var rowSalary = null;
        $('#salaryTable tbody').on( 'click', 'tr', function () {
            var rowSalary = salaryTable.row( this ).data();
            if ( $(this).hasClass('selected') ) 
            {
                $(this).removeClass('selected');
            }
            else 
            {
                salaryTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');                    
            }
            salaryId = rowSalary[0];
        }); 
        /* END SELECT SALARY DATA */

        /* START DELETE DATA SALARY  */
        var is_valid = false;	
        $("#deleteData").on("click", function(){
        	if(salaryId == "")
        	{
                $.notify({
                    title: "<h5>Information : </h5>",
                    icon: "glyphicon glyphicon-save",
                    message: 'Please choose data first '
                },{
                    type: 'success'
                });
        		// alert("Please choose data first");
        		return false;
        	}

        	confirmDialog('Data with Id : '+salaryId+' will delete?', (ans) => {
                if (ans) {
                var idDelete = salaryId; 
                salaryId = "";
                salaryTable.row('.selected').remove().draw( false );

                /* START AJAX DELETE */
                $.ajax({
                    method : "POST",
                    url : "<?php echo base_url() ?>"+"masters/Mst_salary/del", 
                    data : {
                        idDelete : idDelete
                    },
                    success : function(data){
                        $.notify({
                            title: "<h5>Information : </h5>",
                            icon: "glyphicon glyphicon-save",
                            message: data
                        },{
                            type: 'success'
                        });
                    },
                    error : function(){
                        alert("Delete Failed");
                    }

                });
                }else{
                    return false;
                }
            });

        });

        $("#salaryLevel").on("change", function(){
        	var salaryLevel = $("#salaryLevel").val();
        	var clientName = $("#ptName").val();
        	var myUrl = "<?php echo base_url() ?>"+"masters/Mst_salary/getSalaryByClientLevel/"+clientName+"/"+salaryLevel;
        	// alert(myUrl);
        	$.ajax({
            		method : "POST",
            		url : myUrl, 
            		data : {
            			salaryLevel : salaryLevel,
        				clientName : clientName
            		},
            		success : function(data){
            			$("#basicSalary").val(data);
            			// alert(data);
            		},
            		error : function(){
            			alert("Load data failed");
            		}

                });
            
        });

        $("#ptName").on("change", function(){
            var salaryLevel = $("#salaryLevel").val();
            var clientName = $("#ptName").val();
            var myUrl = "<?php echo base_url(); ?>"+"masters/Mst_salary/getSalaryByClientLevel/"+clientName+"/"+salaryLevel;
            /* START SET SALARY CONFIGURATION */

            $("#isAllowanceEconomy").val("0");
            $("#isIncentiveBonus").val("0");
            $("#isTravelBonus").val("0");
            $("#isProductionBonus").val("0");
            $("#isCompanyBonus").val("0");
            $("#isOTBonus").val("0");
            $("#isIncentiveDevBonus").val("0");
            $("#isCompanyBonus").val("0");
            $("#isShiftBonus").val("0");
            $("#isAllowanceEconomy").val("0");
            $("#isOTBonus").val("0");
            $("#isIncentiveBonus").val("0");
            $("#isIncentiveDevBonus").val("0");
            $("#isTravelBonus").val("0");
            $("#isProductionBonus").val("0");
            $("#isRemoteLocation").val("0");
            $("#isCcPayment").val("0");
            if(clientName == "Redpath_Timika")
            {
                $("#isCompanyBonus").val("1");
                $("#isShiftBonus").val("1");
                $("#isOTBonus").val("1");
                $("#isIncentiveDevBonus").val("1");
                $("#isOverTime").val("1");
                $("#isRemoteLocation").val("1");
            }  
            else if(clientName == "Pontil_Timika")
            {
                $("#isShiftBonus").val("1");
                $("#isAllowanceEconomy").val("1");
                $("#isIncentiveBonus").val("1");
                $("#isTravelBonus").val("1");
                $("#isOverTime").val("1");
                $("#isProductionBonus").val("1");
                $("#isCcPayment").val("1");
            }
            else if(clientName == "Agincourt_Martabe")
            {
                $("#isOverTime").val("1");
            }
        	/* END SET SALARY CONFIGURATION */
        	$.ajax({
            		method : "POST",
            		url : myUrl, 
            		data : {
            			salaryLevel : salaryLevel,
        				clientName : clientName
            		},
            		success : function(data){
            			$("#basicSalary").val(data);
            			// alert(data);
            		},
            		error : function(){
            			alert("Load data failed");
            		}

            	});
        	
        });

        function confirmDialog(message, handler){
            $(`<div class="modal fade" id="myModal" role="dialog"> 
                <div class="modal-dialog"> 
                   <!-- Modal content--> 
                    <div class="modal-content"> 
                       <div class="modal-body" style="padding:10px;"> 
                         <h4 class="text-center">${message}</h4> 
                         <div class="text-center"> 
                           <a class="btn btn-danger btn-yes">Yes</a> 
                           <a class="btn btn-default btn-no">No</a> 
                         </div> 
                       </div> 
                   </div> 
                </div> 
            </div>`).appendTo('body');
         
            //Trigger the modal
            $("#myModal").modal({
                backdrop: 'static',
                keyboard: false
            });
          
            //Pass true to a callback function
            $(".btn-yes").click(function () {
                handler(true);
                $("#myModal").modal("hide");
            });
            
            //Pass false to callback function
            $(".btn-no").click(function () {
                handler(false);
                $("#myModal").modal("hide");
            });

            //Remove the modal once it is closed.
            $("#myModal").on('hidden.bs.modal', function () {
                $("#myModal").remove();
            });
        }
	});
</script>
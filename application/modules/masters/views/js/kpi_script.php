<script type="text/javascript">
    
    $(document).ready(function(){

		/* START BIODATA TABLE */	
		var biodataTable = $('#biodataTable').DataTable({
	            "paging":   true,
	            "ordering": false,
	            "info":     false,
	            "filter":   true
	        });
		/* END BIODATA TABLE */

		/* START BIODATA TABLE */	
		var salaryTable = $('#salaryTable').DataTable({
	            "paging":   true,
	            "ordering": false,
	            "info":     false,
	            "filter":   true
	        });
		/* END BIODATA TABLE */	

        /* START CURRENCY DISPLAY */
        $('#basicSalary').on('blur', function(){
            var myVal = $('#basicSalary').val();
            var rs = myVal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            $('#basicSalary').val(rs);
        });

        $('#basicSalary').on('focus', function(){
            var myVal = $('#basicSalary').val();
            var rs = myVal ? parseFloat(myVal.replace(/,/g, '')) : '';
            $('#basicSalary').val(rs);
            $('#basicSalary').select();
        });

        $('#positionAllowance').on('blur', function(){
            var myVal = $('#positionAllowance').val();
            var rs = myVal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            $('#positionAllowance').val(rs);
        });

        $('#positionAllowance').on('focus', function(){
            var myVal = $('#positionAllowance').val();
            var rs = myVal ? parseFloat(myVal.replace(/,/g, '')) : '';
            $('#positionAllowance').val(rs);
            $('#positionAllowance').select();
        });

        $('#housingAllowance').on('blur', function(){
            var myVal = $('#housingAllowance').val();
            var rs = myVal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            $('#housingAllowance').val(rs);
        });

        $('#housingAllowance').on('focus', function(){
            var myVal = $('#housingAllowance').val();
            var rs = myVal ? parseFloat(myVal.replace(/,/g, '')) : '';
            $('#housingAllowance').val(rs);
            $('#housingAllowance').select();
        });
        /* END CURRENCY DISPLAY */    

        /* START LOAD DATA FROM DATABASE TO TABLE */
       	$.ajax({
    		method : "POST",
    		 url : "<?php echo base_url() ?>"+"masters/Mst_kpi/loadKpiData", 
    		data : {
    			id :""
    		},
    		success : function(data){
				salaryTable.clear().draw();
				var dataSrc = JSON.parse(data);	
				salaryTable.rows.add(dataSrc).draw(false);
				// alert("Succeed");	
    		},
    		error : function(){
    			alert("Failed Load Data");
    		}
    	});
        /* END LOAD DATA FROM DATABASE TO TABLE */
		
        /* START LOAD BIODATA  */
        $("#bioId").on("click", function(){
        	$("#idEksternalNo").val("");
        	$.ajax({
        		method : "POST",
        		url : "<?php echo base_url() ?>"+"masters/Mst_kpi", 
        		data : {
        			id :""
        		},
        		success : function(data){
    				biodataTable.clear().draw();
					var dataSrc = JSON.parse(data);	
					biodataTable.rows.add(dataSrc).draw(false);	
        		},
        		error : function(){
        			alert("Failed");
        		}
        	});
        });
        /* END LOAD BIODATA  */

        /* START SELECT BROWSE DATA */
        var internalId = "";
        var externalId = "";
        var name = "";
        var rowData = null;
        $('#biodataTable tbody').on( 'click', 'tr', function () {
            var rowData = biodataTable.row( this ).data();
            if ($(this).hasClass('selected')){
                $(this).removeClass('selected');
            }else{
                biodataTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');                    
            }
            internalId  = rowData[0];
            client      = rowData[1];
            dept        = rowData[2];
            position    = rowData[3];
            name        = rowData[4];

            $("#bioId").val(internalId);
            $("#dept").val(dept);
            $("#position").val(position);
            $("#client").val(client);
            $("#empName").val(name);
            $('#salaryModal').modal('toggle');
        }); 
        /* END SELECT BROWSE DATA */

        // $("#chooseBiodata").on("click", function(){
        // 	$("#bioId").val(internalId);
        // 	$("#idEksternalNo").val(externalId);
        // 	$("#empName").val(name);
        // });

		// $(".errMsg").css({"border": "2px solid #ced4da"});
		/* START SAVE DATA */
        $("#saveSalary").on("click", function(){
        	$(".errMsg").css({"border": "2px solid #ced4da"});     
        	
            var biorecId          = $("#bioId").val();
            var name              = $("#empName").val();           
            var clientName        = $("#client").val();
            var dept              = $("#dept").val();
            var monthPeriod       = $('#monthPeriod').val();
            var yearPeriod        = $('#yearPeriod').val();
            var attendance        = parseInt($("#attendance").val());
            var performance       = parseInt($("#performance").val());
            var dicipline         = parseInt($("#dicipline").val());
            var communication     = parseInt($("#communication").val());
            var remarks           = $("#remarks").val();
            var position          = $("#position").val();

            var nilai = (attendance+performance+dicipline+communication)/4;

            var hasil = '';
            debugger
            if(nilai>='80'){
                hasil = 'A';
            }else if(nilai>='65' & nilai<'80'){
                hasil = 'B';
            }else if(nilai>='55' & nilai<'65'){
                hasil = 'C';
            }else if(nilai>='40' & nilai<'55'){
                hasil = 'D';
            }else if(nilai<'40'){
                hasil = 'E';
            }

            hasil = hasil;
                    
        	
			var isValid = true;
			if($("#bioId").val() == "")
			{
                isValid = false;
				$("#bioId").css({"border": "2px solid red"});
				$("#bioId").focus();	
			}

			else if($("#empName").val() == "")
			{
				isValid = false;
				$("#empName").css({"border": "2px solid red"});	
				$("#empName").focus();	
			}
			else if($('#monthPeriod option:selected').text() == "Pilih")
			{
				isValid = false;
				$("#monthPeriod").css({"border": "2px solid red"}); 
                $('#monthPeriod').focus();
			}
            else if($('#yearPeriod option:selected').text() == "Pilih")
            {
                isValid = false;
                $("#yearPeriod").css({"border": "2px solid red"}); 
                $('#yearPeriod').focus();
            }
            else if($('#attendance option:selected').text() == "Pilih")
            {
                isValid = false;
                $("#attendance").css({"border": "2px solid red"}); 
                $('#attendance').focus();
            }
            else if($('#performance option:selected').text() == "Pilih")
            {
                isValid = false;
                $("#performance").css({"border": "2px solid red"}); 
                $('#performance').focus();
            }

			else if($('#dicipline option:selected').text() == "Pilih")
			{
				isValid = false;
				$("#dicipline").css({"border": "2px solid red"}); 
                $('#dicipline').focus();
			}
            else if($('#communication option:selected').text() == "Pilih")
            {
                isValid = false;
                $("#communication").css({"border": "2px solid red"}); 
                $('#communication').focus();
            }
            else if($("#remarks").val() == "")
            {
                isValid = false;
                $("#remarks").css({"border": "2px solid red"}); 
                $("#remarks").focus();    
            }


			if(isValid == false)
			{
				return false;
			}

			/* START LOAD DATA FROM DATABASE TO TABLE */
            var myUrl = "<?php echo base_url() ?>"+"masters/Mst_kpi/ins";
	       	$.ajax({
        		method : "POST",
        		url : myUrl, 
        		data : {
                    biorecId          : biorecId,
                    name              : name,       
                    clientName        : clientName,
                    dept              : dept,
                    monthPeriod       : monthPeriod,
                    yearPeriod        : yearPeriod,
                    attendance        : attendance,
                    performance       : performance,
                    dicipline         : dicipline,
                    communication     : communication,
                    remarks           : remarks,
                    nilai             : nilai,
                    hasil             : hasil,
                    position          : position
        		},
        		success : function(data){
					/* START ADD DATA TO TABLE */
					salaryId = data;
					salaryTable.row.add([
		                biorecId,
		                name,
		                clientName,
                        dept,
		                position,
		                yearPeriod,
		                monthPeriod,
		                attendance,
		                performance,
		                dicipline,
		                communication,
                        nilai,
                        hasil,
                        remarks
		            ]).draw(false);
					/* END ADD DATA TO TABLE */
                    clearitem();
    				alert("Data has been saved");
        			// alert(data);
        		},
        		error : function(data){
        			isValid = false;
        			alert("Save failed, please check data duplication");
        		}
        	});
	        /* END LOAD DATA FROM DATABASE TO TABLE */

			if(isValid == false)
			{
				return false;
			}


			/* START ADD DATA TO DATABASE */
			/* END ADD DATA TO DATABASE */
        });
        /* END SAVE DATA */

        function clearitem(){
            $(".errMsg").val('');
        }

        /* START VIEW DATA */
        $("#viewData").on("click", function(){
        	/* START LOAD DATA FROM DATABASE TO TABLE */
	       	$.ajax({
	    		method : "POST",
	    		 url : "<?php echo base_url() ?>"+"masters/Mst_salary/loadSalaryData", 
	    		data : {
	    			id :""
	    		},
	    		success : function(data){
					salaryTable.clear().draw();
					var dataSrc = JSON.parse(data);	
					salaryTable.rows.add(dataSrc).draw(false);
					// alert("Succeed");	
	    		},
	    		error : function(){
	    			alert("Failed Load Data");
	    		}
	    	});
	        /* END LOAD DATA FROM DATABASE TO TABLE */
        });
        /* END VIEW DATA */

        /* START SELECT SALARY DATA */
        var salaryId = "";
        var rowSalary = null;
        $('#salaryTable tbody').on( 'click', 'tr', function () {
            var rowSalary = salaryTable.row( this ).data();
            if ( $(this).hasClass('selected') ) 
            {
                $(this).removeClass('selected');
            }
            else 
            {
                salaryTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');                    
            }
            salaryId = rowSalary[0];
        }); 
        /* END SELECT SALARY DATA */

        /* START DELETE DATA SALARY  */
        var is_valid = false;	
        $("#deleteData").on("click", function(){
        	if(salaryId == "")
        	{
        		alert("Please choose data first");
        		return false;
        	}

        	is_valid = confirm('Data with Id : '+salaryId+' will delete?');
            if (is_valid) 
            {
        		var idDelete = salaryId; 
            	salaryId = "";
            	salaryTable.row('.selected').remove().draw( false );

            	/* START AJAX DELETE */
            	$.ajax({
            		method : "POST",
            		url : "<?php echo base_url() ?>"+"masters/Mst_salary/del", 
            		data : {
            			idDelete : idDelete
            		},
            		success : function(data){
            			alert(data);
            		},
            		error : function(){
            			alert("Delete Failed");
            		}

            	});
            	/* END AJAX DELETE */
            }
        });

        $("#salaryLevel").on("change", function(){
        	var salaryLevel = $("#salaryLevel").val();
        	var clientName = $("#ptName").val();
        	var myUrl = "<?php echo base_url() ?>"+"Payroll/getSalaryByClientLevel/"+clientName+"/"+salaryLevel;
        	// alert(myUrl);
        	$.ajax({
            		method : "POST",
            		url : myUrl, 
            		data : {
            			salaryLevel : salaryLevel,
        				clientName : clientName
            		},
            		success : function(data){
            			$("#basicSalary").val(data);
            			// alert(data);
            		},
            		error : function(){
            			alert("Load data failed");
            		}

                });
            
        });

        $("#ptName").on("change", function(){
            var salaryLevel = $("#salaryLevel").val();
            var clientName = $("#ptName").val();
            var myUrl = "<?php echo base_url(); ?>"+"Payroll/getSalaryByClientLevel/"+clientName+"/"+salaryLevel;
            /* START SET SALARY CONFIGURATION */

            $("#isAllowanceEconomy").val("0");
            $("#isIncentiveBonus").val("0");
            $("#isTravelBonus").val("0");
            $("#isProductionBonus").val("0");
            $("#isCompanyBonus").val("0");
            $("#isOTBonus").val("0");
            $("#isIncentiveDevBonus").val("0");
            $("#isCompanyBonus").val("0");
            $("#isShiftBonus").val("0");
            $("#isAllowanceEconomy").val("0");
            $("#isOTBonus").val("0");
            $("#isIncentiveBonus").val("0");
            $("#isIncentiveDevBonus").val("0");
            $("#isTravelBonus").val("0");
            $("#isProductionBonus").val("0");
            $("#isRemoteLocation").val("0");
            $("#isCcPayment").val("0");
            if(clientName == "Redpath_Timika")
            {
                $("#isCompanyBonus").val("1");
                $("#isShiftBonus").val("1");
                $("#isOTBonus").val("1");
                $("#isIncentiveDevBonus").val("1");
                $("#isOverTime").val("1");
                $("#isRemoteLocation").val("1");
            }  
            else if(clientName == "Pontil_Timika")
            {
                $("#isShiftBonus").val("1");
                $("#isAllowanceEconomy").val("1");
                $("#isIncentiveBonus").val("1");
                $("#isTravelBonus").val("1");
                $("#isOverTime").val("1");
                $("#isProductionBonus").val("1");
                $("#isCcPayment").val("1");
            }
            else if(clientName == "Agincourt_Martabe")
            {
                $("#isOverTime").val("1");
            }
        	/* END SET SALARY CONFIGURATION */
        	$.ajax({
            		method : "POST",
            		url : myUrl, 
            		data : {
            			salaryLevel : salaryLevel,
        				clientName : clientName
            		},
            		success : function(data){
            			$("#basicSalary").val(data);
            			// alert(data);
            		},
            		error : function(){
            			alert("Load data failed");
            		}

            	});
        	
        });

        /*var is_valid = false;
        var salaryId = "";
        $('#salaryTable tbody').on( 'click', 'tr', '.btnDelete', function () 
        {   
            var rowSalary = salaryTable.row( this ).data();
            salaryId = rowSalary[0];
            tableIndex = salaryTable.row( $(this ).parents('tr')).index();
            is_valid = confirm('Are you sure to delete data with id : '+tableIndex+'?');
            if (is_valid) 
            {
                salaryTable.row( $(this).parents('tr') ).remove().draw();
            }
        });*/        
        /* END DELETE DATA SALARY  */	
	});
</script>
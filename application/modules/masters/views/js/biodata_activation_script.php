<script type="text/javascript">
	$(document).ready(function(){
		var rowIdx = 0;
		var headerId = ''; 
		var rowEducationId = 0;
		var rowTrainingId = 0;
		var rowExperienceId = 0;
		var rowFamilyId = 0;
		var rowLanguageId = 0;
		var rowOrganizationId = 0;
		var rowReferenceId = 0;

		/* START DETAIL GLOBAL */	
		function clearDetailInputs()
        {
            $('.my_detail :input').each(function(){
                $(this).val('');
            });            
        }
        /* END DETAIL GLOBAL */	

		var biodataListTable = $('#biodataListTable').DataTable({
	            "paging":   true,
	            "ordering": false,
	            "info":     false,
	            "filter":     true,
	            "columnDefs": [
	            // {
	            //     "targets": -2,
	            //     "data": null,
	            //     "defaultContent": "<button class='btn btn-primary fa fa-check btnActive'><span class='glyphicon glyphicon-ok'></span></button>"
	            // },
	            // {
	            //     "targets": -1,
	            //     "data": null,
	            //     "defaultContent": "<button class='btn btn-primary fa fa-times btnDeActive' ><span class='glyphicon glyphicon-remove'></span></button>"
	            // }
	            ]
	        });															

	    $('#searchByName').focus();			
		$('#searchByName').keyup(function(e){
			if(e.keyCode == 13)
			{
				// debugger
				var name = $('#searchByName').val();
				var dataSrc;
				var myURL = "<?php echo base_url() ?>"+"/masters/Mst_bio_rec/GetByName/"+name;
				// alert(myURL);
				/* Do Something Here */	
				$.ajax({
					method 	: "POST",
					url		: myURL,
					data 	: {
						name : name 
					},
					error	: function(data){
						alert("Load Data Gagal");
					},
					success	: function(data){						
						var dataSrc = JSON.parse(data);	
						biodataListTable.clear().draw();
						biodataListTable.rows.add(dataSrc).draw(false);		
					}
				});					
			}
		});		
		/* End HEADER */	

        $(".btnDetail").attr('disabled',true);  
		/* Start Select HEADER */
        $('#biodataListTable tbody').on( 'click', 'tr', function () {
            var rowData = biodataListTable.row( this ).data();
            if ( $(this).hasClass('selected') ) 
            {
                $(this).removeClass('selected');
            }
            else 
            {
                biodataListTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');                    
            }
            headerId = rowData[0];   
        	$(".btnDetail").attr('disabled',false);  
            
            rowIdx = biodataListTable.row( this ).index();
            $("#rowIdx").val(rowIdx);                 
        }); 
        /* End Select PENDIDIKAN */  
        /* End Table Detail */

        $('#biodataListTable tbody').on( 'click', '.btnActive', function () {
		 	var data 			= biodataListTable.row( $(this).parents('tr') ).data();
	        var bioId 			= data[0]; 
	        var tName 			= data[1]; 
	        var tAddress 		= data[2]; 
	        var tContractNo 	= data[3]; 
	        var tPosition 		= data[4]; 
	        var tIsActive 		= data[5]; 
	        var tIsBlackList 	= data[6]; 
	        var tombol_active 	= "<button class='btn btn-primary fa fa-times btnDeActive' ><span class='glyphicon glyphicon-remove'> Non Active</span></button>";
	        if(tIsBlackList=='Black List'){
	        	var tombol_bl 		= "<button class='btn btn-primary fa fa-times btnDeBlist' ><span class='glyphicon glyphicon-remove'></span> Not BlackList</button>";
	        }else{
	        	var tombol_bl 		= "<button class='btn btn-primary fa fa-check btnBlist'><span class='glyphicon glyphicon-ok'> BlackList</span></button>";
	        }
	        
	        var myURL = "<?php echo base_url().'/masters/Mst_bio_rec/UpdateActive/' ?>";

	        $.ajax({
	        	method: "POST",
	        	url : myURL,
	        	data: {
	        		bioId : bioId,
	        		isActive : '1'
	        	},
	        	error : function(data){
	        		alert("Load Data Gagal");
	        	},
	        	success : function(data){
	        		biodataListTable.row(rowIdx).data([
                            bioId,tName,tAddress,tContractNo,tPosition,'Active',tIsBlackList,tombol_active,tombol_bl]); 
	        		$.notify({
	                title: "<h5>Informasi : </h5>",
	                message: "<strong>Data Has Been Actived</strong> </br></br> ",
	                icon: '' 
	            	});      
	        	}
	        });
        });

        $('#biodataListTable tbody').on( 'click', '.btnDeActive', function () {
		 	var data = biodataListTable.row( $(this).parents('tr') ).data();
	        var bioId = data[0]; 
	        var tName = data[1]; 
	        var tAddress = data[2]; 
	        var tContractNo = data[3]; 
	        var tPosition = data[4]; 
	        var tIsActive = data[5]; 
	        var tIsBlackList = data[6]; 
	        var tombol_active 	= "<button class='btn btn-primary fa fa-check btnActive'><span class='glyphicon glyphicon-ok'> Active</span></button>";
	        if(tIsBlackList=='Black List'){
	        	var tombol_bl 		= "<button class='btn btn-primary fa fa-times btnDeBlist' ><span class='glyphicon glyphicon-remove'> Not BlackList</span></button>";
	        }else{
	        	var tombol_bl 		= "<button class='btn btn-primary fa fa-check btnBlist'><span class='glyphicon glyphicon-ok'> BlackList</span></button>";
	        }
	        var myURL = "<?php echo base_url().'/masters/Mst_bio_rec/UpdateActive/' ?>";
	        $.ajax({
	        	method: "POST",
	        	url : myURL,
	        	data: {
	        		bioId : bioId,
	        		isActive : '0'
	        	},
	        	error : function(data){
	        		alert("Load Data Gagal");
	        	},
	        	success : function(data){
	        		biodataListTable.row(rowIdx).data([
                            bioId,tName,tAddress,tContractNo,tPosition,'Non Active',tIsBlackList,tombol_active,tombol_bl]); 
	        		$.notify({
	                title: "<h5>Informasi : </h5>",
	                message: "<strong>Data Has Been Deactived</strong> </br></br> ",
	                icon: '' 
	            	});         
	        	}
	        });
        });

        $('#biodataListTable tbody').on( 'click', '.btnBlist', function () {
		 	var data = biodataListTable.row( $(this).parents('tr') ).data();
	        var bioId = data[0]; 
	        var tName = data[1]; 
	        var tAddress = data[2]; 
	        var tContractNo = data[3]; 
	        var tPosition = data[4]; 
	        var tIsActive = data[5]; 
	        var tIsBlackList = data[6]; 

	        // if(tIsActive=='Active'){
	        // 	var tombol_active 	= "<button class='btn btn-primary fa fa-times btnDeActive'><span class='glyphicon glyphicon-remove'> Non Active</span></button>";
	        // }else{
        	var tombol_active 	= "<button class='btn btn-primary fa fa-check btnActive'><span class='glyphicon glyphicon-ok'> Active</span></button>";
	        // }
	        
	        var tombol_bl 		= "<button class='btn btn-primary fa fa-times btnDeBlist' ><span class='glyphicon glyphicon-remove'> Non BlackList</span></button>";
	        

	        var myURL = "<?php echo base_url().'/masters/Mst_bio_rec/UpdateBlacklist/' ?>";
	        $.ajax({
	        	method: "POST",
	        	url : myURL,
	        	data: {
	        		bioId 		: bioId,
	        		isBlacklist : '1',
	        		isActive 	: '0'
	        	},
	        	error : function(data){
	        		alert("Load Data Gagal");
	        	},
	        	success : function(data){
	        		biodataListTable.row(rowIdx).data([
                            bioId,tName,tAddress,tContractNo,tPosition,'Non Active','BlackList',tombol_active,tombol_bl]); 
	        		$.notify({
	                title: "<h5>Informasi : </h5>",
	                message: "<strong>Data Has Been blacklist</strong> </br></br> ",
	                icon: '' 
	            	});         
	        	}
	        });
        });

        $('#biodataListTable tbody').on( 'click', '.btnDeBlist', function () {
		 	var data = biodataListTable.row( $(this).parents('tr') ).data();
	        var bioId = data[0]; 
	        var tName = data[1]; 
	        var tAddress = data[2]; 
	        var tContractNo = data[3]; 
	        var tPosition = data[4]; 
	        var tIsActive = data[5]; 
	        var tIsBlackList = data[6]; 

	        // if(tIsActive=='Active'){
	        // 	var tombol_active 	= "<button class='btn btn-primary fa fa-times btnDeActive'><span class='glyphicon glyphicon-remove'> Non Active</span></button>";
	        // }else{
	        	var tombol_active 	= "<button class='btn btn-primary fa fa-check btnActive'><span class='glyphicon glyphicon-ok'> Active</span></button>";
	        // }
	        
	        var tombol_bl 		= "<button class='btn btn-primary fa fa-times btnBlist'><span class='glyphicon glyphicon-ok'> BlackList</span></button>";
	        

	        var myURL = "<?php echo base_url().'/masters/Mst_bio_rec/UpdateBlacklist/' ?>";
	        $.ajax({
	        	method: "POST",
	        	url : myURL,
	        	data: {
	        		bioId 		: bioId,
	        		isBlacklist : '0',
	        		isActive 	: '0'
	        	},
	        	error : function(data){
	        		alert("Load Data Gagal");
	        	},
	        	success : function(data){
	        		biodataListTable.row(rowIdx).data([
                            bioId,tName,tAddress,tContractNo,tPosition,'Non Active','Not BlackList',tombol_active,tombol_bl]); 
	        		$.notify({
	                title: "<h5>Informasi : </h5>",
	                message: "<strong>Data Not Blacklisted</strong> </br></br> ",
	                icon: '' 
	            	});         
	        	}
	        });
        });
	});
</script>
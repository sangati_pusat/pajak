<script type="text/javascript">
  $(document).ready(function () {

    // $("#etnics").select2();
    // $("#groupPosition").select2();

    function clearDetailInputs()
    {
      $('.my_detail :input').each(function(){
          $(this).val('');
      });            
    }

  // fungsi acordion
      var $tabs = $('#horizontalTab');
      $tabs.responsiveTabs({
      active: 0,
      rotate: false,
      startCollapsed: 'accordion',
      collapsible: 'accordion',
      setHash: true,
        // disabled: [3,4],
      });
  

        /* END DETAIL GLOBAL */ 
        var isDuplicateId = false;
        var confirmUpdate = false;
        $('#idNoDuplicate').hide();
        $('#idNoDigit').hide();
        $('#idNo').on('focusout', function(){        
          var idNo = $('#idNo').val();
          var myUrl = "<?php echo base_url() ?>"+"masters/Mst_bio_rec/getIdNo/"+idNo;
          var idLength = idNo.length;     

              $.ajax({
                  method : "POST",
                  url : myUrl,
                  data : {
                      idNo : idNo
                  },
                  success : function(data){
                      // debugger
                      $('#idNoDigit').hide(); 
                      if(idLength != 16){
                        $('#idNo').focus();
                        $('#idNoDigit').show(); 
                      } else {
                        isDuplicateId = false;
                        $('#idNoDuplicate').hide(); 
                        if(data > 0){                          
                          /* Confirmation Data Update */
                          confirmUpdate = confirm('Duplicate data, update?');
                          if(confirmUpdate == true) {
                            window.location.href="<?php echo base_url() ?>"+"home/detail/clr/"+idNo;                     
                          } else {
                            $('#idNo').focus();  
                          }

                          $('#idNoDuplicate').show();
                          isDuplicateId = true;
                        }
                      } 
                  }, 
                  error : function(data){
                      alert('Failed');
                  }
              });

        });

        $.ajax({
            method : "POST",
            url : "<?php echo base_url() ?>"+"masters/Mst_city/getAllCity", 
            data : {
                id : "",
                val : ""
            },
            success : function(data) {
                var srcData = JSON.parse(data);
                // alert(data);
                var i;
                for (i = 0; i < srcData.length; ++i) {
                    // alert(srcData[i]);
                    $('#cityAddress').append('<option value="'+srcData[i]+'">'+srcData[i]+'</option>');
                }
            },
            error : function(data) {
                alert("Failed");
            } 
        })

        $.ajax({
            url : "<?php echo base_url() ?>"+"masters/Mst_position/getPositionGroup",
            method : "POST",
            data : {
                id : ""
            },
            success : function(data){
                var dataSrc = JSON.parse(data); 
                var dataLength = dataSrc.length;
                $('#groupPosition').find('option:not(:first)').remove();
                for(var i = 0; i < dataLength; i++){
                    $('#groupPosition').append(new Option(dataSrc[i]['pg_name'], dataSrc[i]['pg_id']));
                }
            },
            error : function(){
                alert('Failed');
            }
        });

        var gpId = '-';
        $('#groupPosition').on('change', function(){
            gpId = $('#groupPosition').val();
            var myUrl = "<?php echo base_url() ?>"+"masters/Mst_position/getPositionByPG/"+gpId; 
            $.ajax({
                url : myUrl,
                method : "POST",
                data : {
                    id : ""
                },
                success : function(data){
                    var dataSrc = JSON.parse(data); 
                    var dataLength = dataSrc.length;
                    $('#appliedPosition').find('option:not(:first)').remove();
                    for(var i = 0; i < dataLength; i++){
                        $('#appliedPosition').append(new Option(dataSrc[i]['position_name'], dataSrc[i]['position_name']));
                    }
                },
                error : function(){
                    alert('Failed');
                }
            });  
        });

        $("#fullName").on('change', function(){
            var fullName   = $("#fullName").val();
            var birthPlace = $("#birthPlace").val();
            var birthDate  = $("#birthDate").val();
            var myUrl = "<?php echo base_url() ?>"+"masters/Mst_bio_rec/getBioByNameBirth/";  
            $.ajax({
                url : myUrl,
                method : "POST",
                data : {
                    fullName   : fullName,
                    birthPlace : birthPlace,
                    birthDate  : birthDate
                },
                success : function(data){
                    if(data != ''){
                        alert('Data dengan nama, tempat, tanggal lahir yang sama sudah terdaftar');
                    }
                },
                error : function(data){
                    alert('Failed');
                }
            });
        });

        $("#birthPlace").on('change', function(){
            var fullName   = $("#fullName").val();
            var birthPlace = $("#birthPlace").val();
            var birthDate  = $("#birthDate").val();
            var myUrl = "<?php echo base_url() ?>"+"masters/Mst_bio_rec/getBioByNameBirth/";  
            $.ajax({
                url : myUrl,
                method : "POST",
                data : {
                    fullName : fullName,
                    birthPlace : birthPlace,
                    birthDate : birthDate
                },
                success : function(data){
                    if(data != ''){
                        alert('Data dengan nama, tempat, tanggal lahir yang sama sudah terdaftar');
                    }
                },
                error : function(data){
                    alert('Failed');
                }
            });
        });

        $("#birthDate").on('change', function(){
            var fullName   = $("#fullName").val();
            var birthPlace = $("#birthPlace").val();
            var birthDate  = $("#birthDate").val();
            var myUrl = "<?php echo base_url() ?>"+"masters/Mst_bio_rec/getBioByNameBirth/";  
            $.ajax({
                url : myUrl,
                method : "POST",
                data : {
                    fullName : fullName,
                    birthPlace : birthPlace,
                    birthDate : birthDate
                },
                success : function(data){
                    if(data != ''){
                        alert('Data dengan nama, tempat, tanggal lahir yang sama sudah terdaftar');
                    }
                },
                error : function(data){
                    alert('Failed');
                }
            });  
        });
        /* Start Save EDUCATION Table */
        var educationTable = $('#educationTable').DataTable({
          "paging"        : false,
          "responsive"    : true,
          "ordering"      : false,
          "info"          : false,
          "filter"        : false,
          "columnDefs": [{
              "targets": -1,
              "data": null,
              "defaultContent": "<button class='btn btn-danger btn-sm btnDelete'><i class='fa fa-times' aria-hidden='true'></i></button>"
          }]
        });

        /* Start Detail Validation */
        $("#addEducation").attr("disabled", true);
        $("#addTraining").attr("disabled", true);
        $("#addExperience").attr("disabled", true);
        $("#addFamily").attr("disabled", true);
        $("#addLanguage").attr("disabled", true);
        $("#addOrganization").attr("disabled", true);
        $("#addReference").attr("disabled", true);
        $("#addQualification").attr("disabled", true);
        $("#fullName").on("change", function(){
          var fullName = $("#fullName").val();    
          if(fullName.trim() != ""){
            $("#addEducation").attr("disabled", false);
            $("#addTraining").attr("disabled", false);
            $("#addExperience").attr("disabled", false);
            $("#addFamily").attr("disabled", false);
            $("#addLanguage").attr("disabled", false);
            $("#addOrganization").attr("disabled", false);
            $("#addReference").attr("disabled", false);
            $("#addQualification").attr("disabled", false);
          }   
          else{
            $("#addEducation").attr("disabled", true);
            $("#addTraining").attr("disabled", true);
            $("#addExperience").attr("disabled", true);
            $("#addFamily").attr("disabled", true);
            $("#addLanguage").attr("disabled", true);
            $("#addOrganization").attr("disabled", true);
            $("#addReference").attr("disabled", true);
            $("#addQualification").attr("disabled", true);
          }
        });
        /* End Detail Validation */
        $('#schoolNameErr').hide();
        $('#educationMajorErr').hide();
        $('#educationYearErr').hide();
        $('#educationCityErr').hide();
        $('#saveEducation').click(function(){
          /* Start EDUCATION Form Validation */
          /* End EDUCATION Form Validation */
          var isCertified = "Y";
          if ($('#isCertified').is(":checked"))
          { 
            isCertified = "Y";
          }              
          else
          {
            isCertified = "T";
          }

          /* Start EDUCATION Form Validation */
          var isDataValid = true;
          $('#schoolNameErr').hide();
          $('#educationMajorErr').hide();
          $('#educationYearErr').hide();
          $('#educationCityErr').hide();
          
          if($('#schoolName').val() == "") 
          {
            $('#schoolName').focus();
            $('#schoolNameErr').show();
            isDataValid = false;
          }
          else if($('#educationMajor').val() == "") 
          {
            $('#educationMajor').focus();
            $('#educationMajorErr').show();
            isDataValid = false;
          }
          else if($('#educationYear option:selected').text() == "Pilih") 
          {
            $('#educationYear').focus();
            $('#educationYearErr').show();
            isDataValid = false;
          }
          else if($('#educationCity').val() == "") 
          {
            $('#educationCity').focus();
            $('#educationCityErr').show();
            isDataValid = false;
          }

          if(isDataValid == false)
          {
            return false;
          }
          /* End EDUCATION Form Validation */

          educationTable.row.add([
            $('#schoolName').val(),
            $('#educationMajor').val(),
            $('#educationYear').val(),
            $('#educationCity').val(),
            isCertified
          ]).draw(false);
          clearDetailInputs();  
          $('#schoolName').focus();                              
        });
        /* End Save EDUCATION Table */

        /* Start Select EDUCATION */
        $('#educationTable tbody').on( 'click', 'tr', function () {
          var rowData = educationTable.row( this ).data();
          if ( $(this).hasClass('selected') ) 
          {
            $(this).removeClass('selected');
          }
          else 
          {
            educationTable.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');                    
          }
        }); 
        /* End Select EDUCATION */

        /* Start Delete EDUCATION  */  
        var is_valid = false;
        $('#educationTable tbody').on( 'click', '.btnDelete', function (){   
          is_valid = confirm('Are you sure to delete?');
          if (is_valid) 
          {
            educationTable.row( $(this).parents('tr') ).remove().draw();
          }
        });
        /* End Delete EDUCATION  */  
        /* END EDUCATION TABLE */  

        /* Start Save TRAINING Table */
        var trainingTable = $('#trainingTable').DataTable({
          "paging"    :false,
          "ordering"  :false,
          "info"      :false,
          "filter"    :false,
          "columnDefs":[{
            "targets"       :-1,
            "data"          :null,
            "defaultContent": "<button class='btn btn-danger btn-sm btnDelete'><i class='fa fa-times' aria-hidden='true'></i></button>"
          }]
        });

        $('#instituteNameErr').hide();
        $('#classesFieldErr').hide();
        $('#trainingYearErr').hide();
        $('#trainingCityErr').hide();
        $('#saveTraining').click(function(){
          var isTrainingCertified = "Y";
          if ($('#isTrainingCertified').is(":checked"))
          { 
            isTrainingCertified = "Y";
          }              
          else
          {
            isTrainingCertified = "T";
          }

          /* Start TRAINING Form Validation */
          $('#instituteNameErr').hide();
          $('#classesFieldErr').hide();
          $('#trainingYearErr').hide();
          $('#trainingCityErr').hide();
          var isDataValid = true;
          if($('#instituteName').val() == "") 
          {
            $('#instituteName').focus();  
            $('#instituteNameErr').show();
            isDataValid = false;  
          }
          else if($('#classesField').val() == "") 
          {
            $('#classesField').focus(); 
            $('#classesFieldErr').show();
            isDataValid = false;    
          }
          else if($('#trainingYear option:selected').text() == "Pilih") 
          {
            $('#trainingYear').focus(); 
            $('#trainingYearErr').show();
            isDataValid = false;    
          }
          else if($('#trainingCity').val() == "")
          {
            $('#trainingCity').focus(); 
            $('#trainingCityErr').show();
            isDataValid = false;      
          }

          if(isDataValid == false)
          {
            return false;
          }

          /* End TRAINING Form Validation */

          trainingTable.row.add([
            $('#instituteName').val(),
            $('#classesField').val(),
            $('#trainingYear').val(),
            $('#trainingCity').val(),
            isTrainingCertified
          ]).draw(false);
          clearDetailInputs();  
          $('#instituteName').focus();                              
        });
        /* End Save TRAINING Table */

        /* Start Select TRAINING */
        $('#trainingTable tbody').on( 'click', 'tr', function () {
          var rowData = trainingTable.row( this ).data();
          if ( $(this).hasClass('selected') ) 
          {
            $(this).removeClass('selected');
          }
          else 
          {
            trainingTable.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');                    
          }
        }); 
        /* End Select TRAINING */

        /* Start Delete TRAINING  */  
        var is_valid = false;
        $('#trainingTable tbody').on( 'click', '.btnDelete', function (){   
          is_valid = confirm('Are you sure to delete?');
          if (is_valid) 
          {
              trainingTable.row( $(this).parents('tr') ).remove().draw();
          }
        });
        /* End Delete TRAINING  */ 

        /* END TRAINING TABLE */

        /* end Save TRAINING Table */

        /* START EXPERIENCE TABLE */
        var experienceTable = $('#experienceTable').DataTable({
          "paging"    : false,
          "ordering"  : false,
          "info"      : false,
          "filter"    : false,
          "columnDefs": [{
            "targets"       : -1,
            "data"          : null,
            "defaultContent": "<button class='btn btn-danger btn-sm btnDelete'><i class='fa fa-times' aria-hidden='true'></i></button>"
          }]
        }); 

        /* Start Save EXPERIENCE Table */
        $('#experienceCompanyNameErr').hide();
        $('#experiencePositionErr').hide();
        $('#startWorkDateErr').hide();
        $('#experienceYearErr').hide();
        $('#experienceBasicSalaryErr').hide();
        $('#jobDescErr').hide();
        $('#resignReasonErr').hide();

        $('#saveExperience').click(function(){
          /* Start EXPERIENCE Form Validation */
          $('#experienceCompanyNameErr').hide();
          $('#experiencePositionErr').hide();
            $('#startWorkDateErr').hide();
          $('#experienceYearErr').hide();
          $('#experienceBasicSalaryErr').hide();
          $('#jobDescErr').hide();
          $('#resignReasonErr').hide();
          /* End EXPERIENCE Form Validation */

          var isDataValid = true;        
          if($('#experienceCompanyName').val() == "")
          {
            $('#experienceCompanyName').focus();
            $('#experienceCompanyNameErr').show();
            isDataValid = false;
          }
          else if($('#experiencePosition').val() == "") 
          {
            $('#experiencePosition').focus();
            $('#experiencePositionErr').show();
            isDataValid = false;
          }
          else if($('#startWorkDate').val() == "") 
          {
            $('#startWorkDate').focus();
            $('#startWorkDateErr').show();
            isDataValid = false;
          }
          else if($('#experienceYear option:selected').text() == "Pilih") 
          {
            $('#experienceYear').focus();
            $('#experienceYearErr').show();
            isDataValid = false;
          }
          else if($('#experienceBasicSalary').val() == "") 
          {
            $('#experienceBasicSalary').focus();
            $('#experienceBasicSalaryErr').show();
            isDataValid = false;
          }
          else if($('#jobDesc').val() == "") 
          {
            $('#jobDesc').focus();
            $('#jobDescErr').show();
            isDataValid = false;
          }

          if(isDataValid == false)
          {
            return false;
          }

          /* Start EXPERIENCE Form Validation */
          var startWork = $('#startWorkDate').val();
          var endWork   = $('#endWorkDate').val();
          if ($('#endWorkDate').val() == ""){
            endWork = ""; 
          }
          var isCurrentWork = "Y";
          if ($('#isCurrentWork').is(":checked")){   
            isCurrentWork = "Y";
          }              
          else{
            isCurrentWork = "T";
          } 

          experienceTable.row.add([
            $('#experienceCompanyName').val(),
            $('#experiencePosition').val(),
            startWork,
            isCurrentWork,
            endWork,
            $('#experienceBasicSalary').val(),
            $('#experienceAllowance').val(),
            $('#jobDesc').val(),
            $('#resignReason').val(),
            $('#addInfo').val()
          ]).draw(false);
          clearDetailInputs();  
          $('#experienceCompanyName').focus();                              
        });
        /* End Save EXPERIENCE Table */

        /* Start Clear end_of_work */
        $("#endWorkDate").attr('disabled',true);      
        $("#resignReason").attr('disabled',true);    
        $('#isCurrentWork').on('change', function(){
          if ($('#isCurrentWork').is(":checked")){
            $('#endWorkDate').val('');
            $('#resignReason').get(0).selectedIndex = 0;
            /* Disabled Select End Work */
            $("#endWorkDate").attr('disabled',true);    
            $("#resignReason").attr('disabled',true);    
          }
          else{
            /* Enable Select End Work */
            $("#endWorkDate").attr('disabled',false);    
            $("#endWorkMonth").attr('disabled',false);    
            $("#endWorkYear").attr('disabled',false);       
            $("#resignReason").attr('disabled',false);       
          }
        });
        /* End Clear end_of_work */

        /* Start Select EXPERIENCE */
        $('#experienceTable tbody').on( 'click', 'tr', function () {
          var rowData = experienceTable.row( this ).data();
          if ( $(this).hasClass('selected') ){
            $(this).removeClass('selected');
          }
          else{
            experienceTable.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');                    
          }
        }); 
        /* End Select EXPERIENCE */

        /* Start Delete EXPERIENCE  */  
        var is_valid = false;
        $('#experienceTable tbody').on( 'click', '.btnDelete', function (){   
          is_valid = confirm('Are you sure to delete?');
          if (is_valid){
            experienceTable.row( $(this).parents('tr') ).remove().draw();
          }
        });
        /* End Delete EXPERIENCE  */

        /* END EXPERIENCE TABLE */

        /* START FAMILY TABLE */
        var familyTable = $('#familyTable').DataTable({
          "paging"    : false,
          "ordering"  : false,
          "info"      : false,
          "filter"    : false,
          "columnDefs": [{
            "targets"       : -1,
            "data"          : null,
            "defaultContent": "<button class='btn btn-danger btn-sm btnDelete'><i class='fa fa-times' aria-hidden='true'></i></button>"
          }]
        }).draw(false);

        /* Start Save FAMILY Table */
        $('#familyNameErr').hide();
        $('#familyRelationErr').hide();
        $('#familyDateOfBirthErr').hide();
        $('#familyEducationErr').hide();

        $('#saveFamily').click(function() 
        {       
          /* Start FAMILY Form Validation */  
          $('#familyNameErr').hide();
          $('#familyRelationErr').hide();
          $('#familyDateOfBirthErr').hide();
          $('#familyEducationErr').hide();

          var isDataValid = true;        
          if($('#familyName').val() == ""){
            $('#familyName').focus();
            $('#familyNameErr').show();
            isDataValid = false;
          }
          else if($('#familyRelation').val() == ""){
            $('#familyRelation').focus();
            $('#familyRelationErr').show();
            isDataValid = false;
          }
          else if($('#familyDateOfBirth').val() == ""){
              $('#familyDateOfBirth').focus();
              $('#familyDateOfBirthErr').show();
              isDataValid = false;
          } 
          else if($('#familyEducation option:selected').text() == "Pilih"){
            $('#familyEducationErr').focus();
            $('#familyEducationErr').show();
            isDataValid = false;
          }

          if(isDataValid == false)
          {
            return false;
          }   
          /* End FAMILY Form Validation */
          
          var birthDate = $('#familyDateOfBirth').val();  
          familyTable.row.add([
            $('#familyName').val(),
            $('#familyRelation').val(),
            birthDate,
            $('#familyEducation').val(),
            $('#familyOccupation').val()
          ]).draw(false);
          clearDetailInputs();  
          $('#familyName').focus();                              
        });
        /* End Save FAMILY Table */

        /* Start Select FAMILY */
        $('#familyTable tbody').on( 'click', 'tr', function () {
          var rowData = familyTable.row( this ).data();
          if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
          }
          else {
            familyTable.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');                    
          }
        }); 
        /* End Select FAMILY */

        /* Start Delete FAMILY  */  
        var is_valid = false;
        $('#familyTable tbody').on( 'click', '.btnDelete', function (){   
          is_valid = confirm('Are you sure to delete?');
          if (is_valid) 
          {
            familyTable.row( $(this).parents('tr') ).remove().draw();
          }
        });
        /* End Delete FAMILY  */
        /* END FAMILY TABLE */

        /* START LANGUAGE TABLE */
        var languageTable = $('#languageTable').DataTable({
          "paging"    : false,
          "ordering"  : false,
          "info"      : false,
          "filter"    : false,
          "columnDefs": [{
            "targets"       : -1,
            "data"          : null,
            "defaultContent": "<button class='btn btn-danger btn-sm btnDelete'><i class='fa fa-times' aria-hidden='true'></i></button>"
           }]
        });

        /* Start Save LANGUAGE Table */
        $('#languageNameErr').hide();
        $('#writtenErr').hide();
        $('#oralErr').hide();

        $('#saveLanguage').click(function(){
          /* Start LANGUAGE Form Validation */
          var isDataValid = true;
          $('#languageNameErr').hide();
          $('#writtenErr').hide();
          $('#oralErr').hide();
          if($('#languageName option:selected').text() == "Pilih"){
            $('#languageName').focus();
            $('#languageNameErr').show();
            isDataValid = false;
          }
          else if($('#written option:selected').text() == "Pilih"){
            $('#written').focus();
            $('#writtenErr').show();
            isDataValid = false;  
          }
          else if($('#oral option:selected').text() == "Pilih"){
            $('#oral').focus();
            $('#oralErr').show();
            isDataValid = false;  
          }

          if(isDataValid == false){
            return false;
          }

          /* End LANGUAGE Form Validation */
          languageTable.row.add([
            $('#languageName').val(),
            $('#written').val(),
            $('#oral').val(),
            $('#languageInfo').val()
          ]).draw(false);
          clearDetailInputs();  
          $('#languageName').focus();                              
        });
        /* End Save LANGUAGE Table */

        /* Start Select LANGUAGE */
        $('#languageTable tbody').on( 'click', 'tr', function () {
          var rowData = languageTable.row( this ).data();
          if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
          }
          else {
            languageTable.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');                    
          }
        }); 
        /* End Select LANGUAGE */

        /* Start Delete LANGUAGE  */  
        var is_valid = false;
        $('#languageTable tbody').on( 'click', '.btnDelete', function () {   
          is_valid = confirm('Are you sure to delete?');
          if (is_valid) 
          {
            languageTable.row( $(this).parents('tr') ).remove().draw();
          }
        });
        /* End Delete LANGUAGE  */
        /* END LANGUAGE TABLE */

        /* START ORGANIZATION TABLE */
        var organizationTable = $('#organizationTable').DataTable({
          "paging"    : false,
          "ordering"  : false,
          "info"      : false,
          "filter"    : false,
          "columnDefs": [{
            "targets"       : -1,
            "data"          : null,
            "defaultContent": "<button class='btn btn-danger btn-sm btnDelete'><i class='fa fa-times' aria-hidden='true'></i></button>"
            }]
        }).draw(false);

        /* Start Save ORGANIZATION Table */
        $('#organizationNameErr').hide();
        $('#organizationYearErr').hide();
        $('#organizationPositionErr').hide();
        
        $('#saveOrganization').click(function(){       
          /* Start ORGANIZATION Form Validation */
          var isDataValid = true;
          $('#organizationNameErr').hide();
          $('#organizationYearErr').hide();
          $('#organizationPositionErr').hide();

          if($('#organizationName').val() == ""){
            $('#organizationName').focus();
            $('#organizationNameErr').show();
            isDataValid = false;
          }
          else if($('#organizationYear option:selected').text() == "Pilih"){           
            $('#organizationYear').focus();
            $('#organizationYearErr').show();
            isDataValid = false;            
          }
          else if($('#organizationPosition').val() == ""){
            $('#organizationPosition').focus();
            $('#organizationPositionErr').show();
            isDataValid = false;
          }
          if(isDataValid == false){
            return false;
          } 
          /* End ORGANIZATION Form Validation */          
          organizationTable.row.add([
            $('#organizationName').val(),
            $('#organizationYear').val(),
            $('#organizationPosition').val()
          ]).draw(false);
          clearDetailInputs();  
          $('#organizationName').focus();                              
        });
        /* End Save ORGANIZATION Table */

        /* Start Select ORGANIZATION */
        $('#organizationTable tbody').on( 'click', 'tr', function () {
          var rowData = organizationTable.row( this ).data();
          if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
          }
          else {
            organizationTable.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');                    
          }
        }); 
        /* End Select ORGANIZATION */

        /* Start Delete ORGANIZATION  */  
        var is_valid = false;
        $('#organizationTable tbody').on( 'click', '.btnDelete', function () {   
          is_valid = confirm('Are you sure to delete?');
          if (is_valid) {
              organizationTable.row( $(this).parents('tr') ).remove().draw();
          }
        });
        /* End Delete ORGANIZATION  */
        /* END ORGANIZATION TABLE */

        /* START REFERENCE TABLE */
        var referenceTable = $('#referenceTable').DataTable({
          "paging"  : false,
          "ordering": false,
          "info"    : false,
          "filter"  : false,
          "columnDefs": [{
            "targets"       : -1,
            "data"          : null,
            "defaultContent": "<button class='btn btn-danger btn-sm btnDelete'><i class='fa fa-times' aria-hidden='true'></i></button>"
            }]
        }).draw(false);

        /* Start Save REFERENCE Table */
        $('#referenceNameErr').hide();
        $('#referenceRelationErr').hide();      
        $('#referenceContactErr').hide();     
        
        $('#saveReference').click(function() {   
          /* Start REFERENCE Form Validation */
          var isDataValid = true; 
          $('#referenceNameErr').hide();
          $('#referenceRelationErr').hide();
          $('#referenceContactErr').hide();           
          if($('#referenceName').val() == ""){
            $('#referenceName').focus();  
            $('#referenceNameErr').show();
            isDataValid = false;
          }     
          else if($('#referenceRelation').val() == ""){
            $('#referenceRelation').focus();
            $('#referenceRelationErr').show();
            isDataValid = false;
          }
          else if($('#referenceContact').val() == ""){
            $('#referenceContact').focus();
            $('#referenceContactErr').show();
            isDataValid = false;
          } 

          if(isDataValid == false){
            return false;
          } 
          /* End REFERENCE Form Validation */

          referenceTable.row.add([
            $('#referenceName').val(),
            $('#referenceRelation').val(),
            $('#referenceAddress').val(),
            $('#referenceContact').val()
          ]).draw(false);
          clearDetailInputs();  
          $('#referenceName').focus();                              
        });
        /* End Save REFERENCE Table */

        /* Start Select REFERENCE */
        $('#referenceTable tbody').on( 'click', 'tr', function () {
          var rowData = referenceTable.row( this ).data();
          if ( $(this).hasClass('selected') ){
            $(this).removeClass('selected');
          }
          else {
            referenceTable.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');                    
          }
        }); 
        /* End Select REFERENCE */

        /* Start Delete REFERENCE  */ 
        var is_valid = false;
        $('#referenceTable tbody').on( 'click', '.btnDelete', function () {   
          is_valid = confirm('Are you sure to delete?');
          if (is_valid) {
            referenceTable.row( $(this).parents('tr') ).remove().draw();
          }
        });
        /* End Delete REFERENCE  */
        /* END REFERENCE TABLE */

        /* START QUALIFICATION TABLE */
        var qualificationTable = $('#qualificationTable').DataTable({
          "paging"  : false,
          "ordering": false,
          "info"    : false,
          "filter"  : false,
          "columnDefs": [{
            "targets"       : -1,
            "data"          : null,
            "defaultContent": "<button class='btn btn-danger btn-sm btnDelete'><i class='fa fa-times' aria-hidden='true'></i></button>"
          }]
        }).draw(false);

        /* Start Save QUALIFICATION Table */
        $('#qualificationErr').hide();
        $('#qualificationYearErr').hide();
        $('#saveQualification').click(function() {       
          /* Start QUALIFICATION Form Validation */
        var isQualificationCertified = "Y";
          if ($('#isQualificationCertified').is(":checked"))
          { 
            isQualificationCertified = "Y";
          }              
          else
          {
            isQualificationCertified = "T";
          }
          var tes = $('#isQualificationCertified').is(":checked");
          var isDataValid = true;
          $('#qualificationErr').hide();
          $('#qualificationYearErr').hide();

          if($('#qualification').val() == ""){
            $('#qualification').focus();
            $('#qualificationErr').show();
            isDataValid = false;
          }
          else if($('#qualificationYear option:selected').text() == "Pilih"){           
            $('#qualificationYear').focus();
            $('#qualificationYearErr').show();
            isDataValid = false;            
          }
          if(isDataValid == false){
            return false;
          } 
          /* End QUALIFICATION Form Validation */          
          qualificationTable.row.add([
            $('#qualification').val(),
            $('#qualificationYear').val(),
            isQualificationCertified
          ]).draw(false);
          clearDetailInputs();  
          $('#qualification').focus();                              
        });
        /* End Save QUALIFICATION Table */

        /* Start Select QUALIFICATION */
        $('#qualificationTable tbody').on( 'click', 'tr', function () {
          var rowData = qualificationTable.row( this ).data();
          if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
          }
          else {
            qualificationTable.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');                    
          }
        }); 
        /* End Select QUALIFICATION */

        /* Start Delete QUALIFICATION  */  
        var is_valid = false;
        $('#qualificationTable tbody').on( 'click', '.btnDelete', function () {   
          is_valid = confirm('Are you sure to delete?');
          if (is_valid) {
            qualificationTable.row( $(this).parents('tr') ).remove().draw();
          }
        });
        /* End Delete QUALIFICATION  */
        /* END QUALIFICATION TABLE */

        function clearHeaderInputs() {
          $('.my_header [type = text]').each(function(){
            $(this).val('');
          }); 
          $("#fullName").focus();  
          educationTable.clear().draw();
          trainingTable.clear().draw();
          experienceTable.clear().draw();
          familyTable.clear().draw();
          languageTable.clear().draw();
          organizationTable.clear().draw();
          referenceTable.clear().draw();
        }

        /* Start Header Focus */        
        $("#addBiodata").click(function()
        {
          $("#fullName").focus();
        });
        /* Start Header Focus */
        $('#fullNameErr').hide();
        $('#genderErr').hide();
        $('#birthPlaceErr').hide();
        $('#birthDateErr').hide();
        $('#nationalityErr').hide();
        $('#etnicsErr').hide();
        $('#idNoErr').hide();
        $('#idAddressErr').hide();
        $('#currentAddressErr').hide();
        $('#cityAddressErr').hide();
        $('#livingStatusErr').hide();
        $('#religionErr').hide();
        $('#maritalStatusErr').hide();
        $('#groupPositionErr').hide();
        $('#appliedPositionErr').hide();
        $('#cvDateErr').hide();
        $('#heightErr').hide();
        $('#weightErr').hide();
        $('#colorBlindnessErr').hide();
        $('#bloodTypeErr').hide();
        $('#emailAddressErr').hide();
        $('#telpNoErr').hide();
        $('#cellNoErr').hide();
        $('#pointOfHireErr').hide();
        $('#staffingStatusErr').hide();
        $('#dateOfHireErr').hide();
        $('#saveBiodata').on('click', function(){
          // debugger
        /* Start Header Form Validation */
          $('#fullNameErr').hide();
          $('#genderErr').hide();
          $('#birthPlaceErr').hide();
          $('#birthDateErr').hide();
          $('#nationalityErr').hide();
          $('#etnicsErr').hide();
          $('#idNoErr').hide();
          $('#idAddressErr').hide();
          $('#currentAddressErr').hide();
          $('#cityAddressErr').hide();
          $('#livingStatusErr').hide();
          $('#religionErr').hide();
          $('#maritalStatusErr').hide();
          $('#groupPositionErr').hide();
          $('#appliedPositionErr').hide();
          $('#cvDateErr').hide();
          $('#heightErr').hide();
          $('#weightErr').hide();
          $('#colorBlindnessErr').hide();
          $('#bloodTypeErr').hide();
          $('#emailAddressErr').hide();
          $('#telpNoErr').hide();
          $('#cellNoErr').hide();
          $('#pointOfHireErr').hide();
          $('#staffingStatusErr').hide();
          $('#dateOfHireErr').hide();

          var isHeaderValid = true;
          if ($('#fullName').val() == "") {
            $('#fullName').focus(); 
            $('#fullNameErr').show();
            isHeaderValid = false;
          }
          else if ($('#gender option:selected').text() == "Pilih") {
            $('#gender').focus(); 
            $('#genderErr').show();
            isHeaderValid = false;
          }
          else if ($('#birthPlace').val() == "") {
            $('#birthPlace').focus(); 
            $('#birthPlaceErr').show();
            isHeaderValid = false;
          }
          else if ($('#birthDate').val() == "") {
              $('#birthDate').focus(); 
              $('#birthDateErr').show();
              isHeaderValid = false;
          }
          else if ($('#nationality').val() == "") {
            $('#nationality').focus(); 
            $('#nationalityErr').show();
            isHeaderValid = false;
          }
          else if ($('#etnics option:selected').text() == "Pilih") {
            $('#etnics').focus(); 
            $('#etnicsErr').show();
            isHeaderValid = false;
          }
          else if ($('#idNo').val() == "") {
            $('#idNo').focus(); 
            $('#idNoErr').show();
            isHeaderValid = false;
          }
          else if ($('#idAddress').val() == "") {
            $('#idAddress').focus(); 
            $('#idAddressErr').show();
            isHeaderValid = false;
          }
          else if ($('#currentAddress').val() == "") {
            $('#currentAddress').focus(); 
            $('#currentAddressErr').show();
            isHeaderValid = false;
          }
          else if ($('#cityAddress option:selected').text() == "Pilih") {
            $('#cityAddress').focus(); 
            $('#cityAddressErr').show();
            isHeaderValid = false;
          }
          else if ($('#livingStatus option:selected').text() == "Pilih") {
            $('#livingStatus').focus(); 
            $('#livingStatusErr').show();
            isHeaderValid = false;
          }
          else if ($('#religion option:selected').text() == "Pilih") {
            $('#religion').focus(); 
            $('#religionErr').show();
            isHeaderValid = false;
          }
          else if ($('#maritalStatus option:selected').text() == "Pilih") {
            $('#maritalStatus').focus(); 
            $('#maritalStatusErr').show();
            isHeaderValid = false;
          }
          else if ($('#groupPosition option:selected').text() == "Pilih") {
            $('#groupPosition').focus(); 
            $('#groupPositionErr').show();
            isHeaderValid = false;
          }
          else if ($('#appliedPosition option:selected').text() == "Pilih") {
            $('#appliedPosition').focus(); 
            $('#appliedPositionErr').show();
            isHeaderValid = false;
          }
          else if ($('#cvDate').val() == "") {
            $('#cvDate').focus(); 
            $('#cvDateErr').show();
            isHeaderValid = false;
          }
          else if ($('#height').val() == "") {
            $('#height').focus(); 
            $('#heightErr').show();
            isHeaderValid = false;
          }
          else if ($('#weight').val() == "") {
            $('#weight').focus(); 
            $('#weightErr').show();
            isHeaderValid = false;
          }
          else if ($('#colorBlindness option:selected').text() == "Pilih") {
            $('#colorBlindness').focus(); 
            $('#colorBlindnessErr').show();
            isHeaderValid = false;
          }
          else if ($('#bloodType option:selected').text() == "Pilih") {
            $('#bloodType').focus(); 
            $('#bloodTypeErr').show();
            isHeaderValid = false;
          }
          else if ($('#staffingStatus option:selected').text() == "Pilih") {
            $('#staffingStatus').focus(); 
            $('#staffingStatusErr').show();
            isHeaderValid = false;
          }
          else if ($('#dateOfHire').val() == "") {
            $('#dateOfHire').focus(); 
            $('#dateOfHireErr').show();
            isHeaderValid = false;
          }
          else if ($('#emailAddress').val() == "") {
            $('#emailAddress').focus(); 
            $('#emailAddressErr').show();
            isHeaderValid = false;
          }
          else if ($('#telpNo').val() == "") {
            $('#telpNo').focus(); 
            $('#telpNoErr').show();
            isHeaderValid = false;
          }
          else if ($('#cellNo').val() == "") {
            $('#cellNo').focus(); 
            $('#cellNoErr').show();
            isHeaderValid = false;
          }
          else if ($('#pointOfHire').val() == "") {
            $('#pointOfHire').focus(); 
            $('#pointOfHireErr').show();
            isHeaderValid = false;
          }

          if(isHeaderValid == false) {
            return false;
          }
          // alert (isDuplicateId);
          if(isDuplicateId == true) {
            return false;
          }

          var headerDataArr = {
            fullName        : $('#fullName').val(),
            gender          : $('#gender').val(),
            birthPlace      : $('#birthPlace').val(),
            birthDate       : $('#birthDate').val(),
            nationality     : $('#nationality').val(),  
            etnics          : $('#etnics').val(),  
            idNo            : $('#idNo').val(),    
            npwpNo          : $('#npwpNo').val(),    
            bpjsNo          : $('#bpjsNo').val(),  
            idAddress       : $('#idAddress').val(),  
            currentAddress  : $('#currentAddress').val(),    
            cityAddress     : $('#cityAddress').val(),  
            livingStatus    : $('#livingStatus').val(),  
            religion        : $('#religion').val(), 
            driveLicence    : $('#driveLicence').val(),  
            maritalStatus   : $('#maritalStatus').val(), 
            appliedPosition : $('#appliedPosition').val(),  
            cvDate          : $('#cvDate').val(),  
            height          : $('#height').val(),  
            weight          : $('#weight').val(),  
            colorBlindness  : $('#colorBlindness').val(),  
            bloodType       : $('#bloodType').val(),  
            emailAddress    : $('#emailAddress').val(),  
            telpNo          : $('#telpNo').val(),  
            cellNo          : $('#cellNo').val(),
            pointOfHire     : $('#pointOfHire').val(),
            staffingStatus  : $('#staffingStatus').val(),
            dateOfHire      : $('#dateOfHire').val()
          }
            /* Start POST EDUCATION Table */

          var educationDataArr = [];
          function getEducationDetails(){
            var dataCount = educationTable.rows().data().length;
            for (var i = 0; i < dataCount; i++) {
              var dataRow = educationTable.row(i).data();
              educationDataArr.push ({
                schoolName  : dataRow[0],   
                major       : dataRow[1],   
                year        : dataRow[2],   
                city        : dataRow[3],   
                isCertified : dataRow[4]  
              });         
            } 
            return educationDataArr;
          } 

          educationArrData = getEducationDetails();
          /* End POST EDUCATION Table */

          /* Start POST TRAINING Table */
          var trainingDataArr = [];
          function getTrainingDetails(){
            var dataCount = trainingTable.rows().data().length;
            for (var i = 0; i < dataCount; i++) {
              var dataRow = trainingTable.row(i).data();
              trainingDataArr.push ({
                institution   : dataRow[0],  
                classesField  : dataRow[1],  
                year          : dataRow[2],  
                city          : dataRow[3],  
                isCertified   : dataRow[4]       
              });         
            } 
            return trainingDataArr;
          } 

          trainingArrData = getTrainingDetails();
          /* End POST TRAINING Table */

          /* Start POST EXPERIENCE Table */
          var experienceDataArr = [];
          function getExperienceDetails()
          {
            var dataCount = experienceTable.rows().data().length;
            for (var i = 0; i < dataCount; i++) {
              var dataRow = experienceTable.row(i).data();
              experienceDataArr.push ({
                companyName   : dataRow[0],   
                position      : dataRow[1],
                startWork     : dataRow[2],
                isCurrentWork : dataRow[3],
                endWork       : dataRow[4],
                basicSalary   : dataRow[5],   
                allownace     : dataRow[6],       
                jobDesc       : dataRow[7],       
                resignReason  : dataRow[8],            
                addInfo       : dataRow[9]      
              });         
            } 
            return experienceDataArr;
          } 

          experienceArrData = getExperienceDetails();
          /* End POST EXPERIENCE Table */

          /* Start POST FAMILY Table */
          var familyDataArr = [];
          function getFamilyDetails()
          {
            var dataCount = familyTable.rows().data().length;
            for (var i = 0; i < dataCount; i++) {
              var dataRow = familyTable.row(i).data();
              familyDataArr.push ({
                familyName    : dataRow[0],   
                relation      : dataRow[1],   
                birthDate     : dataRow[2],   
                education     : dataRow[3],   
                occupation    : dataRow[4]      
              });         
            } 
            return familyDataArr;
          } 

          familyArrData = getFamilyDetails();
          /* End POST FAMILY Table */

          /* Start POST LANGUAGE Table */
          var languageDataArr = [];
          function getLanguageDetails()
          {
            var dataCount = languageTable.rows().data().length;
            for (var i = 0; i < dataCount; i++) {
              var dataRow = languageTable.row(i).data();
              languageDataArr.push ({
                language  : dataRow[0],   
                written   : dataRow[1],   
                oral      : dataRow[2],   
                remark    : dataRow[3]  
              });         
            } 
            return languageDataArr;
          } 

          languageArrData = getLanguageDetails();
          /* End POST LANGUAGE Table */

          /* Start POST ORGANIZATION Table */
          var organizationDataArr = [];
          function getOrganizationDetails()
          {
            var dataCount = organizationTable.rows().data().length;
            for (var i = 0; i < dataCount; i++) {
              var dataRow = organizationTable.row(i).data();
              organizationDataArr.push ({
                organizationName  : dataRow[0],  
                year              : dataRow[1],  
                position          : dataRow[2]           
              });         
            } 
            return organizationDataArr;
          } 
          organizationArrData = getOrganizationDetails();
          /* End POST ORGANIZATION Table */

          /* Start POST REFERENCE Table */
          var referenceDataArr = [];
          function getReferenceDetails()
          {
            var dataCount = referenceTable.rows().data().length;
            for (var i = 0; i < dataCount; i++) {
              var dataRow = referenceTable.row(i).data();
              referenceDataArr.push ({
                refName   : dataRow[0],   
                relation  : dataRow[1],   
                address   : dataRow[2],   
                contact   : dataRow[3]  
              });         
            } 
            return referenceDataArr;
          } 

          referenceArrData = getReferenceDetails();
          /* End POST REFERENCE Table */

          /* Start POST QUALIFICATION Table */
          var qualificationDataArr = [];
          function getQualificationDetails()
          {
            var dataCount = qualificationTable.rows().data().length;
            for (var i = 0; i < dataCount; i++) {
              var dataRow = qualificationTable.row(i).data();
              qualificationDataArr.push ({
                qualification       : dataRow[0],   
                qualificationYear   : dataRow[1],
                isCertified         : dataRow[2]
              });         
            } 
            return qualificationDataArr;
          } 

          qualificationArrData = getQualificationDetails();
          /* End POST QUALIFICATION Table */

          // debugger
          $.ajax({                
            method : "POST", 
            url : "<?php echo base_url() ?>"+"masters/Mst_bio_rec/Ins", 
            data : {
              saveBiodata      : 1,
              dataHeader       : headerDataArr,
              dataEducation    : educationArrData,
              dataTraining     : trainingDataArr,
              dataExperience   : experienceArrData,
              dataFamily       : familyArrData,
              dataLanguage     : languageArrData,
              dataOrganization : organizationArrData,
              dataReference    : referenceArrData,                            
              dataQualification: qualificationArrData                            
            }, 
            error   : function(data) {
                alert(data);
                // $("#saveMsg").html("Failed to save, please check for data duplication");       
            },
            success : function(data) {
                clearHeaderInputs();
                $("#saveMsg").html("<h5>ID : </h5><div id='succeedId'><h3>"+data+"</h3></div><h5> Data saved</h5>");       
            }
          });
          // table.clear().draw();  
          /* End Header Form Validation */
        });        
        /* End HEADER */
    });
</script>
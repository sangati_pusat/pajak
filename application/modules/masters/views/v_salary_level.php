<style type="text/css">
tr.selected {
  background-color: #B0BED9!important;
}
</style>

<div class="col-md-12">
  <div class="tile">
    <div class="tile-body">
      <div class="form-group row">
        <label class="control-label col-md-2">Client</label>
        <div class="col-md-4">
          <select class="errMsg form-control" id="ptName" name="ptName" required="">
            <option value="Redpath_Timika" selected="">Redpath Timika</option>
          </select> 
        </div>
      </div> 
      <div class="form-group row">
        <label class="control-label col-md-2"></label>
        <div class="col-md-4">
          <input type="button" class="btn btn-info btn-sm" data-dismiss="modal" value="Search" id="searchClient">
        </div>
      </div>
    </div>
  </div>
  <div class="tile">
    <div class="tile-body">      
        <div class="table-responsive">
          <table id="table-salary" class="table table-hover table-bordered">
            <thead class="thead-dark">
              <tr role="row">
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 5%;text-align: center;">Table Id</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 5%;text-align: center;">External Id</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 16%;text-align: center;">Name</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 5%;text-align: center;">Payroll Group</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 5%;text-align: center;">Client</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 5%;text-align: center;">Level</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 5%;text-align: center;">Basic Salary</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 10%;text-align: center;">Pic Edit</th>
                <th class="sorting_disabled" rowspan="1" colspan="1">Edit Time</th>
                <!-- <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 8%;text-align: center;">Basic Salary</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 10%;text-align: center;">Bank Name</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 16%;text-align: center;">Account Name</th> -->
                <!-- <th class="sorting_disabled" rowspan="1" colspan="1">Account No</th> -->
                <th class="sorting_disabled" rowspan="1" colspan="1">Action</th>
              </tr>
            </thead>                    
            <tbody>                                  
            </tbody>
          </table>  
        </div>
    </div>
  </div>
</div>

    
<!-- Start salaryModal UPDATE DATA  -->
<div class="modal fade" id="levelSalaryModal" tabindex="-1" role="dialog" aria-labelledby="salaryModalLabel"> 
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header btn-info">
        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
        <h4 class="modal-title" id="salaryModalLabel">Update Salary Level</h4>
      </div>
      <div>
        <div id="detail" class="panel panel-body panelButtonDetail">
          <div class="container-fluid">
            <div class="tile-body"><br/>
              <div class="form-group row">
                <label class="control-label col-md-2">Name</label>
                <div class="col-md-4">
                  <input class="errMsg form-control col-md-12" type="hidden" placeholder="Id Eksternal" id="bioId">
                  <input class="errMsg form-control col-md-12" type="text" placeholder="Id Eksternal" id="fullName" disabled="">
                </div>
                <label class="control-label col-md-2">Client Name</label>
                <div class="col-md-4">
                  <input class="errMsg form-control col-md-12" type="text" placeholder="Nama Karyawan" id="clientName" disabled="">
                </div>
              </div> 
              <div class="form-group row">
                <label class="control-label col-md-2">Salary Level</label>
                <div class="col-md-4">
                  <select class="errMsg form-control" id="salaryLevel" name="salaryLevel" required="">
                    <option value="" disabled="" selected="">Pilih</option>
                    <?php 
                    $data   = $this->db->query('SELECT payroll_level, basic_salary FROM mst_payroll_level')->result();
                    foreach ($data as $key => $value) {
                    ?>
                    <option value="<?php echo $value->payroll_level; ?>" data-basic='<?php echo $value->basic_salary; ?>'><?php echo $value->payroll_level; ?></option>
                    <?php 
                    }
                    ?>
                  </select> 
                  <!-- <input class="errMsg form-control col-md-12" type="text" placeholder="Id Eksternal" id="salaryLevel"> -->
                </div>
                <label class="control-label col-md-2">Basic Salary</label>
                <div class="col-md-4">
                  <input class="errMsg form-control col-md-12" type="text" placeholder="Nama Karyawan" id="basicSalary">
                </div>
              </div> 
              <div class="form-group row">
                <label class="control-label col-md-2">External ID</label>
                <div class="col-md-4">
                  <input class="errMsg form-control col-md-12" type="text" placeholder="Id Eksternal" id="nie">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> 
      <div class="modal-footer">
        <input type="button" class="btn btn-info btn-sm" data-dismiss="modal" value="Save" id="saveSalary">         
        <input type="button" class="btn btn-primary btn-sm" data-dismiss="modal" value="Close"> 
      </div>
    </div>
  </div> 
</div> 
<!-- End salaryModal UPDATE DATA -->
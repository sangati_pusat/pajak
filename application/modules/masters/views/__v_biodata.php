<!--START FORM DATA -->
<div class="col-md-12" >
  <div class="tile bg-info-bio">
    <!-- <h3 class="tile-title">Barang Masuk</h3> -->
    <div class="tile-body" id="saveMsg">
      <form class="row is_header">
      <!-- <form class="row is_header" action="<?php #echo base_url() ?>transactions/sumbawa/Timesheet/payrollProcess"> -->
        
        <div class="form-group col-sm-12 col-md-12">
          <h2>Input Biodata</h2>
        </div>
        <div class="form-group col-sm-12 col-md-12">
          <input type="button" class="btn btn-dark" id="addBiodata" name="addBiodata" value="Add">
          <input type="button" class="btn btn-dark" id="saveBiodata" name="saveBiodata" value="Save">
        </div>
      </form>
    </div>
  </div>
</div>
<!-- END FORM DATA -->

<div class="col-md-12">
  <div class="tile bg-info-bio">
    <!-- <h3 class="tile-title">Barang Masuk</h3> -->
    <div class="tile-body">
      <div id="biodata">
        <form class="form-horizontal" method="POST" class="my_header">
          <div class="form-group row">
            <div class="control-label col-md-6">ID Card Number
            
              <code id="idNoErr" class="errMsg"><span> : Required</span></code>
              <code id="idNoDuplicate" class="errMsg"><span> Duplicate Id</span></code>           
              <code id="idNoDigit" class="errMsg"><span> Required 16 Digit</span></code>            
              <input type="text" class="form-control" id="idNo" name="idNo" placeholder="Nomor KTP 16 Digit" min="16" max="16" required="">
            </div>
            <div class="control-label col-md-6">Full Name
            <code id="fullNameErr" class="errMsg"><span> : Required</span></code> 
            <input type="text" class="form-control" id="fullName" name="fullName" placeholder="Full Name" required="" autofocus="">
            </div>
          </div>
  
          <div class="form-group row">
            <div class="control-label col-md-6">Date Of Birth
              <code id="birthDateErr" class="errMsg"><span> : Required</span></code>                  
              <input type="Date" class="form-control" id="birthDate" name="birthDate" placeholder="Tanggal Lahir" required="">
            </div>
            <div class="control-label col-md-6">Nationality
              <code id="nationalityErr" class="errMsg"><span> : Required</span></code>          
              <input type="text" class="form-control" id="nationality" name="nationality" placeholder="Warga Negara" required="">
            </div>
          </div>

          <div class="form-group row">
            <div class="control-label col-md-6">Place Of Birth
              <code id="birthPlaceErr" class="errMsg"><span> : Required</span></code>   
              <input type="text" class="form-control" id="birthPlace" name="birthPlace" placeholder="Tempat Lahir" required="">
            </div>
            <div class="control-label col-md-6">Gender
              <code id="genderErr" class="errMsg"><span> : Wajib Dipilih</span></code>          
              <select class="form-control" id="gender" name="gender" required="">
    						<option value="" disabled="" selected="">Pilih</option>
    						<option value="L">Laki-Laki</option>
    						<option value="P">Perempuan</option>
    					</select>	
            </div>
          </div>

          <div class="form-group row">
            <div class="control-label col-md-6">Ethnic
              <code id="etnicsErr" class="errMsg"><span> : Wajib Dipilih</span></code>          
              <select class="form-control" id="etnics" name="etnics" required="">
                <option value="" disabled="" selected="">Pilih</option>
                <option value="Jawa">Jawa</option>
                <option value="Sunda">Sunda</option>
                <option value="Melayu">Melayu</option>
                <option value="Menado">Menado</option>            
                <option value="Batak">Batak</option>
                <option value="Toraja">Toraja</option>
                <option value="Papua">Papua</option>
                <option value="Padang">Padang</option>
                <option value="Lain">Lain-Lain</option>
              </select>                                             
            </div>
            <div class="control-label col-md-6">City
              <code id="cityAddressErr" class="errMsg"><span> : Required</span></code>                
              <select class="form-control" id="cityAddress" name="cityAddress" required="">
                <option value="" disabled="" selected="">Pilih</option>
              </select> 
            </div>
          </div>

          <div class="form-group row">
            <div class="control-label col-md-6">ID Card Address
              <code id="idAddressErr" class="errMsg"><span> : Required</span></code>              
              <input type="text" class="form-control" id="idAddress" name="idAddress" placeholder="Alamat KTP" required="">
            </div>
            <div class="control-label col-md-6">Current Address
              <code id="currentAddressErr" class="errMsg"><span> : Required</span></code>               
              <input type="text" class="form-control" id="currentAddress" name="currentAddress" placeholder="Alamat Sekarang" required="">
            </div>
          </div>

          <div class="form-group row">
            <div class="control-label col-md-6">Living Status
              <code id="livingStatusErr" class="errMsg"><span> : Required</span></code>               
              <select class="form-control" id="livingStatus" name="livingStatus" required="">
                <option value="" disabled="" selected="">Pilih</option>
                <option value="Milik Pribadi">Milik Pribadi</option>
                <option value="Milik Orang Tua">Milik Orang Tua</option>
                <option value="Rumah Kontrakan">Rumah Kontrakan</option>
                <option value="Indekost">Indekost</option>        
                <option value="None">None</option>        
              </select> 
            </div>
            <div class="control-label col-md-6">Religion
              <code id="religionErr" class="errMsg"><span> : Required</span></code>                 
              <select class="form-control" id="religion" name="religion" required="">
                <option value="" disabled="" selected="">Pilih</option>
                <option value="Islam">Islam</option>
                <option value="Kristen">Kristen</option>
                <option value="Katolik">Katolik</option>
                <option value="Budha">Budha</option>        
                <option value="Hindu">Hindu</option>        
                <option value="Khonghucu">Khonghucu</option>
                <option value="Lain">Lain-Lain</option>       
              </select>
           </div>
          </div>

          <div class="form-group row">
            <div class="control-label col-md-6">Driving License
              <input type="text" class="form-control" id="driveLicence" name="driveLicence" placeholder="A,B,C,..">
            </div>
            <div class="control-label col-md-6">Marital Status
              <code id="maritalStatusErr" class="errMsg"><span> : Required</span></code>                    
              <select class="form-control" id="maritalStatus" name="maritalStatus" required="">
                <option value="" disabled="" selected="">Pilih</option>
                <option value="TK0">TK-0</option>
                <option value="TK1">TK-1</option>
                <option value="TK2">TK-2</option>
                <option value="TK3">TK-3</option>
                <option value="K0">K-0</option>
                <option value="K1">K-1</option>
                <option value="K2">K-2</option>
                <option value="K3">K-3</option>
              </select>
            </div>
          </div>

          <div class="form-group row">
            <div class="control-label col-md-6">Taxpayer No
              <input type="text" class="form-control" id="npwpNo" name="npwpNo" placeholder="Nomor NPWP" required="">
            </div>
            <div class="control-label col-md-6">BPJS No
              <input type="text" class="form-control" id="bpjsNo" name="bpjsNo" placeholder="Nomor BPJS">
            </div>
          </div>

          <div class="form-group row">
            <div class="control-label col-md-6">Group Position
              <code id="groupPositionErr" class="errMsg"><span> : Wajib Dipilih</span></code>         
              <select class="form-control" id="groupPosition" name="groupPosition" required="">
                <option value="" disabled="" selected="">Pilih</option>
              </select>                           
              <!-- <input class="form-control col-md-5" type="text" placeholder="Is Glasses" id="is_glasses" required=""> -->
            </div>
            <div class="control-label col-md-6">Position
            <code id="appliedPositionErr" class="errMsg"><span> : Wajib Dipilih</span></code>         
            <select class="form-control" id="appliedPosition" name="appliedPosition" required="">
              <option value="" disabled="" selected="">Pilih</option>
            </select>         
            </div>
          </div>

          <div class="form-group row">
            <div class="control-label col-md-6">Height
              <code id="heightErr" class="errMsg"><span> : Required</span></code>                   
              <input type="number" class="form-control" id="height" name="height" min="0" placeholder="Tinggi Badan" required="">
            </div>
            <div class="control-label col-md-6">Weight
              <code id="weightErr" class="errMsg"><span> : Required</span></code> 
              <input type="number" class="form-control" id="weight" name="weight" min="0" placeholder="Berat Badan" required="">
            </div>
          </div>

          <div class="form-group row">
            <div class="control-label col-md-6">Color Blindness
              <code id="colorBlindnessErr" class="errMsg"><span> : Wajib Dipilih</span></code>  
              <select class="form-control" id="colorBlindness" name="colorBlindness">
                <option value="" disabled="" selected="">Pilih</option>
                <option value="1">Ya</option>
                <option value="0">Tidak</option>
              </select>
            </div>
            <div class="control-label col-md-6">Blood Type
              <code id="bloodTypeErr" class="errMsg"><span> : Wajib Dipilih</span></code>   
              <select class="form-control" id="bloodType" name="bloodType" required="">
                <option value="" disabled="" selected="">Pilih</option>
                <option value="A">A</option>
                <option value="B">B</option>
                <option value="AB">AB</option>
                <option value="O">O</option>        
                <option value="None">None</option>        
              </select>
            </div>
          </div>

          <div class="form-group row">
            <div class="control-label col-md-6">Phone Number
              <code id="cellNoErr" class="errMsg"><span> : Required</span></code> 
              <input type="text" class="form-control" id="cellNo" name="cellNo" placeholder="Nomor HP" required="">
            </div>
            <div class="control-label col-md-6">Telephone Number
              <code id="telpNoErr" class="errMsg"><span> : Required</span></code>       
              <input type="text" class="form-control" id="telpNo" name="telpNo" placeholder="Nomor Telepon" required="">
            </div>
          </div>

          <div class="form-group row">
            <div class="control-label col-md-6">Email
             <code id="emailAddressErr" class="errMsg"><span> : Required</span></code>      
             <input type="email" class="form-control" id="emailAddress" name="emailAddress" placeholder="Alamat Email" required="">
            </div>
            <div class="control-label col-md-6">Point Of Hire
              <code id="pointOfHireErr" class="errMsg"><span> : Wajib Dipilih</span></code>   
              <select class="form-control" id="pointOfHire" name="pointOfHire" required="">
                <option value="" disabled="" selected="">Pilih</option>
                <option value="L">Local</option>
                <option value="NL">Non Local</option>
                <option value="NP">Non Papua</option>
                <option value="PO">Papua Others</option>
                <option value="PO">Papua 7 Ethnic</option>
              </select> 
            </div>
          </div>

          <div class="form-group row">
            <div class="control-label col-md-6">CV Date
              <code id="cvDateErr" class="errMsg"><span> : Required</span></code>                 
          <input type="Date" class="form-control" id="cvDate" name="cvDate" placeholder="Tanggal CV" required="">
           </div>
            <div class="control-label col-md-6">Staff / Non Staff
              <code id="staffingStatusErr" class="errMsg"><span>: Wajib Dipilih</span></code>   
              <select class="form-control" id="staffingStatus" name="staffingStatus" required="">
                <option value="" disabled="">Pilih</option>
                <option value="0" selected="">Non Staff</option>
                <option value="1">Staff</option>            
              </select>
           </div>
          </div>  
        </form>


        <div class="tile-footer">
          
        </div>
      </div>     
  	</div>
  </div>
</div>

<div class="col-md-12">
  <div class="tile bg-info-bio">
    <div class="tile-body">
        <div id="horizontalTab">
          <ul>
            <li><a href="#ed">Educations</a></li>
            <li><a href="#tr">Trainings</a></li>
            <li><a href="#ex">Experiences</a></li>
            <li><a href="#fa">Family Data</a></li>
            <li><a href="#la">Languages</a></li>
            <li><a href="#or">Organizations</a></li>
            <li><a href="#re">References</a></li>
            <li><a href="#qa">Qualification</a></li>
          </ul>
       <!-- START DATA TABLE -->
          <div id='ed'>
            <input type="button" class="btn btn-primary btn-sm newEducation" id="addEducation" name="addEducation" data-toggle="modal" data-target="#educationModal" value="Add Detail">
            <table class="table table-hover table-bordered" id="educationTable">
              <thead class="thead-dark">
                <tr>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 139px;">Institution</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 118px;">Major</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 169px;">Year</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 176px;">City</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 176px;">Certificate</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 138px;">Delete</th>
                </tr>
              </thead>
            </table>
          </div>
          <div  id='tr'>
            <input type="button" class="btn btn-primary btn-sm newTraining" id="addTraining" name="addTraining" data-toggle="modal" data-target="#trainingModal" value="Add Detail">
            <table class="table table-hover table-bordered" id="trainingTable">
              <thead class="thead-dark">
                <tr>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 139px;">Institution</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 118px;">Major</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 169px;">Year</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 176px;">City</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 176px;">Certificate</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 138px;">Delete</th>
                </tr>
              </thead>
            </table>
          </div>
          <div  id='ex'>
            <input type="button" class="btn btn-primary btn-sm newExperience" id="addExperience" name="addExperience" data-toggle="modal" data-target="#experienceModal" value="Add Detail">
            <table class="table table-hover table-bordered" id="experienceTable">
              <thead class="thead-dark">
                <tr>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 139px;">Company</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 118px;">Position</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 130px;">Join Date</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Current</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 150px;">End Of Working</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 150px;">Basic Salary</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 130px;">Allowance</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 176px;">Job Desc</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 195px;">Quit Reason</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 195px;">Information</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 120px;">Delete</th></tr>
              </thead>
            </table>
          </div>
          <div  id='fa'>
            <input type="button" class="btn btn-primary btn-sm newFamily" id="addFamily" name="addFamily" data-toggle="modal" data-target="#familyModal" value="Add Detail">
            <table class="table table-hover table-bordered" id="familyTable">
              <thead class="thead-dark">
                <tr>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 139px;">Name</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 118px;">Relationship</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 169px;">Date Of Birth</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 176px;">Education</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 176px;">Occupation</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 138px;">Delete</th></tr>
              </thead>
            </table>
          </div>
          <div  id='la'>
            <input type="button" class="btn btn-primary btn-sm" id="addLanguage" name="addLanguage" data-toggle="modal" data-target="#languageModal" value="Add Detail">
            <table class="table table-hover table-bordered" id="languageTable">
              <thead class="thead-dark">
                <tr>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 139px;">Language</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 118px;">Writing</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 169px;">Oral</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 176px;">Information</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 138px;">Delete</th></tr>
              </thead>
            </table>
          </div>
          <div  id='or'>
            <input type="button" class="btn btn-primary btn-sm" id="addOrganization" name="addOrganization" data-toggle="modal" data-target="#organizationModal" value="Add Detail">
            <table class="table table-hover table-bordered" id="organizationTable">
              <thead class="thead-dark">
                <tr>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 139px;">Organization</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 169px;">Year</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 176px;">Position</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 138px;">Delete</th></tr>
              </thead>
            </table>
          </div>
          <div  id='re'>
            <input type="button" class="btn btn-primary btn-sm" id="addReference" name="addReference" data-toggle="modal" data-target="#referenceModal" value="Add Detail">
            <table class="table table-hover table-bordered" id="referenceTable">
              <thead class="thead-dark">
                <tr>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 139px;">Name</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 118px;">Relationsship</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 169px;">Address</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 176px;">Contact No</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 138px;">Delete</th></tr>
              </thead>
            </table>
          </div>
          <div  id='qa'>
            <input type="button" class="btn btn-primary btn-sm newQualification" id="addQualification" name="addQualification" data-toggle="modal" data-target="#qualificationModal" value="Add Detail">
            <table class="table table-hover table-bordered" id="qualificationTable">
              <thead class="thead-dark">
                <tr>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 150px;">Qualification</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Year Period</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Certificate</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Delete</th></tr>
              </thead>
            </table>
          </div>
        </div>
        <!-- END DATA TABLE -->
    </div>
  </div>
</div>

<div class="modal fade" id="educationModal" tabindex="-1" role="dialog" aria-labelledby="educationModalLabel"> 
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <!-- Start modal-header -->
      <div class="modal-header">
        <h4 class="modal-title" id="educationModalLabel">Education</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
        <!-- End modal-header -->
        <!-- Start modal-body -->
      <div class="modal-body">
        <!-- Start Form PENDIDIKAN -->
        <form method="post" class="my_detail">
          <div class="form-group">
            <label class="control-label" for="schoolName">Institution<code id="schoolNameErr" class="errMsg"><span> : Required</span></code></label>
            <input class="form-control" type="text" id="schoolName" name="schoolName" placeholder="Nama Lembaga">
          </div> 

          <div class="form-group">
            <label class="control-label" for="educationMajor">Major<code id="educationMajorErr" class="errMsg"><span> : Required</span></code></label>
            <input class="form-control" type="text" id="educationMajor" name="educationMajor" placeholder="Nama Jurusan">
          </div> 

          <div class="form-group">
            <label class="control-label" for="educationYear">Year<code id="educationYearErr" class="errMsg"><span> : Wajib Dipilih</span></code></label>
            <select class="form-control" id="educationYear" name="educationYear">
              <option value="" disabled="" selected="">Pilih</option>
              <script type="text/javascript">
                var dt = new Date();
                var startYear = dt.getFullYear();
                var endYear = startYear - 80;             
                for (var i = startYear; i >= endYear; i--) 
                {
                  document.write("<option value='"+i+"'>"+i+"</option>");           
                }
              </script>
            </select>         
          </div>                
          <div class="form-group">
            <label class="control-label" for="educationCity">City<code id="educationCityErr" class="errMsg"><span> : Wajib Dipilih</span></code></label>
            <input class="form-control" type="text" id="educationCity" name="educationCity" placeholder="Nama Kota">
          </div>

          <div class="form-group">
            <label class="control-label" for="isCertified">Certificate</label><br>
            <input type="checkbox" checked="" id="isCertified" name="isCertified" value="1">
          </div>                                                                                                                       
        </form> 
      </div> 
      <div class="modal-footer">
        <input type="button" class="btn btn-primary btn-sm" id="saveEducation" name="saveEducation" value="Save"> 
        <input type="button" class="btn btn-primary btn-sm" data-dismiss="modal" value="Exit"> 
      </div>
    </div>
  </div> 
</div> 

<div class="modal fade" id="trainingModal" tabindex="-1" role="dialog" aria-labelledby="trainingModalLabel"> 
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <!-- Start modal-header -->
      <div class="modal-header">
        <h4 class="modal-title" id="trainingModalLabel">Training</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <!-- End modal-header -->
      <!-- Start modal-body -->
      <div class="modal-body">
        <!-- Start Form TRAINING -->
        <form method="post" class="my_detail">
          <div class="form-group">
            <label class="control-label" for="instituteName">Institution<code id="instituteNameErr" class="errMsg"><span> : Required</span></code></label>
            <input class="form-control" type="text" id="instituteName" name="instituteName" placeholder="Nama Lembaga">
          </div>  

          <div class="form-group">
            <label class="control-label" for="classesField">Field<code id="classesFieldErr" class="errMsg"><span> : Required</span></code></label>
            <input class="form-control" type="text" id="classesField" name="classesField" placeholder="Bidang Training">
          </div>                          

          <div class="form-group">
            <label class="control-label" for="trainingYear">Year<code id="trainingYearErr" class="errMsg"><span> : Required</span></code></label>
            <select class="form-control" id="trainingYear" name="trainingYear">
              <option value="" disabled="" selected="">Pilih</option>
              <script type="text/javascript">
                var dt = new Date();
                var startYear = dt.getFullYear();
                var endYear = startYear - 80;             
                for (var i = startYear; i >= endYear; i--) 
                {
                  document.write("<option value='"+i+"'>"+i+"</option>");           
                }
              </script>
            </select>         
          </div>                              
  
          <div class="form-group">
              <label class="control-label" for="trainingCity">City<code id="trainingCityErr" class="errMsg"><span> : Required</span></code></label>
              <input class="form-control" type="text" id="trainingCity" name="trainingCity" placeholder="Nama Kota">
          </div>

          <div class="form-group">
              <label class="control-label" for="isTrainingCertified">Certificate</label><br>
              <input type="checkbox" checked="" id="isTrainingCertified" name="isTrainingCertified">
          </div>                                                                                                                                
        </form>
          <!-- Start Form TRAINING -->  
      </div> 
    <!-- End modal-body -->
    <!-- Start modal-footer -->
      <div class="modal-footer">
        <input type="button" class="btn btn-primary btn-sm" id="saveTraining" name="saveTraining" value="Save"> 
        <input type="button" class="btn btn-primary btn-sm" data-dismiss="modal" value="Exit"> 
      </div>
          <!-- End modal-footer -->
    </div>
  </div> 
</div> 

<div class="modal fade" id="experienceModal" tabindex="-1" role="dialog" aria-labelledby="experienceModalLabel"> 
  <!-- Start modal-dialog -->
  <div class="modal-dialog" role="document">
    <!-- Start modal-content -->
    <div class="modal-content">
      <!-- Start modal-header -->
      <div class="modal-header">
          <h4 class="modal-title" id="experienceModalLabel">Experience</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <!-- End modal-header -->
        <!-- Start modal-body -->
        <div class="modal-body">
        <!-- Start Form PENGALAMAN -->
        <form method="post" class="my_detail">
            <div class="form-group">
                <label class="control-label" for="experienceCompanyName">Company<code id="experienceCompanyNameErr" class="errMsg"><span> : Required</span></code></label>
                <input class="form-control" type="text" id="experienceCompanyName" name="experienceCompanyName" placeholder="Nama Perusahaan">
            </div> 

            <div class="form-group">
                <label class="control-label" for="experiencePosition">Position<code id="experiencePositionErr" class="errMsg"><span> : Required</span></code></label>
                <input class="form-control" type="text" id="experiencePosition" name="experiencePosition" placeholder="Nama Jabatan">
            </div>

          <div class="form-group">
            <label class="control-label" for="startWorkDate">Join Date<code id="startWorkDateErr" class="errMsg"><span> : Required</span></code></label>
            <input class="form-control" type="date" id="startWorkDate" name="startWorkDate" placeholder="Tanggal Bergabung">
          </div>

          <div class="form-group">
            <!-- <label class="control-label" for="isCurrentWork">Masih Bekerja</label><br> -->
            <input type="checkbox" checked="" id="isCurrentWork" name="isCurrentWork" value="1"><b>  Current</b>  
          </div> 

                <div class="form-group">
                    <label class="control-label" for="endWorkDate">Quit Date</label>
                    <input class="form-control" type="date" id="endWorkDate" name="endWorkDate" placeholder="Tanggal Keluar">
                </div>

                <div class="form-group">
                    <label class="control-label" for="experienceBasicSalary">Basic Salary<code id="experienceBasicSalaryErr" class="errMsg"><span> : Required Angka</span></code></label>
                    <input class="form-control" type="number" min="0.00" step="0.50" id="experienceBasicSalary" name="experienceBasicSalary" placeholder="Gaji Pokok">
                </div>

                <div class="form-group">
                    <label class="control-label" for="experienceAllowance">Allowance</label>
                    <input class="form-control" type="number" min="0.00" step="0.50" id="experienceAllowance" name="experienceAllowance" placeholder="Nilai Tunjangan">
                </div>

                <div class="form-group">
                    <label class="control-label" for="jobDesc">Jobs<code id="jobDescErr" class="errMsg"><span> : Required</span></code></label>
                    <input class="form-control" type="text" id="jobDesc" name="jobDesc" placeholder="Job Desc">
                </div>

                <div class="form-group">
                    <label class="control-label" for="resignReason">Quit Reason<code id="resignReasonErr" class="errMsg"><span> : Required</span></code></label>
                    <select class="form-control" id="resignReason" name="resignReason">
                      <option value="" disabled="" selected="">Pilih</option> 
                      <option value="Resign">Resign</option>  
                      <option value="Contract Completed">Contract Completed</option>  
                      <option value="Terminated">Terminated</option>  
                    </select>
                </div>

                <div class="form-group">
                    <label class="control-label" for="addInfo">Information</label>
                    <input class="form-control" type="text" id="addInfo" name="addInfo" placeholder="Keterangan">
                </div>                                                                                                                                                                                                                                               
            </form>
            <!-- End Form PENGALAMAN -->  
        </div> 
        <!-- End modal-body -->
        <!-- Start modal-footer -->
        <div class="modal-footer">
          <input type="button" class="btn btn-primary btn-sm" id="saveExperience" name="saveExperience" value="Save"> 
          <input type="button" class="btn btn-primary btn-sm" data-dismiss="modal" value="Exit"> 
        </div>
        <!-- End modal-footer -->
    </div>
    <!-- End modal-content -->
  </div>
  <!-- End modal-dialog --> 
</div> 

<!-- Start familyModal -->
<div class="modal fade" id="familyModal" tabindex="-1" role="dialog" aria-labelledby="familyModalLabel"> 
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <!-- Start modal-header -->
      <div class="modal-header">
                    <h4 class="modal-title" id="familyModalLabel">Family Data</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  </div>
                  <!-- End modal-header -->
                  <!-- Start modal-body -->
                  <div class="modal-body">
                    <!-- Start Form FAMILY -->
                    <form method="post" class="my_detail">
                        
          <div class="form-group">
                        <label class="control-label" for="familyName">Name<code id="familyNameErr" class="errMsg"><span> : Required</span></code></label>
                        <input class="form-control" type="text" id="familyName" name="familyName" placeholder="Nama Keluarga">
                    </div>

                        <div class="form-group">
                        <label class="control-label" for="familyRelation">Relationship<code id="familyRelationErr" class="errMsg"><span> : Required</span></code></label>
                        <input class="form-control" type="text" id="familyRelation" name="familyRelation" placeholder="Hubungan Keluarga">
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="familyDateOfBirth">Date Of Birth<code id="familyDateOfBirthErr" class="errMsg"><span> : Required</span></code></label>
                        <input class="form-control" type="date" id="familyDateOfBirth" name="familyDateOfBirth" placeholder="Tanggal Lahir">
                    </div>
              <div class="form-group">
            <label class="control-label" for="familyEducation">Education<code id="familyEducationErr" class="errMsg"><span> : Wajib Dipilih</span></code></label>
            <select class="form-control" id="familyEducation" name="familyEducation">
              <option value="" disabled="" selected="">Pilih</option>
              <option value="None">None</option>
              <option value="TK">TK</option>
              <option value="SD">SD</option>
              <option value="SMP">SMP</option>
              <option value="SMU">SMU</option>
              <option value="S1">S1</option>            
              <option value="S2">S2</option>
              <option value="S3">S3</option>
            </select> 
          </div>                              

                        <div class="form-group">
                        <label class="control-label" for="familyOccupation">Occupation</label>
                        <input class="form-control" type="text" id="familyOccupation" name="familyOccupation" placeholder="Nama Pekerjaan">
                    </div>                                                                                                                                  
                    </form>  
                    <!-- Start Form FAMILY -->
                  </div> 
                <!-- End modal-body -->
                <!-- Start modal-footer -->
                <div class="modal-footer">
                  <input type="button" class="btn btn-primary btn-sm" id="saveFamily" name="saveFamily" value="Save"> 
                  <input type="button" class="btn btn-primary btn-sm" data-dismiss="modal" value="Exit"> 
                </div>
                <!-- End modal-footer -->
    </div>
  </div> 
</div> 
<!-- End familyModal -->

<!-- Start languageModal -->
<div class="modal fade" id="languageModal" tabindex="-1" role="dialog" aria-labelledby="languageModalLabel"> 
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <!-- Start modal-header -->
      <div class="modal-header">
        <h4 class="modal-title" id="languageModalLabel">Language</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <!-- End modal-header -->
      <!-- Start modal-body -->
      <div class="modal-body">
        <!-- Start Form BAHASA -->
        <form method="post" class="my_detail">                                
          <div class="form-group">
            <label class="control-label" for="languageName">Language<code id="languageNameErr" class="errMsg"><span> : Required</span></code></label>
            <select class="form-control" id="languageName" name="languageName">
              <option value="" disabled="" selected="">Pilih</option>
              <option value="Indonesia">Indonesia</option>
              <option value="Inggris">Inggris</option>
              <option value="Mandarin">Mandarin</option>
              <option value="Lain">Lain</option>        
            </select>
          </div>

          <div class="form-group">
            <label class="control-label" for="written">Writing<code id="writtenErr" class="errMsg"><span> : Required</span></code></label>
            <select class="form-control" id="written" name="written">
              <option value="" disabled="" selected="">Pilih</option>
              <option value="S">Sedang</option>
              <option value="CB">Cukup Baik</option>
              <option value="B">Baik</option>
              <option value="SB">Sangat Baik</option>       
            </select>
          </div>

          <div class="form-group">
            <label class="control-label" for="oral">Oral<code id="oralErr" class="errMsg"><span> : Required</span></code></label>
            <select class="form-control" id="oral" name="oral">
              <option value="" disabled="" selected="">Pilih</option>
              <option value="S">Sedang</option>
              <option value="CB">Cukup Baik</option>
              <option value="B">Baik</option>
              <option value="SB">Sangat Baik</option>       
            </select>
          </div>                                

          <div class="form-group">
              <label class="control-label" for="languageInfo">Information</label>
              <input class="form-control" type="text" id="languageInfo" name="languageInfo" placeholder="Keterangan">
          </div>                                                                                                                                  
        </form> 
          <!-- End Form BAHASA --> 
      </div> 
      <!-- End modal-body -->
      <!-- Start modal-footer -->
      <div class="modal-footer">
        <input type="button" class="btn btn-primary btn-sm" id="saveLanguage" name="saveLanguage" value="Save"> 
        <input type="button" class="btn btn-primary btn-sm" data-dismiss="modal" value="Exit"> 
      </div>
      <!-- End modal-footer -->
    </div>
  </div> 
</div> 
<!-- End languageModal -->

<!-- Start organizationModal -->
<div class="modal fade" id="organizationModal" tabindex="-1" role="dialog" aria-labelledby="organizationModalLabel"> 
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <!-- Start modal-header -->
      <div class="modal-header">
        <h4 class="modal-title" id="organizationModalLabel">Organizations</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <!-- End modal-header -->
      <!-- Start modal-body -->
      <div class="modal-body">
        <!-- Start Form ORGANISASI -->
        <form method="post" class="my_detail">                                
          <div class="form-group">
            <label class="control-label" for="organizationName">Organization Name <code id="organizationNameErr" class="errMsg"><span> : Required</span></code></label>
            <input class="form-control" type="text" id="organizationName" name="organizationName" placeholder="Nama Organisasi">
          </div>

          <div class="form-group">
          <label class="control-label" for="organizationYear">Year<code id="organizationYearErr" class="errMsg"><span> : Required</span></code></label>
          <select class="form-control" id="organizationYear" name="organizationYear">
            <option value="" disabled="" selected="">Pilih</option>
            <script type="text/javascript">
              var dt = new Date();
              var startYear = dt.getFullYear();
              var endYear = startYear - 80;             
              for (var i = startYear; i >= endYear; i--) 
              {
                document.write("<option value='"+i+"'>"+i+"</option>");           
              }
              </script>
            </select>   
          </div>                                                                

          <div class="form-group">
              <label class="control-label" for="organizationPosition">Position<code id="organizationPositionErr" class="errMsg"><span> : Required</span></code></label>
              <input class="form-control" type="text" id="organizationPosition" name="organizationPosition" placeholder="Posisi">
          </div>                                                                                                                                  
        </form>  
          <!-- End Form ORGANISASI -->
      </div> 
      <!-- End modal-body -->
      <!-- Start modal-footer -->
      <div class="modal-footer">
        <input type="button" class="btn btn-primary btn-sm" id="saveOrganization" name="saveOrganization" value="Save"> 
        <input type="button" class="btn btn-primary btn-sm" data-dismiss="modal" value="Exit"> 
      </div>
      <!-- End modal-footer -->
    </div>
  </div> 
</div> 
<!-- End organizationModal -->
<!-- Start referenceModal -->
<div class="modal fade" id="referenceModal" tabindex="-1" role="dialog" aria-labelledby="referenceModalLabel"> 
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <!-- Start modal-header -->
      <div class="modal-header">
        <h4 class="modal-title" id="referenceModalLabel">Reference</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
        <!-- End modal-header -->
        <!-- Start modal-body -->
        <div class="modal-body">
          <!-- Start Form REFERENSI -->
          <form method="post" class="my_detail">                                
            <div class="form-group">
              <label class="control-label" for="referenceName">Name<code id="referenceNameErr" class="errMsg"><span> : Required</span></code></label>
              <input class="form-control" type="text" id="referenceName" name="referenceName" placeholder="Nama Referensi">
            </div>
            
            <div class="form-group">
              <label class="control-label" for="referenceRelation">Relationship<code id="referenceRelationErr" class="errMsg"><span> : Required</span></code></label>
              <input class="form-control" type="text" id="referenceRelation" name="referenceRelation" placeholder="Hubungan">
            </div>
            
            <div class="form-group">
              <label class="control-label" for="referenceAddress">Address</label>
              <input class="form-control" type="text" id="referenceAddress" name="referenceAddress" placeholder="Alamat Referensi">
            </div>                                                              

            <div class="form-group">
                <label class="control-label" for="referenceContact">Contact No<code id="referenceContactErr" class="errMsg"><span> : Required</span></code></label>
                <input class="form-control" type="text" id="referenceContact" name="referenceContact" placeholder="No Kontak">
            </div>                                                                                                                                  
          </form> 
          <!-- End Form REFERENSI --> 
        </div> 
      <!-- End modal-body -->
      <!-- Start modal-footer -->
      <div class="modal-footer">
        <input type="button" class="btn btn-primary btn-sm" id="saveReference" name="saveReference" value="Save"> 
        <input type="button" class="btn btn-primary btn-sm" data-dismiss="modal" value="Exit"> 
      </div>
      <!-- End modal-footer -->
    </div>
  </div> 
</div> 
<!-- End referenceModal -->

<!-- Start referenceModal -->
<div class="modal fade" id="qualificationModal" tabindex="-1" role="dialog" aria-labelledby="qualificationModalLabel"> 
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <!-- Start modal-header -->
      <div class="modal-header">
        <h4 class="modal-title" id="qualificationModalLabel">Qualification</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
          <!-- End modal-header -->
          <!-- Start modal-body -->
        <div class="modal-body">
            <!-- Start Form REFERENSI -->
          <form method="post" class="my_detail">                                
            <div class="form-group">
              <label class="control-label" for="qualification">Qualification<code id="qualificationErr" class="errMsg"><span> : Required</span></code></label>
              <input class="form-control" type="text" id="qualification" name="qualification" placeholder="Kualifikasi">
            </div>
            
            <div class="form-group">
            <label class="control-label" for="qualificationYear">Year Period<code id="qualificationYearErr" class="errMsg"><span> : Required</span></code></label>
            <select class="form-control" id="qualificationYear" name="qualificationYear">
              <option value="" disabled="" selected="">Pilih</option>
              <script type="text/javascript">
                var dt = new Date();
                var startYear = dt.getFullYear();
                var endYear = startYear - 80;             
                for (var i = startYear; i >= endYear; i--) 
                {
                  document.write("<option value='"+i+"'>"+i+"</option>");           
                }
              </script>
            </select>   
          </div> 

          <div class="form-group">
              <label class="control-label" for="isQualificationCertified">Certificate</label><br>
              <input type="checkbox" checked="" id="isQualificationCertified" name="isQualificationCertified">
          </div> 
                                                                                                                                          
          </form> 
          <!-- End Form REFERENSI --> 
        </div> 
      <!-- End modal-body -->
      <!-- Start modal-footer -->
      <div class="modal-footer">
        <input type="button" class="btn btn-primary btn-sm" id="saveQualification" name="saveQualification" value="Save"> 
        <input type="button" class="btn btn-primary btn-sm" data-dismiss="modal" value="Exit"> 
      </div>
      <!-- End modal-footer -->
    </div>
  </div> 
</div> 
<!-- End referenceModal -->
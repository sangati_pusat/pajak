<style type="text/css">
tr.selected {
  background-color: #B0BED9!important;
}
</style>

<div class="col-md-12">
  <div class="tile">
    <div class="tile-body">
      <div class="form-group row">
        <label class="control-label col-md-2">Client</label>
        <div class="col-md-4">
          <select class="errMsg form-control" id="ptName" name="ptName" required="">
            <option value="Redpath_Timika" selected="">Redpath Timika</option>
          </select> 
        </div>
      </div> 
      <div class="form-group row">
        <label class="control-label col-md-2"></label>
        <div class="col-md-4">
          <input type="button" class="btn btn-info btn-sm" data-dismiss="modal" value="Search" id="searchClient">
        </div>
      </div>
    </div>
  </div>
  <div class="tile">
    <div class="tile-body">      
      <div class="table-responsive">
        <table id="table-salary" class="table table-hover table-bordered">
          <thead class="thead-dark">
            <tr role="row">
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 5%;text-align: center;">Table Id</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 5%;text-align: center;">External Id</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 16%;text-align: center;">Name</th>
              <!-- <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 5%;text-align: center;">Payroll Group</th> -->
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 5%;text-align: center;">Client</th>
              <!-- <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 5%;text-align: center;">Level</th> -->
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 10%;text-align: center;">Basic Salary</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 10%;text-align: center;">Bank Name</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 16%;text-align: center;">Account Name</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="text-align: center;">Account No</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 10%;text-align: center;">Pic Edit</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 16%;text-align: center;">Edit Time</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="text-align: center;">Action</th>
            </tr>
          </thead>                    
          <tbody>                                  
          </tbody>
        </table>  
      </div>
    </div>
  </div>
</div>

    
<!-- Start salaryModal UPDATE DATA  -->
<div class="modal fade" id="levelSalaryModal" tabindex="-1" role="dialog" aria-labelledby="salaryModalLabel"> 
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header btn-warning">
        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
        <h4 class="modal-title" id="salaryModalLabel">Update Bank Account</h4>
      </div>
      <div>
        <div id="detail" class="panel panel-body panelButtonDetail">
          <div class="container-fluid">
            <div class="tile-body">
              <div class="form-group row">
                <label class="control-label col-md-2">Name</label>
                <div class="col-md-4">
                  <input class="errMsg form-control col-md-12" type="hidden" placeholder="Id" id="salaryId">
                  <input class="errMsg form-control col-md-12" type="text" placeholder="Id Eksternal" id="fullName" disabled="">
                </div>
                <label class="control-label col-md-2">Client Name</label>
                <div class="col-md-4">
                  <input class="errMsg form-control col-md-12" type="text" placeholder="Nama Karyawan" id="clientName" disabled="">
                </div>
              </div> 
              <div class="form-group row">
                <label class="control-label col-md-2">Payroll Group</label>
                <div class="col-md-4">
                  <input class="errMsg form-control col-md-12" type="text" placeholder="Group Payroll" id="payrollGroup" disabled="">
                </div>
                <label class="control-label col-md-2">Bank</label>
                <div class="col-md-4">
                  <select class="errMsg form-control" id="bankName" name="bankName" required="">
                    <!-- <option value="" disabled="" selected="">Pilih</option>
                    <option value="BCA">BCA</option>
                    <option value="BNI">BNI</option>
                    <option value="BNI SYARIAH">BNI SYARIAH</option>
                    <option value="BRI">BRI</option>
                    <option value="BRI SYARIAH">BRI SYARIAH</option>
                    <option value="BTN">BTN</option>
                    <option value="CIMB NIAGA">CIMB NIAGA</option>
                    <option value="DANAMON">DANAMON</option>
                    <option value="MANDIRI">MANDIRI</option>
                    <option value="MEGA">MEGA</option>
                    <option value="MAYBANK">MAYBANK</option>
                    <option value="PAPUA">PAPUA</option>
                    <option value="None">None</option> -->
                  </select> 
                </div>
              </div> 
              <div class="form-group row">
                <label class="control-label col-md-2">Account Name</label>
                <div class="col-md-4">
                  <input class="errMsg form-control col-md-12" type="text" placeholder="Nama Pemilik Rekening" id="accountName">
                </div>
                <label class="control-label col-md-2">Account No</label>
                <div class="col-md-4">
                  <input class="errMsg form-control col-md-12" type="text" placeholder="Nomor Rekening" id="accountNo">
                </div>
              </div> 
            </div>
          </div>
        </div>
      </div> 
      <div class="modal-footer">
        <input type="button" class="btn btn-info btn-sm" data-dismiss="modal" value="Save" id="saveBankaccount">         
        <input type="button" class="btn btn-primary btn-sm" data-dismiss="modal" value="Close"> 
      </div>
    </div>
  </div> 
</div> 
<!-- End salaryModal UPDATE DATA -->
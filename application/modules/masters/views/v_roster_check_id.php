
<style type="text/css">
tr.selected {
  background-color: #B0BED9!important;
}
/*tr.even.selected {
  background-color: #B0BED9!important;
}*/
</style>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>

<div class="col-md-12" id="is_transaction">
  <div class="tile bg-info">
  <div class="tile">
    <div class="tile-body">
      <form class="form-horizontal">
        <div class="form-group row">

			<div class="col-xs-12 col-md-2">
				<label class="control-label" for="clientName">Client Name</label>
				<select class="form-control" id="clientName" name="clientName" required="">
					<option value="" disabled="" selected="">Pilih</option>			
					<option value="Pontil_Timika">Pontil Timika</option>			
					<option value="Redpath_Timika">Redpath Timika</option>			
					<option value="Pontil_Sumbawa">Pontil Sumbawa</option>			
					<option value="Trakindo_Sumbawa">Trakindo Sumbawa</option>			
					<option value="LCP_Sumbawa">LCP Sumbawa</option>			
					<option value="Pontil_Banyuwangi">Pontil Banyuwangi</option>			
				</select>
			</div>

			<div class="col-xs-12 col-md-2">
				<label class="control-label" for="monthPeriod">Month Period</label>
				<select class="form-control" id="monthPeriod" name="monthPeriod" required="">
					<option value="" disabled="" selected="">Pilih</option>
					<script type="text/javascript">
						var tMonth = 1;
						for (var i = tMonth; i <= 12; i++) 
						{
							if(i < 10)
							{
								document.write("<option value='0"+i+"'>0"+i+"</option>");							
							}
							else
							{
								document.write("<option value='"+i+"'>"+i+"</option>");								
							}
							
						}
					</script>
				</select>	
			</div>

			<div class="col-xs-12 col-md-2">
				<label class="control-label" for="yearPeriod">Year Period</label>					
				<select class="form-control" id="yearPeriod" name="yearPeriod" required="">
					<option value="" disabled="" selected="">Pilih</option>
					<script type="text/javascript">
						var dt = new Date();
						var startYear = dt.getFullYear();
						var endYear = startYear - 80;							
						for (var i = startYear; i >= endYear; i--) 
						{
							document.write("<option value='"+i+"'>"+i+"</option>");						
						}
					</script>
				</select>
			</div>

	    </div>

			<!-- START BUTTON VIEW -->
        	<button class="btn btn-warning btnProcessPanel" type="button" id="viewNonBio" name="viewNonBio"><i class="fa fa-refresh"></i> View Data</button>
        	<!-- END BUTTON VIEW -->

        	<br>
            <h3><code id="dataProses" class="backTransparent"><span></span></code></h3> 
        </div>
      </form>
    </div>
  </div>
</div>

<div class="col-md-12">
  	<div class="tile bg-info">
    <!-- <h3 class="tile-title">Barang Masuk</h3> -->
    	<div class="table-responsive">
						<table id="nonBiodataIdList" class="table table-hover table-bordered" class="table table-striped dataTable no-footer" cellspacing="0" width="100%" role="grid" style="width: 100%;">
				            <thead class="thead-dark">
				                <tr role="row">
				                	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Badge No</th>
				                	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Employee Name </th>
				                	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Client Name </th>
				                	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Year Period</th>
				                	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Month Period</th>
				                	<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Remarks</th>
				                					                	
				                </tr>
				            </thead>				            
				          
				        </table>	
				 </div>
	             <!-- END TABLE PENGALAMAN -->			    	
			</div>
		</div>
		<!-- END DATA PAYROLL -->
		<br>	
	</div>	
</div>


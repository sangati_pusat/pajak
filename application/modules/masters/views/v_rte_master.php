<!--START FORM DATA -->
<div class="col-md-12" >
  <div class="tile bg-info-bio">
    <!-- <h3 class="tile-title">Barang Masuk</h3> -->
    <div class="tile-body">
      <div id="saveMsg"></div>
      <form class="row">
      <!-- <form class="row is_header" action="<?php #echo base_url() ?>transactions/sumbawa/Timesheet/payrollProcess"> -->
        <div class="form-group col-sm-12 col-md-12">
          <!-- <input type="button" class="btn btn-dark" id="addBiodata" name="addBiodata" value="Add"> -->
          <input type="button" class="btn btn-dark" id="saveBiodata" name="saveBiodata" value="Save">
          <!-- <input type="button" class="btn btn-dark" id="input" name="input" value="Input" > -->
        </div>
      </form>
    </div>
  </div>
</div>
<!-- END FORM DATA -->

<div class="col-md-12">
  <div class="tile bg-info-bio">
    <!-- <h3 class="tile-title">Barang Masuk</h3> -->
    <div class="tile-body">
      <div id="biodata">
        <form class="form-horizontal my_header" method="POST">
          <div class="form-group row">
            <div class="control-label col-md-6">RTE Number   
              <input type="text" class="form-control" id="rteNumber" name="rteNumber" placeholder="RTE Number"  required="" autofocus="" >
            </div>
            <div class="control-label col-md-6">Department 
            <select class="form-control" id="dept" name="dept" required="">
                <option value="" disabled="" selected="">Select</option>
                <option value="Exploration">Exploration</option>
            </select>
            </div>
          </div>
  
          <div class="form-group row">
            <div class="control-label col-md-6">Date (Month) Required
              <input type="date" class="form-control" id="dateRequired" name="dateRequired" required="">
            </div>
            
            <div class="control-label col-md-6">Company Employing The Labour 
              <select class="form-control" id="company" name="company" required="">
                <option value="" disabled="" selected="">Select</option>
                <option value="Sangati Soerya Sejahtera">PT Sangati Soerya Sejahtera</option>
              </select>
            </div>
            
          </div>
  
          <div class="form-group row">
            <div class="control-label col-md-6">Labour Source              
               <select class="form-control" id="labourSource" name="labourSource" required="">
                <option value="" disabled="" selected="">Select</option>
                <option value="Ex FTC During Lapse Time Break">Ex FTC During Lapse Time Break</option>
                <option value="Ex Daily Worker">Ex Daily Worker</option>
                <option value="New Daily Worker">New Daily Worker</option>
              </select>
            </div>
                      
            
      </div>
      </div>          
      </form>


        <div class="tile-footer">
          
        </div>
      </div>     
    </div>
  </div>
</div>

<div class="col-md-14">
  <div class="tile bg-info-bio">
    <div class="tile-body">
        <div id="horizontalTab">
          <ul>
            <li><a href="#mp">Manpower Plan and Job Establishment</a></li>
           
          </ul>
       <!-- START DATA TABLE -->
          <div id='mp'>
            <input type="button" class="btn btn-primary btn-sm newManpowerPlan" id="addManpowerPlan" name="addManpowerPlan" data-toggle="modal" data-target="#manpowerPlanModal" value="Add Detail">
            <table class="table table-hover table-bordered" id="manpowerPlanTable">
              <thead class="thead-dark">
                <tr>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 139px;">Position</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 118px;">Rate Per Day</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 169px;">Total Labour</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 176px;">Start Date</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 176px;">End Date</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 176px;">Total Date</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 176px;">Work Location</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 176px;">Remarks</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 138px;">Delete</th>
                </tr>
              </thead>
            </table>
          </div>
        <!-- END DATA TABLE -->
    </div>
  </div>
</div>

<div class="modal fade" id="manpowerPlanModal" tabindex="-1" role="dialog" aria-labelledby="manpowerPlanModalLabel"> 
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <!-- Start modal-header -->
      <div class="modal-header">
        <h4 class="modal-title" id="manpowerPlanModalLabel">Classification</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
        <!-- End modal-header -->
        <!-- Start modal-body -->
      <div class="modal-body">
        <!-- Start Form PENDIDIKAN -->
        <form method="post" class="my_detail">
          <div class="form-group">
            <label class="control-label" for="position">Position</label>
            <select class="form-control" id="position" name="position" required="">
              <option value="" disabled="" selected="">Pilih</option>
            </select>
          </div> 

          <div class="form-group">
            <label class="control-label" for="ratePerDay">Rate Per Day</label>
            <input class="form-control" type="text" id="ratePerDay" name="ratePerDay" placeholder="Rate Per Day">
          </div> 

          <div class="form-group">
            <label class="control-label" for="totalLabour">Total Labour</label>
            <select class="errMsg form-control" id="totalLabour" name="totalLabour" required="">
              <option value="" disabled="" selected="">Select</option>
              <script type="text/javascript">
                var tLabour = 1;
                for (var i = tLabour; i <= 100; i++) 
                {
                  if(i < 10)
                  {
                    document.write("<option value='0"+i+"'>0"+i+"</option>");             
                  }
                  else
                  {
                    document.write("<option value='"+i+"'>"+i+"</option>");               
                  }
                }
              </script>
            </select>
          </div> 
                       
          <div class="form-group">
            <label class="control-label" for="startDate">Start Date</label>
            <input class="errMsg form-control datepicker" type="text" id="startDate" placeholder="yyyy-mm-dd" required ="">
          </div>

          <div class="form-group">
            <label class="control-label" for="endDate">End Date</label>
            <input class="errMsg form-control datepicker" type="text" id="endDate" name="endDate" placeholder="yyyy-mm-dd" required ="">
          </div>

          <div class="form-group">
            <label class="control-label" for="totalDate">Total Date</label>
            <input class="form-control datepicker" type="text" id="totalDate" name="totalDate" readonly="" >
          </div>

          <div class="form-group">
            <label class="control-label" for="workLocation">Work Location</label>
            <input class="form-control" type="text" id="workLocation" name="workLocation" placeholder="Work Location">
          </div>

          <div class="form-group">
            <label class="control-label" for="remarks">Remarks</label>
            <input class="form-control" type="text" id="remarks" name="remarks" placeholder="Remarks">
          </div>
                                                                                                                             
        </form> 
      </div> 
      <div class="modal-footer">
        <input type="button" class="btn btn-primary btn-sm" id="saveManpowerPlan" name="saveManpowerPlan" value="Save"> 
        <input type="button" class="btn btn-primary btn-sm" data-dismiss="modal" value="Exit"> 
      </div>
    </div>
  </div> 
</div> 


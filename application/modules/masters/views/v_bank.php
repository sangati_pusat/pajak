<style type="text/css">
tr.selected {
  background-color: #B0BED9!important;
}
</style>

<div class="col-md-12" id="is_transaction">
  <div class="tile bg-info">
    <div class="tile">
      <div class="tile-body">
        <form class="form-horizontal">
          <div class="form-group row">
            <label class="control-label col-md-2">Bank Code</label>
            <div class="col-md-4">
              <input type="text" class="form-control" id="bankCode" name="bankCode" placeholder="Bank Code" required="">
            </div>

            <label class="control-label col-md-2">Bank Name</label>
            <div class="col-md-4">
              <input class="form-control col-md-12" type="text" placeholder="Bank Name" id="bankName" name="bankName" required="">
            </div>
          </div>
          <hr>  
          <input type="button" class="btn btn-warning" id="saveBrd" name="saveBrd" value="Save">
          <br>
        </form>
      </div>
    </div>
  </div>

  <div class="tile bg-info">
      <div class="tile">
        <div class="tile-body">
          <div class="table-responsive">
            <table id="debtBrdTable" class="table table-hover table-bordered" class="table table-striped dataTable no-footer" cellspacing="0" width="100%" role="grid" style="width: 100%;">
              <thead class="thead-dark">           
                <tr role="row">
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Bank Id</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Bank Code</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Bank Name</th>
                  <!-- <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Action</th> -->
                  <!-- <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Client Name</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Date</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Month Begin</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Year Begin</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Loan Amount</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Number Of Month</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Amount Per Month</th> 
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Remarks</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1">Print</th> -->
                </tr>
              </thead>                    
            </table>
          </div>
        </div>
      </div>
  </div>
</div>
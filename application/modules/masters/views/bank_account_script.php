<script type="text/javascript">
    
    $(document).ready(function(){   
        // data_param: {};
        // let that = this;
        // $('select[name=ptName]').val(that.data_param.ptName).change(function(e){
        //     debugger
        //     that.data_param.ptName = e.target.value;
        //     ses.set('data_param', that.data_param);
        // });

        // Start Master Bank
        $.ajax({
            url : "<?php echo site_url('masters/Mst_bank/selectBank');?>",
            method : "POST",
            // data : {id: id},
            async : true,
            dataType : 'json',
            success: function(data){
                // debugger
                var html = '';
                var i;
                html += '<option disabled="" value="" data-code="" data-nama="" selected="">Pilih</option>';
                for(i=0; i<data.length; i++){
                    html += '<option value='+data[i].bank_name+' data-code='+data[i].bank_code+' data-id='+data[i].bank_id+'>'+data[i].bank_name+'</option>';
                }
                html += '<option value="None" data-code="None" data-nama="None">None</option>';
                $('#bankName').html(html);

            }
        });
        // End Master bank

        table = $('#table-salary').DataTable({
            serverSide : true,
            processing: true,
            ordering: false,
            ajax: {
                url: '<?php echo base_url() ?>'+'masters/Mst_salary/get_salaryall/',
                type: "GET", 
                dataSrc: "data",
                data: function(d) {    
                    d.client = $("#ptName").val();
                }
            },
            columnDefs: [
                {
                    "targets": 7, 
                    "className": "text-center",
                }
            ],
            columns : [
                {
                    "data"      : "salary_id"
                },
                {
                    "data"      : "nie"
                },
                {"data"   : "full_name"},
                // {"data"   : "payroll_group"},
                {"data"   : "company_name"},
                // {"data"   : "salary_level"},
                {"data"   : "basic_salary"},
                {"data"   : "bank_name"},
                {"data"   : "account_name"},
                {"data"   : "account_no"},
                {"data"   : "pic_edit_account"},
                {"data"   : "account_edit_time"},
                {
                    "data"      : null,
                    className: 'action',
                    render: function(idx, typ, dt) {
                        var button = '';
                        return button +
                            '<button class="btn btn-warning btn-sm" type="button" id="updateBankaccount" data-salary_id="'+dt.salary_id+'" data-full_name="'+dt.full_name+'" data-company_name="'+dt.company_name+'" data-payroll_group="'+dt.payroll_group+'" data-bank_name="'+dt.bank_name+'" data-account_name="'+dt.account_name+'" data-account_no="'+dt.account_no+'" data-toggle="modal" data-target="#levelSalaryModal">Update</button>'
                    }
                }
            ]
        });

        $('#searchClient').click(function(ev){ 
            ev.preventDefault();
            var ptName  = $('#ptName').val();
            table.ajax.reload();
            
        });

        // $('#table-salary').DataTable({
        //     serverSide : true,
        //     processing: true,
        //     ordering: false,
        //     ajax: {
        //         url: '<?php echo base_url() ?>'+'masters/Mst_salary/get_salaryall/'+ptName,
        //         type: "GET", 
        //         dataSrc: "data",
        //         data: function(d) {  }
        //     },
        //     columnDefs: [
        //         {
        //             "targets": 7, 
        //             "className": "text-center",
        //         }
        //     ],
        //     columns : [
        //         {
        //             "data"      : "salary_id"
        //         },
        //         {
        //             "data"      : "nie"
        //         },
        //         {"data"   : "full_name"},
        //         // {"data"   : "payroll_group"},
        //         {"data"   : "company_name"},
        //         // {"data"   : "salary_level"},
        //         {"data"   : "basic_salary"},
        //         {"data"   : "bank_name"},
        //         {"data"   : "account_name"},
        //         {"data"   : "account_no"},
        //         {
        //             "data"      : null,
        //             className: 'action',
        //             render: function(idx, typ, dt) {
        //                 var button = '';
        //                 return button +
        //                     '<button class="btn btn-warning btn-sm" type="button" id="updateBankaccount" data-salary_id="'+dt.salary_id+'" data-full_name="'+dt.full_name+'" data-company_name="'+dt.company_name+'" data-payroll_group="'+dt.payroll_group+'" data-bank_name="'+dt.bank_name+'" data-account_name="'+dt.account_name+'" data-account_no="'+dt.account_no+'" data-toggle="modal" data-target="#levelSalaryModal">Update</button>'
        //             }
        //         }
        //     ]
        // });


        $('#table-salary').on('click','#updateBankaccount', function (e) {
            var salaryId    = $(this).data('salary_id');
            var fullName    = $(this).data('full_name');
            var companyName = $(this).data('company_name');
            var payrollGroup= $(this).data('payroll_group');
            var bankName    = $(this).data('bank_name');
            var accountName = $(this).data('account_name');
            var accountNo   = $(this).data('account_no');

            $('#salaryId').val(salaryId);
            $('#clientName').val(companyName);
            $('#fullName').val(fullName);
            $('#payrollGroup').val(payrollGroup);
            $('#bankName').val(bankName);
            $('#accountName').val(accountName);
            $('#accountNo').val(accountNo);
        });

		/* START SAVE DATA */
        $("#saveBankaccount").on("click", function(){
        	$(".errMsg").css({"border": "2px solid #ced4da"});
            
            var salaryId    = $('#salaryId').val();
            var companyName = $('#clientName').val();
            var fullName    = $('#fullName').val();
            var payrollGroup= $('#payrollGroup').val();
            var bankName    = $('#bankName').val();
            var bankCode    = $('#bankName option:selected').attr('data-code');
            var bankId      = $('#bankName option:selected').attr('data-id');
            var accountName = $('#accountName').val();
            var accountNo   = $('#accountNo').val();
        	
			var isValid = true;
			if($("#accountName").val() == "")
			{
				isValid = false;
				$("#accountName").css({"border": "2px solid red"}); 	
				$("#accountName").focus();	
			}
            else if($("#accountNo").val() == ""){
                isValid = false;
                $("#accountNo").css({"border": "2px solid red"});     
                $("#accountNo").focus();  
            }

			if(isValid == false)
			{
				return false;
			}

			/* START LOAD DATA FROM DATABASE TO TABLE */
            var myUrl = "<?php echo base_url() ?>"+"masters/Mst_salary/updateAccount";
	       	$.ajax({
        		method : "POST",
        		url : myUrl, 
        		data : {
        			salaryId     : salaryId,
		        	companyName  : companyName,
		        	fullName     : fullName,
		        	payrollGroup : payrollGroup,
                    bankName     : bankName,
                    bankCode     : bankCode,
		        	bankId       : bankId,
                    accountName  : accountName,
                    accountNo    : accountNo
        		},
        		success : function(data){
					/* START ADD DATA TO TABLE */
    				alert("Data has been saved");
                    // location.reload();
        			// alert(data);
        		},
        		error : function(data){
        			isValid = false;
        			alert("Save failed");
        		}
        	});
	        /* END LOAD DATA FROM DATABASE TO TABLE */

			if(isValid == false)
			{
				return false;
			}


			/* START ADD DATA TO DATABASE */
			/* END ADD DATA TO DATABASE */
        });
        /* END SAVE DATA */

        function clearitem(){
            $(".errMsg").val('');
        }

        /* START VIEW DATA */
        $("#viewData").on("click", function(){
        	/* START LOAD DATA FROM DATABASE TO TABLE */
	       	$.ajax({
	    		method : "POST",
	    		 url : "<?php echo base_url() ?>"+"masters/Mst_salary/loadSalaryData", 
	    		data : {
	    			id :""
	    		},
	    		success : function(data){
					salaryTable.clear().draw();
					var dataSrc = JSON.parse(data);	
					salaryTable.rows.add(dataSrc).draw(false);
					// alert("Succeed");	
	    		},
	    		error : function(){
	    			alert("Failed Load Data");
	    		}
	    	});
	        /* END LOAD DATA FROM DATABASE TO TABLE */
        });
        /* END VIEW DATA */

        /* START SELECT SALARY DATA */
        var salaryId = "";
        var rowSalary = null;
        $('#salaryTable tbody').on( 'click', 'tr', function () {
            var rowSalary = salaryTable.row( this ).data();
            if ( $(this).hasClass('selected') ) 
            {
                $(this).removeClass('selected');
            }
            else 
            {
                salaryTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');                    
            }
            salaryId = rowSalary[0];
        }); 
        /* END SELECT SALARY DATA */


	});
</script>
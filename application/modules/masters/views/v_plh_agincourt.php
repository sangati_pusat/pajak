<style type="text/css">
tr.selected {
  background-color: #B0BED9!important;
}
</style>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>

<div class="col-md-12">
  <div class="tile bg-info-bio">
    <!-- <h3 class="tile-title">Barang Masuk</h3> -->
    <div class="tile-body">
      <div id="biodata">
        <form class="form-horizontal my_header" method="POST">

            <div class="form-group row">
              <div class="control-label col-md-6">RTE Number
              <input type="text" class="form-control" id="rteNumber" name="rteNumber" placeholder="RTE Number" readonly="">
              <input type="hidden" class="form-control" id="bioRecIdPlh" name="bioRecIdPlh" readonly="">
              </div>

              <div class="control-label col-md-6">Full Name  
                  <input type="text" class="form-control" id="fullName" name="fullName" placeholder="Full Name " data-toggle="modal" data-target="#salaryModal">
                </div>

            </div>

            <div class="form-group row">
                <div class="control-label col-md-6">ID Card Number
                  <code id="idCardNoDuplicate" class="errMsg"><span> Duplicate Id</span></code>      
                  <code id="idCardNoDigit" class="errMsg"><span> Required 16 Digit</span></code>     
                  <input type="text" class="form-control" id="idCardNo" name="idCardNo" placeholder="16 Digit KTP Number" min="16" max="16" readonly="">
                </div>     

                <div class="control-label col-md-6">Position 
                  <input type="text" class="form-control" id="position" name="position" placeholder="Position" readonly="">
                </div>
      
            </div>

            <div class="form-group row">
                <div class="control-label col-md-6">Badge Number
                  <input type="text" class="form-control" id="badgeNo" name="badgeNo" placeholder="Badge Number" required="">
                </div>

                <div class="control-label col-md-6">Client Name       
                  <select class="form-control" id="clientName" name="clientName" required="">
                    <option value="" disabled="" selected="">Select</option>
                    <option value="Agincourt Resources">Agincourt Resources</option>
                    
                  </select> 
                </div>
            
            </div>

            <div class="form-group row">
                <div class="control-label col-md-6">BPJS Kesehatan 
                  <input type="text" class="form-control" id="bpjskes" name="bpjskes" placeholder="No. BPJS Kesehatan " required="">
                </div>

                <div class="control-label col-md-6">BPJS Ketenagakerjaan       
                  <input type="text" class="form-control" id="bpjstk" name="bpjstk" placeholder="No. BPJS Ketenagakerjaan" required="">
                   
                </div>
            
            </div>

            <div class="form-group row">
                <div class="control-label col-md-6">Start Date  
                    <input type="text" class="form-control datepicker" id="startDate" name="startDate" placeholder="yyyy-mm-dd" required="">
                </div>

                <div class="control-label col-md-6">End Date  
                    <input type="text" class="form-control" id="endDate" name="endDate" placeholder="yyyy-mm-dd" required="">
                </div>
            
            </div>

            <div class="form-group row">
                <div class="control-label col-md-6">Work Day Amount/ Hours
                  <input class="errMsg form-control" type="text" id="workDayAmmount" name="workDayAmmount" disabled="">
                </div>  

                <div class="control-label col-md-6">PLH Salary </label>
                  <input class="errMsg form-control" id="plhSalary" name="plhSalary" placeholder="Input Salary" required ="">
                </div>               
            </div>

            <div class="form-group row">
                <div class="control-label col-md-6">Year Period</label>   
                  <select class="errMsg form-control" id="yearPeriod" name="yearPeriod" required="">
                    <option value="" disabled="" selected="">Select</option>
                    <script type="text/javascript">
                    var dt = new Date();
                    var currYear = dt.getFullYear();
                    var currMonth = dt.getMonth();
                            var currDay = dt.getDate();
                            var tmpDate = new Date(currYear + 1, currMonth, currDay);
                            var startYear = tmpDate.getFullYear();
                    var endYear = startYear - 20;             
                    for (var i = startYear; i >= endYear; i--) 
                    {
                    document.write("<option value='"+i+"'>"+i+"</option>");           
                    }
                    </script>
                  </select> 
            </div>
        
                <div class="control-label col-md-6">Month Period</label>
                  <select class="errMsg form-control" id="monthPeriod" name="monthPeriod" required="">
                    <option value="" disabled="" selected="">Select</option>
                    <script type="text/javascript">
                    var tMonth = 1;
                    for (var i = tMonth; i <= 12; i++) 
                    {
                    if(i < 10)
                    {
                    document.write("<option value='0"+i+"'>0"+i+"</option>");             
                    }
                      else
                      {
                      document.write("<option value='"+i+"'>"+i+"</option>");               
                      }
                    }
                    </script>
                  </select>
            </div> 

</div>

          <div class="tile-footer">
            <input type="button" class="btn btn-warning" id="addPlh" name="addPlh" data-toggle="modal" data-target="#contractModal" value="Add RTE">
            <input type="button" class="btn btn-warning" id="savePlh" name="savePlh" value="Save">
            <input type="button" class="btn btn-warning" id="editPlh" name="editPlh" value="Edit">
          </div>

   <br>
            <h3><code id="dataProses" class="backTransparent"><span></span></code></h3> 
        </div>
      </form>
    </div>
  </div>
</div>

       
<!-- START DATA TABLE -->

<div class="col-md-12">
  <div class="tile">
    <!-- <h3 class="tile-title">Barang Masuk</h3> -->
    <div class="table-responsive">

        <!-- <div class="tile-body"> -->
          <table id="plhTable" class="table table-hover table-bordered" class="table table-striped dataTable no-footer" cellspacing="0" width="100%" role="grid" style="width: 100%;">
            <thead class="thead-dark">           
              <tr role="row">
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Biodata Id</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">RTE Number</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">ID Card No</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Position</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">BPJS Kesehatan </th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">BPJS Ketenagakerjaan </th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Badge No</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Full Name</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Client Name</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Start Date</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">End Date</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Work Day Amount/ Hours</th>                
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">PLH Salary</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Year Period</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Month Period</th>
                
                
              </tr>
            </thead>                    
            <tbody>                      
            </tbody>
          </table>  
         
        </div>
       
    </div>
  </div>
</div>

<!-- END DATA TABLE -->

<!-- START DATA POP UP RTE NUMBER -->

    <div class="modal fade" id="contractModal" tabindex="-1" role="dialog" aria-labelledby="contractModalLabel"> 
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <!-- Start modal-header -->
          <div class="modal-header">
                      
              <h4 class="modal-title" id="contractModalLabel"></i> Master RTE </h4>
          </div>
          <!-- End modal-header -->

          <!-- Start modal-body -->
          <div class="modal-body">
          <!-- START DETAIL BIODATA -->
          <div id="detail" class="panel panel-body panelButtonDetail">
          <!-- END DETAIL BIODATA --> 

          <!-- START Table-Responsive --> 
              <div class="table-responsive">
                <table id="biodataTable" class="table table-hover table-bordered" cellspacing="0" width="100%" role="grid" style="width: 100%;">
                        <thead class="thead-dark">
                            <tr role="row">
                              <th>RTE ID</th>
                              <!-- <th>Eksternal ID</th>
                              <th>Full Name</th> -->
                        </thead>                    
                       
                </table>  
              </div>
          </div>
          </div> 

          <!-- Start modal-footer -->
          <div class="modal-footer">
              <input type="button" class="btn btn-primary btn-sm" id="chooseBiodata" name="chooseBiodata" data-dismiss="modal" value="Choose"> 
              <input type="button" class="btn btn-primary btn-sm" data-dismiss="modal" value="Close"> 
          </div>

        </div>
      </div> 
    </div> 
  </div>      
</div> 

<!-- END DATA POP UP RTE NUMBER -->


<!-- Start salaryModal UPDATE DATA  -->
<div class="modal fade" id="salaryModal" tabindex="-1" role="dialog" aria-labelledby="salaryModalLabel"> 
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
        <h4 class="modal-title" id="salaryModalLabel">Master Daily Worker</h4>
      </div>
      <div>
        <div id="detail" class="panel panel-body panelButtonDetail">
          <div class="table-responsive">
            <table id="namektpTable" class="table table-hover table-bordered">
              <thead class="thead-dark">
                <tr role="row">
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 139px;">Full Name</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 139px;">Id Card No</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 139px;">Position</th>
                  
                </tr>
              </thead>                    
              <tbody>       
              </tbody>
            </table>  
          </div>
        </div>
      </div> 
      <div class="modal-footer">
        <input type="button" class="btn btn-primary btn-sm" data-dismiss="modal" value="Close"> 
      </div>
    </div>
  </div> 
</div> 
<!-- End salaryModal UPDATE DATA -->

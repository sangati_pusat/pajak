<style type="text/css">
tr.selected {
  background-color: #B0BED9!important;
}
/*tr.even.selected {
  background-color: #B0BED9!important;
}*/
</style>

<div class="col-md-12" id="is_transaction">
  <div class="tile bg-info">
  <div class="tile">
    <div class="tile-body">
      <form class="form-horizontal">
        <div class="form-group row">

          <label class="control-label col-md-2">Internal ID</label>
          <div class="col-md-4">
            <input type="text" class="form-control" id="bioRecId" name="bioRecId" placeholder="Internal ID" required="" readonly="">
          </div>

          <label class="control-label col-md-2">Full Name</label>
          <div class="col-md-4">
            <input class="form-control col-md-12" type="text" placeholder="Full Name" id="fullName" required="" readonly="">
          </div>

        </div>

        <div class="form-group row">

          <label class="control-label col-md-2">Client Name</label>
          <div class="col-md-4">
            <input class="form-control col-md-12" type="text" placeholder="Client Name" id="clientName" disabled="">
          </div>

          <label class="control-label col-md-2">Date</label>
          <div class="col-md-4">            
          <input type="Date" class="form-control" id="loanDate" name="loanDate" value="<?php echo date("Y-m-d") ?>" readonly="">
          </div> 

      </div>

        <div class="form-group row">

          <label class="control-label col-md-2">Month Begin</label>  
          <div class="col-md-4">        
          <select class="form-control" id="monthBegin" name="monthBegin" required="">
            <option value="" disabled="" selected="">Select</option>
          <script type="text/javascript">
            var tMonth = 1;
            for (var i = tMonth; i <= 12; i++) 
            {
              if(i < 10)
              {
                document.write("<option value='0"+i+"'>0"+i+"</option>");             
              }
              else
              {
                document.write("<option value='"+i+"'>"+i+"</option>");               
              }
              
            }
          </script>
          </select> 
          </div> 

          <label class="control-label col-md-2">Year Begin</label>
          <div class="col-md-4">            
          <select class="form-control" id="yearBegin" name="yearBegin" required="">
            <option value="" disabled="" selected="">Select</option>
          <script type="text/javascript">
            var dt = new Date();
            var currYear = dt.getFullYear();
            var currMonth = dt.getMonth();
                        var currDay = dt.getDate();
                        var tmpDate = new Date(currYear + 1, currMonth, currDay);
                        var startYear = tmpDate.getFullYear();
            var endYear = startYear - 80;             
            for (var i = startYear; i >= endYear; i--) 
            {
              document.write("<option value='"+i+"'>"+i+"</option>");           
            }
          </script>
          </select>
          </div>   

        </div>

        <div class="form-group row">
          <label class="control-label col-md-2">Loan Amount</label>
          <div class="col-md-4">
            <input type="text" class="form-control" id="loanAmount" name="loanAmount" placeholder="Loan Amount" required="">
          </div>

          <label class="control-label col-md-2">Number Of Month</label>
          <div class="col-md-4">
            <select class="form-control" id="numberOfMonths" name="numberOfMonths" required="">
              <option value="" disabled="" selected="">Select</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>            
            </select>
          </div>

        </div>

        <div class="form-group row">
           <label class="control-label col-md-2">Amount Per Month</label>
          <div class="col-md-4">
            <input type="text" class="form-control" id="amountPerMonth" name="amountPerMonth" placeholder="Amount Per Months" required="" readonly="">
          </div>

          <label class="control-label col-md-2">Remarks</label>
          <div class="col-md-4">
            <input type="text" class="form-control" id="remarks" name="remarks" placeholder="Remarks" required="">
          </div>

        </div>

         <!-- START BUTTON ADD & SAVE -->
        <input type="button" class="btn btn-warning" id="addBiodata" name="addBiodata" data-toggle="modal" data-target="#contractModal" value="Add">
        <input type="button" class="btn btn-warning" id="saveBrd" name="saveBrd" value="Save">
        <!-- END BUTTON ADD & SAVE -->

      <br>
            <h3><code id="dataProses" class="backTransparent"><span></span></code></h3> 
        </div>
      </form>
    </div>
  </div>
</div>

<div class="col-md-12">
  <div class="tile bg-info">
    <!-- <h3 class="tile-title">Barang Masuk</h3> -->
    <div class="table-responsive">
       <!-- START DATA TABLE -->
        <!-- <div class="tile-body"> -->
          <table id="debtBrdTable" class="table table-hover table-bordered" class="table table-striped dataTable no-footer" cellspacing="0" width="100%" role="grid" style="width: 100%;">
            <thead class="thead-dark">           
              <tr role="row">
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Loan Id</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Biodata Id</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Name</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Client Name</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Date</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Month Begin</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Year Begin</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Loan Amount</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Number Of Month</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Amount Per Month</th> 
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Remarks</th>
                <th class="sorting_disabled" rowspan="1" colspan="1">Print</th>
              </tr>
            </thead>                    
        
          </table>
        </div>
        <!-- END DATA TABLE -->
    </div>
  </div>
</div>
  
    <!-- Start salaryModal UPDATE DATA  -->
    <div class="modal fade" id="contractModal" tabindex="-1" role="dialog" aria-labelledby="contractModalLabel"> 
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <!-- Start modal-header -->
          <div class="modal-header">
                        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
                        <h4 class="modal-title" id="contractModalLabel"> <i class="fa fa-money"></i> Debt Of Burden</h4>
                    </div>
                    <!-- End modal-header -->
                    <!-- Start modal-body -->
                    <div class="modal-body">
                      <!-- START DETAIL BIODATA -->
            <div id="detail" class="panel panel-body panelButtonDetail">
              <!-- START TABLE SALARY --> 
              <div class="table-responsive">
                <table id="biodataTable" class="table table-hover table-bordered" cellspacing="0" width="100%" role="grid" style="width: 100%;">
                        <thead class="thead-dark">
                            <tr role="row">
                              <th>Internal ID</th>
                              <th>Full Name</th>
                              <th>Client Name</th>
                        </thead>                    
                        <tbody>                             
                          <!-- <tr role="row" class="odd selected">
                                  <td>2017030001</td>
                                  <td>212</td>
                                  <td>Rahma</td>
                          </tr> -->
                        </tbody>
                    </table>  
              </div>
                    <!-- END TABLE SALARY -->
              </div>
            <!-- END DETAIL BIODATA -->   
                    </div> 
                  <!-- End modal-body -->
                  <!-- Start modal-footer -->
                  <div class="modal-footer">
                    <input type="button" class="btn btn-primary btn-sm" id="chooseBiodata" name="chooseBiodata" data-dismiss="modal" value="Choose"> 
                    <input type="button" class="btn btn-primary btn-sm" data-dismiss="modal" value="Close"> 
                  </div>
                  <!-- End modal-footer -->
        </div>
      </div> 
    </div> 
    <!-- End salaryModal UPDATE DATA -->


    

  </div>      
  <!-- END PANEL BIODATA -->
</div> <!-- /container 
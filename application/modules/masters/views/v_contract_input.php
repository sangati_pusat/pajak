<style type="text/css">
tr.selected {
  background-color: #B0BED9!important;
}
/*tr.even.selected {
  background-color: #B0BED9!important;
}*/
</style>

<div class="col-md-12" id="is_transaction">
  <div class="tile bg-info">
  <div class="tile">
    <div class="tile-body">
      <form class="form-horizontal">
        <div class="form-group row">

          <label class="control-label col-md-2">Internal ID</label>
          <div class="col-md-4"><input type="text" class="form-control" id="bioId" name="bioRecId" placeholder="Internal ID" required="" readonly="">
          </div>

          <label class="control-label col-md-2">Full Name</label>
          <div class="col-md-4">
           <input class="form-control col-md-12" type="text" placeholder="Full Name" id="empName" required="" readonly="">
          </div>

        </div>

        <div class="form-group row">

          <label class="control-label col-md-2">Contract NO. </label>
          <div class="col-md-4">
            <input type="text" class="errMsg form-control" id="contractNo" name="contractNo" placeholder="Contract NO." required="">
          </div>

          <label class="control-label col-md-2">Department </label> 
          <div class="col-md-4">          
          <select class="errMsg form-control" id="deptName" name="deptName" required="">
            <option value="" disabled="" selected="">Select</option>
            <option value="Alimak">Alimak</option>
            <option value="Admin">Admin</option>
            <option value="DMLZ">DMLZ</option>
            <option value="Engineering">Engineering</option>
            <option value="Electric">Electric</option>
            <option value="GBC Area I">GBC Area I</option>
            <option value="GBC Area II">GBC Area II</option>
            <option value="GBC Area III">GBC Area III</option>
            <option value="GBC Construction">GBC Construction</option>
            <option value="GBC I">GBC I</option>
            <option value="GBC II">GBC II</option>
            <option value="GBC III">GBC III</option>
            <option value="GBC # 1">GBC # 1</option>
            <option value="GBC # 2">GBC # 2</option>
            <option value="GBC # 3">GBC # 3</option>
            <option value="MCM">MCM</option>
            <option value="PGD">PGD</option>
            <option value="Raisebore">Raisebore</option>
            <option value="Safety">Safety</option>
            <option value="Safety and Training">Safety and Training</option>
            <option value="STCP">STCP</option>
            <option value="Eksplorasi">Eksplorasi</option>
            <option value="Process">Process</option>
            <option value="Mine Geology">Mine Geology</option>
            <option value="Processing Electric">Processing Electric</option>
            <option value="Maintenance">Maintenance</option>
            <option value="SCM">SCM</option>
            <option value="Coorporate Communications">Coorporate Communications</option>
            <option value="Govrel and Permitting">Govrel and Permitting</option>
            <option value="Processing Plan">Processing Plan</option>
            <option value="Project Development">Project Development</option>
            <option value="Facilities and Services">Facilities and Services</option>
            <option value="OHS and Training">OHS and Training</option>
            <option value="Operation">Operation</option>            
            <option value="Underground">Underground</option>
            <option value="Surface">Surface</option>
            <option value="Shop Maintenance">Shop Maintenance</option>
            <option value="Palm Shop">Palm Shop</option>
            <option value="Lowland">Lowland</option>
            <option value="Office 66">Office 66</option>
            <option value="All Area">All Area</option>
            <option value="MP 72">MP 72</option>
            <option value="Power Plant">Power Plant</option>
            <option value="Fixed Plant">Fixed Plant</option>
            <option value="Linesman">Linesman</option>
            <option value="Welder Plant">Welder Plant</option>
            <option value="Cat Rental">Cat Rental</option>
            <option value="Man Haul">Man Haul</option>
            <option value="Cat Rental">Cat Rental</option>
            <option value="Facility">Facility</option>
            <option value="Engineering">Engineering</option>
            <option value="News">News</option>
            <option value="Process Maintenance">Process Maintenance</option>
            <option value="Minning Project">Minning Project</option>
            <option value="Dry Season">Dry Season</option>
            <option value="Portside">Portside</option>
            <option value="Support Office">Support Office</option>
            <option value="Helper GA">Helper GA</option>
            <option value="IT">IT</option>
            <option value="Parts">Parts</option>
            <option value="Mews">Mews</option>
            <option value="Port & Logistics">Port & Logistics</option>
            <option value="Mrc">Mrc</option>
            <option value="Planning">Planning</option>
            <option value="Environt">Environt</option>
            <option value="Training">Training</option>
            <option value="Service Account">Service Account</option>
            <option value="Pontil">Pontil</option>
            <option value="Production">Production</option>
            <option value="Engineering & PPnC">Engineering & PPnC</option>
            <option value="GA & Facility">GA & Facility</option>
            <option value="OSHE">OSHE</option>
            <option value="Warehouse">Warehouse</option>
            <option value="None">None</option>
          </select> 
          </div>

      </div>

        <div class="form-group row">

          <label class="control-label col-md-2">Position</label>  
          <div class="col-md-4">        
          <input type="text" class="errMsg form-control" id="position" name="position" placeholder="Position" required="">
          </div> 

          <label class="control-label col-md-2">Start Date</label>
          <div class="col-md-4">            
          <input type="Date" class="errMsg form-control" id="contractStart" name="contractStart" required="">
          </div>   

        </div>

        <div class="form-group row">
          <label class="control-label col-md-2">End Date</label>
          <div class="col-md-4">
            <input type="Date" class="errMsg form-control" id="contractEnd" name="contractEnd" required="">
          </div>

          <label class="control-label col-md-2">Client Name</label>
          <div class="col-md-4">
            <select class="errMsg form-control" id="ptName" name="ptName" required="">
            </select>
            <!-- <select class="errMsg form-control" id="ptName" name="ptName" required="">
              <?php 
                $client = $this->session->userdata('hris_user_group'); 
                if($client == 'Pusat') 
                {
              ?>
                  <option value="" disabled="" selected="">Select</option>
                  <option value="Agincourt_Martabe">Agincourt Martabe</option>
                  <option value="AMNT_Sumbawa_Staff">AMNT Sumbawa Staff </option>
                  <option value="AMNT_Sumbawa">AMNT Sumbawa </option>
                  <option value="Asih_Eka_Abadi">Asih Eka Abadi</option>
                  <option value="Cipta_Kridatama">Cipta Kridatama</option>
                  <option value="Eksplorasi_Nusa_Jaya">Eksplorasi Nusa Jaya</option>
                  <option value="Indodrill">Indodrill</option>
                  <option value="Kufpec_Jakarta">Kufpec Jakarta</option>
                  <option value="MHE_Demag_Jakarta">MHE Demag Jakarta</option>
                  <option value="Machmahon_Sumbawa">Machmahon Sumbawa</option>
                  <option value="Petrosea_Timika">PETROSEA TIMIKA</option>
                  <option value="Pontil_Timika">Pontil Timika</option>
                  <option value="Pontil_Sumbawa">Pontil Sumbawa</option>
                  <option value="Pontil_Jakarta">Pontil Jakarta</option>
                  <option value="Pontil_Banyuwangi">Pontil Banyuwangi</option>
                  <option value="LCP_Sumbawa">Lamurung Cipta Persada</option>
                  <option value="Redpath_Timika">Redpath Timika</option>                
                  <option value="Trakindo_Sumbawa">Trakindo Sumbawa</option>
              <?php 
                }else{
              ?>
                  <option value="<?php echo $client ?>" selected=""><?php echo $client ?></option>
              <?php 
                }
              ?>
            </select> -->
          </div>     
        </div>
        <div class="form-group row">
          <label class="control-label col-md-2">Remarks</label>
            <div class="col-md-4">
              <code id="remarksErr" class="errMsg"></code>          
              <select class="form-control" id="remarks" name="remarks" required="">
                <option value="" disabled="" selected="">Select</option>
                <option value="Resignation">Resignation</option>
                <option value="Termination">Termination</option>
                <option value="Pass Away">Pass Away</option>
                <option value="Lay Off">Lay Off</option>
                <option value="Transfer To">Transfer To</option>
                <option value="Unfit Location">Unfit Location</option>
                <option value="Other">Other</option>
                <!-- <option value="None">None</option> -->
              </select> 
              
            </div>      

        </div>
        
          <input type="button" class="btn btn-warning" id="addBiodata" name="addBiodata" data-toggle="modal" data-target="#contractModal" value="Add">
          <input type="button" class="btn btn-warning" id="saveContract" name="saveContract" value="Save">
          <input type="button" class="btn btn-warning" id="editContract" name="editContract" value="Edit">

          <h3><code id="dataProses" class="backTransparent"><span></span></code></h3> 
        </div>
      </form>
    </div>
  </div>
</div>


<div class="col-md-12">
    <div class="tile bg-info">
    <!-- <h3 class="tile-title">Barang Masuk</h3> -->
      <div class="table-responsive">
          <table id="contractTable" class="table table-hover table-bordered" class="table table-striped dataTable no-footer" cellspacing="0" width="100%" role="grid" style="width: 100%;">
            <thead class="thead-dark">
              <tr role="row">
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Contract ID</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">External ID</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Full Name</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Client Name</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Department</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Position</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Contract No</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Start Date</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">End Date</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Remarks</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 80px;">Delete</th>
              </tr>
            </thead>                    
           
          </table>  
         
        </div>
    </div>
  </div>
</div>

  <!-- Start salaryModal UPDATE DATA  -->
    <div class="modal fade" id="contractModal" tabindex="-1" role="dialog" aria-labelledby="contractModalLabel"> 
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <!-- Start modal-header -->
          <div class="modal-header">
                      
              <h4 class="modal-title" id="contractModalLabel"></i> Master Contract </h4>
          </div>
          <!-- End modal-header -->

          <!-- Start modal-body -->
          <div class="modal-body">
          <!-- START DETAIL BIODATA -->
          <div id="detail" class="panel panel-body panelButtonDetail">
          <!-- END DETAIL BIODATA --> 

          <!-- START Table-Responsive --> 
              <div class="table-responsive">
                <table id="biodataTable" class="table table-hover table-bordered" cellspacing="0" width="100%" role="grid" style="width: 100%;">
                        <thead class="thead-dark">
                            <tr role="row">
                              <th>Internal ID</th>
                              <th>Eksternal ID</th>
                              <th>Full Name</th>
                        </thead>                    
                       
                </table>  
              </div>
          <!-- END TABLE SALARY -->

          </div>
          <!-- END DETAIL BIODATA -->   
          </div> 
          <!-- End modal-body -->

          <!-- Start modal-footer -->
          <div class="modal-footer">
              <input type="button" class="btn btn-primary btn-sm" id="chooseBiodata" name="chooseBiodata" data-dismiss="modal" value="Choose"> 
              <input type="button" class="btn btn-primary btn-sm" data-dismiss="modal" value="Close"> 
          </div>
          <!-- End modal-footer -->
        </div>
      </div> 
    </div> 
    <!-- End salaryModal UPDATE DATA -->

  </div>      
  <!-- END PANEL BIODATA -->
</div> <!-- /container 
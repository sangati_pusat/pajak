<style type="text/css">
tr.selected {
  background-color: #B0BED9!important;
}
</style>

<div class="col-md-12">
  <div class="tile">
    <div class="tile-body">
      <form class="form-horizontal">
        <div class="form-group row">
          <label class="control-label col-md-2">Internal Id</label>
          <div class="col-md-4">
            <input class="errMsg form-control col-md-12" type="text" placeholder="Id Internal" id="bioId" data-toggle="modal" data-target="#salaryModal">
          </div>
          <label class="control-label col-md-2">Name</label>
          <div class="col-md-4">
            <input class="errMsg form-control col-md-12" type="hidden" placeholder="Nama Karyawan" id="client" disabled="">
            <input class="errMsg form-control col-md-12" type="hidden" placeholder="Nama Karyawan" id="dept" disabled="">
            <input class="errMsg form-control col-md-12" type="text" placeholder="Nama Karyawan" id="empName" disabled="">
            <input class="errMsg form-control col-md-12" type="hidden" placeholder="Nama Karyawan" id="position" disabled="">
          </div>
        </div>
        <div class="form-group row">
          <label class="control-label col-md-2">Month Period</label>
          <div class="col-md-4">
            <select class="errMsg form-control" id="monthPeriod" name="monthPeriod" required="">
              <option value="" disabled="" selected="">Pilih</option>
              <script type="text/javascript">
                var tMonth = 1;
                for (var i = tMonth; i <= 12; i++) 
                {
                  if(i < 10)
                  {
                    document.write("<option value='0"+i+"'>0"+i+"</option>");             
                  }
                  else
                  {
                    document.write("<option value='"+i+"'>"+i+"</option>");               
                  }
                }
              </script>
            </select>
          </div>
          <label class="control-label col-md-2">Year Period</label>
          <div class="col-md-4">
            <select class="errMsg form-control" id="yearPeriod" name="yearPeriod" required="">
              <option value="" disabled="" selected="">Pilih</option>
              <script type="text/javascript">
                var dt = new Date();
                var currYear = dt.getFullYear();
                var currMonth = dt.getMonth();
                            var currDay = dt.getDate();
                            var tmpDate = new Date(currYear + 1, currMonth, currDay);
                            var startYear = tmpDate.getFullYear();
                var endYear = startYear - 20;             
                for (var i = startYear; i >= endYear; i--) 
                {
                  document.write("<option value='"+i+"'>"+i+"</option>");           
                }
              </script>
            </select> 
          </div>
        </div>
        <div class="form-group row">
          <label class="control-label col-md-2">Attendance</label>
          <div class="col-md-4">
            <select class="errMsg form-control" id="attendance" name="attendance" required="">
              <option value="" disabled="" selected="">Pilih</option>
              <script type="text/javascript">         
              var attendance = 1;
              for (var i = attendance; i <= 100; i++){
                if(i < 10){
                document.write("<option value='0"+i+"'>0"+i+"</option>");             
                }else{
                  document.write("<option value='"+i+"'>"+i+"</option>");               
                }
              }
              </script>
            </select> 
          </div>
          <label class="control-label col-md-2">Performance</label>
          <div class="col-md-4">
            <select class="errMsg form-control" id="performance" name="performance" required="">
              <option value="" disabled="" selected="">Pilih</option>
              <script type="text/javascript">         
              var performance = 1;
              for (var i = performance; i <= 100; i++){
                if(i < 10){
                document.write("<option value='0"+i+"'>0"+i+"</option>");             
                }else{
                  document.write("<option value='"+i+"'>"+i+"</option>");               
                }
              }
              </script>
            </select>
          </div>
        </div>
        <div class="form-group row">
          <label class="control-label col-md-2">Discilpine </label>
          <div class="col-md-4">
            <select class="errMsg form-control" id="dicipline" name="dicipline" required="">
              <option value="" disabled="" selected="">Pilih</option>
              <script type="text/javascript">         
              var dicipline = 1;
              for (var i = dicipline; i <= 100; i++){
                if(i < 10){
                document.write("<option value='0"+i+"'>0"+i+"</option>");             
                }else{
                  document.write("<option value='"+i+"'>"+i+"</option>");               
                }            
              }
              </script>
            </select> 
          </div>
          <label class="control-label col-md-2">Communication</label>
          <div class="col-md-4">
            <select class="errMsg form-control" id="communication" name="communication" required="">
              <option value="" disabled="" selected="">Pilih</option>
              <script type="text/javascript">         
              var communication = 1;
              for (var i = communication; i <= 100; i++){
                if(i < 10){
                document.write("<option value='0"+i+"'>0"+i+"</option>");             
                }else{
                  document.write("<option value='"+i+"'>"+i+"</option>");               
                }
              }
              </script>
            </select>
          </div>
        </div>
        <div class="form-group row">
          <label class="control-label col-md-2">Remarks</label>
          <div class="col-md-4">
            <textarea class="errMsg form-control" id="remarks" name="remarks" placeholder="Add information here" rows="3" required =""></textarea>
          </div>
          <label class="control-label col-md-2"></label>
          <div class="col-md-4"></div>
        </div>

        <div class="tile-footer">
          <!-- <input type="button" class="btn btn-primary" id="addBiodata" name="addBiodata" data-toggle="modal" data-target="#salaryModal" value="Add"> -->
          <input type="button" class="btn btn-primary" id="saveSalary" name="saveSalary" value="Save">
        </div>
      </form>
        <div class="tile-footer"></div>
        <div class="table-responsive">
          <table id="salaryTable" class="table table-hover table-bordered">
            <thead class="thead-dark">
              <tr role="row">
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 120px;">Biodata Id</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 120px;">Name</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 160px;">Client Name</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Dept</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Position</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Year</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Month</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Attendance</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 170px;">Performance</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 170px;">Discipline</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 170px;">Communication</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 170px;">Result Value</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 170px;">Result Grade</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 170px;">Remarks</th>
              </tr>
            </thead>                    
            <tbody>                      
            </tbody>
          </table>  
          <!-- <input type="button" class="btn btn-primary" id="viewData" name="viewData" value="Display Data">
          <input type="button" class="btn btn-primary" id="deleteData" name="deleteData" value="Delete Data"> -->
        </div>
    </div>
  </div>
</div>

    
<!-- Start salaryModal UPDATE DATA  -->
<div class="modal fade" id="salaryModal" tabindex="-1" role="dialog" aria-labelledby="salaryModalLabel"> 
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
        <h4 class="modal-title" id="salaryModalLabel">Salary</h4>
      </div>
      <div>
        <div id="detail" class="panel panel-body panelButtonDetail">
          <div class="table-responsive">
            <table id="biodataTable" class="table table-hover table-bordered">
              <thead class="thead-dark">
                <tr role="row">
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 139px;">Biodata Id</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 139px;">Client Name</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 139px;">Department</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 139px;">Position</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 150px;">Nama</th>
                </tr>
              </thead>                    
              <tbody>       
              </tbody>
            </table>  
          </div>
        </div>
      </div> 
      <div class="modal-footer">
        <input type="button" class="btn btn-primary btn-sm" data-dismiss="modal" value="Close"> 
      </div>
    </div>
  </div> 
</div> 
<!-- End salaryModal UPDATE DATA -->
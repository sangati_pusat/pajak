<?php
/*********************************************************************
 *  Created By       :  Generator Version 1.0.22                     *
 *  Created Date     : May 18, 2018                                  *
 *  Description      : All code generated by model generator         *
 *  Generator Author : Tommy Maurice(tommy_maurice@yahoo.com)        *
 *********************************************************************/
defined('BASEPATH') OR exit('No direct script access allowed');
class M_mst_contract extends CI_Model
{
      /* PRIVATE VARIABLES */
      private $myDb = 'db_recruitment';
      private $my_table = 'mst_contract';
      private $contractId;
      private $bioRecId;
      private $nie;
      private $contractNo;
      private $employeeName;
      private $clientName;
      private $dept;
      private $position;
      private $contractStart;
      private $contractEnd;
      private $contractCounter;
      // private $isClose;
      private $isActive;
      private $inputTime;
      private $picInput;
      private $editTime;
      private $picEdit;

      /* START OF CONSTRUCTOR */
      public function __construct()
      {
        parent::__construct();
        $this->contractId = "";
        $this->bioRecId = "";
        $this->nie = "";
        $this->contractNo = "";
        $this->employeeName = "";
        $this->clientName = "";
        $this->dept = "";
        $this->position = "";
        $this->contractStart = "0000-00-00 00:00:00";
        $this->contractEnd = "0000-00-00 00:00:00";
        $this->contractCounter = "";
        // $this->isClose = False;
        $this->isActive = False;
        $this->inputTime = "0000-00-00 00:00:00";
        $this->picInput = "";
        $this->editTime = "0000-00-00 00:00:00";
        $this->picEdit = "";
      }
      /* END OF CONSTRUCTOR */

      /* START OF SETTER AND GETTER */
      public function setContractId($aContractId)
      {
        $this->contractId = $this->db->escape_str($aContractId);
      }

      public function getContractId()
      {
        return $this->contractId;
      }

      public function setBioRecId($aBioRecId)
      {
        $this->bioRecId = $this->db->escape_str($aBioRecId);
      }

      public function getBioRecId()
      {
        return $this->bioRecId;
      }

      public function setNie($aNie)
      {
        $this->nie = $this->db->escape_str($aNie);
      }

      public function getNie()
      {
        return $this->nie;
      }

      public function setContractNo($aContractNo)
      {
        $this->contractNo = $this->db->escape_str($aContractNo);
      }

      public function getContractNo()
      {
        return $this->contractNo;
      }

      public function setEmployeeName($aEmployeeName)
      {
        $this->employeeName = $this->db->escape_str($aEmployeeName);
      }

      public function getEmployeeName()
      {
        return $this->employeeName;
      }

      public function setClientName($aClientName)
      {
        $this->clientName = $this->db->escape_str($aClientName);
      }

      public function getClientName()
      {
        return $this->clientName;
      }

      public function setDept($aDept)
      {
        $this->dept = $this->db->escape_str($aDept);
      }

      public function getDept()
      {
        return $this->dept;
      }

      public function setPosition($aPosition)
      {
        $this->position = $this->db->escape_str($aPosition);
      }

      public function getPosition()
      {
        return $this->position;
      }

      public function setContractStart($aContractStart)
      {
        $this->contractStart = $this->db->escape_str($aContractStart);
      }

      public function getContractStart()
      {
        return $this->contractStart;
      }

      public function setContractEnd($aContractEnd)
      {
        $this->contractEnd = $this->db->escape_str($aContractEnd);
      }

      public function getContractEnd()
      {
        return $this->contractEnd;
      }

      public function setContractCounter($aContractCounter)
      {
        $this->contractCounter = $this->db->escape_str($aContractCounter);
      }

      public function getContractCounter()
      {
        return $this->contractCounter;
      }

      // public function setIsClose($aIsClose)
      // {
      //   $this->isClose = $this->db->escape_str($aIsClose);
      // }

      // public function getIsClose()
      // {
      //   return $this->isClose;
      // }

      public function setIsActive($aIsActive)
      {
        $this->isActive = $this->db->escape_str($aIsActive);
      }

      public function getIsActive()
      {
        return $this->isActive;
      }

      public function setInputTime($aInputTime)
      {
        $this->inputTime = $this->db->escape_str($aInputTime);
      }

      public function getInputTime()
      {
        return $this->inputTime;
      }

      public function setPicInput($aPicInput)
      {
        $this->picInput = $this->db->escape_str($aPicInput);
      }

      public function getPicInput()
      {
        return $this->picInput;
      }

      public function setEditTime($aEditTime)
      {
        $this->editTime = $this->db->escape_str($aEditTime);
      }

      public function getEditTime()
      {
        return $this->editTime;
      }

      public function setPicEdit($aPicEdit)
      {
        $this->picEdit = $this->db->escape_str($aPicEdit);
      }

      public function getPicEdit()
      {
        return $this->picEdit;
      }

      /* END OF SETTER AND GETTER */

      /*---  START OF INSERT ---*/
      public function insert()
      {
         $st ='INSERT INTO '.$this->db->database.'.'.$this->my_table.' ';
         $st .='( ';
         $st .='contract_id,';
         $st .='bio_rec_id,';
         $st .='nie,';
         $st .='contract_no,';
         $st .='employee_name,';
         $st .='client_name,';
         $st .='dept,';
         $st .='position,';
         $st .='contract_start,';
         $st .='contract_end,';
         $st .='contract_counter,';
         // $st .='is_close,';
         $st .='is_active,';
         $st .='input_time,';
         $st .='pic_input,';
         $st .='edit_time,';
         $st .='pic_edit ';
         $st .=') ';
         $st .='VALUES ';
         $st .='( ';
         if( ($this->contractId == "") || ($this->contractId == null))
         {
           $st .='null'.',';
         }
         else
         {
           $st .='"'.$this->contractId.'",';
         }
         if( ($this->bioRecId == "") || ($this->bioRecId == null))
         {
           $st .='null'.',';
         }
         else
         {
           $st .='"'.$this->bioRecId.'",';
         }
         if( ($this->nie == "") || ($this->nie == null))
         {
           $st .='null'.',';
         }
         else
         {
           $st .='"'.$this->nie.'",';
         }
         if( ($this->contractNo == "") || ($this->contractNo == null))
         {
           $st .='null'.',';
         }
         else
         {
           $st .='"'.$this->contractNo.'",';
         }
         if( ($this->employeeName == "") || ($this->employeeName == null))
         {
           $st .='null'.',';
         }
         else
         {
           $st .='"'.$this->employeeName.'",';
         }
         if( ($this->clientName == "") || ($this->clientName == null))
         {
           $st .='null'.',';
         }
         else
         {
           $st .='"'.$this->clientName.'",';
         }
         if( ($this->dept == "") || ($this->dept == null))
         {
           $st .='null'.',';
         }
         else
         {
           $st .='"'.$this->dept.'",';
         }
         if( ($this->position == "") || ($this->position == null))
         {
           $st .='null'.',';
         }
         else
         {
           $st .='"'.$this->position.'",';
         }
         if( ($this->contractStart == "") || ($this->contractStart == null))
         {
           $st .='null'.',';
         }
         else
         {
           $st .='"'.$this->contractStart.'",';
         }
         if( ($this->contractEnd == "") || ($this->contractEnd == null))
         {
           $st .='null'.',';
         }
         else
         {
           $st .='"'.$this->contractEnd.'",';
         }
         if( ($this->contractCounter == "") || ($this->contractCounter == null))
         {
           $st .='0'.',';
         }
         else
         {
           $st .=$this->contractCounter.',';
         }
         // if( ($this->isClose == "") || ($this->isClose == null))
         // {
         //   $st .='0'.',';
         // }
         // else
         // {
         //   $st .=$this->isClose.',';
         // }
         if( ($this->isActive == "") || ($this->isActive == null))
         {
           $st .='0'.',';
         }
         else
         {
           $st .=$this->isActive.',';
         }
         if( ($this->inputTime == "") || ($this->inputTime == null))
         {
           $st .='null'.',';
         }
         else
         {
           $st .='"'.$this->inputTime.'",';
         }
         if( ($this->picInput == "") || ($this->picInput == null))
         {
           $st .='null'.',';
         }
         else
         {
           $st .='"'.$this->picInput.'",';
         }
         if( ($this->editTime == "") || ($this->editTime == null))
         {
           $st .='null'.',';
         }
         else
         {
           $st .='"'.$this->editTime.'",';
         }
         if( ($this->picEdit == "") || ($this->picEdit == null))
         {
           $st .='null';
         }
         else
         {
           $st .='"'.$this->picEdit.'" ';
         }
         $st .=') ';
         $this->db->query($st);
      }
      /*---  END OF INSERT ---*/

      /*---  START OF UPDATE ---*/
      public function update($id)
      {
         $st ='UPDATE '.$this->db->database.'.'.$this->my_table.' ';
         $st .='SET ';
         if( ($this->bioRecId == "") || ($this->bioRecId == null))
         {
           $st .=' bio_rec_id='.'null'.',';
         }
         else
         {
           $st .=' bio_rec_id='.'"'.$this->bioRecId.'",';
         }
         if( ($this->nie == "") || ($this->nie == null))
         {
           $st .=' nie='.'null'.',';
         }
         else
         {
           $st .=' nie='.'"'.$this->nie.'",';
         }
         if( ($this->contractNo == "") || ($this->contractNo == null))
         {
           $st .=' contract_no='.'null'.',';
         }
         else
         {
           $st .=' contract_no='.'"'.$this->contractNo.'",';
         }
         if( ($this->employeeName == "") || ($this->employeeName == null))
         {
           $st .=' employee_name='.'null'.',';
         }
         else
         {
           $st .=' employee_name='.'"'.$this->employeeName.'",';
         }
         if( ($this->clientName == "") || ($this->clientName == null))
         {
           $st .=' client_name='.'null'.',';
         }
         else
         {
           $st .=' client_name='.'"'.$this->clientName.'",';
         }
         if( ($this->dept == "") || ($this->dept == null))
         {
           $st .=' dept='.'null'.',';
         }
         else
         {
           $st .=' dept='.'"'.$this->dept.'",';
         }
         if( ($this->position == "") || ($this->position == null))
         {
           $st .=' position='.'null'.',';
         }
         else
         {
           $st .=' position='.'"'.$this->position.'",';
         }
         if( ($this->contractStart == "") || ($this->contractStart == null))
         {
           $st .=' contract_start='.'null'.',';
         }
         else
         {
           $st .=' contract_start='.'"'.$this->contractStart.'",';
         }
         if( ($this->contractEnd == "") || ($this->contractEnd == null))
         {
           $st .=' contract_end='.'null'.',';
         }
         else
         {
           $st .=' contract_end='.'"'.$this->contractEnd.'",';
         }
         if( ($this->contractCounter == "") || ($this->contractCounter == null))
         {
           $st .=' contract_counter='.'0'.',';
         }
         else
         {
           $st .=' contract_counter='.$this->contractCounter.',';
         }
         // if( ($this->isClose == "") || ($this->isClose == null))
         // {
         //   $st .=' is_close='.'0'.',';
         // }
         // else
         // {
         //   $st .=' is_close='.$this->isClose.',';
         // }
         if( ($this->isActive == "") || ($this->isActive == null))
         {
           $st .=' is_active='.'0'.',';
         }
         else
         {
           $st .=' is_active='.$this->isActive.',';
         }
         if( ($this->inputTime == "") || ($this->inputTime == null))
         {
           $st .=' input_time='.'null'.',';
         }
         else
         {
           $st .=' input_time='.'"'.$this->inputTime.'",';
         }
         if( ($this->picInput == "") || ($this->picInput == null))
         {
           $st .=' pic_input='.'null'.',';
         }
         else
         {
           $st .=' pic_input='.'"'.$this->picInput.'",';
         }
         if( ($this->editTime == "") || ($this->editTime == null))
         {
           $st .=' edit_time='.'null'.',';
         }
         else
         {
           $st .=' edit_time='.'"'.$this->editTime.'",';
         }
         if( ($this->picEdit == "") || ($this->picEdit == null))
         {
           $st .=' pic_edit='.'null';
         }
         else
         {
           $st .=' pic_edit='.'"'.$this->picEdit.'" ';
         }
         $st .=' WHERE ';
         $st .=' contract_id='.'"'.$this->db->escape_str($id).'"';
         $this->db->query($st);
      }
      /*---  END OF UPDATE ---*/

      /*---  START OF DELETE ---*/
      public function delete($id)
      {
         $st ='DELETE FROM '.$this->db->database.'.'.$this->my_table.' ';
         $st .='WHERE contract_id='.'"'.$this->db->escape_str($id).'"';
         $this->db->query($st);
      }
      /*---  END OF DELETE ---*/

      /*---  START OF LOAD ALL ---*/
      public function loadAll()
      {
         $this->db->select('*');
         $this->db->from('mst_contract');
         $this->db->order_by('contract_start', 'DESC');
         $query = $this->db->get()->result();
         return $query;
      }
      /*---  END OF LOAD ALL ---*/

      /*---  START OF LOAD BY ID ---*/
      public function loadById($id)
      {
         $this->db->select('*');
         $this->db->from('mst_contract');
         $this->db->where('contract_id', $this->db->escape_str($id));
         $this->db->order_by('contract_start', 'DESC');
         $row = $this->db->get()->row();
         $this->contractId = $row->contract_id;
         $this->bioRecId = $row->bio_rec_id;
         $this->nie = $row->nie;
         $this->contractNo = $row->contract_no;
         $this->employeeName = $row->employee_name;
         $this->clientName = $row->client_name;
         $this->dept = $row->dept;
         $this->position = $row->position;
         $this->contractStart = $row->contract_start;
         $this->contractEnd = $row->contract_end;
         $this->contractCounter = $row->contract_counter;
         // $this->isClose = $row->is_close;
         $this->isActive = $row->is_active;
         $this->inputTime = $row->input_time;
         $this->picInput = $row->pic_input;
         $this->editTime = $row->edit_time;
         $this->picEdit = $row->pic_edit;
      }
      /*---  END OF LOAD BY ID ---*/

      /*---  START OF LOAD BY LIMIT OFFSET ---*/
      public function loadByLimitOffset($limit, $offset)
      {
         $this->db->select('*');
         $this->db->from($this->db->database.'.'.$this->my_table);
         $this->db->limit($limit);
         $this->db->offset($offset);
         $this->db->order_by('contract_start', 'DESC');
         $query = $this->db->get()->result();
         return $query;
      }
      /*---  END OF LOAD BY LIMIT OFFSET ---*/

      /*---  START OF GET TOTAL DATA ---*/
      public function getTotalData()
      {
         $query = $this->db->query('SELECT contract_id FROM '.$this->db->database.'.'.$this->my_table);
         return $query->num_rows();
      }
      /*---  END OF GET TOTAL DATA ---*/

      /* START OF RESET VALUES */
      public function resetValues()
      {
        $this->contractId = "";
        $this->bioRecId = "";
        $this->nie = "";
        $this->contractNo = "";
        $this->employeeName = "";
        $this->clientName = "";
        $this->dept = "";
        $this->position = "";
        $this->contractStart = "0000-00-00 00:00:00";
        $this->contractEnd = "0000-00-00 00:00:00";
        $this->contractCounter = "";
        // $this->isClose = False;
        $this->isActive = False;
        $this->inputTime = "0000-00-00 00:00:00";
        $this->picInput = "";
        $this->editTime = "0000-00-00 00:00:00";
        $this->picEdit = "";
      }
      /* END OF RESET VALUES */

      /* START ADD BY MAURICE */
      public function loadData()
      {
        $this->db->select('*');
        $this->db->from('mst_contract');
        $this->db->order_by('contract_start', 'DESC');
        $query = $this->db->get()->result_array();
        return $query;
      }

      public function loadContractExpiredByRemains($clientName, $day1, $day2)
      {       
        $strSql  = 'SELECT ';
        $strSql .= '  client_name, nie, employee_name, contract_end, DATEDIFF(contract_end, date(now())) remains_day ';
        $strSql .= 'FROM ';
        $strSql .= '  mst_contract ';
        $strSql .= 'WHERE ';
        $strSql .= '  client_name = "'.$clientName.'" ';
        $strSql .= 'AND ';
        $strSql .= '  (DATEDIFF(contract_end, date(now())) = '.$day1.' OR DATEDIFF(contract_end, date(now())) = '.$day2.') ';
        $strSql .= 'AND ';
        $strSql .= '  DATEDIFF(contract_end, DATE(NOW())) >= 0 ';
        $strSql .= 'ORDER BY  ';
        $strSql .= '  employee_name';
        $query = $this->db->query($strSql)->result_array();
        return $query;  
      }

      public function GenerateNumber()
      {
        $query = $this->db->query('SELECT DATE_FORMAT(NOW(),"%Y%m") ym, MAX(contract_id) contract_id FROM '.$this->db->database.'.'.$this->my_table);
        $row = $query->row();
        $newNo = 0;
        $currNo = 0;
        $currYearMonth = $row->ym;
        $currNo = $row->contract_id;
        if(! isset($currNo))
        {
          $newNo = $currYearMonth*10000+1;  
        }
        else
        {
          // $currYearMonth = date("Ym");
          $strCurrNo = $currNo;
          settype($strCurrNo, "string");
          $strCurrYearMonth = $currYearMonth;
          settype($strCurrYearMonth, "string");

          if (substr($strCurrNo, 0, 6) == $strCurrYearMonth) 
          {
              $newNo = $currNo + 1;
              // return "Ada";
          }
          else
          {
              $newNo = $currYearMonth*10000+1;
              // return "Tidak";  
          } 
        }

        return $newNo;
      }

      public function getLastCounterByBiodataId($biodataId)
      {
        /* GET  */
        $sql  = 'SELECT count(*)+1 myCounter '; 
        $sql .= 'FROM mst_contract '; 
        $sql .= 'WHERE  bio_rec_id = "'.$this->db->escape_str($biodataId).'"'; 
        $query = $this->db->query($sql);
        $row = $query->row_array();
        $myCounter = $row['myCounter'];
        return $myCounter;
      }

      public function updateOldContractByBioId($biodataId, $currentCounter)
      {      
        $sql  = 'UPDATE mst_contract '; 
        $sql .= 'SET is_active = 0 '; 
        $sql .= 'WHERE  bio_rec_id = "'.$this->db->escape_str($biodataId).'"'; 
        $sql .= 'AND contract_counter < "'.$currentCounter.'"'; 
        $query = $this->db->query($sql);
      }

      public function loadContractByClientName($clientName)
      {
         $sql  = 'SELECT mc.contract_id, ms.nie, mb.full_name, mc.client_name, mc.dept, mc.position, mc.contract_no, mb.date_of_hire,';
         $sql .= 'CASE mb.local_foreign WHEN "L" THEN "Local" WHEN "N" THEN "Non Local" END local_foreign,';
         $sql .= 'mc.contract_start,mc.contract_end,mc.is_active ';
         $sql .= 'FROM mst_contract mc, mst_bio_rec mb LEFT JOIN mst_salary ms ON mb.bio_rec_id = ms.bio_rec_id ';
         $sql .= 'WHERE mb.bio_rec_id = mc.bio_rec_id AND mc.is_active = 1 ';
         if($clientName != 'Pusat') 
         {
            $sql .= 'AND mc.client_name = "'.$clientName.'"';
         }
         $query = $this->db->query($sql)->result_array();
         return $query;
      }

      public function contractChart($clientName)
      {
         $sql  = 'SELECT @s:=@s+1 id, mc.dept, COUNT(*) data_count ';
         $sql .= ' FROM (SELECT @s:= 0) AS s, mst_contract mc, mst_bio_rec mb LEFT JOIN mst_salary ms ON mb.bio_rec_id = ms.bio_rec_id ';
         $sql .= ' WHERE mb.bio_rec_id = mc.bio_rec_id AND mc.is_active = 1 ';
         
         if($clientName != 'Pusat')
         {
            $sql .= ' AND client_name = "'.$clientName.'"';
         }
         
         $sql .= ' GROUP BY mc.dept';
         return $this->db->query($sql)->result_array();
          

        /*$this->db->select('*');
        $query = $this->db->get('company_performance');
        return $query->result_array();*/
      }

      public function myContractChart($clientName)
      {
         $sql  = 'SELECT COUNT(*) AS y, `dept` AS label ';
         $sql .= ' FROM `mst_contract`';
         $sql .= ' WHERE is_active = 1 ';
         
         if($clientName != 'Pusat')
         {
            $sql .= ' AND client_name = "'.$clientName.'"';
         }
         
         $sql .= ' GROUP BY `dept`';
         $sql .= ' ORDER BY `dept`';
         return $this->db->query($sql)->result_array();        
        /*$this->db->select('*');
        $query = $this->db->get('company_performance');
        return $query->result_array();*/
      }
      /* END ADD BY MAURICE */

}

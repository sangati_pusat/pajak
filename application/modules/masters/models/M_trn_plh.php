<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class M_trn_plh extends CI_Model
{
     /* START PRIVATE VARIABLES */
     private $myDb = 'db_recruitment';
     private $myTable = 'daily_worker';
     private $bioRecIdPlh;
     private $badgeNo;
     private $rteNumber;
     private $clientName;
     private $idCardNo;
     private $position;
     private $bpjskes;
     private $bpjstk;
     private $fullName;
     private $workDayAmmount;
     private $startDate;
     private $endDate;
     private $monthPeriod;
     private $yearPeriod;
     private $plhSalary;
     private $isActive;
     private $isBlacklist;
     private $picInput;
     private $inputTime;
     private $picEdit;
     private $editTime;
     /* END PRIVATE VARIABLES */
     /* START CONSTRUCTOR */
     public function __construct()
     {
          // $this->test = $this->load->database('master',true);
     	parent::__construct();
          $this->bioRecIdPlh = 0;
          $this->badgeNo = '';
          $this->rteNumber = '';
          $this->clientName = '';
          $this->idCardNo = '';
          $this->position = '';
          $this->bpjskes = '';
          $this->bpjstk = '';
          $this->fullName = '';
          $this->workDayAmmount = 0;
          $this->startDate = '';
          $this->endDate = '';
          $this->monthPeriod = '';
          $this->yearPeriod = 0;
          $this->plhSalary = 0;
          $this->isActive = 0;
          $this->isBlacklist = 0;
          $this->picInput = '';
          $this->inputTime = '0000-00-00 00:00:00';
          $this->picEdit = '';
          $this->editTime = '0000-00-00 00:00:00';
     }
     /* END CONSTRUCTOR */
     
     /* START GENERATE SETTER AND GETTER */
     public function setBioRecIdPlh($aBioRecIdPlh)
     {
     	$this->bioRecIdPlh = $this->db->escape_str($aBioRecIdPlh);
     }
     public function getBioRecIdPlh()
     {
     	return $this->bioRecIdPlh;
     }
     public function setBadgeNo($aBadgeNo)
     {
     	$this->badgeNo = $this->db->escape_str($aBadgeNo);
     }
     public function getBadgeNo()
     {
     	return $this->badgeNo;
     }
     public function setRteNumber($aRteNumber)
     {
          $this->rteNumber = $this->db->escape_str($aRteNumber);
     }
     public function getRteNumber()
     {
          return $this->rteNumber;
     }
     public function setClientName($aClientName)
     {
     	$this->clientName = $this->db->escape_str($aClientName);
     }
     public function getClientName()
     {
     	return $this->clientName;
     }
     public function setIdCardNo($aIdCardNo)
     {
          $this->idCardNo = $this->db->escape_str($aIdCardNo);
     }
     public function getIdCardNo()
     {
          return $this->idCardNo;
     }
     public function setPosition($aPosition)
     {
          $this->position = $this->db->escape_str($aPosition);
     }
     public function getPosition()
     {
          return $this->position;
     }
     public function setBpjskes($aBpjskes)
     {
          $this->bpjskes = $this->db->escape_str($aBpjskes);
     }
     public function getBpjskes()
     {
          return $this->bpjskes;
     }
     public function setBpjstk($aBpjstk)
     {
          $this->bpjstk = $this->db->escape_str($aBpjstk);
     }
     public function getBpjstk()
     {
          return $this->bpjstk;
     }
     public function setFullName($aFullName)
     {
     	$this->fullName = $this->db->escape_str($aFullName);
     }
     public function getFullName()
     {
     	return $this->fullName;
     }
     public function setWorkDayAmmount($aWorkDayAmmount)
     {
     	$this->workDayAmmount = $this->db->escape_str($aWorkDayAmmount);
     }
     public function getWorkDayAmmount()
     {
     	return $this->workDayAmmount;
     }
     public function setStartDate($aStartDate)
     {
          $this->startDate = $this->db->escape_str($aStartDate);
     }
     public function getStartDate()
     {
          return $this->startDate;
     }
     public function setEndDate($aEndDate)
     {
          $this->endDate = $this->db->escape_str($aEndDate);
     }
     public function getEndDate()
     {
          return $this->endDate;
     }
     public function setMonthPeriod($aMonthPeriod)
     {
     	$this->monthPeriod = $this->db->escape_str($aMonthPeriod);
     }
     public function getMonthPeriod()
     {
     	return $this->monthPeriod;
     }
     public function setYearPeriod($aYearPeriod)
     {
     	$this->yearPeriod = $this->db->escape_str($aYearPeriod);
     }
     public function getYearPeriod()
     {
     	return $this->yearPeriod;
     }
     public function setPlhSalary($aPlhSalary)
     {
     	$this->plhSalary = $this->db->escape_str($aPlhSalary);
     }
     public function getPlhSalary()
     {
     	return $this->plhSalary;
     }
     public function setIsActive($aIsActive)
     {
     	$this->isActive = $this->db->escape_str($aIsActive);
     }
     public function getIsActive()
     {
     	return $this->isActive;
     }
     public function setIsBlacklist($aIsBlacklist)
     {
          $this->isBlacklist = $this->db->escape_str($aIsBlacklist);
     }
     public function getIsBlacklist()
     {
          return $this->isBlacklist;
     }
     public function setPicInput($aPicInput)
     {
     	$this->picInput = $this->db->escape_str($aPicInput);
     }
     public function getPicInput()
     {
     	return $this->picInput;
     }
     public function setInputTime($aInputTime)
     {
     	$this->inputTime = $this->db->escape_str($aInputTime);
     }
     public function getInputTime()
     {
     	return $this->inputTime;
     }
     public function setPicEdit($aPicEdit)
     {
     	$this->picEdit = $this->db->escape_str($aPicEdit);
     }
     public function getPicEdit()
     {
     	return $this->picEdit;
     }
     public function setEditTime($aEditTime)
     {
     	$this->editTime = $this->db->escape_str($aEditTime);
     }
     public function getEditTime()
     {
     	return $this->editTime;
     }
     /* END GENERATE SETTER AND GETTER */
     /* START INSERT */
     public function insert()
     {
     	if($this->bioRecIdPlh =='' || $this->bioRecIdPlh == NULL )
     	{
          	$this->bioRecIdPlh = 0;
     	}
     	if($this->badgeNo =='' || $this->badgeNo == NULL )
     	{
          	$this->badgeNo = '';
     	}
          if($this->rteNumber =='' || $this->rteNumber == NULL )
          {
               $this->rteNumber = '';
          }
     	if($this->clientName =='' || $this->clientName == NULL )
     	{
          	$this->clientName = '';
     	}
          if($this->idCardNo =='' || $this->idCardNo == NULL )
          {
               $this->idCardNo = '';
          }
          if($this->position =='' || $this->position == NULL )
          {
               $this->position = '';
          }
          if($this->bpjskes =='' || $this->bpjskes == NULL )
          {
               $this->bpjskes = '';
          }
          if($this->bpjstk =='' || $this->bpjstk == NULL )
          {
               $this->bpjstk = '';
          }
     	if($this->fullName =='' || $this->fullName == NULL )
     	{
          	$this->fullName = '';
     	}
          if($this->startDate =='' || $this->startDate == NULL )
          {
               $this->startDate = '';
          }
          if($this->endDate =='' || $this->endDate == NULL )
          {
               $this->endDate = '';
          }
     	if($this->monthPeriod =='' || $this->monthPeriod == NULL )
     	{
          	$this->monthPeriod = '';
     	}
     	if($this->yearPeriod =='' || $this->yearPeriod == NULL )
     	{
          	$this->yearPeriod = 0;
     	}
     	if($this->workDayAmmount =='' || $this->workDayAmmount == NULL )
     	{
          	$this->workDayAmmount = 0;
     	}
     	if($this->plhSalary =='' || $this->plhSalary == NULL )
     	{
          	$this->plhSalary = 0;
     	}
     	if($this->isActive =='' || $this->isActive == NULL )
     	{
          	$this->isActive = 0;
     	}
          if($this->isBlacklist =='' || $this->isBlacklist == NULL )
          {
               $this->isBlacklist = 0;
          }
     	if($this->picInput =='' || $this->picInput == NULL )
     	{
          	$this->picInput = '';
     	}
     	if($this->inputTime =='' || $this->inputTime == NULL )
     	{
          	$this->inputTime = '0000-00-00 00:00:00';
     	}
     	if($this->picEdit =='' || $this->picEdit == NULL )
     	{
          	$this->picEdit = '';
     	}
     	if($this->editTime =='' || $this->editTime == NULL )
     	{
          	$this->editTime = '0000-00-00 00:00:00';
     	}
     	
     	$stQuery  = 'INSERT INTO '.$this->myDb.'.'.$this->myTable.' '; 
     	$stQuery .= '( '; 
     	$stQuery .=   'bio_rec_id_plh,'; 
     	$stQuery .=   'badge_no,'; 
          $stQuery .=   'rte_number,'; 
     	$stQuery .=   'client_name,'; 
          $stQuery .=   'id_card_no,'; 
          $stQuery .=   'position,'; 
          $stQuery .=   'bpjskes,'; 
          $stQuery .=   'bpjstk,'; 
     	$stQuery .=   'full_name,'; 
     	$stQuery .=   'start_date,'; 
          $stQuery .=   'end_date,'; 
          $stQuery .=   'month_period,'; 
     	$stQuery .=   'year_period,'; 
     	$stQuery .=   'work_day_ammount,'; 
     	$stQuery .=   'plh_salary,'; 
     	$stQuery .=   'is_active,'; 
          $stQuery .=   'is_blacklist,'; 
     	$stQuery .=   'pic_input,'; 
     	$stQuery .=   'input_time,'; 
     	$stQuery .=   'pic_edit,'; 
     	$stQuery .=   'edit_time'; 
     	$stQuery .= ') '; 
     	$stQuery .= 'VALUES '; 
     	$stQuery .= '( '; 
     	// $stQuery .=   $this->db->escape_str($this->bioRecIdPlh).','; 
     	$stQuery .=   '"'.$this->db->escape_str($this->bioRecIdPlh).'",'; 
          $stQuery .=   '"'.$this->db->escape_str($this->badgeNo).'",'; 
          $stQuery .=   '"'.$this->db->escape_str($this->rteNumber).'",'; 
     	$stQuery .=   '"'.$this->db->escape_str($this->clientName).'",'; 
          $stQuery .=   '"'.$this->db->escape_str($this->idCardNo).'",'; 
          $stQuery .=   '"'.$this->db->escape_str($this->position).'",'; 
          $stQuery .=   '"'.$this->db->escape_str($this->bpjskes).'",'; 
          $stQuery .=   '"'.$this->db->escape_str($this->bpjstk).'",'; 
     	$stQuery .=   '"'.$this->db->escape_str($this->fullName).'",'; 
     	$stQuery .=   '"'.$this->db->escape_str($this->startDate).'",'; 
          $stQuery .=   '"'.$this->db->escape_str($this->endDate).'",'; 
          $stQuery .=   '"'.$this->db->escape_str($this->monthPeriod).'",'; 
     	$stQuery .=   $this->db->escape_str($this->yearPeriod).','; 
     	$stQuery .=   '"'.$this->db->escape_str($this->workDayAmmount).'",';
     	$stQuery .=   $this->db->escape_str($this->plhSalary).','; 
     	$stQuery .=   $this->db->escape_str($this->isActive).','; 
          $stQuery .=   $this->db->escape_str($this->isBlacklist).','; 
     	$stQuery .=   '"'.$this->db->escape_str($this->picInput).'",'; 
     	$stQuery .=   '"'.$this->db->escape_str($this->inputTime).'",'; 
     	$stQuery .=   '"'.$this->db->escape_str($this->picEdit).'",'; 
     	$stQuery .=   '"'.$this->db->escape_str($this->editTime).'"'; 
     	$stQuery .= '); '; 
          // test($stQuery,1);
     	$this->db->query($stQuery); 
     }
     /* END INSERT */
     
     /* START UPDATE */
     public function update($id)
     {
     	if($this->bioRecIdPlh =='' || $this->bioRecIdPlh == NULL )
     	{
          	$this->bioRecIdPlh = 0;
     	}
     	if($this->badgeNo =='' || $this->badgeNo == NULL )
     	{
          	$this->badgeNo = '';
     	}
          if($this->rteNumber =='' || $this->rteNumber == NULL )
          {
               $this->rteNumber = '';
          }
     	if($this->clientName =='' || $this->clientName == NULL )
     	{
          	$this->clientName = '';
     	}
          if($this->idCardNo =='' || $this->idCardNo == NULL )
          {
               $this->idCardNo = '';
          }
          if($this->position =='' || $this->position == NULL )
          {
               $this->position = '';
          }
          if($this->bpjskes =='' || $this->bpjskes == NULL )
          {
               $this->bpjskes = '';
          }
          if($this->bpjstk =='' || $this->bpjstk == NULL )
          {
               $this->bpjstk = '';
          }
     	if($this->fullName =='' || $this->fullName == NULL )
     	{
          	$this->fullName = '';
     	}
     	if($this->workDayAmmount =='' || $this->workDayAmmount == NULL )
     	{
          	$this->workDayAmmount = 0;
     	}
          if($this->startDate =='' || $this->startDate == NULL )
          {
               $this->startDate = '';
          }
          if($this->endDate =='' || $this->endDate == NULL )
          {
               $this->endDate = '';
          }
     	if($this->monthPeriod =='' || $this->monthPeriod == NULL )
     	{
          	$this->monthPeriod = '';
     	}
     	if($this->yearPeriod =='' || $this->yearPeriod == NULL )
     	{
          	$this->yearPeriod = 0;
     	}
     	if($this->plhSalary =='' || $this->plhSalary == NULL )
     	{
          	$this->plhSalary = 0;
     	}
     	if($this->isActive =='' || $this->isActive == NULL )
     	{
          	$this->isActive = 0;
     	}
          if($this->isBlacklist =='' || $this->isBlacklist == NULL )
          {
               $this->isBlacklist = 0;
          }
     	if($this->picInput =='' || $this->picInput == NULL )
     	{
          	$this->picInput = '';
     	}
     	if($this->inputTime =='' || $this->inputTime == NULL )
     	{
          	$this->inputTime = '0000-00-00 00:00:00';
     	}
     	if($this->picEdit =='' || $this->picEdit == NULL )
     	{
          	$this->picEdit = '';
     	}
     	if($this->editTime =='' || $this->editTime == NULL )
     	{
          	$this->editTime = '0000-00-00 00:00:00';
     	}
     	
     	$stQuery  = 'UPDATE '.$this->myDb.'.'.$this->myTable.' '; 
     	$stQuery .= 'SET '; 
     	$stQuery .=   'bio_rec_id_plh ='.$this->db->escape_str($this->bioRecIdPlh).','; 
     	$stQuery .=   'client_name ="'.$this->db->escape_str($this->clientName).'",'; 
     	$stQuery .=   'full_name ="'.$this->db->escape_str($this->fullName).'",'; 
          $stQuery .=   'id_card_no ="'.$this->db->escape_str($this->idCardNo).'",'; 
          $stQuery .=   'position ="'.$this->db->escape_str($this->position).'",'; 
          $stQuery .=   'bpjskes ="'.$this->db->escape_str($this->bpjskes).'",'; 
          $stQuery .=   'bpjstk ="'.$this->db->escape_str($this->bpjstk).'",'; 
     	$stQuery .=   'badge_no ="'.$this->db->escape_str($this->badgeNo).'",'; 
          $stQuery .=   'rte_number ="'.$this->db->escape_str($this->rteNumber).'",'; 
     	$stQuery .=   'start_date ="'.$this->db->escape_str($this->startDate).'",'; 
          $stQuery .=   'end_date ="'.$this->db->escape_str($this->endDate).'",'; 
          $stQuery .=   'month_period ="'.$this->db->escape_str($this->monthPeriod).'",'; 
     	$stQuery .=   'year_period ='.$this->db->escape_str($this->yearPeriod).','; 
     	$stQuery .=   'work_day_ammount ='.$this->db->escape_str($this->workDayAmmount).','; 
     	$stQuery .=   'plh_salary ='.$this->db->escape_str($this->plhSalary).','; 
     	$stQuery .=   'is_active ='.$this->db->escape_str($this->isActive).','; 
          $stQuery .=   'is_blacklist ='.$this->db->escape_str($this->isBlacklist).','; 
     	$stQuery .=   'pic_input ="'.$this->db->escape_str($this->picInput).'",'; 
     	$stQuery .=   'input_time ="'.$this->db->escape_str($this->inputTime).'",'; 
     	$stQuery .=   'pic_edit ="'.$this->db->escape_str($this->picEdit).'",'; 
     	$stQuery .=   'edit_time ="'.$this->db->escape_str($this->editTime).'" '; 
     	$stQuery .= 'WHERE '; 
     	$stQuery .=   'bio_rec_id_plh = '.$this->db->escape_str($id).''; 
     	$this->db->query($stQuery); 
     }
     /* END UPDATE */
     /* START DELETE */
     public function delete($id)
     {
     	$stQuery  = 'DELETE FROM '.$this->myDb.'.'.$this->myTable.' '; 
     	$stQuery .= 'WHERE bio_rec_id_plh = '.$this->db->escape_str($id).''; 
     	$this->db->query($stQuery); 
     }
     /* END DELETE */
     /* START GET ALL DATA */
     public function getAll()
     {
     	$this->db->select('*');
     	$this->db->from($this->myDb.'.'.$this->myTable);
     	$this->db->order_by('bio_rec_id_plh', 'DESC');
     	return $this->db->get()->result_array();
     }
     /* END GET ALL DATA */
     /* START GET DATA BY ID */
     public function getById($id)
     {
     	$this->db->select('*');
     	$this->db->from($this->myDb.'.'.$this->myTable);
     	$this->db->where('bio_rec_id_plh', $this->db->escape_str($id));
     	return $this->db->get()->row_array();
     }
     /* END GET DATA BY ID */
     /* START GET OBJECT DATA BY ID */
     public function getObjectById($id)
     {
     	$this->db->select('*');
     	$this->db->from($this->myDb.'.'.$this->myTable);
     	$this->db->where('bio_rec_id_plh', $this->db->escape_str($id));
     	$row = $this->db->get()->row_array();
     	$this->bioRecIdPlh = $row['bio_rec_id_plh']; 
     	$this->clientName = $row['client_name']; 
     	$this->fullName = $row['full_name']; 
          $this->idCardNo = $row['id_card_no']; 
          $this->position = $row['position']; 
          $this->bpjskes = $row['bpjskes']; 
          $this->bpjstk = $row['bpjstk']; 
     	$this->badgeNo = $row['badge_no']; 
          $this->rteNumber = $row['rte_number']; 
     	$this->monthPeriod = $row['start_date']; 
          $this->monthPeriod = $row['end_date']; 
          $this->monthPeriod = $row['month_period']; 
     	$this->yearPeriod = $row['year_period']; 
     	$this->workDayAmmount = $row['work_day_ammount']; 
     	$this->plhSalary = $row['plh_salary']; 
     	$this->isActive = $row['is_active']; 
          $this->isBlacklist = $row['is_blacklist']; 
     	$this->picInput = $row['pic_input']; 
     	$this->inputTime = $row['input_time']; 
     	$this->picEdit = $row['pic_edit']; 
     	$this->editTime = $row['edit_time']; 
     }
     /* END GET OBJECT DATA BY ID */
     /* START OF GET DATA COUNT */
     public function getDataCount()
     {
     	$stQuery  = 'SELECT bio_rec_id_plh FROM '.$this->myDb.'.'.$this->myTable.' '; 
     	$query = $this->db->query($stQuery);
     	return $query->num_rows();
     }
     /* END OF GET DATA COUNT */
     /* START OF RESET VALUES */
     public function resetValues()
     {
     	$this->bioRecIdPlh = 0; 
     	$this->badgeNo = ''; 
          $this->rteNumber = ''; 
     	$this->clientName = ''; 
          $this->idCardNo = ''; 
          $this->position = ''; 
          $this->bpjskes = ''; 
          $this->bpjstk = ''; 
     	$this->fullName = ''; 
     	$this->workDayAmmount = 0; 
     	$this->startDate = ''; 
          $this->endDate = ''; 
          $this->monthPeriod = ''; 
     	$this->yearPeriod = 0; 
     	$this->plhSalary = 0; 
     	$this->isActive = 0; 
          $this->isBlacklist = 0; 
     	$this->picInput = ''; 
     	$this->inputTime = '0000-00-00 00:00:00'; 
     	$this->picEdit = ''; 
     	$this->editTime = '0000-00-00 00:00:00'; 
     }
     /* END OF RESET VALUES */

     function GenerateNumber($clientName)

     {
          // test($clientName,1);
          $myCurrentDate = GetCurrentDate();
          $Docdate       = $myCurrentDate['CurrentDate'];
          $new_leave     = $this->session->userdata('new_leave');
          $group_code    = '';
          if($clientName == 'Agincourt Resources'){
               $group_code = 'AGR';
          }
          // else if($clientName == 'Pontil'){
          //      $group_code = 'PTL';
          // }
        // else if($clientName == 'Agincourt_Resources'){
        //   $group_code = 'AGN';
        // }
        // else if($clientName == 'Agincourt_Resources'){
        //   $group_code = 'AGN';
        // }
        // else{
          
        // }
          $tahunid = date("Y");        //untuk mendapatkan ID kedua yaitu tahun
          $bulanid = date("m");         //untuk mendapatkan ID ketiga yaitu bulan
          $phId = '';
          $this->db->select('*');
          $this->db->from('db_recruitment.daily_worker');
          $this->db->order_by('bio_rec_id_plh','DESC');    
          $this->db->limit(1);    
          $queryin = $this->db->get();
          if($queryin->num_rows() == 0) //cek
          { 
               $phId = $group_code.$tahunid.$bulanid.'001';   //untuk mengulang ID ketika ID belum di insert  
          }
          else
          {
               $tmpDat=$queryin->row_array();
               $tmp=$tmpDat['bio_rec_id_plh'];
               $tbtahunid = substr($tmp, 3,4);
               $tbbulanid = substr($tmp, 7,2);
               $currNoid  = substr($tmp, 9,3);
               if ( ($tahunid == $tbtahunid) && ((int)$bulanid == (int)$tbbulanid) )
               {
                    $tmpNoid = $currNoid + 1;
                    $idtambahid=str_pad($tmpNoid, 3,"0", STR_PAD_LEFT); //untuk tambahan angka 0
                    $phId = $group_code.$tahunid.$bulanid.$idtambahid; // untuk menampilkan semua ID baru
               }
               else
               {
                    $phId =$group_code.$tahunid.$bulanid.'001'; //untuk mengulang ID baru ketika di bulan berbeda
               }   
          }
          // test($clientName,1);
          return $phId;
     }

    function get_no_doc($periode){
        $sql    = "SELECT IFNULL(LPAD(MAX(SUBSTRING(bio_rec_id_plh,10,3))+1,3,'0'),'001') nomor_dok FROM db_recruitment.daily_worker 
                        WHERE SUBSTRING(bio_rec_id_plh,4,6) = '".$periode."'" ;
        $query  = $this->db->query($sql)->row();
        return $query;

    }
}

?>
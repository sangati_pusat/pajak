<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	function GetCurrentDate() {
		$ci=& get_instance();
		$ci->load->database(); 	
		$strSql  = "SELECT DATE_FORMAT(NOW(),'%Y') CurrentYear,DATE_FORMAT(NOW(),'%m') CurrentMonth,DATE_FORMAT(NOW(),'%d') CurrentDay,";
		$strSql .= "DATE_FORMAT(NOW(),'%Y-%m-%d') CurrentDate, ";
		$strSql .= "DATE_FORMAT(NOW(),'%Y-%m-%d %H:%i:%s') CurrentDateTime, ";
		$strSql .= "DATE_FORMAT(NOW(),'%h-%i-%s') CurrentTime ";
		$query = $ci->db->query($strSql);
        return $query->row_array();
	}

	function GetDbVersion() {
		$ci=& get_instance();
		$ci->load->database(); 	
		$strSql  = "SELECT VERSION() dbVersion";
		$query = $ci->db->query($strSql)->row_array();
        return $query['dbVersion'];
	}

	function GetDbLastDayOfMonth($date) {
		$ci=& get_instance();
		$ci->load->database(); 	
		$strSql  = "SELECT DAY(LAST_DAY('".$date."')) lastDate";
		$query = $ci->db->query($strSql)->row_array();
        return $query['lastDate'];
	}

	function left($str, $length) {
	     return substr($str, 0, $length);
	}

	function right($str, $length) {
	     return substr($str, -$length);
	}

 ?>